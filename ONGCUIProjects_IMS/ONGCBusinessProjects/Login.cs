using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class Login
	{
		public Login()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		

		#region fnValidate User
		public int ValidateUser(ONGCCustumEntity.Login objUser)
		{
			bool b=ListUser(objUser);
			if(b==true)
				return 1;
			else
				return 0;
		}
		#endregion
		
		#region fnListuser
		public bool  ListUser(ONGCCustumEntity.Login objUser)
		{
			
			try
			{
				SqlParameter [] storedParams =  new SqlParameter[3]  ; 
				storedParams[0]= new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedParams[0].Value = "Validate";
				storedParams[1]= new SqlParameter("@UserId",SqlDbType.VarChar);
				storedParams[1].Value = objUser.user_id; 
				storedParams[2]= new SqlParameter("@Password",SqlDbType.VarChar);
				storedParams[2].Value = objUser.password;; 
				SqlDataReader drUser = ONGCDataProjects.SqlHelper.ExecuteReader(strConn,CommandType.StoredProcedure ,"SP_Validate",storedParams);
				
				if(drUser.HasRows == false )
				{

					return false;
				}
				else 
				{  

					return true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}
	
		#endregion

		#region fnLoginDetails
		public string InsertLoginDtl(ONGCCustumEntity.Login objLogin)
		{
			string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
			int iRowsAffected;
			try
			{
				SqlParameter [] storedParams = new SqlParameter[4];
				storedParams[0]= new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedParams[0].Value = "Insert";
			
				storedParams[1]  = new SqlParameter("@UserId",SqlDbType.VarChar);
				storedParams[1].Value = objLogin.user_id; 

				storedParams[2]  = new SqlParameter("@Password",SqlDbType.VarChar);
				storedParams[2].Value = objLogin.password; 
			
				storedParams[3]  = new SqlParameter("@ActiveFlag",SqlDbType.Char);
				storedParams[3].Value = objLogin.active_flag; 
				iRowsAffected = SqlHelper.ExecuteNonQuery (strConn,CommandType.StoredProcedure,"SP_Validate",storedParams);
				return "success";
			}
			catch (SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return ex.Message;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}	
		#endregion

		
		#region BindDropDownListRole
		public  DataSet ListRole(ONGCCustumEntity.Login objLogin)
		{
			ONGCCustumEntity.Login cuslogin=new ONGCCustumEntity.Login();	
			DataSet dsRole; 
			SqlParameter [] storedParams =  new SqlParameter[1]  ; 
			storedParams[0]= new SqlParameter("@E_USER_CODE",SqlDbType.VarChar);
			storedParams[0].Value =objLogin.user_id.ToString();
			dsRole = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_LoginRole",storedParams);
			return dsRole;
		}
		#endregion

	}
}
