using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class ONGC_EmpMstr
	{
		public ONGC_EmpMstr()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region InsertEmployeeMst
		public bool InsertEmployeeMst(ONGCCustumEntity.ONGC_EmpMstr objCusEmployeeMst)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[28];
				
				storedparams[0] = new SqlParameter("@O_CPF_NMBR",SqlDbType.VarChar);
				storedparams[0].Value = objCusEmployeeMst.o_CPF_NMBR;

				storedparams[1] = new SqlParameter("@O_EMP_NM",SqlDbType.VarChar);
				storedparams[1].Value = objCusEmployeeMst.o_EMP_NM;

				storedparams[2] = new SqlParameter("@O_EMP_HM_ADDR1",SqlDbType.VarChar);
				storedparams[2].Value = objCusEmployeeMst.o_EMP_HM_ADDR1;

				storedparams[3] = new SqlParameter("@O_EMP_HM_ADDR2",SqlDbType.VarChar);
				storedparams[3].Value = objCusEmployeeMst.o_EMP_HM_ADDR2;

				storedparams[4] = new SqlParameter("@O_EMP_HM_CITY",SqlDbType.VarChar);
				storedparams[4].Value = objCusEmployeeMst.o_EMP_HM_CITY;

				storedparams[5] = new SqlParameter("@O_EMP_HM_STATE",SqlDbType.VarChar);
				storedparams[5].Value = objCusEmployeeMst.o_EMP_HM_STATE;

				storedparams[6] = new SqlParameter("@O_EMP_HM_PIN_CD",SqlDbType.VarChar);
				storedparams[6].Value = objCusEmployeeMst.o_EMP_HM_PIN_CD;

				storedparams[7] = new SqlParameter("@O_EMP_HM_PHN",SqlDbType.VarChar);
				storedparams[7].Value = objCusEmployeeMst.o_EMP_HM_PHN;

				storedparams[8] = new SqlParameter("@O_EMP_MBL_NMBR",SqlDbType.VarChar);
				storedparams[8].Value = objCusEmployeeMst.o_EMP_MBL_NMBR;

				storedparams[9] = new SqlParameter("@O_EMP_GNDR",SqlDbType.VarChar);
				storedparams[9].Value = objCusEmployeeMst.o_EMP_GNDR;

				storedparams[10] = new SqlParameter("@O_EMP_DESGNTN",SqlDbType.VarChar);
				storedparams[10].Value = objCusEmployeeMst.o_EMP_DESGNTN;

				storedparams[11] = new SqlParameter("@O_EMP_DOB",SqlDbType.VarChar);
				storedparams[11].Value = objCusEmployeeMst.o_EMP_DOB;

				storedparams[12] = new SqlParameter("@O_EMP_SUP_NM",SqlDbType.VarChar);
				storedparams[12].Value = objCusEmployeeMst.o_EMP_SUP_NM;

				storedparams[13] = new SqlParameter("@O_EMP_DEPT_NM",SqlDbType.VarChar);
				storedparams[13].Value = objCusEmployeeMst.o_EMP_DEPT_NM;

				storedparams[14] = new SqlParameter("@O_EMP_WRK_LCTN",SqlDbType.VarChar);
				storedparams[14].Value = objCusEmployeeMst.o_EMP_WRK_LCTN;

				storedparams[15] = new SqlParameter("@O_EMP_BLD_GRP",SqlDbType.VarChar);
				storedparams[15].Value = objCusEmployeeMst.o_EMP_BLD_GRP;

				storedparams[16] = new SqlParameter("@O_USER_CODE",SqlDbType.VarChar);
				storedparams[16].Value = objCusEmployeeMst.o_USER_CODE;

				storedparams[17] = new SqlParameter("@O_BIT_CD",SqlDbType.VarChar);
				storedparams[17].Value = objCusEmployeeMst.o_BIT_CD;

				storedparams[18] = new SqlParameter("@O_UPDATE_BIT",SqlDbType.VarChar);
				storedparams[18].Value = objCusEmployeeMst.o_UPDATE_BIT;

				storedparams[19] = new SqlParameter("@O_LEVEL",SqlDbType.VarChar);
				storedparams[19].Value = objCusEmployeeMst.o_LEVEL;

				storedparams[20] = new SqlParameter("@O_REP",SqlDbType.VarChar);
				storedparams[20].Value = objCusEmployeeMst.o_REP;

				storedparams[21] = new SqlParameter("@O_REV",SqlDbType.VarChar);
				storedparams[21].Value = objCusEmployeeMst.o_REV;

				storedparams[22] = new SqlParameter("@O_MGR",SqlDbType.VarChar);
				storedparams[22].Value = objCusEmployeeMst.o_MGR;

				storedparams[23] = new SqlParameter("@O_ACP_1",SqlDbType.VarChar);
				storedparams[23].Value = objCusEmployeeMst.o_ACP_1;

				storedparams[24] = new SqlParameter("@O_ACP_2",SqlDbType.VarChar);
				storedparams[24].Value = objCusEmployeeMst.o_ACP_2;
				
				storedparams[25] = new SqlParameter("@O_EMAIL_ID1",SqlDbType.VarChar);
				storedparams[25].Value = objCusEmployeeMst.o_EMAIL_ID1;

				storedparams[26] = new SqlParameter("@O_EMAIL_ID2",SqlDbType.VarChar);
				storedparams[26].Value = objCusEmployeeMst.o_EMAIL_ID2;

				storedparams[27] = new SqlParameter("@O_EMAIL_ID3",SqlDbType.VarChar);
				storedparams[27].Value = objCusEmployeeMst.o_EMAIL_ID3;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"CY_SP_InsertONGCEmpMstr",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion
	}
}
