using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class Complaint
	{
		public Complaint()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region Fill DropDown Complaint Type
		public  DataSet fillComplaintTypeDdl()
		{
			DataSet dsLocation; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillComplaintDDL";
			dsLocation = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsLocation;
		}
		#endregion

		#region Insert Complaint Registration
		public bool ComplaintRegistration(ONGCCustumEntity.Complaint objcusComplaint)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[8];

				storedparams[0]=new SqlParameter("@O_APLCTN_NM",SqlDbType.VarChar);
				storedparams[0].Value = objcusComplaint.o_APLCTN_NM;

				storedparams[1] = new SqlParameter("@O_APLCTN_DSG",SqlDbType.VarChar);
				storedparams[1].Value = objcusComplaint.o_APLCTN_DSG;

				storedparams[2] = new SqlParameter("@O_APLCTN_CPF_NMBR",SqlDbType.VarChar);
				storedparams[2].Value = objcusComplaint.o_APLCTN_CPF_NMBR;
				
				storedparams[3] = new SqlParameter("@O_APLCTN_LCTN",SqlDbType.VarChar);
				storedparams[3].Value = objcusComplaint.o_APLCTN_LCTN;

				storedparams[4] = new SqlParameter("@O_CMPLNT_TYPE",SqlDbType.VarChar);
				storedparams[4].Value = objcusComplaint.o_CMPLNT_TYPE;

				storedparams[5] = new SqlParameter("@O_CMPLNT_DTL",SqlDbType.VarChar);
				storedparams[5].Value = objcusComplaint.o_CMPLNT_DTL;

				storedparams[6] = new SqlParameter("@O_CMPLNT_OFF_NM",SqlDbType.VarChar);
				storedparams[6].Value = objcusComplaint.o_CMPLNT_OFF_NM;
			
				storedparams[7] = new SqlParameter("@O_CMPLNT_OFF_CPF_NMBR",SqlDbType.VarChar);
				storedparams[7].Value = objcusComplaint.o_CMPLNT_OFF_CPF_NMBR;
			
				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertComplaint",storedparams));
				
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion
	}
}
