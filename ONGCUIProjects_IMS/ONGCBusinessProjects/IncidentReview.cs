using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class IncidentReview
	{
		public IncidentReview()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region InsertReviewDetails
		public bool InsertReviewDetails(ONGCCustumEntity.IncidentReview objCusReview)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[16];
				
				storedparams[0] = new SqlParameter("@O_INCIDENT_NMBR",SqlDbType.Int);
				storedparams[0].Value = objCusReview.o_INCIDENT_NMBR;

				storedparams[1] = new SqlParameter("@O_REV_INCDNT_CTGRY",SqlDbType.VarChar);
				storedparams[1].Value = objCusReview.o_REV_INCDNT_CTGRY;

				storedparams[2] = new SqlParameter("@O_REV_NM",SqlDbType.VarChar);
				storedparams[2].Value = objCusReview.o_REV_NM;

				storedparams[3] = new SqlParameter("@O_REV_CPF_NMBR",SqlDbType.VarChar);
				storedparams[3].Value = objCusReview.o_REV_CPF_NMBR;

				storedparams[4] = new SqlParameter("@O_REV_ROOT_CAUSE",SqlDbType.VarChar);
				storedparams[4].Value = objCusReview.o_REV_ROOT_CAUSE;

				storedparams[5] = new SqlParameter("@O_REV_RECMND",SqlDbType.VarChar);
				storedparams[5].Value = objCusReview.o_REV_RECMND;

				storedparams[6] = new SqlParameter("@O_REV_PRECTN_MEASR",SqlDbType.VarChar);
				storedparams[6].Value = objCusReview.o_REV_PRECTN_MEASR;

				storedparams[7] = new SqlParameter("@O_REV_ACTION_BY_NM",SqlDbType.VarChar);
				storedparams[7].Value = objCusReview.o_REV_ACTION_BY_NM;

				storedparams[8] = new SqlParameter("@O_REV_ACTION_BY_CPF_NMBR",SqlDbType.VarChar);
				storedparams[8].Value = objCusReview.o_REV_ACTION_BY_CPF_NMBR;

				storedparams[9] = new SqlParameter("@O_REV_RMRKS",SqlDbType.VarChar);
				storedparams[9].Value = objCusReview.o_REV_RMRKS;


				storedparams[10] = new SqlParameter("@O_INCDNT_DESC",SqlDbType.VarChar);
				storedparams[10].Value = objCusReview.o_INCDNT_DESC;

				// Severity Matrix

				storedparams[11] = new SqlParameter("@O_SEVR_TYPE",SqlDbType.VarChar);
				storedparams[11].Value = objCusReview.o_SEVR_TYPE;

				storedparams[12] = new SqlParameter("@O_SEVR_MIN_LOSS",SqlDbType.VarChar);
				storedparams[12].Value = objCusReview.o_SEVR_MIN_LOSS;

				storedparams[13] = new SqlParameter("@O_SEVR_MAX_LOSS",SqlDbType.VarChar);
				storedparams[13].Value = objCusReview.o_SEVR_MAX_LOSS;

				storedparams[14] = new SqlParameter("@O_ROOT_CS_EMP_NM",SqlDbType.VarChar);
				storedparams[14].Value = objCusReview.o_ROOT_CS_EMP_NM;

				storedparams[15] = new SqlParameter("@O_ROOT_CS_EMP_NMBR",SqlDbType.VarChar);
				storedparams[15].Value = objCusReview.o_ROOT_CS_EMP_NMBR;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertReviewDetails",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertActionDetails
		public bool InsertActionDetails(ONGCCustumEntity.IncidentReview objCusReview)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[10];

				storedparams[0] = new SqlParameter("@O_REV_NMBR",SqlDbType.Int);
				storedparams[0].Value = objCusReview.o_REV_NMBR;
				
				storedparams[1] = new SqlParameter("@O_INCIDENT_NMBR",SqlDbType.Int);
				storedparams[1].Value = objCusReview.o_INCIDENT_NMBR;

				storedparams[2] = new SqlParameter("@O_ACTN_NM",SqlDbType.VarChar);
				storedparams[2].Value = objCusReview.o_ACTN_NM;

				storedparams[3] = new SqlParameter("@O_ACTN_CPF_NMBR",SqlDbType.VarChar);
				storedparams[3].Value = objCusReview.o_ACTN_CPF_NMBR;

				storedparams[4] = new SqlParameter("@O_ACTN_TAKN",SqlDbType.VarChar);
				storedparams[4].Value = objCusReview.o_ACTN_TAKN;

				storedparams[5] = new SqlParameter("@O_RMRKS",SqlDbType.VarChar);
				storedparams[5].Value = objCusReview.o_RMRKS;

				storedparams[6] = new SqlParameter("@O_ACPTNC_NM",SqlDbType.VarChar);
				storedparams[6].Value = objCusReview.o_ACPTNC_NM;

				storedparams[7] = new SqlParameter("@O_ACPTNC_CPF_NMBR",SqlDbType.VarChar);
				storedparams[7].Value = objCusReview.o_ACPTNC_CPF_NMBR;

				storedparams[8] = new SqlParameter("@O_INCDNT_DESC",SqlDbType.VarChar);
				storedparams[8].Value = objCusReview.o_INCDNT_DESC;

				storedparams[9] = new SqlParameter("@O_ACTN_TRGT_DT",SqlDbType.VarChar);
				storedparams[9].Value = objCusReview.o_ACTN_TRGT_DT;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertIncidentActionDetails",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertExecuterDetails
		public bool InsertExecuterDetails(ONGCCustumEntity.IncidentReview objCusReview)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[6];

				storedparams[0] = new SqlParameter("@O_REV_NMBR",SqlDbType.Int);
				storedparams[0].Value = objCusReview.o_REV_NMBR;
				
				storedparams[1] = new SqlParameter("@O_INCIDENT_NMBR",SqlDbType.Int);
				storedparams[1].Value = objCusReview.o_INCIDENT_NMBR;

				storedparams[2] = new SqlParameter("@O_ACTN_NMBR",SqlDbType.Int);
				storedparams[2].Value = objCusReview.o_ACTN_NMBR;

				storedparams[3] = new SqlParameter("@O_INCDNT_DESC",SqlDbType.VarChar);
				storedparams[3].Value = objCusReview.o_INCDNT_DESC;

				storedparams[4] = new SqlParameter("@O_RMRKS",SqlDbType.VarChar);
				storedparams[4].Value = objCusReview.o_RMRKS;

				storedparams[5] = new SqlParameter("@O_USR_CODE",SqlDbType.VarChar);
				storedparams[5].Value = objCusReview.o_USR_CODE;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertExecuterDetails",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertIncidenceAcceptanceDetails
		public bool InsertIncidenceAcceptanceDetails(ONGCCustumEntity.IncidentReview objCusReview)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[13];

				storedparams[0] = new SqlParameter("@O_REV_NMBR",SqlDbType.Int);
				storedparams[0].Value = objCusReview.o_REV_NMBR;
				
				storedparams[1] = new SqlParameter("@O_INCDNT_NMBR",SqlDbType.Int);
				storedparams[1].Value = objCusReview.o_INCDNT_NMBR;

				storedparams[2] = new SqlParameter("@O_ACTN_NMBR",SqlDbType.VarChar);
				storedparams[2].Value = objCusReview.o_ACTN_NMBR;

				storedparams[3] = new SqlParameter("@O_INCDNT_DESC",SqlDbType.VarChar);
				storedparams[3].Value = objCusReview.o_INCDNT_DESC;

				storedparams[4] = new SqlParameter("@O_SELF_CMNT",SqlDbType.VarChar);
				storedparams[4].Value = objCusReview.o_SELF_CMNT;

				storedparams[5] = new SqlParameter("@O_ACTN_REV_REQ",SqlDbType.VarChar);
				storedparams[5].Value = objCusReview.o_ACTN_REV_REQ;

				storedparams[6] = new SqlParameter("@O_INC_DECSN",SqlDbType.VarChar);
				storedparams[6].Value = objCusReview.o_INC_DECSN;

				storedparams[7] = new SqlParameter("@O_ACPTN2_NM",SqlDbType.VarChar);
				storedparams[7].Value = objCusReview.o_ACPTN2_NM;

				storedparams[8] = new SqlParameter("@O_ACPTN2_CPF_NMBR",SqlDbType.VarChar);
				storedparams[8].Value = objCusReview.o_ACPTN2_CPF_NMBR;

				storedparams[9] = new SqlParameter("@O_ACPTN1_NM",SqlDbType.VarChar);
				storedparams[9].Value = objCusReview.o_ACPTN1_NM;

				storedparams[10] = new SqlParameter("@O_ACPTN1_CPF_NMBR",SqlDbType.VarChar);
				storedparams[10].Value = objCusReview.o_ACPTN1_CPF_NMBR;

				storedparams[11] = new SqlParameter("@O_ACPTN1_FLG",SqlDbType.VarChar);
				storedparams[11].Value = objCusReview.o_ACPTN1_FLG;

				storedparams[12] = new SqlParameter("@O_ACTN_CPF_NMBR",SqlDbType.VarChar);
				storedparams[12].Value = objCusReview.o_ACTN_CPF_NMBR;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertIncidentAcceptanceDetails",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertIncidenceFinalAcceptanceDetails
		public bool InsertIncidenceFinalAcceptanceDetails(ONGCCustumEntity.IncidentReview objCusReview)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[15];

				storedparams[0] = new SqlParameter("@O_REV_NMBR",SqlDbType.Int);
				storedparams[0].Value = objCusReview.o_REV_NMBR;
				
				storedparams[1] = new SqlParameter("@O_INCDNT_NMBR",SqlDbType.Int);
				storedparams[1].Value = objCusReview.o_INCDNT_NMBR;

				storedparams[2] = new SqlParameter("@O_ACTN_NMBR",SqlDbType.VarChar);
				storedparams[2].Value = objCusReview.o_ACTN_NMBR;

				storedparams[3] = new SqlParameter("@O_ACPTN_1_NMBR",SqlDbType.VarChar);
				storedparams[3].Value = objCusReview.o_ACPTN_1_NMBR;

				storedparams[4] = new SqlParameter("@O_INCDNT_DESC",SqlDbType.VarChar);
				storedparams[4].Value = objCusReview.o_INCDNT_DESC;

				storedparams[5] = new SqlParameter("@O_ACTN_REV_REQ",SqlDbType.VarChar);
				storedparams[5].Value = objCusReview.o_ACTN_REV_REQ;

				storedparams[6] = new SqlParameter("@O_INC_DECSN",SqlDbType.VarChar);
				storedparams[6].Value = objCusReview.o_INC_DECSN;

				storedparams[7] = new SqlParameter("@O_SELF_CMNT",SqlDbType.VarChar);
				storedparams[7].Value = objCusReview.o_SELF_CMNT;

				storedparams[8] = new SqlParameter("@O_ACTN_TKN",SqlDbType.VarChar);
				storedparams[8].Value = objCusReview.o_ACTN_TKN;

				storedparams[9] = new SqlParameter("@O_REV_RMRKS",SqlDbType.VarChar);
				storedparams[9].Value = objCusReview.o_REV_RMRKS;

				storedparams[10] = new SqlParameter("@O_ACPTN1_CPF_NMBR",SqlDbType.VarChar);
				storedparams[10].Value = objCusReview.o_ACPTN1_CPF_NMBR;

				storedparams[11] = new SqlParameter("@O_ACPTN1_NM",SqlDbType.VarChar);
				storedparams[11].Value = objCusReview.o_ACPTN1_NM;

				storedparams[12] = new SqlParameter("@O_USR_LGN_NM",SqlDbType.VarChar);
				storedparams[12].Value = objCusReview.o_USR_LGN_NM;

				storedparams[13] = new SqlParameter("@O_USR_CPF_NMBR",SqlDbType.VarChar);
				storedparams[13].Value = objCusReview.o_USR_CPF_NMBR;

				storedparams[14] = new SqlParameter("@O_ACPTN2_FLG",SqlDbType.VarChar);
				storedparams[14].Value = objCusReview.o_ACPTN2_FLG;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertIncidentFinalAcceptanceDetails",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region Insert Root Cause Analysis Details
		public bool InsertRootAnalysisDetails(ONGCCustumEntity.IncidentReview objCusReview)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[6];
				
				storedparams[0] = new SqlParameter("@O_INCIDENT_NMBR",SqlDbType.Int);
				storedparams[0].Value = objCusReview.o_INCIDENT_NMBR;

				storedparams[1] = new SqlParameter("@O_REV_INCDNT_CTGRY",SqlDbType.VarChar);
				storedparams[1].Value = objCusReview.o_REV_INCDNT_CTGRY;

				storedparams[2] = new SqlParameter("@O_REV_ROOT_CAUSE",SqlDbType.VarChar);
				storedparams[2].Value = objCusReview.o_REV_ROOT_CAUSE;

				storedparams[3] = new SqlParameter("@O_INCDNT_DESC",SqlDbType.VarChar);
				storedparams[3].Value = objCusReview.o_INCDNT_DESC;

				storedparams[4] = new SqlParameter("@O_ROOT_CS_EMP_NMBR",SqlDbType.VarChar);
				storedparams[4].Value = objCusReview.o_ROOT_CS_EMP_NMBR;

				storedparams[5] = new SqlParameter("@O_RT_CAUSE_ANALYSIS_DESC",SqlDbType.VarChar);
				storedparams[5].Value = objCusReview.o_RT_CAUSE_ANALYSIS_DESC;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertRootCauseAnalysis",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion


	}
}
