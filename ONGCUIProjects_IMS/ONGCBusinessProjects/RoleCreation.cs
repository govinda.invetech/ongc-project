using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;
namespace ONGCBusinessProjects
{
	public class RoleCreation
	{
		public RoleCreation()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region InsertUserdtls
		public bool InsertUserDtl(ONGCCustumEntity.RoleCreation objcusRole)
		{
		
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@P_ROLE_ID",SqlDbType.VarChar);
				storedparams[0].Value = objcusRole.p_ROLE_ID;

				storedparams[1] = new SqlParameter("@P_ROLE_DSCRPTN",SqlDbType.VarChar);
				storedparams[1].Value = objcusRole.p_ROLE_DSCRPTN;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"RoleChkExist",storedparams));
				
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region Insert InTo RoleDetails
		public void InsertRoleDetails(ONGCCustumEntity.RoleCreation objcusRole)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@P_ROLE_ID",SqlDbType.VarChar);
				storedparams[0].Value = objcusRole.p_ROLE_ID;

				storedparams[1] = new SqlParameter("@P_CMPNT_CODE",SqlDbType.VarChar);
				storedparams[1].Value = objcusRole.p_CMPNT_CODE;
				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"ChkRoleExist_E_ROLE_DTLS",storedparams));
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region Insert InTo RoleModuleDetails
		public void InsertRoleModuleDetails(ONGCCustumEntity.RoleCreation objcusRole)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@P_ROLE_ID",SqlDbType.VarChar);
				storedparams[0].Value = objcusRole.p_ROLE_ID;

				storedparams[1] = new SqlParameter("@P_ROLE_MDLE",SqlDbType.VarChar);
				storedparams[1].Value = objcusRole.p_ROLE_MDLE;
				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"ChkModuleExist_E_ROLE_MDLE_DTLS",storedparams));
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region  DeleteRoleDetails
		public void DeleteRoleModuleDetails(ONGCCustumEntity.RoleCreation objcusRole)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[1];
				storedparams[0]=new SqlParameter("@P_ROLE_ID",SqlDbType.VarChar);
				storedparams[0].Value = objcusRole.p_ROLE_ID;
				SqlHelper.ExecuteNonQuery(strConn,CommandType.StoredProcedure,"RoleModuleDetailsDelByRoleID",storedparams);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion
	}
}
