using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class GuestHouse
	{
		public GuestHouse()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region InsertGuestHouseBooking
		public bool InsertGuestHouseBooking(ONGCCustumEntity.GuestHouse objCusGuestHouse)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[14];
				
				storedparams[0] = new SqlParameter("@O_APLCNT_NM",SqlDbType.VarChar);
				storedparams[0].Value = objCusGuestHouse.o_APLCNT_NM;

				storedparams[1] = new SqlParameter("@O_APLCNT_CPF_NMBR",SqlDbType.VarChar);
				storedparams[1].Value = objCusGuestHouse.o_APLCNT_CPF_NMBR;

				storedparams[2] = new SqlParameter("@O_APLCNT_DESG",SqlDbType.VarChar);
				storedparams[2].Value = objCusGuestHouse.o_APLCNT_DESG;

				storedparams[3] = new SqlParameter("@O_APLCNT_LCTN",SqlDbType.VarChar);
				storedparams[3].Value = objCusGuestHouse.o_APLCNT_LCTN;

				storedparams[4] = new SqlParameter("@O_APLCNT_EXT_NMBR",SqlDbType.VarChar);
				storedparams[4].Value = objCusGuestHouse.o_APLCNT_EXT_NMBR;

				storedparams[5] = new SqlParameter("@O_BKNG_START_DT_TM",SqlDbType.VarChar);
				storedparams[5].Value = objCusGuestHouse.o_BKNG_START_DT_TM;

				storedparams[6] = new SqlParameter("@O_BKNG_END_DT_TM",SqlDbType.VarChar);
				storedparams[6].Value = objCusGuestHouse.o_BKNG_END_DT_TM;

				storedparams[7] = new SqlParameter("@O_BKNG_TYP",SqlDbType.VarChar);
				storedparams[7].Value = objCusGuestHouse.o_BKNG_TYP;

				storedparams[8] = new SqlParameter("@O_BKNG_FD_TYP",SqlDbType.VarChar);
				storedparams[8].Value = objCusGuestHouse.o_BKNG_FD_TYP;

				storedparams[9] = new SqlParameter("@O_BKNG_PRPS",SqlDbType.VarChar);
				storedparams[9].Value = objCusGuestHouse.o_BKNG_PRPS;

				storedparams[10] = new SqlParameter("@O_RMRKS",SqlDbType.VarChar);
				storedparams[10].Value = objCusGuestHouse.o_RMRKS;

				storedparams[11] = new SqlParameter("@O_BKNG_APRVL_OFF_NM",SqlDbType.VarChar);
				storedparams[11].Value = objCusGuestHouse.o_BKNG_APRVL_OFF_NM;

				storedparams[12] = new SqlParameter("@O_BKNG_APRVL_OFF_DESG",SqlDbType.VarChar);
				storedparams[12].Value = objCusGuestHouse.o_BKNG_APRVL_OFF_DESG;

				storedparams[13] = new SqlParameter("@O_BKNG_APRVL_OFF_CPF_NMBR",SqlDbType.VarChar);
				storedparams[13].Value = objCusGuestHouse.o_BKNG_APRVL_OFF_CPF_NMBR;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_GuestHouseBkngDtl",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion
	}
}
