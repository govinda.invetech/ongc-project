using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;
namespace ONGCBusinessProjects
{
	public class GatePassReq
	{
		public GatePassReq()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region Insert GatePassReq
		public bool GatePassReqest(ONGCCustumEntity.GatePassReq objCusGatePassReq)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[20];
				
				storedparams[0] = new SqlParameter("@O_APLCNT_NM",SqlDbType.VarChar);
				storedparams[0].Value = objCusGatePassReq.o_APLCNT_NM;

				storedparams[1] = new SqlParameter("@O_APLCNT_CPF_NMBR",SqlDbType.VarChar);
				storedparams[1].Value = objCusGatePassReq.o_APLCNT_CPF_NMBR;

				storedparams[2] = new SqlParameter("@O_APLCNT_DESG",SqlDbType.VarChar);
				storedparams[2].Value = objCusGatePassReq.o_APLCNT_DESG;

				storedparams[3] = new SqlParameter("@O_APLCNT_LCTN",SqlDbType.VarChar);
				storedparams[3].Value = objCusGatePassReq.o_APLCNT_LCTN;

				storedparams[4] = new SqlParameter("@O_APLCNT_EXT_NMBR",SqlDbType.VarChar);
				storedparams[4].Value = objCusGatePassReq.o_APLCNT_EXT_NMBR;

				storedparams[5] = new SqlParameter("@O_REQSTN_TYPE",SqlDbType.VarChar);
				storedparams[5].Value = objCusGatePassReq.o_REQSTN_TYPE;

				storedparams[6] = new SqlParameter("@O_ENTRY_DT",SqlDbType.VarChar);
				storedparams[6].Value = objCusGatePassReq.o_ENTRY_DT;

				storedparams[7] = new SqlParameter("@O_EXIT_DT",SqlDbType.VarChar);
				storedparams[7].Value = objCusGatePassReq.o_EXIT_DT;

				storedparams[8] = new SqlParameter("@O_ENTRY_AREA",SqlDbType.VarChar);
				storedparams[8].Value = objCusGatePassReq.o_ENTRY_AREA;

				storedparams[9] = new SqlParameter("@O_MTNG_OFF_NM",SqlDbType.VarChar);
				storedparams[9].Value = objCusGatePassReq.o_MTNG_OFF_NM;

				storedparams[10] = new SqlParameter("@O_MTNG_OFF_DESG",SqlDbType.VarChar);
				storedparams[10].Value = objCusGatePassReq.o_MTNG_OFF_DESG;

				storedparams[11] = new SqlParameter("@O_MTNG_OFF_LCTN",SqlDbType.VarChar);
				storedparams[11].Value = objCusGatePassReq.o_MTNG_OFF_LCTN;

				storedparams[12] = new SqlParameter("@O_VISTR_NM",SqlDbType.VarChar);
				storedparams[12].Value = objCusGatePassReq.o_VISTR_NM;

				storedparams[13] = new SqlParameter("@O_VISTR_ORGNZTN",SqlDbType.VarChar);
				storedparams[13].Value = objCusGatePassReq.o_VISTR_ORGNZTN;

				storedparams[14] = new SqlParameter("@O_VISTR_AGE",SqlDbType.VarChar);
				storedparams[14].Value = objCusGatePassReq.o_VISTR_AGE;

				storedparams[15] = new SqlParameter("@O_VISTR_GNDR",SqlDbType.VarChar);
				storedparams[15].Value = objCusGatePassReq.o_VISTR_GNDR;

				storedparams[16] = new SqlParameter("@O_PRPS_OF_VISIT",SqlDbType.VarChar);
				storedparams[16].Value = objCusGatePassReq.o_PRPS_OF_VISIT;

				storedparams[17] = new SqlParameter("@O_RMRKS",SqlDbType.VarChar);
				storedparams[17].Value = objCusGatePassReq.o_RMRKS;

				storedparams[18] = new SqlParameter("@O_ACP1_NM",SqlDbType.VarChar);
				storedparams[18].Value = objCusGatePassReq.o_ACP1_NM;

				storedparams[19] = new SqlParameter("@O_ACP1_CPF_NMBR",SqlDbType.VarChar);
				storedparams[19].Value = objCusGatePassReq.o_ACP1_CPF_NMBR;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_GatePassReq",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region Fill DropDown of Location Name
		public  DataSet fillLocationDdl()
		{
			DataSet dsLocation; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillLocationDDL";
			dsLocation = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsLocation;
		}
		#endregion

		#region Fill DropDown of Employee Name
		public  DataSet fillEmployeeDdl()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillEmpDDL";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion

		#region Approver Level 1 List
		public  DataSet Approver1()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "ApproverEmp1";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion

		#region Contract Cell List
		public  DataSet ContractCell()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "ContractCellEmp";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion

		#region Security Employee List
		public  DataSet Security()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "SecurityEmp";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion

		#region Fill DropDown of Employee Name - Reviewer Only
		public  DataSet fillEmployeeRevDdl()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillEmpRevDDL";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion

		#region Fill DropDown of Incident Status
		public  DataSet fillIncStatusddl()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillIncStatusDDL";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion
	}
}
