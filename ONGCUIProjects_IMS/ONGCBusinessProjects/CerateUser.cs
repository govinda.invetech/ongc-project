#region NameSpaces
using System;
using System.Data; 
using System.Data.SqlClient; 
using ONGCDataProjects; 
#endregion

namespace ONGCBusinessProjects
{
	public class CerateUser
	{
		public CerateUser()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		#region CreateUser()
		public bool CreateUser(ONGCCustumEntity.CerateUser objUser)
		{
        
			try
			{
				string SP_Check_User = "UserChkExist";
				SqlParameter [] sqlpCreateUser = new SqlParameter[5];
			
				
				sqlpCreateUser[0] = new SqlParameter("@E_USER_CODE", objUser.UserCode);
				sqlpCreateUser[0].SqlDbType = SqlDbType.VarChar;
				sqlpCreateUser[0].Direction = ParameterDirection.Input;


				sqlpCreateUser[1] = new SqlParameter("@E_USER_DSCRPTN", objUser.UserDescription);
				sqlpCreateUser[1].SqlDbType = SqlDbType.VarChar;
				sqlpCreateUser[1].Direction = ParameterDirection.Input;


				sqlpCreateUser[2] = new SqlParameter("@E_USER_PSWRD", objUser.UserPassword);
				sqlpCreateUser[2].SqlDbType = SqlDbType.VarChar;
				sqlpCreateUser[2].Direction = ParameterDirection.Input;


				sqlpCreateUser[3] = new SqlParameter("@E_USER_NM",objUser.UserName);
				sqlpCreateUser[3].SqlDbType = SqlDbType.VarChar;
				sqlpCreateUser[3].Direction = ParameterDirection.Input;


				sqlpCreateUser[4] = new SqlParameter("@E_ROLE_ID", objUser.RoleID);
				sqlpCreateUser[4].SqlDbType = SqlDbType.VarChar;
				sqlpCreateUser[4].Direction = ParameterDirection.Input;
            
				int i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,SP_Check_User,sqlpCreateUser));
                
				
				if (i == 0)  
				{
					return true;
				}

				else
				{					
					return false;
				}
			}
			catch (IndexOutOfRangeException objIndexRange)
			{
				throw objIndexRange;
			}
			catch (SqlException objSqlEx)
			{
				throw objSqlEx;
			}
			catch (Exception objUserEx)
			{
				throw objUserEx;
			}
     
		}
		#endregion 

		#region FillUserAndRole()-(Sunil)
		public DataSet FillUserAndRole()
		{
			string SP_USER_ROLE_SELECT ="USER_AND_ROLE_SEL";
			DataSet ds;
			ds = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure,SP_USER_ROLE_SELECT);
			return ds;
		}

		#endregion
	}
}
