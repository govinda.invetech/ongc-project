using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class IncidentManagement
	{
		public IncidentManagement()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region Fill DropDown of Employee Name
		public  DataSet fillEmployeeDdl()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillEmpDDL";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion

		#region Fill DropDown of IncidentCategory
		public  DataSet fillIncCatgry()
		{
			DataSet IncCatgry; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillIncCatgry";
			IncCatgry = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return IncCatgry;
		}
		#endregion

		#region IncidentManagement
		public bool IncidentMgmt(ONGCCustumEntity.IncidentManagement objCusIncCatgry)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[10];
		
				storedparams[0] = new SqlParameter("@O_ENG_NM",SqlDbType.VarChar);
				storedparams[0].Value = objCusIncCatgry.o_ENG_NM;

				storedparams[1] = new SqlParameter("@O_ENG_DSG",SqlDbType.VarChar);
				storedparams[1].Value = objCusIncCatgry.o_ENG_DSG;

				storedparams[2] = new SqlParameter("@O_CPF_NMBR",SqlDbType.VarChar);
				storedparams[2].Value = objCusIncCatgry.o_CPF_NMBR;

				storedparams[3] = new SqlParameter("@O_INCDNT_DESCRPTN",SqlDbType.VarChar);
				storedparams[3].Value = objCusIncCatgry.o_INCDNT_DESCRPTN;

				storedparams[4] = new SqlParameter("@O_INCDNT_DT",SqlDbType.VarChar);
				storedparams[4].Value = objCusIncCatgry.o_INCDNT_DT;

				storedparams[5] = new SqlParameter("@O_INCDNT_TIME",SqlDbType.VarChar);
				storedparams[5].Value = objCusIncCatgry.o_INCDNT_TIME;

				storedparams[6] = new SqlParameter("@O_INCDNT_LCTN",SqlDbType.VarChar);
				storedparams[6].Value = objCusIncCatgry.o_INCDNT_LCTN;

				storedparams[7] = new SqlParameter("@O_RUSH_EMRGNCY",SqlDbType.VarChar);
				storedparams[7].Value = objCusIncCatgry.o_RUSH_EMRGNCY;

				storedparams[8] = new SqlParameter("@O_EMP_NM",SqlDbType.VarChar);
				storedparams[8].Value = objCusIncCatgry.o_EMP_NM;

				storedparams[9] = new SqlParameter("@O_RMRKS",SqlDbType.VarChar);
				storedparams[9].Value = objCusIncCatgry.o_RMRKS;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_IncidentManagement",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion
	}
}
