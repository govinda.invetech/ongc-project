using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class TransportBooking
	{
		public TransportBooking()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region InserTransportBooking
		public bool InsertTransportBooking(ONGCCustumEntity.TransportBooking objcusTransportBooking)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[19];
				storedparams[0]=new SqlParameter("@O_APLCNT_NM",SqlDbType.VarChar);
				storedparams[0].Value = objcusTransportBooking.o_APLCNT_NM;

				storedparams[1] = new SqlParameter("@O_APLCNT_CPF_NMBR",SqlDbType.VarChar);
				storedparams[1].Value = objcusTransportBooking.o_APLCNT_CPF_NMBR;

				storedparams[2] = new SqlParameter("@O_APLCNT_DESG",SqlDbType.VarChar);
				storedparams[2].Value = objcusTransportBooking.o_APLCNT_DESG;
				
				storedparams[3] = new SqlParameter("@O_APLCNT_LCTN",SqlDbType.VarChar);
				storedparams[3].Value = objcusTransportBooking.o_APLCNT_LCTN;

				storedparams[4] = new SqlParameter("@O_APLCNT_EXT_NMBR",SqlDbType.VarChar);
				storedparams[4].Value = objcusTransportBooking.o_APLCNT_EXT_NMBR;

				storedparams[5] = new SqlParameter("@O_BKNG_START_DT_TM",SqlDbType.VarChar);
				storedparams[5].Value = objcusTransportBooking.o_BKNG_START_DT_TM;

				storedparams[6] = new SqlParameter("@O_BKNG_END_DT_TM",SqlDbType.VarChar);
				storedparams[6].Value = objcusTransportBooking.o_BKNG_END_DT_TM;

				storedparams[7] = new SqlParameter("@O_BKNG_TYP",SqlDbType.VarChar);
				storedparams[7].Value = objcusTransportBooking.o_BKNG_TYP;
				
				storedparams[8] = new SqlParameter("@O_RPRTNG_TM",SqlDbType.VarChar);
				storedparams[8].Value = objcusTransportBooking.o_RPRTNG_TM;

				storedparams[9] = new SqlParameter("@O_RPRT_TO_NM",SqlDbType.VarChar);
				storedparams[9].Value = objcusTransportBooking.o_RPRT_TO_NM;

				storedparams[10] = new SqlParameter("@O_RPRT_TO_ADDR",SqlDbType.VarChar);
				storedparams[10].Value = objcusTransportBooking.o_RPRT_TO_ADDR;

				storedparams[11] = new SqlParameter("@O_RPRT_TO_CONT_NMBR1",SqlDbType.VarChar);
				storedparams[11].Value = objcusTransportBooking.o_RPRT_TO_CONT_NMBR1;

				storedparams[12] = new SqlParameter("@O_RPRT_TO_CONT_NMBR2",SqlDbType.VarChar);
				storedparams[12].Value = objcusTransportBooking.o_RPRT_TO_CONT_NMBR2;
				
				storedparams[13] = new SqlParameter("@O_NO_OF_PERSONS",SqlDbType.VarChar);
				storedparams[13].Value = objcusTransportBooking.o_NO_OF_PERSONS;

				storedparams[14] = new SqlParameter("@O_BKNG_PRPS",SqlDbType.VarChar);
				storedparams[14].Value = objcusTransportBooking.o_BKNG_PRPS;

				storedparams[15] = new SqlParameter("@O_RMRKS",SqlDbType.VarChar);
				storedparams[15].Value = objcusTransportBooking.o_RMRKS;

				storedparams[16] = new SqlParameter("@O_BKNG_APRVL_OFF_NM",SqlDbType.VarChar);
				storedparams[16].Value = objcusTransportBooking.o_BKNG_APRVL_OFF_NM;

				storedparams[17] = new SqlParameter("@O_BKNG_APRVL_OFF_DESG",SqlDbType.VarChar);
				storedparams[17].Value = objcusTransportBooking.o_BKNG_APRVL_OFF_DESG;
				
				storedparams[18] = new SqlParameter("@O_BKNG_APRVL_OFF_CPF_NMBR",SqlDbType.VarChar);
				storedparams[18].Value = objcusTransportBooking.o_BKNG_APRVL_OFF_CPF_NMBR;
				
				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertTransportBooking",storedparams));
				
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region Fill DropDown of ALL Employee Name
		public  DataSet fillEmployeeDdl()
		{
			DataSet dsEmployee; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillEmpDDL";
			dsEmployee = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployee;
		}
		#endregion

        #region Fill DropDown of ALL Employee Name who are in pending list
        //cyBuzz Code : Ashish Gupta || 5th november 2012
        public DataSet fillPendingEmployeesList()
        {
            DataSet dsEmployee;
            SqlParameter[] storedParams = new SqlParameter[1];
            storedParams[0] = new SqlParameter("@CommandType", SqlDbType.VarChar);
            storedParams[0].Value = "FillPendingEmpList";
            dsEmployee = SqlHelper.ExecuteDataset(strConn, CommandType.StoredProcedure, "CY_SP_FillPendingEmpList", storedParams);
            return dsEmployee;
            //   CY_SP_FillPendingEmpList
        }
        #endregion

		#region Fill DropDown of Employee Name with Manager Role Only
		public  DataSet fillEmpManagerDDL()
		{
			DataSet dsEmployeeMgr; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillEmpMgrDDL";
			dsEmployeeMgr = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployeeMgr;
		}
		#endregion

		#region Fill DropDown of Employee Desg
		public  DataSet GetEmployeeDesg(ONGCCustumEntity.TransportBooking objcusTransportBooking)
		{
			DataSet dsEmployeeDesg; 
			SqlParameter [] storedParams = new SqlParameter[2];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "SelectDesg";

			storedParams[1]  = new SqlParameter("@O_INDX_NMBR",SqlDbType.VarChar);
			storedParams[1].Value = objcusTransportBooking.o_INDX_NMBR;

			dsEmployeeDesg = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployeeDesg;
		}
		#endregion
		
		#region Fill DropDown of Location
		public  DataSet fillLocationDdl()
		{
			DataSet dsLocation; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillLocationDDL";
			dsLocation = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsLocation;
		}
		#endregion
		
		#region Fill DropDown of Booking Type
		public  DataSet fillBookingDdl()
		{
			DataSet dsBooking; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillBookingDDL";
			dsBooking = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsBooking;
		}
		#endregion

		#region Fill DropDown of Designation
		public  DataSet fillDesignationDdl()
		{
			DataSet dsDesignation; 
			SqlParameter [] storedParams = new SqlParameter[1];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "FillDesignationDDL";
			dsDesignation = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsDesignation;
		}
		#endregion

		#region Get Employee Email IDs.
		public  DataSet GetEmployeeEmail(ONGCCustumEntity.TransportBooking objcusTransportBooking)
		{
			DataSet dsEmployeeEmail; 
			SqlParameter [] storedParams = new SqlParameter[2];
			storedParams[0]  = new SqlParameter("@CommandType",SqlDbType.VarChar);
			storedParams[0].Value = "SelectEmail";

			storedParams[1]  = new SqlParameter("@O_CPF_NMBR",SqlDbType.VarChar);
			storedParams[1].Value = objcusTransportBooking.o_CPF_NMBR;

			dsEmployeeEmail = SqlHelper.ExecuteDataset(strConn,CommandType.StoredProcedure ,"SP_FillDdl",storedParams);
			return dsEmployeeEmail;
		}
		#endregion
	}
}
