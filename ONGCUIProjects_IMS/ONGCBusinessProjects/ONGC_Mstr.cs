using System;
using System.Data;
using System.Data.SqlClient;
using ONGCDataProjects;
using System.Collections;

namespace ONGCBusinessProjects
{
	public class ONGC_Mstr
	{
		public ONGC_Mstr()
		{
		}
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

		#region InsertTransport
		public bool InsertTransport(ONGCCustumEntity.ONGC_Mstr objCusTransport)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertTransport";

				storedparams[1] = new SqlParameter("@O_TRNSP_NM",SqlDbType.VarChar);
				storedparams[1].Value = objCusTransport.o_TRNSP_NM;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertIncident
		public bool InsertIncident(ONGCCustumEntity.ONGC_Mstr objCusIncident)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertIncident";

				storedparams[1] = new SqlParameter("@O_INCDNT_CTGRY",SqlDbType.VarChar);
				storedparams[1].Value = objCusIncident.o_INCDNT_CTGRY;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertLocation
		public bool InsertLocation(ONGCCustumEntity.ONGC_Mstr objCusLocation)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertLocation";

				storedparams[1] = new SqlParameter("@O_LCTN_NM",SqlDbType.VarChar);
				storedparams[1].Value = objCusLocation.o_LCTN_NM;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertComplaintCatgry
		public bool InsertComplaintCatgry(ONGCCustumEntity.ONGC_Mstr objComplaintCatgry)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertComplaintCatgry";

				storedparams[1] = new SqlParameter("@O_CMPLNT_TYP",SqlDbType.VarChar);
				storedparams[1].Value = objComplaintCatgry.o_CMPLNT_TYP;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertSafetyMeasure
		public bool InsertSftyMeasure(ONGCCustumEntity.ONGC_Mstr objSftyMeasure)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertComplaintCatgry";

				storedparams[1] = new SqlParameter("@O_SFTY_MSR",SqlDbType.VarChar);
				storedparams[1].Value = objSftyMeasure.o_SFTY_MSR;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertObservation
		public bool InsertObservation(ONGCCustumEntity.ONGC_Mstr objObservation)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertComplaintCatgry";

				storedparams[1] = new SqlParameter("@O_OBSRVTN_DTL",SqlDbType.VarChar);
				storedparams[1].Value = objObservation.o_OBSRVTN_DTL;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertDesignation
		public bool InsertDesignation(ONGCCustumEntity.ONGC_Mstr objCusDesignation)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertDesignation";

				storedparams[1] = new SqlParameter("@O_DSG_DTL",SqlDbType.VarChar);
				storedparams[1].Value = objCusDesignation.o_DSG_DTL;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertHotNews
		public bool InsertHotNews(ONGCCustumEntity.ONGC_Mstr objComplaintCatgry)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertHotNews";

				storedparams[1] = new SqlParameter("@O_HOT_NEWS",SqlDbType.VarChar);
				storedparams[1].Value = objComplaintCatgry.o_HOT_NEWS;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertPHUNews
		public bool InsertPHUNews(ONGCCustumEntity.ONGC_Mstr objComplaintCatgry)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertPHUNews";

				storedparams[1] = new SqlParameter("@O_GM_NEWS",SqlDbType.VarChar);
				storedparams[1].Value = objComplaintCatgry.o_GM_NEWS;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region InsertUranNews
		public bool InsertUranNews(ONGCCustumEntity.ONGC_Mstr objComplaintCatgry)
		{
			try
			{
				SqlParameter[] storedparams = new SqlParameter[2];
				storedparams[0]=new SqlParameter("@CommandType",SqlDbType.VarChar);
				storedparams[0].Value = "InsertUranNews";

				storedparams[1] = new SqlParameter("@O_BULLETION",SqlDbType.VarChar);
				storedparams[1].Value = objComplaintCatgry.o_BULLETION;

				int	i = Convert.ToInt32(SqlHelper.ExecuteScalar(strConn,CommandType.StoredProcedure,"SP_InsertONGCMaster",storedparams));
				if(i == 0)
				{
					return true;
				}
				else
				{
					return false;
				}			
			}
			catch(SqlException ex)
			{
				if(ex.Number >= 70001 && ex.Number <= 80000)
				{
					return false;
				}
				else
				{
					throw ex;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

	}
}
