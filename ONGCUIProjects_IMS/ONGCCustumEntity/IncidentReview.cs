using System;

namespace ONGCCustumEntity
{
	public class IncidentReview
	{
		public IncidentReview()
		{
		}

		#region Incident Review Fields
			// Incidence Review Detail
			private System.Int32 O_INCIDENT_NMBR;
			private string O_REV_INCDNT_CTGRY;
			private string O_REV_NM;
			private string O_REV_CPF_NMBR;
			private string O_REV_ROOT_CAUSE;
			private string O_REV_RECMND;
			private string O_REV_PRECTN_MEASR;
			private string O_REV_ACTION_BY_NM;
			private string O_REV_ACTION_BY_CPF_NMBR;
			private string O_REV_ACTION_TARGET_DT;
			private string O_REV_RMRKS;
			private string O_INCDNT_DESC;
			private string O_SEVR_TYPE;
			private string O_SEVR_MIN_LOSS;
			private string O_SEVR_MAX_LOSS;
			private string O_ROOT_CS_EMP_NM;
			private string O_ROOT_CS_EMP_NMBR;
			
		#endregion

		#region Incident - Root Cause Analysis
		private string O_RT_CAUSE_ANALYSIS_DESC;
		#endregion

		#region Incident Action Detail Fields
			// Incidence Action Detail Table
			private string O_REV_NMBR;
			private string O_ACTN_NM;
			private string O_ACTN_CPF_NMBR;			
			private string O_ACTN_TAKN;
			private string O_RMRKS;
			private string O_ACPTNC_NM;
			private string O_ACPTNC_CPF_NMBR;			
			private string O_ACTN_TRGT_DT;

		#endregion

		#region Incident Action Executer Fields
		// Incident Action Executer Detail
			private string O_USR_CODE;

		#endregion
		
		#region Incident Action Acceptance 1 Fields
		// Incidence Action Acceptance Detail Table			
			private System.Int32 O_INCDNT_NMBR;
			private System.Int32 O_ACTN_NMBR;
			private string O_SELF_CMNT;			
			private string O_ACTN_REV_REQ;
			private string O_INC_DECSN;
			private string O_ACPTN2_NM;
			private string O_ACPTN2_CPF_NMBR;
			private string O_ACPTN1_NM;
			private string O_ACPTN1_CPF_NMBR;
			private string O_ACPTN1_FLG;

		#endregion

		#region Incident Final Acceptance Fields

			// Incidence Final Acceptance Detail

			private string O_ACPTN_1_NMBR;
			private string O_ACTN_TKN;
			private string O_USR_LGN_NM;
			private string O_USR_CPF_NMBR;
			private string O_ACPTN2_FLG;
		
		#endregion


			// User code
			public string o_USR_CODE
			{
				get{return O_USR_CODE;}
				set{O_USR_CODE=value;}
			}

			#region SetProperty
			public System.Int32 o_INCIDENT_NMBR 
			{
				get{return O_INCIDENT_NMBR;}
				set{O_INCIDENT_NMBR=value;}
			}
			public string o_REV_INCDNT_CTGRY
			{
				get{return O_REV_INCDNT_CTGRY;}
				set{O_REV_INCDNT_CTGRY=value;}
			}
			public string o_REV_NM 
			{
				get{return O_REV_NM;}
				set{O_REV_NM=value;}
			}
				public string o_REV_CPF_NMBR 
				{
					get{return O_REV_CPF_NMBR;}
					set{O_REV_CPF_NMBR=value;}
				}
				public string o_REV_ROOT_CAUSE 
				{
					get{return O_REV_ROOT_CAUSE;}
					set{O_REV_ROOT_CAUSE=value;}
				}
				public string o_REV_RECMND 
				{
					get{return O_REV_RECMND;}
					set{O_REV_RECMND=value;}
				}
				public string o_REV_PRECTN_MEASR 
				{
					get{return O_REV_PRECTN_MEASR;}
					set{O_REV_PRECTN_MEASR=value;}
				}
				public string o_REV_ACTION_BY_NM 
				{
					get{return O_REV_ACTION_BY_NM;}
					set{O_REV_ACTION_BY_NM=value;}
				}
				public string o_REV_ACTION_BY_CPF_NMBR 
				{
					get{return O_REV_ACTION_BY_CPF_NMBR;}
					set{O_REV_ACTION_BY_CPF_NMBR=value;}
				}
				public string o_REV_ACTION_TARGET_DT 
				{
					get{return O_REV_ACTION_TARGET_DT;}
					set{O_REV_ACTION_TARGET_DT=value;}
				}
				public string o_REV_RMRKS 
				{
					get{return O_REV_RMRKS;}
					set{O_REV_RMRKS=value;}
				}
				public string o_INCDNT_DESC 
				{
					get{return O_INCDNT_DESC;}
					set{O_INCDNT_DESC=value;}
				}

			// severity matrix

				public string o_SEVR_TYPE
				{
					get{return O_SEVR_TYPE;}
					set{O_SEVR_TYPE=value;}
				}		

				public string o_SEVR_MIN_LOSS
				{
					get{return O_SEVR_MIN_LOSS;}
					set{O_SEVR_MIN_LOSS=value;}
				}		
				
				public string o_SEVR_MAX_LOSS 
				{
					get{return O_SEVR_MAX_LOSS;}
					set{O_SEVR_MAX_LOSS=value;}
				}		

				public string o_ROOT_CS_EMP_NM 
				{
					get{return O_ROOT_CS_EMP_NM;}
					set{O_ROOT_CS_EMP_NM=value;}
				}
				public string o_ROOT_CS_EMP_NMBR 
				{
					get{return O_ROOT_CS_EMP_NMBR;}
					set{O_ROOT_CS_EMP_NMBR=value;}
				}


		// Insertion for Action Detail
		
		
					public string o_REV_NMBR
					{
						get{return O_REV_NMBR;}
						set{O_REV_NMBR=value;}
					}

					public string o_ACTN_NM
					{
						get{return O_ACTN_NM;}
						set{O_ACTN_NM=value;}
					}

					public string o_ACTN_CPF_NMBR
					{
						get{return O_ACTN_CPF_NMBR;}
						set{O_ACTN_CPF_NMBR=value;}
					}

					public string o_ACTN_TAKN
					{
						get{return O_ACTN_TAKN;}
						set{O_ACTN_TAKN=value;}
					}

					public string o_RMRKS
					{
						get{return O_RMRKS;}
						set{O_RMRKS=value;}
					}

					public string o_ACPTNC_NM
					{
						get{return O_ACPTNC_NM;}
						set{O_ACPTNC_NM=value;}
					}

					public string o_ACPTNC_CPF_NMBR
					{
						get{return O_ACPTNC_CPF_NMBR;}
						set{O_ACPTNC_CPF_NMBR=value;}
					}

					public string o_ACTN_TRGT_DT
					{
						get{return O_ACTN_TRGT_DT;}
						set{O_ACTN_TRGT_DT=value;}
					}

				// ============================================================================
				// Insertion for Incidence Acceptance Detail
						
					public System.Int32 o_INCDNT_NMBR
					{
						get{return O_INCDNT_NMBR;}
						set{O_INCDNT_NMBR=value;}
					}

					public System.Int32 o_ACTN_NMBR
					{
						get{return O_ACTN_NMBR;}
						set{O_ACTN_NMBR=value;}
					}
		
					public string o_SELF_CMNT
					{
						get{return O_SELF_CMNT;}
						set{O_SELF_CMNT=value;}
					}

					public string o_ACTN_REV_REQ
					{
						get{return O_ACTN_REV_REQ;}
						set{O_ACTN_REV_REQ=value;}
					}

					public string o_INC_DECSN
					{
						get{return O_INC_DECSN;}
						set{O_INC_DECSN=value;}
					}
		
					public string o_ACPTN2_NM
					{
						get{return O_ACPTN2_NM;}
						set{O_ACPTN2_NM=value;}
					}

					public string o_ACPTN2_CPF_NMBR
					{
						get{return O_ACPTN2_CPF_NMBR;}
						set{O_ACPTN2_CPF_NMBR=value;}
					}
		
					public string o_ACPTN1_NM
					{
						get{return O_ACPTN1_NM;}
						set{O_ACPTN1_NM=value;}
					}
		
					public string o_ACPTN1_CPF_NMBR
					{
						get{return O_ACPTN1_CPF_NMBR;}
						set{O_ACPTN1_CPF_NMBR=value;}
					}

					public string o_ACPTN1_FLG
					{
						get{return O_ACPTN1_FLG;}
						set{O_ACPTN1_FLG=value;}
					}

		// Insertion for Incidence Final Acceptance Detail

		public string o_ACPTN_1_NMBR
		{
			get{return O_ACPTN_1_NMBR;}
			set{O_ACPTN_1_NMBR=value;}
		}

		public string o_ACTN_TKN
		{
			get{return O_ACTN_TKN;}
			set{O_ACTN_TKN=value;}
		}

		public string o_USR_LGN_NM
		{
			get{return O_USR_LGN_NM;}
			set{O_USR_LGN_NM=value;}
		}

		public string o_USR_CPF_NMBR
		{
			get{return O_USR_CPF_NMBR;}
			set{O_USR_CPF_NMBR=value;}
		}

		public string o_ACPTN2_FLG
		{
			get{return O_ACPTN2_FLG;}
			set{O_ACPTN2_FLG=value;}
		}

		// Root Cause Analysis
		public string o_RT_CAUSE_ANALYSIS_DESC
		{
			get{return O_RT_CAUSE_ANALYSIS_DESC;}
			set{O_RT_CAUSE_ANALYSIS_DESC=value;}
		}

		//==========================================================================================

		#endregion
	}
}
