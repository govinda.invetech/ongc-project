using System;

namespace ONGCCustumEntity
{
	public class IncidentManagement
	{
		public IncidentManagement()
		{
		}
			private string O_ENG_NM;
			private string O_ENG_DSG;
			private string O_CPF_NMBR;
			private string O_INCDNT_DESCRPTN;
			private string O_INCDNT_DT;
			private string O_INCDNT_TIME;
			private string O_INCDNT_LCTN;
			//private string O_INCDNT_CATGRY;
			//private string O_TYPE_OF_INJRY;
			private string O_RUSH_EMRGNCY;
			private string O_EMP_NM;
			private string O_RMRKS;

			public string o_ENG_NM
			{
				get
				{
					return O_ENG_NM;
				}
				set
				{
					O_ENG_NM = value;
				}
			}
			public string o_ENG_DSG
			{
				get
				{
					return O_ENG_DSG;
				}
				set
				{
					O_ENG_DSG = value;
				}
			}
			public string o_CPF_NMBR
			{
				get
				{
					return O_CPF_NMBR;
				}
				set
				{
					O_CPF_NMBR = value;
				}
			}
			public string o_INCDNT_DESCRPTN
			{
				get
				{
					return O_INCDNT_DESCRPTN;
				}
				set
				{
					O_INCDNT_DESCRPTN = value;
				}
			}
			public string o_INCDNT_DT
			{
				get
				{
					return O_INCDNT_DT;
				}
				set
				{
					O_INCDNT_DT = value;
				}
			}
			public string o_INCDNT_TIME
			{
				get
				{
					return O_INCDNT_TIME;
				}
				set
				{
					O_INCDNT_TIME = value;
				}
			}
			public string o_INCDNT_LCTN
			{
				get
				{
					return O_INCDNT_LCTN;
				}
				set
				{
					O_INCDNT_LCTN = value;
				}
			}
//			public string o_INCDNT_CATGRY
//			{
//				get
//				{
//					return O_INCDNT_CATGRY;
//				}
//				set
//				{
//					O_INCDNT_CATGRY = value;
//				}
//			}
//			public string o_TYPE_OF_INJRY
//			{
//				get
//				{
//					return O_TYPE_OF_INJRY;
//				}
//				set
//				{
//					O_TYPE_OF_INJRY = value;
//				}
//			}
			public string o_RUSH_EMRGNCY
			{
				get
				{
					return O_RUSH_EMRGNCY;
				}
				set
				{
					O_RUSH_EMRGNCY = value;
				}
			}
			public string o_EMP_NM
			{
				get
				{
					return O_EMP_NM;
				}
				set
				{
					O_EMP_NM = value;
				}
			}
			public string o_RMRKS
			{
				get
				{
					return O_RMRKS;
				}
				set
				{
					O_RMRKS = value;
				}
			}

	}
}
