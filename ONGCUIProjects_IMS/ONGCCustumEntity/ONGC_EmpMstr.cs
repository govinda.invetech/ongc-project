using System;

namespace ONGCCustumEntity
{
	public class ONGC_EmpMstr
	{
		public ONGC_EmpMstr()
		{
		}
			private string O_CPF_NMBR;
			private string O_EMP_NM;
			private string O_EMP_HM_ADDR1;
			private string O_EMP_HM_ADDR2;
			private string O_EMP_HM_CITY;
			private string O_EMP_HM_STATE;
			private string O_EMP_HM_PIN_CD;
			private string O_EMP_HM_PHN;
			private string O_EMP_MBL_NMBR;
			private string O_EMP_GNDR;
			private string O_EMP_DESGNTN;
			private string O_EMP_DOB;
			private string O_EMP_SUP_NM;
			private string O_EMP_DEPT_NM;
			private string O_EMP_WRK_LCTN;
			private string O_EMP_BLD_GRP;
			private string O_EMAIL_ID1;
			private string O_EMAIL_ID2;
			private string O_EMAIL_ID3;
			private string O_USER_CODE;
			private string O_BIT_CD;
			private string O_UPDATE_BIT;
			private string O_LEVEL;
			private string O_REP;
			private string O_REV;
			private string O_MGR;
			private string O_ACP_1;
			private string O_ACP_2;

		
	
		public string o_USER_CODE
		{
			get
			{
				return O_USER_CODE;
			}
			set
			{
				O_USER_CODE = value;
			}
		}


		public string o_BIT_CD
		{
			get
			{
				return O_BIT_CD;
			}
			set
			{
				O_BIT_CD = value;
			}
		}


		public string o_UPDATE_BIT
		{
			get
			{
				return O_UPDATE_BIT;
			}
			set
			{
				O_UPDATE_BIT = value;
			}
		}


		public string o_LEVEL
		{
			get
			{
				return O_LEVEL;
			}
			set
			{
				O_LEVEL = value;
			}
		}


		public string o_REP
		{
			get
			{
				return O_REP;
			}
			set
			{
				O_REP = value;
			}
		}

		public string o_REV
		{
			get
			{
				return O_REV;
			}
			set
			{
				O_REV = value;
			}
		}

		public string o_MGR
		{
			get
			{
				return O_MGR;
			}
			set
			{
				O_MGR = value;
			}
		}

		public string o_ACP_1
		{
			get
			{
				return O_ACP_1;
			}
			set
			{
				O_ACP_1 = value;
			}
		}

		public string o_ACP_2
		{
			get
			{
				return O_ACP_2;
			}
			set
			{
				O_ACP_2 = value;
			}
		}



			public string o_CPF_NMBR
			{
				get
				{
					return O_CPF_NMBR;
				}
				set
				{
					O_CPF_NMBR = value;
				}
			}
			public string o_EMP_NM
			{
				get
				{
					return O_EMP_NM;
				}
				set
				{
					O_EMP_NM = value;
				}
			}
			public string o_EMP_HM_ADDR1
			{
				get
				{
					return O_EMP_HM_ADDR1;
				}
				set
				{
					O_EMP_HM_ADDR1 = value;
				}
			}
			public string o_EMP_HM_ADDR2
			{
				get
				{
					return O_EMP_HM_ADDR2;
				}
				set
				{
					O_EMP_HM_ADDR2 = value;
				}
			}
			public string o_EMP_HM_CITY
			{
				get
				{
					return O_EMP_HM_CITY;
				}
				set
				{
					O_EMP_HM_CITY = value;
				}
			}
			public string o_EMP_HM_STATE
			{
				get
				{
					return O_EMP_HM_STATE;
				}
				set
				{
					O_EMP_HM_STATE = value;
				}
			}
			public string o_EMP_HM_PIN_CD
			{
				get
				{
					return O_EMP_HM_PIN_CD;
				}
				set
				{
					O_EMP_HM_PIN_CD = value;
				}
			}
			public string o_EMP_HM_PHN
			{
				get
				{
					return O_EMP_HM_PHN;
				}
				set
				{
					O_EMP_HM_PHN = value;
				}
			}
			public string o_EMP_MBL_NMBR
			{
				get
				{
					return O_EMP_MBL_NMBR;
				}
				set
				{
					O_EMP_MBL_NMBR = value;
				}
			}
			public string o_EMP_GNDR
			{
				get
				{
					return O_EMP_GNDR;
				}
				set
				{
					O_EMP_GNDR = value;
				}
			}
			public string o_EMP_DESGNTN
			{
				get
				{
					return O_EMP_DESGNTN;
				}
				set
				{
					O_EMP_DESGNTN = value;
				}
			}
			public string o_EMP_DOB
			{
				get
				{
					return O_EMP_DOB;
				}
				set
				{
					O_EMP_DOB = value;
				}
			}
			public string o_EMP_SUP_NM
			{
				get
				{
					return O_EMP_SUP_NM;
				}
				set
				{
					O_EMP_SUP_NM = value;
				}
			}
			public string o_EMP_DEPT_NM
			{
				get
				{
					return O_EMP_DEPT_NM;
				}
				set
				{
					O_EMP_DEPT_NM = value;
				}
			}
			public string o_EMP_WRK_LCTN
			{
				get
				{
					return O_EMP_WRK_LCTN;
				}
				set
				{
					O_EMP_WRK_LCTN = value;
				}
			}
			public string o_EMP_BLD_GRP
			{
				get
				{
					return O_EMP_BLD_GRP;
				}
				set
				{
					O_EMP_BLD_GRP = value;
				}
			}

			// Email 1
			public string o_EMAIL_ID1
			{
				get
				{
					return O_EMAIL_ID1;
				}
				set
				{
					O_EMAIL_ID1 = value;
				}
			}

		// Email 2
		public string o_EMAIL_ID2
		{
			get
			{
				return O_EMAIL_ID2;
			}
			set
			{
				O_EMAIL_ID2 = value;
			}
		}

		// Email 3
		public string o_EMAIL_ID3
		{
			get
			{
				return O_EMAIL_ID3;
			}
			set
			{
				O_EMAIL_ID3 = value;
			}
		}

	}
}
