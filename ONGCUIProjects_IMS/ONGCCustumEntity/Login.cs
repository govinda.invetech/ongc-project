using System;

namespace ONGCCustumEntity
{
	public class Login
	{
		public Login()
		{
		}
		#region Declare Variables
		private string User_Id;
		private string Password;
		private string Active_Flag; 
		#endregion

		#region SetProperty
		public string user_id 
		{
			get{return User_Id;}
			set{User_Id=value;}
		}
		public string password
		{
			get{return Password;}
			set{Password=value;}
		}
		public string active_flag 
		{
			get{return Active_Flag;}
			set{Active_Flag=value;}
		}
		#endregion
	}
}
