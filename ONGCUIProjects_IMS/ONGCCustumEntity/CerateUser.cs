using System;

namespace ONGCCustumEntity
{
	public class CerateUser
	{
		public CerateUser()
		{
		}
		#region Variable Declaretion 
		
		private string E_USER_CODE;
		private string E_USER_DSCRPTN;
		private string E_USER_PSWRD;
		private string E_USER_NM;
		private string E_ROLE_ID;
		
		#endregion
		
		#region SetProperty 
	
		public string UserCode
		{
			get
			{
				return E_USER_CODE;
			}
			set
			{
				E_USER_CODE=value;
			}
		}

		public string UserDescription
		{
			get
			{
				return E_USER_DSCRPTN;
			}
			set
			{
				E_USER_DSCRPTN=value;
			}
		}
		
		public string UserPassword
		{
			get
			{
				return E_USER_PSWRD;
			}
			set
			{
				E_USER_PSWRD=value;
			}
		}

		public string UserName
		{
			get
			{
				return E_USER_NM;
			}
			set
			{
				E_USER_NM=value;
			}
		}

		public string RoleID
		{
			get
			{
				return E_ROLE_ID;
			}
			set
			{
				E_ROLE_ID=value;
			}
		}

		#endregion
	}
}
