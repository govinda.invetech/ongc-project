using System;

namespace ONGCCustumEntity
{
	public class RoleCreation
	{
		public RoleCreation()
		{
		}
		#region Declare Variable
		private string P_ROLE_ID;
		private string P_ROLE_DSCRPTN;
		private string P_CMPNT_CODE;
		private string P_ROLE_MDLE;

		#endregion

		#region set Property 
		public string p_ROLE_ID
		{
			get
			{
				return P_ROLE_ID;
			}
			set
			{
				P_ROLE_ID=value;
			}
		}
		public string p_ROLE_MDLE
		{
			get
			{
				return P_ROLE_MDLE;
			}
			set
			{
				P_ROLE_MDLE=value;
			}
		}
		public string p_ROLE_DSCRPTN
		{
			get
			{
				return P_ROLE_DSCRPTN;
			}
			set
			{
				P_ROLE_DSCRPTN=value;
			}
		}
		public string p_CMPNT_CODE
		{
			get
			{
				return P_CMPNT_CODE;
			}
			set
			{
				P_CMPNT_CODE=value;
			}
		}
		#endregion
	}
}
