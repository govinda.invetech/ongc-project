using System;

namespace ONGCCustumEntity
{
	public class ONGC_Mstr
	{
		public ONGC_Mstr()
		{
		}
		private string O_TRNSP_NM;
		private string O_INCDNT_CTGRY;
		private string O_LCTN_NM;
		private string O_CMPLNT_TYP;
		private string O_SFTY_MSR;
		private string O_OBSRVTN_DTL;
		private string O_DSG_DTL;
		private string O_HOT_NEWS;
		private string O_GM_NEWS;
		private string O_BULLETION;

		public string o_BULLETION
		{
			get
			{
				return O_BULLETION;
			}
			set
			{
				O_BULLETION = value;
			}
		}

		public string o_GM_NEWS
		{
			get
			{
				return O_GM_NEWS;
			}
			set
			{
				O_GM_NEWS = value;
			}
		}

		public string o_HOT_NEWS
		{
			get
			{
				return O_HOT_NEWS;
			}
			set
			{
				O_HOT_NEWS = value;
			}
		}

		public string o_TRNSP_NM
		{
			get
			{
				return O_TRNSP_NM;
			}
			set
			{
				O_TRNSP_NM = value;
			}
		}
		public string o_INCDNT_CTGRY
		{
			get
			{
				return O_INCDNT_CTGRY;
			}
			set
			{
				O_INCDNT_CTGRY = value;
			}
		}
		public string o_LCTN_NM
		{
			get
			{
				return O_LCTN_NM;
			}
			set
			{
				O_LCTN_NM = value;
			}
		}
		public string o_CMPLNT_TYP
		{
			get
			{
				return O_CMPLNT_TYP;
			}
			set
			{
				O_CMPLNT_TYP = value;
			}
		}
		public string o_SFTY_MSR
		{
			get
			{
				return O_SFTY_MSR;
			}
			set
			{
				O_SFTY_MSR = value;
			}
		}
		public string o_OBSRVTN_DTL
		{
			get
			{
				return O_OBSRVTN_DTL;
			}
			set
			{
				O_OBSRVTN_DTL = value;
			}
		}

		public string o_DSG_DTL
		{
			get
			{
				return O_DSG_DTL;
			}
			set
			{
				O_DSG_DTL = value;
			}
		}

	}
}
