using System;

namespace ONGCCustumEntity
{
	public class GatePassReq
	{
		public GatePassReq()
		{
		}

		#region Private variables

			private string O_APLCNT_NM;
			private string O_APLCNT_CPF_NMBR;
			private string O_APLCNT_DESG;
			private string O_APLCNT_LCTN;
			private string O_APLCNT_EXT_NMBR;
			private string O_REQSTN_TYPE;
			private string O_ENTRY_DT;
			private string O_ENTRY_TM;
			private string O_EXIT_DT;
			private string O_EXIT_TM;
			private string O_ENTRY_AREA;
			private string O_MTNG_OFF_NM;
			private string O_MTNG_OFF_DESG;
			private string O_MTNG_OFF_LCTN;
			private string O_VISTR_NM;
			private string O_VISTR_ORGNZTN;
			private string O_VISTR_DESG;
			private string O_VISTR_AGE;
			private string O_VISTR_GNDR;
			private string O_VISTR_IDENT_MARK;
			private string O_VISTR_BLD_GRP;
			private string O_PRPS_OF_VISIT;
			private string O_RMRKS;
			private string O_ACP1_NM;
			private string O_ACP1_CPF_NMBR;

		#endregion

		#region Public variables

		public string o_APLCNT_NM
		{
			get
			{
				return O_APLCNT_NM;
			}
			set
			{
				O_APLCNT_NM = value;
			}
		}
		public string o_APLCNT_CPF_NMBR
		{
			get
			{
				return O_APLCNT_CPF_NMBR;
			}
			set
			{
				O_APLCNT_CPF_NMBR = value;
			}
		}
		public string o_APLCNT_DESG
		{
			get
			{
				return O_APLCNT_DESG;
			}
			set
			{
				O_APLCNT_DESG = value;
			}
		}
		public string o_APLCNT_LCTN
		{
			get
			{
				return O_APLCNT_LCTN;
			}
			set
			{
				O_APLCNT_LCTN = value;
			}
		}
		public string o_APLCNT_EXT_NMBR
		{
			get
			{
				return O_APLCNT_EXT_NMBR;
			}
			set
			{
				O_APLCNT_EXT_NMBR = value;
			}
		}
		public string o_REQSTN_TYPE
		{
			get
			{
				return O_REQSTN_TYPE;
			}
			set
			{
				O_REQSTN_TYPE = value;
			}
		}
		public string o_ENTRY_DT
		{
			get
			{
				return O_ENTRY_DT;
			}
			set
			{
				O_ENTRY_DT = value;
			}
		}
		public string o_ENTRY_TM
		{
			get
			{
				return O_ENTRY_TM;
			}
			set
			{
				O_ENTRY_TM = value;
			}
		}
		public string o_EXIT_DT
		{
			get
			{
				return O_EXIT_DT;
			}
			set
			{
				O_EXIT_DT = value;
			}
		}
		public string o_EXIT_TM
		{
			get
			{
				return O_EXIT_TM;
			}
			set
			{
				O_EXIT_TM = value;
			}
		}
		public string o_ENTRY_AREA
		{
			get
			{
				return O_ENTRY_AREA;
			}
			set
			{
				O_ENTRY_AREA = value;
			}
		}
		public string o_MTNG_OFF_NM
		{
			get
			{
				return O_MTNG_OFF_NM;
			}
			set
			{
				O_MTNG_OFF_NM = value;
			}
		}
		public string o_MTNG_OFF_DESG
		{
			get
			{
				return O_MTNG_OFF_DESG;
			}
			set
			{
				O_MTNG_OFF_DESG = value;
			}
		}
		public string o_MTNG_OFF_LCTN
		{
			get
			{
				return O_MTNG_OFF_LCTN;
			}
			set
			{
				O_MTNG_OFF_LCTN = value;
			}
		}
		public string o_VISTR_NM
		{
			get
			{
				return O_VISTR_NM;
			}
			set
			{
				O_VISTR_NM = value;
			}
		}
		public string o_VISTR_ORGNZTN
		{
			get
			{
				return O_VISTR_ORGNZTN;
			}
			set
			{
				O_VISTR_ORGNZTN = value;
			}
		}
		public string o_VISTR_DESG
		{
			get
			{
				return O_VISTR_DESG;
			}
			set
			{
				O_VISTR_DESG = value;
			}
		}
		public string o_VISTR_AGE
		{
			get
			{
				return O_VISTR_AGE;
			}
			set
			{
				O_VISTR_AGE = value;
			}
		}
		public string o_VISTR_GNDR
		{
			get
			{
				return O_VISTR_GNDR;
			}
			set
			{
				O_VISTR_GNDR = value;
			}
		}
		public string o_VISTR_IDENT_MARK
		{
			get
			{
				return O_VISTR_IDENT_MARK;
			}
			set
			{
				O_VISTR_IDENT_MARK = value;
			}
		}
		public string o_VISTR_BLD_GRP
		{
			get
			{
				return O_VISTR_BLD_GRP;
			}
			set
			{
				O_VISTR_BLD_GRP = value;
			}
		}
		public string o_PRPS_OF_VISIT
		{
			get
			{
				return O_PRPS_OF_VISIT;
			}
			set
			{
				O_PRPS_OF_VISIT = value;
			}
		}
		public string o_RMRKS
		{
			get
			{
				return O_RMRKS;
			}
			set
			{
				O_RMRKS = value;
			}
		}
		
		public string o_ACP1_NM
		{
			get
			{
				return O_ACP1_NM;
			}
			set
			{
				O_ACP1_NM = value;
			}
		}

		public string o_ACP1_CPF_NMBR
		{
			get
			{
				return O_ACP1_CPF_NMBR;
			}
			set
			{
				O_ACP1_CPF_NMBR = value;
			}
		}

		#endregion

	}
}
