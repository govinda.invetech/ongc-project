using System;

namespace ONGCCustumEntity
{
	public class Complaint
	{
		public Complaint()
		{
		}
			private string O_APLCTN_NM;
			private string O_APLCTN_DSG;
			private string O_APLCTN_CPF_NMBR;
			private string O_APLCTN_LCTN;
			private string O_CMPLNT_TYPE;
			private string O_CMPLNT_DTL;
			private string O_CMPLNT_OFF_NM;
			private string O_CMPLNT_OFF_DESG;
			private string O_CMPLNT_OFF_CPF_NMBR;
			public string o_APLCTN_NM
			{
				get
				{
					return O_APLCTN_NM;
				}
				set
				{
					O_APLCTN_NM = value;
				}
			}
			public string o_APLCTN_DSG
			{
				get
				{
					return O_APLCTN_DSG;
				}
				set
				{
					O_APLCTN_DSG = value;
				}
			}
			public string o_APLCTN_CPF_NMBR
			{
				get
				{
					return O_APLCTN_CPF_NMBR;
				}
				set
				{
					O_APLCTN_CPF_NMBR = value;
				}
			}
			public string o_APLCTN_LCTN
			{
				get
				{
					return O_APLCTN_LCTN;
				}
				set
				{
					O_APLCTN_LCTN = value;
				}
			}
			public string o_CMPLNT_TYPE
			{
				get
				{
					return O_CMPLNT_TYPE;
				}
				set
				{
					O_CMPLNT_TYPE = value;
				}
			}
			public string o_CMPLNT_DTL
			{
				get
				{
					return O_CMPLNT_DTL;
				}
				set
				{
					O_CMPLNT_DTL = value;
				}
			}
			public string o_CMPLNT_OFF_NM
			{
				get
				{
					return O_CMPLNT_OFF_NM;
				}
				set
				{
					O_CMPLNT_OFF_NM = value;
				}
			}
			public string o_CMPLNT_OFF_DESG
			{
				get
				{
					return O_CMPLNT_OFF_DESG;
				}
				set
				{
					O_CMPLNT_OFF_DESG = value;
				}
			}
			public string o_CMPLNT_OFF_CPF_NMBR
			{
				get
				{
					return O_CMPLNT_OFF_CPF_NMBR;
				}
				set
				{
					O_CMPLNT_OFF_CPF_NMBR = value;
				}
			}
	}
}
