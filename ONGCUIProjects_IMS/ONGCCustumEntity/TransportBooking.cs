using System;

namespace ONGCCustumEntity
{
	public class TransportBooking
	{
		public TransportBooking()
		{
		}

		#region Variable Declaretion
		private string O_APLCNT_NM;
		private string O_APLCNT_CPF_NMBR;
		private string O_APLCNT_DESG;
		private string O_APLCNT_LCTN;
		private string O_APLCNT_EXT_NMBR;
		private string O_BKNG_START_DT_TM;
		private string O_BKNG_END_DT_TM;
		private string O_BKNG_TYP;
		private string O_RPRTNG_TM;
		private string O_RPRT_TO_NM;
		private string O_RPRT_TO_ADDR;
		private string O_RPRT_TO_CONT_NMBR1;
		private string O_RPRT_TO_CONT_NMBR2;
		private string O_NO_OF_PERSONS;
		private string O_BKNG_PRPS;
		private string O_RMRKS;
		private string O_BKNG_APRVL_OFF_NM;
		private string O_BKNG_APRVL_OFF_DESG;
		private string O_BKNG_APRVL_OFF_CPF_NMBR;
		private string O_CPF_NMBR;
		private int O_INDX_NMBR;
		#endregion

		#region SetProperty
		public string o_APLCNT_NM
		{
			get
			{
				return O_APLCNT_NM;
			}
			set
			{
				O_APLCNT_NM=value;
			}
					
		}
		public string o_APLCNT_CPF_NMBR
		{
			get
			{
				return O_APLCNT_CPF_NMBR;
			}
			set
			{
				O_APLCNT_CPF_NMBR=value;
			}
					
		}
		public string o_APLCNT_DESG
		{
			get
			{
				return O_APLCNT_DESG;
			}
			set
			{
				O_APLCNT_DESG=value;
			}
					
		}
		public string o_APLCNT_LCTN
		{
			get
			{
				return O_APLCNT_LCTN;
			}
			set
			{
				O_APLCNT_LCTN=value;
			}
					
		}
		public string o_APLCNT_EXT_NMBR
		{
			get
			{
				return O_APLCNT_EXT_NMBR;
			}
			set
			{
				O_APLCNT_EXT_NMBR=value;
			}
					
		}
		public string o_BKNG_START_DT_TM
		{
			get
			{
				return O_BKNG_START_DT_TM;
			}
			set
			{
				O_BKNG_START_DT_TM=value;
			}
					
		}
		public string o_BKNG_END_DT_TM
		{
			get
			{
				return O_BKNG_END_DT_TM;
			}
			set
			{
				O_BKNG_END_DT_TM=value;
			}
					
		}
		public string o_BKNG_TYP
		{
			get
			{
				return O_BKNG_TYP;
			}
			set
			{
				O_BKNG_TYP=value;
			}
					
		}
		public string o_RPRTNG_TM
		{
			get
			{
				return O_RPRTNG_TM;
			}
			set
			{
				O_RPRTNG_TM=value;
			}
					
		}
		public string o_RPRT_TO_NM
		{
			get
			{
				return O_RPRT_TO_NM;
			}
			set
			{
				O_RPRT_TO_NM=value;
			}
					
		}
		public string o_RPRT_TO_ADDR
		{
			get
			{
				return O_RPRT_TO_ADDR;
			}
			set
			{
				O_RPRT_TO_ADDR=value;
			}
					
		}
		public string o_RPRT_TO_CONT_NMBR1
		{
			get
			{
				return O_RPRT_TO_CONT_NMBR1;
			}
			set
			{
				O_RPRT_TO_CONT_NMBR1=value;
			}
					
		}
		
		public string o_RPRT_TO_CONT_NMBR2
		{
			get
			{
				return O_RPRT_TO_CONT_NMBR2;
			}
			set
			{
				O_RPRT_TO_CONT_NMBR2=value;
			}
							
		}
		public string o_NO_OF_PERSONS
		{
			get
			{
				return O_NO_OF_PERSONS;
			}
			set
			{
				O_NO_OF_PERSONS=value;
			}
					
		}
		public string o_BKNG_PRPS
		{
			get
			{
				return O_BKNG_PRPS;
			}
			set
			{
				O_BKNG_PRPS=value;
			}
					
		}
		public string o_RMRKS
		{
			get
			{
				return O_RMRKS;
			}
			set
			{
				O_RMRKS=value;
			}
					
		}
		public string o_BKNG_APRVL_OFF_NM
		{
			get
			{
				return O_BKNG_APRVL_OFF_NM;
			}
			set
			{
				O_BKNG_APRVL_OFF_NM=value;
			}
					
		}
		public string o_BKNG_APRVL_OFF_DESG
		{
			get
			{
				return O_BKNG_APRVL_OFF_DESG;
			}
			set
			{
				O_BKNG_APRVL_OFF_DESG=value;
			}
					
		}
		public string o_BKNG_APRVL_OFF_CPF_NMBR
		{
			get
			{
				return O_BKNG_APRVL_OFF_CPF_NMBR;
			}
			set
			{
				O_BKNG_APRVL_OFF_CPF_NMBR=value;
			}
		}
		public string o_CPF_NMBR
		{
			get
			{
				return O_CPF_NMBR;
			}
			set
			{
				O_CPF_NMBR=value;
			}
		}
		public int o_INDX_NMBR
		{
			get
			{
				return O_INDX_NMBR;
			}
			set
			{
				O_INDX_NMBR=value;
			}
		}
	
		#endregion

	}
}
