<%@ Page Language="c#" CodeBehind="VisitorPass_Approval1.aspx.cs" AutoEventWireup="True"
    Inherits="ONGCUIProjects.Pass_Approval1" %>

<!DOCTYPE html>
<html>
<head>
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/script.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $(document).ajaxStop($.unblockUI);
            $(".button").hide();
            $("#request_detail_div").hide();
            var ddl_type = $("#ddlGatePassType").val();



            if (ddl_type == "Visitor Gate Pass") {
                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                $.ajax({
                    type: "GET",
                    url: "services/gettotalvisitorgatepassrequest.aspx",
                    data: "",
                    success: function (msg) {
                        //alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $("#request_detail_div").html(values[4]);
                            $("#request_detail_div").slideToggle();
                            $("#theory_text").attr('value', values[3]);
                            $("#theory_text").text("Total " + values[3] + " Request");
                            $('.cmdViewApprove').fancybox();
                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {

                            $.gritter.add({
                                title: "Notification",
                                image: "images/cross_big.png",
                                text: values[2]
                            }); $("#request_detail_div").slideToggle();
                            $("#theory_text").text("No Request Found");
                        } else if (values[0] == "FALSE" && values[1] == "ERR") {

                            alert(values[2]);
                        }

                    }
                });
            }

            //view reuistion
            $("#approve_request").live('click', function () {
                var serial = $(this).attr('serial');
                var group_no = $(this).attr('group_no');
                var total_visitor = $(this).parent('.my_complete_div').attr('total_visitor');

                var my_index_list = "";
                var my_count = 0;
                $("." + serial + "single_selected").each(function () {

                    if ($(this).attr("checked")) {
                        my_index_list = my_index_list + $(this).attr('index_no') + "`";
                        my_count = my_count + 1;
                    }
                });
                if (my_index_list == "") {

                    $.gritter.add({
                        title: "Notification",

                        text: "Please Select Atleast one Visitor From Upper"
                    });
                    return false;
                }



                $.blockUI({ message: '' });
                $.ajax({
                    type: "GET",
                    url: "services/approvevisitorgatepassrequest.aspx",
                    data: "cy_group=" + group_no + "&index_no=" + my_index_list + "&type=APPROVE",
                    success: function (msg) {
                        //alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {

                            $.gritter.add({
                                title: "Notification",
                                image: "images/tick.png",
                                text: values[2]
                            });
                            alert(values[2]);
                            window.location = self.location;


                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            $.gritter.add({
                                title: "Notification",
                                text: values[2]
                            });

                        } else if (values[0] == "FALSE" && values[1] == "ERR") {

                        }

                    }
                });
            });
            //reject_request
            $("#reject_request").live('click', function () {
                var serial = $(this).attr('serial');
                var group_no = $(this).attr('group_no');
                var total_visitor = $(this).parent('.my_complete_div').attr('total_visitor');

                var my_index_list = "";
                var my_count = 0;
                $("." + serial + "single_selected").each(function () {

                    if ($(this).attr("checked")) {
                        my_index_list = my_index_list + $(this).attr('index_no') + "`";
                        my_count = my_count + 1;
                    }
                });
                if (my_index_list == "") {

                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Atleast one Visitor From Upper"
                    });
                    return false;
                }



                $.blockUI({ message: '' });
                $.ajax({
                    type: "GET",
                    url: "services/approvevisitorgatepassrequest.aspx",
                    data: "cy_group=" + group_no + "&index_no=" + my_index_list + "&type=REJECT",
                    success: function (msg) {
                        //alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {

                            $.gritter.add({
                                title: "Notification",
                                image: "images/tick.png",
                                text: values[2]
                            });
                            alert(values[2]);
                            window.location = self.location;


                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            $.gritter.add({
                                title: "Notification",
                                text: values[2]
                            });

                        } else if (values[0] == "FALSE" && values[1] == "ERR") {
                            $.gritter.add({
                                title: "Notification",
                                image: "images/tick.png",
                                text: values[2]
                            });
                        }

                    }
                });
            });
            // select all
            $(".select_all").live('click', function () {
                var my_serial = $(this).attr('serial');
                if ($(this).attr('checked')) {
                    $("." + my_serial + "single_selected").attr('checked', 'checked');

                } else {

                    $("." + my_serial + "single_selected").removeAttr('checked');

                }
            });
        });
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server" class="MainInterface-iframe">
        <div class="div-fullwidth iframeMainDiv">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;">Visitor Gate Pass Approval</h1>
                <h1 id="theory_text" class="heading-content-right" style="color: Red;"></h1>
            </div>
            <div class="div-pagebottom">
            </div>
            <h1 class="gatepass-content" style="margin-top: 2px;">Select Gate Pass Type</h1>
            <asp:DropDownList ID="ddlGatePassType" runat="server">
                <asp:ListItem Value="select">-Select The Type Of Gate Pass-</asp:ListItem>
                <asp:ListItem Selected="True">Visitor Gate Pass</asp:ListItem>
                <asp:ListItem>Non Employee Duty Pass</asp:ListItem>
            </asp:DropDownList>
            <p id="P1" class="gatepass-content" style="font-size: 12px; font-family: Arial; margin-top: 10px; display: none">
                Any theory here and if it's not used then hide it.
            </p>
            <!-- Strip Container Starts ------------------------------------------------------------------------------------------->
            <div class="div-fullwidth" id="request_detail_div" style="margin-top: 10px;">
                <!-- Visitor Gate Pass Strip Starts ----------------------------------------------------------------------------------->
                <!-- Visitor Gate Pass Strip Ends ------------------------------------------------------------------------------------->
            </div>
            <!-- Strip Container Ends --------------------------------------------------------------------------------------------->
        </div>
    </form>
</body>
</html>
