﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace ONGCUIProjects
{
    public partial class ImageGallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

     
        [System.Web.Services.WebMethod]
        public static string GetImagesFolder(string Type)
        {
            try
            {
                string JsonSting = "{\"folder\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~/imgs");
                string[] filPaths = Directory.GetDirectories(sOutputPath);
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath + "\\", "");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"folderName\":\"" + filPaths[i] + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetImagesFromFolder(string Folder)
        {
            try
            {
                string JsonSting = "{\"photo\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~/imgs/" + Folder + "");
                string[] filPaths = Directory.GetFiles(sOutputPath);
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath, "\\imgs\\" + Folder + "");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"baseUrl\":\"" + filPaths[i] + "\",\"title\":\"" + filPaths[i].Replace(".jpg", "").Replace(".png", "") + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetDocFromFolder(string Type)
        {
            try
            {
                string JsonSting = "{\"photo\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~");
                string[] filPaths = Directory.GetFiles(sOutputPath + "docs");
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath, "\\");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"baseUrl\":\"" + filPaths[i] + "\",\"title\":\"" + filPaths[i].Replace("/docs/", "").Replace(".jpg", "").Replace(".png", "") + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }

        }



    }
}