using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace ONGCUIProjects
{
	public partial class Gate_Pass_Confirmation : System.Web.UI.Page
	{
		#region Variables

		string EString;
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		#endregion

		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Session["Login_Name"]="222222";
			if(!IsPostBack)
			{
				BindGrid();
				BtnPassConfirmation.Enabled=false;
				//BtnApprove.Enabled=false;
			}
		}

		#endregion

		#region Get Employee Name 
		private void GetEmployee()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select O_EMP_NM,O_EMP_DESGNTN from O_EMP_MSTR where O_CPF_NMBR='"+Session["Login_Name"].ToString()+"'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				if(sqlDataReader.Read())
				{
					ViewState["Abhishek"]=sqlDataReader["O_EMP_NM"].ToString();
				
				}
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.DGrd_Complaints.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DGrd_Complaints_ItemCommand);

		}
		#endregion

		#region BindDataGrid
		private void BindGrid()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select O_INDX_NMBR,O_VISTR_NM,O_MTNG_OFF_NM,O_MTNG_OFF_DESG,O_MTNG_OFF_LCTN from O_GATE_PASS_RQSTN_DTL where O_REQ_COMPLTN_FLG='A'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				DGrd_Complaints.DataSource=sqlDataReader;
				DGrd_Complaints.DataBind();
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception e)
			{
				EString=e.ToString();
			}
		}
		#endregion

		#region blank Field
		public void BlankFields()
		{
			txtAppName.Text="";
			txtDesg.Text="";
			txtCPFNo.Text="";
			txtLocation.Text="";
			txtExtNo.Text="";
			txtEntryDate.Text="";
			txtEntryTime.Text="";
			txtExitDate.Text="";
			txtExitTime.Text="";
			txtReqType.Text="";
			TxtEntryArea.Text="";
			txtOffName.Text="";
			txtMtng_Off_Desg.Text="";
			txtOffLocation.Text="";
			txtVistr_NM.Text="";
			txtVistr_Org.Text="";
			txtVistr_Desg.Text="";
			txtVistr_Age.Text="";
			txtGender.Text="";
			txtVistr_Idnt_Mark.Text="";
			txtVistr_BldGrp.Text="";
			TxtPurpose.Text="";
			txtRemarks.Text="";
		}
		#endregion

		#region Refresh
		protected void BtnRefresh_Click(object sender, System.EventArgs e)
		{
			BlankFields();
			BindGrid();
			BtnPassConfirmation.Enabled=false;
			//BtnApprove.Enabled=false;
		}
		#endregion

		#region Btn Pass Generation Click
		protected void BtnPassConfirmation_Click(object sender, System.EventArgs e)
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("update O_GATE_PASS_RQSTN_DTL set O_REQ_COMPLTN_FLG='Y' where O_INDX_NMBR='"+txtComplaintID.Text+"'",sqlConnection);
				sqlCommand.ExecuteNonQuery();
				sqlConnection.Close();
				BlankFields();
				BindGrid();
				BtnPassConfirmation.Enabled=false;
				//BtnApprove.Enabled=false;
			}
			catch(Exception ex)
			{
				EString = ex.ToString();
			}
		}
		#endregion

		#region Datagrid Item Command
		private void DGrd_Complaints_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				if(e.CommandName.Equals("ComplaintClose"))
				{
					//BtnReject.Enabled=true;
					//BtnApprove.Enabled=true;
					LinkButton lnk=new LinkButton();
					lnk=(LinkButton)e.Item.Cells[0].FindControl("LnkButton");
					string ComplaintID=e.CommandArgument.ToString();
					SqlConnection sqlConnection=new SqlConnection(strConn);
					sqlConnection.Open();
					string val="select O_INDX_NMBR,O_APLCNT_NM,O_APLCNT_CPF_NMBR,O_APLCNT_DESG,O_APLCNT_LCTN,O_APLCNT_EXT_NMBR,O_REQSTN_TYPE,O_ENTRY_DT,O_ENTRY_TM,O_EXIT_DT,O_EXIT_TM,O_ENTRY_AREA,O_MTNG_OFF_NM,O_MTNG_OFF_DESG,O_MTNG_OFF_LCTN,O_VISTR_NM,O_VISTR_ORGNZTN,O_VISTR_DESG,O_VISTR_AGE,O_VISTR_GNDR,O_VISTR_IDENT_MARK,O_VISTR_BLD_GRP,O_PRPS_OF_VISIT,O_RMRKS from O_GATE_PASS_RQSTN_DTL  where O_INDX_NMBR='" +ComplaintID+"'";
					SqlDataAdapter sqlDataAdapter=new SqlDataAdapter(val,sqlConnection);
					DataSet ds=new DataSet();
					sqlDataAdapter.Fill(ds);
					if(ds.Tables[0].Rows.Count>0)
					{				
						txtAppName.Text					= ds.Tables[0].Rows[0]["O_APLCNT_NM"].ToString();
						txtDesg.Text					= ds.Tables[0].Rows[0]["O_APLCNT_DESG"].ToString();
						txtCPFNo.Text					= ds.Tables[0].Rows[0]["O_APLCNT_CPF_NMBR"].ToString();
						txtLocation.Text				= ds.Tables[0].Rows[0]["O_APLCNT_LCTN"].ToString();
						txtExtNo.Text					= ds.Tables[0].Rows[0]["O_APLCNT_EXT_NMBR"].ToString();
						txtEntryDate.Text				= ds.Tables[0].Rows[0]["O_ENTRY_DT"].ToString();
						txtEntryTime.Text				= ds.Tables[0].Rows[0]["O_ENTRY_TM"].ToString();
						txtExitDate.Text				= ds.Tables[0].Rows[0]["O_EXIT_DT"].ToString();
						txtExitTime.Text				= ds.Tables[0].Rows[0]["O_EXIT_TM"].ToString();
						txtReqType.Text					= ds.Tables[0].Rows[0]["O_REQSTN_TYPE"].ToString();
						TxtEntryArea.Text				= ds.Tables[0].Rows[0]["O_ENTRY_AREA"].ToString();
						txtOffName.Text					= ds.Tables[0].Rows[0]["O_MTNG_OFF_NM"].ToString();
						txtMtng_Off_Desg.Text			= ds.Tables[0].Rows[0]["O_MTNG_OFF_DESG"].ToString();
						txtOffLocation.Text				= ds.Tables[0].Rows[0]["O_MTNG_OFF_LCTN"].ToString();
						txtVistr_NM.Text				= ds.Tables[0].Rows[0]["O_VISTR_NM"].ToString();
						txtVistr_Org.Text				= ds.Tables[0].Rows[0]["O_VISTR_ORGNZTN"].ToString();
						txtVistr_Desg.Text				= ds.Tables[0].Rows[0]["O_VISTR_DESG"].ToString();
						txtVistr_Age.Text				= ds.Tables[0].Rows[0]["O_VISTR_AGE"].ToString();
						txtGender.Text					= ds.Tables[0].Rows[0]["O_VISTR_GNDR"].ToString();
						txtVistr_Idnt_Mark.Text			= ds.Tables[0].Rows[0]["O_VISTR_IDENT_MARK"].ToString();
						txtVistr_BldGrp.Text			= ds.Tables[0].Rows[0]["O_VISTR_BLD_GRP"].ToString();
						TxtPurpose.Text					= ds.Tables[0].Rows[0]["O_PRPS_OF_VISIT"].ToString();
						txtRemarks.Text					= ds.Tables[0].Rows[0]["O_RMRKS"].ToString();
						txtComplaintID.Text				= ComplaintID;
					}
				}

				BtnPassConfirmation.Enabled=true;
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
			
		}
		#endregion

	}
}
