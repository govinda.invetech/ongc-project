﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{

    public partial class quiz_admin : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            //HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
            //HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //HttpContext.Current.Response.Cache.SetNoStore();
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(0));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetValidUntilExpires(true);
            Response.Cache.SetNoStore();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            //HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
            //HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //HttpContext.Current.Response.Cache.SetNoStore();
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(0));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetValidUntilExpires(true);

           
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {

                if (Request.QueryString["type"] == null)
                {
                    lblQueType.Text = "ALL";
                }
                else
                {
                    lblQueType.Text = Request.QueryString["type"];
                }

                string sQuery = "SELECT [IMG_PATH], [PREAMBLE_TEXT] FROM [CY_ABT_PULSE_OF_URAN]";
                DataSet dsPreamble = My.ExecuteSELECTQuery(sQuery);
                if (dsPreamble.Tables[0].Rows.Count == 0)
                {

                }
                else
                {
                    txtPreambleText.Text = dsPreamble.Tables[0].Rows[0][1].ToString();
                }

            }
        }
    }
}