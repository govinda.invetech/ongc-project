﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident
{
    public partial class _8_IncdntReportGeneration : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlDateSelect.Visible = false;
            ddlChoice.Visible = false;
            cmdGenerateReport.Visible = false;
        }
        /*
        protected void optWithDate_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (optWithDate.Checked == true)
                {
                    optWithoutDate.Checked = false;
                    pnlDateSelect.Visible = true;
                    pnlChoice.Visible = true;
                    ddlChoice.Visible = false;
                    cmdGenerateReport.Visible = false;
                }
            }
        }

        protected void optWithoutDate_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (optWithoutDate.Checked == true)
                {
                    optWithDate.Checked = false;
                    pnlDateSelect.Visible = false;
                    pnlChoice.Visible = true;
                    ddlChoice.Visible = false;
                    cmdGenerateReport.Visible = false;
                }
            }
        }
        */
        protected void optReportingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string sReportType = optReportingType.SelectedValue;
                switch (sReportType)
                {
                    case "REPORTED":
                        pnlDateSelect.Visible = true;
                        pnlChoiceSelect.Visible = true;
                        cmdGenerateReport.Visible = true;

                        ddlChoice.Items.Clear();
                        ddlChoice.Items.Add(new ListItem("SELECT REPORTER NAME", ""));
                        string sQuery = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], UPPER([O_EMP_MSTR].[O_EMP_NM]) FROM [CY_MENU_EMP_RELATION], [O_EMP_MSTR] WHERE [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '3' ORDER BY [O_EMP_MSTR].[O_EMP_NM] ASC";
                        DataSet dsReporter = My.ExecuteSELECTQuery(sQuery);
                        if (dsReporter.Tables[0].Rows.Count == 0)
                        {
                            ddlChoice.Visible = false;
                            cmdGenerateReport.Visible = false;
                        }
                        else
                        {
                            for (int i = 0; i < dsReporter.Tables[0].Rows.Count; i++)
                            {
                                string sReporterCPF = dsReporter.Tables[0].Rows[i][0].ToString();
                                string sReporterName = dsReporter.Tables[0].Rows[i][1].ToString();
                                ddlChoice.Items.Add(new ListItem(sReporterName, sReporterCPF));
                            }
                            ddlChoice.Visible = true;
                        }

                        break;
                    case "CATWISE":
                        break;
                    case "STSWISE":
                        break;
                    case "REVWISE":
                        break;
                    case "ACTTGT":
                        break;
                }
            }
        }
        /*
        protected void optCategory_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddlChoice.Items.Clear();
                string sQuery = "SELECT [O_INCDNT_CTGRY] FROM [CY_INC_0_TYPE_MSTR] ORDER BY [O_INCDNT_CTGRY] ASC";
                DataSet dsCategory = My.ExecuteSELECTQuery(sQuery);
                if (dsCategory.Tables[0].Rows.Count == 0)
                {
                    ddlChoice.Visible = false;
                    cmdGenerateReport.Visible = false;
                }
                else
                {
                    for (int i = 0; i < dsCategory.Tables[0].Rows.Count; i++)
                    {
                        string sCategory = dsCategory.Tables[0].Rows[i][0].ToString();
                        ddlChoice.Items.Add(new ListItem(sCategory, sCategory));
                    }
                    ddlChoice.Visible = true;
                    cmdGenerateReport.Visible = true;
                }

                if (optWithDate.Checked == true)
                {
                    pnlDateSelect.Visible = true;
                }
                else
                {
                    pnlDateSelect.Visible = false;
                }
                optReported.Checked  = false;
                optReviewer.Checked = false;
                optStatus.Checked = false;
                optActionDate.Checked = false;
                
            }
        }

        protected void optReported_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddlChoice.Items.Clear();
                string sQuery = "SELECT [O_INCDNT_CTGRY] FROM [CY_INC_0_TYPE_MSTR] ORDER BY [O_INCDNT_CTGRY] ASC";
                DataSet dsCategory = My.ExecuteSELECTQuery(sQuery);
                if (dsCategory.Tables[0].Rows.Count == 0)
                {
                    ddlChoice.Visible = false;
                    cmdGenerateReport.Visible = false;
                }
                else
                {
                    for (int i = 0; i < dsCategory.Tables[0].Rows.Count; i++)
                    {
                        string sCategory = dsCategory.Tables[0].Rows[i][0].ToString();
                        ddlChoice.Items.Add(new ListItem(sCategory, sCategory));
                    }
                    ddlChoice.Visible = true;
                    cmdGenerateReport.Visible = true;
                }

                if (optWithDate.Checked == true)
                {
                    pnlDateSelect.Visible = true;
                }
                else
                {
                    pnlDateSelect.Visible = false;
                }
                optCategory.Checked = false;
                optReviewer.Checked = false;
                optStatus.Checked = false;
                optActionDate.Checked = false;

            }
        }

        protected void optStatus_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddlChoice.Items.Clear();
                string sQuery = "SELECT [O_INCDNT_CTGRY] FROM [CY_INC_0_TYPE_MSTR] ORDER BY [O_INCDNT_CTGRY] ASC";
                DataSet dsCategory = My.ExecuteSELECTQuery(sQuery);
                if (dsCategory.Tables[0].Rows.Count == 0)
                {
                    ddlChoice.Visible = false;
                    cmdGenerateReport.Visible = false;
                }
                else
                {
                    for (int i = 0; i < dsCategory.Tables[0].Rows.Count; i++)
                    {
                        string sCategory = dsCategory.Tables[0].Rows[i][0].ToString();
                        ddlChoice.Items.Add(new ListItem(sCategory, sCategory));
                    }
                    ddlChoice.Visible = true;
                    cmdGenerateReport.Visible = true;
                }

                if (optWithDate.Checked == true)
                {
                    pnlDateSelect.Visible = true;
                }
                else
                {
                    pnlDateSelect.Visible = false;
                }
                optCategory.Checked = false;
                optReviewer.Checked = false;
                optReported.Checked = false;
                optActionDate.Checked = false;

            }
        }

        protected void optReviewer_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddlChoice.Items.Clear();
                string sQuery = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], UPPER([O_EMP_MSTR].[O_EMP_NM]) FROM [CY_MENU_EMP_RELATION], [O_EMP_MSTR] WHERE [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '4' ORDER BY [O_EMP_MSTR].[O_EMP_NM] ASC";
                DataSet dsReviewer = My.ExecuteSELECTQuery(sQuery);
                if (dsReviewer.Tables[0].Rows.Count == 0)
                {
                    ddlChoice.Visible = false;
                    cmdGenerateReport.Visible = false;
                }
                else
                {
                    for (int i = 0; i < dsReviewer.Tables[0].Rows.Count; i++)
                    {
                        string sReviewerCPF = dsReviewer.Tables[0].Rows[i][0].ToString();
                        string sReviewerName = dsReviewer.Tables[0].Rows[i][1].ToString();
                        ddlChoice.Items.Add(new ListItem(sReviewerName, sReviewerCPF));
                    }
                    ddlChoice.Visible = true;
                    cmdGenerateReport.Visible = true;
                }

                if (optWithDate.Checked == true)
                {
                    pnlDateSelect.Visible = true;
                }
                else
                {
                    pnlDateSelect.Visible = false;
                }
                optCategory.Checked = false;
                optStatus.Checked = false;
                optReported.Checked = false;
                optActionDate.Checked = false;

            }
        }

        protected void optActionDate_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddlChoice.Items.Clear();
                string sQuery = "SELECT [O_INCDNT_CTGRY] FROM [CY_INC_0_TYPE_MSTR] ORDER BY [O_INCDNT_CTGRY] ASC";
                DataSet dsCategory = My.ExecuteSELECTQuery(sQuery);
                if (dsCategory.Tables[0].Rows.Count == 0)
                {
                    ddlChoice.Visible = false;
                    cmdGenerateReport.Visible = false;
                }
                else
                {
                    for (int i = 0; i < dsCategory.Tables[0].Rows.Count; i++)
                    {
                        string sCategory = dsCategory.Tables[0].Rows[i][0].ToString();
                        ddlChoice.Items.Add(new ListItem(sCategory, sCategory));
                    }
                    ddlChoice.Visible = true;
                    cmdGenerateReport.Visible = true;
                }

                if (optWithDate.Checked == true)
                {
                    pnlDateSelect.Visible = true;
                }
                else
                {
                    pnlDateSelect.Visible = false;
                }
                optCategory.Checked = false;
                optReviewer.Checked = false;
                optReported.Checked = false;
                optStatus.Checked = false;

            }
        }
         * */
    }
}