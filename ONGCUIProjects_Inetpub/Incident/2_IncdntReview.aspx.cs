﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident
{
    public partial class _2_IncdntReview : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        #region Variables
        string sReviewTableData = "";
        string sNonReviewTableData = "";
        int count = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncDetailsQuery = "SELECT [CY_INC_1_DETAILS].[O_INDX_NMBR], [CY_INC_1_DETAILS].[O_ENG_NM], [CY_INC_1_DETAILS].[O_ENG_DSG], [CY_INC_1_DETAILS].[O_CPF_NMBR], ";
            sIncDetailsQuery = sIncDetailsQuery + "[CY_INC_1_DETAILS].[O_INCDNT_DT], [CY_INC_1_DETAILS].[O_INCDNT_TIME], [CY_INC_1_DETAILS].[CY_TYPE_OF_INCIDENT], [CY_INC_1_DETAILS].[CY_STATUS_LEVEL], ";
            sIncDetailsQuery = sIncDetailsQuery + "[CY_INC_2_REV_DTL].[CY_INCIDENT_CATEGORY], [CY_INC_2_REV_DTL].[O_REV_INCDNT_CTGRY],[CY_INC_2_REV_DTL].[O_REV_CPF_NMBR]";
            sIncDetailsQuery = sIncDetailsQuery + "FROM [CY_INC_1_DETAILS] LEFT JOIN";
            sIncDetailsQuery = sIncDetailsQuery + "[CY_INC_2_REV_DTL] ON [CY_INC_1_DETAILS].[O_INDX_NMBR] = [CY_INC_2_REV_DTL].[O_INCIDENT_NMBR]";
            sIncDetailsQuery = sIncDetailsQuery + "ORDER BY [CY_INC_1_DETAILS].[O_INCDNT_DT] DESC, [CY_INC_1_DETAILS].[CY_STATUS_LEVEL] ASC";
            //string sIncDetailsQuery = "SELECT [O_INDX_NMBR], [O_ENG_NM], [O_ENG_DSG], [O_CPF_NMBR], [O_INCDNT_DT], [O_INCDNT_TIME], [CY_TYPE_OF_INCIDENT],[CY_STATUS_LEVEL] FROM [CY_INC_DETAILS] WHERE [CY_STATUS_LEVEL] >= '1' ORDER BY [CY_STATUS_LEVEL] ASC";

            DataSet dsIncDetails = My.ExecuteSELECTQuery(sIncDetailsQuery);

            for (int i = 0; i < dsIncDetails.Tables[0].Rows.Count; i++)
            {
                

                string sIncdID = dsIncDetails.Tables[0].Rows[i][0].ToString();


                IncidentClass CurrentIncident = new IncidentClass(sIncdID);

                string sIncdRptrName = CurrentIncident.RptName;
                string sIncdRptrDesig = CurrentIncident.RptDesig;
                string sIncdRptrCPF = CurrentIncident.RptCPF;
                DateTime sIncdDate = CurrentIncident.RptDate;
                string sIncdTime = CurrentIncident.RptTime;
                string sIncdRptrType = CurrentIncident.RptIncType;
                string sIncdStatus = CurrentIncident.StatusLevel;
                string sIncdCategory = CurrentIncident.RevIncCategory;
                string sIncdType = CurrentIncident.RevIncType;
                string sReviewerCPF=CurrentIncident.RevCPF;

                if (sIncdStatus == "7" || sIncdStatus == "10")
                {

                }
                else
                {
                    count = count + 1;
                    if (sIncdStatus == "1")
                    {
                        //Date   Time    Type    Category    Reporter    Details     Status
                        sNonReviewTableData = sNonReviewTableData + "<tr>";
                        sNonReviewTableData = sNonReviewTableData + "<td>" + sIncdDate.ToString("d MMM, yyyy") + "</td>";
                        sNonReviewTableData = sNonReviewTableData + "<td>" + sIncdTime + "</td>";
                        sNonReviewTableData = sNonReviewTableData + "<td>" + sIncdType + "</td>";
                        sNonReviewTableData = sNonReviewTableData + "<td>" + sIncdCategory + "</td>";
                        sNonReviewTableData = sNonReviewTableData + "<td>" + sIncdRptrCPF + ". " + sIncdRptrName + " (" + sIncdRptrDesig + ")" + "</td>";
                        sNonReviewTableData = sNonReviewTableData + "<td>" + My.getStatusStringFromLevel(sIncdStatus) + "</td>";
                        sNonReviewTableData = sNonReviewTableData + "<td>";
                        sNonReviewTableData = sNonReviewTableData + "<a IncdID=\"" + sIncdID + "\" style=\"margin-right:15px;\" class=\"IncdDtlFancyBox\" href=\"fancybox/2_IncidentReview.aspx?IncdID=" + sIncdID + "\">Review</a>";
                        if (sIncdRptrType == "INCIDENT")
                        {
                            sNonReviewTableData = sNonReviewTableData + "<a IncdID=\"" + sIncdID + "\" style=\"margin-right:15px;\" class=\"IncdDtlFancyBox\" href=\"fancybox/rpt_iso.aspx?IncdID=" + sIncdID + "\">Print ISO</a>";

                        }
                        sNonReviewTableData = sNonReviewTableData + "</td>";
                        sNonReviewTableData = sNonReviewTableData + "</tr>";
                    }
                    else
                    {
                        if (sReviewerCPF == Session["Login_Name"].ToString())
                        {
                            sReviewTableData = sReviewTableData + "<tr>";
                            sReviewTableData = sReviewTableData + "<td>" + sIncdDate.ToString("d MMM, yyyy") + "</td>";
                            sReviewTableData = sReviewTableData + "<td>" + sIncdTime + "</td>";
                            sReviewTableData = sReviewTableData + "<td>" + sIncdType + "</td>";
                            sReviewTableData = sReviewTableData + "<td>" + sIncdCategory + "</td>";
                            sReviewTableData = sReviewTableData + "<td>" + sIncdRptrCPF + ". " + sIncdRptrName + " (" + sIncdRptrDesig + ")" + "</td>";
                            sReviewTableData = sReviewTableData + "<td>" + My.getStatusStringFromLevel(sIncdStatus) + "</td>";
                            sReviewTableData = sReviewTableData + "<td>";
                            sReviewTableData = sReviewTableData + "<a IncdID=\"" + sIncdID + "\" style=\"margin-right:15px;\" class=\"IncdDtlFancyBox\" href=\"fancybox/IncidentDetails.aspx?IncdID=" + sIncdID + "&level=2\">View Details</a>";
                            if (sIncdRptrType == "INCIDENT")
                            {
                                sReviewTableData = sReviewTableData + "<a IncdID=\"" + sIncdID + "\" style=\"margin-right:15px;\" class=\"IncdDtlFancyBox\" href=\"fancybox/rpt_iso.aspx?IncdID=" + sIncdID + "\">Print ISO</a>";

                            }
                            sReviewTableData = sReviewTableData + "</td>";
                            sReviewTableData = sReviewTableData + "</tr>";
                        }
                    }


                }


            }

            if (count > 0)
            {
                string sTableHeading = "<tr><th>Date</th><th>Time</th><th>Type</th><th>Category</th><th>Reporter Details</th><th>Status</th><th></th></tr>";

                if (sNonReviewTableData == "")
                {
                    div_nonreview_table.InnerHtml = "<h2>No Incident or Near Miss to Review</h2>";
                }
                else
                {
                    div_nonreview_table.InnerHtml = "<table class=\"tftable\" border=\"1\">" + sTableHeading + sNonReviewTableData + "</table>";
                }

                if (sReviewTableData == "")
                {
                    div_review_table.InnerHtml = "<h2>You have not reviewed any Incident or Near Miss</h2>";
                }
                else
                {
                    div_review_table.InnerHtml = "<table class=\"tftable\" border=\"1\">" + sTableHeading + sReviewTableData + "</table>";
                }
            }
        }
    }
}