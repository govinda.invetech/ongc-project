﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="0_IncdntMasters.aspx.cs" Inherits="ONGCUIProjects.Incident._0_IncdntMasters" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" href="../css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="../css/css3.css" media="screen" type="text/css" />

		<script type="text/javascript" language="javascript" src="../js/jquery-1.7.2.min_a39dcc03.js"></script>
        <script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.timepicker.js"></script>
        <script type="text/javascript" src="../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox-1.3.4.css" media="screen">
        <link rel="stylesheet" href="../js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen">
        <link rel="stylesheet" type="text/css" href="../css/jquery.gritter.css">
        <script type="text/javascript" src="../js/jquery.gritter.js"></script>

        <script src="../js/jquery.autocomplete-min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/AutoCompleteStyle.css" type="text/css" media="screen" />
        <script type="text/javascript">
            $(document).ready(function () {
                var my_hint = $("#lblmodule_hint").text();
                $("#lblmodule_hint").hide();

                $("#ADDNEWDATA").click(function () {
                    switch (my_hint) {
                        case "Location":
                            $.fancybox({
                                modal: true,
                                href: "#AddNewLocation"
                            });
                            break;
                        case "Safety":
                            $.fancybox({
                                modal: true,
                                href: "#AddNewSafety"
                            });
                            break;
                        case "Type":
                            $("#my_heading").text("Add New Incident Type");
                            $("#content_heading").text("Incident Type Name");
                            $.fancybox({
                                modal: true,
                                href: "#AddNewSafety"
                            });
                            break;
                        case "Category":
                            $("#my_heading").text("Add New Category");
                            $("#content_heading").text("Category Name");
                            $.fancybox({
                                modal: true,
                                href: "#AddNewSafety"
                            });
                            break;
                    }
                });


                

                $(".Close_ME").click(function () {
                    window.location = self.location;
                    $.fancybox.close();
                });

                var AreaSearch_options, AreaSearch_a;
                jQuery(function () {
                    var options = {
                        serviceUrl: '../services/auto.aspx',
                        onSelect: AreaSearch_onAutocompleteSelect,
                        deferRequestBy: 0, //miliseconds
                        params: { type: 'area_name', limit: '10' },
                        noCache: true //set to true, to disable caching
                    };
                    AreaSearch_a = $("#txtAreaName").autocomplete(options);
                });

                var AreaSearch_onAutocompleteSelect = function (AreaSearch_value, AreaSearch_data) {
                    if (AreaSearch_value == "Enter something else..") {
                        $("#txtAreaName").val("");
                    }
                }

                var EmployeeSearch_options, EmployeeSearch_a;
                jQuery(function () {
                    var options = {
                        serviceUrl: '../../services/auto.aspx',
                        onSelect: EmployeeSearch_onAutocompleteSelect,
                        deferRequestBy: 0, //miliseconds
                        params: { type: 'incdnt_mgr', limit: '10' },
                        noCache: true //set to true, to disable caching
                    };
                    EmployeeSearch_a = $("#txtEmployee").autocomplete(options);
                });

                var sEmployeeCPFNo = "";
                var EmployeeSearch_onAutocompleteSelect = function (EmployeeSearch_value, EmployeeSearch_data) {
                    if (EmployeeSearch_value == "Enter something else..") {
                        $("#txtEmployee").val("");
                        sEmployeeCPFNo = "";
                    } else {
                        sEmployeeCPFNo = EmployeeSearch_data;
                    }
                }


                $(".Save_data").click(function () {
                    var my_data = "";
                    switch (my_hint) {
                        case "Location":
                            var sAreaName = $("#txtAreaName").val().trim();
                            var sLocationName = $("#txtLocationName").val().trim();
                            if (sAreaName == "" && sLocationName == "") {
                                alert("Please fill all fields...!");
                                return;
                            } my_data = "type=ADD&LocationName=" + sLocationName + "&AreaName=" + sAreaName + "&EmployeeCPFNo=" + sEmployeeCPFNo + "&MasterType=" + my_hint;

                            break;
                        case "Safety":
                            var safety_name = $("#txt_safety_name").val();
                            if (safety_name == "") {
                                alert("Please fill safety name");
                                return;
                            } my_data = "type=ADD&safetyname=" + safety_name + "&MasterType=" + my_hint;
                            break;
                        case "Type":
                            var incident_type = $("#txt_my_data").val();
                            if (incident_type == "") {
                                alert("Please fill incident type");
                                return;
                            } my_data = "type=ADD&incident_type=" + incident_type + "&MasterType=" + my_hint;
                            break;
                        case "Category":
                            var categoryname = $("#txt_my_data").val();
                            if (categoryname == "") {
                                alert("Please fill category name");
                                return;
                            } my_data = "type=ADD&categoryname=" + categoryname + "&MasterType=" + my_hint;
                            break;
                        default:
                            return false;
                            break;
                    }

                    $.ajax({
                        type: "GET",
                        url: "service/0_IncdntMasterSave.aspx",
                        data: my_data,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                $(".Response").html(msg_arr[2]);
                                switch (my_hint) {
                                    case "Location":
                                        $("#Response").html(msg_arr[2]);
                                        $("#txtAreaName").val("");
                                        $("#txtLocationName").val("");
                                        $("#txtEmployee").val("");
                                        sEmployeeCPFNo = "";
                                        break;
                                    case "Safety":
                                        $("#txt_safety_name").val("");
                                        break;
                                    case "Type":
                                        $("#txt_my_data").val("");
                                        break;
                                    case "Category":
                                        $("#txt_my_data").val("");
                                        break;
                                }
                            }
                        }
                    });
                });

                $(".cmdDeleteLocation").click(function () {
                    var sLocationID = $(this).attr("My_ID");
                    if (confirm("Are you sure to delete this location?")) {
                        $.ajax({
                            type: "GET",
                            url: "service/0_IncdntMasterSave.aspx",
                            data: "type=DELETE&my_id=" + sLocationID + "&MasterType=" + my_hint,
                            success: function (msg) {
                                var msg_arr = msg.split("~");
                                if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                    alert(msg_arr[2]);
                                    window.location = self.location;
                                } else {
                                    alert(msg_arr[2]);
                                }
                            }
                        });
                    }
                });
                $(document).click(function () { $("#Response").html(""); });
            });
        </script>
    </head>
<body>
    <form id="form1" runat="server">
    <div class="div-fullwidth iframeMainDiv">
     <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;"><asp:Label ID="lblMasterTypeHeading" runat="server"></asp:Label></h1>
                <asp:Label ID="lblmodule_hint" runat="server"></asp:Label>
                <input type="button" class="g-button g-button-submit" style="text-transform: uppercase; float:right; margin-right:10px; margin-top:5px; text-decoration:none;" onclick="window.location='0_IncdntMasters.aspx?MasterType=Category';" id="cmdCategory" value="Category" />
                <input type="button" class="g-button g-button-submit" style="text-transform: uppercase; float:right; margin-right:10px; margin-top:5px; text-decoration:none;" onclick="window.location='0_IncdntMasters.aspx?MasterType=Type';" id="cmdType" value="Incident Type" />
                <input type="button" class="g-button g-button-submit" style="text-transform: uppercase; float:right; margin-right:10px; margin-top:5px; text-decoration:none;" onclick="window.location='0_IncdntMasters.aspx?MasterType=Safety';" id="cmdSafety" value="Safety" />
                <input type="button" class="g-button g-button-submit" style="text-transform: uppercase; float:right; margin-right:10px; margin-top:5px; text-decoration:none;" onclick="window.location='0_IncdntMasters.aspx?MasterType=Location';" id="cmdLocation" value="Location" />
         <asp:Button 
                    ID="ADDNEWDATA" runat="server" Text="Button" 
                    class="g-button g-button-share" 
                    style="float:right; margin-right:10px; margin-top:5px; text-decoration:none; text-transform: uppercase;"  
                    href="#AddNewLocation" onclientclick="return false;" />
                
            &nbsp;</div>
            <div class="div-pagebottom"></div>

            <div runat="server" id="divTableData" class="div-fullwidth">
            <div style="left: 40%; top: 50%; position: fixed;">
            
                <p class="content" style="font-size:20pt;">Please select an option</p>
            
        </div>
            </div>

        </div>

        <div style="display: none;">
            <div id="AddNewLocation" style="position:relative;float:left; width:400px;">
                <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A;">Add New Location</h1>
                <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px; width: 100%;">
                    <p class="content" style="width: 140px;">Area Name<span style="color:Red;">(*)</span></p>
                    <asp:TextBox ID="txtAreaName" runat="server" EnableTheming="True" Width="250px"></asp:TextBox>
                </div>
                <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px; width: 100%;">
                    <p class="content" style="width: 140px;">Location Name<span style="color:Red;">(*)</span></p>
                    <asp:TextBox ID="txtLocationName" runat="server" EnableTheming="True" Width="250px"></asp:TextBox>
                </div>
                <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px; width: 100%; display :none;">
                    <p class="content" style="width: 140px;">Employee Name</p>
                    <asp:TextBox ID="txtEmployee" runat="server" EnableTheming="True" Width="250px"></asp:TextBox>
                </div>
                <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
                <div class="div-fullwidth">
                    <p class="Response content" style="width: auto; color:Red;"></p>
                     <a class="g-button g-button-red Close_ME " href="javascript:void(0)"  style="float:right;" >CLOSE</a>
                      <a class="g-button g-button-submit Save_data " href="javascript:void(0)"  style="float:right; margin-right:10px" >SAVE</a>
                    
                </div>
            </div>
            // for safety
             <div id="AddNewSafety" style="position:relative;float:left; width:400px;">
                <h1 class="heading" id="my_heading"style="padding:0; border-bottom: 2px solid #58595A;">Add New Safety</h1>
                <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px; width: 100%;">
                    <p class="content" id="content_heading" style="width: auto;">Safety Name</p><span style="color:Red;">(*)</span>
                    <asp:TextBox runat="server" EnableTheming="True" Width="250px" TextMode="SingleLine" ID="txt_my_data"></asp:TextBox>
                      <asp:TextBox runat="server" EnableTheming="True" Width="100%" 
                        TextMode="MultiLine" ID="txt_safety_name" Height="150px" style="margin-top:10px"></asp:TextBox>
                </div>               
                <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
                <div class="div-fullwidth">
                 <p class="Response content" style="width: auto; color:Red;"></p>                                      
                     <a class="g-button g-button-red Close_ME " href="javascript:void(0)"  style="float:right;" >CLOSE</a>
                      <a class="g-button g-button-submit Save_data " href="javascript:void(0)"  style="float:right; margin-right:10px" >SAVE</a>
                   
                </div>
            </div>
        </div>
    </form>
</body>
</html>
