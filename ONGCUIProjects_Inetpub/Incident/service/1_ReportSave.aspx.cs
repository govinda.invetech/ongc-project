﻿using System;
using System.Data;

namespace ONGCUIProjects.Incident.service
{
    public partial class _1_ReportSave : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        #region Variables
        string sInsert = "";
        string sInsert2 = "";
        string sIncidentNumber = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            DataSet dsIncidentNumber = My.ExecuteSELECTQuery("SELECT {fn IFNULL ( MAX([O_INDX_NMBR])+1,1)} FROM [CY_INC_1_DETAILS]");
            sIncidentNumber = dsIncidentNumber.Tables[0].Rows[0][0].ToString();

            string sSysDate = System.DateTime.Today.ToShortDateString();
            string sSysTime = System.DateTime.Now.ToString("HH:mm");

            string sCPFNo = Session["Login_Name"].ToString();

            string sTypeOfIncident = Request.Form["type"];
            if (sTypeOfIncident == "NEARMISS" || sTypeOfIncident == "DOREPORT")
            {
                string sRptEngCPF = Request.Form["RptEngCPF"];
                string sRptEngName = Request.Form["RptEngName"];
                string sRptEngDesig = Request.Form["RptEngDesig"];
                string sIncdntDesc = Request.Form["NearMissDesc"];
              //  sIncdntDesc = sIncdntDesc.Replace("!-!", "&").Replace("'"," ");
                string sIncdntDate = Request.Form["NearMissDate"];
                string sIncdntTime = Request.Form["NearMissTime"];
                string sIncdntLocation = Request.Form["NearMissLocation"];
                string sIncdntArea = Request.Form["NearMissArea"];
                string sIncdntDept = Request.Form["NearMissDept"];
                string sRushToEmg = Request.Form["RushToEmg"];

                string curntdate = Request.Form["curntdate"];
                string curnttime = Request.Form["curnttime"];
                DateTime dtcurntdate = System.Convert.ToDateTime(My.ConvertDateStringintoSQLDateString(curntdate.ToString()));

                DateTime dtIncdntDate = System.Convert.ToDateTime(My.ConvertDateStringintoSQLDateString( sIncdntDate.ToString()));
                int iDiff = DateTime.Compare(dtIncdntDate, System.DateTime.Today);
                if (iDiff > 0)
                {
                    Response.Write("FALSE~ERR~Near Miss date should not be in future date.");
                    return;
                }

                sInsert = "INSERT INTO [CY_INC_1_DETAILS] ([O_INDX_NMBR],[O_ENG_NM], [O_ENG_DSG], [O_CPF_NMBR], [O_INCDNT_DESCRPTN], [O_INCDNT_DT], [O_INCDNT_TIME],";
                sInsert = sInsert + "[O_INCDNT_LCTN], [O_RUSH_EMRGNCY], [O_SYS_DT], [O_SYS_TIME], [CY_O_AREA], [CY_O_DEPT], [CY_TYPE_OF_INCIDENT],[CY_ENTRY_BY],[CY_CURRENT_DATE],[CY_CURRENT_TIME]) VALUES(";
                sInsert = sInsert + "'" + sIncidentNumber + "','" + sRptEngName + "', '" + sRptEngDesig + "', '" + sRptEngCPF + "', '" + sIncdntDesc + "', '" + My.ConvertDateStringintoSQLDateString(sIncdntDate) + "', '" + sIncdntTime + "',";
                sInsert = sInsert + "'" + sIncdntLocation + "', '" + sRushToEmg + "', '" + My.ConvertDateStringintoSQLDateString(sSysDate) + "', '" + My.ConvertDateStringintoSQLDateString(sSysTime) + "', '" + sIncdntArea + "', '" + sIncdntDept + "',";
                sInsert = sInsert + "'" + sTypeOfIncident + "','" + sCPFNo + "','" + My.ConvertDateStringintoSQLDateString(curntdate) + "','" + curnttime + "')";

                try
                {
                    CheckAndInsertLocation(sIncdntArea, sIncdntLocation, sCPFNo);

                    if (My.ExecuteSQLQuery(sInsert) == true)
                    {
                        Response.Write("TRUE~SUCCESS~Report Successfully Saved");
                    }
                    else //else case of execute insert query
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *INCNIMSRPTG183");
                    }
                }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //  try catch ends

            }
            else if (sTypeOfIncident == "INCIDENT")
            {
                string sRptEngCPF = Request.Form["RptEngCPF"];
                string sRptEngName = Request.Form["RptEngName"];
                string sRptEngDesig = Request.Form["RptEngDesig"];
                string sIncdntDate = Request.Form["IncdntDate"];
                string sIncdntTime = Request.Form["IncdntTime"];
                string sIncdntLocation = Request.Form["IncdntLocation"];
                string sIncdntArea = Request.Form["IncdntArea"];
                string sIncdntDept = Request.Form["IncdntDept"];
                string sProperty = Request.Form["Property"];
                string sProcess = Request.Form["Process"];
                string sEnvironment = Request.Form["Environment"];
                string sPersonalInjury = Request.Form["PersonalInjury"];
                string sIncdntDesc = Request.Form["IncdntDesc"];
                string sIncdntAppCause = Request.Form["IncdntAppCause"];
                string sIncdntImmdRemedial = Request.Form["IncdntImmdRemedial"];
                string sRushToEmg = Request.Form["RushToEmg"];
                string sInjuryPersonDetails = Request.Form["InjuryPersonDetails"];

                string curntdate = Request.Form["curntdate"];
                string curnttime = Request.Form["curnttime"];
                DateTime dtcurntdate = System.Convert.ToDateTime(My.ConvertDateStringintoSQLDateString(curntdate.ToString()));

                DateTime dtIncdntDate = System.Convert.ToDateTime(My.ConvertDateStringintoSQLDateString(sIncdntDate.ToString()));
                int iDiff = DateTime.Compare(dtIncdntDate, System.DateTime.Today);
                if (iDiff > 0)
                {
                    Response.Write("FALSE~ERR~Near Miss date should not be in future date.");
                    return;
                }

                sInsert = "INSERT INTO [CY_INC_1_DETAILS] ([O_INDX_NMBR],[O_ENG_NM],[O_ENG_DSG],[O_CPF_NMBR],[O_INCDNT_DESCRPTN],[O_INCDNT_DT],[O_INCDNT_TIME],[O_INCDNT_LCTN],[O_RUSH_EMRGNCY],[O_SYS_DT],[O_SYS_TIME],[CY_O_AREA],[CY_O_DEPT],[CY_O_APP_ACC],[CY_O_IMD_REM_ACTION],[CY_TYPE_OF_INCIDENT],[CY_PROPERTY],[CY_PROCESS],[CY_ENVIRONMENT],[CY_PERSONAL_INJURY],[CY_ENTRY_BY],[CY_TIMESTAMP],[CY_CURRENT_DATE],[CY_CURRENT_TIME]) VALUES ('";
                sInsert += sIncidentNumber + "', '" + sRptEngName + "', '" + sRptEngDesig + "', '" + sRptEngCPF + "', '" + sIncdntDesc + "', '" + My.ConvertDateStringintoSQLDateString(sIncdntDate) + "', '" + sIncdntTime + "', '" + sIncdntLocation + "', '" + sRushToEmg + "', '" + My.ConvertDateStringintoSQLDateString(sSysDate) + "', '" + sSysTime + "', '" + sIncdntArea + "', '" + sIncdntDept + "', '" + sIncdntAppCause + "', '" + sIncdntImmdRemedial + "', '" + sTypeOfIncident + "', '" + sProperty + "', '" + sProcess + "', '" + sEnvironment + "', '" + sPersonalInjury + "','" + sCPFNo + "', getdate(),'" + My.ConvertDateStringintoSQLDateString(curntdate) + "','" + curnttime + "')";
                try
                {
                    CheckAndInsertLocation(sIncdntArea, sIncdntLocation, sCPFNo);

                    if (My.ExecuteSQLQuery(sInsert) == true)
                    {

                        string[] sInjuryPersonDetailsArr = sInjuryPersonDetails.Split('`');
                        for (int i = 0; i < sInjuryPersonDetailsArr.Length - 1; i++)
                        {
                            string sNameOfPerson = My.sDataStringTrim(1, sInjuryPersonDetailsArr[i]);
                            string sDesig = My.sDataStringTrim(2, sInjuryPersonDetailsArr[i]);
                            string sCPF = My.sDataStringTrim(3, sInjuryPersonDetailsArr[i]);
                            string sNatureOfInjury = My.sDataStringTrim(4, sInjuryPersonDetailsArr[i]);
                            sInsert2 = sInsert2 + "('" + sIncidentNumber + "','" + sNameOfPerson + "','" + sDesig + "','" + sCPF + "','" + sNatureOfInjury + "','" + sCPFNo + "'),";

                        }
                        if (sInsert2 == "")
                        {
                            Response.Write("TRUE~SUCCESS~Report Successfully Saved");
                        }
                        else
                        {
                            sInsert2 = "INSERT INTO [CY_INC_1_INJURY_RELATION] ([INCIDENT_NO],[NAME_OF_PERSON],[DESIG],[CPF_NO],[NATURE_OF_INJURY],[ENTRY_BY]) VALUES " + sInsert2.Substring(0, sInsert2.Length - 1);
                            if (My.ExecuteSQLQuery(sInsert2) == true)
                            {
                                Response.Write("TRUE~SUCCESS~Report Successfully Saved");
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *INCNIMSRPTG183");
                            }
                        }

                    }
                    else //else case of execute insert query
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *INCNIMSRPTG183");
                    }
                }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //  try catch ends
            }


        }

        private void CheckAndInsertLocation(string sArea, string sLocation, string sEntryBy)
        {
            string sCheckQuery = "SELECT * FROM [CY_INC_0_LOCATION_MSTR] WHERE UPPER([AREA_NAME])='" + sArea.ToUpper() + "' AND UPPER([LOCATION_NAME]) = '" + sLocation.ToUpper() + "'";

            DataSet dsCheck = My.ExecuteSELECTQuery(sCheckQuery);

            if (dsCheck.Tables[0].Rows.Count == 0)
            {
                string sInsert = "INSERT INTO [CY_INC_0_LOCATION_MSTR] ([AREA_NAME], [LOCATION_NAME], [ENTRY_BY])";
                sInsert = sInsert + "VALUES('" + sArea.ToUpper() + "','" + sLocation.ToUpper() + "','" + sEntryBy + "')";

                My.ExecuteSQLQuery(sInsert);
            }

        }
    }
}