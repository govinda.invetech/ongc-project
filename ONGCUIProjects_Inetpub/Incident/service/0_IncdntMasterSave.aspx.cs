﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.Incident.service
{
    public partial class _0_IncdntMasterSave : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            string sEntryBy = Session["Login_Name"].ToString();
            string type = Request.QueryString["type"];
            string MasterType = Request.QueryString["MasterType"];
            string sInsert = "";
            string sDelete = "";
            if (type == "ADD") {
                switch (MasterType)
                {

                    case "Location":                        
                        string sLocationName = Request.QueryString["LocationName"];
                        string sAreaName = Request.QueryString["AreaName"];
                        string sEmployeeCPFNo = Request.QueryString["EmployeeCPFNo"];
                         sInsert = "INSERT INTO [CY_INC_0_LOCATION_MSTR] ([AREA_NAME], [LOCATION_NAME], [LOCATION_MANAGER], [ENTRY_BY])";
                         sInsert = sInsert + "VALUES('" + sAreaName.ToUpper() + "','" + sLocationName.ToUpper() + "','" + sEmployeeCPFNo.ToUpper() + "','" + sEntryBy + "')";
                        break;
                    case "Category":
                        string sCategoryname = Request.QueryString["categoryname"];
                        sInsert = "INSERT INTO [CY_INC_0_CATEGORY_MSTR]([CATEGORY_NAME] ,[ENTRY_BY],[TIMESTAMP]) VALUES('" + sCategoryname.ToUpper() + "','" + sEntryBy + "',GETDATE());";
                        break;
                    case "Safety":
                        string sSafetyName = Request.QueryString["safetyname"];
                        sInsert = "INSERT INTO [CY_INC_0_SAFETY_MSTR] ([O_SFTY_MSR] ,[CY_ENTRY_BY] ,[O_SYS_DT_TM])  VALUES('" + sSafetyName + "','" + sEntryBy + "',GETDATE())";
                        break;
                    case "Type":
                        string sIncident_type = Request.QueryString["incident_type"];
                        sInsert = "INSERT INTO [CY_INC_0_TYPE_MSTR] ([O_INCDNT_CTGRY],[CY_ENTRY_BY] ,[O_SYS_DT_TM]) VALUES('" + sIncident_type.ToUpper() + "','" + sEntryBy + "',GETDATE())";
                        break;
                    default:
                        Response.Write("FALSE~SUCCESS~Invalid Input");
                        break;
                }


            }
            else if (type == "DELETE")
            {
                string my_id = "";
                my_id = Request.QueryString["my_id"];
                switch (MasterType)
                {

                    case "Location":
                        sDelete = "DELETE FROM [CY_INC_0_LOCATION_MSTR] WHERE [id]='" + my_id + "'";
                        break;
                    case "Category":
                        sDelete = "DELETE FROM [CY_INC_0_CATEGORY_MSTR] WHERE [ID] ='" + my_id + "'";
                        break;
                    case "Safety":
                        sDelete = "DELETE FROM [CY_INC_0_SAFETY_MSTR] WHERE [O_INDX_NMBR]='" + my_id + "'";
                        break;
                    case "Type":
                        sDelete = "DELETE FROM [CY_INC_0_TYPE_MSTR] WHERE [O_INCDNT_ID]='" + my_id + "'";
                        break;
                    default:
                        Response.Write("FALSE~ERR~Invalid Input");
                        break;
                }


            }
            else {
                Response.Write("FALSE~SUCCESS~Invalid Input");
                return;
            }
            // Execute Query
            try
            {
              
                if (type == "ADD")
                {
                    if (My.ExecuteSQLQuery(sInsert))
                    {
                        Response.Write("TRUE~SUCCESS~" + type + "ED SUCCESSFULLY");
                    }                    
                   
                }
                else {
                    if (My.ExecuteSQLQuery(sDelete))
                    {
                        Response.Write("TRUE~SUCCESS~"+type + "ED SUCCESSFULLY");
                    }
                }
               
            }
            catch (Exception a) {
                Response.Write("FALSE~SUCCESS~" + a.Message);
            }

           
        }
    }
}