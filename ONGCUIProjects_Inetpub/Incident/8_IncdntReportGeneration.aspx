﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="8_IncdntReportGeneration.aspx.cs" Inherits="ONGCUIProjects.Incident._8_IncdntReportGeneration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->

    <style type="text/css">  
        .RadioButtonListCssClass  
        {  
            color: rgb(92, 92, 92);
font-family: MyriadPro-Regular;
font-size: 14px;
font-weight: bold;
margin-left: 3px;  
            }  
    </style> 
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;">Closed Incident Details</h1>
                <%--<a class="g-button g-button-red" style="float:right; margin-right:10px; margin-top:5px; text-decoration:none;" id="cmdClosedIncident" href="javascript:void(0);" >CLOSED</a>
                <a class="g-button g-button-submit cmdReportingType" style="float:right; margin-right:10px; margin-top:5px; text-decoration:none;" href="fancybox/1d_DOReport.aspx">D O REPORT</a>
                <a class="g-button g-button-submit cmdReportingType" style="float:right; margin-right:10px; margin-top:5px; text-decoration:none;" href="fancybox/1c_PIReport.aspx">P I REPORT</a>
                <a class="g-button g-button-submit cmdReportingType" style="float:right; margin-right:10px; margin-top:5px; text-decoration:none;" href="fancybox/1b_Incident.aspx">INCIDENT</a>
                <a class="g-button g-button-submit cmdReportingType" style="float:right; margin-right:10px; margin-top:5px; text-decoration:none;" href="fancybox/1a_NearMiss.aspx">NEAR MISS</a>--%>
            </div>
        <div class="div-pagebottom"></div>
        <div runat="server" class="div-fullwidth">

                <asp:Panel ID="pnlReportingType" runat="server" CssClass="div-fullwidth" 
                    BorderStyle="Solid" BorderWidth="2px" style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 0px;width: 99.7%;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
                background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom:20px;">
                    <asp:Label ID="Label3" runat="server" Text="Select Type" 
                        style='position:relative;float:left; top: 0px; left: 0px;padding :6px 0px;' 
                        CssClass="RadioButtonListCssClass" Width="10%" ></asp:Label>
                    
                    <asp:RadioButtonList ID="optReportingType" runat="server" AutoPostBack="True" 
                        CellPadding="3" CellSpacing="5"
                        onselectedindexchanged="optReportingType_SelectedIndexChanged" 
                        RepeatColumns="5" CssClass="RadioButtonListCssClass" 
                        style='position:relative;float:left; top: 0px; left: 0px; width: 89%;' 
                        RepeatDirection="Horizontal" Width="89%">
                        <asp:ListItem Value="REPORTED">Reported</asp:ListItem>
                        <asp:ListItem Value="CATWISE">Category Wise</asp:ListItem>
                        <asp:ListItem Value="STSWISE">Status Wise</asp:ListItem>
                        <asp:ListItem Value="REVWISE">Reviewer Wise</asp:ListItem>
                        <asp:ListItem Value="ACTTGT">Action Target</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Panel>

                <div style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 0px;width: 99.7%;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
                background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom:20px;">
                    <asp:Panel CssClass="div-halfwidth" runat="server" ID="pnlDateSelect" Width="450px">
                        <%--<asp:Label ID="Label1" runat="server" Text="From Date" CssClass="RadioButtonListCssClass"></asp:Label>--%>
                        <asp:TextBox ID="txtFromDate" runat="server" placeholder="From Date"></asp:TextBox>
                        <%--<asp:Label ID="Label2" runat="server" Text="To Date" CssClass="RadioButtonListCssClass"></asp:Label>--%>
                        <asp:TextBox ID="txtToDate" runat="server" placeholder="To Date"></asp:TextBox>
                    </asp:Panel>
                    <asp:Panel CssClass="div-halfwidth" runat="server" ID="pnlChoiceSelect" Width="470px">
                        <asp:DropDownList ID="ddlChoice" runat="server" Width="450px">
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:Button ID="cmdGenerateReport" runat="server" Text="Filter" 
                    CssClass="g-button g-button-submit" />
                </div>
                

            
        </div>
    </div>
                    
    </form>
</body>
</html>
