﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident
{
    public partial class _1_IncidentReporting : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        int count = 0;
        int srno;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            #region Incident Details

            string sIncdentDetailsQuery = "";
            if (string.IsNullOrEmpty(Request["CurrentDate"]))
            {
                DateTime dtCurrentDate = System.DateTime.Today;
                txtCurrentDate.Value = dtCurrentDate.ToString("dd-MM-yyyy");
                sIncdentDetailsQuery = "SELECT [O_INDX_NMBR],[CY_TYPE_OF_INCIDENT],[O_INCDNT_DESCRPTN], [O_INCDNT_DT], [O_INCDNT_TIME], [CY_STATUS_LEVEL],[O_ENG_NM],[O_ENG_DSG] FROM [CY_INC_1_DETAILS] WHERE [CY_STATUS_LEVEL] != '10' AND [O_INCDNT_DT] >= DATEADD(DAY,-6,GETDATE()) ORDER BY [O_INCDNT_DT] DESC, [O_INCDNT_TIME] DESC";
            }
            else
            {
                string sCurrentDate = Request["CurrentDate"];
                txtCurrentDate.Value = sCurrentDate;
                sIncdentDetailsQuery = "SELECT [O_INDX_NMBR],[CY_TYPE_OF_INCIDENT],[O_INCDNT_DESCRPTN], [O_INCDNT_DT], [O_INCDNT_TIME], [CY_STATUS_LEVEL],[O_ENG_NM],[O_ENG_DSG] FROM [CY_INC_1_DETAILS] WHERE [CY_STATUS_LEVEL] != '10' AND [O_INCDNT_DT] >= '" + My.ConvertDateStringintoSQLDateString(sCurrentDate) + "' ORDER BY [O_INCDNT_DT] DESC, [O_INCDNT_TIME] DESC";
            }



            DataSet dsIncdentDetails = My.ExecuteSELECTQuery(sIncdentDetailsQuery);
            if (dsIncdentDetails.Tables[0].Rows.Count == 0)
            {
                divTableData.InnerHtml = "<h1 style=\"text-align:center;\">No Incident Reported yet</h1>";
            }
            else
            {
                string sIncdentTableData = "";
                for (int i = 0; i < dsIncdentDetails.Tables[0].Rows.Count; i++)
                {
                    srno = i + 1;
                    string sIncdentStatus = dsIncdentDetails.Tables[0].Rows[i][5].ToString();
                    if (sIncdentStatus == "7" || sIncdentStatus == "10")
                    {

                    }
                    else
                    {
                        count = count + 1;
                        string sIncdentID = dsIncdentDetails.Tables[0].Rows[i][0].ToString();
                        string sType = dsIncdentDetails.Tables[0].Rows[i][1].ToString();
                        string sName = dsIncdentDetails.Tables[0].Rows[i][6].ToString();
                        string sDesig = dsIncdentDetails.Tables[0].Rows[i][7].ToString();
                        string sIncdentDescription = dsIncdentDetails.Tables[0].Rows[i][2].ToString();
                        DateTime sIncdentDate = System.Convert.ToDateTime(dsIncdentDetails.Tables[0].Rows[i][3]);
                        string sIncdentTime = dsIncdentDetails.Tables[0].Rows[i][4].ToString();
                        sIncdentTableData = sIncdentTableData + "<tr>";
                        sIncdentTableData = sIncdentTableData + "<td>" + srno + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sName + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sDesig + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sType + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDate.ToString("dd-MM-yyyy") + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sIncdentTime + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDescription + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + My.getStatusStringFromLevel(sIncdentStatus) + "</td>";
                        if (sIncdentStatus == "1")
                        {

                        }
                        else
                        {
                            sIncdentTableData = sIncdentTableData + "<td><a IncdID=\"" + sIncdentID + "\" class=\"IncdDtlFancyBox\" href=\"fancybox/IncidentDetails.aspx?IncdID=" + sIncdentID + "&level=1\">View Details</a></td>";
                        }
                        sIncdentTableData = sIncdentTableData + "</tr>";
                    }

                }
                if (count > 0)
                {
                    string sTableHeading = "<tr><th>Sr.No</th><th>Name</th><th>Designation</th><th>Type</th><th style='width:60px;'>Date</th><th>Time</th><th>Description</th><th colspan=\"2\">Status</th></tr>";
                    divTableData.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading + sIncdentTableData + "</table>";
                }
                else
                {
                    divTableData.InnerHtml = "<h2>You have not any Incident or Near Miss</h2>";
                }
            }
            #endregion
        }
    }
}