﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="1a_NearMiss.aspx.cs" Inherits="ONGCUIProjects.Incident._1a_NearMiss" %>

<script type="text/javascript">
    $(document).ready(function () {
        var param1 = new Date();
        var param2 = param1.getDate() + '-' + (param1.getMonth() + 1) + '-' + param1.getFullYear();
        var param3 = param1.getHours() + ':' + param1.getMinutes() + ':' + param1.getSeconds();
        $("#curtdate").val(param2);
        $("#curttime").val(param3);

        $("#txtNearMissDate").datetimepicker({
            dateFormat: 'dd-mm-yy',
            altField: "#txtNearMissTime",
            maxDate: 0
        });

        $("#txtNearMissArea").keypress(function () {
            $("#txtNearMissLocation").val("");
            $("#txtNearMissDept").val("");
        });

        $("#txtNearMissArea").bind('keyup', function (e) {
            $("#txtNearMissArea").val(($("#txtNearMissArea").val()).toLowerCase());
        });

        var NearMissAreaSearch_options, NearMissAreaSearch_a;
        jQuery(function () {
            var options = {
                serviceUrl: '../services/auto.aspx',
                onSelect: NearMissAreaSearch_onAutocompleteSelect,
                deferRequestBy: 0, //miliseconds
                params: { type: 'area_name', limit: '10' },
                noCache: true //set to true, to disable caching
            };
            NearMissAreaSearch_a = $("#txtNearMissArea").autocomplete(options);
        });

        var NearMissAreaSearch_onAutocompleteSelect = function (NearMissAreaSearch_value, NearMissAreaSearch_data) {
            if (NearMissAreaSearch_value == "Enter something else..") {
                $("#txtNearMissArea").val("");
                $("#txtNearMissDept").val("");
                $("#txtNearMissArea").focus();
            } else {
                $("#txtNearMissLocation").val("");
                $("#txtNearMissLocation").focus();
            }
        }
        $("#txtNearMissLocation").bind('keyup', function (e) {
            $("#txtNearMissLocation").val(($("#txtNearMissLocation").val()).toLowerCase());
        });
        //------------------------------- new autocomplete for location ----------------------//
        //---Author:Shiv Shakti,Date:07/04/2015-----------------------------------------------//
//        $("#txtNearMissLocation").autocomplete({
//            source: function (request, response) {
//                $.ajax({
//                    url: '../services/auto.aspx/GetAutoCompleteDetail',
//                    data: { prefix: '" + request.term + "',type: 'area_location_name', limit: '10', extra_data: sLocationArea },
//                    dataType: "json",
//                    type: "POST",
//                    contentType: "application/json; charset=utf-8",
//                    success: function (data) {
//                        response($.map(data.d, function (item) {
//                            return {
//                                label: item.split('-')[0],
//                                val: item.split('-')[1]
//                            }
//                        }))
//                    },
//                    error: function (response) {
//                        alert(response.responseText);
//                    },
//                    failure: function (response) {
//                        alert(response.responseText);
//                    }
//                });
//            },
//            select: function (e, i) {
//                alert(i.item.val);
//                //   $("#txtNearMissLocation").val(i.item.val);
//            },
//            minLength: 1
//        });
        //--------------------------------------------------------------------------------------------//
                $("#txtNearMissLocation").keypress(function () {
                    $("#txtNearMissDept").val("");
                    var sLocationArea = $("#txtNearMissArea").val().trim();
                    if (sLocationArea == "") {
                        alert("Please search and select a location");
                        $("#txtNearMissArea").val("");
                    } else {
                        var NearMissLocationSearch_options, NearMissLocationSearch_a;
                        jQuery(function () {
                            var options = {
                                serviceUrl: '../services/auto.aspx',
                                onSelect: NearMissLocationSearch_onAutocompleteSelect,
                                deferRequestBy: 0, //miliseconds
                                params: { type: 'area_location_name', limit: '10', extra_data: sLocationArea },
                                noCache: true //set to true, to disable caching
                            };
                            NearMissLocationSearch_a = $("#txtNearMissLocation").autocomplete(options);
                        });
                        var sLocationArea = "";
                    }
                });
         var NearMissLocationSearch_onAutocompleteSelect = function (NearMissLocationSearch_value, NearMissLocationSearch_data) {
             if (NearMissLocationSearch_value == "Enter something else..") {

                $("#txtNearMissLocation").val("");
                $("#txtNearMissLocation").focus();
            }
            else {
                if (NearMissLocationSearch_data == "undefined") {
                    NearMissLocationSearch_data = "";
                }
                $("#txtNearMissDept").val(NearMissLocationSearch_data);
                $("#txtNearMissDesc").focus();
            }
            $(".autocomplete").hide();
        }

        $("input[name=rushedtohospital]").change(function () {
            if ($(this).val() == "Y") {
                alert("You have choosen wrong form please select 'Report New Incident'");
                $('input[name="rushedtohospital"][value="N"]').prop('checked', true);

            }
        });

        $("#cmdSubmitRpt").click(function () {
            var sFormComplete = "TRUE";
            var sRptEngDesig = $("#txtRptEngDesig").val().trim();
            var sRushedToHospital = $("input[name=rushedtohospital]:checked").val().trim();
            var sNearMissDept = $("#txtNearMissDept").val().trim();
            var sRptEngCPF = "";
            if ($("#txtRptEngCPF").val().trim() == "") {
                $("#txtRptEngCPF").css("border-color", "red");
                sFormComplete = "FALSE";
            } else {
                sRptEngCPF = $("#txtRptEngCPF").val().trim();
            }
            var sRptEngName = "";
            if ($("#txtRptEngName").val().trim() == "") {
                $("#txtRptEngName").css("border-color", "red");
                sFormComplete = "FALSE";
            } else {
                sRptEngName = $("#txtRptEngName").val().trim();
            }
            var sNearMissDate = "";
            if ($("#txtNearMissDate").val().trim() == "") {
                $("#txtNearMissDate").css("border-color", "red");
                sFormComplete = "FALSE";
            } else {
                sNearMissDate = $("#txtNearMissDate").val().trim();
            }

            var sNearMissTime = "";
            if ($("#txtNearMissTime").val().trim() == "") {
                $("#txtNearMissTime").css("border-color", "red");
                sFormComplete = "FALSE";
            }
            else {

                sNearMissTime = $("#txtNearMissTime").val().trim();
            }
            var sNearMissArea = "";
            if ($("#txtNearMissArea").val().trim() == "") {
                $("#txtNearMissArea").css("border-color", "red");
                sFormComplete = "FALSE";
            } else {
                sNearMissArea = $("#txtNearMissArea").val().trim();
            }
            var sNearMissLocation = "";
            if ($("#txtNearMissLocation").val().trim() == "") {
                $("#txtNearMissLocation").css("border-color", "red");
                sFormComplete = "FALSE";
            } else {
                sNearMissLocation = $("#txtNearMissLocation").val().trim();
            }
            var sNearMissDesc = "";
            if ($("#txtNearMissDesc").val().trim() == "") {
                $("#txtNearMissDesc").css("border-color", "red");
                sFormComplete = "FALSE";
            } else {
                sNearMissDesc = $("#txtNearMissDesc").val().trim();
            }

            // & replace by !-! 
            //  sNearMissDesc = sNearMissDesc.toString().replace("&", "!-!");
            alert(encodeURIComponent(sNearMissDesc));
            if (sFormComplete == "FALSE") {
                alert("Please fill all fields...!");
            } else {
                $("#response").text("Saving.....");
                // alert("type=NEARMISS&RptEngCPF=" + sRptEngCPF + "&RptEngName=" + sRptEngName + "&RptEngDesig=" + sRptEngDesig + "&NearMissDesc=" + sNearMissDesc + "&NearMissDate=" + sNearMissDate + "&NearMissTime=" + sNearMissTime + "&NearMissLocation=" + sNearMissLocation + "&NearMissArea=" + sNearMissArea + "&NearMissDept=" + sNearMissDept + "&RushToEmg=" + sRushedToHospital);
                $.ajax({
                    type: "POST",
                    url: "service/1_ReportSave.aspx",
                    data: "type=NEARMISS&RptEngCPF=" + sRptEngCPF + "&RptEngName=" + sRptEngName + "&RptEngDesig=" + sRptEngDesig + "&NearMissDesc=" + encodeURIComponent(sNearMissDesc) + "&NearMissDate=" + sNearMissDate + "&NearMissTime=" + sNearMissTime + "&NearMissLocation=" + sNearMissLocation + "&NearMissArea=" + sNearMissArea + "&NearMissDept=" + sNearMissDept + "&RushToEmg=" + sRushedToHospital + "&curntdate=" + param2 + "&curnttime=" + param3,
                    success: function (msg) {
                        var msg_arr = msg.split("~");
                        if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                            $("#response").text(msg_arr[2]);
                            $("#cmdSubmitRpt").hide();
                        } else {
                            alert(msg_arr[2]);
                        }
                    }
                });
            }
        });

        $("#cmdReset").click(function () {
            window.location = self.location;
            $.fancybox.close();
        });

        $(".CheckValidation").focusin(function () {
            if ($(this).css("border-color", "red")) {
                $(this).css("border-color", "");
            }
        });

    });
</script>
<form id="form1" runat="server">
<div id="ReportNewNearMiss" style="width: 708px;">
    <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">
        Report New Near Miss <span style="color: Red; float: right; font-size: 15px;">* Mandatory
            Fields</span></h1>
    <div class="div-fullwidth marginbottom">
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Reporter Name</p>
            <asp:textbox class="CheckValidation" id="txtRptEngName" runat="server" readonly="True"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto; margin: 0 8px;">
            <p class="content">
                CPF No</p>
            <asp:textbox class="CheckValidation" id="txtRptEngCPF" runat="server" readonly="True"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Desig</p>
            <asp:textbox id="txtRptEngDesig" runat="server" readonly="True"></asp:textbox>
        </div>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Rushed to Hospital</p>
        <input style="position: relative; float: left; margin: 6px 5px 0 0;" id="rushedtohospital_yes"
            type="radio" name="rushedtohospital" value="Y" />
        <label class="content" for="rushedtohospital_yes">
            Yes</label>
        <input style="position: relative; float: left; margin: 6px 5px 0 0;" id="rushedtohospital_no"
            type="radio" name="rushedtohospital" value="N" checked />
        <label class="content" for="rushedtohospital_no">
            No</label>
    </div>
    <div class="div-fullwidth marginbottom">
        <div class="div-halfwidth" style="width: 48%;">
            <p class="content">
                Current Date<span style="color: Red;">*</span></p>
            <asp:textbox class="CheckValidation" id="curtdate" runat="server" value="" style="width: 225px;"
                enabled="False" readonly="True"></asp:textbox>
        </div>
        <div class="div-halfwidth" style="width: 48%; margin: 0 5px;">
            <p class="content">
                Current Time<span style="color: Red;">*</span></p>
            <asp:textbox class="CheckValidation" id="curttime" runat="server" value="" style="width: 225px;"
                enabled="False" readonly="True"></asp:textbox>
        </div>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Near Miss Date & Time(24-hour)<span style="color: Red;">*</span></p>
        <asp:textbox class="CheckValidation" id="txtNearMissDate" runat="server"></asp:textbox>
        <asp:textbox class="CheckValidation" id="txtNearMissTime" runat="server"></asp:textbox>
    </div>
    <div class="div-fullwidth marginbottom">
        <div class="div-halfwidth" style="width: 48%;">
            <p class="content">
                Area<span style="color: Red;">*</span></p>
            <asp:textbox class="CheckValidation" id="txtNearMissArea" runat="server" value=""
                style="width: 225px;"></asp:textbox>
        </div>
        <div class="div-halfwidth" style="width: 48%; margin: 0 10px;">
            <p class="content">
                Location<span style="color: Red;">*</span></p>
            <asp:textbox class="CheckValidation" id="txtNearMissLocation" runat="server" value=""
                style="width: 225px;"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto; display: none;">
            <p class="content">
                Department</p>
            <asp:textbox class="CheckValidation" id="txtNearMissDept" runat="server" readonly="True"
                value=""></asp:textbox>
        </div>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Near Miss Description<span style="color: Red;">*</span></p>
        <asp:textbox class="CheckValidation" id="txtNearMissDesc" runat="server" textmode="MultiLine"
            style="max-height: 100px; min-height: 100px; max-width: 528px; min-width: 528px;"></asp:textbox>
    </div>
    <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">
    </h1>
    <div class="div-fullwidth">
        <p id="response" class="content" style="width: auto; color: Red;">
        </p>
        <input id="cmdReset" type="button" value="CLOSE" class="g-button g-button-red" style="float: right;" />
        <input id="cmdSubmitRpt" type="button" value="SAVE" class="g-button g-button-submit"
            style="float: right; margin-right: 10px;" />
    </div>
</div>
</form>
