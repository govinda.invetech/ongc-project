﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="1_IncidentReporting.aspx.cs"
    Inherits="ONGCUIProjects.Incident._1_IncidentReporting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="../css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="../css/css3.css" media="screen" type="text/css" />
    <link href="../css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" src="../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="../js/themes/smoothness/jquery-ui-1.8.22.custom.css"
        type="text/css" media="screen">
    <script type="text/javascript" src="../js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/jquery.gritter.css">
    <script type="text/javascript" src="../js/jquery.blockUI.js"></script>
    <script src="../js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.cmdReportingType').fancybox({
                modal: true
            });
            $('.cmdDOReportingType').fancybox({
                modal: true
            });
            $('.IncdDtlFancyBox').fancybox();

            $("#txtCurrentDate").datepicker({
                dateFormat: 'dd-mm-yy'
            });
            $('#cmdViewTodayReports').click(function () {
                var sCurrentDate = $('#txtCurrentDate').val().trim();
                if (sCurrentDate == "") {
                    alert('Please select a date');
                } else {
                    window.location = "1_IncidentReporting.aspx?CurrentDate=" + sCurrentDate;
                }
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                Incident Details</h1>
            <%-- <a class="g-button g-button-red" style="float:right; margin-right:10px; margin-top:5px; text-decoration:none;" id="cmdClosedIncident" href="javascript:void(0);" >CLOSED</a>--%>
            <a class="g-button g-button-red" style="float: right; margin-right: 10px; margin-top: 5px;
                text-decoration: none;" id="cmdClosedIncident" href="7_Closed_Incident.aspx?type=Reporting">
                CLOSED</a> 
                <a class="g-button g-button-submit cmdDOReportingType" style="float: right; margin-right: 10px;
                    margin-top: 5px; text-decoration: none;"  href="fancybox/1d_DOReport.aspx">D O REPORT</a>
                    
                     <a class="g-button g-button-submit "
                        style="float: right; margin-right: 10px; margin-top: 5px; text-decoration: none;"
                        href="#">P I REPORT</a> 

                        <a class="g-button g-button-submit cmdReportingType" style="float: right;
                            margin-right: 10px; margin-top: 5px; text-decoration: none;" href="fancybox/1b_Incident.aspx">
                            INCIDENT</a>

                             <a class="g-button g-button-submit cmdReportingType" style="float: right;
                                margin-right: 10px; margin-top: 5px; text-decoration: none;" href="fancybox/1a_NearMiss.aspx">
                                NEAR MISS</a>
        </div>
        <div class="div-pagebottom">
        </div>
        <div class="div-fullwidth marginbottom" style='border-bottom:2px solid black; padding-bottom:3px;'>
            <div style='position: relative; float: right;'>
                <p class='content'>
                    Date</p>
                <input runat="server" type='text' id='txtCurrentDate' style="position: relative;
                    float: left;" />
                <input runat="server" type="button" id='cmdViewTodayReports' style="position: relative;
                    float: left; margin-left: 10px;" class='g-button g-button-submit' value='GO' />
            </div>
        </div>
        <div runat="server" id="divTableData" class="div-fullwidth" style="overflow:scroll;">
        </div>
    </div>
    </form>
</body>
</html>
