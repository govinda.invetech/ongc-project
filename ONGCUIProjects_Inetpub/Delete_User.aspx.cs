using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ONGCCustumEntity;
using ONGCBusinessProjects;

namespace ONGCUIProjects
{
	/// <summary>
	/// Summary description for Delete_User.
	/// </summary>
	public partial class Delete_User : System.Web.UI.Page
	{
		#region Variable

		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		string ReadExecp;
		string EString;
		#endregion
	
		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			btnDeleteEmployee.Attributes.Add("onclick","return ConfirmDelete()");
			//txtCPF.Text = "";
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Delete User
		protected void btnDeleteEmployee_Click(object sender, System.EventArgs e)
		{
			try
			{
				ReadExecp = txttest.Text;

				// Delete from Employee Master
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("Delete From O_EMP_MSTR where O_CPF_NMBR = '" +txttest.Text+ "'",sqlConnection);
				ReadExecp = "Delete From O_EMP_MSTR where O_CPF_NMBR = '" +txttest.Text+ "'";
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				sqlDataReader.Close();
				sqlConnection.Close();
				
				// Delete From USer Master
				SqlConnection sqlConnection1=new SqlConnection(strConn);
				sqlConnection1.Open();
				SqlCommand sqlCommand1=new SqlCommand("Delete From E_USER_MSTR where E_USER_CODE = '" +txttest.Text+ "'",sqlConnection1);
				SqlDataReader sqlDataReader1=sqlCommand1.ExecuteReader();
				sqlDataReader.Close();
				sqlConnection.Close();

				Response.Write("<Script language='JavaScript'>"+"alert('Employee Deleted Sucessfully')"+"</Script>");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
			txttest.Text = "";		
		}
		#endregion
	}
}
