﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects
{
    public partial class SetPrivilege : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();

        private string getChildMenu(int id, string sCPF)
        {
            string sStr_2 = "SELECT id, priority, tool_tip_content, caption, child_code, mandatory, default_checked FROM CY_CHILD_MENU WHERE (menu_type = 'MENU' OR menu_type = 'PRIVILEGE') AND main_menu_id = '" + id + "' ORDER BY priority";
            DataSet dsChildMenu = my.ExecuteSELECTQuery(sStr_2);
            string returnString2 = "";
            for (int i = 0; i < dsChildMenu.Tables[0].Rows.Count; i++)
            {
                int ChildMenuId = System.Convert.ToInt32(dsChildMenu.Tables[0].Rows[i][0].ToString());
                string tooltipcon = dsChildMenu.Tables[0].Rows[i][2].ToString();
                string caption = dsChildMenu.Tables[0].Rows[i][3].ToString();
                string mandatory = dsChildMenu.Tables[0].Rows[i][5].ToString();
                string default_chk = dsChildMenu.Tables[0].Rows[i][6].ToString();
                if (default_chk != "TRUE")
                {
                    string PriAssigned = "";
                    if (PrivilegeToEmp(sCPF, ChildMenuId) == true)
                    {
                        PriAssigned = "checked";
                    }
                    returnString2 = returnString2 + "<div class=\"module_list module_list_css\"><img src=\"Images/bullet-orange.png\" style=\"float: left; margin-top: -1px;\" /><input id='" + ChildMenuId + "' class=\"inputPriviledge\" style=\"position: relative; float: left;margin-top: 3px;margin-left: 5px;margin-right: 5px;border-color: #34343B;\" type=\"CheckBox\" " + PriAssigned + "/><label style=\"position: relative; float: left; font-family: verdana; font-size: 12px; width: 89%; cursor: pointer; margin-top: 2px;\" for='" + ChildMenuId + "' >" + caption + "</label></div>";
                }
            }
            return returnString2;
        }

        private Boolean PrivilegeToEmp(string sCPF, int childmenuid)
        {
            string sStr_3 = "SELECT child_id FROM CY_MENU_EMP_RELATION WHERE cpf_number ='" + sCPF + "' AND child_id ='" + childmenuid + "'";
            DataSet dsPriEmp = my.ExecuteSELECTQuery(sStr_3);
            if (dsPriEmp.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string sCPF = Request.QueryString["cpf_no"];
            string sStr_1 = "SELECT id,priority,tool_tip_content,caption FROM CY_MAIN_MENU  ORDER BY priority";
            DataSet dsMainMenu = my.ExecuteSELECTQuery(sStr_1);
            string returnString = "";
            for (int i = 0; i < dsMainMenu.Tables[0].Rows.Count; i++)
            {
                int MainMenuId = System.Convert.ToInt32(dsMainMenu.Tables[0].Rows[i][0].ToString());
                string MainMenuPriority = dsMainMenu.Tables[0].Rows[i][1].ToString();
                string MainMenuTTC = dsMainMenu.Tables[0].Rows[i][2].ToString();
                string MainMenuCaption = dsMainMenu.Tables[0].Rows[i][3].ToString();
                string ChildMenu = getChildMenu(MainMenuId, sCPF);
                if (ChildMenu != "")
                {
                    returnString = returnString + "<div class=\"div_class\"><p class=\"main_menu_css\">" + MainMenuCaption + "</p><div class=\"child_css\">" + ChildMenu + "</div></div>";
                }
            }
            privilage_list.InnerHtml = returnString;
            save_privilage.Attributes.Add("CurrentCPFNo", sCPF);
        }
    }
}