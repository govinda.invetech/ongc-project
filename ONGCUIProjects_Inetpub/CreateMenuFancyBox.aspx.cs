﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;


namespace ONGCUIProjects
{
    public partial class CreateMenuFancyBox : System.Web.UI.Page
    {
        string MenuType = "";
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void CmdSaveMenuItem_Click2(object sender, EventArgs e)
        {
            string sCPFNo = Session["Login_Name"].ToString();
            string strOutput = "";

           

            string menu_title = txtTitle.Text;
            string sToolTip = txtToolTip.Text;
            string sHref = txtURL.Text;
            int iOrderBy = System.Convert.ToInt32(txtOrder.Text);
            string get_menu_id = getmenuid();
            string output = "";
            string sIsImp = "";
            string link_type = "";
            
            // file upload 

            if (OptDwld.Checked)
            {
                link_type = "DOWNLOAD";
                if (FileUploadControl.HasFile)
                {
                    try
                    {
                        string sFileName = System.IO.Path.GetFileName(FileUploadControl.FileName);
                        string sFileExt = System.IO.Path.GetExtension(FileUploadControl.FileName);
                        sFileName = sFileName.Replace(" ", "_");
                        if (!Directory.Exists(Server.MapPath("~/uploadedlink/" + get_menu_id)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/uploadedlink/" + get_menu_id));
                        }
                        string setlocation = Server.MapPath("~/uploadedlink/" + get_menu_id) + "\\" + sFileName;
                        FileUploadControl.SaveAs(setlocation);

                        if (sFileExt == ".zip")
                        {
                            sHref = "uploadedlink/" + get_menu_id + "/index.html";
                            File.Delete(setlocation);
                        }
                        else
                        {
                            sHref = "uploadedlink/" + get_menu_id + "/" + sFileName;
                        }
                        output = "file uploaded successfully";
                    }
                    catch (Exception a)
                    {
                        output = a.Message;
                    }
                }
                else
                {
                    output = "Please select A file";
                }
            }
            try
            {
                string sUpdate = "UPDATE [cy_launch_page_menu] SET [order_by]=[order_by]+1";
                sUpdate = sUpdate + " WHERE [order_by]>" + (iOrderBy - 1) + " AND [menu_type] ='" + MenuType + "';";
                if (My.ExecuteSQLQuery(sUpdate) == true)
                {
                    string sInsert = "INSERT into [cy_launch_page_menu] ([id],[menu_title],[tooltip],[href],[menu_type],[order_by],[is_important],[entry_by],[link_type])  VALUES ";
                    sInsert = sInsert + "('" + get_menu_id + "','" + menu_title + "','" + sToolTip + "','" + sHref + "','" + MenuType + "','" + iOrderBy + "','" + sIsImp + "','" + sCPFNo + "','" + link_type + "')";
                    if (My.ExecuteSQLQuery(sInsert) == true)
                    {
                        output = "Menu item saved successfully...!";
                    }
                }
                else //else case of execute update query
                {
                    output = "MyApp oocured error ! ERR CODE : *LP187";
                }
            }
            catch (Exception a)
            {
                output = a.Message;
            } //  try catch ends   
            strOutput = "<Script language='JavaScript'>alert('" + output + "');window.location='LaunchPageCtrlPnl.aspx'; </Script>";
            ClientScript.RegisterStartupScript(typeof(Page), "alert", strOutput);
        }
        
        public string getmenuid()
        {
            string my_str = "SELECT  [id] FROM [cy_launch_page_menu] order by [id] DESC";
            DataSet ds = My.ExecuteSELECTQuery(my_str);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "0";
            }
            else
            {
                return (int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1).ToString();
            }
        }




    }
}