using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace ONGCUIProjects
{
	public partial class Guest_House_Booking_App : System.Web.UI.Page
	{
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
		#region PageLoad
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {

                string cpf_no = Session["Login_Name"].ToString();

                #region Pending Request
                string sQuery = "SELECT id, BOOKING_TYPE, BOOKING_START_DATETIME, BOOKING_END_DATETIME, ";
                sQuery += "INCHARGE_CPF_NO, INCHARGE_NAME, INCHARGE_STATUS, INCHARGE_TIMESTAMP, ";
                sQuery += "APPROVER_CPF_NO, APPROVER_NAME, APPROVER_STATUS, APPROVER_TIMESTAMP, ";
                sQuery += "ALLOCATER_CPF_NO, ALLOCATER_NAME, ALLOCATTER_STATUS, ALLOCATTER_TIMESTAMP,TIMESTAMP, [APP_CPF_NO],[APP_NAME] ";
                sQuery += "FROM CY_GUEST_HOUSE_BOOKING_DETAIL ";
                sQuery += "WHERE ([INCHARGE_CPF_NO]='" + cpf_no + "' OR ";
                sQuery += "[APPROVER_CPF_NO] ='" + cpf_no + "' OR ";
                sQuery += "[ALLOCATER_CPF_NO] ='" + cpf_no + "') ORDER BY TIMESTAMP DESC";

                DataSet dsBookingRequest = My.ExecuteSELECTQuery(sQuery);

                int count = 1;
                if (dsBookingRequest.Tables[0].Rows.Count == 0)
                {
                    P1.InnerHtml = "No Request Pending";
                }
                else
                {
                    string output = "";
                    output += "<table border=\"1\" style=\"margin-left: 10px; margin-top: 10px; margin-right: 10px; width: 98%; height: 85px;\" class=\"tftable\" id=\"tfhover\">";
                    output += "<tbody>";
                    output += "<tr>";
                    output += "<th>Type</th>";
                    output += "<th>Applicant Name</th>";
                    output += "<th>Requested On</th>";
                    output += "<th>Booking From</th>";
                    //output += "<th>Booking To</th>";
                    output += "<th>Incharge</th>";
                    output += "<th>Approver</th>";
                    output += "<th>Alloter</th>";
                    output += "<th>Action</th>";
                    output += "</tr>";
                    for (int i = 0; i < dsBookingRequest.Tables[0].Rows.Count; i++)
                    {
                        //Booking Details
                        string sBookingID = dsBookingRequest.Tables[0].Rows[i][0].ToString();
                        string sBookingType = dsBookingRequest.Tables[0].Rows[i][1].ToString();
                        DateTime dtBookingStartDate = Convert.ToDateTime(dsBookingRequest.Tables[0].Rows[i][2]);
                        DateTime dtBookingEndDate = Convert.ToDateTime(dsBookingRequest.Tables[0].Rows[i][3]);


                        //Incharge Detail
                        string sInchargeCPF = dsBookingRequest.Tables[0].Rows[i][4].ToString();
                        string sInchargeName = dsBookingRequest.Tables[0].Rows[i][5].ToString();
                        string sInchargeStatus = dsBookingRequest.Tables[0].Rows[i][6].ToString();
                        string sInchargeTimestamp = dsBookingRequest.Tables[0].Rows[i][7].ToString();
                        string incharge_image_tooltip = "";
                        if (sInchargeStatus == "APPROVE")
                        {
                            DateTime dtInchargeTimestamp = Convert.ToDateTime(sInchargeTimestamp);
                            incharge_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                            incharge_image_tooltip += "<img src=\"Images/tick.png\"/>";
                            incharge_image_tooltip += "<span class=\"info\">";
                            incharge_image_tooltip += "Incharge Name : <b>" + sInchargeName + "(" + sInchargeCPF + ")" + "</b><br/>";
                            incharge_image_tooltip += "Status : <b>Approved</b><br/>";
                            incharge_image_tooltip += "Approved on : <b>" + dtInchargeTimestamp.ToString("d MMM, yyyy") + "</b> at <b>" + dtInchargeTimestamp.ToString("HH:mm") + "</b>";
                            incharge_image_tooltip += "</span>";
                            incharge_image_tooltip += "</a>";
                        }
                        else if (sInchargeStatus == "REJECT")
                        {
                            DateTime dtInchargeTimestamp = Convert.ToDateTime(sInchargeTimestamp);
                            incharge_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                            incharge_image_tooltip += "<img src=\"Images/cross.png\"/>";
                            incharge_image_tooltip += "<span class=\"info\">";
                            incharge_image_tooltip += "Incharge Name : <b>" + sInchargeName + "(" + sInchargeCPF + ")" + "</b><br/>";
                            incharge_image_tooltip += "Status : <b>Rejected</b><br/>";
                            incharge_image_tooltip += "Rejected on : <b>" + dtInchargeTimestamp.ToString("d MMM, yyyy") + "</b> at <b>" + dtInchargeTimestamp.ToString("HH:mm") + "</b>";
                            incharge_image_tooltip += "</span>";
                            incharge_image_tooltip += "</a>";
                        }
                        else
                        {
                            incharge_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                            incharge_image_tooltip += "PENDING";
                            incharge_image_tooltip += "<span class=\"info\">";
                            incharge_image_tooltip += "Incharge Name : <b>" + sInchargeName + "(" + sInchargeCPF + ")" + "</b><br/>";
                            incharge_image_tooltip += "Status : <b>Pending</b><br/>";
                            incharge_image_tooltip += "</span>";
                            incharge_image_tooltip += "</a>";
                        }

                        //Approver Detail
                        string sApproverCPF = dsBookingRequest.Tables[0].Rows[i][8].ToString();
                        string sApproverName = dsBookingRequest.Tables[0].Rows[i][9].ToString();
                        string sApproverStatus = dsBookingRequest.Tables[0].Rows[i][10].ToString();
                        string sApproverTimestamp = dsBookingRequest.Tables[0].Rows[i][11].ToString();
                        string approver_image_tooltip = "";
                        if (sInchargeStatus == "REJECT")
                        {
                            approver_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                            approver_image_tooltip += "<img src=\"Images/cross.png\"/>";
                            approver_image_tooltip += "<span class=\"info\">";
                            approver_image_tooltip += "Approver Name : <b>" + sApproverName + "(" + sApproverCPF + ")" + "</b><br/>";
                            approver_image_tooltip += "Status : <b>Incharge has been rejected.</b>";
                            approver_image_tooltip += "</span>";
                            approver_image_tooltip += "</a>";
                        }
                        else
                        {
                            if (sApproverStatus == "APPROVE")
                            {
                                DateTime dtApproverTimestamp = Convert.ToDateTime(sApproverTimestamp);
                                approver_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                approver_image_tooltip += "<img src=\"Images/tick.png\"/>";
                                approver_image_tooltip += "<span class=\"info\">";
                                approver_image_tooltip += "Approver Name : <b>" + sApproverName + "(" + sApproverCPF + ")" + "</b><br/>";
                                approver_image_tooltip += "Status : <b>Approved</b><br/>";
                                approver_image_tooltip += "Approved on : <b>" + dtApproverTimestamp.ToString("d MMM, yyyy") + "</b> at <b>" + dtApproverTimestamp.ToString("HH:mm") + "</b>";
                                approver_image_tooltip += "</span>";
                                approver_image_tooltip += "</a>";
                            }
                            else if (sApproverStatus == "DIRECT")
                            {
                                approver_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                approver_image_tooltip += "<img src=\"Images/tick.png\"/>";
                                approver_image_tooltip += "<span class=\"info\">";
                                approver_image_tooltip += "Alloter Name : <b>" + sApproverName + "(" + sApproverCPF + ")" + "</b><br/>";
                                approver_image_tooltip += "Status : <b>Direct alloted by alloter.</b>";
                                approver_image_tooltip += "</span>";
                                approver_image_tooltip += "</a>";
                            }
                            else if (sApproverStatus == "REJECT")
                            {
                                DateTime dtApproverTimestamp = Convert.ToDateTime(sApproverTimestamp);
                                approver_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                approver_image_tooltip += "<img src=\"Images/cross.png\"/>";
                                approver_image_tooltip += "<span class=\"info\">";
                                approver_image_tooltip += "Approver Name : <b>" + sApproverName + "(" + sApproverCPF + ")" + "</b><br/>";
                                approver_image_tooltip += "Status : <b>Rejected</b><br/>";
                                approver_image_tooltip += "Rejected on : <b>" + dtApproverTimestamp.ToString("d MMM, yyyy") + "</b> at <b>" + dtApproverTimestamp.ToString("HH:mm") + "</b>";
                                approver_image_tooltip += "</span>";
                                approver_image_tooltip += "</a>";
                            }
                            else
                            {
                                approver_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                approver_image_tooltip += "PENDING";
                                approver_image_tooltip += "<span class=\"info\">";
                                approver_image_tooltip += "Approver Name : <b>" + sApproverName + "(" + sApproverCPF + ")" + "</b><br/>";
                                approver_image_tooltip += "Status : <b>Pending</b><br/>";
                                approver_image_tooltip += "</span>";
                                approver_image_tooltip += "</a>";
                            }
                        }

                        //Alloter Detail
                        string sAlloterCPF = dsBookingRequest.Tables[0].Rows[i][12].ToString();
                        string sAlloterName = dsBookingRequest.Tables[0].Rows[i][13].ToString();
                        string sAlloterStatus = dsBookingRequest.Tables[0].Rows[i][14].ToString();
                        string sAlloterTimestamp = dsBookingRequest.Tables[0].Rows[i][15].ToString();
                        string alloter_image_tooltip = "";
                        if (sInchargeStatus == "REJECT")
                        {
                            alloter_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                            alloter_image_tooltip += "<img src=\"Images/cross.png\"/>";
                            alloter_image_tooltip += "<span class=\"info\">";
                            alloter_image_tooltip += "Alloter Name : <b>" + sAlloterName + "(" + sAlloterCPF + ")" + "</b><br/>";
                            alloter_image_tooltip += "Status : <b>Incharge has been rejected.</b>";
                            alloter_image_tooltip += "</span>";
                            alloter_image_tooltip += "</a>";
                        }
                        else
                        {
                            if (sApproverStatus == "REJECT")
                            {
                                alloter_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                alloter_image_tooltip += "<img src=\"Images/cross.png\"/>";
                                alloter_image_tooltip += "<span class=\"info\">";
                                alloter_image_tooltip += "Alloter Name : <b>" + sAlloterName + "(" + sAlloterCPF + ")" + "</b><br/>";
                                alloter_image_tooltip += "Status : <b>Approver has been rejected.</b>";
                                alloter_image_tooltip += "</span>";
                                alloter_image_tooltip += "</a>";
                            }
                            else
                            {
                                if (sAlloterStatus == "APPROVE")
                                {
                                    DateTime dtAlloterTimestamp = Convert.ToDateTime(sAlloterTimestamp);
                                    alloter_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                    alloter_image_tooltip += "<img src=\"Images/tick.png\"/>";
                                    alloter_image_tooltip += "<span class=\"info\">";
                                    alloter_image_tooltip += "Alloter Name : <b>" + sAlloterName + "(" + sAlloterCPF + ")" + "</b><br/>";
                                    alloter_image_tooltip += "Status : <b>Approved</b><br/>";
                                    alloter_image_tooltip += "Approved on : <b>" + dtAlloterTimestamp.ToString("d MMM, yyyy") + "</b> at <b>" + dtAlloterTimestamp.ToString("HH:mm") + "</b>";
                                    alloter_image_tooltip += "</span>";
                                    alloter_image_tooltip += "</a>";
                                }
                                else if (sAlloterStatus == "REJECT")
                                {
                                    DateTime dtAlloterTimestamp = Convert.ToDateTime(sAlloterTimestamp);
                                    alloter_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                    alloter_image_tooltip += "<img src=\"Images/cross.png\"/>";
                                    alloter_image_tooltip += "<span class=\"info\">";
                                    alloter_image_tooltip += "Alloter Name : <b>" + sAlloterName + "(" + sAlloterCPF + ")" + "</b><br/>";
                                    alloter_image_tooltip += "Status : <b>Rejected</b><br/>";
                                    alloter_image_tooltip += "Rejected on : <b>" + dtAlloterTimestamp.ToString("d MMM, yyyy") + "</b> at <b>" + dtAlloterTimestamp.ToString("HH:mm") + "</b>";
                                    alloter_image_tooltip += "</span>";
                                    alloter_image_tooltip += "</a>";
                                }
                                else
                                {
                                    alloter_image_tooltip += "<a class=\"tooltip\" href=\"#\">";
                                    alloter_image_tooltip += "PENDING";
                                    alloter_image_tooltip += "<span class=\"info\">";
                                    alloter_image_tooltip += "Alloter Name : <b>" + sAlloterName + "(" + sAlloterCPF + ")" + "</b><br/>";
                                    alloter_image_tooltip += "Status : <b>Pending</b><br/>";
                                    alloter_image_tooltip += "</span>";
                                    alloter_image_tooltip += "</a>";
                                }
                            }
                        }

                        DateTime dtBookingDate = Convert.ToDateTime(dsBookingRequest.Tables[0].Rows[i][16]);
                        string sApplicantName = dsBookingRequest.Tables[0].Rows[i][17].ToString();
                        string sApplicantCPF = dsBookingRequest.Tables[0].Rows[i][18].ToString();

                        string Editable = "";
                        if (sAlloterStatus == "APPROVE")
                        {
                            Editable = "<a class='cmdApproveReject' href=\"services/guesthouse/print_GuestHouse.aspx?id=" + sBookingID + "\">View & Print</a>";
                        }
                        else
                        {
                            Editable = "<a class='cmdApproveReject' href=\"services/guesthouse/GuestHouseDetailToApprove.aspx?type=APPROVE~" + sBookingID + "\">View & Approve</a>";
                        }
                        //output += "<div style=\"padding: 10px; width: 97.5%;\" class=\"div-fullwidth marginbottom gatepass-approvalbox\">";
                        //output += "<h1 class=\"content-gatepass-approval count\">" + count + ".</h1>";
                        // output += "<div style=\"width: auto;\" class=\"div-fullwidth\">";
                        //output += "<div class=\"divider\"></div>";
                        //output += "</div>";
                      
                        var eDate = "-";
                        if (sBookingType != "Food")                        {
                           
                            eDate = dtBookingEndDate.ToString("d MMM, yyyy HH:mm");
                        }
                        output += "<tr>";
                        output += "<td>" + sBookingType + "</td>";
                        output += "<td>" + sApplicantName + "[" + sApplicantCPF + "]" + "</td>";
                        output += "<td>" + dtBookingStartDate.ToString("d MMM, yyyy HH:mm") + "</td>";
                        output += "<td>" + eDate + "</td>";
                        //output += "<td>" + dtBookingEndDate.ToString("d MMM, yyyy HH:mm") + "</td>";
                        output += "<td style=\"text-align:center\">" + incharge_image_tooltip + "</td>";
                        output += "<td style=\"text-align:center\">" + approver_image_tooltip + "</td>";
                        output += "<td style=\"text-align:center\">" + alloter_image_tooltip + "</td>";
                        if (Editable != "")
                        {
                            output += "<td style=\"text-align:center\">" + Editable + "</td>";
                        }
                        output += "</tr>";

                        //output += "</div>";
                        count++;
                    }
                    output += "</tbody>";
                    output += "</table>";
                    pening_detail_div.InnerHtml = output;
                }
                #endregion
            }
		}
		#endregion

	}
}
