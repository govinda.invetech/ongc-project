﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects
{
    public partial class ActivateRejectAccount : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            string cpf_no = Session["Login_Name"].ToString();

            if (cpf_no == "")
            {
                Response.Write("FALSE~ERR~Invalid CPF Number loggined");
                return;
            }
            #region Get Feedback Report
            try
            {
                string sStr1 = "SELECT LTRIM(RTRIM([O_EMP_MSTR].[O_EMP_NM])) AS [EMP_NAME] ,[O_EMP_MSTR].[O_CPF_NMBR] ,[O_EMP_MSTR].[O_EMP_DESGNTN] ,[E_USER_MSTR].[CY_STATUS] FROM [O_EMP_MSTR],[E_USER_MSTR] WHERE [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE] ORDER BY [EMP_NAME] ASC";
                DataSet dsActivation = new DataSet();
                dsActivation = My.ExecuteSELECTQuery(sStr1);

                if (dsActivation.Tables[0].Rows.Count == 0)
                {
                    DivActivationData.InnerHtml = "<p class=\"feedback-no-data\">No Pending Request</p>";
                }
                else
                {
                    string tableData = "";
                    tableData = tableData + "<table class='tftable'>";
                    tableData = tableData + "<thead style=\"background-color: #DADADA;\">";
                    tableData = tableData + "<tr>";
                    tableData = tableData + "<th style='white-space:nowrap'>Sl. No.</th>";
                    tableData = tableData + "<th style='white-space:nowrap'>CPF No</th>";
                    tableData = tableData + "<th style='white-space:nowrap'>Employee Name</th>";
                    tableData = tableData + "<th style='white-space:nowrap'>Designation</th>";
                    tableData = tableData + "<th style='text-align:center;width:175px;'>Action</th>";
                    tableData = tableData + "</tr>";
                    tableData = tableData + "</thead>";
                    tableData = tableData + "<tbody>";
                    for (int i = 0; i < dsActivation.Tables[0].Rows.Count; i++)
                    {
                        string sEmployeeName = dsActivation.Tables[0].Rows[i][0].ToString();
                        string sCPFNo = dsActivation.Tables[0].Rows[i][1].ToString();
                        string sDesignation = dsActivation.Tables[0].Rows[i][2].ToString();
                        string sStatus = dsActivation.Tables[0].Rows[i][3].ToString();

                        tableData += "<tr>";
                        tableData += "<td style=\"text-align: center;\">" + (i + 1) + "</td>";
                        tableData += "<td>" + sEmployeeName.ToUpper() + "</td>";
                        tableData += "<td>" + sCPFNo + "</td>";
                        tableData += "<td>" + sDesignation.ToUpper() + "</td>";
                        tableData += "<td style=\"text-align: center;\">";

                        switch (sStatus)
                        {
                            case "ACTIVE":
                                tableData += "<input type=\"button\" user=\"" + sCPFNo + "\" class=\"g-button g-button-red cmdAction\" value=\"DE-ACTIVATE\" ActionType='ADMINDEACTIVE' />";
                                break;
                            case "ADMINACTIVE":
                                tableData += "<input type=\"button\" user=\"" + sCPFNo + "\" class=\"g-button g-button-red cmdAction\" value=\"DE-ACTIVATE\" ActionType='ADMINDEACTIVE' />";
                                break;
                            case "REQUEST":
                                tableData += "<input type=\"button\" user=\"" + sCPFNo + "\" class=\"g-button g-button-share cmdAction\" style=\"width:80px;margin-right:10px;\" value=\"ACTIVATE\" ActionType='ADMINACTIVE' />";
                                tableData += "<input type=\"button\" user=\"" + sCPFNo + "\" class=\"g-button g-button-red cmdAction\" style=\"width:80px;\" value=\"REJECT\" ActionType=\"REJECT\" />";
                                break;
                            case "REJECT":
                                tableData += "Already Rejected";
                                break;
                            case "ADMINDEACTIVE":
                                tableData += "<input type=\"button\" user=\"" + sCPFNo + "\" class=\"g-button g-button-share cmdAction\" value=\"ACTIVATE\" ActionType='ADMINACTIVE' />";
                                break;
                        }

                        tableData += "</td>";
                        tableData += "</tr>";
                    }
                    tableData = tableData + "</tbody>";
                    tableData = tableData + "</table>";
                    DivActivationData.InnerHtml = tableData;
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends
            #endregion
        }
    }
}