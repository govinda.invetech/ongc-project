﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace ONGCUIProjects
{
    public partial class UpdateGallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"].ToString() != "admin")//authorization only for admin 
            {
                foradmin1.Visible = false;
                foradmin2.Visible = false;
                foradmin3.Visible = false;
            }


        }

        [System.Web.Services.WebMethod]
        public static string DeleteImage(string imageurl)
        {
            if (HttpContext.Current.Session["Login_Name"].ToString() == "admin")//authorization only for admin 
            {
                string imgPath = imageurl.Replace("/imgs", "");
                string mappath = HttpContext.Current.Server.MapPath("~/imgs");
                FileInfo info1 = new FileInfo(mappath + imgPath);
                try
                {
                    if (info1.Exists)
                    {
                        info1.Delete();
                        return "{\"status\":\"success\" ,\"message\":\"Image has been delete successfully !\"}";
                    }
                    else
                    {
                        return "{\"status\":\"notfound\" ,\"message\":\"Image can't be delete!\"}";
                    }
                }
                catch (Exception ex)
                {
                    return "{\"status\":\"error\" ,\"message\":\" " + ex.Message.ToString() + "!\"}";
                }
            }
            else
            {
                return "{\"status\":\"notadmin\" ,\"message\":\"Sorry you can't delete it!\"}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string DeleteImageFolder(string imageurl)
        {
            if (HttpContext.Current.Session["Login_Name"].ToString() == "admin")//authorization only for admin 
            {
                string imgPath = imageurl.Replace("/imgs", "");
                string mappath = HttpContext.Current.Server.MapPath("~/imgs/") + imgPath;
                FileInfo info1 = new FileInfo(mappath + imgPath);
                try
                {
                    if (Directory.Exists(mappath))
                    {
                        RemoveDirectories(mappath);
                        Directory.Delete(mappath);
                        return "{\"status\":\"success\" ,\"message\":\"folder has been delete successfully !\"}";
                    }
                    else
                    {
                        return "{\"status\":\"notfound\" ,\"message\":\"folder can't be delete!\"}";
                    }
                }
                catch (Exception ex)
                {
                    return "{\"status\":\"error\" ,\"message\":\" " + ex.Message.ToString() + "!\"}";
                }
            }
            else
            {
                return "{\"status\":\"notadmin\" ,\"message\":\"Sorry you can't delete it!\"}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetImagesFolder(string Type)
        {
            try
            {
                string JsonSting = "{\"folder\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~/imgs");
                string[] filPaths = Directory.GetDirectories(sOutputPath);
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath + "\\", "");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"folderName\":\"" + filPaths[i] + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetImagesFromFolder(string Folder)
        {
            try
            {
                string JsonSting = "{\"photo\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~/imgs/" + Folder + "");
                string[] filPaths = Directory.GetFiles(sOutputPath);
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath, "\\imgs\\" + Folder + "");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"baseUrl\":\"" + filPaths[i] + "\",\"title\":\"" + filPaths[i].Replace(".jpg", "").Replace(".png", "") + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;

            }
            catch
            {
                return "{}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetDocFromFolder(string Type)
        {
            try
            {
                string JsonSting = "{\"photo\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~");
                string[] filPaths = Directory.GetFiles(sOutputPath + "docs");
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath, "\\");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"baseUrl\":\"" + filPaths[i] + "\",\"title\":\"" + filPaths[i].Replace("/docs/", "").Replace(".jpg", "").Replace(".png", "") + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }

        }

        protected void btnUploadDoc_Click(object sender, EventArgs e)
        {
            Span1.Text = string.Empty;
            string filepath = "";
            if (Session["Login_Name"].ToString() == "admin")//authorization only for admin 
            {
                filepath = Server.MapPath("\\docS");
                HttpFileCollection uploadedFiles = Request.Files;
                for (int i = 0; i < uploadedFiles.Count; i++)
                {
                    HttpPostedFile userPostedFile = uploadedFiles[i];
                    try
                    {
                        if (userPostedFile.ContentLength > 0)
                        {
                            string fileExtension = Path.GetExtension(userPostedFile.FileName).ToUpper();
                            if (fileExtension == ".PDF" || fileExtension == ".XLS" || fileExtension == ".XLSX" || fileExtension == ".TXT" || fileExtension == ".DOCX")
                            {
                                userPostedFile.SaveAs(filepath + "\\" + Path.GetFileName(userPostedFile.FileName));
                                string filename = userPostedFile.FileName;
                                Response.Write("<script>alert('" + filename + " has  uploaded successfully');</script>");
                            }
                            else
                            {
                                Response.Write("<script>alert('Sorry! uploading document should be xls or xlsx or txt or docx!');</script>");
                                return;
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        Span1.Text += "Error: <br>" + Ex.Message;
                    }
                }

            }
            else
            {
                Response.Write("<script>alert('Sorry! only Admin has authorization of uploading!');</script>");
            }
        }
        public static bool createPath(string FilePath) //"files/Folder/SubFolder/example.extn"
        {
            try
            {
                string[] DirPathArr = FilePath.Split('/');
                string sOutputPath = HttpContext.Current.Server.MapPath("~");
                for (int i = 0; i < DirPathArr.Length; i++)
                {
                    if (i == DirPathArr.Length - 1)
                    {
                        sOutputPath += "/" + DirPathArr[i].ToString();
                        if (File.Exists(sOutputPath))
                        {
                            File.Delete(sOutputPath);
                        }
                    }
                    else
                    {
                        sOutputPath += "/" + DirPathArr[i].ToString();
                        if (!Directory.Exists(sOutputPath))
                        {
                            Directory.CreateDirectory(sOutputPath);
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (Session["Login_Name"].ToString() == "admin")//authorization only for admin 
            {
                Span1.Text = string.Empty;
                string filepath = "";
                Button btn = (Button)sender;
                string folderName = btn.Attributes["folder_name"];

                if (folderName == "new")
                {
                    try
                    {
                        filepath = Server.MapPath("\\imgs");
                        Directory.CreateDirectory(filepath + "\\" + txtFolderName.Text);
                    }
                    catch
                    {
                        Span1.Text = "There is some error plz try again!";
                        return;
                    }
                }
                filepath = Server.MapPath("\\imgs\\" + txtFolderName.Text + "");
                HttpFileCollection uploadedFiles = Request.Files;

                for (int i = 0; i < uploadedFiles.Count; i++)
                {
                    HttpPostedFile userPostedFile = uploadedFiles[i];
                    try
                    {
                        if (userPostedFile.ContentLength > 0)
                        {
                            //Span1.Text += "<u>File #" + (i + 1) +  "</u><br>";
                            //Span1.Text += "File Content Type: " +  userPostedFile.ContentType      + "<br>";
                            //Span1.Text += "File Size: " + userPostedFile.ContentLength           + "kb<br>";
                            //Span1.Text += "File Name: " + userPostedFile.FileName + "<br>";
                            string fileExtension = Path.GetExtension(userPostedFile.FileName).ToUpper();
                            if (fileExtension == ".JPG" || fileExtension == ".PNG" || fileExtension == ".JPEG" || fileExtension == ".TIF" || fileExtension == ".GIF")
                            {
                                userPostedFile.SaveAs(filepath + "\\" + Path.GetFileName(userPostedFile.FileName));

                            }
                            else
                            {
                                Response.Write("<script>alert('Sorry! uploading document should be jpg or png or jpeg or tif or gif!');</script>");
                                return;
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        Span1.Text += "Error: <br>" + Ex.Message;
                    }
                }
                Response.Write("<script>alert('Images Uploaded Successfully!');</script>");
            }
            else
            {
                Response.Write("<script>alert('Sorry! only Admin has authorization of uploading!');</script>");
            }
        }

        public static void RemoveDirectories(string strpath)
        {
            //This condition is used to delete all files from the Directory
            foreach (string file in Directory.GetFiles(strpath))
                File.Delete(file);
        }


    }
}