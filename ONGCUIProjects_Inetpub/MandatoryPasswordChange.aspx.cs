﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class MandatoryPasswordChange : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                string sUserID = Session["Login_Name"].ToString();

                if (!IsPostBack)
                {
                    txtOldPassword.Text = "";
                    txtNewPassword.Text = "";
                    txtReTypeNewPassword.Text = "";
                }
                else if (IsPostBack)
                {
                    string sOldPassword = txtOldPassword.Text.ToString();
                    string sNewPassword = txtNewPassword.Text.ToString();
                    string sReTypeNewPassword = txtReTypeNewPassword.Text.ToString();


                    if (sOldPassword.Trim() == "")
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('Old password field is blank.')" + "</Script>");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(sNewPassword) && sNewPassword.Length > 0)
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('New Password field does not allow blank or space.')" + "</Script>");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(sReTypeNewPassword) && sReTypeNewPassword.Length > 0)
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('Re-typed New Password field does not allow blank or space.')" + "</Script>");
                        return;
                    }


                    //Response.Write("<Script language='JavaScript'>" + "alert('Incident Description Cannot be left blank'" + sOldPassword + ")" + "</Script>");
                    if (checkcurrentpassword(sUserID, sOldPassword) == "FALSE")
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('Old password mismatch.')" + "</Script>");
                        return;
                    }
                    else
                    {
                        if (sNewPassword != sReTypeNewPassword)
                        {
                            Response.Write("<Script language='JavaScript'>" + "alert('New password and re-typed new password mismatch.')" + "</Script>");
                            return;
                        }
                        else
                        {
                            string str = "UPDATE [E_USER_MSTR] SET [E_USER_PSWRD] = '" + sNewPassword + "' WHERE  [E_USER_CODE] = '" + sUserID + "' ";
                            try
                            {
                                if (My.ExecuteSQLQuery(str))
                                {
                                    string QueryString = "INSERT INTO [CY_LOGIN_LOGS] ([USER_ID],[USER_PWD],[TYPE]) VALUES ('" + sUserID + "','" + sNewPassword + "','SUCCESS')";
                                    My.ExecuteSQLQuery(QueryString);
                                    Response.Write("<Script language='JavaScript'>" + "alert('Your password has been successfully changed. Please use your ID and New Password to login again.'); window.location='Index.aspx';" + "</Script>");
                                    return;
                                    //Response.Redirect("Index.aspx", false);
                                }
                                else
                                {
                                    txtMessage.Text = "Password Not Changed Successfully.";
                                    return;
                                }
                            }
                            catch (Exception a)
                            {
                                txtMessage.Text = a.Message + "ERR403PASSCHANGE";
                                return;
                            }
                        }

                    }
                }
            }
        }


        private string checkcurrentpassword(string UserID,string OldPassword)
        {
            string my_str = "SELECT [E_USER_CODE]  FROM [E_USER_MSTR] where [E_USER_CODE]='" + UserID + "' and [E_USER_PSWRD]='" + OldPassword + "'";
            DataSet ds = My.ExecuteSELECTQuery(my_str);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                return "TRUE";
            }
        }
    }
}