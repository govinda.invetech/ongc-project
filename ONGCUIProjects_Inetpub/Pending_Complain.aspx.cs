﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects
{
    public partial class Pending_Complain : System.Web.UI.Page
    {
        string Departments = "";
        string strip_type = "";
        MyApplication1 my = new MyApplication1();
        private bool button1WasClicked = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            string str = "";
            string cpf_no = "";
           
            string queryId = Request.QueryString["id"];
            string[] query1 = queryId.Split('?');
            
            cpf_no = Session["Login_Name"].ToString();
            //string str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where [ENTRY_BY]='" + cpf_no + "' ORDER BY [TIMESTAMP] DESC";

            string query = "";
            //query = "select [child_id] from [CY_MENU_EMP_RELATION] where [cpf_number]='" + cpf_no + "' and child_id in('62','63','64','65','67','77','98','99')";
            query = "select [child_id] from [CY_MENU_EMP_RELATION] where [cpf_number]='" + cpf_no + "' and child_id in('"+ query1[0] + "')";
            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            DataSet ds = My.ExecuteSELECTQuery(query);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {

                if (ds.Tables[0].Rows[0]["child_id"].ToString() == "62")
                {
                    Departments = "TELEPHONE";
                }
                else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "63")
                {
                    Departments = "ELECTRICAL(AC)";
                }
                else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "64")
                {
                    Departments = "CIVIL";
                }
                else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "77")
                {
                    Departments = "ELECTRICAL(MAINTENANCE)";
                }
                else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "65")
                {
                    Departments = "HOUSEKEEPING";
                }
                else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "67")
                {
                    Departments = "OTHER";
                }
                else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "99")
                {
                    Departments = "WalkieTalkie";
                }

                else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "98")
                {
                    Departments = "PAPaging";
                }
                else
                {
                    Departments = "";
                }

            }
            if (Departments != "")
            {
                str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where [COMPLAIN_DEPARTMENT]='" + Departments + "' ORDER BY [TIMESTAMP] DESC";
                ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
                DataSet ds1 = My1.ExecuteSELECTQuery(str);
                if (!IsPostBack)
                {
                    grid_detail_complaint.DataSource = ds1;
                    grid_detail_complaint.DataBind();

                }
               
            }

        }

        public static DataTable BindStatuss()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("VIEWED_RESPONSE");
            dt.Rows.Add("NEW");
            dt.Rows.Add("ASSIGN");
            dt.Rows.Add("WORK IN PROGRESS");
            dt.Rows.Add("RESOLVED");
            return dt;

        }

        //Update Dropdown Values
        public static Dictionary<string,string> BindStatuss1()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("VIEWED_RESPONSE", "VIEWED_RESPONSE");
            list.Add("NEW", "NEW");
            list.Add("ASSIGN", "ASSIGNED");
            list.Add("WORK IN PROGRESS", "WORK IN PROGRESS");
            list.Add("RESOLVED", "RESOLVED");
            return list;

        }
        protected void btn2_Click(object sender, EventArgs e)
        {
            string date0 = "";
            string date3 = "";
            string date1 = "";
            DateTime dt;
            string dates = "";
            string date2 = "";
            MyApplication1 myapp = new MyApplication1();
            date0 = DateTime.Now.ToString();
            date3 = myapp.convertdatetimeformattosql(date0);
            if (txtdatefrom.Text != "" && txtdatefrom.Text != null)
                date1 = myapp.convertdateforsql1(txtdatefrom.Text);
            if (txtdateto.Text != "" && txtdateto.Text != null)
            {
                //dt = Convert.ToDateTime(myapp.convertfordt(txtdateto.Text));
                //dates = dt.AddDays(1).ToString();
                date2 = myapp.convertdateforsql1(txtdateto.Text);
            }
            if (date1 == "" && date2 == "")
            {
                Response.Write("<Script language='JavaScript'>" + "alert('Please Select Valid From Date Or To Date')" + "</Script>");
                return;
            }
            else if (date1 != null && date1 != "" && date2 == "")
            {
                MyApplication1 myapp1 = new MyApplication1();
                string stri = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL]  where TIMESTAMP>='" + date1 + "' and   [COMPLAIN_DEPARTMENT]='" + Departments + "' ORDER BY [TIMESTAMP] DESC";
                DataSet ds5 = myapp1.ExecuteSELECTQuery(stri);
                grid_detail_complaint.DataSource = null;
                grid_detail_complaint.DataSource = ds5;
                grid_detail_complaint.DataBind();
            }
            else if (date2 != null && date2 != "" && date1 == "")
            {
                MyApplication1 myapp1 = new MyApplication1();
                string stri = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL]  where TIMESTAMP<='" + date2 + "' and   [COMPLAIN_DEPARTMENT]='" + Departments + "' ORDER BY [TIMESTAMP] DESC";
                DataSet ds5 = myapp1.ExecuteSELECTQuery(stri);
                grid_detail_complaint.DataSource = null;
                grid_detail_complaint.DataSource = ds5;
                grid_detail_complaint.DataBind();
            }
            else if (date1 != null && date1 != "" && date2 != null && date2 != "")
            {
                MyApplication1 myapp1 = new MyApplication1();
                string stri = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL]  where TIMESTAMP between'" + date1 + "' and '" + date2 + "' and   [COMPLAIN_DEPARTMENT]='" + Departments + "' ORDER BY [TIMESTAMP] DESC";
                DataSet ds5 = myapp1.ExecuteSELECTQuery(stri);
                grid_detail_complaint.DataSource = null;
                grid_detail_complaint.DataSource = ds5;
                grid_detail_complaint.DataBind();
            }
        }

        protected void ddl_action_SelectedIndexChanged(object sender, EventArgs e)
        {
            string date3 = "";
            MyApplication1 myapp = new MyApplication1();
            string date0 = DateTime.Now.ToString();
            date3 = myapp.convertdtforsql2008(date0);//myapp.convertsqltoddmmyyyy(myapp.convertdatetimeformattosql(date0));
            DropDownList ddl_status = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl_status.Parent.Parent;
            int idx = row.RowIndex;


            //Retrieve bookid and studentid from Gridview and status(dropdownlist)
            String lblbookid = ((Label)row.Cells[0].FindControl("lblId_m")).Text;
            DropDownList ddl = (DropDownList)row.Cells[0].FindControl("ddl_action");


            //Update Status            
            string query = "Update CY_COMPLAIN_REGISTER_DETAIL set VIEWED_RESPONSE='" + ddl.SelectedValue.ToString() + "',VIEW_TIMESTAMP='"+date3+"' where ID='" + lblbookid + "'";
            ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
            bool res = My1.updateQuery(query);
            if (res)
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Status has been updated successfully.');", true);


            ONGCUIProjects.MyApplication1 M = new ONGCUIProjects.MyApplication1();
            string stry = "update [CY_COMPLAIN_REGISTER_DETAIL] set notification_status='YES1' where COMPLAIN_DEPARTMENT='" + Departments + "' and VIEWED_RESPONSE='RESOLVED' and ID='" + lblbookid + "' ";
            M.updateQuery(stry);
        }

        protected void grid_detail_complaint_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList ddl_status_edits = (DropDownList)e.Row.FindControl("ddl_action");
                //ddl_status_edits.DataSource = BindStatuss();
                ddl_status_edits.DataSource = BindStatuss1();
                //ddl_status_edits.DataValueField = "VIEWED_RESPONS";
                //ddl_status_edits.DataTextField = "VIEWED_RESPONSE";

                ddl_status_edits.DataValueField = "key";
                ddl_status_edits.DataTextField = "value";
                ddl_status_edits.DataBind();
                DataRowView dr1 = e.Row.DataItem as DataRowView;
                ddl_status_edits.SelectedValue = dr1["VIEWED_RESPONSE"].ToString();

            
            }
        }

        protected void ddl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Departments != "")
            {
                if (ddl_status.SelectedValue == "ALL")
                {
                    string strys = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where  [COMPLAIN_DEPARTMENT]='" + Departments + "' ORDER BY [TIMESTAMP] DESC";
                    ONGCUIProjects.MyApplication1 My2 = new ONGCUIProjects.MyApplication1();
                    DataSet ds3 = My2.ExecuteSELECTQuery(strys);

                    grid_detail_complaint.DataSource = ds3;
                    grid_detail_complaint.DataBind();
                }
                else
                {
                    string stry = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where VIEWED_RESPONSE='" + ddl_status.SelectedValue + "' and   [COMPLAIN_DEPARTMENT]='" + Departments + "' ORDER BY [TIMESTAMP] DESC";
                    ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
                    DataSet ds2 = My1.ExecuteSELECTQuery(stry);

                    grid_detail_complaint.DataSource = ds2;
                    grid_detail_complaint.DataBind();
                }



            }
        }
     
    }
}