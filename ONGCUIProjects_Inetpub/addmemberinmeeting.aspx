﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addmemberinmeeting.aspx.cs" Inherits="ONGCUIProjects.addmemberinmeeting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>TC_Meeting</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <script src="js/jquery-1.8.2.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>     
      
        <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
       
        <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
        <script src="js/jquery.callout.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/our.js"></script>
        <link rel="stylesheet" href="css/diggy.css?ver=4.5.3.3" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/jquery.callout.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style/privilage.css" />
        <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/wave.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
		<script language="javascript">
		    $(document).ready(function () {
		        $(document).ajaxStop($.unblockUI);
		        // for load dropdown list
		        $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
		        $.ajax({
		            type: "GET",
		            url: "services/getdesigsupervisiorandworklocationlist.aspx",
		            data: "",
		            success: function (msg) {
		                // alert(msg);
		                var values = msg.split("~");

		                if (values[0] == "TRUE" && values[1] == "SUCCESS") {
		                    $("#ddldesignation").html(values[3]);

		                }

		            }
		        });
		        $.blockUI({ message: '' });
		        $.ajax({

		            type: "GET",

		            url: "services/getdepartmentlistoremployeelist.aspx",

		            data: "type=DEPARTMENT",

		            success: function (msg) {
		                //  alert(msg);
		                var values = msg.split("~");

		                if (values[0] == "TRUE" && values[1] == "SUCCESS") {

		                    $("#ddldepartment").html(values[4]);


		                }

		            }

		        });

		        //cmd Go
		        $("#cmdgo").click(function () {
		            var data = $("#ddldesignation").val();
		            if (data == "") {
		                data = $("#ddldepartment").val();
		                if (data == "") {
		                    $.gritter.add({
		                        title: "Notification",

		                        text: "Please Select Designation or Department From DropDown List"
		                    });
		                    return false;

		                } else {

		                    //

		                    $.blockUI({ message: '' });
		                    $.ajax({

		                        type: "GET",

		                        url: "services/meeting/getmemberlistformeeting.aspx",

		                        data: "type=DEPARTMENT&data=" + data,

		                        success: function (msg) {
		                            //alert(msg);
		                            var values = msg.split("~");

		                            if (values[0] == "TRUE" && values[1] == "SUCCESS") {

		                                $("#left_member_container").html(values[3]);
		                                $.gritter.add({
		                                    title: "Notification",

		                                    text: values[2]
		                                });

		                            } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {

		                                $("#left_member_container").html("<p class='content' style='font-size: 25px; margin: 0 auto; text-align: center; margin-top: 90px;'>" + values[3] + "</p>");
		                                $.gritter.add({
		                                    title: "Notification",

		                                    text: values[2]
		                                });

		                            } else if (values[0] == "FALSE" && values[1] == "ERR") {

		                                $.gritter.add({
		                                    title: "Notification",

		                                    text: values[2]
		                                });


		                            }

		                        }

		                    });

		                }

		            } else {


		                $.blockUI({ message: '' });
		                $.ajax({

		                    type: "GET",

		                    url: "services/meeting/getmemberlistformeeting.aspx",

		                    data: "type=DESIGNATION&data=" + data,

		                    success: function (msg) {
		                        //alert(msg);
		                        var values = msg.split("~");

		                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {

		                            $("#left_member_container").html(values[3]);


		                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {

		                            $("#left_member_container").html("<p class='content' style='font-size: 25px; margin: 0 auto; text-align: center; margin-top: 90px;'>" + values[2] + "</p>");
		                            $.gritter.add({
		                                title: "Notification",

		                                text: values[2]
		                            });

		                        }

		                    }

		                });
		            }
		        });

		        // selectall
		        var my_select_hint = 0;
		        $(".selectall").live('click', function () {
		            if (my_select_hint == 0) {
		                $(".select_strip").each(function () {
		                    $(this).attr('checked', 'checked');
		                });
		                my_select_hint = 1;
		            } else if (my_select_hint == 1) {
		                $(".select_strip").each(function () {
		                    $(this).removeAttr('checked');
		                });
		                my_select_hint = 0;
		            }
		        });
		        $("#close_me").click(function () {
		            $.fancybox.close();
		        });
		    });
            </script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .strip-box
        {
            
            position: relative; float: left; color: White; border-radius: 5px; padding: 5px 10px 10px 10px;font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif; background-color: #1D8D22; width: 95%; margin-top:5px;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="position: relative ; float : left; width:920px; overflow: hidden; ">
    <div class="Add-MeetingMember-Container">
        <img src="Images/meetingmembers.png" alt="" style="position: relative; float: left; margin: 0 5px 2px 0;" />
        <h1 class="heading" style="width:auto; font-size: 26px;">Add Meeting Members</h1>
    </div>
    <div class="div-fullwidth">
        <asp:DropDownList ID="ddldesignation" runat="server" style="position: relative; float: left;">
            <asp:ListItem>Select Designation</asp:ListItem>
        </asp:DropDownList>
        <p class="content" style="margin-left: 10px;">OR</p>
        <asp:DropDownList ID="ddldepartment" runat="server" style="position: relative; float: left; margin-right: 10px; width: 216px;">
            <asp:ListItem>Select Department</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="cmdgo" runat="server" Text="GO" onclientclick="return false" class="g-button g-button-share" />
    </div>
    <div class="div-fullwidth" style="margin-top: 10px;">
        <label class='selectall content'style="position:relative; float:left; cursor: pointer;  width:100px"><input class='selectall' type="checkbox" style="position: relative; float: left; border-color: #34343b; margin: 5px 5px 0 0;"  /> Select All</label>
        <div style=" position: relative; float: right;">
            <img src="Images/bucket.png" style="position: relative; float: left; margin-right: 5px; width: 35px;" />
            <p id="bucket_total" class="content" style="float:right; font-size: 26px;">Bucket Area</p>
        </div>
    </div>
    <div id="left_member_container"style="position: relative; float: left; width: 40%;padding: 10px; border: 2px dashed #8D8D8D;overflow:auto;height:250px;" >
    </div>
    <div style=" position: relative; float: left; width: 125px; margin-left: 5px; height: 275px;">
    <input id="Addtobucket" type="button" value="ADD TO BUCKET" class="buttonAddtoBucket">
    <input id="save_strip" class="buttonBigGreen" style="width: 100%; position: absolute; bottom: 0; left: 0;" type="button" value="Save" />
    </div>
    <div id="bucket_div"style="position: relative; float: right; width: 40%;padding: 10px; border: 2px dashed #8D8D8D;overflow:auto; height:250px;">
        <p class="content" style="font-size: 25px; margin: 0 auto; text-align: center; margin-top: 90px;">Please Select Member From Left And Add To Bucket Area</p>
    </div>

        

    </div>
    </form>
</body>
</html>
