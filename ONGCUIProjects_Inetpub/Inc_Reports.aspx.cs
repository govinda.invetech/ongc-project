﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;

namespace ONGCUIProjects
{
    public partial class Inc_Reports : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        DataSet ds;
        string str = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");

            }
        }
        protected void cmdgenerate_Click1(object sender, EventArgs e)
        {
            // string type = ddltypeofreport.SelectedItem.Value.ToString();
            string date1 = txtdatefrom.Text;
            string date2 = txtdateto.Text;

            if (date1 == "" || date2 == "")
            {
                Response.Write("<Script language='JavaScript'>" + "alert('Please Select Valid From Date Or To Date')" + "</Script>");
                return;
            }
             string cpf_no = Session["Login_Name"].ToString();
             string sIsModuleAssigned = My.checkcpfhaveassignedmodule(cpf_no, "90");

            string str = "SELECT ";
            str += "[CY_INC_1_DETAILS].[O_INDX_NMBR] as [Incidence No] , ";
            str += "[CY_INC_2_REV_DTL].[CY_INCIDENT_CATEGORY] as [Incident Category],  ";
            str += "[CY_INC_2_REV_DTL].[O_REV_INCDNT_CTGRY] as [Incident Type], ";
            str += "[CY_INC_1_DETAILS].[O_INCDNT_LCTN] as [Incident Location], ";
            str += "[CY_INC_1_DETAILS].[CY_O_AREA] as [Incident Area], ";
            str += "[CY_INC_1_DETAILS].[O_ENG_NM] as [Reporting Engineer Name], ";
            str += "[CY_INC_1_DETAILS].[O_ENG_DSG] as [Engineer Designation], ";
            str += "[CY_INC_1_DETAILS].[O_CPF_NMBR] as [Engineer CPF No.], ";
            str += "[CY_INC_1_DETAILS].[O_INCDNT_DESCRPTN] as [Incident Description] , ";
            str += "[CY_INC_1_DETAILS].[O_INCDNT_DT] as [Incident Date] , ";
            str += "[CY_INC_1_DETAILS].[O_INCDNT_TIME] as [Incident Time] , ";
            str += "[CY_INC_1_DETAILS].[O_RUSH_EMRGNCY] as [Rushed To Hospital], ";
            str += "[CY_INC_1_DETAILS].[CY_STATUS_LEVEL] as [Status], ";
            str += "[CY_INC_1_DETAILS].[CY_TIMESTAMP] as [Recorded Date],  ";
            str += "[CY_INC_2_REV_DTL].[O_REV_ACTION_BY_NM] as [Incident Manager Name],  ";
            str += "[CY_INC_2_REV_DTL].[O_REV_ACTION_BY_CPF_NMBR] as [Incident Manager CPF]  ";
            str += "FROM [CY_INC_1_DETAILS]  ";
            str += "LEFT JOIN [CY_INC_2_REV_DTL] ";
            str += "ON [CY_INC_1_DETAILS].[O_INDX_NMBR]= [CY_INC_2_REV_DTL].[O_INCIDENT_NMBR] ";
            str += "WHERE [CY_INC_1_DETAILS].[O_INCDNT_DT] between '" + date1 + "' and '" + date2 + "' order by [CY_INC_1_DETAILS].[O_INCDNT_DT] DESC";//[CY_INC_1_DETAILS].[O_INDX_NMBR]";

            HiddenField1.Value = str;
            ds = new DataSet();
            ds = My.ExecuteSELECTQuery(str);

            string sIncdentTableData = "";
            if (ds.Tables[0].Rows.Count > 0)
            {
                string sTableHeading = "<tr>";
                sTableHeading += "<th>#</th>";
                foreach (DataColumn column in ds.Tables[0].Columns)
                {
                    sTableHeading += "<th>" + column.ColumnName + "</th>";
                }
                if (sIsModuleAssigned == "TRUE")
                {
                    sTableHeading += "<th>Action</th>";
                }
                sTableHeading += "</tr>";

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string sIncdentID = ds.Tables[0].Rows[i][0].ToString();
                    string sCategory = ds.Tables[0].Rows[i][1].ToString();
                    string sType = ds.Tables[0].Rows[i][2].ToString();
                    string Location = ds.Tables[0].Rows[i][3].ToString();
                    string Area = ds.Tables[0].Rows[i][4].ToString();
                    string enggname = ds.Tables[0].Rows[i][5].ToString();
                    string enggdesig = ds.Tables[0].Rows[i][6].ToString();
                    string enggcpfno = ds.Tables[0].Rows[i][7].ToString();
                    string description = ds.Tables[0].Rows[i][8].ToString();
                    DateTime date = Convert.ToDateTime(ds.Tables[0].Rows[i][9].ToString());
                    string time = ds.Tables[0].Rows[i][10].ToString();
                    string sRushedToHospital = ds.Tables[0].Rows[i][11].ToString();
                    string incidentstatus = My.getStatusStringFromLevel(ds.Tables[0].Rows[i][12].ToString());
                    DateTime incidentdate = Convert.ToDateTime(ds.Tables[0].Rows[i][13].ToString());
                    string sMgrName = ds.Tables[0].Rows[i][14].ToString();
                    string sMgrCPF = ds.Tables[0].Rows[i][15].ToString();


                    sIncdentTableData = sIncdentTableData + "<tr>";
                    sIncdentTableData = sIncdentTableData + "<td>" + (i + 1).ToString().PadLeft(ds.Tables[0].Rows.Count.ToString().Length, '0') + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + sIncdentID + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + sCategory + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + sType + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + Location + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + Area + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + enggname + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + enggdesig + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + enggcpfno + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + description + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + date.ToString("dd-MM-yyyy") + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + time + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + sRushedToHospital + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + incidentstatus + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + incidentdate.ToString("dd-MM-yyyy HH:mm") + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + sMgrName + "</td>";
                    sIncdentTableData = sIncdentTableData + "<td>" + sMgrCPF + "</td>";
                    if (sIsModuleAssigned == "TRUE")
                    {
                        sIncdentTableData = sIncdentTableData + "<td><a class='g-button g-button-red cmdDeleteRecord' recordNumber='" + sIncdentID + "'>Delete</a></td>";
                    }
                    sIncdentTableData = sIncdentTableData + "</tr>";
                }

                divTableData.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading + sIncdentTableData + "</table>";
                cmdExporttoExcel.Visible = true;
                cmdgeneratepdf.Visible = false;
            }
            else
            {
                Label2.Text = "No Data Found b/w Selected Date";
                cmdExporttoExcel.Visible = false;
                cmdgeneratepdf.Visible = false;

            }

        }



        protected void cmdExporttoExcel_Click(object sender, EventArgs e)
        {

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=IncidentReport.xls");
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stwr = new StringWriter();
            HtmlTextWriter htew = new HtmlTextWriter(stwr);
            divTableData.RenderControl(htew);
            Response.Write(stwr.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void cmdgeneratepdf_Click(object sender, EventArgs e)
        {
            string sQuery = HiddenField1.Value.ToString();

            DataSet ds = My.ExecuteSELECTQuery(sQuery);
            DataTable dtdataTable = ds.Tables[0];
            Font font = FontFactory.GetFont(FontFactory.TIMES, 7, Font.NORMAL);
            Font font1 = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            DateTime dtToday = System.DateTime.Today;
            string filename = dtToday.Day + "_" + dtToday.ToString("MM") + "_" + dtToday.Year + " - " + dtdataTable.Rows.Count + " Contacts";
            Document document = new Document(new Rectangle(288f, 144f), 22f, 22f, 25f, 25f);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + filename + ".pdf"; ;
            FileInfo newFile = new FileInfo(path);
            PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
            Font Heading1font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.NORMAL);
            Font Heading2font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, Font.NORMAL);
            Font ColFont = FontFactory.GetFont(FontFactory.TIMES, 7, Font.NORMAL);
            Font ColFonttotal = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            Font ColFontHeader = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            Paragraph Heading1 = new Paragraph("Oil and Natural Gas Corporation Limited\n", Heading1font);
            Heading1.Alignment = Element.ALIGN_CENTER;
            Paragraph Heading2 = new Paragraph("Uran Plant, Uran", Heading2font);
            Heading2.Alignment = Element.ALIGN_CENTER;
            document.Open();
            document.Add(Heading1);
            document.Add(Heading2);
            document.Add(new Paragraph(" "));
            PdfPTable TelDir = new PdfPTable(dtdataTable.Columns.Count);
            float[] TableCwidths = new float[] { .5f, 1f, 1f, 1f, 1f, 1f, 1f, 5f, 1f, 1f, 1.5f, 1.5f, 1f, 1f, 1f, 1f, 1f };
            TelDir.SetWidths(TableCwidths);
            TelDir.WidthPercentage = 100;
            for (int Col = 0; Col < dtdataTable.Columns.Count; Col++)
            {
                Paragraph pr = new Paragraph(dtdataTable.Columns[Col].ColumnName, ColFontHeader);
                if (Col == 0)
                { pr.Alignment = Element.ALIGN_CENTER; }
                else
                { pr.Alignment = Element.ALIGN_CENTER; }
                PdfPCell cell = new PdfPCell();
                cell.AddElement(pr);
                TelDir.AddCell(cell);
            }

            for (int Row = 0; Row < dtdataTable.Rows.Count; Row++)
            {
                for (int Col1 = 0; Col1 < dtdataTable.Columns.Count; Col1++)
                {
                    PdfPCell cell = new PdfPCell();
                    Paragraph ph = new Paragraph(dtdataTable.Rows[Row].ItemArray[Col1].ToString(), ColFont);
                    if (Col1 == 0)
                    {
                        ph.Alignment = Element.ALIGN_CENTER;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    else if (Col1 >= 1 && Col1 <= 3)
                    {
                        ph.Alignment = Element.ALIGN_LEFT;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    else
                    {
                        ph.Alignment = Element.ALIGN_CENTER;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    cell.AddElement(ph);
                    TelDir.AddCell(cell);
                }
            }
            document.Add(TelDir);
            document.Close();
        }


    }
}