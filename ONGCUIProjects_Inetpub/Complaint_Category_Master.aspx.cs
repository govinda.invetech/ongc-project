using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebApplication1
{
	public partial class Complaint_Category_Master : System.Web.UI.Page
	{


		#region variables
		string ReadExecp;
		#endregion
	
		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BtnSaveCmplntCtgry.Attributes.Add("onclick","return ConfirmDelete()");
		}
		#endregion

		#region Insert Record
		private void InsertRecord()
		{
			try
			{
				ONGCBusinessProjects.ONGC_Mstr objBusComplaintCatgry	= new ONGCBusinessProjects.ONGC_Mstr();
				ONGCCustumEntity.ONGC_Mstr objCusComplaintCatgry		= new ONGCCustumEntity.ONGC_Mstr();
				objCusComplaintCatgry.o_CMPLNT_TYP						= txtComplaintCatgry.Text;
				bool Message = (bool)objBusComplaintCatgry.InsertComplaintCatgry(objCusComplaintCatgry);
				if(Message == true)
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Records SuccessFully Saved')"+"</Script>");
				}
				else
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Role Already exist')"+"</Script>");
				}
			}
			catch(Exception ex)
			{
				ReadExecp=ex.ToString();
			}
		}
		#endregion

		#region BlankTextBox
		private void BlankTxtBox()
		{
			txtComplaintCatgry.Text="";
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Save Complaint Category Master
		protected void BtnSaveCmplntCtgry_Click(object sender, System.EventArgs e)
		{
			InsertRecord();
			BlankTxtBox();
		}
		#endregion

		#region Refresh
		protected void BtnRefresh_Click(object sender, System.EventArgs e)
		{
			BlankTxtBox();
		}
		#endregion
	}
}
