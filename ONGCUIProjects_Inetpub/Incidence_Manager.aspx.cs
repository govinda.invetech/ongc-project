using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ONGCCustumEntity;
using ONGCBusinessProjects;

namespace ONGCUIProjects
{
	public class Incidence_Manager : System.Web.UI.Page
	{
		#region Variable

		protected System.Web.UI.WebControls.Button BtnShowRevInc;
		protected System.Web.UI.WebControls.Button BtnShowAccReturns;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.RequiredFieldValidator ActionTaken;
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		protected System.Web.UI.WebControls.Button BtnRefresh;
		protected System.Web.UI.WebControls.Button BtnActionTaken;
		protected System.Web.UI.WebControls.DropDownList cmbAccpName;
		protected System.Web.UI.WebControls.TextBox txtComments;
		protected System.Web.UI.WebControls.TextBox txRevRmrks;
		protected System.Web.UI.WebControls.TextBox txtActionTaken;
		protected System.Web.UI.WebControls.TextBox txtIncDesc;
		protected System.Web.UI.WebControls.TextBox txtComplaintID;
		protected System.Web.UI.WebControls.Label lblActnRev;
		protected System.Web.UI.WebControls.DataGrid DGrd_RevInc;
		protected System.Web.UI.WebControls.Label lblRevDetls;
		protected System.Web.UI.HtmlControls.HtmlTable IncidentDetail;
		protected System.Web.UI.HtmlControls.HtmlTable tblFreshRevInc;
		protected System.Web.UI.HtmlControls.HtmlTable tblReturns;
		protected System.Web.UI.WebControls.DataGrid Dgrd_AcptnReturn;
		protected System.Web.UI.WebControls.Label lblreturn;
		protected System.Web.UI.WebControls.Label lblViewData;
		protected System.Web.UI.WebControls.TextBox txtRevNo;
		protected System.Web.UI.WebControls.TextBox txtAcptNo;
		protected System.Web.UI.WebControls.TextBox txtActNo;
		protected System.Web.UI.WebControls.TextBox txtIncNo;
		protected System.Web.UI.WebControls.Button BtnRevsActnTaken;
		protected System.Web.UI.WebControls.Button BtnRevsRefresh;
		protected System.Web.UI.WebControls.TextBox TxtIncDescr;
		protected System.Web.UI.WebControls.TextBox txtActTaken;
		protected System.Web.UI.WebControls.TextBox txtRevRmrks;
		protected System.Web.UI.WebControls.TextBox TxtActnRmrks;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		string EString;
		#endregion

		#region pageload
		private void Page_Load(object sender, System.EventArgs e)
		{
			tblFreshRevInc.Visible=false;
			tblReturns.Visible=false;
			if(!IsPostBack)
			{
				lblRevDetls.Visible=false;
				DGrd_RevInc.Visible=false;
				lblActnRev.Visible=false;
				IncidentDetail.Visible=false;
				BtnActionTaken.Visible=false;
				BtnRefresh.Visible=false;
				BindEmployee();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.BtnShowRevInc.Click += new System.EventHandler(this.BtnShowRevInc_Click);
			this.BtnShowAccReturns.Click += new System.EventHandler(this.BtnShowAccReturns_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Show review Incident Records
		private void BtnShowRevInc_Click(object sender, System.EventArgs e)
		{
			tblFreshRevInc.Visible=true;
			DGrd_RevInc.Visible=true;
			lblRevDetls.Visible=true;
			BindGrid();
			// Populate Grid "DGrd_RevInc"
		}
		#endregion

		#region BindDataGrid
		private void BindGrid()
		{ 
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("Select O_INDX_NMBR, O_INCIDENT_NMBR, O_INCDNT_DESC, O_REV_INCDNT_CTGRY, O_REV_ROOT_CAUSE, O_REV_RECMND, O_REV_PRECTN_MEASR, O_REV_ACTION_BY_NM, O_REV_ACTION_TARGET_DT, O_REV_TASK_STS, O_REV_RMRKS From O_INCDNT_REV_DTL where O_REV_FLG in ('R') and O_REV_ACTION_BY_CPF_NMBR = "+Session["Login_Name"].ToString()+ "",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				DGrd_RevInc.DataSource=sqlDataReader;
				DGrd_RevInc.DataBind();				
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Get Employee Name 
		private void BindEmployee()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select O_EMP_NM,O_EMP_DESGNTN from O_EMP_MSTR where O_CPF_NMBR='"+Session["Login_Name"].ToString()+"'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				if(sqlDataReader.Read())
				{
					ViewState["EmployeeName"] = sqlDataReader["O_EMP_NM"].ToString();
				
				}
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Data Grid Item Command
		private void DGrd_RevInc_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				if(e.CommandName.Equals("Action"))
				{
					BindEmpDdl();
					BtnActionTaken.Visible=true;
					BtnRefresh.Visible=true;
					lblActnRev.Visible=true;
					IncidentDetail.Visible=true;
					LinkButton lnk=new LinkButton();
					lnk=(LinkButton)e.Item.Cells[0].FindControl("LnkButton");
					string ComplaintID=e.CommandArgument.ToString();
					SqlConnection sqlConnection=new SqlConnection(strConn);
					sqlConnection.Open();
					string val="select O_INDX_NMBR,O_INCIDENT_NMBR,O_INCDNT_DESC,O_REV_RMRKS from O_INCDNT_REV_DTL where O_INDX_NMBR='" +ComplaintID+"'";
					SqlDataAdapter sqlDataAdapter=new SqlDataAdapter(val,sqlConnection);
					DataSet ds=new DataSet();
					sqlDataAdapter.Fill(ds);
					if(ds.Tables[0].Rows.Count>0)
					{
						txtIncDesc.Text					= ds.Tables[0].Rows[0]["O_INCDNT_DESC"].ToString();
						txRevRmrks.Text					= ds.Tables[0].Rows[0]["O_REV_RMRKS"].ToString();
						txtComplaintID.Text				= ComplaintID;
						ViewState["IncNumber"]			= ds.Tables[0].Rows[0]["O_INCIDENT_NMBR"].ToString();
					}
				}
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}			
		}
		#endregion

		#region Bind Employee Dropdown List
		public void BindEmpDdl()
		{
			try
			{
				DataSet dsEmployeeDtl=new DataSet();
				ONGCBusinessProjects.TransportBooking objbusFillEmpDdl		    = new ONGCBusinessProjects.TransportBooking();
				dsEmployeeDtl=objbusFillEmpDdl.fillEmployeeDdl();
				cmbAccpName.DataSource=dsEmployeeDtl;
				cmbAccpName.DataMember=dsEmployeeDtl.Tables[0].ToString();
				cmbAccpName.DataValueField=dsEmployeeDtl.Tables[0].Columns["O_CPF_NMBR"].ToString();
				cmbAccpName.DataTextField=dsEmployeeDtl.Tables[0].Columns["O_EMP_NM"].ToString();
				cmbAccpName.DataBind();
				cmbAccpName.Items.Insert(0,"Select Authority");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}

		}
		#endregion

		#region BlankFields
		private void BlankFields()
		{
			txtActionTaken.Text="";
			txtComments.Text="";
			txtIncDesc.Text="";
			txtComplaintID.Text="";
			cmbAccpName.SelectedIndex=0;
			lblRevDetls.Visible=false;
			DGrd_RevInc.Visible=false;
			lblActnRev.Visible=false;
			IncidentDetail.Visible=false;
			BtnActionTaken.Visible=false;
			BtnRefresh.Visible=false;
		}
		#endregion

		#region Insert Record
		private void InsertRecord()
		{
			try
			{
				ONGCBusinessProjects.IncidentReview objBusReview	= new ONGCBusinessProjects.IncidentReview();
				ONGCCustumEntity.IncidentReview objCusReview		= new ONGCCustumEntity.IncidentReview();
				objCusReview.o_REV_NMBR						= txtComplaintID.Text;
				objCusReview.o_INCIDENT_NMBR				= Convert.ToInt32(ViewState["IncNumber"].ToString());
				objCusReview.o_ACTN_NM						= ViewState["EmployeeName"].ToString();
				objCusReview.o_ACTN_CPF_NMBR				= Session["Login_Name"].ToString();
				objCusReview.o_INCDNT_DESC					= txtIncDesc.Text;
				objCusReview.o_ACTN_TAKN					= txtActionTaken.Text;
				objCusReview.o_RMRKS						= txtComments.Text;
				objCusReview.o_ACPTNC_NM					= cmbAccpName.SelectedItem.Text;
				objCusReview.o_ACPTNC_CPF_NMBR				= cmbAccpName.SelectedValue.ToString();
				bool Message = (bool)objBusReview.InsertActionDetails(objCusReview);
				if(Message == true)
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Records SuccessFully Saved')"+"</Script>");
				}
				else
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Role Already exist')"+"</Script>");
				}
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Update Review Detail Flag
		private void UpdateReviewFlag()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("update O_INCDNT_REV_DTL set O_REV_FLG='T' where O_INDX_NMBR='"+txtComplaintID.Text+"'",sqlConnection);
				sqlCommand.ExecuteNonQuery();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Update Review Data Status
		private void UpdateReviewStatus()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("update O_INCDNT_REV_DTL set O_REV_TASK_STS='Action Taken' where O_INDX_NMBR='"+txtComplaintID.Text+"'",sqlConnection);
				sqlCommand.ExecuteNonQuery();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Button Action Taken
		private void BtnActionTaken_Click(object sender, System.EventArgs e)
		{
			if(cmbAccpName.SelectedItem.Text=="Select Authority")
			{
				Response.Write("<Script language='JavaScript'>"+"alert('Please Select Acceptance Authority Name for Auditing your Corrective Action Taken on the Incident')"+"</Script>");
			}
			else
			{
				InsertRecord();
				UpdateReviewFlag();
				UpdateReviewStatus();
				BlankFields();
				tblFreshRevInc.Visible=false;
			}
		}
		#endregion

		#region Button refresh Click
		private void BtnRefresh_Click(object sender, System.EventArgs e)
		{
			BlankFields();
		}
		#endregion

		private void BtnShowAccReturns_Click(object sender, System.EventArgs e)
		{
			tblReturns.Visible=true;
		}
	}
}
