﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class mergecompanyname : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sQuery = "SELECT DISTINCT([O_VISTR_ORGNZTN]) FROM [O_VSTR_GATE_PASS_RQSTN_DTL] ORDER BY [O_VISTR_ORGNZTN]";
                if (sQuery != "")
                {
                    DataSet dsData = My.ExecuteSELECTQuery(sQuery);

                    if (dsData.Tables[0].Rows.Count == 0)
                    {
                        //div_master_list.InnerHtml = "<h1 style=\"width: 100%; text-align:center;\" class=\"heading\">" + sValueNoFound + "</h1>";

                    }
                    else
                    {

                        string sSelectList = "<select class='mergewith'>";
                        for (int j = 0; j < dsData.Tables[0].Rows.Count; j++)
                        {
                            string sCompanyNameMerge = dsData.Tables[0].Rows[j][0].ToString();
                            sSelectList += "<option value='" + sCompanyNameMerge + "'>" + sCompanyNameMerge + "</option>";
                        }
                        sSelectList += "</select>";


                        string sOutput = "<table class='IncTable'>";

                        sOutput += "<tr>";
                        sOutput += "<th>#</th>";
                        sOutput += "<th>Visitor Company Name</th>";
                        sOutput += "<th colspan ='2'>Change with</th>";
                        sOutput += "</tr>";
                        for (int i = 0; i < dsData.Tables[0].Rows.Count; i++)
                        {
                            string sCompanyName = dsData.Tables[0].Rows[i][0].ToString();

                            sOutput += "<tr>";
                            sOutput += "<td>" + (i + 1).ToString().PadLeft(dsData.Tables[0].Rows.Count.ToString().Length, '0') + "</td>";
                            sOutput += "<td mytype='NAMETD'>" + sCompanyName + "</td>";
                            sOutput += "<td>";
                            sOutput += sSelectList;
                            sOutput += "</td>";
                            sOutput += "<td mytype='ACTIONTD'><a previousname='" + sCompanyName + "' newname='" + sCompanyName + "' style='margin:0px;' href='javascript:void(0);' class='g-button g-button-share cmdUpdate'>Update</a></td>";
                            sOutput += "</tr>";
                        }

                        sOutput += "</table>";

                        div_master_list.InnerHtml = sOutput;
                    }
                }
            }
            catch (Exception ex)
            {
                div_master_list.InnerHtml = ex.Message;
            }






        }
    }
}