﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewUserReg.aspx.cs" Inherits="ONGCUIProjects.NewUserReg" %>

<!DOCTYPE html>
<html>
<head>
    <title>URAN Online Reporting System</title>
    <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" src="js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
    <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script language="javascript">
        $(document).ready(function () {
            $(document).ajaxStop($.unblockUI);
            $("#txtEmpCPFNo").keyup(function () {
                reg = /[^0-9]/g;
                $(this).val($(this).val().replace(reg, ""));
            });

            $("#txtemployeeexno").keyup(function () {
                reg = /[^0-9]/g;
                $(this).val($(this).val().replace(reg, ""));
            });

            $("#lblmy_module_hint").hide();
            $("#lblMySession_cpfno").hide();
            $("#txtDOB").datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true,
                yearRange: "1950:+0"
            });
            $.gritter.options = {
                position: 'middle',
                class_name: '', // could be set to 'gritter-light' to use white notifications
                fade_in_speed: 'medium', // how fast notifications fade in
                fade_out_speed: 1000, // how fast the notices fade out
                time: 6000 // hang on the screen for...
            }
            var my_hint = $("#lblmy_module_hint").text();
            var session_cpf_no = $("#lblMySession_cpfno").text();
            if (my_hint == "UPDATE" || my_hint == "NEW") {
                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                $.ajax({
                    type: "GET",
                    url: "services/getdesigsupervisiorandworklocationlist.aspx",
                    data: "",
                    success: function (msg) {
                        // alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $("#ddlDesignation").html(values[3]);
                            $("#ddlWorkLocation").html(values[4]);
                            $("#ddlSupervisiorname").html(values[5]);
                            $("#ddlDepartment").html(values[7]);
                        }

                    }
                });

            } else {
                window.location = "default.aspx";

            }

            $("#txtEmail1").focusin(function (e) {
                if ($("#txtEmpCPFNo").val().trim() != "") {
                    $(this).val($("#txtEmpCPFNo").val().trim() + "@ongc.co.in");
                }
            });

            if (my_hint == "UPDATE" && session_cpf_no == "") {
                window.location = "default.aspx";
            }
            if (my_hint == "UPDATE") {
                $("#cmdRegisterMe").val("Update Details");
                $(".heading-registrationform").text("My Account Setting");
                $("#module_theory").hide();
                $("#registration_step_theory").remove();



                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                $.ajax({

                    type: "POST",

                    url: "services/editandupdateuserdetail.aspx",

                    data: "cpf_no=" + session_cpf_no + "&type=UPDATE",

                    success: function (msg) {
                        // alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $("#txtEmpCPFNo").attr("disabled", "disabled");
                            $("#txtEmpName").val(values[4]);
                            $("#txtEmpCPFNo").val(values[3]);
                            $("#txtAddr1").val(values[5]);
                            $("#txtAddr2").val(values[6]);
                            $("#txtCity").val(values[7]);
                            $("#txtState").val(values[8]);
                            $("#txtPinCode").val(values[9]);
                            $("#txtHomeNo").val(values[10]);
                            $("#txtMblNo").val(values[11]);
                            $("#CmbGender").val(values[12]);
                            $("#txtDOB").val(values[13]);
                            $("#txtEmail1").val(values[14]);
                            $("#txtEmail2").val(values[15]);
                            $("#ddlDesignation").val(values[17]);
                            $("#ddlSupervisiorname").val(values[18]);
                            $("#ddlDepartment").val(values[19]);
                            $("#ddlWorkLocation").val(values[20]);
                            $("#cmbBldGrp").val(values[22]);
                            $("#txtemployeeexno").val(values[21]);
                            $.gritter.add({
                                title: "Notification",
                                image: "images/tick.png",
                                text: values[2]
                            });
                            $("#Table2").show();

                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            $("#Table2").hide();


                            $.gritter.add({
                                title: "Notification",
                                image: "images/exclamation.png",
                                text: values[2]
                            });

                        }

                    }

                });
            } else if (my_hint != "NEW") {
                alert("Invalid Type");
                return false;
            }
            $("#Emp_Err_Msg").hide();
            $("#Emailid_Err_Msg").hide();
            $("#Cpf_No_Err_Msg").hide();
            $("#lblfinalmsg").hide();
            $("#Mbl_Err_Msg").hide();

            //$("#exno_Err_Msg").hide();

            $("#txtEmpName").focus(function () {
                $(this).css('border-color', '');
                $("#Emp_Err_Msg").hide();
            });
     

            $("#txtEmpCPFNo").focus(function () {
                $(this).css('border-color', '');
                $("#Cpf_No_Err_Msg").hide();
            });
            $("#txtMblNo").focus(function () {
                $(this).css('border-color', '');
                $("#Mbl_Err_Msg").hide();
            });
            $("#txtEmail1").focus(function () {
                $(this).css('border-color', '');
                $("#Emailid_Err_Msg").hide();
            });
            $("#cmdRegisterMe").click(function (event) {

                var employee_name = $("#txtEmpName").val();
                var mbl_no = $("#txtMblNo").val();
                var cpf_no = "";
                if (my_hint == "UPDATE") {
                    cpf_no = session_cpf_no;
                } else {
                    cpf_no = $("#txtEmpCPFNo").val();
                }
                var email = $("#txtEmail1").val();
                var add1 = $("#txtAddr1").val();
                var add2 = $("#txtAddr2").val();
                var city = $("#txtCity").val();
                var state = $("#txtState").val();
                var pincode = $("#txtPinCode").val();
                var homephoeno = $("#txtHomeNo").val();
                var mobile_no = $("#txtMblNo").val();
                var department = $('#ddlDepartment').val();
                var email2 = $("#txtEmail2").val();
                var worklocation = $('#ddlWorkLocation').val();
                var supervisior = $('#ddlSupervisiorname').val();
                var designation = $('#ddlDesignation').val();
                var emplopyeeexno = $('#txtemployeeexno').val();
                var dob = $("#txtDOB").val();
                var designnation = $("#cmbDesg").val();
                var blood_group = $("#cmbBldGrp").val();
                var gender = $("#CmbGender").val();
                if (employee_name == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter Employee Name"
                    });
                    return false;
                }
                if (cpf_no == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter CPF No."
                    });
                    return false;
                }
                if (emplopyeeexno == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter Extension No."
                    });
                    return false;
                }

                
                if (mbl_no == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter Mobile No"
                    });
                    return false;
                }

                // For Email
                if (email == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter Email id"
                    });
                    return false;
                } else {
                    $("#Emailid_Err_Msg").hide();
                    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                    if (emailPattern.test(email) == false) {
                        $("#txtEmail1").css('border-color', 'red');
                        $.gritter.add({
                            title: "Notification",
                            text: "Please Enter A Valid Email"
                        });
                        return false;
                    } else {
                    }
                }

                //for check that mobile no is valid or not
                if (isNaN(mbl_no)) {
                    $.gritter.add({
                        title: "Notification",

                        text: "Please Enter a Valid Mobile No."
                    });
                    return false;

                } else if (mbl_no.length != 10) {
                    $.gritter.add({
                        title: "Notification",

                        text: "Please Enter a Valid Mobile No."
                    });
                    return false;
                }
                if (isNaN(homephoeno)) {
                    $.gritter.add({
                        title: "Notification",

                        text: "Please enter a valid office phone No."
                    });
                    return false;

                }
                if (dob == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter date of birth"
                    });
                    return false;
                }
                if (isNaN(pincode)) {
                    $.gritter.add({
                        title: "Notification",

                        text: "Please Enter a Valid Pin Code"
                    });
                    return false;

                }
                // for blood group
                blood_group = jQuery.trim(blood_group);
                if (blood_group == "SELECT") {
                    blood_group = "";
                }
                blood_group = encodeURIComponent(blood_group);
                // calling service
                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                $.ajax({

                    type: "POST",

                    url: "services/addnewemployeedetail.aspx",

                    data: "e_name=" + employee_name + "&cpf_no=" + cpf_no + "&add1=" + add1 + "&add2=" + add2 + "&city=" + city + "&state=" + state + "&pin_no=" + pincode + "&home_ph_no=" + homephoeno + "&mbl_no=" + mbl_no + "&gender=" + gender + "&designation=" + encodeURIComponent(designation) + "&dob=" + dob + "&email1=" + email + "&email2=" + email2 + "&blood_group=" + blood_group + "&type=" + my_hint + "&supervisior=" + supervisior + "&worklocation=" + worklocation + "&department=" + department + "&employee_exno=" + emplopyeeexno,

                    success: function (msg) {
                        //alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $.gritter.add({
                                title: "Notification",
                                image: "images/tick.png",
                                text: values[2]
                            });
                            $("#lblfinalmsg").text(values[2]);
                            $("#lblfinalmsg").css('color', 'green');

                            $("#lblfinalmsg").show();

                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {

                            $.gritter.add({
                                title: "Notification",
                                image: "images/exclamation.png",
                                text: values[2]
                            });
                            $("#lblfinalmsg").text(values[2]);
                            $("#lblfinalmsg").css('color', 'red');
                            $("#lblfinalmsg").css('font-size', '25');
                            $("#lblfinalmsg").show();
                        }

                    }

                });

            });

            $("#cmdrefresh").live('click', function () {
                window.location = self.location;
            });
            $("#cmdBack").live('click', function () {
                if (my_hint == "UPDATE") {
                    window.location = "launchpage.aspx";
                } else {
                    window.location = "default.aspx";
                }
            });

            var CitySearch_options, CitySearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: CitySearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'CitySearch', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                CitySearch_a = $("#txtCity").autocomplete(options);
            });
            var CitySearch_onAutocompleteSelect = function (CitySearch_value, CitySearch_data) {
                if (CitySearch_value != "Enter something else..") {
                    var data_arr = CitySearch_data.split("~");
                    $("#txtCity").val(data_arr[0]);
                    $("#txtState").val(data_arr[1]);
                }
            }

            var StateSearch_options, StateSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: StateSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'StateSearch', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                CitySearch_a = $("#txtState").autocomplete(options);
            });
            var StateSearch_onAutocompleteSelect = function (StateSearch_value, StateSearch_data) {
                if (StateSearch_value != "Enter something else..") {
                    $("#txtState").val(StateSearch_data);
                }
            }
        });
    </script>
    <!--[if lt IE 7]>
                    <![if gte IE 6]>
                        
        <!-- ACDN-RJS -->
    <script type="text/javascript" src="js/fixpng.js"></script>

    <style type="text/css">
        .iePNG {
            filter: expression(fixPNG(this));
        }
    </style>
    <![endif]>
        <![endif]-->
</head>
<body>
    <div style="position: fixed; height: 100%; width: 100%; z-index: 1;">
        <img class="ongc-bg" src="images/ongc-background.jpg" />
    </div>
    <!-- Container Starts------------------------------------------------------------------------------------------------->
    <div class="container">
        <!-- Body Content Starts---------------------------------------------------------------------------------------------->
        <div class="body-content" style="z-index: 2;">
            <form id="Form1" method="get" runat="server">
                <asp:Label ID="lblmy_module_hint" runat="server"
                    Text="my_module_hint"></asp:Label>
                <asp:Label ID="lblMySession_cpfno" runat="server" Text="Label"></asp:Label>
                <div class="div-fullwidth base" style="padding-bottom: 10px; border-bottom: 5px solid green; border-radius: 0 0 10px 10px;">
                    <h1 class="heading-registrationform">Employee Registration Form</h1>
                </div>

                <div class="div-fullwidth" style="margin-top: 20px; margin-bottom: 20px;">
                    <div class="div-fullwidth base" style="border-radius: 10px 10px 0 0;">
                        <h1 class="registration-content" style="width: 100%; margin-bottom: 20px; font-size: 16px;">Fields Marked with <span style="color: Red;">( * )</span> are Mandatory Fields</h1>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Employee Name :</h1>
                            <asp:TextBox ID="txtEmpName" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                            <h1 class="registration-content star">*</h1>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">CPF No :</h1>
                            <asp:TextBox ID="txtEmpCPFNo" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                            <h1 class="registration-content star">*</h1>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Designation :</h1>
                            <asp:DropDownList ID="ddlDesignation" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                            </asp:DropDownList>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Gender :</h1>
                            <asp:DropDownList ID="CmbGender" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Date of Birth :</h1>
                            <h1 class="registration-content star">*</h1>
                            <asp:TextBox ID="txtDOB" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Department :</h1>
                            <asp:DropDownList ID="ddlDepartment" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                            </asp:DropDownList>
                            <%--<asp:textbox id="txtdepartment" runat="server" style="width: 300px;"></asp:textbox>--%>
                        </div>
                    </div>


                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Mobile No :</h1>
                            <asp:TextBox ID="txtMblNo" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                            <h1 class="registration-content star">*</h1>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Employee Ext. No.:</h1>
                            <asp:TextBox ID="txtemployeeexno" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                            <h1 class="registration-content star">*</h1>
                        </div>
                    </div>

                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">ONGC Mail</h1>
                            <asp:TextBox ID="txtEmail1" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                            <h1 class="registration-content star">*</h1>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Email ID 2 :</h1>
                            <asp:TextBox ID="txtEmail2" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Blood Group :</h1>
                            <asp:DropDownList ID="cmbBldGrp" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                                <asp:ListItem Value="SELECT" Selected="True">Select Blood Group</asp:ListItem>
                                <asp:ListItem Value="A +ve">A +ve</asp:ListItem>
                                <asp:ListItem Value="A -ve">A -ve</asp:ListItem>
                                <asp:ListItem Value="AB +ve">AB +ve</asp:ListItem>
                                <asp:ListItem Value="AB -ve">AB -ve</asp:ListItem>
                                <asp:ListItem Value="B +ve">B +ve</asp:ListItem>
                                <asp:ListItem Value="B -ve">B -ve</asp:ListItem>
                                <asp:ListItem Value="O +ve">O +ve</asp:ListItem>
                                <asp:ListItem Value="O -ve">O -ve</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Work Location :</h1>
                            <asp:DropDownList ID="ddlWorkLocation" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Address Line 1 :</h1>
                            <asp:TextBox ID="txtAddr1" runat="server" CssClass="flattxt" TextMode="Multiline" Style="width: 60%;"></asp:TextBox>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Address Line 2 :</h1>
                            <asp:TextBox ID="txtAddr2" runat="server" CssClass="flattxt" TextMode="Multiline" Style="width: 60%;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">City :</h1>
                            <asp:TextBox ID="txtCity" runat="server" CssClass="flattxt" class="autocompleteclass" Style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat; padding-left: 35px; width: 300px;"></asp:TextBox>
                            <%--<asp:textbox id="txtCity" runat="server" CssClass="flattxt" style="width: 300px;"></asp:textbox>--%>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">State :</h1>
                            <asp:TextBox ID="txtState" runat="server" CssClass="flattxt" class="autocompleteclass" Style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat; padding-left: 35px; width: 300px;"></asp:TextBox>
                            <%--<asp:textbox id="txtState" runat="server" CssClass="flattxt" style="width: 300px;"></asp:textbox>--%>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Pin Code :</h1>
                            <asp:TextBox ID="txtPinCode" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Office Phone No :</h1>
                            <asp:TextBox ID="txtHomeNo" runat="server" CssClass="flattxt" Style="width: 300px;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <div class="div-halfwidth">
                            <h1 class="registration-content">Supervisor Name :</h1>
                            <asp:DropDownList ID="ddlSupervisiorname" runat="server" CssClass="flattxt" Style="width: 61%;">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="div-fullwidth base">
                        <asp:Label ID="lblfinalmsg" runat="server" Text="Label" class="registration-content" Style="font-size: 16px; width: 400px"></asp:Label>
                    </div>
                    <div class="div-fullwidth base">
                        <asp:Button ID="cmdRegisterMe" runat="server" Text="REGISTER ME" class="g-button g-button-share"
                            CausesValidation="False" UseSubmitBehavior="False" OnClientClick="return false;"
                            Style="font-size: 16px; text-decoration: none; cursor: pointer; padding: 10px; height: auto;" />
                        <a href="javascript:void(0);" class="g-button g-button-red" id="cmdBack" style="font-size: 16px; text-decoration: none; cursor: pointer; padding: 10px; height: auto;">Back</a>
                    </div>
                    <div class="div-fullwidth base" style="padding-bottom: 10px;">
                        <h1 class="registration-content note" id="registration_step_theory">
                            <span style="font-size: 20px;">Note:</span>
                            <br />
                            1. Please fill above detail and click on register me button<br />
                            <%--2. After Successfully completion of above step may send your registration request to infocom ADMIN.<br />--%>
                        2. You will be able to login in URAN ONLINE INFORMATION PORTAL when ADMINISTRATOR approves your request.<br />
                            3. After approval, you can use your CPF NO. as your login id and password.<br />
                            4. For any query Please contact to INFOCOM ADMIN.
                        </h1>
                    </div>
                </div>
            </form>
        </div>
        <!-- Body Content Ends------------------------------------------------------------------------------------------------>
    </div>
    <!-- Container Ends--------------------------------------------------------------------------------------------------->
</body>
</html>
