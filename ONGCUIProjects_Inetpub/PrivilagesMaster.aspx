<%@ Page language="c#" Codebehind="PrivilagesMaster.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.PrivilagesMaster" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivilagesMaster</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href=style/treeStyle.css type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" height="100%" cellSpacing="1" cellPadding="1" width="80%" align="center"
				border="1">
				<TR>
					<TD align="center" width="50%" bgColor="#cccccc" height="10%"><asp:dropdownlist id="ddlRole" runat="server" AutoPostBack="True" Width="168px" CssClass="flattxt"
							Font-Names="Verdana" Font-Size="XX-Small" onselectedindexchanged="ddlRole_SelectedIndexChanged"></asp:dropdownlist></TD>
					<TD width="50%" bgColor="#cccccc"><asp:imagebutton id="imgBtnAddPrivillege" ImageUrl="Images/Add_A.gif" Runat="server" onclick="imgBtnAddPrivillege_Click"></asp:imagebutton><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="Images/clearall.gif" onclick="ImageButton1_Click"></asp:imagebutton><asp:label id="MessageBox" runat="server"></asp:label></TD>
				</TR>
				<TR id="TreeRow" height="80%" runat="server">
					<TD vAlign="top" align="center" width="100%" colSpan="2" height="80%"><COMPONENTART:TREEVIEW id="TreeView2" runat="server" Width="100%" CssClass="TreeView" Height="100%" BackColor="#eeeeee"
							ExpandedParentNodeImageUrl="Images/DIRECTORY.gif" HoverNodeCssClass="HoverTreeNode" LeafNodeImageUrl="Images/right2.gif" NodeCssClass="TreeNode" ParentNodeImageUrl="Images/DIRECTORY.gif"
							SelectedExpandedParentNodeImageUrl="Images/DIRECTORY.gif" SelectedNodeCssClass="SelectedTreeNode" DefaultTarget="doc" CollapseImageUrl="Images/Minus.gif" ExpandImageUrl="Images/Plus.gif"
							NoExpandImageUrl="Images/Minus.gif" SelectedLeafNodeImageUrl="Images/right2.gif" ItemSpacing="3" ExtendNodeCells="True"></COMPONENTART:TREEVIEW></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
