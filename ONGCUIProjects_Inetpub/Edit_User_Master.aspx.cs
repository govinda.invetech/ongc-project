using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ONGCCustumEntity;
using ONGCBusinessProjects;
using System.Data.SqlClient;

namespace ONGCUIProjects
{
	/// <summary>
	/// Summary description for Edit_User_Master.
	/// </summary>
	public partial class Edit_User_Master : System.Web.UI.Page
	{
		#region Variables
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		#endregion
	
		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				FillUserAndRole();
				Page.DataBind(); 
				ddlRole.Items.Insert(0,"Select Role");
				ddlUserName.Items.Insert(0,"Select User"); 			
				ClearAll();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Fill User & Role
		private void FillUserAndRole()
		{
			DataSet dsUserRole;
			ONGCBusinessProjects.CerateUser objBusCreateUsers = new ONGCBusinessProjects.CerateUser();
			dsUserRole = (DataSet)objBusCreateUsers.FillUserAndRole(); //returning dataset. 
								
			ddlUserName.DataSource = dsUserRole;
			ddlUserName.DataMember =  dsUserRole.Tables[0].ToString();
			ddlUserName.DataValueField = dsUserRole.Tables[0].Columns["O_CPF_NMBR"].ToString();
			ddlUserName.DataTextField =  dsUserRole.Tables[0].Columns["O_EMP_NM"].ToString();
			ddlUserName.DataBind();	
			ddlUserName.Items.Insert(0,"Select User");

			//ddlRole.ClearSelection();		
			ddlRole.DataSource = dsUserRole;
			ddlRole.DataMember= dsUserRole.Tables[1].ToString();
			ddlRole.DataValueField = dsUserRole.Tables[1].Columns["P_ROLE_ID"].ToString();
			ddlRole.DataTextField =  dsUserRole.Tables[1].Columns["P_ROLE_DSCRPTN"].ToString();
			ddlRole.DataBind();	
			ddlRole.Items.Insert(0,"Select Role"); 
		}
		#endregion		

		#region Clear All
		private void ClearAll()
		{
			txtDesc.Text = "";
			txtUserID.Text = "";
			txtPassword.Text = "";
		}
		#endregion

		#region User Details on Selected Index Changed of User Name.
		protected void ddlUserName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Session["DDLSelectedValue"]=ddlUserName.SelectedValue.ToString();
			Session["DDlSelectedText"]=ddlUserName.SelectedItem.Text;
			ClearAll();

			SqlConnection con=new SqlConnection(strConn);
			SqlCommand sqlcmd =new SqlCommand("Select * from E_USER_MSTR where E_USER_NM = '" + Session["DDlSelectedText"] + "'" ,con );
			con.Open();
			SqlDataReader sdr=sqlcmd.ExecuteReader();
			while(sdr.Read())
			{
				//strProjectCode.Add(sdr["E_CMPNT_MDLE"]);
				txtUserID.Text = sdr["E_USER_CODE"].ToString();
				txtDesc.Text = sdr["E_USER_DSCRPTN"].ToString();
				txtPassword.Text = sdr["E_USER_PSWRD"].ToString();

				SqlConnection con1 = new SqlConnection(strConn);
				SqlCommand sqlcmd1 =new SqlCommand("Select * from E_ROLE_MSTR where P_ROLE_ID = '" + sdr["E_ROLE_ID"] + "'" ,con1 );
				con1.Open();
				SqlDataReader sdr1=sqlcmd1.ExecuteReader();
				while(sdr1.Read())
				{
					ddlRole.SelectedItem.Text = sdr1["P_ROLE_DSCRPTN"].ToString();

				}
			}		
		}
		#endregion

		#region Update User Rights Back to The Database.
		protected void cmdSave_Click(object sender, System.EventArgs e)
		{
			// Setting Local Variables
			string QueryLine;
			string RoleID;

			// Fetching Changed Role ID Here.
			SqlConnection con=new SqlConnection(strConn);
			SqlCommand sqlcmd =new SqlCommand("Select * from E_ROLE_MSTR where P_ROLE_DSCRPTN = '" + ddlRole.SelectedItem.Text + "'" ,con );
			con.Open();
			SqlDataReader sdr=sqlcmd.ExecuteReader();
			while(sdr.Read())
			{
				RoleID = sdr["P_ROLE_ID"].ToString();
				Session["UPDATED_ROLEID"] = sdr["P_ROLE_ID"].ToString();
			}

			// Updating User Master Here
			SqlConnection sqlConnection=new SqlConnection(strConn);
			sqlConnection.Open();

			QueryLine = "Update E_USER_MSTR set E_USER_CODE = '" + txtUserID.Text + "', E_USER_PSWRD = '" + txtPassword.Text + "', E_USER_DSCRPTN = '" + txtDesc.Text + "', E_ROLE_ID = '" + Session["UPDATED_ROLEID"].ToString() + "' where E_USER_NM = '" + Session["DDlSelectedText"].ToString() + "'";

			SqlCommand sqlCommand=new SqlCommand("Update E_USER_MSTR set E_USER_CODE = '" + txtUserID.Text + "', E_USER_PSWRD = '" + txtPassword.Text + "', E_USER_DSCRPTN = '" + txtDesc.Text + "', E_ROLE_ID = '" + Session["UPDATED_ROLEID"].ToString() + "' where E_USER_NM = '" + Session["DDlSelectedText"].ToString() + "'",sqlConnection);
			sqlCommand.ExecuteNonQuery();
			sqlConnection.Close();

			ClearAll();
			ddlUserName.Items.Clear();
			ddlRole.Items.Clear();
			FillUserAndRole();
		}
		#endregion

		#region Refresh
		protected void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			ClearAll();
			ddlRole.Items.Clear();
			ddlUserName.Items.Clear();
			FillUserAndRole();
		}
		#endregion

	}
}
