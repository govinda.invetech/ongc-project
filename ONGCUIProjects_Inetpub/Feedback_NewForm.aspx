﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Feedback_NewForm.aspx.cs" Inherits="ONGCUIProjects.Feedback_NewForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <!-- JS Links Starts -->
    <script type="text/javascript">
      
    </script>
</head>
<body>
    <form id="form1" runat="server">

         <div class="header">
            <div class="logo-container">
                <div class="logo">
                </div>
                <div class="logo-text">
                    <h1 class="logo-text-line2">Oil and Natural Gas Corporation Limited
                    </h1>
                    <h1 class="logo-text-line3">URAN PLANT, MUMBAI
                    </h1>
                </div>
             
                <div style="float:right;margin-right:50px;">
            <asp:LinkButton ID="cmdHome" runat="server" CssClass="home" OnClick="cmdHome_Click">Home</asp:LinkButton>
        </div>
           
        </div>
    
            <div class="logged-user-details">
              
                    
                    <span runat="server" id="NoOfUser" class="TotalRegisteredCount"></span></a>
                <div style="position: relative; float: right;">
                    <a runat="server" href="javascript:void(0)" class="buttonsNotification" id="cmdNewNotifications">
                        <p class="notificationsCircle" style="float: left;">
                            <span></span>
                        </p>
                    </a>
                    <div class="notification-box" id="notificationContainer">
                        <div class="jewelBeeperHeader">
                            <div class="beeperNubWrapper">
                                <div class="beeperNub">
                                </div>
                            </div>
                        </div>
                        <div class="notification_content">
                            <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                                <div class="clearfix uiHeaderTop">
                                    <div>
                                        <h3 runat="server" id="notification_heading" class="uiHeaderTitle"></h3>
                                </div>
                            </div>
                            <div runat="server" id="div_notification" class="uiScrollableArea fade uiScrollableAreaWithShadow"
                                style="width: 330px; height: 100%;">
                                <div class="uiScrollableAreaWrap scrollable" tabindex="0">
                                    <div class="uiScrollableAreaBody" style="width: 330px;">
                                        <div id="ul_notification" class="uiScrollableAreaContent">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Ends----------------------------------------------------------------------------->
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">Infocom FeedBack Form</h1>
            <%-- <a href="TransportBookingHistory.aspx" id="add_new_transport_Request" style="float: right; margin-right: 10px; margin-top: 5px;"
                    class="g-button g-button-red">View Bookings</a>--%>
        </div>
        <div class="div-pagebottom">
        </div>

        <div id="div_full_form" class="div-fullwidth" style="width: 100%;margin-top: 0;">


            <fieldset style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Employee's Details</legend>
                <div class="div-fullwidth iframeMainDiv" style="width: 99%;">

                    <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                        <h1 class="gatepass-content2" style="width: 110px;">CPF No.</h1>
                        <asp:TextBox ID="txtCPFNo" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                    </div>
                    <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                        <h1 class="gatepass-content2" style="width: 110px;">Name</h1>
                        <asp:TextBox ID="txtEmpName" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                    </div>
                    <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                        <h1 class="gatepass-content2" style="width: 110px;">Designation</h1>
                        <asp:TextBox ID="txtDesg" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                    </div>
                    <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                        <h1 class="gatepass-content2" style="width: 110px;">Location</h1>
                        <asp:TextBox ID="txtLocation" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                    </div>

                    <%--    <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                        <h1 class="gatepass-content2" style="width: 110px;">Section</h1>
                        <asp:TextBox ID="txtSection" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                    </div>--%>
                </div>

            </fieldset>
            </div>




            <div class="div-fullwidth" style="float: right; margin-top: 10px;"> 

                <div class="" style="float: left; margin-left:230px; margin-top: 10px;">
               
                <!-- GRIDVIEW------->
                <asp:GridView ID="grdFeedBack" runat="server"
                    AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" Font-Size="Medium" Height="200px" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">
                  
                           
                    <Columns>
                        <asp:TemplateField Visible="false" HeaderText="S No.">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%#Eval("ID")%>' CssClass="" ToolTip="S No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkbox_Header" runat="server" AutoPostBack="true" OnCheckedChanged="chkbox_Header_CheckedChanged" />
                                <asp:Label runat="server" Font-Bold="true" Text="Select All."></asp:Label>

                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkbox_confgrd" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Infocom Facility/Services">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Rating Scale (5-Best)">
                            <ItemTemplate>
                                <asp:RadioButton ID="Radiobtn1" GroupName="a" runat="server" />
                                <asp:Label ID="Label1" runat="server" Style="color: #3684D1;" Text="1"></asp:Label>
                                <asp:RadioButton ID="Radiobtn2" GroupName="a" runat="server" />
                                <asp:Label ID="Label3" runat="server" Style="color: #3684D1;" Text="2"></asp:Label>

                                <asp:RadioButton ID="Radiobtn3" GroupName="a" runat="server" />
                                <asp:Label ID="Label4" runat="server" Style="color: #3684D1;" Text="3"></asp:Label>
                                <asp:RadioButton ID="Radiobtn4" GroupName="a" runat="server" />
                                <asp:Label ID="Label5" runat="server" Style="color: #3684D1;" Text="4"></asp:Label>
                                <asp:RadioButton ID="Radiobtn5" GroupName="a" runat="server" />
                                <asp:Label ID="Label6" runat="server" Style="color: #3684D1;" Text="5"></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Comment">
                            <ItemTemplate>
                                <asp:TextBox ID="txtComment" MaxLength="50" runat="server" Style="width: 185px;" Enabled="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"
                        BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle ForeColor="#330099" HorizontalAlign="Center" BackColor="#FFFFCC" />
                    <RowStyle Wrap="True" HorizontalAlign="Center"  Height="10px" BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
                     </div>
                <!-- Visitor Gate Pass Strip Ends ------------------------------------------------------------------------------------->
               
            </div>
           

            <div class="createlink-divfullwidth" style="margin-top: 0px; border-bottom: 2px solid rgb(88, 89, 90); margin-bottom: 10px;">
            </div>




        
              <div class="" style="margin-bottom: 30px; float:left; margin-left:590px;">

                    <asp:Button ID="btnSave" class="g-button g-button-submit" runat="server" Width="100px" Height="50px" Text="Submit" Style="font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size: 14px; text-transform: capitalize; cursor: pointer; top: 0px; left: 0px;" OnClick="btnSave_Click" />
                  
                   
                </div>
            
              <div style="margin-top: 0px; float:left; clear:both;">
                 
              </div>
    </form>
</body>
</html>
