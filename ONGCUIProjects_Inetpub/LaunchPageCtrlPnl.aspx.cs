﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace ONGCUIProjects
{
    public partial class LaunchPageCtrlPnl : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        string sSelectedMenuGrp = "-1";
        protected void Page_Load(object sender, EventArgs e)
        {          

            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                cmdDeleteMnuGrp.Visible = false;
                divRightContainer.Visible = false;
                string sCPF = Session["Login_Name"].ToString();
                if (Request.QueryString["setting"] != null)
                {
                    string sSettingType = Request.QueryString["setting"].ToString();
                    switch (sSettingType)
                    {
                        case "MENU":

                            DivDefault.Visible = false;
                            divRightContainer.Visible = true;
                            divCreateSubMenu.Visible = false;
                            edit_sbmn.Visible = false;
                            divCreateMenuGrp.Visible = true;
                            lblSettingTypeHeading.Text = "Menus and Sub-menu Setting";


                          
                            string sSelectedSubMenuID = "-1";
                            if (Request.QueryString["mnugrp"] != null)
                            {
                                sSelectedMenuGrp = Request.QueryString["mnugrp"].ToString();
                            }
                            if (Request.QueryString["submenu"] != null)
                            {
                                sSelectedSubMenuID = Request.QueryString["submenu"].ToString();
                            }


                            string sMenuGrpQuery = "SELECT [ID],[GROUP_NAME],[POSITION],[PRIORITY] FROM [CY_LP_MENU_GROUP] ORDER BY [GROUP_NAME] ASC";
                            DataSet dsMenuGrp = My.ExecuteSELECTQuery(sMenuGrpQuery);
                            if (dsMenuGrp.Tables[0].Rows.Count == 0)
                            {
                                divLeftContainer.InnerHtml = "<h1>Menu Group Not Added Yet. Please add a menu group.</h1>";
                            }
                            else
                            {
                                //string ddlMenuGrpOptions = "<option value=\"-1\">Select a Menu Group</option>";
                                ddlMenuGrp.Visible = true;
                                ddlMenuGrp.Items.Clear();
                                ddlMenuGrp.Items.Add(new ListItem("Select a Menu Group", "-1"));
                                for (int i = 0; i < dsMenuGrp.Tables[0].Rows.Count; i++)
                                {
                                    string sGroupID = dsMenuGrp.Tables[0].Rows[i][0].ToString();
                                    string sGroupName = dsMenuGrp.Tables[0].Rows[i][1].ToString();
                                    string sGroupPosition = dsMenuGrp.Tables[0].Rows[i][2].ToString();
                                    string sGroupPriority = dsMenuGrp.Tables[0].Rows[i][3].ToString();
                                    ddlMenuGrp.Items.Add(new ListItem(sGroupName, sGroupID));

                                }
                                ddlMenuGrp.Items.FindByValue(sSelectedMenuGrp).Selected = true;
                                //MenuGroupSelect.InnerHtml = "<select style=\"width:100%; margin-bottom: 20px;\" id=\"ddlMenuGrp\">" + ddlMenuGrpOptions + "</select>";
                            }

                            if (sSelectedMenuGrp != "-1")
                            {

                                txtMenuGrpIDForUpdateDelete.Value = sSelectedMenuGrp;
                               if (!IsPostBack)
                               {
                                string query = "";
                                query = "select [ID],[MENU_TITLE] from [CY_LP_MENU_ITEM] where [GROUP_ID]='" + ddlMenuGrp.SelectedValue.ToString() + "'";
                                DataSet dss = My.ExecuteSELECTQuery(query);
                                if (dss != null && dss.Tables[0].Rows.Count > 0)
                                {
                                    ddl_sm.DataSource = dss;
                                    ddl_sm.DataTextField = "MENU_TITLE";
                                    ddl_sm.DataValueField = "ID";
                                    ddl_sm.DataBind();
                                    ddl_sm.Items.Insert(0, new ListItem("-- Select Sub_Menu --", "0"));
                                }
                               }
                                string sSingleMenuGrpQuery = "SELECT [GROUP_NAME],[POSITION],[PRIORITY] FROM [CY_LP_MENU_GROUP] WHERE [ID] = '" + sSelectedMenuGrp + "'";

                                DataSet dsSingleMenuGroup = My.ExecuteSELECTQuery(sSingleMenuGrpQuery);
                                txtMnuGrpTitle.Text = dsSingleMenuGroup.Tables[0].Rows[0][0].ToString();
                                string sSingleMenuGrpPosition = dsSingleMenuGroup.Tables[0].Rows[0][1].ToString();
                                optSelectMenuGrpPosition.Items.FindByValue(sSingleMenuGrpPosition).Selected = true;
                                txtMnuGrpPriority.Text = dsSingleMenuGroup.Tables[0].Rows[0][2].ToString();
                                cmdCreateMnuGrp.Text = "UPDATE";
                                cmdCreateMnuGrp.Attributes.Add("UpdateID", sSelectedMenuGrp);
                                CreateMenuHeading.InnerText = "Edit Menu Group";
                                divCreateSubMenu.Visible = true;
                                edit_sbmn.Visible = true;
                                CreateNewHeading.Text = "Create Sub Menu";
                                divCreateMenuGrp.Visible = true;
                                lblMenuGrpID.Text = sSelectedMenuGrp;
                                string sSubMenuQuery = "SELECT [ID],[MENU_TITLE],[TOOLTIP],[HREF],[IS_DWNL] FROM [CY_LP_MENU_ITEM] WHERE [GROUP_ID] = '" + sSelectedMenuGrp + "' ORDER BY [ORDER_BY] ASC";
                                DataSet dsSubMenu = My.ExecuteSELECTQuery(sSubMenuQuery);
                                if (dsSubMenu.Tables[0].Rows.Count == 0)
                                {
                                    cmdDeleteMnuGrp.Visible = true;
                                    divGroupSubMenu.InnerHtml = "No Sub Menu Found(0)";
                                }
                                else
                                {
                                    string sSubMenuList = "<ul style=\"padding-left: 0; padding-right:7px;\">";
                                    for (int i = 0; i < dsSubMenu.Tables[0].Rows.Count; i++)
                                    {
                                        string sSubMenuID = dsSubMenu.Tables[0].Rows[i][0].ToString();
                                        string sSubMenuTitle = dsSubMenu.Tables[0].Rows[i][1].ToString();
                                        string sSubMenuTooltip = dsSubMenu.Tables[0].Rows[i][2].ToString();
                                        string sSubMenuHref = dsSubMenu.Tables[0].Rows[i][3].ToString();
                                        string sSubMenuIsDwnl = dsSubMenu.Tables[0].Rows[i][4].ToString();
                                        sSubMenuList += "<li class=\"submenu_list\" submenuid=\"" + sSubMenuID + "\" title=\"" + sSubMenuTooltip + "\" style='word-wrap:break-word'>";
                                        //sSubMenuList += "<a class=\"cmdEditSubMenu\" href=\"javascript:void(0);\">E</a>";
                                        sSubMenuList += "<a class=\"cmdDeleteSubMenu ButtonDeleteSubMenu\" href=\"javascript:void(0);\"></a>";
                                        sSubMenuList += "<p class=\"submenu_name\">" + sSubMenuTitle + "</p>";
                                        sSubMenuList += "<p class=\"submenu_linktype\">" + sSubMenuHref + "</p>";
                                        sSubMenuList += "</li>";
                                    }
                                    sSubMenuList += "</ul>";
                                    divGroupSubMenu.InnerHtml = sSubMenuList;
                                }
                            }
                            if (sSelectedSubMenuID != "-1")
                            {
                                lblSubMenuID.Text = sSelectedSubMenuID;
                            }

                            break;
                        case "TOD":
                            DivDefault.Visible = false;
                            divRightContainer.Visible = false;
                            lblSettingTypeHeading.Text = "Thought of the day setting";
                            try
                            {
                                string sTODQuery = "SELECT [id],[publish_date],[thought_text],[entry_by] FROM [CY_LP_TOD] ORDER BY [publish_date] DESC";
                                DataSet dsTODDetail = My.ExecuteSELECTQuery(sTODQuery);

                                if (dsTODDetail.Tables[0].Rows.Count == 0)
                                {
                                    divContainer.InnerHtml = "<a href=\"services/launchpageadmin/AddUpdTODFancyBox.aspx\" style=\"float:right;\" class=\"cmdEditTOD\">Add Thought</a><h1 style=\"width: 100%; text-align:center;\" class=\"heading\">There is no any \"Thought of the Day\"</h1>";
                                }
                                else
                                {
                                    string sTableData = "<table class=\"IncTable\">";
                                    sTableData = sTableData + "<tbody>";
                                    sTableData = sTableData + "<tr>";
                                    sTableData = sTableData + "<th>Display Date</th>";
                                    sTableData = sTableData + "<th>Thought of the Day</th>";
                                    sTableData = sTableData + "<th style=\"text-align:center;\" colspan=\"2\">Action</th>";
                                    sTableData = sTableData + "</tr>";

                                    for (int i = 0; i < dsTODDetail.Tables[0].Rows.Count; i++)
                                    {
                                        string sTOD_ID = dsTODDetail.Tables[0].Rows[i][0].ToString();
                                        DateTime sTOD_DispDate = Convert.ToDateTime(dsTODDetail.Tables[0].Rows[i][1]);
                                        string sTOD_Text = dsTODDetail.Tables[0].Rows[i][2].ToString();
                                        string sEntryByCPF = dsTODDetail.Tables[0].Rows[i][3].ToString();
                                        string sEntryByName = My.GetEmployeeNameFromCPFNo(sEntryByCPF);
                                        string sToolTip = "Entered By : " + sEntryByName + "[" + sEntryByCPF + "]";

                                        sTableData = sTableData + "<tr title=\"" + sToolTip + "\">";
                                        sTableData = sTableData + "<td style=\"white-space:nowrap; width: 1px;\">" + sTOD_DispDate.ToLongDateString() + "</td>";
                                        sTableData = sTableData + "<td>" + sTOD_Text + "</td>";
                                        sTableData = sTableData + "<td style=\"width: 1px;\"><a href=\"services/launchpageadmin/AddUpdTODFancyBox.aspx?ID=" + sTOD_ID + "\" class=\"g-button g-button-submit cmdEditTOD\" />EDIT</a></td>";
                                        sTableData = sTableData + "<td style=\"width: 1px;\"><input TOD_ID=\"" + sTOD_ID + "\" type=\"button\" value=\"DELETE\" class=\"g-button g-button-red cmdDeleteTOD\" /></td>";
                                        sTableData = sTableData + "</tr>";
                                    }

                                    sTableData = sTableData + "</tbody>";
                                    sTableData = sTableData + "</table>";

                                    divContainer.InnerHtml = "<a href=\"services/launchpageadmin/AddUpdTODFancyBox.aspx\" style=\"float:right;\" class=\"cmdEditTOD\">Add Thought</a>" + sTableData;
                                }
                            }
                            catch (Exception ex)
                            {
                                divContainer.InnerHtml = ex.ToString();
                            }
                            break;
                        case "WHATSNEW":
                            DivDefault.Visible = false;
                            divRightContainer.Visible = true;
                            divCreateSubMenu.Visible = true;                           
                            CreateNewHeading.Text = "Create Whats New";
                            divCreateMenuGrp.Visible = false;
                            lblSettingTypeHeading.Text = "Whats New setting";
                            string sWhatsNewQuery = "SELECT [ID],[MENU_TITLE],[TOOLTIP],[HREF],[IS_DWNL],[LP_STATUS] FROM [CY_LP_WHATS_NEW] WHERE [LP_STATUS] = 'SHOW'ORDER BY [ORDER_BY] ASC, [TIMESTAMP] DESC";
                            DataSet dsWhatsNew = My.ExecuteSELECTQuery(sWhatsNewQuery);
                            if (dsWhatsNew.Tables[0].Rows.Count == 0)
                            {
                                divLeftContainer.InnerHtml = "No Whats New Found(0)";
                            }
                            else
                            {
                                string sWhatsNewList = "<ul id='WhatsNewUl' style=\"padding-left: 0; padding-right:2px; overflow:auto;\">";
                                for (int i = 0; i < dsWhatsNew.Tables[0].Rows.Count; i++)
                                {
                                    string sWhatsNewID = dsWhatsNew.Tables[0].Rows[i][0].ToString();
                                    string sWhatsNewTitle = dsWhatsNew.Tables[0].Rows[i][1].ToString();
                                    string sWhatsNewTooltip = dsWhatsNew.Tables[0].Rows[i][2].ToString();
                                    string sWhatsNewHref = dsWhatsNew.Tables[0].Rows[i][3].ToString();
                                    string sWhatsNewIsDwnl = dsWhatsNew.Tables[0].Rows[i][4].ToString();
                                    string sWhatsNewLPStatus = dsWhatsNew.Tables[0].Rows[i][5].ToString();
                                    sWhatsNewList += "<li class=\"submenu_list\" whatsnewid=\"" + sWhatsNewID + "\" title=\"" + sWhatsNewTooltip + "\" style='word-wrap:break-word'>";
                                    //sWhatsNewList += "<a class=\"cmdEditSubMenu\" href=\"javascript:void(0);\">E</a>";
                                    sWhatsNewList += "<a class=\"cmdDeleteWhatsNew ButtonDeleteSubMenu\" href=\"javascript:void(0);\"></a>";
                                    sWhatsNewList += "<p class=\"submenu_name\">" + sWhatsNewTitle + "</p>";
                                    sWhatsNewList += "<p class=\"submenu_linktype\" style=\"margin-bottom:10px;\">" + sWhatsNewHref + "</p>";

                                    sWhatsNewList += "<a class=\"ButtonHide cmdShowHideWhatsNew\" href=\"javascript:void(0);\">ARCHIVE</a>";

                                    sWhatsNewList += "</li>";
                                }
                                sWhatsNewList += "</ul>";
                                divLeftContainer.InnerHtml = sWhatsNewList;
                            }
                            break;
                        case "PLANTHEAD":
                            DivDefault.Visible = false;
                            divRightContainer.Visible = true;
                            divCreateSubMenu.Visible = true;                           
                            CreateNewHeading.Text = "Manage Plant Head Page";
                            divCreateMenuGrp.Visible = false;
                            lblSettingTypeHeading.Text = "Plant Head Page setting";
                            string sPH_CODE = System.Configuration.ConfigurationSettings.AppSettings["PlantHeadCode"].ToString();

                            string sPantHeadQuery = "SELECT [ID],[PH_EMP_CODE],[MENU_TITLE],[TOOLTIP],[HREF],[IS_DWNL],[ORDER_BY],[ENTRY_BY],[TIMESTAMP] FROM [CY_PLANT_HEAD_PAGE] WHERE [PH_EMP_CODE] = '" + sPH_CODE + "' ORDER BY [TIMESTAMP] DESC";
                            DataSet dsPantHead = My.ExecuteSELECTQuery(sPantHeadQuery);
                            if (dsPantHead.Tables[0].Rows.Count == 0)
                            {
                                divLeftContainer.InnerHtml = "No Plant Head Information(0)";
                            }
                            else
                            {
                                string sPlantHeadList = "<ul id='PlantHeadUL' style=\"padding-left: 0; padding-right:2px; overflow:auto;\">";
                                for (int i = 0; i < dsPantHead.Tables[0].Rows.Count; i++)
                                {
                                    string sID = dsPantHead.Tables[0].Rows[i]["ID"].ToString();
                                    string sTitle = dsPantHead.Tables[0].Rows[i]["MENU_TITLE"].ToString();
                                    string sHref = dsPantHead.Tables[0].Rows[i]["HREF"].ToString();
                                    sPlantHeadList += "<li class=\"submenu_list\" phid=\"" + sID + "\" style='word-wrap:break-word'>";
                                    sPlantHeadList += "<a class=\"cmdDeletePlantHeadQuots ButtonDeleteSubMenu\" href=\"javascript:void(0);\"></a>";
                                    sPlantHeadList += "<p class=\"submenu_name\">" + sTitle + "</p>";
                                    sPlantHeadList += "<p class=\"submenu_linktype\" style=\"margin-bottom:10px;\">" + sHref + "</p>";
                                    sPlantHeadList += "</li>";
                                }
                                sPlantHeadList += "</ul>";
                                divLeftContainer.InnerHtml = sPlantHeadList;
                            }

                            break;
                        default:
                            DivDefault.Visible = true;
                            divRightContainer.Visible = false;
                            lblSettingTypeHeading.Text = "Launch Page Settings";
                            break;
                    }
                }
            }         
                          
        }

        protected void CmdSaveMenuItem_Click(object sender, EventArgs e)
        {
            string sSettingType = Request.QueryString["setting"].ToString();
            string sCPFNo = Session["Login_Name"].ToString();
            string strOutput = "";
            string menu_title = txtTitle.Text;
            string sToolTip = txtToolTip.Text;
            string sHref = txtURL.Text;
            int iOrderBy = System.Convert.ToInt32(txtOrder.Text);
            string get_menu_id = GetMenuID(sSettingType);
            string output = "";
            string sIsImp = "";
            string link_type = "";
            string grp_id = lblMenuGrpID.Text;

            // file upload 

            if (OptDwld.Checked)
            {
                link_type = "DOWNLOAD";
                if (FileUploadControl.HasFile)
                {
                    try
                    {
                        string sFileName = System.IO.Path.GetFileName(FileUploadControl.FileName);
                        string sFileExt = System.IO.Path.GetExtension(FileUploadControl.FileName);
                        sFileName = sFileName.Replace(" ", "_");
                        if (!Directory.Exists(Server.MapPath("~/uploadedlink/" + get_menu_id)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/uploadedlink/" + get_menu_id));
                        }
                        string setlocation = Server.MapPath("~/uploadedlink/" + get_menu_id) + "\\" + sFileName;
                        FileUploadControl.SaveAs(setlocation);

                        if (sFileExt == ".zip")
                        {
                            sHref = "uploadedlink/" + get_menu_id + "/index.html";
                            ExtractZipFile(setlocation, "", Server.MapPath("~/uploadedlink/" + get_menu_id + "/"));
                            File.Delete(setlocation);
                        }
                        else
                        {
                            sHref = "uploadedlink/" + get_menu_id + "/" + sFileName;
                        }
                        output = "file uploaded successfully";
                    }
                    catch (Exception a)
                    {
                        output = a.Message;
                    }
                }
                else
                {
                    output = "Please select A file";
                }
            }
            else if (OptUpldFile.Checked)
            {
                link_type = "DOWNLOAD";
                if (FileUploadControl.HasFile)
                {
                    try
                    {
                        string sFileName = System.IO.Path.GetFileName(FileUploadControl.FileName);
                        string sFileExt = System.IO.Path.GetExtension(FileUploadControl.FileName);
                        sFileName = sFileName.Replace(" ", "_");
                        if (!Directory.Exists(Server.MapPath("~/uploadedlink/" + get_menu_id)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/uploadedlink/" + get_menu_id));
                        }
                        string setlocation = Server.MapPath("~/uploadedlink/" + get_menu_id) + "\\" + sFileName;
                        FileUploadControl.SaveAs(setlocation);

                        sHref = "uploadedlink/" + get_menu_id + "/" + sFileName;
                        output = "file uploaded successfully";
                    }
                    catch (Exception a)
                    {
                        output = a.Message;
                    }
                }
                else
                {
                    output = "Please select A file";
                }
            }
            try
            {
                if (sSettingType == "MENU")
                {                   
                    string sUpdate = "UPDATE [CY_LP_MENU_ITEM] SET [ORDER_BY]=[ORDER_BY]+1";
                    sUpdate += " WHERE [ORDER_BY]>" + (iOrderBy - 1);
                    if (My.ExecuteSQLQuery(sUpdate) == true)
                    {
                        string sInsert = "INSERT INTO [CY_LP_MENU_ITEM] ";
                        sInsert += "([ID],[GROUP_ID],[MENU_TITLE],[TOOLTIP],[HREF],[IS_DWNL],[PRIORITY],[ORDER_BY],[ENTRY_BY],[TIMESTAMP])";
                        sInsert += "VALUES";
                        sInsert += "('" + get_menu_id + "','" + grp_id + "',N'" + menu_title.Replace("'", "''") + "','" + sToolTip.Replace("'", "''") + "','" + sHref + "','" + link_type + "','" + iOrderBy + "','" + iOrderBy + "','" + sCPFNo + "',GETDATE())";
                        if (My.ExecuteSQLQuery(sInsert) == true)
                        {
                            output = "Menu item saved successfully...!";
                        }
                    }
                    else //else case of execute update query
                    {
                        output = "MyApp oocured error ! ERR CODE : *LP187";
                    }
                }
                else if (sSettingType == "WHATSNEW")
                {

                    string sUpdate = "UPDATE [CY_LP_WHATS_NEW] SET [ORDER_BY]=[ORDER_BY]+1";
                    sUpdate += " WHERE [ORDER_BY]>" + (iOrderBy - 1);
                    if (My.ExecuteSQLQuery(sUpdate) == true)
                    {
                        string sInsert = "INSERT INTO [CY_LP_WHATS_NEW] ";
                        sInsert += "([ID],[MENU_TITLE],[TOOLTIP],[HREF],[LP_STATUS],[IS_DWNL],[ORDER_BY],[ENTRY_BY],[TIMESTAMP])";
                        sInsert += "VALUES";
                        sInsert += "('" + get_menu_id + "',N'" + menu_title.Replace("'", "''") + "','" + sToolTip.Replace("'", "''") + "','" + sHref + "','SHOW','" + link_type + "','" + iOrderBy + "','" + sCPFNo + "',GETDATE())";
                        if (My.ExecuteSQLQuery(sInsert) == true)
                        {
                            output = "Whats New Updated Successfully...!";
                        }
                    }
                    else //else case of execute update query
                    {
                        output = "MyApp oocured error ! ERR CODE : *LP187";
                    }
                }
                else if (sSettingType == "PLANTHEAD")
                {
                    string sPH_CODE = System.Configuration.ConfigurationSettings.AppSettings["PlantHeadCode"].ToString();
                    string sUpdate = "UPDATE [CY_PLANT_HEAD_PAGE] SET [ORDER_BY]=[ORDER_BY]+1";
                    sUpdate += " WHERE [ORDER_BY]>" + (iOrderBy - 1);
                    if (My.ExecuteSQLQuery(sUpdate) == true)
                    {
                        string sInsert = "INSERT INTO [CY_PLANT_HEAD_PAGE] ";
                        sInsert += "([ID],[PH_EMP_CODE],[MENU_TITLE],[TOOLTIP],[HREF],[IS_DWNL],[ORDER_BY],[ENTRY_BY],[TIMESTAMP])";
                        sInsert += "VALUES";
                        sInsert += "('" + get_menu_id + "','" + sPH_CODE + "',N'" + menu_title.Replace("'", "''") + "','" + sToolTip.Replace("'", "''") + "','" + sHref + "','" + link_type + "','" + iOrderBy + "','" + sCPFNo + "',GETDATE())";
                        if (My.ExecuteSQLQuery(sInsert) == true)
                        {
                            output = "Plant Head Quotation Updated Successfully...!";
                        }
                    }
                    else //else case of execute update query
                    {
                        output = "MyApp oocured error ! ERR CODE : *LP187";
                    }
                }
            }
            catch (Exception a)
            {
                output = a.Message;
            } //  try catch ends   
            strOutput = "<Script language='JavaScript'>alert('" + output + "');window.location=self.location;</Script>";
            ClientScript.RegisterStartupScript(typeof(Page), "alert", strOutput);
        }

        public string GetMenuID(string SettingType)
        {
            string my_str = "";
            if (SettingType == "MENU")
            {
                my_str = "SELECT { fn IFNULL(MAX(ID), 0) } FROM [CY_LP_MENU_ITEM]";
            }
            else if (SettingType == "WHATSNEW")
            {
                my_str = "SELECT { fn IFNULL(MAX(ID), 0) } FROM [CY_LP_WHATS_NEW]";
            }
            else if (SettingType == "PLANTHEAD")
            {
                my_str = "SELECT { fn IFNULL(MAX(ID), 0) } FROM [CY_PLANT_HEAD_PAGE]";
            }
            DataSet ds1 = My.ExecuteSELECTQuery(my_str);
            int iMenuID = System.Convert.ToInt32(ds1.Tables[0].Rows[0][0]) + 1;
            return iMenuID.ToString();
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {

                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;     // AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }

        protected void cmdDeleteMnuGrp_Click(object sender, EventArgs e)
        {
            try
            {
                string sDeleteQuery = "DELETE FROM [CY_LP_MENU_GROUP] WHERE [ID] = '" + txtMenuGrpIDForUpdateDelete.Value + "'";
                if (My.ExecuteSQLQuery(sDeleteQuery) == true)
                {
                    Response.Write("<script lanuage='javascript'>alert('Menu Group deleted successfully.');window.location = 'LaunchPageCtrlPnl.aspx?setting=MENU';</script>");
                }
            }
            catch (Exception a)
            {
                Response.Write("<script lanuage='javascript'>alert('" + a.Message + "');</script>");
            }
        }    
              

        protected void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                string qury = "";
                qury = "update CY_LP_MENU_ITEM set MENU_TITLE='" + TextBox1.Text + "',TOOLTIP='" + TextBox2.Text + "',PRIORITY='" + TextBox3.Text + "' where ID='" + ddl_sm.SelectedValue.ToString() + "'";
                bool res = My.ExecuteSQLQuery(qury);
                if (res)
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Data hasbeen updated successfully.');", true);
            }
            catch(Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + ex.Message + "');", true);
            }
        }

        protected void ddl_sm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string querys = "";
                querys = "select MENU_TITLE,TOOLTIP,PRIORITY from CY_LP_MENU_ITEM where ID='" + ddl_sm.SelectedValue.ToString() + "'";
                DataSet d2s = My.ExecuteSELECTQuery(querys);
                if (d2s != null && d2s.Tables[0].Rows.Count > 0)
                {
                    TextBox1.Text = d2s.Tables[0].Rows[0]["MENU_TITLE"].ToString();
                    TextBox2.Text = d2s.Tables[0].Rows[0]["TOOLTIP"].ToString();
                    TextBox3.Text = d2s.Tables[0].Rows[0]["PRIORITY"].ToString();
                }
            }
            catch(Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + ex.Message + "');", true);
            }
        }

        

   

    }
}