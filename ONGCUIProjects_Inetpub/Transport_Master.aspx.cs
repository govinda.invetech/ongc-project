﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class Transport_Master : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {

                string sQuery = "SELECT [ID],[ALLOTER_CPF],[VEHICLE_TYPE],[VEHICLE_NO],[DRIVER_NAME],[CONTACT] FROM [CY_TRANSPORT_DRIVER] ORDER BY [ALLOTER_CPF] ASC, [VEHICLE_TYPE] ASC";
                DataSet dsVehicle = My.ExecuteSELECTQuery(sQuery);
                string sOutput="";
                if (dsVehicle.Tables[0].Rows.Count == 0)
                {
                    sOutput = "<p style='text-align:center; float:none; font-size:40px;' class='content'>No Vehicle Found</p>";
                }
                else
                {

                    sOutput = "<table class='tftable' border='1'>";
                    sOutput += "<tr>";
                    sOutput += "<th style='width:10%;'>Alloter Name</th>";
                    sOutput += "<th style='width:10%;'>Vehicle Type</th>";
                    sOutput += "<th style='width:25%;'>Vehicle No</th>";
                    sOutput += "<th style='width:40%;'>Driver Name</th>";
                    sOutput += "<th>Driver Contact</th>";
                    sOutput += "<th style='width:1px;'>Action</th>";
                    sOutput += "</tr>";
                    for (int i = 0; i < dsVehicle.Tables[0].Rows.Count; i++)
                    {

                        string sVehicleID = dsVehicle.Tables[0].Rows[i][0].ToString(); 
                        string sAlloterCPF = dsVehicle.Tables[0].Rows[i][1].ToString();
                        string sVehicleType = dsVehicle.Tables[0].Rows[i][2].ToString();
                        string sVehicleNo = dsVehicle.Tables[0].Rows[i][3].ToString();
                        string sDriverName = dsVehicle.Tables[0].Rows[i][4].ToString();
                        string sDriverContact = dsVehicle.Tables[0].Rows[i][5].ToString();

                        sOutput += "<tr>";
                        sOutput += "<td style='white-space:nowrap'>" + My.GetEmployeeNameFromCPFNo(sAlloterCPF) + "</td>";
                        sOutput += "<td style='white-space:nowrap'>" + sVehicleType + "</td>";
                        sOutput += "<td>" + sVehicleNo + "</td>";
                        sOutput += "<td style='white-space:nowrap'>" + sDriverName + "</td>";
                        sOutput += "<td>" + sDriverContact + "</td>";
                        sOutput += "<td><a class='logout cmdDeleteVehicle' href='javascript:void(0);' style='margin:0px;' id='" + sVehicleID + "'>DELETE</a></td>";
                        sOutput += "</tr>";
                    }                    
                    sOutput += "</table>";
                }
                div_full_form.InnerHtml = sOutput;
            }
        }
    }
}