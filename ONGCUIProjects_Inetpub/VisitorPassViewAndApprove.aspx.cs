﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ONGCCustumEntity;
using ONGCBusinessProjects;
using System.Web.Mail;
using System.Data;

namespace ONGCUIProjects
{
    public partial class VisitorPassViewAndApprove : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {

            string sCPFNo = Session["Login_Name"].ToString();

            string sData = Request["GroupData"].ToString();

            string[] sDataArr = sData.Split('`');
            string sGroupID = sDataArr[0];
            string sSerial = sDataArr[1];
            string sTotalVisitor = sDataArr[2];


            string sQuery = "SELECT TOP 1 [O_APLCNT_NM],[O_APLCNT_DESG],[O_APLCNT_CPF_NMBR],[O_APLCNT_LCTN],[O_APLCNT_EXT_NMBR],[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG],[CY_O_MTNG_OFF_CPF],[O_MTNG_OFF_LCTN],[O_ENTRY_AREA],[O_ENTRY_DT],[O_EXIT_DT],[O_RMRKS],[O_REQSTN_TYPE],[O_PRPS_OF_VISIT] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [CY_GROUP] = '" + sGroupID + "'";

            DataSet dsRequestedGroup = My.ExecuteSELECTQuery(sQuery);

            txtEmpName.Text = dsRequestedGroup.Tables[0].Rows[0][0].ToString();
            txtDesg.Text = dsRequestedGroup.Tables[0].Rows[0][1].ToString();
            txtCPFNo.Text = dsRequestedGroup.Tables[0].Rows[0][2].ToString();
            cmbLocation.Text = dsRequestedGroup.Tables[0].Rows[0][3].ToString();
            txtExtNo.Text = dsRequestedGroup.Tables[0].Rows[0][4].ToString();

            txtWhomToMeet.Text = dsRequestedGroup.Tables[0].Rows[0][5].ToString();
            txtMtng_Off_Desg.Text = dsRequestedGroup.Tables[0].Rows[0][6].ToString();
            txtWhomToMeetCPF.Text = dsRequestedGroup.Tables[0].Rows[0][7].ToString();

            string sMeetingLocation = dsRequestedGroup.Tables[0].Rows[0][8].ToString();


            ArrayList sMeetingLocationArr = new ArrayList(sMeetingLocation.Split(new string[] { " and " }, StringSplitOptions.None));
            string sMeetingLctn = "";
            for (int i = 0; i < sMeetingLocationArr.Count; i++)
            {
                sMeetingLctn += sMeetingLocationArr[i] + ", ";
            }
            divMeetingLocation.Text = sMeetingLctn.Substring(0, sMeetingLctn.Length - 2);
            cmbEntryArea.Items.FindByValue(dsRequestedGroup.Tables[0].Rows[0][9].ToString()).Selected = true;

            DateTime dtEntryDate = Convert.ToDateTime(dsRequestedGroup.Tables[0].Rows[0][10].ToString());
            visitor_start_date.Text = dtEntryDate.ToString("dd-MM-yyyy");

            DateTime dtExitDate = Convert.ToDateTime(dsRequestedGroup.Tables[0].Rows[0][11].ToString());
            visitor_end_date.Text = dtExitDate.ToString("dd-MM-yyyy");

            CmbRequisitionType.Items.FindByValue(dsRequestedGroup.Tables[0].Rows[0][13].ToString()).Selected = true;

            cmbReason.Items.FindByValue(dsRequestedGroup.Tables[0].Rows[0][14].ToString()).Selected = true;

            txtRemarks.Text = dsRequestedGroup.Tables[0].Rows[0][12].ToString();


            div_visitor_list.InnerHtml = getgroupdetail(sGroupID, sTotalVisitor, sCPFNo, sSerial);
        }

        private string getgroupdetail(string group_name, string total_visitor, string cpf_no, string serial)
        {

            string sStr = "Select [O_INDX_NMBR],[O_APLCNT_CPF_NMBR] ,[O_APLCNT_NM],[O_APLCNT_DESG],[O_ENTRY_DT],[O_VISTR_NM], [O_VISTR_AGE], [O_VISTR_ORGNZTN],[O_PRPS_OF_VISIT], O_ENTRY_DT ,[CY_O_VISTR_ENTRY_TIME],[O_RMRKS],[CY_WITH_TOOL] ,[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG] ,[O_VISTR_GNDR]from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_ACP1_CPF_NMBR =  '" + cpf_no + "' and O_PASS_TYPE_FLG = 'V' and O_REQ_COMPLTN_FLG = 'A' and CY_GROUP <>'' and CY_GROUP='" + group_name + "'";
            DataSet ds = My.ExecuteSELECTQuery(sStr);
            string sVisitorData = "";
            if (ds.Tables[0].Rows.Count == 0)
            {
                sVisitorData = "FALSE";
            }
            else
            {
                string my_div_id = serial + "hhhdiv";
                string date_time_str = System.Convert.ToDateTime(ds.Tables[0].Rows[0][4].ToString()).ToLongDateString().ToString() + " " + ds.Tables[0].Rows[0][10].ToString();
                sVisitorData = " <div id=" + my_div_id + "  g_n=" + group_name + " class='div-fullwidth'>";
                sVisitorData += "<table id=\"tfhover\" class=\"tftable\" border=\"1\">";
                sVisitorData += " <tr><th style=\"width: 40px;\">Sr. No</th><th>Visitor's Name</th> <th style=\"width: 40px;\">Age</th> <th style=\"width: 50px;\">Gender</th>  <th style=\"width: 140px;\">Organization</th><th style=\"width: 150px;\">Purpose Of Visit</th><th style=\"width: 150px;\">Tools</th> <th style=\"width: 70px;\"><label><input class='select_all' type=\"checkbox\" serial=" + serial + " style=\"margin-right: 5px; border-color: #929292;\" />Select all</lable></th></tr>";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string my_class = serial + "single_selected";
                    sVisitorData += "<tr id='my_com_row'><td>" + (i + 1).ToString() + "</td><td>" + ds.Tables[0].Rows[i][5].ToString() + "</td><td>" + ds.Tables[0].Rows[i][6].ToString() + "</td><td>" + ds.Tables[0].Rows[i][15].ToString() + "</td><td>" + ds.Tables[0].Rows[i][7].ToString() + "</td><td>" + ds.Tables[0].Rows[i][11].ToString() + "</td><td>" + ds.Tables[0].Rows[i][12].ToString() + "</td><td ><label><input class=" + my_class + " index_no=" + ds.Tables[0].Rows[i][0].ToString() + " style='margin: 0 auto; border-color: #929292;' type='checkbox' value=''/></label></td></tr>";

                }
                sVisitorData += "  </table><div class=\"div-fullwidth\" style=\"margin-top: 10px;\">";
                sVisitorData += "<input class=\"g-button g-button-red\"  id='reject_request' serial=" + serial + " group_no=" + group_name + " value=\"REJECT\" type=\"button\"  style=\" position: relative; float:right; text-decoration: none; margin-right: 10px;\" />";
                sVisitorData += "  <input class=\"g-button g-button-share\"  id='approve_request' serial=" + serial + "  group_no=" + group_name + "  value=\"Approve\" type=\"button\" style=\"position: relative; float:right; text-decoration: none; margin-right: 5px;\" /></div></div>";
            }
            return sVisitorData;
        }
    }
}