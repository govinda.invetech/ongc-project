using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ONGCCustumEntity;
using ONGCBusinessProjects;

namespace ONGCUIProjects
{

	public partial class Discussion_Forum : System.Web.UI.Page
	{
		#region Variables

		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		string EString;
		#endregion
	
		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				BindEmployee();
				BindGrid();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	
		#region Get Employee Name 
		private void BindEmployee()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select O_EMP_NM,O_EMP_DESGNTN from O_EMP_MSTR where O_CPF_NMBR='"+Session["Login_Name"].ToString()+"'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				if(sqlDataReader.Read())
				{
					ViewState["EmployeeName"] = sqlDataReader["O_EMP_NM"].ToString();
				
				}
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Button Enter Click
		protected void BtnEnter_Click(object sender, System.EventArgs e)
		{
			InsertSuggestions();
			BlankField();
			Response.Redirect("Discussion_Forum.aspx");
		}
		#endregion

		#region Insert Suggestions
		public void InsertSuggestions()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("Insert into O_KB_DISCSN_DTL(O_DSCSN_DTL, O_LGN_NM, O_LGN_ID, O_SYS_DT_TM) Values('" + TxtIncDescr.Text + "', '" + ViewState["EmployeeName"].ToString() + "', '"+Session["Login_Name"].ToString()+"', '"+ System.DateTime.Now.ToString()+"')",sqlConnection);
				sqlCommand.ExecuteNonQuery();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region BindDataGrid
		private void BindGrid()
		{ 
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("Select O_LGN_NM, O_DSCSN_DTL From O_KB_DISCSN_DTL Order by O_INDX_NMBR desc",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				Dgrd_Suggestion.DataSource=sqlDataReader;
				Dgrd_Suggestion.DataBind();				
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Blank Field
		private void BlankField()
		{
			TxtIncDescr.Text="";
		}
		#endregion

		#region Refresh Page
		protected void BtnRefresh_Click(object sender, System.EventArgs e)
		{

			Response.Redirect("Discussion_Forum.aspx");
			//BlankField();
			//BindGrid();
		}
		#endregion

	}
}
