﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class guesthousedetailforapprove : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sActionType = "";
                if (string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    txtHideActionType.Value = "NEW";
                    txtHideBookingID.Value = "-1";
                }
                else
                {
                    sActionType = Request.QueryString["type"].ToString();
                    txtHideActionType.Value = My.sDataStringTrim(1, sActionType);
                    txtHideBookingID.Value = My.sDataStringTrim(2, sActionType);
                }
                string cpf_no = Session["Login_Name"].ToString();

                #region Applicant Detail
                string str = "SELECT [O_EMP_NM] ,[O_CPF_NMBR]  ,[O_EMP_DESGNTN] ,[O_EMP_DEPT_NM],[CY_EMP_EX_NO],[O_EMP_MBL_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
                DataSet ds = My.ExecuteSELECTQuery(str);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Response.Write("FALSE~ERR~Applicant Detail Not Found");
                }
                else
                {
                    txtappname.Value = ds.Tables[0].Rows[0][0].ToString();

                    txtappcpf_no.Value = ds.Tables[0].Rows[0][1].ToString();
                    txtappdesign.Value = ds.Tables[0].Rows[0][2].ToString() + " ";
                    txtapplocation.Value = ds.Tables[0].Rows[0][3].ToString() + " ";
                    txtappphoneex_no.Value = ds.Tables[0].Rows[0][4].ToString();
                }

                #endregion
                #region If Update
                if (sActionType != "")
                {
                    string sStr = "SELECT [id] , [APP_CPF_NO],[APP_NAME],[APP_DESIG] ,[APP_LOCATION],[APP_EMP_EXTENSION_NO] ,[BOOKING_TYPE],[BOOKING_START_DATETIME],[BOOKING_END_DATETIME],[FOOD_TYPE],[PURPOSE] ,[REMARKS_ALLOCATE],[ROOM_ALLOCATE],[INCHARGE_STATUS],[APPROVER_STATUS],[ALLOCATTER_STATUS] ,[INCHARGE_TIMESTAMP] ,[APPROVER_TIMESTAMP],[ALLOCATTER_TIMESTAMP],[INCHARGE_CPF_NO],[INCHARGE_NAME],[INCHARGE_DESIG],[INCHARGE_LOCATION],[INCHARGE_EXT_NO],[APPROVER_CPF_NO],[APPROVER_NAME],[APPROVER_DESIG],[APPROVER_EXT_NO],[ALLOCATER_CPF_NO],[ALLOCATER_NAME],[ALLOCATER_DESIG],[ALLOCATER_EXT_NO],[ENTRY_BY],[TIMESTAMP],[REMARKS],[no_of_veg],[no_of_nonveg] FROM [CY_GUEST_HOUSE_BOOKING_DETAIL] where [id]='" + txtHideBookingID.Value + "'";
                    DataSet ds2 = My.ExecuteSELECTQuery(sStr);


                    ddltypeofbooking.Items.FindByValue(ds2.Tables[0].Rows[0][6].ToString()).Selected = true;
                    txtbookingstartdatetime.Value = System.Convert.ToDateTime(ds2.Tables[0].Rows[0][7]).ToString("dd-MM-yyyy HH:mm");
                    txtbookingEnddatetime.Value = System.Convert.ToDateTime(ds2.Tables[0].Rows[0][8]).ToString("dd-MM-yyyy HH:mm");

                    if (ds2.Tables[0].Rows[0][6].ToString() == "Food")
                    {
                        txtFoodDeliveryDate.Value = System.Convert.ToDateTime(ds2.Tables[0].Rows[0][7]).ToString("dd-MM-yyyy HH:mm");
                    }

                    type_of_food.Items.FindByValue(ds2.Tables[0].Rows[0][9].ToString()).Selected = true;
                    txtpurpose.Value = ds2.Tables[0].Rows[0][10].ToString();
                    txtremarks.Value = ds2.Tables[0].Rows[0][34].ToString();
                    no_of_veg.Value = ds2.Tables[0].Rows[0][35].ToString();
                    no_of_nonveg.Value = ds2.Tables[0].Rows[0][36].ToString();

                    //incharge detail
                    incharge_officer_name.Value = ds2.Tables[0].Rows[0][20].ToString();
                    incharge_officer_cpf_no.Value = ds2.Tables[0].Rows[0][19].ToString();
                    incharge_officer_Desig.Value = ds2.Tables[0].Rows[0][21].ToString();

                    // approve 
                    guesthouse_approver_nm.Value = ds2.Tables[0].Rows[0][25].ToString();
                    guesthouse_approver_cpf_no.Value = ds2.Tables[0].Rows[0][24].ToString();
                    guesthouse_approver_desig.Value = ds2.Tables[0].Rows[0][26].ToString();

                    //allocater
                    gusethouse_allotment.Value = ds2.Tables[0].Rows[0][29].ToString();
                    guesthouse_allotment_cpf_no.Value = ds2.Tables[0].Rows[0][28].ToString();
                    guesthouse_allotment_desig.Value = ds2.Tables[0].Rows[0][30].ToString();
                    HiddenField1.Value = ds2.Tables[0].Rows[0][6].ToString();





                }
            }
                #endregion
        }
    }
}
