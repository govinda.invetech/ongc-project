<%@ Page Language="c#" CodeBehind="Transport_Booking_App.aspx.cs" AutoEventWireup="True"
    Inherits="ONGCUIProjects.Transport_Booking_App" %>

<!DOCTYPE html>
<html>
<head>
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/script.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $(document).ajaxStop($.unblockUI);

            $('.cmdApproveReject').fancybox();


        });
		       
    </script>
   
    <style type="text/css">
        .tooltip
        {
            color: #000000;
            outline: none;
            cursor: default;
            text-decoration: none;
            position: relative;
        }
        .tooltip span
        {
            margin-left: -999em;
            position: absolute;
        }
        .tooltip:hover span
        {
            border-radius: 5px 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            font-family: Calibri, Tahoma, Geneva, sans-serif;
            position: absolute;
            left: 1em;
            top: 1.5em;
            z-index: 99;
            margin-left: 0;
            width: 250px;
            color: white;
        }
        .info
        {
            background: black;
            padding: 0.3em 0.3em 0.3em 0.3em;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server" class="MainInterface-iframe">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                Transport Booking Details</h1>
        </div>
        <div class="div-pagebottom">
        </div>
        <p runat="server" id="P1" class="content" style="color: red; font-size: 20px;">
        </p>
        <div runat="server" class="div-fullwidth" id="pending_detail_div" style="margin-top: 10px;">
        </div>
    </div>
    </form>
</body>
</html>
