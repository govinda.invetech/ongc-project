<%@ Page language="c#" Codebehind="Edit_User_Master.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Edit_User_Master" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Edit_User_Master</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<DIV align="center">
			<form id="Form1" method="post" runat="server">
				<P align="center"><FONT face="Trebuchet MS" size="2"></FONT>&nbsp;</P>
				<P align="center"><FONT size="4"><U><EM><FONT face="Trebuchet MS" color="#ff9966"><STRONG>EDIT USER 
										RIGHT</STRONG></FONT><FONT face="Trebuchet MS" color="#ff9966"><STRONG>S</STRONG></FONT></EM></U></FONT></P>
				<P align="center">
					<TABLE id="Table1" height="60%" cellSpacing="0" cellPadding="0" width="75%" align="center"
						border="1" bgColor="gainsboro">
						<TR>
							<TD align="center" style="HEIGHT: 18px"><STRONG><FONT face="Trebuchet MS" color="#ff9966" size="1">
										SELECT USER</FONT></STRONG></TD>
							<TD align="center" style="HEIGHT: 18px">
								<asp:DropDownList id="ddlUserName" runat="server" Width="261px" Height="24px" Font-Names="Trebuchet MS"
									Font-Size="XX-Small" Font-Bold="True" AutoPostBack="True" onselectedindexchanged="ddlUserName_SelectedIndexChanged"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD align="center" style="HEIGHT: 19px"><STRONG><FONT face="Trebuchet MS" color="#ff9966" size="1">USER 
										ID</FONT></STRONG></TD>
							<TD align="center" style="HEIGHT: 19px">
								<asp:TextBox id="txtUserID" runat="server" BorderColor="Silver" BorderStyle="None" Font-Names="Trebuchet MS"
									Font-Size="XX-Small" MaxLength="10" Width="257px" Font-Bold="True"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD align="center" style="HEIGHT: 20px"><STRONG><FONT face="Trebuchet MS" color="#ff9966" size="1">PASSWORD</FONT></STRONG></TD>
							<TD align="center" style="HEIGHT: 20px">
								<asp:TextBox id="txtPassword" runat="server" BorderColor="Silver" BorderStyle="None" Font-Names="Trebuchet MS"
									Font-Size="XX-Small" MaxLength="10" Width="257px" Font-Bold="True"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD align="center" style="HEIGHT: 19px"><STRONG><FONT face="Trebuchet MS" color="#ff9966" size="1">USER 
										DESCRIPTION</FONT></STRONG></TD>
							<TD align="center" style="HEIGHT: 19px">
								<asp:TextBox id="txtDesc" runat="server" BorderColor="Silver" BorderStyle="None" Font-Names="Trebuchet MS"
									Font-Size="XX-Small" MaxLength="40" Width="257px" Font-Bold="True"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD align="center" style="HEIGHT: 8px"><STRONG><FONT face="Trebuchet MS" color="#ff9966" size="1">USER 
										ROLE</FONT></STRONG></TD>
							<TD align="center" style="HEIGHT: 8px">
								<asp:DropDownList id="ddlRole" runat="server" Font-Names="Trebuchet MS" Font-Size="XX-Small" Width="261px"
									Font-Bold="True" Height="24px"></asp:DropDownList></TD>
						</TR>
					</TABLE>
				</P>
				<P align="center">
					<asp:Button id="cmdSave" runat="server" BorderColor="White" Font-Names="Trebuchet MS" Font-Size="XX-Small"
						Font-Bold="True" Text="UPDATE USER RIGHTS" ForeColor="#FF8000" BackColor="White" onclick="cmdSave_Click"></asp:Button>&nbsp;
					<asp:Button id="cmdRefresh" runat="server" BorderColor="White" Font-Names="Trebuchet MS" Font-Size="XX-Small"
						Width="160px" Font-Bold="True" Text="REFRESH" Height="24px" ForeColor="#FF8000" BackColor="White" onclick="cmdRefresh_Click"></asp:Button></P>
				<P align="center">&nbsp;</P>
			</form>
		</DIV>
	</body>
</HTML>
