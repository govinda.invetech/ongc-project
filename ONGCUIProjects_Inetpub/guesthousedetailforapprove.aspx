﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="guesthousedetailforapprove.aspx.cs"
    Inherits="ONGCUIProjects.guesthousedetailforapprove" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            var sActionType = $("#txtHideActionType").val();
            var sBookingID = $("#txtHideBookingID").val();
            type_selection_change($("#HiddenField1").val());
            $(document).ajaxStop($.unblockUI);

            $('#txtFoodDeliveryDate').datetimepicker({
                dateFormat: "dd-mm-yy",
                minDate: 0
            });

            $(function () {
                var startDateTextBox = $('#txtbookingstartdatetime');
                var endDateTextBox = $('#txtbookingEnddatetime');

                startDateTextBox.datetimepicker({
                    onClose: function (dateText, inst) {
                        if (endDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datetimepicker('getDate');
                            var testEndDate = endDateTextBox.datetimepicker('getDate');
                            if (testStartDate > testEndDate)
                                endDateTextBox.datetimepicker('setDate', testStartDate);
                        }
                        else {
                            endDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime) {
                        endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate'));
                    },
                    dateFormat: "dd-mm-yy",
                    minDate: 0
                });
                endDateTextBox.datetimepicker({
                    onClose: function (dateText, inst) {
                        if (startDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datetimepicker('getDate');
                            var testEndDate = endDateTextBox.datetimepicker('getDate');
                            if (testStartDate > testEndDate)
                                startDateTextBox.datetimepicker('setDate', testEndDate);
                        }
                        else {
                            startDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime) {
                        startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
                    },
                    dateFormat: "dd-mm-yy"
                });
            });





            // send request click function
            $("#sendrequest").click(function () {
                var app_name = $("#txtappname").val().trim();
                var app_cpf_no = $("#txtappcpf_no").val().trim();
                var app_design = $("#txtappdesign").val().trim();
                var app_location = $("#txtapplocation").val().trim();
                var app_phone_ex_no = $("#txtappphoneex_no").val().trim();
                var booking_start_Time = $("#txtbookingstartdatetime").val().trim();
                var booking_end_Time = $("#txtbookingEnddatetime").val().trim();
                var type_of_booking = $("#ddltypeofbooking").val().trim();
                var type_of_food = $("#type_of_food").val().trim();
                var no_of_veg = $("#no_of_veg").val().trim();
                var no_of_nonveg = $("#no_of_nonveg").val().trim();
                var remarks = $("#txtremarks").val().trim();
                var purpose = $("#txtpurpose").val().trim();
                //incharge Detail
                var incharge_officer_name = $("#incharge_officer_name").val().trim();
                var incharge_officer_cpfno = $("#incharge_officer_cpf_no").val().trim();
                var incharge_officer_desig = $("#incharge_officer_Desig").val().trim();

                //approver Detail
                var approver_officer_name = $("#guesthouse_approver_nm").val().trim();
                var approver_officer_cpfno = $("#guesthouse_approver_cpf_no").val().trim();
                var approver_officer_desig = $("#guesthouse_approver_desig").val().trim();
                //allotement Detail
                var allotement_officer_name = $("#gusethouse_allotment").val().trim();
                var allotement_officer_cpfno = $("#guesthouse_allotment_cpf_no").val().trim();
                var allotement_officer_desig = $("#guesthouse_allotment_desig").val().trim();

                if (app_name == "") {

                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Your Name"
                    });
                    return false;
                } else if (app_cpf_no == "") {

                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Your CPF No."
                    });
                    return false;
                } else if (type_of_booking == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Type of Booking"
                    });

                    return false;
                } if (type_of_booking != "Food") {

                    if (booking_start_Time == "") {

                        $.gritter.add({
                            title: "Notification",
                            text: "Please Select Valid Booking Start Date And Time"
                        });
                        return false;
                    }
                    else if (booking_end_Time == "") {

                        $.gritter.add({
                            title: "Notification",
                            text: "Please Select Valid Booking End Date And Time"
                        });
                        return false;
                    }
                    var my_today_date = new Date();

                    if (Date.parse(booking_start_Time) >= Date.parse(booking_end_Time)) {
                        alert("Booking End Date Must Be Greater Than to Booking Start Date");
                        return false;
                    }
                } if (type_of_booking == "Food") {
                    if (type_of_food == "") {
                        $.gritter.add({
                            title: "Notification",
                            text: "Please Select Type of Food"
                        });
                        return false;
                    } else if (no_of_veg == "" && no_of_nonveg == "") {

                        $.gritter.add({
                            title: "Notification",
                            text: "Please Fill No. of Veg OR No. of NonVeg"
                        });
                        return false;
                    } else if (isNaN(no_of_veg)) {

                        $.gritter.add({
                            title: "Notification",
                            text: "Please Fill Valid No. of Veg "
                        });
                        return false;
                    } else if (isNaN(no_of_nonveg)) {

                        $.gritter.add({
                            title: "Notification",
                            text: "Please Fill Valid No. of NonVeg "
                        });
                        return false;
                    }
                    if ($('#txtFoodDeliveryDate').val() == "") {
                        $.gritter.add({
                            title: "Notification",
                            text: "Please select a delivery date"
                        });
                        return false;
                    } else {
                        booking_start_Time = $('#txtFoodDeliveryDate').val();
                    }
                } if (purpose == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Purpose"
                    });
                    return false;

                } if (incharge_officer_name == "" || incharge_officer_cpfno == "") {

                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Incharge Name"
                    });
                    return false;
                } else if (approver_officer_name == "Select Employee Name" || approver_officer_cpfno == "") {

                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Guest House Approver  Name"
                    });
                    return false;
                } else if (allotement_officer_name == "Select Employee Name" || allotement_officer_cpfno == "") {

                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Guest House Alloter  Name"
                    });
                    return false;
                }

                //  alert("app_name=" + app_name + "&app_cpf_no=" + app_cpf_no + "&app_design=" + app_design + "&app_loc=" + app_location + "&app_ph_ex_no=" + app_phone_ex_no + "&type_booking=" + type_of_booking + "&booking_start_datetime=" + booking_start_Time + "&booking_end_datetime=" + booking_end_Time + "&remarks=" + remarks + "&purpose=" + purpose + "&approver_name=" + approver_officer_name + "&approver_cpf_no=" + approver_officer_cpfno + "&approver_desig=" + approver_officer_desig + "&type_of_food=" + type_of_food + "&module_type=" + sActionType + "&incharge_name=" + incharge_officer_name + "&incharge_cpf_no=" + incharge_officer_cpfno + "&incharge_desig=" + incharge_officer_desig + "&allotemet_name=" + allotement_officer_name + "&allotemet_cpf_no=" + allotement_officer_cpfno + "&allotemet_desig=" + allotement_officer_desig + "&no_of_veg=" + no_of_veg + "&no_of_nonveg=" + no_of_nonveg + "&my_index_no=" + sBookingID);
                $.blockUI({ message: '' });
                $.ajax({

                    type: "GET",

                    url: "services/guesthouse/addnewgusethouserequest.aspx",

                    data: "app_name=" + app_name + "&app_cpf_no=" + app_cpf_no + "&app_design=" + app_design + "&app_loc=" + app_location + "&app_ph_ex_no=" + app_phone_ex_no + "&type_booking=" + type_of_booking + "&booking_start_datetime=" + booking_start_Time + "&booking_end_datetime=" + booking_end_Time + "&remarks=" + remarks + "&purpose=" + purpose + "&approver_name=" + approver_officer_name + "&approver_cpf_no=" + approver_officer_cpfno + "&approver_desig=" + approver_officer_desig + "&type_of_food=" + type_of_food + "&module_type=" + sActionType + "&incharge_name=" + incharge_officer_name + "&incharge_cpf_no=" + incharge_officer_cpfno + "&incharge_desig=" + incharge_officer_desig + "&allotemet_name=" + allotement_officer_name + "&allotemet_cpf_no=" + allotement_officer_cpfno + "&allotemet_desig=" + allotement_officer_desig + "&no_of_veg=" + no_of_veg + "&no_of_nonveg=" + no_of_nonveg + "&my_index_no=" + sBookingID,

                    success: function (msg) {
                        // alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                            $.gritter.add({
                                title: "Notification",
                                image: "images/tick.png",
                                text: values[2]
                            });


                            window.location = self.location;

                        } else if (values[0] == "FALSE" && values[1] == "ERR") {
                            $.gritter.add({
                                title: "Notification",
                                image: "images/exclamation.png",
                                text: values[2]
                            });
                        }

                    }

                });
            });

            // incharge_officer_name autocomplete
            var mainmenuSearch_options1, mainmenuSearch_a1;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: mainmenuSearch_onAutocompleteSelect1,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'emp_for_guesthouse', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                mainmenuSearch_a1 = $("#incharge_officer_name").autocomplete(options);
            });
            var mainmenuSearch_onAutocompleteSelect1 = function (mainmenuSearch_value1, mainmenuSearch_data11) {
                if (mainmenuSearch_data11 == -1) {
                    $("#incharge_officer_name").val("");
                } else {
                    var abc = mainmenuSearch_data11.split("~");
                    $("#incharge_officer_cpf_no").val(abc[0]);
                    $("#incharge_officer_Desig").val(abc[1]);
                }

            }

            // Guesthouse approver autocomplete
            var mainmenuSearch_options11, mainmenuSearch_a11;

            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: mainmenuSearch_onAutocompleteSelect11,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'GuestHouseApprover', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                mainmenuSearch_a11 = $("#guesthouse_approver_nm").autocomplete(options);
            });
            var mainmenuSearch_onAutocompleteSelect11 = function (mainmenuSearch_value11, mainmenuSearch_data111) {
                if (mainmenuSearch_data111 == -1) {
                    $("#guesthouse_approver_nm").val("");
                } else {
                    var abc = mainmenuSearch_data111.split("~");
                    $("#guesthouse_approver_cpf_no").val(abc[0]);
                    $("#guesthouse_approver_desig").val(abc[1]);
                }
            }

            // Guesthouse Allotement autocomplete
            var mainmenuSearch_options111, mainmenuSearch_a111;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: mainmenuSearch_onAutocompleteSelect111,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'GuestHouseAlottement', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                mainmenuSearch_a1 = $("#gusethouse_allotment").autocomplete(options);
            });
            var mainmenuSearch_onAutocompleteSelect111 = function (mainmenuSearch_value111, mainmenuSearch_data1111) {
                if (mainmenuSearch_data1111 == -1) {
                    $("#gusethouse_allotment").val("");
                } else {
                    var abc = mainmenuSearch_data1111.split("~");
                    $("#guesthouse_allotment_cpf_no").val(abc[0]);
                    $("#guesthouse_allotment_desig").val(abc[1]);
                }
            }

            //type of booking selection change
            $("#ddltypeofbooking").change(function () {
                var abc = $("#ddltypeofbooking").val();
                type_selection_change(abc);
            });
            function type_selection_change(type) {
                if (type == "Food") {
                    $(".div_type_of_food").show();
                    $(".datetime_div").hide();
                } else {
                    $(".div_type_of_food").hide();
                    $(".datetime_div").show();
                }
            }



        });
    </script>
    <!-- Script Ends -->
</head>
<body>
    <form id="form1" runat="server" class="MainInterface-iframe">
    <asp:HiddenField ID="txtHideActionType" runat="server" />
    <asp:HiddenField ID="txtHideBookingID" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                <img src="Images/guesthouse_icon.png" style="position: relative; float: left; margin: 2px 5px 0 0;" />Guest
                House Booking System</h1>
            <a id="my_booking_history" href="Guest_House_Booking.aspx" style="float: right; margin-right: 10px;
                margin-top: 5px;" class="g-button g-button-red">View Booking</a>
        </div>
        <div class="div-pagebottom">
        </div>
        <div id="div_full_form" class="div-fullwidth" style="width: 100%;">
            <fieldset style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Applicant Details</legend>
                <div class="div-fullwidth marginbottom">
                    <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                        <h1 class="content">
                            Applicant Name</h1>
                        <input runat="server" id="txtappname" type="text" value=" " class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                        <h1 class="content">
                            Designation</h1>
                        <input runat="server" id="txtappdesign" type="text" value=" " class="contactdir-inputbox"
                            style="width: 180px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 26%; margin-top: 0;">
                        <h1 class="content">
                            CPF No.</h1>
                        <input runat="server" id="txtappcpf_no" type="text" value=" " class="contactdir-inputbox"
                            style="width: 150px;" disabled="disabled" />
                    </div>
                </div>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                        <h1 class="content" style="width: 121px;">
                            Department</h1>
                        <input runat="server" id="txtapplocation" type="text" value="  " class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content">
                            Phone Ex. no.</h1>
                        <input runat="server" id="txtappphoneex_no" type="text" value="  " class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend class="content" style="float: none; color: green">Other Detail</legend>
                <div class="div-fullwidth marginbottom">
                    <div class="div-halfwidth">
                        <h1 class="content" style='width:120px;'>
                            Type of Booking</h1>
                        <select runat="server" id="ddltypeofbooking" style="width: 200px;">
                            <option value="">Type of Booking</option>
                            <option value="Conference Hall">Conference Hall</option>
                            <option value="Food">Food</option>
                            <option value="Room">Room</option>
                        </select>
                    </div>
                    <div class="div-halfwidth div_type_of_food" runat="server" style="display: none">
                        <h1 class="content" style='width:120px;'>
                            Type of Food</h1>
                        <select runat="server" id="type_of_food" style="padding: 5px;">
                            <option value="">Type of Food</option>
                            <option value="Lunch">Lunch </option>
                            <option value="Dinner">Dinner </option>
                            <option value="Snacks">Snacks</option>
                        </select>
                    </div>
                    
                </div>
                <div class="div-fullwidth">
                    <div id="Div1" class="div-halfwidth datetime_div" runat="server">
                        <h1 class="content" style='width:120px;'>
                            Booking From</h1>
                        <input runat="server" id="txtbookingstartdatetime" type="text" class="contactdir-inputbox"
                            value=" " style="width: 160px;" />
                    </div>
                    <div id="Div4" class="div-halfwidth datetime_div" runat="server">
                        <h1 class="content" style='width:120px;'>
                            Booking To</h1>
                        <input value=" " runat="server" id="txtbookingEnddatetime" type="text" class="contactdir-inputbox"
                            style="width: 160px;" />
                    </div>
                </div>
                <div class="div-fullwidth">
                    <div id="Div7" class="div-halfwidth div_type_of_food" runat="server">
                        <h1 class="content" style='width:120px;'>
                            Date</h1>
                        <input runat="server" id="txtFoodDeliveryDate" type="text" class="contactdir-inputbox"
                            value=" " style="width: 160px;" />
                    </div>
                    <div id="Div5" class="div-halfwidth div_type_of_food" runat="server" style="display: none">
                        <div class="div-halfwidth">
                            <h1 class="content">Veg</h1>
                            <input runat="server" value=" " id="no_of_veg" type="text" style="position:relative;float:left; width: 60px; margin-right:20px;" />
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="content">Non-Veg</h1>
                            <input runat="server" value=" " id="no_of_nonveg" type="text" style=" position:relative;float:left; width: 60px;margin-right:20px;" />
                        </div>
                    </div>
                </div>
                <div class="div-fullwidth" style='margin-top:10px;'>
                    <div class="div-halfwidth">
                        <h1 class="content" style='width:120px;'>
                            Remarks</h1>
                        <input value=" " runat="server" id="txtremarks" type="text" class="contactdir-inputbox"
                            style="width: 290px;" />
                    </div>
                    <div class="div-halfwidth">
                        <h1 class="content" style='width:120px;'>
                            Purpose</h1>
                        <input value=" " runat="server" id="txtpurpose" type="text" class="contactdir-inputbox"
                            style="width: 285px;" />
                    </div>
                </div>
            </fieldset>
            <fieldset id="incharge_fieldset_bar">
                <legend class="content" style="float: none; color: green">Incharge Detail</legend>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content marginbottom" style="width: 124px;">
                            Incharge Name</h1>
                        <input value=" " runat="server" id="incharge_officer_name" type="text" class="autocompleteclass"
                            style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat;
                            padding-left: 35px; width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 24%; margin-top: 0;">
                        <h1 class="content marginbottom">
                            CPF No.</h1>
                        <input value=" " runat="server" id="incharge_officer_cpf_no" type="text" class="contactdir-inputbox"
                            style="width: 130px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content marginbottom">
                            Designation</h1>
                        <input value=" " runat="server" id="incharge_officer_Desig" type="text" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
            <fieldset id="approver_fieldset_bar">
                <legend class="content" style="float: none; color: green">Guest House Approver Detail</legend>
                <div class="div-fullwidth">
                    <div id="div2" class="div-fullwidth" style="width: 38%;">
                        <h1 class="content marginbottom" style="width: 124px;">
                            Approver Name</h1>
                        <input value=" " runat="server" id="guesthouse_approver_nm" type="text" class="autocompleteclass"
                            style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat;
                            padding-left: 35px; width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 24%;">
                        <h1 class="content marginbottom">
                            CPF No.</h1>
                        <input value=" " runat="server" id="guesthouse_approver_cpf_no" type="text" class="contactdir-inputbox"
                            style="width: 130px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content marginbottom">
                            Designation</h1>
                        <input value=" " runat="server" id="guesthouse_approver_desig" type="text" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
            <fieldset id="allotment_fieldset_bar">
                <legend class="content" style="float: none; color: green">Allotment Officer Details</legend>
                <div class="div-fullwidth">
                    <div id="div3" class="div-fullwidth" style="width: 38%;">
                        <h1 class="content marginbottom" style="width: 124px;">
                            Alloter Name
                        </h1>
                        <input value=" " runat="server" id="gusethouse_allotment" type="text" class="autocompleteclass"
                            style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat;
                            padding-left: 35px; width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 24%; margin-top: 0;">
                        <h1 class="content marginbottom">
                            CPF No.</h1>
                        <input value=" " runat="server" id="guesthouse_allotment_cpf_no" type="text" class="contactdir-inputbox"
                            style="width: 130px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content marginbottom">
                            Designation</h1>
                        <input value=" " runat="server" id="guesthouse_allotment_desig" type="text" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
            <div class="div-fullwidth marginbottom ">
                <input runat="server" id="sendrequest" style="position: relative; float: left;" type="button"
                    class="buttonBigGreen" value="Request For Booking" />
            </div>
        </div>
    </div>
    </form>
