using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.VisualBasic;
namespace ONGCUIProjects
{
    public partial class ONGCMenu : System.Web.UI.Page
    {
        #region Variables


        int i;
        int j;
        int k;
        string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
        #endregion

        ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();


        #region PageLoad
        protected void Page_Load(object sender, System.EventArgs e)
        {

            HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();



            if (!IsPostBack)
            {


                if (Session["Login_Name"] != null)
                {
                    lblSessionLoginName.Text = Session["EmployeeName"].ToString();


                    String sCPFno = Session["Login_Name"].ToString();

                    if (sCPFno == "guest")
                    {
                        my.InsertPageHit(sCPFno, "MENU_PAGE");

                        //Response.Redirect("~/ONGCMenu.aspx");
                    }
                    else
                    {

                        my.InsertPageHit(sCPFno, "MENU_PAGE");

                        lastlogin.Text = "Last logged in : " + my.EmpLastLogin(sCPFno).ToString("F");


                        List<string> MainmenuItems = new List<string>();

                        List<string> ChildmenuItems = new List<string>();

                        GetMenuItemsOfEmployee(ref MainmenuItems, ref  ChildmenuItems, sCPFno);

                        for (int i = 0; i < MainmenuItems.Count; i++)
                        {
                            TreeNode pNode_RR = new TreeNode();
                            string SSS = MainmenuItems[i];
                            pNode_RR.Text = my.sDataStringTrim(1, SSS);
                            pNode_RR.SelectAction = TreeNodeSelectAction.Expand;
                            TreeView1.Nodes.Add(pNode_RR);

                            for (int j = 0; j < ChildmenuItems.Count; j++)
                            {
                                string sChildItem = ChildmenuItems[j];
                                string sId = my.sDataStringTrim(1, sChildItem);
                                string sName = my.sDataStringTrim(2, sChildItem);
                                string sURL = my.sDataStringTrim(3, sChildItem);

                                int iParentNumber = System.Convert.ToInt32(my.sDataStringTrim(4, sChildItem));
                                if (i == iParentNumber - 1)
                                {
                                    Random rnd = new Random();
                                    int randomnumber = rnd.Next(1, 9999);

                                    TreeNode pcNode_TM = new TreeNode();
                                    pcNode_TM.Text = sName;
                                    if(sURL== "Pending_Complain.aspx")
                                    {
                                        pcNode_TM.NavigateUrl = sURL + "?id=" + sId + "";
                                    }
                                    else
                                    {
                                        pcNode_TM.NavigateUrl = sURL ;
                                    }

                                  
                                    if (sId.Trim() == "61")
                                    {
                                        pcNode_TM.Target = "doc";
                                    }
                                    else
                                    {
                                        pcNode_TM.Target = "doc";
                                    }
                                    pNode_RR.ChildNodes.Add(pcNode_TM);
                                }
                            }
                        } //for loop ends
                        int iModuleID = System.Convert.ToInt32(Request.QueryString["p_id"]);
                        string sPagePath = my.getPagePathFromModuleID(iModuleID);

                        if (iModuleID == 80)
                        {
                            sPagePath += "?type=REQUEST";
                        }

                        if (iModuleID != 0)
                        {
                            doc.Attributes.Add("src", sPagePath);
                        }
                    }
                }
                else
                {
                    //if session will null then
                    Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                }
            }

        }
        #endregion

        private string GetMenuItemsOfEmployee(ref List<string> mainmenu, ref List<string> childitems, string sCPF)
        {
            mainmenu.Clear();
            childitems.Clear();

            // Data set for Bind TreeView
            string sStr;
            sStr = "SELECT [CY_MAIN_MENU].[id],";
            sStr = sStr + "[CY_MAIN_MENU].[caption],";
            sStr = sStr + "[CY_MAIN_MENU].[tool_tip_content],";
            sStr = sStr + "[CY_MAIN_MENU].[priority],";
            sStr = sStr + "[CY_CHILD_MENU].[id],";
            sStr = sStr + "[CY_CHILD_MENU].[caption],";
            sStr = sStr + "[CY_CHILD_MENU].[tool_tip_content],";
            sStr = sStr + "[CY_CHILD_MENU].[priority],";//7
            sStr = sStr + "[CY_CHILD_MENU].[main_menu_id],";
            sStr = sStr + "[CY_CHILD_MENU].[child_code],";
            sStr = sStr + "[CY_MENU_EMP_RELATION].[child_id]";
            sStr = sStr + " FROM ((";
            sStr = sStr + "[CY_MAIN_MENU] LEFT JOIN [CY_CHILD_MENU] ON [CY_MAIN_MENU].[id] = [CY_CHILD_MENU].[main_menu_id])";
            sStr = sStr + " LEFT JOIN [CY_MENU_EMP_RELATION] ON [CY_CHILD_MENU].[id] = [CY_MENU_EMP_RELATION].[child_id]) WHERE [CY_MENU_EMP_RELATION].[cpf_number]='" + sCPF + "' AND [CY_CHILD_MENU].[menu_type] = 'MENU' ORDER BY [CY_MAIN_MENU].[priority] ASC, [CY_CHILD_MENU].[priority] ASC";
            SqlConnection con = new SqlConnection(strConn);
            SqlDataAdapter sda1 = new SqlDataAdapter(sStr, con);
            DataSet ds = new DataSet();
            sda1.Fill(ds);
            String sMainMenuCaption_old = "NA";
            int iCount = 0;
            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                String sMainMenuCaption_new = ds.Tables[0].Rows[i][1].ToString();
                if (sMainMenuCaption_new != "Pulse of URAN")
                { 
                if (sMainMenuCaption_new == sMainMenuCaption_old)
                {
                    //Sample data of Child Menu collection/List  i.e. "11~Incidentchild~Incident_Data_Entry.aspx~1"
                    string sChildItem;
                    sChildItem = ds.Tables[0].Rows[i][4].ToString() + "~" + ds.Tables[0].Rows[i][5].ToString() + "~" + ds.Tables[0].Rows[i][9].ToString() + "~" + iCount;  //MainMenu capion and its ID
                        //if (!sChildItem.Contains("PA Paging System") && !sChildItem.Contains(" Walkie-Talkie"))
                        //{
                            childitems.Add(sChildItem);
                        //}
                       
                }
                else
                {
                    iCount++;
                    string sMainItem;


                    sMainItem = sMainMenuCaption_new + "~" + ds.Tables[0].Rows[i][0].ToString();  //MainMenu capion and its ID

                    //if (!sMainMenuCaption_new.Contains("Pulse of URAN"))
                    //{

                    //}
                    mainmenu.Add(sMainItem);


                    //Sample data of Child Menu collection/List  i.e. "11~Incidentchild~Incident_Data_Entry.aspx~1"
                    string sChildItem;
                    sChildItem = ds.Tables[0].Rows[i][4].ToString() + "~" + ds.Tables[0].Rows[i][5].ToString() + "~" + ds.Tables[0].Rows[i][9].ToString() + "~" + iCount;  //MainMenu capion and its ID
                                                                                                                                                                           //if (!sMainMenuCaption_new.Contains("Pulse of URAN"))
                                                                                                                                                                           //{

                    //}
                    childitems.Add(sChildItem);
                    sMainMenuCaption_old = sMainMenuCaption_new;


                }
            }
                }
           

            con.Close();
            return "YES";
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void cmdAccountSetting_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewUserReg.aspx?my_hint=UPDATE");
        }

        protected void cmdHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("launchpage.aspx");
        }

        protected void cmdLogout_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Session["Login_Name"] = "";
            Session["EmployeeName"] = "";
        }

    }
}
