﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace ONGCUIProjects
{
    public partial class Feedback_NewForm : System.Web.UI.Page
    {
        MyApplication1 My = new MyApplication1();
        int dept_ids;
        string s_id="";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string cpf_no = Session["Login_Name"].ToString();
                string sQuery = "SELECT  O_EMP_NM, O_CPF_NMBR, O_EMP_DESGNTN, O_EMP_WRK_LCTN,CY_EMP_EX_No FROM  O_EMP_MSTR where O_CPF_NMBR='" + cpf_no + "'";
                DataSet dsRequisitioner = My.ExecuteSELECTQuery(sQuery);
                if (dsRequisitioner.Tables[0].Rows.Count != 0)
                {
                    txtEmpName.Text = dsRequisitioner.Tables[0].Rows[0][0].ToString();
                    txtCPFNo.Text = dsRequisitioner.Tables[0].Rows[0][1].ToString();
                    ViewState["CPF_NO"] = txtCPFNo.Text.ToString().Trim();
                    ViewState["EMP_NAME"] = txtEmpName.Text.ToString().Trim();
                    txtDesg.Text = dsRequisitioner.Tables[0].Rows[0][2].ToString();
                    txtLocation.Text = dsRequisitioner.Tables[0].Rows[0][3].ToString();
                    //cmbLocation.Text = dsRequisitioner.Tables[0].Rows[0][3].ToString();
                }

                BindGridview();


            }
        }
       
        private void BindGridview()
        {

      
            string cpf_no = Session["Login_Name"].ToString();
            string Query = " ";
            Query += " SELECT DISTINCT(id) FROM [ONGC].[dbo].[O_EMP_MSTR]";
            Query += " join [CY_DEPT_DTLS]                               ";
            Query += "  on [CY_DEPT_DTLS].[name]=[O_EMP_DEPT_NM]         ";

            Query += " where [O_CPF_NMBR]='" + cpf_no + "'               ";

            DataSet dsComment = My.ExecuteSELECTQuery(Query);
             if (dsComment.Tables[0].Rows.Count != 0)
             {
                 dept_ids = Convert.ToInt32(dsComment.Tables[0].Rows[0]["id"]);   ///get dept id on the basis of cpf_no 
                     
             }
             Query = "";
             Query = "SELECT  [ID] ,[NAME] FROM [COMMENT] where id  not in (SELECT  [SERVICE_ID] FROM DEPT_SERVICES where dept_id='"+dept_ids+"' ) ";

             DataSet ds = My.ExecuteSELECTQuery(Query);     /////get all services from comment table that is not hide for this department(dept id)
            if (ds.Tables[0].Rows.Count != 0)
            {
               
                grdFeedBack.DataSource = null;
                grdFeedBack.DataBind();
                grdFeedBack.DataSource = ds;
                grdFeedBack.DataBind();
            }

        }
        int ids;
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string sCPF_no = Convert.ToString(ViewState["CPF_NO"]);
                string emp_name = Convert.ToString(ViewState["EMP_NAME"]);
                DataTable info_dt = new DataTable();
                info_dt.Columns.Add("cpf_no");
                info_dt.Rows.Add(sCPF_no);
                bool responses = My.insertBulkData(info_dt, "cy_Feedback_info");

                string query = "select max(id) as id from cy_Feedback_info";
                DataSet ds = (DataSet)My.ExecuteSELECTQuery(query);
                if (ds != null)
                {
                    ids = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
                }
                DataTable NEWDT = new DataTable();
                NEWDT.Columns.Add("NAME", typeof(string));
                NEWDT.Columns.Add("CPF_NO", typeof(string));
                NEWDT.Columns.Add("EPABX Phone-Office", typeof(string));
                NEWDT.Columns.Add("Landline Office", typeof(string));
                NEWDT.Columns.Add("Mobile-Services", typeof(string));
                NEWDT.Columns.Add("Datacard", typeof(string));
                NEWDT.Columns.Add("Internet", typeof(string));
                NEWDT.Columns.Add("ONGC Reports", typeof(string));
                NEWDT.Columns.Add("SAP", typeof(string));
                NEWDT.Columns.Add("Webice", typeof(string));
                NEWDT.Columns.Add("Lotus Client", typeof(string));
                NEWDT.Columns.Add("Lotus WebMail", typeof(string));
                NEWDT.Columns.Add("Lotus SameTime", typeof(string));
                NEWDT.Columns.Add("IBM Connections/Myspace", typeof(string));
                NEWDT.Columns.Add("Computer Performance", typeof(string));
                NEWDT.Columns.Add("Printer", typeof(string));
                NEWDT.Columns.Add("IT Maintenance Support", typeof(string));
                NEWDT.Columns.Add("SCADA System", typeof(string));
                NEWDT.Columns.Add("Video Conferencing", typeof(string));
                NEWDT.Columns.Add("FAX Services", typeof(string));
                NEWDT.Columns.Add("VHF/Walkie Talkie", typeof(string));
                NEWDT.Columns.Add("HIS", typeof(string));
                NEWDT.Columns.Add("PA System", typeof(string));
                NEWDT.Columns.Add("Audio Visual Systems", typeof(string));
                NEWDT.Columns.Add("EPABX-Residence", typeof(string));
                NEWDT.Columns.Add("Landline Residence", typeof(string));
                NEWDT.Columns.Add("Broadband", typeof(string));
                NEWDT.Columns.Add("Any Other", typeof(string));
                DataRow drs = NEWDT.NewRow();
                DataRow urs = NEWDT.NewRow();


                DataTable dt = new DataTable();
                dt.Columns.Add("Emp_name");
                dt.Columns.Add("cpf_no");
                dt.Columns.Add("remark");
                dt.Columns.Add("rate");
                dt.Columns.Add("C_ID");
                dt.Columns.Add("F_ID");


                int numberofRow1 = grdFeedBack.Rows.Count;
                drs[0] = emp_name;
                drs[1] = sCPF_no;
                for (int i = 0; i < numberofRow1; i++)
                {

                    CheckBox chk = (CheckBox)grdFeedBack.Rows[i].FindControl("chkbox_confgrd");
                    if (chk.Checked == true)
                    {
                        int irate = 0;
                        Label lblC_Id = (Label)grdFeedBack.Rows[i].FindControl("lblId");
                        RadioButton radioBtn1 = (RadioButton)grdFeedBack.Rows[i].FindControl("Radiobtn1");
                        RadioButton radioBtn2 = (RadioButton)grdFeedBack.Rows[i].FindControl("Radiobtn2");
                        RadioButton radioBtn3 = (RadioButton)grdFeedBack.Rows[i].FindControl("Radiobtn3");
                        RadioButton radioBtn4 = (RadioButton)grdFeedBack.Rows[i].FindControl("Radiobtn4");
                        RadioButton radioBtn5 = (RadioButton)grdFeedBack.Rows[i].FindControl("Radiobtn5");

                        if (radioBtn1.Checked == true)
                            irate = 1;
                        else if (radioBtn2.Checked == true)
                            irate = 2;
                        else if (radioBtn3.Checked == true)
                            irate = 3;
                        else if (radioBtn4.Checked == true)
                            irate = 4;
                        else if (radioBtn5.Checked == true)
                            irate = 5;

                        int iC_ID = Convert.ToInt32(lblC_Id.Text.ToString().Trim());
                        TextBox txtComment = (TextBox)grdFeedBack.Rows[i].FindControl("txtComment");
                        string sREMARK = txtComment.Text;
                        #region MyRegion
                        if (iC_ID == 1)
                        {
                            drs[2] = irate;
                            urs[2] = sREMARK;
                        }
                        if (iC_ID == 2)
                        {
                            drs[3] = irate;
                            urs[3] = sREMARK;
                        }
                        if (iC_ID == 3)
                        {
                            drs[4] = irate;
                            urs[4] = sREMARK;
                        }
                        if (iC_ID == 4)
                        {
                            drs[5] = irate;
                            urs[5] = sREMARK;
                        }
                        if (iC_ID == 5)
                        {
                            drs[6] = irate;
                            urs[6] = sREMARK;
                        }
                        if (iC_ID == 6)
                        {
                            drs[7] = irate;
                            urs[7] = sREMARK;
                        }
                        if (iC_ID == 7)
                        {
                            drs[8] = irate;
                            urs[8] = sREMARK;
                        }
                        if (iC_ID == 8)
                        {
                            drs[9] = irate;
                            urs[9] = sREMARK;
                        }
                        if (iC_ID == 9)
                        {
                            drs[10] = irate;
                            urs[10] = sREMARK;
                        }
                        if (iC_ID == 10)
                        {
                            drs[11] = irate;
                            urs[11] = sREMARK;
                        }
                        if (iC_ID == 11)
                        {
                            drs[12] = irate;
                            urs[12] = sREMARK;
                        }
                        if (iC_ID == 12)
                        {
                            drs[13] = irate;
                            urs[13] = sREMARK;
                        }
                        if (iC_ID == 13)
                        {
                            drs[14] = irate;
                            urs[14] = sREMARK;
                        }
                        if (iC_ID == 14)
                        {
                            drs[15] = irate;
                            urs[15] = sREMARK;
                        }
                        if (iC_ID == 15)
                        {
                            drs[16] = irate;
                            urs[16] = sREMARK;
                        }
                        if (iC_ID == 16)
                        {
                            drs[17] = irate;
                            urs[17] = sREMARK;
                        }
                        if (iC_ID == 17)
                        {
                            drs[18] = irate;
                            urs[18] = sREMARK;
                        }
                        if (iC_ID == 18)
                        {
                            drs[19] = irate;
                            urs[19] = sREMARK;
                        }
                        if (iC_ID == 19)
                        {
                            drs[20] = irate;
                            urs[20] = sREMARK;
                        }
                        if (iC_ID == 20)
                        {
                            drs[21] = irate;
                            urs[21] = sREMARK;
                        }
                        if (iC_ID == 21)
                        {
                            drs[22] = irate;
                            urs[22] = sREMARK;
                        }
                        if (iC_ID == 22)
                        {
                            drs[23] = irate;
                            urs[23] = sREMARK;
                        }
                        if (iC_ID == 23)
                        {
                            drs[24] = irate;
                            urs[24] = sREMARK;
                        }
                        if (iC_ID == 24)
                        {
                            drs[25] = irate;
                            urs[25] = sREMARK;
                        }
                        if (iC_ID == 25)
                        {
                            drs[26] = irate;
                            urs[26] = sREMARK;
                        }
                        if (iC_ID == 26)
                        {
                            drs[27] = irate;
                            urs[27] = sREMARK;
                        }
                        #endregion
                        dt.Rows.Add(emp_name, sCPF_no, sREMARK, irate, iC_ID, ids);

                    }

                }
                NEWDT.Rows.Add(drs);
                NEWDT.Rows.Add(urs);

                if (dt != null && dt.Rows.Count > 0)
                {


                    My.insertBulkData(NEWDT, "DATE_REPORT");
                    bool response = My.insertBulkData(dt, "cy_feedback");
                    if (response)
                        WebMsgApp.WebMsgBox.Show("Data has been saved succesfully.");
                    else
                        WebMsgApp.WebMsgBox.Show("Data is not saved !");
                }

                BindGridview();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        protected void chkbox_Header_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = (CheckBox)sender;
                GridViewRow gr = (GridViewRow)chk.Parent.Parent;
                int currentRowIndex = int.Parse(gr.RowIndex.ToString());

                CheckBox ChkBoxHeader = (CheckBox)grdFeedBack.HeaderRow.FindControl("chkbox_Header");
                foreach (GridViewRow row in grdFeedBack.Rows)
                {
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkbox_confgrd");
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Please try again,Error massage " + ex.Message + "');", true);

            }
        }

        protected void cmdHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("launchpage.aspx");
        }
    }
}