using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
namespace ONGCUIProjects
{
    public partial class Edit_feedback_services : System.Web.UI.Page
    {
        #region Variables


        int i;
        int j;
        int k;
        string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"]);
        #endregion

        ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();



        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
                BindDept();
        }
        private void BindDept()
        {
            try
            {
                string query = "";
                query = "SELECT [name],[id] FROM [CY_DEPT_DTLS] order by name ";
                DataSet ds = (DataSet)my.ExecuteSELECTQuery(query);
                if (ds != null)
                {

                    DataRow dr = ds.Tables[0].NewRow();
                    dr["name"] = "--SELECT--";
                    dr["id"] = "0";
                    ds.Tables[0].Rows.InsertAt(dr, 0);

                    ds.Tables[0].Rows.Add("N.A.");

                    ddl_dept.DataSource = ds;
                    ddl_dept.DataTextField = "name";
                    ddl_dept.DataValueField = "id";
                    ddl_dept.DataBind();
                }
            }
            catch (Exception exe)
            {
                WebMsgApp.WebMsgBox.Show("Error" + exe);
            }

        }

        private string GetMenuItemsOfEmployee(ref List<string> mainmenu, ref List<string> childitems, string sCPF)
        {
            mainmenu.Clear();
            childitems.Clear();

            // Data set for Bind TreeView
            string sStr;
            sStr = "SELECT [CY_MAIN_MENU].[id],";
            sStr = sStr + "[CY_MAIN_MENU].[caption],";
            sStr = sStr + "[CY_MAIN_MENU].[tool_tip_content],";
            sStr = sStr + "[CY_MAIN_MENU].[priority],";
            sStr = sStr + "[CY_CHILD_MENU].[id],";
            sStr = sStr + "[CY_CHILD_MENU].[caption],";
            sStr = sStr + "[CY_CHILD_MENU].[tool_tip_content],";
            sStr = sStr + "[CY_CHILD_MENU].[priority],";//7
            sStr = sStr + "[CY_CHILD_MENU].[main_menu_id],";
            sStr = sStr + "[CY_CHILD_MENU].[child_code],";
            sStr = sStr + "[CY_MENU_EMP_RELATION].[child_id]";
            sStr = sStr + " FROM ((";
            sStr = sStr + "[CY_MAIN_MENU] LEFT JOIN [CY_CHILD_MENU] ON [CY_MAIN_MENU].[id] = [CY_CHILD_MENU].[main_menu_id])";
            sStr = sStr + " LEFT JOIN [CY_MENU_EMP_RELATION] ON [CY_CHILD_MENU].[id] = [CY_MENU_EMP_RELATION].[child_id]) WHERE [CY_MENU_EMP_RELATION].[cpf_number]='" + sCPF + "' AND [CY_CHILD_MENU].[menu_type] = 'MENU' ORDER BY [CY_MAIN_MENU].[priority] ASC, [CY_CHILD_MENU].[priority] ASC";
            SqlConnection con = new SqlConnection(strConn);
            SqlDataAdapter sda1 = new SqlDataAdapter(sStr, con);
            DataSet ds = new DataSet();
            sda1.Fill(ds);
            String sMainMenuCaption_old = "NA";
            int iCount = 0;
            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                String sMainMenuCaption_new = ds.Tables[0].Rows[i][1].ToString();
                if (sMainMenuCaption_new == sMainMenuCaption_old)
                {
                    //Sample data of Child Menu collection/List  i.e. "11~Incidentchild~Incident_Data_Entry.aspx~1"
                    string sChildItem;
                    sChildItem = ds.Tables[0].Rows[i][4].ToString() + "~" + ds.Tables[0].Rows[i][5].ToString() + "~" + ds.Tables[0].Rows[i][9].ToString() + "~" + iCount;  //MainMenu capion and its ID
                    childitems.Add(sChildItem);
                }
                else
                {
                    iCount++;
                    string sMainItem;
                    sMainItem = sMainMenuCaption_new + "~" + ds.Tables[0].Rows[i][0].ToString();  //MainMenu capion and its ID
                    mainmenu.Add(sMainItem);

                    //Sample data of Child Menu collection/List  i.e. "11~Incidentchild~Incident_Data_Entry.aspx~1"
                    string sChildItem;
                    sChildItem = ds.Tables[0].Rows[i][4].ToString() + "~" + ds.Tables[0].Rows[i][5].ToString() + "~" + ds.Tables[0].Rows[i][9].ToString() + "~" + iCount;  //MainMenu capion and its ID
                    childitems.Add(sChildItem);

                    sMainMenuCaption_old = sMainMenuCaption_new;
                }
            }

            con.Close();
            return "YES";
        }



        protected void cmdHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("launchpage.aspx");
        }

        protected void cmdLogout_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Session["Login_Name"] = "";
            Session["EmployeeName"] = "";
        }


        private void BindGridview()
        {
            try
            {
                string Query = " ";
                Query += " SELECT ID,NAME,ISNULL(TABLEA.IS_HIDE,0) as IS_HIDE FROM [COMMENT]   ";
                Query += " LEFT JOIN                                                           ";
                Query += " (                                                                   ";
                Query += " SELECT  [SERVICE_ID], IS_HIDE FROM DEPT_SERVICES                    ";
                Query += " WHERE dept_id='" + ddl_dept.SelectedValue + "'                      ";
                Query += " )TABLEA                                                             ";
                Query += " ON TABLEA.[SERVICE_ID]=[COMMENT].ID                                 ";
                DataSet dsComment = my.ExecuteSELECTQuery(Query);
                if (dsComment.Tables[0].Rows.Count != 0)
                {
                    grid_dept.DataSource = null;
                    grid_dept.DataBind();
                    grid_dept.DataSource = dsComment;
                    grid_dept.DataBind();
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }






        protected void chkbox_Headers_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = (CheckBox)sender;
                GridViewRow gr = (GridViewRow)chk.Parent.Parent;
                int currentRowIndex = int.Parse(gr.RowIndex.ToString());

                CheckBox ChkBoxHeader = (CheckBox)grid_dept.HeaderRow.FindControl("chkbox_Headers");
                foreach (GridViewRow row in grid_dept.Rows)
                {
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkbox_confgrd");
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                    }
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Please try again,Error massage " + ex.Message + "');", true);

            }
        }

        protected void btn_all_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("dept_id");
            dt.Columns.Add("service_id");
            int numberofRow1 = grid_dept.Rows.Count;
            for (int i = 0; i < numberofRow1; i++)
            {
                CheckBox chk = (CheckBox)grid_dept.Rows[i].FindControl("chkbox_confgrd");
                if (chk.Checked == true)
                {
                    HiddenField hdn_Id = (HiddenField)grid_dept.Rows[i].FindControl("hdn_ids");
                    string query = "select [service_id] from [Dept_services] where [service_id]='" + hdn_Id.Value + "' and dept_id='" + ddl_dept.SelectedValue + "'";
                    DataSet ds = (DataSet)my.ExecuteSELECTQuery(query);
                    if ( ds.Tables[0].Rows.Count == 0)
                        dt.Rows.Add(ddl_dept.SelectedValue.ToString(), hdn_Id.Value);
                }
            }
            if (dt != null && dt.Rows.Count > 0)
            {

                bool response = my.insertBulkData(dt, "Dept_services");
                if (response)
                    WebMsgApp.WebMsgBox.Show("Data has been saved succesfully.");
                else
                    WebMsgApp.WebMsgBox.Show("Data is not saved !");
            }
        }

        protected void ddl_dept_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGridview();
        }



    }
}