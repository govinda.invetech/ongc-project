﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services
{
    public partial class editandupdateuserdetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cpf_no = Request["cpf_no"];
            if (string.IsNullOrEmpty(cpf_no) || cpf_no == "null") { 
                return;
            }
            string type = Request["type"];
            if (checkcpfno_cybuzz(cpf_no) == "FALSE" && type!="UPDATE")
            {
                Response.Write("FALSE~ERR~Invalid CPF NO");
                return;
            }
            else { 
            
            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
             string sStr ="";
            if (type == "UPDATE") {
                sStr = "SELECT O_CPF_NMBR, O_EMP_NM, O_EMP_HM_ADDR1, O_EMP_HM_ADDR2, O_EMP_HM_CITY, O_EMP_HM_STATE, O_EMP_HM_PIN_CD, ";
                sStr = sStr + " O_EMP_HM_PHN, O_EMP_MBL_NMBR, O_EMP_GNDR, O_EMP_DOB , O_EMAIL_ID1, O_EMAIL_ID2, O_EMAIL_ID3  ";
                sStr = sStr + "  , O_EMP_DESGNTN,O_EMP_SUP_NM, O_EMP_DEPT_NM, O_EMP_WRK_LCTN,CY_EMP_EX_No,O_EMP_BLD_GRP FROM  O_EMP_MSTR where O_CPF_NMBR='" + cpf_no + "'";
            } else {
                sStr = "SELECT O_CPF_NMBR, O_EMP_NM, O_EMP_HM_ADDR1, O_EMP_HM_ADDR2, O_EMP_HM_CITY, O_EMP_HM_STATE, O_EMP_HM_PIN_CD, ";
                sStr = sStr + " O_EMP_HM_PHN, O_EMP_MBL_NMBR, O_EMP_GNDR,cast([O_EMP_DOB] as date) , O_EMAIL_ID1, O_EMAIL_ID2, O_EMAIL_ID3  ";
                sStr = sStr + "  , O_EMP_DESGNTN,O_EMP_SUP_NM, O_EMP_DEPT_NM, O_EMP_WRK_LCTN,CY_EMP_EX_No,O_EMP_BLD_GRP FROM  CY_O_EMP_MSTR where O_CPF_NMBR='" + cpf_no + "'";
            }
          

            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            string output = "";
            if (ds.Tables[0].Rows.Count == 0) {
                Response.Write("FALSE~SUCCESS~Detail Not Found Of Selected Employee");
                return;
            }
   //ds.Tables[0].Rows[i][0].ToString() 
            string my_date = DateTime.Parse(ds.Tables[0].Rows[0][10].ToString()).ToString("dd-MM-yyyy");
            output = output + "TRUE~SUCCESS~detail Found Successfully !~" + ds.Tables[0].Rows[0][0].ToString()+"~"+ds.Tables[0].Rows[0][1].ToString();
            output = output + "~" + ds.Tables[0].Rows[0][2].ToString() + "~" + ds.Tables[0].Rows[0][3].ToString() + "~" + ds.Tables[0].Rows[0][4].ToString() + "~" + ds.Tables[0].Rows[0][5].ToString() + "~" + ds.Tables[0].Rows[0][6].ToString() + "~" + ds.Tables[0].Rows[0][7].ToString() + "~" + ds.Tables[0].Rows[0][8].ToString() + "~" + ds.Tables[0].Rows[0][9].ToString() + "~" + my_date.ToString() + "~" + ds.Tables[0].Rows[0][11].ToString() + "~" + ds.Tables[0].Rows[0][12].ToString() + "~" + ds.Tables[0].Rows[0][13].ToString() + "~" + ds.Tables[0].Rows[0][14].ToString() + "~";

            output = output + ds.Tables[0].Rows[0][15].ToString() + "~" + ds.Tables[0].Rows[0][16].ToString() + "~" + ds.Tables[0].Rows[0][17].ToString() + "~" + ds.Tables[0].Rows[0][18].ToString() + "~" + ds.Tables[0].Rows[0][19].ToString() + "~";
                Response.Write(output);


            
        }
        // validate cpf_no
        private string checkcpfno_cybuzz(string cpf_no)
        {
            
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_CPF_NMBR]FROM [CY_O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }

        }

       
    }
}