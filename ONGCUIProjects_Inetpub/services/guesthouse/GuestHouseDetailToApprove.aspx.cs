﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.guesthouse
{
    public partial class GuestHouseDetailToApprove : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sActionType = "";
                if (string.IsNullOrEmpty(Request.QueryString["type"]))
                {

                }
                else
                {
                    sActionType = Request.QueryString["type"].ToString();
                }
                string cpf_no = Session["Login_Name"].ToString();
                txtSessionCPF.Value = cpf_no;

                string sBookingID = My.sDataStringTrim(2, sActionType);
                txtBookingID.Value = sBookingID;

                string sViewEditQuery = "SELECT DISTINCT([id]),[APP_NAME],[APP_CPF_NO],[APP_DESIG] ,[APP_LOCATION],[APP_EMP_EXTENSION_NO] ,[BOOKING_TYPE],[BOOKING_START_DATETIME],[BOOKING_END_DATETIME],[REMARKS],[PURPOSE] ,[FOOD_TYPE],[no_of_veg],[no_of_nonveg],[INCHARGE_CPF_NO],[INCHARGE_NAME],[INCHARGE_DESIG],[INCHARGE_LOCATION],[INCHARGE_EXT_NO],[INCHARGE_STATUS],[APPROVER_CPF_NO],[APPROVER_NAME],[APPROVER_DESIG],[APPROVER_EXT_NO],[APPROVER_STATUS],[ALLOCATER_CPF_NO],[ALLOCATER_NAME],[ALLOCATER_DESIG],[ALLOCATER_EXT_NO],[ALLOCATTER_STATUS] ,[REMARKS_ALLOCATE],[ROOM_ALLOCATE],[ENTRY_BY],[TIMESTAMP] FROM [CY_GUEST_HOUSE_BOOKING_DETAIL] where [id]='" + sBookingID + "'";

                DataSet dsViewUpdate = My.ExecuteSELECTQuery(sViewEditQuery);

                #region Applicant Detail
                txtappname.Value = dsViewUpdate.Tables[0].Rows[0][1].ToString();
                txtappcpf_no.Value = dsViewUpdate.Tables[0].Rows[0][2].ToString();
                txtappdesign.Value = dsViewUpdate.Tables[0].Rows[0][3].ToString();
                txtapplocation.Value = dsViewUpdate.Tables[0].Rows[0][4].ToString();
                txtappphoneex_no.Value = dsViewUpdate.Tables[0].Rows[0][5].ToString();

                #endregion
                #region bad main
                HiddenField1.Value = dsViewUpdate.Tables[0].Rows[0][6].ToString();
                ddltypeofbooking.Value = dsViewUpdate.Tables[0].Rows[0][6].ToString();
                DateTime dtBookingDateTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][7]);
                txtbookingstartdatetime.Value = dtBookingDateTime.ToString("dd-MM-yyyy HH:mm");
                DateTime dtEndDateTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][8]);
                txtbookingEnddatetime.Value = dtEndDateTime.ToString("dd-MM-yyyy HH:mm");

                txtremarks.Value = dsViewUpdate.Tables[0].Rows[0][9].ToString();
                txtpurpose.Value = dsViewUpdate.Tables[0].Rows[0][10].ToString();
                type_of_food.Value = dsViewUpdate.Tables[0].Rows[0][11].ToString();
                no_of_veg.Value = dsViewUpdate.Tables[0].Rows[0][12].ToString();
                no_of_nonveg.Value = dsViewUpdate.Tables[0].Rows[0][13].ToString();
                #endregion

                #region Approving Authorities
                string sInchargeCPF = dsViewUpdate.Tables[0].Rows[0][14].ToString();
                string sInchargeName = dsViewUpdate.Tables[0].Rows[0][15].ToString();
                string sInchargeDesig = dsViewUpdate.Tables[0].Rows[0][16].ToString();
                string sInchargeStatus = dsViewUpdate.Tables[0].Rows[0][19].ToString();

                string sApproverCPF = dsViewUpdate.Tables[0].Rows[0][20].ToString();
                string sApproverName = dsViewUpdate.Tables[0].Rows[0][21].ToString();
                string sApproverDesig = dsViewUpdate.Tables[0].Rows[0][22].ToString();
                string sApproverStatus = dsViewUpdate.Tables[0].Rows[0][24].ToString();

                string sAlloterCPF = dsViewUpdate.Tables[0].Rows[0][25].ToString();
                string sAlloterName = dsViewUpdate.Tables[0].Rows[0][26].ToString();
                string sAlloterDesig = dsViewUpdate.Tables[0].Rows[0][27].ToString();
                string sAlloterStatus = dsViewUpdate.Tables[0].Rows[0][29].ToString();

                string sApprovingAuth = "";
                sApprovingAuth = "<table class=\"tftable\">";
                #endregion
                #region Incharge Row
                txtInchargeStatus.Value = sInchargeCPF + "~" + sInchargeStatus;
                sApprovingAuth += "<tr>";
                sApprovingAuth += "<td>Incharge Name & CPF No : " + sInchargeName + " (" + sInchargeCPF + ")</td>";
                sApprovingAuth += "<td>Designation : " + sInchargeDesig + "</td>";
                sApprovingAuth += "<td style='text-align:center'>";
                switch (sInchargeStatus)
                {
                    case "PENDING":
                        sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-red approve_reject\" value=\"REJECT\" hint=\"REJECT\" type_desig=\"INCHARGE\" />";
                        sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-submit approve_reject\" value=\"APPROVE\" hint=\"APPROVE\" type_desig=\"INCHARGE\" />";
                        break;
                    case "APPROVE":
                        sApprovingAuth += "<img src=\"Images/tick.png\">";
                        break;
                    case "REJECT":
                        sApprovingAuth += "<img src=\"Images/cross.png\">";
                        break;
                }
                sApprovingAuth += "</td>";
                sApprovingAuth += "</tr>";
                #endregion

                #region Approver Row
                txtApproverStatus.Value = sApproverCPF + "~" + sApproverStatus;
                sApprovingAuth += "<tr>";
                sApprovingAuth += "<td>Approver Name & CPF No : " + sApproverName + " (" + sApproverCPF + ")</td>";
                sApprovingAuth += "<td>Designation : " + sApproverDesig + "</td>";
                sApprovingAuth += "<td style='text-align:center'>";
                if (sInchargeStatus == "REJECT")
                {
                    sApprovingAuth += "Incharge has been rejected";
                }
                else
                {
                    switch (sApproverStatus)
                    {
                        case "PENDING":
                            sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-red approve_reject\" value=\"REJECT\" hint=\"REJECT\" type_desig=\"APPROVER\" />";
                            sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-submit approve_reject\" value=\"APPROVE\" hint=\"APPROVE\" type_desig=\"APPROVER\" />";
                            break;
                        case "APPROVE":
                            sApprovingAuth += "<img src=\"Images/tick.png\">";
                            break;
                        case "DIRECT":
                            sApprovingAuth += "<img src=\"Images/tick.png\">";
                            break;
                        case "REJECT":
                            sApprovingAuth += "<img src=\"Images/cross.png\">";
                            break;
                    }
                }
                sApprovingAuth += "</td>";
                sApprovingAuth += "</tr>";
                #endregion

                #region Alloter Row
                txtAlloterStatus.Value = sAlloterCPF + "~" + sAlloterStatus;
                sApprovingAuth += "<tr>";
                sApprovingAuth += "<td>Alloter Name & CPF No : " + sAlloterName + " (" + sAlloterCPF + ")</td>";
                sApprovingAuth += "<td>Designation : " + sAlloterDesig + "</td>";
                sApprovingAuth += "<td style='text-align:center'>";
                if (sInchargeStatus == "REJECT")
                {
                    sApprovingAuth += "Incharge has been rejected";
                }
                else
                {
                    if (sApproverStatus == "REJECT")
                    {
                        sApprovingAuth += "Approver has been rejected";
                    }
                    else
                    {
                        switch (sAlloterStatus)
                        {
                            case "PENDING":
                                sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-red approve_reject\" value=\"NOT AVAILABLE\" hint=\"REJECT\" type_desig=\"ALLOTER\" />";
                                sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-submit approve_reject\" value=\"ALLOT\" hint=\"APPROVE\" type_desig=\"ALLOTER\" />";
                                break;
                            case "APPROVE":
                                sApprovingAuth += "<img src=\"Images/tick.png\">";
                                break;
                            case "REJECT":
                                sApprovingAuth += "<img src=\"Images/cross.png\">";
                                break;
                        }
                    }
                }
                sApprovingAuth += "</td>";
                sApprovingAuth += "</tr>";
                #endregion

                sApprovingAuth += "</table>";
                div_approving_authorities.InnerHtml = sApprovingAuth;
              

                #region Remarks and Room Detail
                txtAlloterRemarks.Value = dsViewUpdate.Tables[0].Rows[0][30].ToString();
                txtRoomNo.Value = dsViewUpdate.Tables[0].Rows[0][31].ToString();
                #endregion
            }

        }
    }
}
