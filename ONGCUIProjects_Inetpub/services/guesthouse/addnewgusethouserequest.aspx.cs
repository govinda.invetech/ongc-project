﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services
{
    public partial class addnewgusethouserequest : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            string module_type = Request.QueryString["module_type"];
            string my_index_no = Request.QueryString["my_index_no"];
            string app_name = Request.QueryString["app_name"];
            string app_cpf_no = Request.QueryString["app_cpf_no"];
            string app_design = Request.QueryString["app_design"];
            string app_loc = Request.QueryString["app_loc"];
            string app_ph_ex_no = Request.QueryString["app_ph_ex_no"];
            string type_booking = Request.QueryString["type_booking"];
            string booking_time = Request.QueryString["booking_start_datetime"];
            string end_time = Request.QueryString["booking_end_datetime"];
            DateTime booking_start_datetime = DateTime.Now;
            DateTime booking_end_datetime = DateTime.Now;
            if (type_booking != "Food")
            {
                booking_start_datetime = System.Convert.ToDateTime(My.ConvertDateStringintoSQLDateString(booking_time));
                booking_end_datetime = System.Convert.ToDateTime(My.ConvertDateStringintoSQLDateString(booking_time));
            }
            string remarks = Request.QueryString["remarks"];
            string purpose = Request.QueryString["purpose"];
            //approver ndetail
            string approver_name = Request.QueryString["approver_name"];
            string approver_cpf_no = Request.QueryString["approver_cpf_no"];
            string approver_desig = Request.QueryString["approver_desig"];
            //incharge detail
            string incharge_name = Request.QueryString["incharge_name"];
            string incharge_cpf_no = Request.QueryString["incharge_cpf_no"];
            string incharge_desig = Request.QueryString["incharge_desig"];


            ////allotemet detail
            string allotemet_name = Request.QueryString["allotemet_name"];
            string allotemet_cpf_no = Request.QueryString["allotemet_cpf_no"];
            string allotemet_desig = Request.QueryString["allotemet_desig"];
            string type_of_food = Request.QueryString["type_of_food"];
            string no_of_veg = Request.QueryString["no_of_veg"];
            string no_of_nonveg = Request.QueryString["no_of_nonveg"];
            string cpf_no = Session["Login_Name"].ToString();





            //if(System.DateTime.Now>System.Convert.ToDateTime(booking_start_datetime)){
            // Response.Write("FALSE~ERR~Booking Start Date And Time Must Be Equal to or Greater Than Today Date And Time");
            //    return;
            //}
            if (type_booking == "Conference Hall")
            {
                if (checkavaibility(booking_time) == "TRUE" || checkavaibility(end_time) == "TRUE")
                {

                    Response.Write("FALSE~ERR~Conference Hall has already been booked for the time slot. Please Select another Time Slot");
                    return;

                }
                type_of_food = "";
            }


            if (checkcpfno(app_cpf_no) == "FALSE")
            {
                Response.Write("FALSE~ERR~Invalid Applicant CPF No.");
                return;

            }
            else if (checkcpfno(approver_cpf_no) == "FALSE")
            {
                Response.Write("FALSE~ERR~Invalid Approver CPF No.");
                return;

            }
            else if (checkcpfno(incharge_cpf_no) == "FALSE")
            {
                Response.Write("FALSE~ERR~Invalid incharge CPF No.");
                return;

            }
            else if (checkcpfno(allotemet_cpf_no) == "FALSE")
            {
                Response.Write("FALSE~ERR~Invalid Alloter officer CPF No.");
                return;

            }


            try
            {
                string sUpdate = "";

                if (module_type == "NEW")
                {
                    sUpdate = "INSERT INTO [CY_GUEST_HOUSE_BOOKING_DETAIL]  ([APP_CPF_NO] ,[APP_NAME] ,[APP_DESIG]  ,[APP_LOCATION],[APP_EMP_EXTENSION_NO] ,[BOOKING_TYPE] ,[BOOKING_START_DATETIME],[BOOKING_END_DATETIME] ,[FOOD_TYPE] ,[PURPOSE] ,[INCHARGE_STATUS],[APPROVER_STATUS],[ALLOCATTER_STATUS],[INCHARGE_CPF_NO],[INCHARGE_NAME],[INCHARGE_DESIG] ,[APPROVER_CPF_NO] ,[APPROVER_NAME] ,[APPROVER_DESIG] ,[ALLOCATER_CPF_NO] ,[ALLOCATER_NAME] ,[ALLOCATER_DESIG] ,[ENTRY_BY] ,[TIMESTAMP],[REMARKS],[no_of_veg],[no_of_nonveg])";
                    sUpdate = sUpdate + "VALUES ('" + app_cpf_no + "','" + app_name + "','" + app_design + "','" + app_loc + "','" + app_ph_ex_no + "','" + type_booking + "','" + My.ConvertDateStringintoSQLDateString(booking_time) + "','" + My.ConvertDateStringintoSQLDateString(end_time) + "','" + type_of_food + "','" + purpose + "','PENDING','PENDING','PENDING','" + incharge_cpf_no + "','" + incharge_name + "','" + incharge_desig + "','" + approver_cpf_no + "','" + approver_name + "','" + approver_desig + "','" + allotemet_cpf_no + "','" + allotemet_name + "','" + allotemet_desig + "','" + cpf_no + "',GETDATE(),'" + remarks + "','" + no_of_veg + "','" + no_of_nonveg + "') ";
                }
                else
                {
                    sUpdate = "UPDATE [CY_GUEST_HOUSE_BOOKING_DETAIL] SET [BOOKING_TYPE] = '" + type_booking + "',[BOOKING_START_DATETIME] = '" + My.ConvertDateStringintoSQLDateString(booking_time) + "',[BOOKING_END_DATETIME]='" + My.ConvertDateStringintoSQLDateString(end_time) + "',[FOOD_TYPE] ='" + type_of_food + "',[no_of_veg]='" + no_of_veg + "',[no_of_nonveg]='" + no_of_nonveg + "',[REMARKS] = '" + remarks + "' ,[PURPOSE] = '" + purpose + "' ,[INCHARGE_CPF_NO] = '" + incharge_cpf_no + "',[INCHARGE_NAME] = '" + incharge_name + "',[INCHARGE_DESIG] ='" + incharge_desig + "',[APPROVER_CPF_NO] ='" + approver_cpf_no + "',[APPROVER_NAME] ='" + approver_name + "',[APPROVER_DESIG] = '" + approver_desig + "',[ALLOCATER_CPF_NO] = '" + allotemet_cpf_no + "',[ALLOCATER_NAME] = '" + allotemet_name + "',[ALLOCATER_DESIG] ='" + allotemet_desig + "',[ENTRY_BY] = '" + cpf_no + "' ,[TIMESTAMP] = GETDATE() WHERE [id]='" + my_index_no + "'";

                }

                if (My.ExecuteSQLQuery(sUpdate) == true)
                {
                    if (module_type == "NEW")
                    {
                        Response.Write("TRUE~SUCCESS~Your Guest House Booking Request Sended Successfully !");
                    }
                    else
                    {
                        Response.Write("TRUE~SUCCESS~Your Guest House Booking Request Updated Successfully !");
                    }
                }
                else //else case of execute update query
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *GUESTHOUSE403");
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends


        }
        private string checkavaibility(string dateandtime)
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "select * from [CY_GUEST_HOUSE_BOOKING_DETAIL] where ( [BOOKING_START_DATETIME] >'" + My.ConvertDateStringintoSQLDateString(dateandtime) + "' OR [BOOKING_END_DATETIME] >'" + My.ConvertDateStringintoSQLDateString(dateandtime) + "') and [INCHARGE_STATUS]='APPROVE' and [APPROVER_STATUS]='APPROVE' and [ALLOCATTER_STATUS]='APPROVE'";
            da = new SqlDataAdapter(sStr, conn);

            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }
        }
        private string checkcpfno(string cpf_no)
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_CPF_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }
        }
    }
}