﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace ONGCUIProjects.services.guesthouse
{
    public partial class print_GuestHouse : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sBookingID = Request.QueryString["id"].ToString();
                string sQuery = "SELECT DISTINCT([id]),[APP_NAME],[APP_CPF_NO],[APP_DESIG] ,[APP_LOCATION],[APP_EMP_EXTENSION_NO] ,[BOOKING_TYPE],[BOOKING_START_DATETIME],[BOOKING_END_DATETIME],[REMARKS],[PURPOSE] ,[FOOD_TYPE],[no_of_veg],[no_of_nonveg],[INCHARGE_CPF_NO],[INCHARGE_NAME],[INCHARGE_DESIG],[INCHARGE_LOCATION],[INCHARGE_EXT_NO],[INCHARGE_STATUS],[APPROVER_CPF_NO],[APPROVER_NAME],[APPROVER_DESIG],[APPROVER_EXT_NO],[APPROVER_STATUS],[ALLOCATER_CPF_NO],[ALLOCATER_NAME],[ALLOCATER_DESIG],[ALLOCATER_EXT_NO],[ALLOCATTER_STATUS] ,[REMARKS_ALLOCATE],[ROOM_ALLOCATE],[ENTRY_BY],[TIMESTAMP],[INCHARGE_TIMESTAMP],[APPROVER_TIMESTAMP],[ALLOCATTER_TIMESTAMP] FROM [CY_GUEST_HOUSE_BOOKING_DETAIL] where [id]='" + sBookingID + "'";
                DataSet ds= My.ExecuteSELECTQuery(sQuery);

                string sAppCPF = ds.Tables[0].Rows[0]["APP_CPF_NO"].ToString();
                string sAppName = ds.Tables[0].Rows[0]["APP_NAME"].ToString();
                string sAppDesig = ds.Tables[0].Rows[0]["APP_DESIG"].ToString();
                string sExNo = ds.Tables[0].Rows[0]["APP_EMP_EXTENSION_NO"].ToString();
                string sAppLocation = ds.Tables[0].Rows[0]["APP_LOCATION"].ToString();
                string sBookingType = ds.Tables[0].Rows[0]["BOOKING_TYPE"].ToString();
                DateTime sBookingDate =System.Convert.ToDateTime(ds.Tables[0].Rows[0]["BOOKING_START_DATETIME"].ToString());
                DateTime sEndDate = System.Convert.ToDateTime(ds.Tables[0].Rows[0]["BOOKING_END_DATETIME"].ToString());
                string sPurpose = ds.Tables[0].Rows[0]["PURPOSE"].ToString();
                string sPlaceToVisit = ds.Tables[0].Rows[0]["REMARKS"].ToString();

                string sfoodType = ds.Tables[0].Rows[0]["FOOD_TYPE"].ToString();
                string sVeg = ds.Tables[0].Rows[0]["no_of_veg"].ToString();
                string sNonVeg = ds.Tables[0].Rows[0]["no_of_nonveg"].ToString();
            
                string sInchargeCPF = ds.Tables[0].Rows[0]["INCHARGE_CPF_NO"].ToString();
                string sInchargeName = ds.Tables[0].Rows[0]["INCHARGE_NAME"].ToString();
                string sInchargeDesig = ds.Tables[0].Rows[0]["INCHARGE_DESIG"].ToString();
                string sInchargeStatus = ds.Tables[0].Rows[0]["INCHARGE_STATUS"].ToString();
                DateTime dtInchargeTimestamp = Convert.ToDateTime(ds.Tables[0].Rows[0]["INCHARGE_TIMESTAMP"]);
                string sApproverCPF = ds.Tables[0].Rows[0]["APPROVER_CPF_NO"].ToString();
                string sApproverName = ds.Tables[0].Rows[0]["APPROVER_NAME"].ToString();
                string sApproverDesig = ds.Tables[0].Rows[0]["APPROVER_DESIG"].ToString();
                string sApproverStatus = ds.Tables[0].Rows[0]["APPROVER_STATUS"].ToString();
                DateTime dtApproverTimestamp = Convert.ToDateTime(ds.Tables[0].Rows[0]["APPROVER_TIMESTAMP"]);
                string sAllocaterStatus = ds.Tables[0].Rows[0]["ALLOCATTER_STATUS"].ToString();
              
                string sRoomNo = ds.Tables[0].Rows[0]["ROOM_ALLOCATE"].ToString();
                string sAlloterRemarks = ds.Tables[0].Rows[0]["REMARKS_ALLOCATE"].ToString();
                td_app_name.InnerHtml = sAppName;
                td_app_desig_cpf.InnerHtml = sAppDesig + "(" + sAppCPF + ")";
                td_app_contact.InnerHtml = sExNo;
                td_app_section.InnerHtml = sAppLocation;
                td_purpose.InnerHtml = sPurpose;
                if (sBookingType == "Food")
                {
                    tr_td_date_time.Visible = false;
                    food_detail.InnerHtml = sfoodType+" / Veg:"+sVeg+" , NonVeg :"+sNonVeg;
                }
                else {
                    tr_food_detail.Visible = false;
                    td_date_time.InnerHtml = sBookingDate.ToString("d MMM, yyyy") + " at " + sBookingDate.ToString("HH:mm") + " To " + sEndDate.ToString("d MMM, yyyy") + " at " + sEndDate.ToString("HH:mm");
                }
               
                td_inc_name.InnerHtml = sInchargeName;
                td_inc_desig.InnerHtml = sInchargeDesig;
                td_inc_timestamp.InnerHtml = dtInchargeTimestamp.ToString("dd/MM/yyyy HH:mm") + " Hrs";
                td_approver_name.InnerHtml = sApproverName;
                td_approver_desig.InnerHtml = sApproverDesig;
                td_approver_timestamp.InnerHtml = dtApproverTimestamp.ToString("dd/MM/yyyy HH:mm") + " Hrs";
                alloter_remarks.InnerHtml = sAlloterRemarks;
                if (sBookingType == "Room")
                {
                    td_room_no.InnerHtml =" , Room No-"+sRoomNo;
                }
                else {
                    td_room_no.Visible = false;
                }

            }
        }
    }
}