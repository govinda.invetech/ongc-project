﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class approveorrejectrequestofguesthouse : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {

            string sSessionCPF = "";
            if (Session["Login_Name"] == null)
            {
                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                try
                {
                    sSessionCPF = Session["Login_Name"].ToString();
                    string type = Request.Form["type"];
                    string sBookingID = Request.Form["BookingID"];
                    string type_desig = Request.Form["type_desig"];
                    string sBookingType = Request.Form["sBookingType"];
                    string txtStartdatetime = Request.Form["txtStartdatetime"];
                    string txtEnddatetime = Request.Form["txtEnddatetime"];
                    string sViewEditQuery = "SELECT INCHARGE_CPF_NO, INCHARGE_STATUS, ";
                    sViewEditQuery += "APPROVER_CPF_NO, APPROVER_STATUS, ";
                    sViewEditQuery += "ALLOCATER_CPF_NO, ALLOCATTER_STATUS ";
                    sViewEditQuery += "FROM CY_GUEST_HOUSE_BOOKING_DETAIL WHERE id = '" + sBookingID + "'";

                    DataSet dsViewUpdate = My.ExecuteSELECTQuery(sViewEditQuery);

                    string sInchargeCPF = dsViewUpdate.Tables[0].Rows[0][0].ToString();
                    string sInchargeStatus = dsViewUpdate.Tables[0].Rows[0][1].ToString();


                    string sApproverCPF = dsViewUpdate.Tables[0].Rows[0][2].ToString();
                    string sApproverStatus = dsViewUpdate.Tables[0].Rows[0][3].ToString();


                    string sAlloterCPF = dsViewUpdate.Tables[0].Rows[0][4].ToString();
                    string sAlloterStatus = dsViewUpdate.Tables[0].Rows[0][5].ToString();

                    string sOutput = "";
                    if (sBookingType == "Conference Hall" && type=="APPROVE")
                    {
                        if (checkavaibility(txtStartdatetime) == "TRUE" || checkavaibility(txtEnddatetime) == "TRUE")
                        {

                            Response.Write("FALSE~ERR~Conference Hall has already been booked for the time slot. You Can't Approve");
                            return;

                        }
                        
                    }
            
                    switch (type_desig)
                    {
                        case "INCHARGE":
                            if (sInchargeCPF == sSessionCPF)
                            {
                                if (UpdateGuestHouseStatus(type_desig, type, sBookingID) == "TRUE")
                                {
                                    if (type == "APPROVE")
                                    {
                                        sOutput = "TRUE~SUCCESS~<img src=\"Images/tick.png\">~Request has been approved Successfully.";
                                    }
                                    else if (type == "REJECT")
                                    {
                                        sOutput = "TRUE~SUCCESS~<img src=\"Images/cross.png\">~Request has been rejected Successfully.";
                                    }
                                }
                                else
                                {
                                    sOutput = "FALSE~ERR~MyApp oocured error ! ERR CODE : *APPREJGMS";
                                }
                            }
                            else
                            {
                                sOutput = "FALSE~ERR~You are not authorised to Approve/Reject this request.";
                            }
                            break;
                        case "APPROVER":
                            if (sApproverCPF == sSessionCPF)
                            {
                                if (sInchargeStatus == "APPROVE")
                                {
                                    if (UpdateGuestHouseStatus(type_desig, type, sBookingID) == "TRUE")
                                    {
                                        if (type == "APPROVE")
                                        {
                                            sOutput = "TRUE~SUCCESS~<img src=\"Images/tick.png\">~Request has been approved Successfully.";
                                        }
                                        else if (type == "REJECT")
                                        {
                                            sOutput = "TRUE~SUCCESS~<img src=\"Images/cross.png\">~Request has been rejected Successfully.";
                                        }
                                    }
                                }
                                else
                                {
                                    sOutput = "FALSE~ERR~This request is still pending or rejected from Incharge.";
                                }
                            }
                            else
                            {
                                sOutput = "FALSE~ERR~You are not authorised to Approve/Reject this request.";
                            }
                            break;
                        case "ALLOTER":
                            if (sAlloterCPF == sSessionCPF)
                            {
                                if (sInchargeStatus == "APPROVE")
                                {
                                    if (type == "APPROVE")
                                    {
                                        string txtRoomNo = Request.Form["txtRoomNo"];
                                        string txtAlloterRemrks = Request.Form["txtAlloterRemrks"];
                                       
                                        if (sApproverStatus == "PENDING")
                                        {
                                            if (UpdateGuestHouseStatus("APPROVER", "DIRECT", sBookingID) == "TRUE")
                                            {
                                                if (UpdateGuestHouseStatus(type_desig, type, sBookingID) == "TRUE")
                                                {
                                                    if (UpdateRoomStatus(txtRoomNo, txtAlloterRemrks, sBookingID) == "TRUE")
                                                    {
                                                        sOutput = "TRUE~SUCCESS~<img src=\"Images/tick.png\">~Request has been approved Successfully.";
                                                    }
                                                    else
                                                    {
                                                        sOutput = "FALSE~ERR~This request is still pending or rejected from Incharge.";
                                                    }
                                                }
                                                else
                                                {
                                                    sOutput = "FALSE~ERR~This request is still pending or rejected from Incharge.";
                                                }
                                            }
                                            else
                                            {
                                                sOutput = "FALSE~ERR~This request is still pending or rejected from Incharge.";
                                            }
                                        }
                                        else
                                        {
                                            if (UpdateGuestHouseStatus(type_desig, type, sBookingID) == "TRUE")
                                            {
                                                if (UpdateRoomStatus(txtRoomNo, txtAlloterRemrks , sBookingID) == "TRUE")
                                                {
                                                    sOutput = "TRUE~SUCCESS~<img src=\"Images/tick.png\">~Request has been rejected Successfully.";
                                                }
                                                else
                                                {
                                                    sOutput = "FALSE~ERR~This request is still pending or rejected from Incharge.";
                                                }
                                            }
                                            else
                                            {
                                                sOutput = "FALSE~ERR~This request is still pending or rejected from Incharge.";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (UpdateGuestHouseStatus(type_desig, type, sBookingID) == "TRUE")
                                        {
                                            sOutput = "TRUE~SUCCESS~<img src=\"Images/cross.png\">~Request has been rejected Successfully.";

                                        }
                                    }
                                }
                                else
                                {
                                    sOutput = "This request is still pending or rejected from Incharge.";
                                }
                            }
                            else
                            {
                                sOutput = "You are not authorised to allot the " + sBookingType + " to this request.";
                            }
                            break;
                    }
                    Response.Write(sOutput);
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }

            }
        }
        private string UpdateGuestHouseStatus(string ApproverType, string UpdateText, string BookingID)
        {
            string sQuery = "";
            string sOutput = "";
            switch (ApproverType)
            {
                case "INCHARGE":
                    sQuery = "UPDATE [CY_GUEST_HOUSE_BOOKING_DETAIL] SET [INCHARGE_STATUS] = '" + UpdateText + "',[INCHARGE_TIMESTAMP] =GETDATE()  WHERE [id]='" + BookingID + "'";
                    break;
                case "APPROVER":
                    sQuery = "UPDATE [CY_GUEST_HOUSE_BOOKING_DETAIL] SET [APPROVER_STATUS] = '" + UpdateText + "',[APPROVER_TIMESTAMP] =GETDATE()  WHERE [id]='" + BookingID + "'";
                    break;
                case "ALLOTER":
                    sQuery = "UPDATE [CY_GUEST_HOUSE_BOOKING_DETAIL] SET [ALLOCATTER_STATUS] = '" + UpdateText + "',[ALLOCATTER_TIMESTAMP] =GETDATE()  WHERE [id]='" + BookingID + "'";
                    break;
            }
            if (sQuery != "")
            {
                if (My.ExecuteSQLQuery(sQuery) == true)
                {
                    sOutput = "TRUE";
                }
                else
                {
                    sOutput = "FALSE";
                }
            }
            return sOutput;
        }
        private string UpdateRoomStatus(string txtRoomNo, string txtAlloterRemarks , string BookingID)
        {
            string sOutput = "";
            string sQuery = "UPDATE [CY_GUEST_HOUSE_BOOKING_DETAIL] SET [ROOM_ALLOCATE] = '" + txtRoomNo + "', [REMARKS_ALLOCATE] = '" + txtAlloterRemarks + "'  WHERE [id]='" + BookingID + "'";
            if (My.ExecuteSQLQuery(sQuery) == true)
            {
                sOutput = "TRUE";
            }
            else
            {
                sOutput = "FALSE";
            }
            return sOutput;
        }
        private string checkavaibility(string dateandtime)
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "select * from [CY_GUEST_HOUSE_BOOKING_DETAIL] where ( [BOOKING_START_DATETIME] >'" + My.ConvertDateStringintoSQLDateString(dateandtime) + "' OR [BOOKING_END_DATETIME] >'" + My.ConvertDateStringintoSQLDateString(dateandtime) + "') and [INCHARGE_STATUS]='APPROVE' and [APPROVER_STATUS]='APPROVE' and [ALLOCATTER_STATUS]='APPROVE'";
            da = new SqlDataAdapter(sStr, conn);

            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }
        }
    }
}