﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services
{
    public partial class AccountAction : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }

            string cpf_no = Session["Login_Name"].ToString();

            if (cpf_no == "")
            {
                Response.Write("FALSE~ERR~Invalid CPF Number loggined");
                return;
            }

            string sActionType = Request["type"].ToString();
            string sActionCPF = Request["user"].ToString();

            if (sActionType == "REJECT")
            {
                my.InsUpdEmpAccountStatus(sActionCPF, "REJECT");
                Response.Write("TRUE~SUCCESS~Account Rejected Successfully");
            }
            else if (sActionType == "ADMINDEACTIVE")
            {
                my.InsUpdEmpAccountStatus(sActionCPF, "ADMINDEACTIVE");
                Response.Write("TRUE~SUCCESS~Account De-Activated Successfully");
            }
            else if (sActionType == "ADMINACTIVE")
            {
                my.InsUpdEmpAccountStatus(sActionCPF, "ADMINACTIVE");
                Response.Write("TRUE~SUCCESS~Account Activated Successfully");
            }

        }
    }
}