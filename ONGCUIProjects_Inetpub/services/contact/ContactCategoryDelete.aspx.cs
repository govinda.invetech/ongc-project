﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.contact
{
    public partial class ContactCategoryDelete : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }

            string sCatID= Request.QueryString["catID"];
            
            string cpf_no = Session["Login_Name"].ToString();

            if (cpf_no == "")
            {
                Response.Write("FALSE~ERR~Invalid CPF Number loggined");
                return;
            }

            try
            {
                string sDelete1 = "DELETE FROM [CY_CONTACT_CATEGORY_DTLS] WHERE [id] =" + sCatID + "";

                if (My.ExecuteSQLQuery(sDelete1) == true)
                {
                    string sDelete2 = "DELETE FROM [CY_CONTACT_CATEGORY_RELATION] WHERE [category_id] =" + sCatID + "";
                    if (My.ExecuteSQLQuery(sDelete2) == true)
                    {
                        Response.Write("TRUE~SUCCESS~Category Deleted Successfully...!");
                    }
                }
                else //else case of execute update query
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *CC1298");
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends
        }
    }
}