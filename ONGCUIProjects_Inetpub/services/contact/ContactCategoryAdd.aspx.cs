﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.contact
{
    public partial class ContactCategoryAdd : System.Web.UI.Page
    {
        MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }

            string sCatName = Request.QueryString["catName"];
            int iCatOrder = System.Convert.ToInt32(Request.QueryString["catOrder"]);
            string cpf_no = Session["Login_Name"].ToString();

            if (cpf_no == "")
            {
                Response.Write("FALSE~ERR~Invalid CPF Number loggined");
                return;
            }

            Boolean bResult = false;
            try
            {
                string sUpdate = "UPDATE [CY_CONTACT_CATEGORY_DTLS] SET [order_by]=[order_by] + 1 WHERE [order_by] >" + (iCatOrder - 1) + "";

                if (My.ExecuteSQLQuery(sUpdate) == true)
                {
                    string sInsert = "INSERT INTO [CY_CONTACT_CATEGORY_DTLS] ([name], [order_by], [entry_by])  VALUES ('" + sCatName + "','" + iCatOrder + "','" + cpf_no + "')";
                    if (My.ExecuteSQLQuery(sInsert) == true)
                    {
                        Response.Write("TRUE~SUCCESS~Category Addedd Successfully...!");
                    }
                }
                else //else case of execute update query
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *CC1298");
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends
        }
    }
}