﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.contact
{
    public partial class checkCPF : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                try
                {
                    string sCPF_NO = Request["cpfno"];
                    string sQuery = "SELECT [O_EMP_NM] FROM [O_EMP_MSTR] WHERE [O_CPF_NMBR] = '" + sCPF_NO + "'";
                    DataSet ds = My.ExecuteSELECTQuery(sQuery);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        Response.Write("This CPF No is already registered with " + ds.Tables[0].Rows[0][0].ToString() + ". Please update the profile.");
                    }
                    else
                    {
                        string sQuery2 = "SELECT [name] FROM [CY_CONTACT_DTLS] WHERE [id] = '" + sCPF_NO + "'";
                        DataSet ds2 = My.ExecuteSELECTQuery(sQuery2);
                        if (ds2.Tables[0].Rows.Count != 0)
                        {
                            Response.Write("This CPF No is already exist with " + ds2.Tables[0].Rows[0][0].ToString());
                        }
                        else
                        {
                            Response.Write("TRUE");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }
    }
}