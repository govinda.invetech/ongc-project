﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class AddContact : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("<script type=\"text/javascript\">top.location = '../../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {

                string sSessionCPF = Session["Login_Name"].ToString();

                string sName = Request["name"];
                string sDesig = Request["desig"];
                string sExtn = Request["extn"];
                string sResi_d = Request["resi_d"];
                string sOff_d = Request["off_d"];
                string sMobile = Request["mobile"];
                string sEmail = Request["email"];
                string sEmail2 = Request["email2"];
                string ContactDept = Request["dept"];
                string sType = Request["ContactType"];

                try
                {
                    if (sType == "NONEMP")
                    {
                        string sCPF_NO = Request["cpfno"];
                        string sQuery = "SELECT [O_EMP_NM] FROM [O_EMP_MSTR] WHERE [O_CPF_NMBR] = '" + sCPF_NO + "'";
                        DataSet ds = My.ExecuteSELECTQuery(sQuery);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            Response.Write("FALSE~CPF~This CPF No is already registered with " + ds.Tables[0].Rows[0][0].ToString() + ". Please update the profile.");
                        }
                        else
                        {
                            string sQuery2 = "SELECT [name] FROM [CY_CONTACT_DTLS] WHERE [id] = '" + sCPF_NO + "'";
                            DataSet ds2 = My.ExecuteSELECTQuery(sQuery2);
                            if (ds2.Tables[0].Rows.Count != 0)
                            {
                                Response.Write("FALSE~CPF~This CPF No is already exist with " + ds.Tables[0].Rows[0][0].ToString());
                            }
                            else
                            {
                                string sQueryStringContact = "INSERT INTO [CY_CONTACT_DTLS] ([id], [name], [desig], [dept_name], [extn], [resi_d], [off_d], [mobile], [email],[email_2],[contact_type], [entry_by]) VALUES ";
                                sQueryStringContact = sQueryStringContact + "('" + sCPF_NO + "','" + sName + "','" + sDesig + "','" + ContactDept + "','" + sExtn + "','" + sResi_d + "','" + sOff_d + "','" + sMobile + "','" + sEmail + "','" + sEmail2 + "','" + sType + "','" + sSessionCPF + "')";

                                if (My.ExecuteSQLQuery(sQueryStringContact) == true)
                                {
                                    Response.Write("TRUE~SUCCESS~Contact Saved Successfully");
                                }
                                else //else case of execute update query
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ADDUPDCONTACT142");
                                }
                            }
                        }
                    }
                    else if (sType == "LOCATION")
                    {
                         sDesig = "";
                         sResi_d = "";
                         sMobile = "";
                         sEmail = "";
                         sEmail2 = "";
                        
                        string sQuery3 = "SELECT { fn IFNULL(MAX(id), 0) } FROM [CY_CONTACT_DTLS] WHERE [contact_type] = 'LOCATION'";
                        DataSet ds3 = My.ExecuteSELECTQuery(sQuery3);
                        if (ds3.Tables[0].Rows.Count != 0)
                        {
                            int iID = System.Convert.ToInt32(ds3.Tables[0].Rows[0][0]) + 1;
                            string sQueryStringContact = "INSERT INTO [CY_CONTACT_DTLS] ([id],[name], [desig], [dept_name], [extn], [resi_d], [off_d], [mobile], [email],[email_2],[contact_type], [entry_by]) VALUES ";
                            sQueryStringContact = sQueryStringContact + "('" + iID + "','" + sName + "','" + sDesig + "','" + ContactDept + "','" + sExtn + "','" + sResi_d + "','" + sOff_d + "','" + sMobile + "','" + sEmail + "','" + sEmail2 + "','" + sType + "','" + sSessionCPF + "')";

                            if (My.ExecuteSQLQuery(sQueryStringContact) == true)
                            {
                                Response.Write("TRUE~SUCCESS~Contact Saved Successfully");
                            }
                            else //else case of execute update query
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ADDUPDCONTACT142");
                            }
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ADDUPDCONTACT142");
                        }
                    }else if (sType == "REGEMP")
                    {
                        string sCPF_NO = Request["cpfno"];

                        string sQueryStringContact = "UPDATE [O_EMP_MSTR] SET [O_EMP_HM_PHN] = '" + sResi_d + "',[O_EMP_OFF_PHN] = '" + sOff_d + "',[O_EMP_MBL_NMBR] = '" + sMobile + "',[O_EMP_DEPT_NM] = '" + ContactDept + "',[CY_EMP_EX_NO] = '" + sExtn + "' WHERE [O_CPF_NMBR] = '" + sCPF_NO + "'";

                        if (My.ExecuteSQLQuery(sQueryStringContact) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Contact Saved Successfully");
                        }
                        else //else case of execute update query
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ADDUPDCONTACT142");
                        }
                    }
                                    }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //  try catch ends
            }
        }
    }
}