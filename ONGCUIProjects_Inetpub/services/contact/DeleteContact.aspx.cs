﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.contact
{
    public partial class DeleteContact : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '../../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sContactID = Request["ContactID"];
                try
                {
                    string sDelete = "DELETE FROM [CY_CONTACT_DTLS] WHERE [id] = '" + My.sDataStringTrim(1, sContactID) + "' AND [contact_type]= '" + My.sDataStringTrim(2, sContactID) + "'";
                    if (My.ExecuteSQLQuery(sDelete) == true)
                    {
                        Response.Write("TRUE~SUCCESS~Contact Deleted Successfully...!");
                    }
                    else //else case of execute update query
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *CONTACTDEL1298");
                    }
                }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //  try catch ends
            }
        }
    }
}