﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace ONGCUIProjects.services
{
    public partial class ModuleListHtml : System.Web.UI.Page
    {
        SqlConnection conn ;
        string sCPF;
        private string getChildMenu(int id)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds2 = new DataSet();
            string sStr_2 = "SELECT id, priority, tool_tip_content, caption, child_code, mandatory, default_checked FROM CY_CHILD_MENU WHERE main_menu_id = '" + id + "' ORDER BY priority";
            da = new SqlDataAdapter(sStr_2, conn);
            da.Fill(ds2);
            string returnString2 = "";
            for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
            {
                int ChildMenuId = System.Convert.ToInt32(ds2.Tables[0].Rows[i][0].ToString());
                string tooltipcon = ds2.Tables[0].Rows[i][2].ToString();
                string caption = ds2.Tables[0].Rows[i][3].ToString();
                string mandatory = ds2.Tables[0].Rows[i][5].ToString();
                string default_chk = ds2.Tables[0].Rows[i][6].ToString();
                if (default_chk != "TRUE")
                {
                    string PriAssigned="";
                    if (PrivilegeToEmp(sCPF, ChildMenuId) == true)
                    {
                        PriAssigned = "checked";
                    }
                    returnString2 = returnString2 + "<div class=\"module_list module_list_css\"><input id='" + ChildMenuId + "' type=\"CheckBox\" " + PriAssigned + "/><label for='" + ChildMenuId + "' >" + caption + "</label></div>";
                }
            }
            return returnString2;
        }
        
        private Boolean PrivilegeToEmp(string sCPF, int childmenuid)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds3 = new DataSet();
            string sStr_3 = "SELECT child_id FROM CY_MENU_EMP_RELATION WHERE cpf_number ='" + sCPF + "' AND child_id ='" + childmenuid + "'";
            da = new SqlDataAdapter(sStr_3, conn);
            da.Fill(ds3);
            if (ds3.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["pri_cpf"] = "56";
            sCPF = Request.QueryString["cpf_no"];
            //sCPF = Session["pri_cpf"].ToString() ;
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds1 = new DataSet();
            string sStr_1 = " SELECT id,priority,tool_tip_content,caption FROM CY_MAIN_MENU  ORDER BY priority";
            da = new SqlDataAdapter(sStr_1, conn);
            da.Fill(ds1);
            string returnString1 = "";
            for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
            {
                int MainMenuId = System.Convert.ToInt32(ds1.Tables[0].Rows[i][0].ToString());
                string MainMenuPriority = ds1.Tables[0].Rows[i][1].ToString();
                string MainMenuTTC = ds1.Tables[0].Rows[i][2].ToString();
                string MainMenuCaption = ds1.Tables[0].Rows[i][3].ToString();
                string ChildMenu = getChildMenu(MainMenuId);
                returnString1 = returnString1 + "<div class=\"div_class\"><p class=\"main_menu_css\">" + MainMenuCaption + "</p><div class=\"child_css\">" + ChildMenu + "</div></div>";
            }
            Response.Write(returnString1);
        }
    }
}