﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services.quiz
{
    public partial class add_edit_fancybox : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">if (top.location!= self.location) { top.location = 'Index.aspx?error=session&message=Session expired, please login again.'}</script>");
            }
            else
            {
                string opr_type=Request.QueryString["type"];
                string que_type=Request.QueryString["que_type"];
                lbl_type.Text = opr_type;
                lbl_que_type.Text = que_type;
                string sQueID = Request.QueryString["que_id"];

                if (opr_type == "EDIT" && sQueID != "")
                {
                    
                    cdm_post_que.Attributes.Add("que_id",sQueID);
                    string sQuery = "SELECT [id],[que_txt],[start_date],[end_date] FROM [CY_QUE_DTLS] WHERE [id] = " + sQueID + "";
                    DataSet dsQueDtl = My.ExecuteSELECTQuery(sQuery);
                    string sQueText = dsQueDtl.Tables[0].Rows[0][1].ToString().Replace("<br/>","\n");
                    DateTime sQueStartDate = Convert .ToDateTime( dsQueDtl.Tables[0].Rows[0][2]);
                    DateTime sQueEndDate = Convert.ToDateTime(dsQueDtl.Tables[0].Rows[0][3]);
                    txt_que.InnerText = sQueText;
                    start_date.Value = sQueStartDate.ToString("dd-MM-yyyy HH:mm");
                    end_date.Value = sQueEndDate.ToString("dd-MM-yyyy HH:mm");



                    string sStr = "SELECT id, option_text, is_true, order_by FROM CY_QUE_ANS_RELATION WHERE que_id = " + sQueID + "ORDER BY order_by ASC";
                    DataSet dsOptDtl = My.ExecuteSELECTQuery(sStr);
                    int opt_no = 1;
                    string opt_div_child = "";
                    for (int i = 0; i < dsOptDtl.Tables[0].Rows.Count; i++)
                    {
                        string OptionID = dsOptDtl.Tables[0].Rows[i][0].ToString();
                        string OptionText = dsOptDtl.Tables[0].Rows[i][1].ToString();
                        string OptionIsTrue = dsOptDtl.Tables[0].Rows[i][2].ToString();
                        
                        opt_div_child = opt_div_child + "<div class=\"for_opt_select\" style=\"position: relative; float: left; width: 50%; margin-top: 3px; margin-bottom: 3px;\">";
                        opt_div_child = opt_div_child + "<h1 class=\"content\" style=\"width: 100%; margin: 0 0 5px 5px;\">Option : " + opt_no + "</h1><textarea  style=\"font-weight: bold; position: relative; float: left; margin-bottom: 0px; margin-top: 0px; margin-left: 5px; width: 83%;\" option_id=" + OptionID + ">" + OptionText + "</textarea>";
                        if (que_type == "QUIZ")
                        {
                            if (OptionIsTrue == "TRUE")
                            {
                                opt_div_child = opt_div_child + "<input type=\"radio\" name=\"opt_true\" style=\"position:relative;float:left; border: 3px solid green; width: 19px; height: 19px; margin: 18px 0 0 10px;\" checked>";
                            }
                            else
                            {

                                opt_div_child = opt_div_child + "<input type=\"radio\" name=\"opt_true\" style=\"position:relative;float:left; border: 3px solid green; width: 19px; height: 19px; margin: 18px 0 0 10px;\">";
                            }
                        }
                        opt_div_child = opt_div_child + "</div>";
                        opt_no++;
                    }
                    opt_div.InnerHtml = opt_div_child;
            
                    
                }
            }
        }
    }
}