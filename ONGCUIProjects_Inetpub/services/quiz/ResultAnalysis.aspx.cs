﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class ResultAnalysis : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string CPFNo = Session["Login_Name"].ToString();
                string QueID = Request.QueryString["que_id"];
                

                #region FancyBox Content
                string sStr = "SELECT [id],[que_txt],[is_multiple],[dept],[start_date],[end_date],[que_type],[is_approved],[approved_by],[approved_ts],[entry_by] FROM [CY_QUE_DTLS] WHERE [CY_QUE_DTLS].[id] = '" + QueID + "'";
                DataSet ds = My.ExecuteSELECTQuery(sStr);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    string QueText = ds.Tables[0].Rows[0][1].ToString();
                    DateTime StartDate = System.Convert.ToDateTime(ds.Tables[0].Rows[0][4].ToString());
                    DateTime EndDate = System.Convert.ToDateTime(ds.Tables[0].Rows[0][5].ToString());
                    decimal iTotalReponse = My.getReponseCountFromQueID(QueID);
                    
                    string OptStr = "SELECT [id], [option_text], [is_true], [order_by] FROM CY_QUE_ANS_RELATION WHERE [que_id] = " + QueID + "ORDER BY [order_by] ASC";
                    DataSet dsOption = My.ExecuteSELECTQuery(OptStr);
                    if (dsOption.Tables[0].Rows.Count != 0)
                    {
                        string sOptionData = "";
                        string sGraphData = "";
                        decimal dOptionData = 0;
                        for (int i = 0; i <= dsOption.Tables[0].Rows.Count - 1; i++)
                        {

                            string OptionText = dsOption.Tables[0].Rows[i][1].ToString();
                            string OptID = dsOption.Tables[0].Rows[i][3].ToString();
                            string FullOption = OptionText;
                            string IsTrue = dsOption.Tables[0].Rows[i][2].ToString();

                            #region For Graph
                            decimal OptionData = My.getOptionReponseCountFromQueIDAndOptID(QueID, OptID);
                            if (OptionData != 0)
                            {
                                dOptionData = Math.Round((OptionData * 100) / iTotalReponse, 1);
                            }
                            else if (OptionData == 0)
                            {
                                dOptionData = 0;
                            }
                            sGraphData = sGraphData + My.getOptionLetter(OptID) + "`"+dOptionData  + "~";
                            #endregion

                            if (OptionText.Trim().Length > 90)
                            {
                                OptionText = OptionText.Substring(0, 90) + "...";
                            }

                            sOptionData = sOptionData + "<tr>";
                            sOptionData = sOptionData + "<td style=\"width:15px; border-right: 2px solid rgb(65, 65, 65); color: green; font-size: 25px;\">" + My.getOptionLetter(OptID) + "</td>";
                            sOptionData = sOptionData + "<td style=\"\" title=\"" + FullOption + "\">" + OptionText + "</td>";
                            sOptionData = sOptionData + "<td style=\"width:35px; text-align:center;\">" + OptionData + "</td>";
                            sOptionData = sOptionData + "</tr>";

                        }

                        TotalResponse.InnerHtml = iTotalReponse.ToString();
                        QuestionText.InnerHtml = QueText;
                        DivOptionData.InnerHtml = "<table class=\"ratable\">"+sOptionData+"</table>";
                        lblGraphData.Text = sGraphData.Substring(0, sGraphData.Length - 1);

                        
                        
                        
                    
                    }
                }
                #endregion
            }
        }
    }
}