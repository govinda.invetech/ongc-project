﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class que_list : System.Web.UI.Page
    {
        private string getOptionFromQueId(string QueId)
        {

            string output = "";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT id, option_text, is_true, order_by FROM CY_QUE_ANS_RELATION WHERE que_id = " + QueId + "ORDER BY order_by ASC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);

            int opt_count;
            if (ds.Tables[0].Rows.Count == 0)
            {
                output = "NA";
            }
            else
            {
                opt_count = ds.Tables[0].Rows.Count;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string opt_id = ds.Tables[0].Rows[i][0].ToString();
                    string opt_text = ds.Tables[0].Rows[i][1].ToString();
                    string opt_is_true = ds.Tables[0].Rows[i][2].ToString();
                    string opt_order = ds.Tables[0].Rows[i][3].ToString();
                    output = output + opt_id + "`" + opt_text + "`" + opt_is_true + "`" + opt_order + "~";
                }
                output = output.Substring(0, output.Length - 1);
            }
            return output;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string sSessionCPF = Session["Login_Name"].ToString();
                string type = Request.QueryString["type"];
                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                SqlConnection conn = new SqlConnection(strConn);
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                string sStr = "";
                if (type == "ALL")
                {
                    sStr = "SELECT id, que_txt, is_multiple, dept, start_date, end_date, is_approved, approved_by, approved_ts, entry_by, timestamp, que_type FROM CY_QUE_DTLS ORDER BY id DESC";
                }
                else
                {
                    sStr = "SELECT id, que_txt, is_multiple, dept, start_date, end_date, is_approved, approved_by, approved_ts, entry_by, timestamp, que_type FROM CY_QUE_DTLS WHERE que_type = '" + type + "' ORDER BY id DESC";
                }
                da = new SqlDataAdapter(sStr, conn);
                da.Fill(ds);
                string output = "";
                int que_count;
                if (ds.Tables[0].Rows.Count == 0)
                {
                    output = "FALSE~SUCCESS~Data Not Found~NA~";
                    output = output + "<h2 class='content' style=\"width: 99%; text-align:center; font-size: 22px; color: #F53838; margin-top: 10px;\">No question posted yet.</h2>";
                }
                else
                {
                    char que_no = 'A';
                    // string que_no ="a";
                    que_count = ds.Tables[0].Rows.Count;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        //output = output + ds.Tables[0].Rows[0][i].ToString() + "`";
                        //que_no = GetNextBase26("a");
                        string que_id = ds.Tables[0].Rows[i][0].ToString();
                        string que_text = ds.Tables[0].Rows[i][1].ToString();
                        string is_multiple = ds.Tables[0].Rows[i][2].ToString();
                        string dept = ds.Tables[0].Rows[i][3].ToString();
                        string start_date_temp = ds.Tables[0].Rows[i][4].ToString();

                        DateTime start_date = System.Convert.ToDateTime(start_date_temp);

                        string end_date = ds.Tables[0].Rows[i][5].ToString();
                        string is_approved = ds.Tables[0].Rows[i][6].ToString();
                        string approved_by = ds.Tables[0].Rows[i][7].ToString();
                        string approved_ts = ds.Tables[0].Rows[i][8].ToString();
                        string entry_by = ds.Tables[0].Rows[i][9].ToString();
                        string timestamp = ds.Tables[0].Rows[i][10].ToString();
                        string My_text_print = ds.Tables[0].Rows[i][11].ToString();

                        DateTime dEndDate = System.Convert.ToDateTime(end_date);

                        string status = "";
                        string border = "";
                        string datestripcolor = "";
                        string questionStatus = "";
                        string StatusBoxColor = "";
                        string StatusBoxBorderColor = "";

                        if (System.DateTime.Now > dEndDate)
                        {
                            status = "CLOSE";
                            border = "#808080";
                            datestripcolor = "#808080";
                            questionStatus = "CLOSED";
                            StatusBoxColor = "rgb(240, 82, 82)";
                            StatusBoxBorderColor = "rgb(175, 0, 0)";

                        }
                        else
                        {
                            status = "OPEN";
                            border = "#10880B";
                            datestripcolor = "#1D8D22";
                            questionStatus = "OPEN";
                            StatusBoxColor = "#47C70B";
                            StatusBoxBorderColor = "#46D30F";
                        }

                        output = output + "<div style=\"position: relative;float: left;width: 98%;margin: 5px 0px;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);padding: 8px; box-shadow: 0px 2px 5px #424141; border: 2px dashed" + border + "\">";

                        output = output + "<div class=\"strip-questiondates\" style=\"background-color:" + datestripcolor + "\">";

                        output = output + "<a class=\"cmd_delete\" que_id=\"" + que_id + "\" style=\"position: relative; float: right; margin-top: 2px; margin-right: 3px;\" href='javascript:void(0);'><img src=\"Images/crosswhite.png\" title=\"Delete Question\" style=\"width: 15px;\" /></a>";

                        if (status == "OPEN")
                        {
                            output = output + "<a class=\"click_to_edit\" que_id=\"" + que_id + "\" style=\"position: relative; float: right; margin-top: 2px; margin-right: 7px;\" href=\"services/quiz/add_edit_fancybox.aspx?type=EDIT&que_id=" + que_id + "&que_type=" + My_text_print + "\"><img src=\"Images/pencil.ico\" title=\"Edit Question\" style=\"width: 15px; background-color: white; padding: 2px 2px 3px 5px; border-radius: 10px; margin-top: -3px;\" /></a>";
                        }
                        output = output + "<div style=\"position: relative; float: left; margin: 0px 5px 5px; width: auto; min-width: 230px;\">";
                        output = output + "<p class=\"questiondates\">Opening Date & Time :</p>";
                        output = output + "<p class=\"questiondates\" style=\"font-weight:bold;text-align:justify;\">" + start_date.ToString("d MMM yyyy HH:mm") + " hrs";
                        output = output + "</div>";

                        output = output + "<div style=\"position: relative; float: left; margin: 0px 5px 5px; width: auto; min-width: 230px;\">";
                        output = output + "<p class=\"questiondates\">Closing Date & Time :</p>";
                        output = output + "<p class=\"questiondates\" style=\"font-weight:bold;text-align:justify;\">" + dEndDate.ToString("d MMM yyyy HH:mm") + " hrs";
                        output = output + "</div>";

                        if (sSessionCPF=="admin")
                        {
                            output = output + "<a class=\"ResultAnalyse cmdResultAnalysis\" href=\"services/quiz/ListOfRespondedEmployee.aspx?QuestionID=" + que_id + "\">View Response</a>";
                        }
                        output = output + "<a class=\"ResultAnalyse cmdResultAnalysis\" href=\"services/quiz/ResultAnalysis.aspx?que_id=" + que_id + "\">Result Analysis</a>";

                        //output = output + "<div class=\"questionstatusbox\" style=\"border-color:" + StatusBoxBorderColor + ";background-color:" + StatusBoxColor + "\">" + questionStatus + "</div>";
                        output = output + "<div style=\"border-color:rgb(175, 0, 0);background-color:rgb(240, 82, 82); margin-right: 0; right: 60px; border-radius: 10px; text-align: center; top: 4px;\" class=\"questionstatusbox\">" + My_text_print + "</div>";
                        output = output + "</div>";


                        output = output + "<div style=\"position: relative; float: left; width: 100%;\">";
                        output = output + "<div style=\"position: relative; float: left; margin: 0px 5px 5px; width: 90%;\">";
                        output = output + "<img src=\"Images/question.png\" title=\"Delete Question\" style=\"float: left; width: 32px; margin-right: 5px; margin-top: 13px;\" />";

                        output = output + "<p class=\"questiondates\" style=\"text-align:justify; color: #0087E0; width: 90%; margin-top: 20px; font-size: 14px; font-weight: bold; font-family: Lucida Sans Unicode, Lucida Grande, sans-serif;\">" + que_text + "</p>";
                        output = output + "</div>";

                        output = output + "<div style=\"position: relative; float: left; margin: 0px 5px 0px; width: 99.2%;\">";
                        output = output + "<p style=\"width: 100%;color: #34343B;font-weight: bold;font-family: Lucida Sans Unicode, Lucida Grande, sans-serif;border-bottom: 1px solid green;margin-bottom: 5px;line-height: 30px;\">Options</p>";
                        output = output + "<div style=\"position:relative;float:left; width:93%;\">";


                        string option_str = getOptionFromQueId(que_id);
                        if (option_str != "NA")
                        {
                            string[] option_str_arr = option_str.Split('~');
                            for (int j = 0; j < option_str_arr.Length; j++)
                            {
                                string[] option_arr = option_str_arr[j].Split('`');
                                string opt_id = option_arr[0].ToString();
                                string opt_text = option_arr[1].ToString();
                                string opt_is_true = option_arr[2].ToString();
                                string opt_order = option_arr[3].ToString();
                                if (opt_is_true.Trim() == "TRUE")
                                {
                                    output = output + "<p style=\"font-weight: bold;position: relative;float: left;margin: 0px 20px 0px 5px;background-color: #05BE05;color: white;padding: 1px 5px 2px 5px;font-family: Lucida Sans Unicode, Lucida Grande, sans-serif;font-size: 14px;\">[" + opt_order.Trim() + ". " + opt_text.Trim() + "]</p>";
                                }
                                else
                                {
                                    output = output + "<p style=\"font-weight:bold;position: relative; float: left; margin: 0px 20px 0px 5px; font-family: Lucida Sans Unicode, Lucida Grande, sans-serif;font-size: 14px;\">[" + opt_order.Trim() + ". " + opt_text.Trim() + "]</p>";
                                }
                            }
                        }
                        output = output + "</div>";
                        output = output + "</div>";

                        output = output + "</div>";

                        //output = output + "<p style=\"position: relative; float: right; margin-bottom: 0px; margin-top: 0px; margin-left: 5px; margin-right:5px;\">Total <strong>72%</strong> correct answers.</p>";
                        output = output + "</div>";
                        que_no++;

                    }
                    output = "TRUE~SUCCESS~Total " + que_count + " questions loaded successfully.~" + que_count + "~" + output;

                }
                Response.Write(output);
            }
        }
    }
}