﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResultAnalysis.aspx.cs" Inherits="ONGCUIProjects.services.quiz.ResultAnalysis" %>
 <script src="../js/highcharts.js"></script>
<script type="text/javascript">


    $(document).ready(function () {
        $('#lblGraphData').hide();

        var data = $('#lblGraphData').text();
        //alert(data);
        //var data = "A`50~B`20~C`16~D`14";
        ColumnChart(data);
        $('input[name=ChartOpt]').change(function () {
            var ChartType = $('input[name=ChartOpt]:checked').val();
            if (ChartType == 'COLUMN') {
                ColumnChart(data);
            } else if (ChartType == 'PIE') {
                PieChart(data);
            }
        });
        function PieChart(data) {

            var chart;

            var sCat = data;
            var myCat = sCat.split('~');

            var colors = Highcharts.getOptions().colors;

            data = [];
            for (var i = 0; i < myCat.length; i++) {
                var myData = myCat[i].split('`');
                data.push([myData[0], parseFloat(myData[1])])
            }



            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: false
                },
                tooltip: {
                    pointFormat: '{point.percentage}%',
                    percentageDecimals: 1,
                    style: {
                        fontWeight: 'bold'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            },
                            formatter: function () {
                                return this.point.name;
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    data: data
                }],
                exporting: {
                    enabled: true
                },
                credits: {
                    enabled: false
                }
            });

        }


        function ColumnChart(data) {

            var chart;
            var sCat = data;
            var myCat = sCat.split('~');

            var colors = Highcharts.getOptions().colors;

            categories = [];
            data = [];
            for (var i = 0; i < myCat.length; i++) {
                var myData = myCat[i].split('`');
                categories.push(myData[0])
                data.push({ y: parseFloat(myData[1]), color: colors[i] })
            }


            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container',
                    type: 'column'
                },
                title: {
                    text: false
                },

                xAxis: {
                    categories: categories

                },
                legend: {
                    enabled: false
                },
                yAxis: {
                    title: false
                },
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true,
                            color: colors[0],
                            style: {
                                fontWeight: 'bold'
                            },
                            formatter: function () {
                                return this.y + '%';
                            }
                        }
                    }
                },
                tooltip: {

                    formatter: function () {
                        var s = '<b>' + this.x + ':' + this.y + ' %</b>';
                        return s;
                    }
                },

                series: [{
                    name: name,
                    data: data
                }],
                exporting: {
                    enabled: true
                },
                credits: {
                    enabled: false
                }
            });

        }
    });
        </script>
      <div style="position:relative;float:left; width:900px;">
          <asp:label runat="server" ID="lblGraphData" text="Label"></asp:label>
            <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">Analyse Pulse of Uran</h1>
            <div class="div-fullwidth">
                <p runat="server" id="QuestionText" class="content" style="width: 100%;">The server at www.google.com can't be found, because the DNS lookup failed. DNS is the network service that translates a website's name to its Internet address. This error is most often caused by having no connection to the Internet or a misconfigured network. It can also be caused by an unresponsive DNS server or a firewall preventing Google Chrome from accessing the network.</p>
            </div>
            <div class="div-halfwidth" style="width: 390px;">
                <div style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 0px;width: 99.6%;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom:20px;height: 31px;text-align: center;">
                    <p class="content" style="width: 100%;text-align: center;">Total Response : <span runat="server" id="TotalResponse">100</span></p>
                </div>
                <div runat="server" id="DivOptionData" class="div-fullwidth">
                    <table class="ratable">
                        <tr>
                            <td>A</td>
                            <td>The server at www.google.com can't be found.DNS is the network service that translates.</td>
                            <td>70</td>
                        </tr>
                        <tr>
                            <td>A</td>
                            <td>The server at www.google.com can't be found.DNS is the network service that translates.</td>
                            <td>70</td>
                        </tr>
                        <tr>
                            <td>A</td>
                            <td>The server at www.google.com can't be found.DNS is the network service that translates.</td>
                            <td>70</td>
                        </tr>
                        <tr>
                            <td>A</td>
                            <td>The server at www.google.com can't be found.DNS is the network service that translates.</td>
                            <td>70</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="position:relative; float:right; width:500px;">
             <div style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 0px;width: 99.6%;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom:20px;">
                <input name="ChartOpt" id="optColumn" type="radio" value="COLUMN" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;" checked/><label  class="content" style="cursor: pointer;" for="optColumn">COLUMN CHART</label>
                <input name="ChartOpt" id="optPie" type="radio" value="PIE" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;"/><label  class="content" style="cursor: pointer;" for="optPie">PIE CHART</label>
            </div>
            <div id="container"  style="position: relative; float: left; width:500px; height: 300px; margin: 0px;"></div>
            </div>
            <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
           <%-- <div class="div-fullwidth">
                <input type="button" id="cmdReset" class="g-button g-button-red" value="CLOSE" onclick="window.location=self.location; $.fancybox.close();" style="float:right;" />
            </div>--%>
        </div>