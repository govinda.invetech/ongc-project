﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

namespace ONGCUIProjects.services.quiz
{
    public partial class ListOfRespondedEmployee : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string sQueID = Request["QuestionID"].ToString();


                string sStr = "SELECT [id],[que_txt],[is_multiple],[dept],[start_date],[end_date],[que_type],[is_approved],[approved_by],[approved_ts],[entry_by] FROM [CY_QUE_DTLS] WHERE [CY_QUE_DTLS].[id] = '" + sQueID + "'";
                DataSet ds = My.ExecuteSELECTQuery(sStr);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    string QueText = ds.Tables[0].Rows[0][1].ToString();
                    DateTime StartDate = System.Convert.ToDateTime(ds.Tables[0].Rows[0][4].ToString());

                    string sOutput = "";

                    sOutput = "<table border=\"1\" class=\"tftable\">";
                    sOutput += "<tr><td colspan='5' style='padding:2px;'><p  runat='server' class='content' style='font-weight:bold;'>" + QueText + "</p></td></tr>";
                    sOutput += "<tr><td colspan='5' style='padding:2px;'><p  runat='server' class='content' style='float:right; font-size:15px;'>" + StartDate.ToString("d MMM yyyy HH:mm") + " hrs</p></td></tr>";
                    sOutput += "<tr>";
                    sOutput += "<th style=\"width:20px;white-space:nowrap;\">#</th>";
                    sOutput += "<th>Employee Name</th>";
                    sOutput += "<th>CPF No</th>";
                    sOutput += "<th>Designation</th>";
                    sOutput += "<th>Option</th>";
                    sOutput += "</tr>";

                    string sQuery = "SELECT ROW_NUMBER() OVER (ORDER BY [O_EMP_MSTR].[O_EMP_NM] ASC) AS 'SLNO', [O_EMP_MSTR].[O_CPF_NMBR], UPPER([O_EMP_MSTR].[O_EMP_NM]), UPPER([O_EMP_MSTR].[O_EMP_DESGNTN]),[CY_EMP_ANS_RELATION].[option_id] ";
                    sQuery += "FROM [CY_EMP_ANS_RELATION],[O_EMP_MSTR] ";
                    sQuery += "WHERE [CY_EMP_ANS_RELATION].[cpf_no] = [O_EMP_MSTR].[O_CPF_NMBR] ";
                    sQuery += "AND [CY_EMP_ANS_RELATION].[que_id] = '" + sQueID + "' ORDER BY [O_EMP_MSTR].[O_EMP_NM] ASC";

                    DataSet dsRegUser = My.ExecuteSELECTQuery(sQuery);
                    if (dsRegUser.Tables[0].Rows.Count == 0)
                    {
                        sOutput += "<tr><td colspan='5'><p  runat='server' class='content' style='margin:5px 0'>No Employee Responded Yet</p></td></tr>";
                    }
                    else
                    {
                        for (int i = 0; i < dsRegUser.Tables[0].Rows.Count; i++)
                        {
                            string sSlNo = dsRegUser.Tables[0].Rows[i][0].ToString();
                            string sCPFNo = dsRegUser.Tables[0].Rows[i][1].ToString();
                            string sEmpName = dsRegUser.Tables[0].Rows[i][2].ToString();
                            string sDesignation = dsRegUser.Tables[0].Rows[i][3].ToString();
                            sOutput = sOutput + "<tr>";
                            sOutput = sOutput + "<td>" + sSlNo + "</td>";
                            sOutput = sOutput + "<td>" + sEmpName + "</td>";
                            sOutput = sOutput + "<td>" + sCPFNo + "</td>";
                            sOutput = sOutput + "<td>" + sDesignation + "</td>";
                            sOutput = sOutput + "<td>" + My.getOptionLetter(dsRegUser.Tables[0].Rows[i][4].ToString()) + "</td>";
                            sOutput = sOutput + "</tr>";
                        }

                    }
                    sOutput += "</table>";
                    RegisteredEmpList.InnerHtml = sOutput;
                }


            }
        }
    }
}