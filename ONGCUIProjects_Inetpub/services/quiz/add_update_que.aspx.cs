﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class add_update_que : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MyApplication1 My = new MyApplication1();
            if (Session["Login_Name"] == null)
            {
            }
            else
            {
                try
                {
                    string entry_by = Session["Login_Name"].ToString();

                    string sType = Request.QueryString["type"];
                    string sStr = Request.QueryString["str"];
                    

                    //dfdfasd~FALSE~DEPT~29/11/2012~30/11/2012~YES~App_by~0000-00-00~[1. Option Text]`FALSE`[2. Option Text]`FALSE`[3. Option Text]`TRUE`[4. Option Text]`FALSE
                    //(que_txt, is_multiple, dept, start_date, end_date, is_approved, approved_by, approved_ts, entry_by)
                    string[] str_arr = sStr.Split('~');
                    string que_text = str_arr[0];
                    string is_multiple = str_arr[1];
                    string dept = str_arr[2];
                    string start_date_temp = str_arr[3];
                    // 21/12/2012 2012-12-21

                    //DateTime s_date = DateTime.Parse(start_date_temp);
                    string s_date = My.ConvertDateStringintoSQLDateString(start_date_temp);

                    string end_date_temp = str_arr[4];
                    //DateTime e_date = DateTime.Parse(end_date_temp);
                    string e_date = My.ConvertDateStringintoSQLDateString(end_date_temp);

                    string is_approved = str_arr[5];
                    string approved_by = str_arr[6];
                    //string approved_date_temp = str_arr[7];
                    string approved_date = s_date;
                    string que_type = str_arr[8];

                    
                    
                    

                    if (sType == "Post")
                    {

                        #region Get Question Id Start
                        string sStr1 = "SELECT { fn IFNULL(MAX(id), 0) } AS LAST_QUE_ID FROM CY_QUE_DTLS";
                        DataSet ds1 = My.ExecuteSELECTQuery(sStr1);
                        int que_id = System.Convert.ToInt32(ds1.Tables[0].Rows[0][0]) + 1;
                        #endregion
                       

                        #region Insert Question Details

                        string QueryStringQue = "INSERT INTO CY_QUE_DTLS (id, que_txt, is_multiple, dept, start_date, end_date, que_type, is_approved, approved_by, approved_ts, entry_by) VALUES ('" + que_id + "', '" + que_text.Replace("\n", "<br/>") + "', '" + is_multiple + "', '" + dept + "', '" + s_date + "', '" + e_date + "', '" + que_type + "', '" + is_approved + "', '" + approved_by + "', '" + approved_date + "', '" + entry_by + "')";
                        if (My.ExecuteSQLQuery(QueryStringQue) == true)
                        {
                            string[] options_arr = str_arr[9].Split('`');
                            int order_by = 1;
                            string QueryStringAns = "INSERT INTO CY_QUE_ANS_RELATION (que_id, option_text, is_true, order_by) VALUES ";
                            for (int i = 0; i < options_arr.Length; i = i + 3)
                            {
                                //Option Text 1`FALSE`Option Text 2`FALSE`Option Text 3`FALSE`Option Text 4`FALSE
                                string option_text = options_arr[i];
                                string is_true = options_arr[i + 1];

                                QueryStringAns = QueryStringAns + "('" + que_id + "','" + option_text + "','" + is_true + "','" + order_by + "'),";
                                order_by++;
                            }
                            QueryStringAns = QueryStringAns.Substring(0, QueryStringAns.Length - 1);
                            if (My.ExecuteSQLQuery(QueryStringAns) == true)
                            {
                                Response.Write("TRUE~SUCCESS~Question Posted Successfully");
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *POSTQUE#126");
                            }
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *POSTQUE#126");
                        }
                        #endregion
                    }
                    else if (sType == "UPDATE")
                    {
                        #region Update Question Details
                        string sQue_id = Request.QueryString["que_id"];
                        string QueryStringQue = "UPDATE CY_QUE_DTLS SET que_txt='" + que_text.Replace("\n","<br/>") + "', start_date='" + s_date + "', end_date='" + e_date + "' WHERE [id]= '" + sQue_id + "'";
                        if (My.ExecuteSQLQuery(QueryStringQue) == true)
                        {

                            string[] options_arr = str_arr[9].Split('`');
                            for (int i = 0; i < options_arr.Length; i = i + 3)
                            {
                                string option_text = options_arr[i];
                                string is_true = options_arr[i + 1];
                                string option_id = options_arr[i + 2];
                                string QueryStringAns = " UPDATE [CY_QUE_ANS_RELATION] SET [option_text] = '" + option_text + "', [is_true]= '" + is_true + "' WHERE [id] = '" + option_id + "'";
                                if (My.ExecuteSQLQuery(QueryStringAns) == true)
                                {
                                    Response.Write("TRUE~SUCCESS~Question Posted Successfully");
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *POSTQUE#126");
                                }
                            }
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *POSTQUE#126");
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
            }
        }
    }
}