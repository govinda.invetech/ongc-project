﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class UserAnsFancyBox : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string CPFNo = Session["Login_Name"].ToString();
                string QueID = Request.QueryString["que_id"];
                lblQueId.Text = QueID;

                #region FancyBox Content
                string sStr = "SELECT [id],[que_txt],[is_multiple],[dept],[start_date],[end_date],[que_type],[is_approved],[approved_by],[approved_ts],[entry_by] FROM [CY_QUE_DTLS] WHERE [CY_QUE_DTLS].[id] = '" + QueID + "'";
                DataSet ds = My.ExecuteSELECTQuery(sStr);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    string QueText = ds.Tables[0].Rows[0][1].ToString();
                    DateTime StartDate = System.Convert.ToDateTime(ds.Tables[0].Rows[0][4].ToString());
                    DateTime EndDate = System.Convert.ToDateTime(ds.Tables[0].Rows[0][5].ToString());
                    decimal iTotalReponse = My.getReponseCountFromQueID(QueID);
                    string sGraphCat = "";
                    string sGraphData = "";

                    string status = "";
                    string add_css = "";

                    if (System.DateTime.Now > EndDate)
                    {
                        status = "CLOSE";
                        add_css = " correct ";
                    }
                    else
                    {
                        status = "OPEN";
                        add_css = "";
                    }

                    string OptStr = "SELECT [id], [option_text], [is_true], [order_by] FROM CY_QUE_ANS_RELATION WHERE [que_id] = " + QueID + "ORDER BY [order_by] ASC";
                    DataSet dsOption = My.ExecuteSELECTQuery(OptStr);
                    if (dsOption.Tables[0].Rows.Count != 0)
                    {
                        
                        string CPFAns = My.getAnswerFromCPFNo(QueID, CPFNo);
                        string option = "";
                        for (int i = 0; i <= dsOption.Tables[0].Rows.Count - 1; i++)
                        {

                            string OptionText = dsOption.Tables[0].Rows[i][1].ToString();
                            string OptID = dsOption.Tables[0].Rows[i][3].ToString();
                            string FullOption = OptionText;
                            string IsTrue = dsOption.Tables[0].Rows[i][2].ToString();
                            
                            #region For Graph
                            sGraphCat = sGraphCat + My.getOptionLetter(OptID) + "`";
                            decimal OptionData = My.getOptionReponseCountFromQueIDAndOptID(QueID, OptID);
                            if (OptionData != 0)
                            {
                                OptionData = Math.Round((OptionData * 100) / iTotalReponse, 1);
                            }
                            sGraphData = sGraphData + OptionData + "`";  
                            #endregion

                            if (OptionText.Trim().Length > 50)
                            {
                                OptionText = OptionText.Substring(0, 50) + "...";
                            }
                            string CssClass = "";
                            if (i % 2 == 0)
                            {
                                CssClass = "MCQ-option-container-posLeft-normal";
                            }
                            else
                            {
                                CssClass = "MCQ-option-container-posRight-normal";
                            }

                            if (status == "CLOSE")
                            {
                                if (IsTrue == "TRUE")
                                {
                                    if (OptID == CPFAns)
                                    {
                                        CssClass = CssClass + add_css + " selected";
                                    }
                                    else
                                    {
                                        CssClass = CssClass + add_css;
                                    }
                                }
                                else
                                {
                                    if (OptID == CPFAns)
                                    {
                                        CssClass = CssClass + " selected";
                                    }
                                    else
                                    {
                                        //CssClass = CssClass;
                                    }
                                }
                            }
                            else if (status == "OPEN")
                            {
                                if (OptID == CPFAns)
                                {
                                    CssClass = CssClass + " selected";
                                }
                                else
                                {
                                    //CssClass = CssClass;
                                }
                            }
                            option = option + "<div OptionID=\"" + OptID + "\" class=\"" + CssClass + " my_option_select\">";
                            option = option + "<h1 class=\"MCQ-option-number\">" + My.getOptionLetter(OptID) + "</h1>";
                            option = option + "<p class=\"MCQ-option\" title=\"" + FullOption + "\">" + FullOption + "</p>";/*Replace OptionText with FullOption*/
                            option = option + "</div>";
                        }


                        lblStartDate.Text = StartDate.ToString("f");
                        lblEndDate.Text = EndDate.ToString("f");
                        lblQueText.Text = QueText;
                        div_options_container.InnerHtml = option;
                        lblResponseCount.Text = iTotalReponse.ToString();
                        lblQueStatus.Text = status;
                        lblGraphValues.Text = sGraphCat.Substring(0, sGraphCat.Length - 1) + "~" + sGraphData.Substring(0, sGraphData.Length - 1);
                    }
                }
                #endregion
            }
        }
    }
}