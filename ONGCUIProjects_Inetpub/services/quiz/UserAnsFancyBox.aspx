﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserAnsFancyBox.aspx.cs" Inherits="ONGCUIProjects.services.quiz.UserAnsFancyBox" %>

<script type="text/javascript" language="javascript" src="../../js/highcharts.js"></script>
<script type="text/javascript">
    $("#lblQueStatus").hide();
    $("#lblQueId").hide();
    $("#lblGraphValues").hide();
    if ($(".my_option_select").is('.selected')) {
        $("#complete_option").html("You selected : " + $(".selected").children('h1').text() + '. ' + $(".selected").children('p').text());
    }

    //$("#div_options_container").html( $("#lblFBType").text());
//    if ($("#lblQueStatus").text() == "OPEN") {
    $(".my_option_select").click(function () {
        if ($("#lblQueStatus").text() == "OPEN") {
            if ($(".my_option_select").is('.selected')) {
                alert("You have already responded to this question.");
            } else {
                if (confirm("Are you sure to lock this option?")) {
                    var OptId = $(this).attr("OptionID");
                    var QueId = $("#lblQueId").text();
                    var str = QueId + "~" + OptId;
                    $.ajax({
                        type: "GET",
                        data: "str=" + str,
                        url: "services/quiz/SaveEmpAnswer.aspx",
                        success: function (msg) {
                            //alert(msg);
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                var TotalReponse = parseInt($("#lblResponseCount").text());
                                $("#lblResponseCount").text(TotalReponse + 1);
                                alert(msg_arr[2]);
                                $("#click_to_answer").text("Responded");
                                //window.location = self.location;
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                    var my_option = $(this).children('h1').text();
                    $(".my_option_select").removeClass('selected');
                    $(this).addClass('selected');
                    $("#complete_option").html("You selected : " + $(".selected").children('h1').text() + '. ' + $(".selected").children('p').text());

                }
            }
        } else if ($("#lblQueStatus").text() == "CLOSE") {
            alert("This question is closed.");
        }
    });



    if ($("#lblQueStatus").text() == "CLOSE") {
        $(function () {
            var chart;
            var sChartData = $("#lblGraphValues").text().trim();

            var sChartDataArr = sChartData.split('~');

            var GraphCat = sChartDataArr[0].split('`');
            var GraphData = sChartDataArr[1].split('`');

            var colors = Highcharts.getOptions().colors;

            var categories = [];
            for (var i = 0; i < GraphCat.length; i++) {
                categories.push(GraphCat[i]);
            }

            var data = [];
            for (var j = 0; j < GraphData.length; j++) {
                data.push({ y: parseFloat(GraphData[j]), color: colors[j] })
            }

            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container',
                    type: 'column'
                },
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true,
                            color: colors[0],
                            style: {
                                fontWeight: 'bold'
                            },
                            formatter: function () {
                                return this.y;
                            }
                        }
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + ':' + Highcharts.numberFormat(this.y, 1) + '%';
                    }
                },
                title: { text: false },
                xAxis: { categories: categories },
                legend: { enabled: false },
                yAxis: { title: false },
                series: [{ data: data}],
                exporting: { enabled: true },
                credits: { enabled: true }
            });
        });
    } else {
        $("#container").html("<h2 style=\"margin-top: 30%;text-align: center; position: relative; float: left; margin-left: 5px;\">Result will be displayed after close time</h2>");
    }
</script>
<form id="form1" runat="server">
<div class="MCQ-container">
<!-- Top Part Starts-------------------------------------------------------------------------------------------------->
	<div class="MCQ-TopPart">
    <div style=" position: relative; float: left; width: 98%; padding: 10px 10px 0 10px; border-bottom: 2px solid #58595A;">
        <h1 class="questionDates" style="color: rgb(20, 126, 224); margin-right: 10px;">Start Date: </h1>
        <asp:Label ID="lblStartDate" runat="server" class="questionDates"></asp:Label>
        <h1 class="questionDates" style="color: rgb(20, 126, 224); margin-right: 10px; margin-left: 20px;">End Date: </h1>
        <asp:Label ID="lblEndDate" runat="server" class="questionDates"></asp:Label>
        <a href="javascript:void(0)" title="Close" onclick="$.fancybox.close();" style="float: right; background-image: url('js/fancybox/fancy_close.png'); width: 30px; height: 30px; margin-top: -7px;margin-right: -7px;"></a>
    </div>
<!-- Top Left Part Starts--------------------------------------------------------------------------------------------->
		<div class="MCQ-PartLeft">
    		<div class="MCQ-question-container">
       			<h1 class="MCQ-question">
                    <p style=" position: relative; float: left;color: red; font-size: 22px; font-weight: bold; margin-right: 10px;">Question: </p><asp:Label ID="lblQueText" runat="server"></asp:Label>
             	</h1>
        	</div>
<!-- Options Container Starts----------------------------------------------------------------------------------------->
        	<div runat="server" id="div_options_container" class="MCQ-All-options-container">

                <%--<div  class="MCQ-option-container-posLeft-normal my_option_select">
            		<h1 class="MCQ-option-number">A.</h1>
                	<p class="MCQ-option">Black fadf sdfa sdf asdf asdfa sdf asdfa Area</p>
            	</div>
                <div  class="MCQ-option-container-posLeft-normal my_option_select">
            		<h1 class="MCQ-option-number">B.</h1>
                	<p class="MCQ-option">Black fadf sdfa sdf asdf asdfa sdf asdfa Area</p>
            	</div>
                <div  class="MCQ-option-container-posLeft-normal my_option_select">
            		<h1 class="MCQ-option-number">C.</h1>
                	<p class="MCQ-option">Black fadf sdfa sdf asdf asdfa sdf asdfa Area</p>
            	</div>
                <div  class="MCQ-option-container-posLeft-normal my_option_select">
            		<h1 class="MCQ-option-number">D.</h1>
                	<p class="MCQ-option">Black fadf sdfa sdf asdf asdfa sdf asdfa Area</p>
            	</div>--%>                






                <%-- 
        		<div class="MCQ-option-container-posLeft-normal">
            		<h1 class="MCQ-option-number">A.</h1>
                	<h1 class="MCQ-option">Black fadf sdfa sdf asdf asdfa sdf asdfa Area</h1>
            	</div>
            	<div class="MCQ-option-container-posRight-selected">
            		<h1 class="MCQ-option-number">B.</h1>
                	<h1 class="MCQ-option">Black Area</h1>
            	</div>
            	<div class="MCQ-option-container-posLeft-selected-correct">
            		<h1 class="MCQ-option-number">C.</h1>
                	<h1 class="MCQ-option">Black Area</h1>
            	</div>
            	<div class="MCQ-option-container-posRight-correct">
            		<h1 class="MCQ-option-number">D.</h1>
                	<h1 class="MCQ-option">Black Area</h1>
            	</div>
                --%>
        	</div>
<!-- Options Container Ends------------------------------------------------------------------------------------------->
    	</div>
<!-- Top Left Part Ends----------------------------------------------------------------------------------------------->
<!-- Top Right Part Starts-------------------------------------------------------------------------------------------->
    	<div class="MCQ-PartRight">
    		<h1 class="MCQ-responses-received">
            	Response Received
                <br />
                <!--<span class="reponse-count">145</span>-->
                <asp:Label ID="lblResponseCount" runat="server" CssClass="reponse-count"></asp:Label>
            </h1>
            <div class="graph-container">
            	<div id="container" style="width: 270px; height: 236px; margin: 0 auto">
                    
                </div>
            </div>
            
    	</div>
<!-- Top Right Part Ends---------------------------------------------------------------------------------------------->
    </div>
<!-- Top Part Ends---------------------------------------------------------------------------------------------------->
<!-- Bottom Part Starts----------------------------------------------------------------------------------------------->
    <div class="MCQ-BottomPart">
    	<h1 id="complete_option" class="MCQ-question" style="font-size: 20px; padding-left: 10px; padding-right: 10px;"></h1>
        <!--<h1 id="H1" class="MCQ-question" style="font-size: 20px; padding-left: 10px; padding-right: 10px;"><span style="color:green;">OPTION (A)</span> : Black Area</h1>-->
    </div>
<!-- Bottom Part Ends------------------------------------------------------------------------------------------------->
</div>
<asp:Label ID="lblQueStatus" runat="server"></asp:Label>
<asp:Label ID="lblQueId" runat="server"></asp:Label>
<asp:Label ID="lblGraphValues" runat="server" Text="Label"></asp:Label>
</form>

