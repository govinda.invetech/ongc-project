﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class DeleteQuestion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string que_id = Request.QueryString["que_id"].ToString();
                //string entry_by = Session["Login_Name"].ToString();
                SqlConnection conn;
                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                conn = new SqlConnection(strConn);

                #region Delete Question Details
                string DelQueryStringTable1 = "";
                DelQueryStringTable1 = "DELETE FROM CY_QUE_DTLS WHERE id = '" + que_id + "'";
                SqlCommand del_cmd_T1 = new SqlCommand(DelQueryStringTable1);
                del_cmd_T1.Connection = conn;
                conn.Open();
                del_cmd_T1.ExecuteNonQuery();
                conn.Close();
                //Response.Write("TRUE~SUCCESS~Question Deleted Successfully");
                #endregion

                #region Delete Options
                string DelQueryStringTable2 = "";
                DelQueryStringTable2 = "DELETE FROM CY_QUE_ANS_RELATION WHERE que_id = '" + que_id + "'";
                SqlCommand del_cmd_T2 = new SqlCommand(DelQueryStringTable2);
                del_cmd_T2.Connection = conn;
                conn.Open();
                del_cmd_T2.ExecuteNonQuery();
                conn.Close();
                //Response.Write("TRUE~SUCCESS~Question Deleted Successfully");
                #endregion

                #region Delete Emp Question Relation
                string DelQueryStringTable3 = "";
                DelQueryStringTable3 = "DELETE FROM CY_EMP_ANS_RELATION WHERE que_id = '" + que_id + "'";
                SqlCommand del_cmd_T3 = new SqlCommand(DelQueryStringTable3);
                del_cmd_T3.Connection = conn;
                conn.Open();
                del_cmd_T3.ExecuteNonQuery();
                conn.Close();
                Response.Write("TRUE~SUCCESS~Question Deleted Successfully");
                #endregion

            }
        }
    }
}