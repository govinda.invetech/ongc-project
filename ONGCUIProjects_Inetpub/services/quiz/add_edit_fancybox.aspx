﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add_edit_fancybox.aspx.cs" Inherits="ONGCUIProjects.services.quiz.add_edit_fancybox" %>
 <script type="text/javascript">
     $("#lbl_type").hide();
     $("#lbl_que_type").hide();
    
     if ($("#lbl_type").text() == "EDIT") {
         $('#option_div').children('p').hide();
         $('#option_div').children('input').hide();
         $('input[name=my_hint]').attr('disabled', true);
         $('#cdm_post_que').attr('value', 'UPDATE');
     }
     var my_hint = $("#lbl_que_type").text();
     $('input[name="my_hint"][value="' + my_hint + '"]').prop('checked', true);

     $(function () {
//         $('#start_date').datetimepicker({ dateFormat: "dd-mm-yy" });
//         $('#end_date').datetimepicker({ dateFormat: "dd-mm-yy" });
         //{ dateFormat: "dd/mm/yy" }
         var startDateTextBox = $('#start_date');
         var endDateTextBox = $('#end_date');

         startDateTextBox.datetimepicker({
             onClose: function (dateText, inst) {
                 if (endDateTextBox.val() != '') {
                     var testStartDate = startDateTextBox.datetimepicker('getDate');
                     var testEndDate = endDateTextBox.datetimepicker('getDate');
                     if (testStartDate > testEndDate)
                         endDateTextBox.datetimepicker('setDate', testStartDate);
                 }
                 else {
                     endDateTextBox.val(dateText);
                 }
             },
             onSelect: function (selectedDateTime) {
                 endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate'));
             },
             dateFormat: "dd-mm-yy",
             minDate: 0
         });
         endDateTextBox.datetimepicker({
             onClose: function (dateText, inst) {
                 if (startDateTextBox.val() != '') {
                     var testStartDate = startDateTextBox.datetimepicker('getDate');
                     var testEndDate = endDateTextBox.datetimepicker('getDate');
                     if (testStartDate > testEndDate)
                         startDateTextBox.datetimepicker('setDate', testEndDate);
                 }
                 else {
                     startDateTextBox.val(dateText);
                 }
             },
             onSelect: function (selectedDateTime) {
                 startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
             },
             dateFormat: "dd-mm-yy",
             minDate: 0
         });
     });



 </script>
 <style type="text/css"> 
            #ui-datepicker-div, .ui-datepicker{ font-size: 85%; width: 304px; }
        </style>
 <form id="form1" runat="server">
 
 <div id="add_edit_que" style="position:relative;float:left; width:600px; padding: 10px;">
            <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">Add New Question<img src="Images/question.png" style="margin: 5px 0 -5px 5px;" /></h1>
            <textarea runat="server" id="txt_que" style="position: relative; float: left; min-height: 50px; max-height: 100px;text-align: justify; width: 99%; max-width: 99%; overflow: auto;"></textarea>
            <div style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 10px 2px 10px;width: 580px;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc;">
                <h1 class="content">Question Type :</h1>
                <input type="radio" name="my_hint" id="s" value="SURVEY" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;" /><label for="s" class="content" style="cursor: pointer;">Survey Question</label>
                <input type="radio" name="my_hint" id="q" value="QUIZ" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;"/><label for="q" class="content" style="cursor: pointer;">Quiz Question</label>
            </div>
            <div runat="server" id="option_div" style="position: relative; float: left; margin: 15px 5px 10px 5px; width: 99.2%;">
                <p class="content">No of Options : </p>
                <input id="txt_no_opt" type="text" style="width:70px; margin-right: 10px;">
                <input id="cmd_fill_opt" type="button" value="Fill Options" class="g-button g-button-share">
                <div runat="server" id="opt_div" style="position: relative; float: left; width: 100%; top: 0px; left: 0px;">
                
            </div>
        </div>
        <div style="position: relative; float: left; margin: 0px 5px 5px; width: 48%;">
            <p class="content">Start Date</p>
            <input runat="server" id="start_date" type="text" style="width:200px;">
        </div>
       <div style="position: relative; float: right; margin: 0px 0px 5px 0; width: 48%;">
            <p class="content">End Date</p>
            <input runat="server" id="end_date" type="text" style="width:200px;">
        </div>
        <p id="require_notification" style="color:red;position:relative;float:left;"></p>
        <input id="cmd_que_post_cancel" type="button" class="g-button g-button-red" value="Cancel" style="position:relative;float:right;margin-right:10px;" onclick="$.fancybox.close();" />
        <input runat="server" id="cdm_post_que" type="button" class="g-button g-button-share" value="Post" style="position:relative;float:right; margin-right:10px; text-transform: uppercase;" />
        <asp:Label ID="lbl_type" runat="server"></asp:Label>
       
        <asp:Label ID="lbl_que_type" runat="server"></asp:Label>
       
    </div>
</form>
