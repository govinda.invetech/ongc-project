﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class QueListLaunchPage : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string login_name = Session["Login_Name"].ToString();
                string sTodayQuestion = Request["TodayQuestion"];
                int page = Convert.ToInt32(Request["page"]);

                int per_page = 5;

                int iFrom = (page - 1) * per_page;
                int iTo = iFrom + per_page;

                int cur_page = page;
                bool previous_btn = true;
                bool next_btn = true;
                bool first_btn = true;
                bool last_btn = true;

                string sQueryData = "SELECT [id],[que_txt],[start_date] FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY [start_date] DESC) AS ROW FROM [CY_QUE_DTLS] WHERE [id] != '" + sTodayQuestion + "'  AND [start_date] < GETDATE()) A WHERE ROW > '" + iFrom + "' AND ROW <= '" + iTo + "'";
                DataSet dsData = My.ExecuteSELECTQuery(sQueryData);
                string sOutput = "";
                if (dsData.Tables[0].Rows.Count != 0)
                {
                    sOutput = "<h1 class='heading-previous-questions' style='font-size: 25px; margin-bottom: 5px;'>View Previous Results</h1>";
                    sOutput += "<div class='revious-question-container-content'>";
                    for (int i = 0; i < dsData.Tables[0].Rows.Count; i++)
                    {

                        string sQueID = dsData.Tables[0].Rows[i][0].ToString();
                        string sQueText = dsData.Tables[0].Rows[i][1].ToString();
                        DateTime sQueStartDate = Convert.ToDateTime(dsData.Tables[0].Rows[i][2]);
                        sOutput += "<div class='question-container-div'>";
                        sOutput += "<span class='previous-question-Postdate'>" + sQueStartDate.ToString("dd-MM-yyyy HH:mm") + " hrs</span>";
                        sOutput += "<h1 class='previous-question'>" + sQueText + "</h1>";
                        sOutput += "<a href='javascript:void(0);' style='margin-top: 0; line-height: 25px;' class='buttonGreen' id='f_click_to_answer' my_value='services/quiz/UserAnsFancyBox.aspx?que_id=" + sQueID + "'>View Result</a>";
                        sOutput += "</div>";
                    }
                    sOutput += "</div>"; // Content for Data
                }



                string sQueryPagination = "SELECT [id],[que_txt],[start_date] FROM [CY_QUE_DTLS]  WHERE [id] != '" + sTodayQuestion + "'  AND [start_date] < GETDATE() ORDER BY [start_date] DESC";
                DataSet dsPagination = My.ExecuteSELECTQuery(sQueryPagination);
                double count = dsPagination.Tables[0].Rows.Count;
                double no_of_paginations = Math.Ceiling(count / per_page);

                double start_loop, end_loop;
                if (cur_page >= 7)
                {
                    start_loop = cur_page - 3;
                    if (no_of_paginations > cur_page + 3)
                    {
                        end_loop = cur_page + 3;
                    }
                    else if (cur_page <= no_of_paginations && cur_page > no_of_paginations - 6)
                    {
                        start_loop = no_of_paginations - 6;
                        end_loop = no_of_paginations;
                    }
                    else
                    {
                        end_loop = no_of_paginations;
                    }
                }
                else
                {
                    start_loop = 1;
                    if (no_of_paginations > 7)
                    {
                        end_loop = 7;
                    }
                    else
                    {
                        end_loop = no_of_paginations;
                    }
                }

                sOutput += "<div class='pagination'><ul>";
                // FOR ENABLING THE FIRST BUTTON
                if (first_btn && cur_page > 1)
                {
                    sOutput += "<li p='1' class='active'>First</li>";
                }
                else if (first_btn)
                {
                    sOutput += "<li p='1' class='inactive'>First</li>";
                }

                if (previous_btn && cur_page > 1)
                {
                    int pre = cur_page - 1;
                    sOutput += "<li p='" + pre + "' class='active'>Previous</li>";
                }
                else if (previous_btn)
                {
                    sOutput += "<li class='inactive'>Previous</li>";
                }
                for (double i = start_loop; i <= end_loop; i++)
                {
                    if (cur_page == i)
                        sOutput += "<li p='" + i + "' style='color:#fff;background-color:#006699;' class='active'>" + i + "</li>";
                    else
                        sOutput += "<li p='" + i + "' class='active'>" + i + "</li>";
                }
                if (next_btn && cur_page < no_of_paginations)
                {
                    int nex = cur_page + 1;
                    sOutput += "<li p='" + nex + "' class='active'>Next</li>";
                }
                else if (next_btn)
                {
                    sOutput += "<li class='inactive'>Next</li>";
                }


                if (last_btn && cur_page < no_of_paginations)
                {
                    sOutput += "<li p='" + no_of_paginations + "' class='active'>Last</li>";
                }
                else if (last_btn)
                {
                    sOutput += "<li p='" + no_of_paginations + "' class='inactive'>Last</li>";
                }
                //string goto_function = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
                //string total_string = "<span class='total' a='" + no_of_paginations + "'>Page <b>" + cur_page + "</b> of <b>" + no_of_paginations + "</b></span>";
                //sOutput = sOutput + "</ul>" + goto_function + total_string + "</div>";
                sOutput = sOutput + "</ul></div>";

                Response.Write(sOutput);
            }
        }
    }
}