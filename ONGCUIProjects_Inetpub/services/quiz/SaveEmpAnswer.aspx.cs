﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class SaveEmpAnswer : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                
                if (Request.QueryString["str"] != null)
                {
                    string CPFNo = Session["Login_Name"].ToString();
                    string str = Request.QueryString["str"];
                    string[] str_arr = str.Split('~');
                    string QueID = str_arr[0];
                    string OptID = str_arr[1];
                    
                    //#region Delete Previous Answer if Exist
                    //string QueryStringQue = "";
                    //QueryStringQue = "DELETE FROM CY_EMP_ANS_RELATION WHERE cpf_no = '" + CPFNo + "' AND que_id = '" + QueID + "'";
                    //SqlCommand DeleteCMD = new SqlCommand(QueryStringQue);
                    //DeleteCMD.Connection = conn;
                    //conn.Open();
                    //DeleteCMD.ExecuteNonQuery();
                    //conn.Close();
                    //#endregion

                    #region Insert New Answer
                    string sQueryStringQue = "INSERT INTO CY_EMP_ANS_RELATION (que_id, option_id, cpf_no, entry_by) VALUES ('" + QueID + "','" + OptID + "','" + CPFNo + "','" + CPFNo + "')";
                    try
                    {
                        if (My.ExecuteSQLQuery(sQueryStringQue) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Your response has been received");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *PUPOSTANS3452");
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write("FALSE~ERR~" + ex.Message);
                    }
                    #endregion
                    
                }
                
            }
        }
    }
}