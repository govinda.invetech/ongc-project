﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.quiz
{
    public partial class SavePreamble : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {

                
                    string CPFNo = Session["Login_Name"].ToString();
                    string sPreambleText = Request.Form["PreambleText"];
                    string sPreambleImage = "Images/ONGClogo_big.jpg";

                    #region Delete Previous Answer if Exist
                    try
                    {
                        string sDeleteQuery = "DELETE FROM [CY_ABT_PULSE_OF_URAN]";
                        if (My.ExecuteSQLQuery(sDeleteQuery) == true)
                        {
                            string sQueryStringQue = "INSERT INTO [CY_ABT_PULSE_OF_URAN] ([IMG_PATH], [PREAMBLE_TEXT], [ENTRY_BY]) VALUES ('" + sPreambleImage + "','" + sPreambleText + "','" + CPFNo + "')";

                            if (My.ExecuteSQLQuery(sQueryStringQue) == true)
                            {
                                Response.Write("TRUE~SUCCESS~Preamble Saved Successfully");
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *SAVEPREAMBLE");
                            }

                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *SAVEPREAMBLE");
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write("FALSE~ERR~" + ex.Message);
                    }
                    #endregion

                }

            
        }
    }
}