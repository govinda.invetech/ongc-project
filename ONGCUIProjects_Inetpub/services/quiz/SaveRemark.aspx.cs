﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.quiz
{
    public partial class SaveRemark : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                try
                {
                    string sRemarkData = Request["RemarkData"].ToString();

                    sRemarkData = sRemarkData.Substring(0, sRemarkData.Length - 1);
                    string[] sRemarkDataArr = sRemarkData.Split('~');
                    for (int i = 0; i < sRemarkDataArr.Length; i++)
                    {
                        string[] sQueRemarkArr = sRemarkDataArr[i].Split('`');
                        string sQueID = sQueRemarkArr[0].ToString();
                        string sQueRemark = sQueRemarkArr[1].ToString();

                        string sUpdateQuery = "UPDATE [CY_QUE_DTLS] SET [remark] = '" + sQueRemark + "' WHERE [id] = '" + sQueID + "'";


                        if (My.ExecuteSQLQuery(sUpdateQuery) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Saved Successfully");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *QUEREMSAVE857");
                        }

                    }
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
            }
        }
    }
}