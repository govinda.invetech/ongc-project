﻿using System;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class Editvisitorpass : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["INDX_NMBR"]) || string.IsNullOrEmpty(Request["CY_GROUP"]))
            {
            }
            else
            {
                string sIndexNumber = Request["INDX_NMBR"];
                string sGroupID = Request["CY_GROUP"];
                string sEmpQuery = " ";
                sEmpQuery += "  ";
                sEmpQuery += "   SELECT                                                  ";
                sEmpQuery += "   [O_VISTR_NM],                                           ";
                sEmpQuery += "   [O_VISTR_ORGNZTN],                                      ";
                sEmpQuery += "   [O_VISTR_AGE],                                          ";
                sEmpQuery += "   [O_VISTR_GNDR],                                         ";
                sEmpQuery += "   [O_PRPS_OF_VISIT],                                      ";
                sEmpQuery += "   [O_PASS_TYPE_FLG],                                      ";
                sEmpQuery += "   [O_REQ_COMPLTN_FLG],                                    ";
                sEmpQuery += "   [CY_WITH_TOOL],                                         ";
                sEmpQuery += "   [O_INDX_NMBR],[O_ACP1_NM]                               ";
                sEmpQuery += " FROM [O_VSTR_GATE_PASS_RQSTN_DTL]                         ";
                sEmpQuery += " WHERE [CY_GROUP] = '" + sGroupID + "' and [O_INDX_NMBR]='" + sIndexNumber + "'   ";

                DataSet dsEmpDtl = My.ExecuteSELECTQuery(sEmpQuery);
                if (dsEmpDtl.Tables[0].Rows.Count != 0)
                {
                    txtIndexNumber.Text = sIndexNumber;
                    txtIndexNumber.Enabled = false;
                    txttool.Text = dsEmpDtl.Tables[0].Rows[0]["CY_WITH_TOOL"].ToString();
                   

                }
            }



        }
    }
}