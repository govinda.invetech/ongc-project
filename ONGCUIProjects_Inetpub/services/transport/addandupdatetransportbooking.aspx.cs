﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services.transport
{
    public partial class addandupdatetransportbooking : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string cpf_no = Session["Login_Name"].ToString();

                string module_type = Request.Form["module_type"].ToString();
                string my_index_no = Request.Form["my_index_no"].ToString();

                //Applicant Detail
                string app_name = Request.Form["app_name"].ToString();
                string app_cpf_no = Request.Form["app_cpf_no"].ToString();
                string app_design = Request.Form["app_design"].ToString();
                string app_loc = Request.Form["app_loc"].ToString();
                string app_ph_ex_no = Request.Form["app_ph_ex_no"].ToString();

                //Booking Detail
                string type_booking = Request.Form["type_booking"].ToString();
                string booking_start_datetime = Request.Form["booking_start_datetime"].ToString();
                string booking_end_datetime = Request.Form["booking_end_datetime"].ToString();
                string no_of_person_travel = Request.Form["no_of_person_travel"].ToString();
                string remarks = Request.Form["remarks"].ToString();
                string purpose = Request.Form["purpose"].ToString();

                //Approver Detail
                string approver_name = Request.Form["approver_name"].ToString();
                string approver_cpf_no = Request.Form["approver_cpf_no"].ToString();
                string approver_desig = Request.Form["approver_desig"].ToString();

                //Incharge Detail
                string incharge_name = Request.Form["incharge_name"].ToString();
                string incharge_cpf_no = Request.Form["incharge_cpf_no"].ToString();
                string incharge_desig = Request.Form["incharge_desig"].ToString();

                //Alloter Detail
                string allotemet_name = Request.Form["allotemet_name"].ToString();
                string allotemet_cpf_no = Request.Form["allotemet_cpf_no"].ToString();
                string allotemet_desig = Request.Form["allotemet_desig"].ToString();

                //Reporting Detail
                string reporting_time = Request.Form["reporting_time"].ToString();
                string report_to_name = Request.Form["report_to_name"].ToString();
                string report_to_address = Request.Form["report_to_address"].ToString();
                string report_contact1 = Request.Form["report_contact1"].ToString();
                string report_contact2 = Request.Form["report_contact2"].ToString();

                
                if (checkcpfno(app_cpf_no) == "FALSE")
                {
                    Response.Write("FALSE~ERR~Invalid Applicant CPF No.");
                    return;

                }
                else if (checkcpfno(approver_cpf_no) == "FALSE")
                {
                    Response.Write("FALSE~ERR~Invalid Approver CPF No.");
                    return;

                }
                else if (checkcpfno(incharge_cpf_no) == "FALSE")
                {
                    Response.Write("FALSE~ERR~Invalid incharge CPF No.");
                    return;

                }
                else if (checkcpfno(allotemet_cpf_no) == "FALSE")
                {
                    Response.Write("FALSE~ERR~Invalid ALLOCATER officer CPF No.");
                    return;

                }


                try
                {
                    string sUpdate = "";

                    if (module_type == "NEW")
                    {
                        sUpdate = "INSERT INTO [CY_TRANSPORT_BOOKING_DETAIL] ([APP_CPF_NO] ,[APP_NAME],[APP_DESIG],[APP_LOCATION],[APP_EMP_EXTENSION_NO],[BOOKING_TYPE],[BOOKING_START_DATETIME],[BOOKING_END_DATETIME],[REPORTING_TIME],[REPORT_TO_NAME],[REPORT_TO_ADDERSS],[REPORT_TO_CONTACT1],[REPORT_TO_CONTACT2],[NO_OF_PERSON_TRAVELLING] ,[REMARKS],[PURPOSE],[INCHARGE_STATUS],[APPROVER_STATUS] ,[ALLOCATTER_STATUS] ,[INCHARGE_CPF_NO] ,[INCHARGE_NAME],[INCHARGE_DESIG],[APPROVER_CPF_NO] ,[APPROVER_NAME] ,[APPROVER_DESIG],[ALLOCATER_CPF_NO],[ALLOCATER_NAME],[ALLOCATER_DESIG],[ENTRY_BY],[TIMESTAMP])";
                        sUpdate = sUpdate + "VALUES('" + app_cpf_no + "','" + app_name + "','" + app_design + "','" + app_loc + "','" + app_ph_ex_no + "','" + type_booking + "','" + My.ConvertDateStringintoSQLDateString( booking_start_datetime) + "','" + My.ConvertDateStringintoSQLDateString(booking_end_datetime) + "','" + My.ConvertDateStringintoSQLDateString(reporting_time) + "','" + report_to_name + "','" + report_to_address + "','" + report_contact1 + "','" + report_contact2 + "','" + no_of_person_travel + "','" + remarks + "','" + purpose + "','PENDING','PENDING','PENDING','" + incharge_cpf_no + "','" + incharge_name + "','" + incharge_desig + "','" + approver_cpf_no + "','" + approver_name + "','" + approver_desig + "','" + allotemet_cpf_no + "','" + allotemet_name + "','" + allotemet_desig + "','" + cpf_no + "',GETDATE())";
                    }
                    else if (module_type == "UPDATE")
                    {
                        sUpdate = "UPDATE [CY_TRANSPORT_BOOKING_DETAIL] SET [APP_CPF_NO] = '" + app_cpf_no + "',[APP_NAME] ='" + app_name + "',[APP_DESIG] = '" + app_design + "',[APP_LOCATION] ='" + app_loc + "' ,[APP_EMP_EXTENSION_NO] ='" + app_ph_ex_no + "',[BOOKING_TYPE] = '" + type_booking + "',[BOOKING_START_DATETIME] = '" + My.ConvertDateStringintoSQLDateString(booking_start_datetime) + "' ,[BOOKING_END_DATETIME] ='" + My.ConvertDateStringintoSQLDateString(booking_end_datetime) + "',[REPORTING_TIME] = '" + My.ConvertDateStringintoSQLDateString(reporting_time) + "' ,[REPORT_TO_NAME] = '" + report_to_name + "' ,[REPORT_TO_ADDERSS] = '" + report_to_address + "',[REPORT_TO_CONTACT1] = '" + report_contact1 + "',[REPORT_TO_CONTACT2] = '" + report_contact2 + "' ,[NO_OF_PERSON_TRAVELLING] ='" + no_of_person_travel + "',[REMARKS] = '" + remarks + "',[PURPOSE] ='" + purpose + "',[INCHARGE_CPF_NO] ='" + incharge_cpf_no + "',[INCHARGE_NAME] ='" + incharge_name + "',[INCHARGE_DESIG] ='" + incharge_desig + "',[APPROVER_CPF_NO] ='" + approver_cpf_no + "',[APPROVER_NAME] = '" + approver_name + "' ,[APPROVER_DESIG] ='" + approver_desig + "',[ALLOCATER_CPF_NO] ='" + allotemet_cpf_no + "',[ALLOCATER_NAME] = '" + allotemet_name + "',[ALLOCATER_DESIG] = '" + allotemet_desig + "',[ENTRY_BY] ='" + cpf_no + "' ,[TIMESTAMP] =GETDATE()  where [id]='" + my_index_no + "'";
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~Invalid Input");
                        return;
                    }

                    if (My.ExecuteSQLQuery(sUpdate) == true)
                    {
                        if (module_type == "NEW")
                        {
                            Response.Write("TRUE~SUCCESS~Your Transport Booking Request Sent For Approval!");
                        }
                        else
                        {
                            Response.Write("TRUE~SUCCESS~Your Transport Booking Request Updated and Sent For Approval !");
                        }
                    }
                    else //else case of execute update query
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *TRANSPORT403");
                    }
                }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //  try catch ends
            }

        }
        private string checkcpfno(string cpf_no)
        {
            string sStr = "SELECT [O_CPF_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            DataSet ds = My.ExecuteSELECTQuery(sStr);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                return "TRUE";
            }
        }
    }
}