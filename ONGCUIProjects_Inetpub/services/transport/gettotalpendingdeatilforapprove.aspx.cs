﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services.transport
{
    public partial class gettotalpendingdeatilforapprove : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cpf_no = "";
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();

            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT DISTINCT( [id]),[APP_CPF_NO],[APP_NAME],[APP_DESIG],[APP_LOCATION],[APP_EMP_EXTENSION_NO],[BOOKING_TYPE],[BOOKING_START_DATETIME],[BOOKING_END_DATETIME],[REPORTING_TIME] ,[REPORT_TO_NAME],[REPORT_TO_ADDERSS],[REPORT_TO_CONTACT1],[REPORT_TO_CONTACT2],[NO_OF_PERSON_TRAVELLING],[REMARKS],[PURPOSE],[REMARKS_APPROVER],[REMARKS_ALLOCATE],[ROOM_ALLOCATE],[INCHARGE_STATUS],[APPROVER_STATUS],[ALLOCATTER_STATUS],[INCHARGE_TIMESTAMP],[APPROVER_TIMESTAMP],[ALLOCATTER_TIMESTAMP],[INCHARGE_CPF_NO],[INCHARGE_NAME],[INCHARGE_DESIG],[APPROVER_CPF_NO],[APPROVER_NAME],[APPROVER_DESIG],[ALLOCATER_CPF_NO],[ALLOCATER_NAME],[ALLOCATER_DESIG] FROM [CY_TRANSPORT_BOOKING_DETAIL] where([INCHARGE_CPF_NO]='" + cpf_no + "' OR [APPROVER_CPF_NO] ='" + cpf_no + "' OR ALLOCATER_CPF_NO ='" + cpf_no + "') and ([INCHARGE_STATUS]='PENDING' OR [APPROVER_STATUS]='PENDING' OR [ALLOCATTER_STATUS]='PENDING')";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            string output = "";
            int count = 0;
            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("TRUE~SUCCESS~No Request Pending~");
            }
            else
            {



                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    count = count + 1;
                    //applicant detail
                    string index_no = ds.Tables[0].Rows[i][0].ToString();
                    string app_cpfno = ds.Tables[0].Rows[i][1].ToString();
                    string app_name = ds.Tables[0].Rows[i][2].ToString();
                    string app_desig = ds.Tables[0].Rows[i][3].ToString();
                    string ex_no = ds.Tables[0].Rows[i][5].ToString();
                    string booking_type = ds.Tables[0].Rows[i][6].ToString();
                    string booking_start_time = System.Convert.ToDateTime(ds.Tables[0].Rows[i][7].ToString()).ToLongDateString().ToString() + " " + System.Convert.ToDateTime(ds.Tables[0].Rows[i][7].ToString()).ToLongTimeString().ToString();
                    string booking_end_time = System.Convert.ToDateTime(ds.Tables[0].Rows[i][8].ToString()).ToLongDateString().ToString() + " " + System.Convert.ToDateTime(ds.Tables[0].Rows[i][8].ToString()).ToLongTimeString().ToString();
                    // Reporting Detail
                    string reporting_time = ds.Tables[0].Rows[i][9].ToString();
                    string report_to_name = ds.Tables[0].Rows[i][10].ToString();
                    string report_to_address = ds.Tables[0].Rows[i][11].ToString();
                    string report_contact1 = ds.Tables[0].Rows[i][12].ToString();
                    string report_contact2 = ds.Tables[0].Rows[i][13].ToString();
                    string no_of_person_travel = ds.Tables[0].Rows[i][14].ToString();
                    //
                    string remarks = ds.Tables[0].Rows[i][15].ToString();
                    string purpose = ds.Tables[0].Rows[i][16].ToString();
                    string remarks_allocate = ds.Tables[0].Rows[i][11].ToString();
                    string room_Allocate = ds.Tables[0].Rows[i][12].ToString();
                    //status
                    string INCHARGE_STATUS = ds.Tables[0].Rows[i][20].ToString();
                    string APPROVER_STATUS = ds.Tables[0].Rows[i][21].ToString();
                    string ALLOCATTER_STATUS = ds.Tables[0].Rows[i][22].ToString();
                    /******** start approve timestamp **********/
                    string INCHARGE_TIMESTAMP = ds.Tables[0].Rows[i][23].ToString();
                    string APPROVER_TIMESTAMP = ds.Tables[0].Rows[i][24].ToString();
                    string ALLOCATTER_TIMESTAMP = ds.Tables[0].Rows[i][25].ToString();
                    if (INCHARGE_TIMESTAMP != "")
                    {
                        INCHARGE_TIMESTAMP = System.Convert.ToDateTime(ds.Tables[0].Rows[i][23].ToString()).ToLongDateString().ToString() + " " + System.Convert.ToDateTime(ds.Tables[0].Rows[i][23].ToString()).ToLongTimeString().ToString();
                    } if (APPROVER_TIMESTAMP != "")
                    {
                        APPROVER_TIMESTAMP = System.Convert.ToDateTime(ds.Tables[0].Rows[i][24].ToString()).ToLongDateString().ToString() + " " + System.Convert.ToDateTime(ds.Tables[0].Rows[i][24].ToString()).ToLongTimeString().ToString();
                    } if (ALLOCATTER_TIMESTAMP != "")
                    {
                        ALLOCATTER_TIMESTAMP = System.Convert.ToDateTime(ds.Tables[0].Rows[i][25].ToString()).ToLongDateString().ToString() + " " + System.Convert.ToDateTime(ds.Tables[0].Rows[i][25].ToString()).ToLongTimeString().ToString();
                    }
                    /*********end approve timestamp**********/
                    //incharge detail
                    string incharge_cpf_no = ds.Tables[0].Rows[i][26].ToString();
                    string incharge_name = ds.Tables[0].Rows[i][27].ToString();
                    string incharge_desig = ds.Tables[0].Rows[i][28].ToString();
                    //image status
                    string incharge_image = "";
                    string approver_image = "";
                    string allocater_image = "";
                    string for_edit = "";
                    if (INCHARGE_STATUS == "APPROVE") { incharge_image = "images/tick.png"; } else { incharge_image = "images/cross.png"; }
                    if (APPROVER_STATUS == "APPROVE") { approver_image = "images/tick.png"; } else { approver_image = "images/cross.png"; }
                    if (ALLOCATTER_STATUS == "APPROVE") { allocater_image = "images/tick.png"; } else { allocater_image = "images/cross.png"; }
                    for_edit = "<a class='edit_in_fancy_box' href=\"Transport_Booking_francybox.aspx?my_hint=APPROVE`" + index_no + "\">View & Approve</a>";
                    string myoutput = "<div style=\"padding: 10px; width: 97.5%;\" <div class=\"div-fullwidth marginbottom gatepass-approvalbox\" > <h1 class=\"content-gatepass-approval count\">" + (i + 1).ToString() + ".</h1><div style=\"width: auto;\" ";
                    myoutput = myoutput + "class=\"div-fullwidth\"><div class=\"divider\"></div></div><table border=\"1\" style=\"margin-left: 10px; margin-top: 10px; margin-right: 10px; width: 98%;\" class=\"tftable\" id=\"tfhover\"> <tbody>";
                    myoutput = myoutput + "<tr><th style=\"width: 200px;\">Applicant</th><th style=\"width: 80px;\">Type</th> <th style=\"width: 80px;\">Start dateTime</th> <th style=\"width: 80px;\">End DateTime</th>  <th style=\"width: 140px;\">Incharge</th><th style=\"width: 30px;\">INCHARGE</th><th style=\"width: 30px;\">APPROVER</th> <th style=\"width: 30px;\">ALLOTER</th><th style=\"width: 40px;\"></th></tr>";
                    myoutput = myoutput + "<tr><td>" + app_name + "(" + app_cpfno + "-" + app_desig + ")</td><td>" + booking_type + "</td><td>" + booking_start_time + "</td><td>" + booking_end_time + "</td><td>" + incharge_name + "(" + incharge_cpf_no + "-" + incharge_desig + ")</td><td><a class='black' title=\" "+INCHARGE_STATUS+" : " + INCHARGE_TIMESTAMP + "\" href=\"javascript:void(0);\"><img src=\"" + incharge_image + "\" /></a></td><td><a class='black' title=\" "+APPROVER_STATUS+" : " + APPROVER_TIMESTAMP + "\" href=\"javascript:void(0);\"><img src=\"" + approver_image + "\" /></a></td><td><a class='black' title=\" "+ALLOCATTER_STATUS+" : " + ALLOCATTER_TIMESTAMP + "\" href=\"javascript:void(0);\"><img src=\"" + allocater_image + "\" /></a></td><td>" + for_edit + " </td></tr>  </tbody></table></div>";
                    output = output + myoutput;
                }
                Response.Write("TRUE~SUCCESS~Total " + count.ToString() + " Request Pending To Approve~" + output);
            }
        }
    }
}