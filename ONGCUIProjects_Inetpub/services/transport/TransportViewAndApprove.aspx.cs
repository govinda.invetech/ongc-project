﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class TransportViewAndApprove : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sActionType = "";
                if (string.IsNullOrEmpty(Request.QueryString["type"]))
                {

                }
                else
                {
                    sActionType = Request.QueryString["type"].ToString();
                }
                string cpf_no = Session["Login_Name"].ToString();
                txtSessionCPF.Value = cpf_no;

                string sBookingID = My.sDataStringTrim(2, sActionType);
                txtBookingID.Value = sBookingID;

                string sViewEditQuery = "SELECT BOOKING_TYPE, BOOKING_START_DATETIME, BOOKING_END_DATETIME, REMARKS, PURPOSE, NO_OF_PERSON_TRAVELLING, ";
                sViewEditQuery += "INCHARGE_NAME, INCHARGE_CPF_NO, INCHARGE_DESIG, INCHARGE_STATUS, ";
                sViewEditQuery += "APPROVER_NAME, APPROVER_CPF_NO, APPROVER_DESIG, APPROVER_STATUS, ";
                sViewEditQuery += "ALLOCATER_NAME, ALLOCATER_CPF_NO, ALLOCATER_DESIG, ALLOCATTER_STATUS, ";
                sViewEditQuery += "REPORTING_TIME, REPORT_TO_NAME, REPORT_TO_ADDERSS, REPORT_TO_CONTACT1, REPORT_TO_CONTACT2, ";
                sViewEditQuery += "VEHICLE_NO, DRIVER_NAME, DRIVER_CONTACT ";
                sViewEditQuery += "FROM CY_TRANSPORT_BOOKING_DETAIL WHERE id = '" + sBookingID + "'";

                DataSet dsViewUpdate = My.ExecuteSELECTQuery(sViewEditQuery);


                #region Applicant Detail
                string str = "SELECT [APP_NAME],[APP_CPF_NO],[APP_DESIG],[APP_LOCATION],[APP_EMP_EXTENSION_NO] FROM CY_TRANSPORT_BOOKING_DETAIL WHERE id = '" + sBookingID + "'";
                DataSet ds = My.ExecuteSELECTQuery(str);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Response.Write("FALSE~ERR~Applicant Detail Not Found");
                }
                else
                {
                    txtappname.Value = ds.Tables[0].Rows[0][0].ToString();
                    txtappcpf_no.Value = ds.Tables[0].Rows[0][1].ToString();
                    txtappdesign.Value = ds.Tables[0].Rows[0][2].ToString();
                    txtapplocation.Value = ds.Tables[0].Rows[0][3].ToString();
                    txtappphoneex_no.Value = ds.Tables[0].Rows[0][4].ToString();
                }
                #endregion

                #region Baad Mein
                ddltypeofbooking.Value = dsViewUpdate.Tables[0].Rows[0][0].ToString();

                DateTime dtBookingDateTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][1]);
                txtbookingstartdatetime.Value = dtBookingDateTime.ToString("dd-MM-yyyy HH:mm");

                DateTime dtEndDateTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][2]);
                txtbookingEnddatetime.Value = dtEndDateTime.ToString("dd-MM-yyyy HH:mm");

                txtremarks.Value = dsViewUpdate.Tables[0].Rows[0][3].ToString();
                txtpurpose.Value = dsViewUpdate.Tables[0].Rows[0][4].ToString();
                no_of_person_travel.Value = dsViewUpdate.Tables[0].Rows[0][5].ToString();

                DateTime dtReportingTimeTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][18]);
                reporting_time.Value = dtReportingTimeTime.ToString("dd-MM-yyyy HH:mm");
                reporting_name.Value = dsViewUpdate.Tables[0].Rows[0][19].ToString();
                reporting_address.Value = dsViewUpdate.Tables[0].Rows[0][20].ToString();
                reporting_contact1.Value = dsViewUpdate.Tables[0].Rows[0][21].ToString();
                reporting_contact2.Value = dsViewUpdate.Tables[0].Rows[0][22].ToString();
                #endregion

                #region Approving Authorities

                string sInchargeName = dsViewUpdate.Tables[0].Rows[0][6].ToString();
                string sInchargeCPF = dsViewUpdate.Tables[0].Rows[0][7].ToString();
                string sInchargeDesig = dsViewUpdate.Tables[0].Rows[0][8].ToString();
                string sInchargeStatus = dsViewUpdate.Tables[0].Rows[0][9].ToString();


                string sApproverName = dsViewUpdate.Tables[0].Rows[0][10].ToString();
                string sApproverCPF = dsViewUpdate.Tables[0].Rows[0][11].ToString();
                string sApproverDesig = dsViewUpdate.Tables[0].Rows[0][12].ToString();
                string sApproverStatus = dsViewUpdate.Tables[0].Rows[0][13].ToString();


                string sAlloterName = dsViewUpdate.Tables[0].Rows[0][14].ToString();
                string sAlloterCPF = dsViewUpdate.Tables[0].Rows[0][15].ToString();
                string sAlloterDesig = dsViewUpdate.Tables[0].Rows[0][16].ToString();
                string sAlloterStatus = dsViewUpdate.Tables[0].Rows[0][17].ToString();

                string sApprovingAuth = "";
                sApprovingAuth = "<table class=\"tftable\">";

                #region Incharge Row
                txtInchargeStatus.Value = sInchargeCPF + "~" + sInchargeStatus;
                sApprovingAuth += "<tr>";
                sApprovingAuth += "<td>Incharge Name & CPF No : " + sInchargeName + " (" + sInchargeCPF + ")</td>";
                sApprovingAuth += "<td>Designation : " + sInchargeDesig + "</td>";
                sApprovingAuth += "<td style='text-align:center'>";
                switch (sInchargeStatus)
                {
                    case "PENDING":
                        sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-red approve_reject\" value=\"REJECT\" hint=\"REJECT\" type_desig=\"INCHARGE\" />";
                        sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-submit approve_reject\" value=\"APPROVE\" hint=\"APPROVE\" type_desig=\"INCHARGE\" />";
                        break;
                    case "APPROVE":
                        sApprovingAuth += "<img src=\"Images/tick.png\">";
                        break;
                    case "REJECT":
                        sApprovingAuth += "<img src=\"Images/cross.png\">";
                        break;
                }
                sApprovingAuth += "</td>";
                sApprovingAuth += "</tr>";
                #endregion

                #region Approver Row
                txtApproverStatus.Value = sApproverCPF + "~" + sApproverStatus;
                sApprovingAuth += "<tr>";
                sApprovingAuth += "<td>Approver Name & CPF No : " + sApproverName + " (" + sApproverCPF + ")</td>";
                sApprovingAuth += "<td>Designation : " + sApproverDesig + "</td>";
                sApprovingAuth += "<td style='text-align:center'>";
                if (sInchargeStatus == "REJECT")
                {
                    sApprovingAuth += "Incharge has been rejected";
                }
                else
                {
                    switch (sApproverStatus)
                    {
                        case "PENDING":
                            sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-red approve_reject\" value=\"REJECT\" hint=\"REJECT\" type_desig=\"APPROVER\" />";
                            sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-submit approve_reject\" value=\"APPROVE\" hint=\"APPROVE\" type_desig=\"APPROVER\" />";
                            break;
                        case "APPROVE":
                            sApprovingAuth += "<img src=\"Images/tick.png\">";
                            break;
                        case "DIRECT":
                            sApprovingAuth += "<img src=\"Images/tick.png\">";
                            break;
                        case "REJECT":
                            sApprovingAuth += "<img src=\"Images/cross.png\">";
                            break;
                    }
                }
                sApprovingAuth += "</td>";
                sApprovingAuth += "</tr>";
                #endregion

                #region Alloter Row
                txtAlloterStatus.Value = sAlloterCPF + "~" + sAlloterStatus;
                sApprovingAuth += "<tr>";
                sApprovingAuth += "<td>Alloter Name & CPF No : " + sAlloterName + " (" + sAlloterCPF + ")</td>";
                sApprovingAuth += "<td>Designation : " + sAlloterDesig + "</td>";
                sApprovingAuth += "<td id='AllotterAction' style='text-align:center'>";
                if (sInchargeStatus == "REJECT")
                {
                    sApprovingAuth += "Incharge has been rejected";
                }
                else
                {
                    if (sApproverStatus == "REJECT")
                    {
                        sApprovingAuth += "Approver has been rejected";
                    }
                    else
                    {
                        switch (sAlloterStatus)
                        {
                            case "PENDING":
                                sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-red approve_reject\" value=\"NOT AVAILABLE\" hint=\"REJECT\" type_desig=\"ALLOTER\" />";
                                sApprovingAuth += "<br /><p class='content'>or Enter Transport details to allot.</p>";
                                sApprovingAuth += "<input type=\"button\" class=\"g-button g-button-submit approve_reject\" value=\"ALLOT\" hint=\"APPROVE\" style='display:none;' type_desig=\"ALLOTER\" />";
                                break;
                            case "APPROVE":
                                sApprovingAuth += "<img src=\"Images/tick.png\">";
                                break;
                            case "REJECT":
                                sApprovingAuth += "<img src=\"Images/cross.png\">";
                                break;
                        }
                    }
                }
                sApprovingAuth += "</td>";
                sApprovingAuth += "</tr>";
                #endregion

                sApprovingAuth += "</table>";

                

                div_approving_authorities.InnerHtml = sApprovingAuth;
                #endregion

                #region Vehicle Detail
                txtVehicleNo.Value = dsViewUpdate.Tables[0].Rows[0][23].ToString();
                txtDriverName .Value = dsViewUpdate.Tables[0].Rows[0][24].ToString();
                txtDriverContact.Value = dsViewUpdate.Tables[0].Rows[0][25].ToString();
                #endregion

                txtAlloterCPFforSearch.Value = sAlloterCPF;
            }
        }
    }
}