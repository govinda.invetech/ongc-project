﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.transport
{
    public partial class PrintTransport : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sBookingID = Request.QueryString["id"].ToString();

                string sQuery = "SELECT [APP_CPF_NO],[APP_NAME],[APP_DESIG],[APP_LOCATION],[BOOKING_TYPE],[NO_OF_PERSON_TRAVELLING],[PURPOSE],[REMARKS], ";
                sQuery += "[REPORTING_TIME],[REPORT_TO_NAME],[REPORT_TO_ADDERSS],[REPORT_TO_CONTACT1],[REPORT_TO_CONTACT2], ";
                sQuery += "[INCHARGE_CPF_NO],[INCHARGE_NAME],[INCHARGE_DESIG],[INCHARGE_STATUS],[INCHARGE_TIMESTAMP], ";
                sQuery += "[APPROVER_CPF_NO],[APPROVER_NAME],[APPROVER_DESIG],[APPROVER_STATUS],[APPROVER_TIMESTAMP], ";
                sQuery += "[ALLOCATTER_STATUS],[VEHICLE_NO],[DRIVER_NAME],[DRIVER_CONTACT] FROM [CY_TRANSPORT_BOOKING_DETAIL] WHERE [id] = '" + sBookingID + "'";
                DataSet dsTransportBooking = My.ExecuteSELECTQuery(sQuery);


                string sAppCPF = dsTransportBooking.Tables[0].Rows[0]["APP_CPF_NO"].ToString();
                string sAppName = dsTransportBooking.Tables[0].Rows[0]["APP_NAME"].ToString();
                string sAppDesig = dsTransportBooking.Tables[0].Rows[0]["APP_DESIG"].ToString();
                string sAppLocation = dsTransportBooking.Tables[0].Rows[0]["APP_LOCATION"].ToString();
                string sBookingType = dsTransportBooking.Tables[0].Rows[0]["BOOKING_TYPE"].ToString();
                string sNoOfPerson = dsTransportBooking.Tables[0].Rows[0]["NO_OF_PERSON_TRAVELLING"].ToString();
                string sPurpose = dsTransportBooking.Tables[0].Rows[0]["PURPOSE"].ToString();
                string sPlaceToVisit = dsTransportBooking.Tables[0].Rows[0]["REMARKS"].ToString();
                DateTime dtReportingDateTime = Convert.ToDateTime(dsTransportBooking.Tables[0].Rows[0]["REPORTING_TIME"]);
                string sReportToPerson = dsTransportBooking.Tables[0].Rows[0]["REPORT_TO_NAME"].ToString();
                string sReportToAdd = dsTransportBooking.Tables[0].Rows[0]["REPORT_TO_ADDERSS"].ToString();
                string sReportToContact1 = dsTransportBooking.Tables[0].Rows[0]["REPORT_TO_CONTACT1"].ToString();
                string sReportToContact2 = dsTransportBooking.Tables[0].Rows[0]["REPORT_TO_CONTACT2"].ToString();
                string sInchargeCPF = dsTransportBooking.Tables[0].Rows[0]["INCHARGE_CPF_NO"].ToString();
                string sInchargeName = dsTransportBooking.Tables[0].Rows[0]["INCHARGE_NAME"].ToString();
                string sInchargeDesig = dsTransportBooking.Tables[0].Rows[0]["INCHARGE_DESIG"].ToString();
                string sInchargeStatus = dsTransportBooking.Tables[0].Rows[0]["INCHARGE_STATUS"].ToString();
                DateTime dtInchargeTimestamp = Convert.ToDateTime(dsTransportBooking.Tables[0].Rows[0]["INCHARGE_TIMESTAMP"]);
                string sApproverCPF = dsTransportBooking.Tables[0].Rows[0]["APPROVER_CPF_NO"].ToString();
                string sApproverName = dsTransportBooking.Tables[0].Rows[0]["APPROVER_NAME"].ToString();
                string sApproverDesig = dsTransportBooking.Tables[0].Rows[0]["APPROVER_DESIG"].ToString();
                string sApproverStatus = dsTransportBooking.Tables[0].Rows[0]["APPROVER_STATUS"].ToString();
                DateTime dtApproverTimestamp = Convert.ToDateTime(dsTransportBooking.Tables[0].Rows[0]["APPROVER_TIMESTAMP"]);
                string sAllocaterStatus = dsTransportBooking.Tables[0].Rows[0]["ALLOCATTER_STATUS"].ToString();
                string sVehichNo = dsTransportBooking.Tables[0].Rows[0]["VEHICLE_NO"].ToString();
                string sDriverName = dsTransportBooking.Tables[0].Rows[0]["DRIVER_NAME"].ToString();
                string sDriverContact = dsTransportBooking.Tables[0].Rows[0]["DRIVER_CONTACT"].ToString();

                td_app_name.InnerHtml = sAppName;
                td_app_desig_cpf.InnerHtml = sAppDesig + "(" + sAppCPF + ")";
                td_app_contact.InnerHtml = sReportToContact1 + ", " + sReportToContact2;
                td_app_section.InnerHtml = sAppLocation;
                td_purpose.InnerHtml = sPurpose;
                td_place_to_visit.InnerHtml = sPlaceToVisit + "(" + sNoOfPerson + " Person)";
                td_rptg_date_time.InnerHtml = dtReportingDateTime.ToString("dd/MM/yyyy HH:mm") + " Hrs";
                td_rptg_place.InnerHtml = sReportToAdd;
                td_inc_name.InnerHtml = sInchargeName;
                td_inc_desig.InnerHtml = sInchargeDesig;
                td_inc_timestamp.InnerHtml = dtInchargeTimestamp.ToString("dd/MM/yyyy HH:mm") + " Hrs";
                td_approver_name.InnerHtml = sApproverName;
                td_approver_desig.InnerHtml = sApproverDesig;
                td_approver_timestamp.InnerHtml = dtApproverTimestamp.ToString("dd/MM/yyyy HH:mm") + " Hrs";
                td_vehicle_no.InnerHtml = sVehichNo + " (" + sBookingType + ")";
                td_driver_name.InnerHtml = sDriverName;
                td_driver_contact.InnerHtml = sDriverContact;



            }
        }
    }
}