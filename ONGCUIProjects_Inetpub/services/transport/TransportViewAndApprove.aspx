﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransportViewAndApprove.aspx.cs" Inherits="ONGCUIProjects.services.TransportViewAndApprove" %>
<script type="text/javascript">
    var sSessionCPF = $("#txtSessionCPF").val();
    var sBookingID = $("#txtBookingID").val();

    var sIncharge = $("#txtInchargeStatus").val();
    var sInchargeArr = sIncharge.split('~');
    var sInchargeCPF = sInchargeArr[0];
    var sInchargeStatus = sInchargeArr[1];

    var sApprover = $("#txtApproverStatus").val();
    var sApproverArr = sApprover.split('~');
    var sApproverCPF = sApproverArr[0];
    var sApproverStatus = sApproverArr[1];

    var sAlloter = $("#txtAlloterStatus").val();
    var sAlloterArr = sAlloter.split('~');
    var sAlloterCPF = sAlloterArr[0];
    var sAlloterStatus = sAlloterArr[1];

    if (sInchargeStatus == "APPROVE") {
        $("#div_vehicle_detail").show();
    } else {
        $("#div_vehicle_detail").hide();
    }

    if (sAlloterStatus == "APPROVE") {
        $("#div_vehicle_allotment").remove();
    }

    if (sApproverStatus == "REJECT") {
        $("#div_vehicle_detail").hide();
    }

    /*--------------Start Vehicle autocomplete-----------------------*/
    $("input#txtVehicleNo:not(.ui-autocomplete-input)").live("focus", function (event) {
        var sBookTypeforSearch = $('#ddltypeofbooking').val();
        var sAlloterCPFforSearch = $('#txtAlloterCPFforSearch').val();
        var sExtraData = sBookTypeforSearch + "`" + sAlloterCPFforSearch;
        var VehicleNoSearch_options, VehicleNoSearch_a;
        //jQuery(function () {
        var options = {
            serviceUrl: 'services/auto.aspx',
            onSelect: VehicleNoSearch_onAutocompleteSelect,
            deferRequestBy: 0, //miliseconds
            params: { type: 'vehicle', limit: '10', extra_data: sExtraData },
            noCache: true //set to true, to disable caching
        };
        VehicleNoSearch_a = $("#txtVehicleNo").autocomplete(options);
    });

    var VehicleNoSearch_onAutocompleteSelect = function (VehicleNoSearch_value, VehicleNoSearch_data) {
        if (VehicleNoSearch_data != -1) {
            var VehicleNoSearch_data_arr = VehicleNoSearch_data.split('~');
            $("#txtDriverName").val(VehicleNoSearch_data_arr[0]);
            $("#txtDriverContact").val(VehicleNoSearch_data_arr[1]);

            $('#AllotterAction').children('input[type="button"][hint="REJECT"]').hide();
            $('#AllotterAction').children('p').hide();
            $('#AllotterAction').children('input[type="button"][hint="APPROVE"]').show();
        }
    }
    /*---------------End of Vehicle autocomplete box-------------------*/

    /*--------------Start Driver Name autocomplete-----------------------*/
    $("input#txtDriverName:not(.ui-autocomplete-input)").live("focus", function (event) {
        var sBookTypeforSearch = $('#ddltypeofbooking').val();
        var sAlloterCPFforSearch = $('#txtAlloterCPFforSearch').val();
        var sExtraData = sBookTypeforSearch + "`" + sAlloterCPFforSearch;
        var DriverNameSearch_options, DriverNameSearch_a;
        //jQuery(function () {
        var options = {
            serviceUrl: 'services/auto.aspx',
            onSelect: DriverNameSearch_onAutocompleteSelect,
            deferRequestBy: 0, //miliseconds
            params: { type: 'drivername', limit: '10', extra_data: sExtraData },
            noCache: true //set to true, to disable caching
        };
        DriverNameSearch_a = $("#txtDriverName").autocomplete(options);
    });

    var DriverNameSearch_onAutocompleteSelect = function (DriverNameSearch_value, DriverNameSearch_data) {
        if (DriverNameSearch_data != -1) {
            var DriverNameSearch_data_arr = DriverNameSearch_data.split('~');
            $("#txtVehicleNo").val(DriverNameSearch_data_arr[0]);
            $("#txtDriverContact").val(DriverNameSearch_data_arr[1]);

            $('#AllotterAction').children('input[type="button"][hint="REJECT"]').hide();
            $('#AllotterAction').children('p').hide();
            $('#AllotterAction').children('input[type="button"][hint="APPROVE"]').show();
        }
    }
    /*---------------End of Driver Name autocomplete box-------------------*/

    /*--------------Start Driver Contact autocomplete-----------------------*/
    $("input#txtDriverContact:not(.ui-autocomplete-input)").live("focus", function (event) {
        var sBookTypeforSearch = $('#ddltypeofbooking').val();
        var sAlloterCPFforSearch = $('#txtAlloterCPFforSearch').val();
        var sExtraData = sBookTypeforSearch + "`" + sAlloterCPFforSearch;
        var DriverContactSearch_options, DriverContactSearch_a;
        //jQuery(function () {
        var options = {
            serviceUrl: 'services/auto.aspx',
            onSelect: DriverContactSearch_onAutocompleteSelect,
            deferRequestBy: 0, //miliseconds
            params: { type: 'drivercontact', limit: '10', extra_data: sExtraData },
            noCache: true //set to true, to disable caching
        };
        DriverContactSearch_a = $("#txtDriverContact").autocomplete(options);
    });

    var DriverContactSearch_onAutocompleteSelect = function (DriverContactSearch_value, DriverContactSearch_data) {
        if (DriverContactSearch_data != -1) {
            var DriverContactSearch_data_arr = DriverContactSearch_data.split('~');
            $("#txtVehicleNo").val(DriverContactSearch_data_arr[0]);
            $("#txtDriverName").val(DriverContactSearch_data_arr[1]);

            $('#AllotterAction').children('input[type="button"][hint="REJECT"]').hide();
            $('#AllotterAction').children('p').hide();
            $('#AllotterAction').children('input[type="button"][hint="APPROVE"]').show();
        }
    }
    /*---------------End of Driver contact autocomplete box-------------------*/


    $(".approve_reject").click(function () {
        var type = $(this).attr('hint');
        var type_desig = $(this).attr('type_desig');
        $(this).parent('td');

        switch (type_desig) {
            case 'INCHARGE':
                if (sInchargeCPF == sSessionCPF) {
                    if (confirm("Are you sure to " + type.toLowerCase() + " this request?")) {
                        UpdateStatus();
                    }
                } else {
                    alert('You are not authorised to Approve/Reject this request.');
                }
                break;
            case 'APPROVER':
                if (sApproverCPF == sSessionCPF) {
                    if (sInchargeStatus == 'APPROVE') {
                        if (confirm("Are you sure to " + type.toLowerCase() + " this request?")) {
                            UpdateStatus();
                        }
                    } else {
                        alert('This request is still pending or rejected from Incharge.');
                    }
                } else {
                    alert('You are not authorised to Approve/Reject this request.');
                }
                break;
            case 'ALLOTER':
                if (sAlloterCPF == sSessionCPF) {
                    if (sInchargeStatus == 'APPROVE') {
                        if (type == "APPROVE") {
                            var sVehicleNo = $("#txtVehicleNo").val();
                            var sDriverName = $("#txtDriverName").val();
                            var sDriverContact = $("#txtDriverContact").val();
                            if (sVehicleNo == "") {
                                alert("Please enter a vehicle no.");
                                return false;
                            }
                            if (sDriverName == "") {
                                alert("Please fill driver name.");
                                return false;
                            }
                            if (sDriverContact == "") {
                                alert("Please fill driver contact.");
                                return false;
                            }
                            if (sApproverStatus == "PENDING") {
                                if (confirm("Are you sure to allot the vehicle to this request without approver confirmation?")) {
                                    AllotVehicle();
                                }
                            } else {
                                if (confirm("Are you sure to allot the vehicle this request?")) {
                                    AllotVehicle();
                                }
                            }
                        } else {
                            if (confirm("Are you sure vehicle not availble for this request?")) {
                                UpdateStatus();
                            }
                        }
                    } else {
                        alert('This request is still pending or rejected from Incharge.');
                    }
                } else {
                    alert('You are not authorised to allot the vehicle to this request.');
                }
                break;
        }

        function UpdateStatus() {
            $.ajax({
                type: "POST",
                url: "services/transport/approveorrejectbookingrequest.aspx",
                data: "type=" + type + "&type_desig=" + type_desig + "&BookingID=" + sBookingID,
                success: function (msg) {
                    // alert(msg);
                    var values = msg.split("~");
                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                        $('input[type="button"][type_desig="' + type_desig + '"]').parent('td').html(values[2]);
                        $.gritter.add({
                            title: "Notification",
                            image: "images/tick.png",
                            text: values[3]
                        });
                    } else if (values[0] == "FALSE" && values[1] == "ERR") {
                        alert(values[2]);
                    }
                }
            });
        }
        function AllotVehicle() {
            $.ajax({
                type: "POST",
                url: "services/transport/approveorrejectbookingrequest.aspx",
                data: "type=" + type + "&type_desig=" + type_desig + "&BookingID=" + sBookingID + "&VehicleNo=" + sVehicleNo + "&DriverName=" + sDriverName + "&DriverContact=" + sDriverContact,
                success: function (msg) {
                    // alert(msg);
                    var values = msg.split("~");
                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                        $('input[type="button"][type_desig="' + type_desig + '"]').parent('td').html(values[2]);
                        $.gritter.add({
                            title: "Notification",
                            image: "images/tick.png",
                            text: values[3]
                        });
                    } else if (values[0] == "FALSE" && values[1] == "ERR") {
                        alert(values[2]);
                    }
                }
            });
        }
    });
</script>
<form id="form1" runat="server">
<div id="div_full_form"class="div-fullwidth" style="width: 100%; margin-top: 0;">
    <fieldset style="width: 97%;">  
        <legend class="content" style=" float: none; color: green">Applicant Details</legend>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: 40%;  margin-top: 0;">
                <h1 class="content" style="width:121px;">Applicant Name</h1>
                <input runat="server" id="txtappname" type="text" value=" " class="contactdir-inputbox" style="width:200px;" disabled="disabled" />
            </div>
            <div class="div-fullwidth" style="width: 36%; margin-top: 0;">
                <h1 class="content" style="width:105px;">Designation</h1>
                <input runat="server" id="txtappdesign" type="text" value=" " class="contactdir-inputbox" style="width:180px;" disabled="disabled" />
            </div>
            <div class="div-fullwidth" style="width: 24%; margin-top: 0;">
                <h1 class="content"> CPF No.</h1>
                <input runat="server" id="txtappcpf_no" type="text" value=" " class="contactdir-inputbox" style="width:135px;" disabled="disabled"/>
            </div>
        </div>
        <div class="div-fullwidth">
            <div class="div-fullwidth" style="width: 40%;  margin-top: 0;">
                <h1 class="content" style="width: 121px;">Department</h1>
                <input runat="server" id="txtapplocation" type="text" value=" " class="contactdir-inputbox" style="width:200px;" disabled="disabled"/>
            </div>
            <div class="div-fullwidth" style="width: 36%;  margin-top: 0;">
                <h1 class="content" style="width:105px;">Phone Ex. no.</h1>
                <input runat="server" id="txtappphoneex_no" type="text" value=" " class="contactdir-inputbox" style="width:180px;" disabled="disabled"/>
            </div>
        </div>
    </fieldset>  
    <fieldset style="width: 97%;">  
        <legend class="content" style=" float: none; color: green">Booking Details</legend>
        <div class="div-fullwidth marginbottom">
                        <div class="div-fullwidth" style="width: 30%;  margin-top: 0;">
                                <h1 class="content">Booking Type</h1>
                                <input runat="server" id="ddltypeofbooking" type="text" value="" class="contactdir-inputbox autocompleteclass" style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat; padding-left: 35px; width:130px;" />
                            </div>
                            <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                                <h1 class="content" style="width:110px;">Booking From</h1>
                                <input runat="server" id="txtbookingstartdatetime" type="text" value="" class="contactdir-inputbox" style="width:170px;" />
                            </div>
                            <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                                <h1 class="content" style="width:110px;">Booking To</h1>
                                <input runat="server" id="txtbookingEnddatetime" type="text" value="" class="contactdir-inputbox" style="width:183px;" />
                            </div>
                        </div>
                          
                        <div class="div-fullwidth marginbottom">
                        <div class="div-fullwidth" style="width: 30%; margin-top: 0;">
                                <h1 class="content" >No of Person</h1>
                                <div style="position: relative; float: left;">
                                    <input runat="server" id="no_of_person_travel" type="text" value="" class="contactdir-inputbox" style="width:130px; margin-left:5px;"/>
                                </div>
                            </div>
                            <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                                <h1 class="content" style="width:110px;">Purpose</h1>
                                <input runat="server" id="txtpurpose" type="text" value="" class="contactdir-inputbox" style="width:170px;"/>
                            </div>
                            <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                                <h1 class="content" style="width:110px;">Place To Visit</h1>
                                <input runat="server" id="txtremarks"   type="text" value="" class="contactdir-inputbox" style="width:183px;"/>
                            </div>
                        </div>
    </fieldset>
    <fieldset style="width: 97%;">  
        <legend class="content" style=" float: none; color: green">Reporting Details</legend>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                <h1 class="content marginbottom">Report To Name</h1>
                <input runat="server" id="reporting_name" type="text" value=" " class="contactdir-inputbox" style="width:150px; float:right;margin-right:10px;" />
            </div>
            <div class="div-fullwidth" style="width: 60%;margin-top: 0;">
                <h1 class="content marginbottom" >Report To Address</h1>
                <input runat="server" id="reporting_address" type="text" value=" " class="contactdir-inputbox" style="float: right; width: 368px;"/>
            </div>
        </div>
        <div class="div-fullwidth">
            <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                <h1 class="content marginbottom">Reporting Date & Time</h1>
                <input runat="server" id="reporting_time" type="text" value=" " class="contactdir-inputbox" style="width:150px; float:right;margin-right:10px;"  />
            </div>
            <div class="div-fullwidth" style="width: 30%;margin-top: 0;">
                <h1 class="content marginbottom">Contact 1</h1>
                <input runat="server" id="reporting_contact1" type="text" value=" " class="contactdir-inputbox" style="width:150px; float:right;margin-right:10px;" />
            </div>
            <div class="div-fullwidth" style="width: 30%; margin-top: 0;">
                <h1 class="content marginbottom">Contact 2</h1>
                <input runat="server" id="reporting_contact2" type="text" value=" " class="contactdir-inputbox" style="width:150px; float:right;" />
            </div>
        </div>
    </fieldset >
    <fieldset style="width: 97%;"  id="div_vehicle_allotment">  
        <legend class="content" style=" float: none; color: green">Authority Actions</legend> 
    <div runat="server" id="div_approving_authorities">

    </div>
    </fieldset>
    <fieldset style="width: 97%;" id="div_vehicle_detail">  
        <legend class="content" style=" float: none; color: green">Transport Details</legend>
            <div class="div-fullwidth">
            <div class="div-fullwidth" style="width: 35%;  margin-top: 0;">
                <h1 class="content">Vehicle No</h1>

                <input runat="server" id="txtVehicleNo" type="text" value=" " class="contactdir-inputbox" style="width:200px;" />
            </div>
            <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                <h1 class="content">Driver Name</h1>
                <input runat="server" id="txtDriverName" type="text" value=" " class="contactdir-inputbox" style="width:180px;" />
            </div>
            <div class="div-fullwidth" style="width: 30%; margin-top: 0;">
                <h1 class="content">Driver Contact</h1>
                <input runat="server" id="txtDriverContact" type="text" value=" " class="contactdir-inputbox" style="width:135px;" />
            </div>
        </div>
    </fieldset >

</div>


<asp:hiddenfield runat="server" ID="txtBookingID"></asp:hiddenfield>
<asp:hiddenfield runat="server" ID="txtSessionCPF"></asp:hiddenfield>
<asp:hiddenfield runat="server" ID="txtInchargeStatus"></asp:hiddenfield>
<asp:hiddenfield runat="server" ID="txtApproverStatus"></asp:hiddenfield>
<asp:hiddenfield runat="server" ID="txtAlloterStatus"></asp:hiddenfield>


<asp:hiddenfield runat="server" ID="txtAlloterCPFforSearch"></asp:hiddenfield>
</form>
