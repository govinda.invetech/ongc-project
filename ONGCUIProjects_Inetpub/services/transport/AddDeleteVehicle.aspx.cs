﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.transport
{
    public partial class AddDeleteVehicle : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sOutput = "";
                string sQuery = "";
                string msg = "";
                try
                {
                    if (string.IsNullOrEmpty(Request["type"]))
                    {
                        sOutput = "FALSE~ERR~Proplem in input, please contact admin";
                    }
                    else
                    {
                        string sType = Request["type"].ToString();
                        switch (sType)
                        {
                            case "ADD":
                                if (string.IsNullOrEmpty(Request["VehicleType"]) || string.IsNullOrEmpty(Request["VehicleNo"]))
                                {
                                    sOutput = "FALSE~ERR~Proplem in input, please contact admin";
                                }
                                else
                                {
                                    string sCPFNo = Session["Login_Name"].ToString();
                                    string VehicleType = Request["VehicleType"].ToString();
                                    string VehicleNo = Request["VehicleNo"].ToString();
                                    string DriverName = Request["DriverName"].ToString();
                                    string DriverContact = Request["DriverContact"].ToString();
                                    string AlloterCPF = Request["AlloterCPF"].ToString();
                                    sQuery = "INSERT INTO [CY_TRANSPORT_DRIVER]([ALLOTER_CPF],[VEHICLE_TYPE],[VEHICLE_NO],[DRIVER_NAME],[CONTACT],[ENTRY_BY])";
                                    sQuery += "VALUES ('" + AlloterCPF + "','" + VehicleType + "','" + VehicleNo + "','" + DriverName + "','" + DriverContact + "','" + sCPFNo + "')";
                                }
                                msg = "Vehicle Added Succesfully";
                                break;
                            case "DEL":
                                if (string.IsNullOrEmpty(Request["DelID"]))
                                {
                                    sOutput = "FALSE~ERR~Proplem in input, please contact admin";
                                }
                                else
                                {
                                    string sDelID = Request["DelID"].ToString();
                                    sQuery = "DELETE FROM [CY_TRANSPORT_DRIVER] WHERE [ID] = '" + sDelID + "'";
                                    msg = "Vehicle Deleted Succesfully";
                                }
                                break;
                        }
                        if (sQuery != "")
                        {
                            if (My.ExecuteSQLQuery(sQuery) == true)
                            {
                                sOutput = "TRUE~SUCCESS~" + msg;
                            }
                            else
                            {
                                sOutput = "FALSE~ERR~Proplem in execution, please contact admin";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    sOutput = "FALSE~ERR~" + ex.Message;
                }
                Response.Write(sOutput);
            }
        }
    }
}