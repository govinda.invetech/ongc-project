﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;

namespace ONGCUIProjects.services
{
    public partial class auto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           string session_cpf_no = "";
            if (Session["Login_Name"] == null)
            {

            }
            else
            {
                session_cpf_no = Session["Login_Name"].ToString();
            }
            string data = Request.QueryString["query"];
            string type = Request.QueryString["type"];
            string extra_data = Request.QueryString["extra_data"];
            string complain_type_id = Request.QueryString["complain_type_id"];
            data = data.ToLower();
            // basic json structure   "data":["903","950"]
            string json = "{ \"query\":\"" + data + "\",";
            string json1 = " \"suggestions\":[<putdata1>],";
            string json2 = " \"data\":[<putdata2>],";
            string json1data = "";
            string json2data = "";
            string fulljsonstring = json + json1 + json2;
            //    \"query\":\"s\"
            //   , \"suggestions\":[\"AARNA BHAN [Nursery A]\",\"AARNA RAY [Nursery A]\"], \"data\":[\"903\",\"950\"] }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "";
            switch (type)
            {
                case "emp_list":
                    // sStr = "SELECT a.[O_CPF_NMBR],a.[O_EMP_NM]  FROM [O_EMP_MSTR] a,[E_USER_MSTR] b where LOWER(a.[O_EMP_NM]) LIKE '%" + data + "%' AND (b.[CY_STATUS]='ACTIVE' OR b.[CY_STATUS]='ADMINACTIVE') AND a.[O_CPF_NMBR]=b.[E_USER_CODE] ";
                    //   sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM] FROM [O_EMP_MSTR]INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') LEFT JOIN (SELECT [USER_ID],MAX([TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID])[USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] WHERE LOWER([O_EMP_NM]) LIKE '%" + data + "%' AND DATEDIFF(DAY,[TIMESTAMP],getdate())<60 ORDER BY [O_EMP_NM] ASC"; 

                    //---------change by shiv shakti on 30/03/2015 for purpose of correction in near miss forword by incident manager to FPR-----------//  
                    sStr = "SELECT   [O_EMP_NM],O_CPF_NMBR AS [O_CPF_NMBR] FROM CY_EMP_STATUS WHERE CY_STATUS='ACTIVE' AND LOWER([O_EMP_NM]) LIKE '%" + data + "%' ORDER BY [O_EMP_NM] ASC ";

                    break;
                case "cmpny_list":
                    sStr = "SELECT distinct([O_VISTR_ORGNZTN]),[O_VISTR_ORGNZTN]  FROM [O_VSTR_GATE_PASS_RQSTN_DTL] where LOWER([O_VISTR_ORGNZTN]) LIKE '%" + data + "%'";
                    break;
                case "cmpny_name_list":
                    sStr = "SELECT DISTINCT([O_VISTR_NM])+ '~' +[O_VISTR_AGE]+ '~' +[O_VISTR_GNDR], [O_VISTR_NM]+ ' [' +[O_VISTR_AGE]+ '] [' +[O_VISTR_GNDR]+']' FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE lower([O_VISTR_ORGNZTN]) LIKE '%" + extra_data + "%' and lower([O_VISTR_NM]) LIKE '%" + data + "%'";
                    break;
                case "desig_list":
                    sStr = "SELECT [O_INDX_NMBR],[O_DSG_DTL] FROM [O_DSG_MSTR] WHERE [O_DSG_DTL] != '' AND LOWER([O_DSG_DTL]) LIKE '%" + data + "%' ORDER BY [O_DSG_DTL] ASC";
                    break;
                case "contact_category_list":
                    sStr = "SELECT [id], [name] FROM [CY_CONTACT_CATEGORY_DTLS] WHERE lower([name]) LIKE '%" + data + "%' ORDER BY [name] ASC";
                    break;
                case "emp_meeting":
                    sStr = "SELECT a.[O_CPF_NMBR],a.[O_EMP_NM]  FROM [O_EMP_MSTR] a,[E_USER_MSTR] b where LOWER(a.[O_EMP_NM]) LIKE '%" + data + "%' and a.[O_CPF_NMBR]!='" + session_cpf_no + "' AND (b.CY_STATUS='ACTIVE' OR b.CY_STATUS='ADMINACTIVE') AND a.[O_CPF_NMBR]=b.[E_USER_CODE]";
                    break;
                case "emp_for_guesthouse":
                    // sStr = "SELECT (a.[O_CPF_NMBR] + '~' + a.[O_EMP_DESGNTN] + '~' + a.[O_EMP_WRK_LCTN]+'~'+a.[CY_EMP_EX_NO]+'~'+a.[O_EMP_MBL_NMBR]) ,a.[O_EMP_NM] FROM [O_EMP_MSTR] a, [E_USER_MSTR] b where LOWER(a.[O_EMP_NM]) LIKE '%" + data + "%' AND (b.CY_STATUS='ACTIVE' OR b.CY_STATUS='ADMINACTIVE') AND a.[O_CPF_NMBR]=b.[E_USER_CODE]";
                    // sStr = "SELECT ([O_CPF_NMBR] + '~' + [O_EMP_DESGNTN] + '~' + [O_EMP_WRK_LCTN]+'~'+[CY_EMP_EX_NO]+'~'+[O_EMP_MBL_NMBR]),[O_EMP_NM] FROM [O_EMP_MSTR]INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') LEFT JOIN (SELECT [USER_ID],MAX([TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID])[USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] WHERE LOWER([O_EMP_NM]) LIKE '%" + data + "%' AND DATEDIFF(DAY,[TIMESTAMP],getdate())<60 ORDER BY [O_EMP_NM] ASC";
                    sStr = "SELECT ([O_CPF_NMBR] + '~' + [O_EMP_DESGNTN] + '~' + [O_EMP_WRK_LCTN]+'~'+[CY_EMP_EX_NO]+'~'+[O_EMP_MBL_NMBR]),[O_EMP_NM] AS O_EMP_NM   FROM CY_EMP_STATUS  WHERE LOWER([O_EMP_NM]) LIKE '%" + data + "%' AND  CY_STATUS='ACTIVE' ORDER BY [O_EMP_NM] ASC";
                    break;
                case "GuestHouseApprover":
                    //sStr = "SELECT ([CY_MENU_EMP_RELATION].[cpf_number]+'~'+[O_EMP_MSTR].[O_EMP_DESGNTN]),[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION],[O_EMP_MSTR],[E_USER_MSTR]  where [CY_MENU_EMP_RELATION].[child_id]='13' and [O_EMP_MSTR].[O_CPF_NMBR]= [CY_MENU_EMP_RELATION].[cpf_number] and ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') AND [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] and LOWER([O_EMP_MSTR].[O_EMP_NM]) LIKE '%" + data + "%'";
                    sStr = "SELECT ([CY_MENU_EMP_RELATION].[cpf_number]+'~'+[O_EMP_MSTR].[O_EMP_DESGNTN]),[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION]INNER JOIN [O_EMP_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR]= [CY_MENU_EMP_RELATION].[cpf_number] INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] LEFT JOIN (SELECT [USER_ID], MAX([CY_LOGIN_LOGS].[TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID]) [USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] where [CY_MENU_EMP_RELATION].[child_id]='13' AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') AND DATEDIFF(DAY,[USER_LOGINS].[TIMESTAMP],GETDATE())<60 and LOWER([O_EMP_MSTR].[O_EMP_NM]) LIKE '%" + data + "%'";
                    break;
                case "GuestHouseAlottement":
                    // sStr = "SELECT ([CY_MENU_EMP_RELATION].[cpf_number]+'~'+[O_EMP_MSTR].[O_EMP_DESGNTN]),[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION],[O_EMP_MSTR],[E_USER_MSTR]   where [CY_MENU_EMP_RELATION].[child_id]='72' and [O_EMP_MSTR].[O_CPF_NMBR]= [CY_MENU_EMP_RELATION].[cpf_number] and ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') AND [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] and LOWER([O_EMP_MSTR].[O_EMP_NM]) LIKE '%" + data + "%'";
                    sStr = "SELECT ([CY_MENU_EMP_RELATION].[cpf_number]+'~'+[O_EMP_MSTR].[O_EMP_DESGNTN]),[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION]INNER JOIN [O_EMP_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR]= [CY_MENU_EMP_RELATION].[cpf_number] INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] LEFT JOIN (SELECT [USER_ID], MAX([CY_LOGIN_LOGS].[TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID]) [USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] where [CY_MENU_EMP_RELATION].[child_id]='72' AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') AND DATEDIFF(DAY,[USER_LOGINS].[TIMESTAMP],GETDATE())<60 and LOWER([O_EMP_MSTR].[O_EMP_NM]) LIKE '%" + data + "%'";
                    break;
                case "prblm_desc":
                    sStr = "SELECT DISTINCT([PROBLEM_DISCRIPTION]),PROBLEM_DISCRIPTION FROM [CY_COMPLAIN_REGISTER_DETAIL] where [COMPLAIN_DEPARTMENT]='" + extra_data + "' and [COMPLAIN_TYPE_ID]='" + complain_type_id + "' and [PROBLEM_DISCRIPTION]<>'' and [PROBLEM_DISCRIPTION] LIKE '%" + data + "%'";
                    break;
                case "TENDER_MEETING":
                    sStr = "SELECT DISTINCT([CY_MTNG_TYPE]),[CY_MTNG_TYPE]  FROM [O_TC_MTNG_DTL] where [CY_ENTRY_BY]='" + session_cpf_no + "' and [CY_MTNG_TYPE] LIKE '%" + data + "%'";
                    break;
                case "transport_type":
                    sStr = "SELECT DISTINCT([VEHICLE_TYPE]),[VEHICLE_TYPE] FROM [CY_TRANSPORT_DRIVER] WHERE [VEHICLE_TYPE] LIKE '%" + data + "%'";
                    break;
                case "vehicle":
                    string[] sExtraDataVehicle = extra_data.Split('`');
                    sStr = "SELECT ([DRIVER_NAME]+'~'+[CONTACT]),[VEHICLE_NO] FROM [CY_TRANSPORT_DRIVER] WHERE [ALLOTER_CPF] = '" + sExtraDataVehicle[1] + "' AND UPPER([VEHICLE_TYPE])='" + sExtraDataVehicle[0].ToUpper() + "' AND [VEHICLE_NO] LIKE '%" + data + "%'";
                    break;
                case "drivername":
                    string[] sExtraDataDriver = extra_data.Split('`');
                    sStr = "SELECT ([VEHICLE_NO]+'~'+[CONTACT]),[DRIVER_NAME] FROM [CY_TRANSPORT_DRIVER] WHERE [ALLOTER_CPF] = '" + sExtraDataDriver[1] + "' AND UPPER([VEHICLE_TYPE])='" + sExtraDataDriver[0].ToUpper() + "' AND[DRIVER_NAME] LIKE '%" + data + "%'";
                    break;
                case "drivercontact":
                    string[] sExtraDataDrvContact = extra_data.Split('`');
                    sStr = "SELECT ([VEHICLE_NO]+'~'+[DRIVER_NAME]),[CONTACT] FROM [CY_TRANSPORT_DRIVER] WHERE [ALLOTER_CPF] = '" + sExtraDataDrvContact[1] + "' AND UPPER([VEHICLE_TYPE])='" + sExtraDataDrvContact[0].ToUpper() + "' AND [CONTACT] LIKE '%" + data + "%'";
                    break;
                case "CitySearch":
                    sStr = "SELECT ([CITY_NAME]+'~'+[STATE_NAME]) AS 'DATA',([CITY_NAME]+' ['+[STATE_NAME]+']') AS 'RESULT' FROM [CY_STATE_CITY_DETAILS] WHERE [CITY_NAME] LIKE UPPER ('%" + data + "%')";
                    break;
                case "StateSearch":
                    sStr = "SELECT DISTINCT([STATE_NAME]),[STATE_NAME] FROM [CY_STATE_CITY_DETAILS] WHERE [STATE_NAME] LIKE UPPER ('%" + data + "%')";
                    break;
                case "TransportIncharge":
                    sStr = "SELECT ([O_CPF_NMBR] + '~' + [O_EMP_DESGNTN] + '~' + [O_EMP_WRK_LCTN]+'~'+[CY_EMP_EX_NO]+'~'+[O_EMP_MBL_NMBR]) ,[O_EMP_NM] FROM [O_EMP_MSTR] where LOWER([O_EMP_NM]) LIKE '%" + data + "%'";
                    break;
                case "TransportApprover":
                    sStr = "SELECT ([CY_MENU_EMP_RELATION].[cpf_number]+'~'+[O_EMP_MSTR].[O_EMP_DESGNTN]),[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION],[O_EMP_MSTR]  where [CY_MENU_EMP_RELATION].[child_id]='15' and [O_EMP_MSTR].[O_CPF_NMBR]= [CY_MENU_EMP_RELATION].[cpf_number] and LOWER([O_EMP_MSTR].[O_EMP_NM]) LIKE '%" + data + "%'";
                    break;
                case "TransportAlloter":
                    sStr = "SELECT ([CY_MENU_EMP_RELATION].[cpf_number]+'~'+[O_EMP_MSTR].[O_EMP_DESGNTN]),[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION],[O_EMP_MSTR]  where [CY_MENU_EMP_RELATION].[child_id]='75' and [O_EMP_MSTR].[O_CPF_NMBR]= [CY_MENU_EMP_RELATION].[cpf_number] and LOWER([O_EMP_MSTR].[O_EMP_NM]) LIKE '%" + data + "%'";
                    break;
                case "contact":
                    sStr = "SELECT ([ID]+'~'+[DEPARTMENT]+'~'+[TYPE]),[NAME] FROM [CY_VIEW_CONTACTS] WHERE ([ID]+'~'+[NAME]) LIKE '%" + data + "%'";
                    break;
                #region Incident
                case "area_location_name":
                    //sStr = "SELECT [O_EMP_MSTR].[O_EMP_DEPT_NM],[CY_INC_0_LOCATION_MSTR].[LOCATION_NAME] FROM [CY_INC_0_LOCATION_MSTR],[O_EMP_MSTR] WHERE [O_EMP_MSTR].[O_CPF_NMBR] = [CY_INC_0_LOCATION_MSTR].[LOCATION_MANAGER] AND [CY_INC_0_LOCATION_MSTR].[AREA_NAME] = '" + extra_data + "' AND [CY_INC_0_LOCATION_MSTR].[LOCATION_NAME] LIKE '%" + data + "%'";
                    sStr = "SELECT DISTINCT([LOCATION_NAME]),[LOCATION_NAME] FROM [CY_INC_0_LOCATION_MSTR] WHERE [AREA_NAME] = '" + extra_data + "' AND [LOCATION_NAME] LIKE '%" + data + "%'";
                    break;
                case "area_name":
                    sStr = "SELECT DISTINCT([AREA_NAME]),[AREA_NAME] FROM [CY_INC_0_LOCATION_MSTR] WHERE [AREA_NAME] LIKE '%" + data + "%'";
                    break;
                case "incdnt_mgr":
                    //sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '6' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%'";
                    sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '6' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') LEFT JOIN (SELECT [USER_ID],MAX([TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID])[USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%' AND DATEDIFF(DAY,[USER_LOGINS].[TIMESTAMP],getdate())<60 ORDER BY [O_EMP_NM] ASC ";
                    break;
                case "incdnt_fpr":
                    //sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '8' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%'";
                    sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '8' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') LEFT JOIN (SELECT [USER_ID],MAX([TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID])[USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%' AND DATEDIFF(DAY,[USER_LOGINS].[TIMESTAMP],getdate())<60 ORDER BY [O_EMP_NM] ASC ";
                    break;
                case "incdnt_level_1":
                    //sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '5' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%'";
                    sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '5' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') LEFT JOIN (SELECT [USER_ID],MAX([TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID])[USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%' AND DATEDIFF(DAY,[USER_LOGINS].[TIMESTAMP],getdate())<60 ORDER BY [O_EMP_NM] ASC ";
                    break;
                case "incdnt_level_2":
                    // sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '9' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%'";
                    sStr = "SELECT [O_EMP_MSTR].[O_CPF_NMBR], [O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '9' inner join [E_USER_MSTR] on [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') LEFT JOIN (SELECT [USER_ID],MAX([TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID])[USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] WHERE [O_EMP_MSTR].[O_EMP_NM] LIKE '%" + data + "%' AND DATEDIFF(DAY,[USER_LOGINS].[TIMESTAMP],getdate())<60 ORDER BY [O_EMP_NM] ASC ";
                    break;
                #endregion Incident
                default:
                    return;
            }

            #region else if which ashish karn has changed to switch case
            /*if (type == "emp_list")
                {
                     sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM]  FROM [O_EMP_MSTR] where LOWER([O_EMP_NM]) LIKE '%" + data + "%'";
                }
                else if (type == "cmpny_list") {
                    sStr = "SELECT distinct([O_VISTR_ORGNZTN]),[O_VISTR_ORGNZTN]  FROM [O_VSTR_GATE_PASS_RQSTN_DTL] where LOWER([O_VISTR_ORGNZTN]) LIKE '%" + data + "%' ORDER BY [O_VISTR_ORGNZTN]";
                } else if (type == "cmpny_name_list") {
                    sStr = "SELECT distinct([O_VISTR_NM]),[O_VISTR_AGE] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE lower([O_VISTR_ORGNZTN]) LIKE '%" + extra_data + "%' and lower([O_VISTR_NM]) LIKE '%" + data + "%'";
                }

                else
                {
                    return;

                }*/
            #endregion

            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                fulljsonstring = fulljsonstring.Replace("<putdata1>", "\"Enter something else..\"");
                fulljsonstring = fulljsonstring.Replace("<putdata2>", "-1");
                fulljsonstring += " }";
                Response.Write(fulljsonstring);
            }
            else
            {
                //int count = 0;
                if (type == "emp_list")/*---for displaying name with CPF Number in Serch Employee 10-feb-2016---*/
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        json1data = json1data + "\"" +ds.Tables[0].Rows[i][0]+"/"+ds.Tables[0].Rows[i][1] + "\",";
                        json2data = json2data + "\"" + ds.Tables[0].Rows[i][0] + "\",";
                    }
                }
                else
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        json1data = json1data + "\"" + ds.Tables[0].Rows[i][1] + "\",";
                        json2data = json2data + "\"" + ds.Tables[0].Rows[i][0] + "\",";
                    }
                }

                json1data = json1data.Substring(0, json1data.Length);
                json2data = json2data.Substring(0, json2data.Length);
                fulljsonstring = fulljsonstring.Replace("<putdata1>", json1data);
                fulljsonstring = fulljsonstring.Replace("<putdata2>", json2data);
                fulljsonstring += " }";
                Response.Write(fulljsonstring);
            }
        }
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public string[] GetAutoCompleteDetail(string prefix,string type, string limit, string extra_data)
        //{
        //    List<string> customers = new List<string>();
        //    string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"].ToString();
        //    SqlConnection conn = new SqlConnection(strConn);
        //    SqlDataAdapter da = new SqlDataAdapter();
        //    DataSet ds = new DataSet();

        //    string query = "SELECT DISTINCT([LOCATION_NAME]),[LOCATION_NAME] FROM [CY_INC_0_LOCATION_MSTR] WHERE [AREA_NAME] = '" + extra_data + "' AND [LOCATION_NAME] LIKE '%" + prefix + "%'";
        //    da = new SqlDataAdapter(query, conn);
        //    da.Fill(ds);

        //    if (ds.Tables[0].Rows.Count == 0)
        //        customers.Add(string.Format("{0}-{1}", "Enter something else..", "0"));
        //    else
        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    {
        //      customers.Add(string.Format("{0}-{1}", ds.Tables[0].Rows[i]["ContactName"].ToString(),ds.Tables[0].Rows[i]["ContactName"].ToString()));
        //    }
        //    return customers.ToArray();
        //}

    }
}