﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services
{
    public partial class Approveuserandupdatedetail : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();
        SqlConnection conn;
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = Request.QueryString["type"];

            string e_name = "";
            e_name = Request.QueryString["e_name"];
            string cpf_no = "";
            cpf_no = Request.QueryString["cpf_no"];
            string add1 = Request.QueryString["add1"];
            string add2 = Request.QueryString["add2"];
            string city = Request.QueryString["city"];
            string state = Request.QueryString["state"];
            string pin_no = Request.QueryString["pin_no"];
            string home_ph_no = Request.QueryString["home_ph_no"];
            string mbl_no = Request.QueryString["mbl_no"];
            string gender = Request.QueryString["gender"];
            string designation = Request.QueryString["designation"];
            string department = Request.QueryString["department"];
            string worklocation = Request.QueryString["worklocation"];
            string dob = my.ConvertDateStringintoSQLDateString(Request.QueryString["dob"].ToString());
            string email1 = Request.QueryString["email1"];
            string email2 = Request.QueryString["email2"];
            string email3 = "";
            string blood_group = Request.QueryString["blood_group"];

            string supervisior = Request.QueryString["supervisior"];
            string employee_ex_no = Request.QueryString["employee_ex_no"];

            if (string.IsNullOrEmpty(e_name))
            {
                Response.Write("FALSE~SUCCESS~Please Enter Employee Name First~");
                return;
            }
            else if (string.IsNullOrEmpty(cpf_no))
            {
                Response.Write("FALSE~SUCCESS~Please Enter Cpf No. First~");
                return;
            }
            else
            {
                e_name = e_name.Replace("~", "");
                cpf_no = cpf_no.Replace("~", "");
                add1 = add1.Replace("~", "");
                add2 = add2.Replace("~", "");
                city = city.Replace("~", "");
                state = state.Replace("~", "");
                pin_no = pin_no.Replace("~", "");
                home_ph_no = home_ph_no.Replace("~", "");
                mbl_no = mbl_no.Replace("~", "");
                gender = gender.Replace("~", "");
                designation = designation.Replace("~", "");
                if (checkcpfno_cybuzz(cpf_no) == "FALSE" && type != "UPDATE")
                {

                    Response.Write("FALSE~SUCCESS~this cpf no is not available to approve~");
                    return;
                }
                else if (checkcpfno_old(cpf_no) == "TRUE" && type != "UPDATE")
                {
                    Response.Write("FALSE~SUCCESS~this cpf no is Already approved~");
                    return;
                }
                else if (checkcpfno_old(cpf_no) == "FALSE" && type == "UPDATE")
                {
                    Response.Write("FALSE~SUCCESS~Detailed Not Saved~");
                    return;
                }


                if (type == "UPDATE")
                {
                    deletecpfnodetailformo_emp_mstr(cpf_no);
                }
                //sql connection
                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                string sqlQuery = "INSERT INTO O_EMP_MSTR([O_CPF_NMBR],[O_EMP_NM],[O_EMP_HM_ADDR1],[O_EMP_HM_ADDR2],[O_EMP_HM_CITY] ,[O_EMP_HM_STATE],[O_EMP_HM_PIN_CD],[O_EMP_HM_PHN],[O_EMP_MBL_NMBR] ,[O_EMP_GNDR],[O_EMP_DOB] ,[O_EMP_BLD_GRP]  ,[O_EMAIL_ID1],[O_EMAIL_ID3] ,[O_EMAIL_ID2],[O_EMP_DEPT_NM],[O_EMP_WRK_LCTN] ,[O_EMP_DESGNTN],[O_EMP_SUP_NM],[CY_EMP_EX_NO]) ";
                sqlQuery = sqlQuery + "values('" + cpf_no + "','" + e_name + "','" + add1 + "' ,'" + add2 + "','" + city + "','" + state + "','" + pin_no + "','" + home_ph_no + "','" + mbl_no + "','" + gender + "','" + dob + "' ,'" + blood_group + "','" + email1 + "','" + email3 + "','" + email2 + "','" + department + "','" + worklocation + "','" + designation + "','" + supervisior + "','" + employee_ex_no + "')";
                if (my.ExecuteSQLQuery(sqlQuery))
                {
                    deletependinginformation(cpf_no);

                    if (type != "UPDATE")
                    {
                        inserusernameandpassword(cpf_no);
                        DefaultPrivilegeToEmp(cpf_no);
                        Response.Write("TRUE~SUCCESS~User Request Approved Successfully~");
                    }
                    else
                    {
                        Response.Write("TRUE~SUCCESS~User Detail Updated Successfully~");
                    }

                }


            }

        }
        private string checkcpfno_cybuzz(string cpf_no)
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_CPF_NMBR]FROM [CY_O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }

        }
        private string checkcpfno_old(string cpf_no)
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_CPF_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }
        }
        private void deletecpfnodetailformo_emp_mstr(string cpf_no)
        {
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);

            string insQuery = "DELETE FROM [O_EMP_MSTR] WHERE [O_CPF_NMBR]='" + cpf_no + "'";
            SqlCommand cmd = new SqlCommand(insQuery);
            cmd.Connection = conn;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        private void deletependinginformation(string cpf_no)
        {
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);

            string insQuery = "DELETE FROM [CY_O_EMP_MSTR] WHERE [O_CPF_NMBR]='" + cpf_no + "'";
            SqlCommand cmd = new SqlCommand(insQuery);
            cmd.Connection = conn;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        private void inserusernameandpassword(string cpf_no)
        {
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            string insQuery = "INSERT INTO   E_USER_MSTR( E_USER_CODE, E_USER_DSCRPTN, E_USER_PSWRD) VALUES('" + cpf_no + "','" + cpf_no + "','" + cpf_no + "') ";
            SqlCommand cmd = new SqlCommand(insQuery);
            cmd.Connection = conn;
            conn.Open();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string a = e.Message;
            }

            conn.Close();
        }
        private void DefaultPrivilegeToEmp(string sCPF)
        {
            string Sessionid = "1";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds3 = new DataSet();
            string sStr_3 = "SELECT id FROM CY_CHILD_MENU WHERE default_checked = 'TRUE'";
            da = new SqlDataAdapter(sStr_3, conn);
            da.Fill(ds3);
            if (ds3.Tables[0].Rows.Count != 0)
            {
                string QueryString = "";
                for (int i = 0; i < ds3.Tables[0].Rows.Count; i++)
                {
                    int defaultChildId = System.Convert.ToInt32(ds3.Tables[0].Rows[i][0].ToString());
                    QueryString = QueryString + "('" + sCPF + "','" + defaultChildId + "','" + Sessionid + "'),";
                }

                QueryString = "INSERT INTO CY_MENU_EMP_RELATION (cpf_number,child_id,entry_by) VALUES " + QueryString.Substring(0, QueryString.Length - 1); ;
                SqlCommand cmd = new SqlCommand(QueryString);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

        }

    }
}