﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services
{
    public partial class SaveFeedback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            string sCPFNo = Session["Login_Name"].ToString();
            string sRemark = Request.QueryString["Comment"];
            string sRate = Request.QueryString["Rate"];

            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            try
            {
                if (My.InsertFeedback(sCPFNo, sRemark, sRate) == true)
                {
                    Response.Write("TRUE~SUCCESS~Feedback Received. Thank You.");
                }
                else
                {
                    Response.Write("FALSE~ERR~Some internal error");
                }
            }
            catch (Exception ex)
            {
                Response.Write("FALSE~ERR~" + ex.Message);
            }
        }
    }
}