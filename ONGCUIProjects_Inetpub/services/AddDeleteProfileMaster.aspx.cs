﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class AddDeleteProfileMaster : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '../../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                try
                {
                    string sSessionCPF=Session["Login_Name"].ToString();
                    string sActionType = Request["ActionType"].ToString();
                    string sActionData = Request["ActionData"].ToString();
                    string sMasterType = My.sDataStringTrim(2, sActionData);
                    string sMasterText = My.sDataStringTrim(1, sActionData);
                    string sQuery = "";
                    string sMessage = "";

                    if (sActionType == "ADD")
                    {
                        switch (sMasterType)
                        {
                            case "DESIGNATION":
                                sQuery = "INSERT INTO [O_DSG_MSTR] ([O_DSG_DTL], [CY_ENTRY_BY]) VALUES ('" + sMasterText + "','" + sSessionCPF + "')";
                                break;
                            case "LOCATION":
                                sQuery = "INSERT INTO [O_LCTN_MSTR] ([O_LCTN_NM], [CY_ENTRY_BY]) VALUES ('" + sMasterText + "','" + sSessionCPF + "')";
                                break;
                            case "DEPARTMENT":
                                sQuery = "INSERT INTO [CY_DEPT_DTLS] ([name], [entry_by]) VALUES ('" + sMasterText + "','" + sSessionCPF + "')";
                                break;
                        }
                        sMessage = "Added successfully";
                    }
                    else if (sActionType == "DEL")
                    {
                        switch (sMasterType)
                        {
                            case "DESIGNATION":
                                sQuery = "DELETE FROM [O_DSG_MSTR] WHERE [O_INDX_NMBR] = '" + sMasterText + "'";
                                break;
                            case "LOCATION":
                                sQuery = "DELETE FROM [O_LCTN_MSTR]  WHERE [O_INDX_NMBR] = '" + sMasterText + "'";
                                break;
                            case "DEPARTMENT":
                                sQuery = "DELETE FROM [CY_DEPT_DTLS]  WHERE [id] = '" + sMasterText + "'";
                                break;
                        }
                        sMessage = "Deleted successfully";
                    }
                    if (sQuery != "")
                    {
                        if (My.ExecuteSQLQuery(sQuery) == true)
                        {
                            Response.Write("TRUE~SUCCESS~" + sMessage);
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ADDDELPROMSTR162");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
            }
        }
    }
}