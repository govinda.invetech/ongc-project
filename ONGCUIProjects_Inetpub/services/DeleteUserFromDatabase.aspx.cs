﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services
{
    public partial class DeleteUserFromDatabase : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                try
                {
                    string sCPFNo = Request["cpfno"].ToString();
                    string message= My.deleteEmployeeSP("sp_delete_o_emp_mstr", sCPFNo);
                    Response.Write("TRUE~SUCCESS~" + message + "");
                    
                    //string sDeleteQuery1 = "DELETE FROM [E_USER_MSTR] WHERE [E_USER_CODE] = '" + sCPFNo + "'";
                    //string sDeleteQuery2 = "DELETE FROM [CY_MENU_EMP_RELATION] WHERE [cpf_number] = '" + sCPFNo + "'";

                    //if (My.ExecuteSQLQuery(sDeleteQuery1) == true)
                    //{
                    //    if (My.ExecuteSQLQuery(sDeleteQuery2) == true)
                    //    {
                    //        Response.Write("TRUE~SUCCESS~User successfully removed from database.");
                    //    }
                    //    else
                    //    {
                    //        Response.Write("TRUE~SUCCESS~My App has occured some error.");
                    //    }
                    //}
                    //else
                    //{
                    //    Response.Write("TRUE~SUCCESS~My App has occured some error.");
                    //}
                }
                catch (Exception ex)
                {
                    Response.Write("TRUE~SUCCESS~"+ex.Message);
                }
            }
        }
    }
}