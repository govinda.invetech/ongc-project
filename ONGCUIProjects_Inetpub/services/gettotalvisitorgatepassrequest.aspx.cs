﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class gettotalvisitorgatepassrequest : System.Web.UI.Page
    {
        string sCPF;
        protected void Page_Load(object sender, EventArgs e)
        {
            string cpf_no = "";
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();
                sCPF = cpf_no;
            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT COUNT([CY_GROUP]),[CY_GROUP] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE O_ACP1_CPF_NMBR = '" + cpf_no + "' AND O_PASS_TYPE_FLG = 'V' AND O_REQ_COMPLTN_FLG = 'A' AND CY_GROUP <>'' GROUP BY CY_GROUP,O_SYS_DT_TM ORDER BY O_SYS_DT_TM DESC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("FALSE~SUCCESS~No Request Found~NA");
                return;
            }
            else
            {
                string print = "";

                string group_name = "";
                string total_visitor = "";
                int count = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    count = count + 1;
                    total_visitor = ds.Tables[0].Rows[i][0].ToString();
                    group_name = ds.Tables[0].Rows[i][1].ToString();
                    print = print + getgroupdetail(group_name, total_visitor, cpf_no, count.ToString());


                }
                Response.Write("TRUE~SUCCESS~Visitor detail Found Successfully !~" + count + "~" + print);


            }
        }
        private string getgroupdetail(string group_name, string total_visitor, string cpf_no, string serial)
        {
            string hello = "";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "Select [O_INDX_NMBR],[O_APLCNT_CPF_NMBR] ,[O_APLCNT_NM],[O_APLCNT_DESG],[O_ENTRY_DT],[O_VISTR_NM], [O_VISTR_AGE], [O_VISTR_ORGNZTN],[O_PRPS_OF_VISIT], O_ENTRY_DT ,[CY_O_VISTR_ENTRY_TIME],[O_RMRKS],[CY_WITH_TOOL] ,[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG] ,[O_VISTR_GNDR]from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_ACP1_CPF_NMBR =  '" + cpf_no + "' and O_PASS_TYPE_FLG = 'V' and O_REQ_COMPLTN_FLG = 'A' and CY_GROUP <>'' and CY_GROUP='" + group_name + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                string sGroupData = group_name + "`" + serial + "`" + total_visitor;
                string my_div_id = serial + "hhhdiv";
                string date_time_str = System.Convert.ToDateTime(ds.Tables[0].Rows[0][4].ToString()).ToLongDateString().ToString() + " " + ds.Tables[0].Rows[0][10].ToString();
                hello = " <div id=" + my_div_id + "  g_n=" + group_name + " class='div-fullwidth marginbottom gatepass-approvalbox'";
                hello = hello + " <div class=\"div-fullwidth\" style=\"padding: 10px; width: 98%;\">";
                hello = hello + " <h1 class=\"content-gatepass-approval count\">" + serial + ".</h1>";
                hello = hello + "<div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div>";
                hello = hello + " <h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px; margin-left: 5px;\"><span>Applicant</span> : " + "<a href='javascript:void(0);' title='Desig : " + ds.Tables[0].Rows[0][3].ToString() + "'>" + ds.Tables[0].Rows[0][1].ToString() + "( " + ds.Tables[0].Rows[0][2].ToString() + ")</a> " + "</h1></div>";
                hello = hello + " <div class=\"div-fullwidth\" style=\"width: auto;\"> <div class=\"divider\"></div> <h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\">  <span>Whom to Meet</span> : <a title='Desig : " + ds.Tables[0].Rows[0][14].ToString() + "' href='javascript:void(0);'>" + ds.Tables[0].Rows[0][13].ToString() + "</a></h1>   </div>";
                hello = hello + "<div class=\"div-fullwidth\" style=\"width: auto;\"> <div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Visiting Date</span> : " + date_time_str + "</h1></div>";
                hello = hello + "<div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div> <h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span>Total Visitor</span> : " + total_visitor + "</h1></div>";
                //hello = hello + "<div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Remark</span> :  " + ds.Tables[0].Rows[0][11].ToString() + "</h1> </div>";
                hello = hello + "<table id=\"tfhover\" class=\"tftable\" border=\"1\" style=\"margin-left: 10px; margin-top: 10px; margin-right: 10px; width: 98%;\">";
                hello = hello + " <tr><th style=\"width: 40px;\">Sr. No</th><th>Visitor's Name</th> <th style=\"width: 40px;\">Age</th> <th style=\"width: 50px;\">Gender</th>  <th style=\"width: 140px;\">Organization</th><th style=\"width: 150px;\">Purpose Of Visit</th><th style=\"width: 150px;\">Tools</th> <th style=\"width: 70px;\"><label><input class='select_all' type=\"checkbox\" serial=" + serial + " style=\"margin-right: 5px; border-color: #929292;\" />Select all</lable></th></tr>";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string my_class = serial + "single_selected";
                    hello = hello + "<tr id='my_com_row'><td>" + (i + 1).ToString() + "</td><td>" + ds.Tables[0].Rows[i][5].ToString() + "</td><td>" + ds.Tables[0].Rows[i][6].ToString() + "</td><td>" + ds.Tables[0].Rows[i][15].ToString() + "</td><td>" + ds.Tables[0].Rows[i][7].ToString() + "</td></td><td>" + ds.Tables[0].Rows[i][11].ToString() + "</td><td>" + ds.Tables[0].Rows[i][12].ToString() + "</td><td ><label><input class=" + my_class + " index_no=" + ds.Tables[0].Rows[i][0].ToString() + " style='margin: 0 auto; border-color: #929292;' type='checkbox' value=''/></label></td></tr>";

                }
                hello = hello + "  </table><div class=\"div-fullwidth\" style=\"margin-top: 10px;\">";
                hello = hello + "<input class=\"g-button g-button-red\"  id='reject_request' serial=" + serial + " group_no=" + group_name + " value=\"REJECT\" type=\"button\"  style=\" position: relative; float:right; text-decoration: none; margin-right: 10px;\" />";
                hello = hello + "  <input class=\"g-button g-button-share\"  id='approve_request' serial=" + serial + "  group_no=" + group_name + "  value=\"Approve\" type=\"button\" style=\"position: relative; float:right; text-decoration: none; margin-right: 5px;\" />";
                hello = hello + "  <a class=\"g-button g-button-share cmdViewApprove\" href='VisitorPassViewAndApprove.aspx?GroupData=" + sGroupData + "' type=\"button\" style=\"position: relative; float:right; text-decoration: none; margin-right: 5px;\">View & Approve</a></div></div></div></div>";

                return hello;
            }
        }



    }
}