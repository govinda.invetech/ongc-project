﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace ONGCUIProjects.services
{
    public partial class getpassrequestdetailforedit : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            string cpf_no = "";
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();

            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "Select COUNT([CY_GROUP]),[CY_GROUP] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_APLCNT_NM = '" + cpf_no + "' and O_PASS_TYPE_FLG = 'V' and O_REQ_COMPLTN_FLG = 'A' and CY_GROUP <>'' GROUP BY CY_GROUP";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
       
            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("FALSE~SUCCESS~No Request Found~NA");
                return;
            }
            else
            {
                string total_visitor="";
                string  group_name="";
                string print="<option value=''>Select Old type pass request</option>";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                   
                    total_visitor = ds.Tables[0].Rows[i][0].ToString();
                    group_name = ds.Tables[0].Rows[i][1].ToString();
                    print = print + getgroupdetail(group_name, total_visitor, cpf_no);

                }
                Response.Write("TRUE~SUCCESS~Visitor detail Found Successfully !~" + print );
              

            }
        }
        private string getgroupdetail(string group_name, string total_visitor, string cpf_no)
        {
         
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "Select [O_ENTRY_DT],[O_MTNG_OFF_NM] ,[O_VISTR_NM]from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_APLCNT_NM =  '" + cpf_no + "' and O_PASS_TYPE_FLG = 'V' and O_REQ_COMPLTN_FLG = 'A' and CY_GROUP <>'' and CY_GROUP='" + group_name + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "<option value="+group_name+">Total Visitor["+total_visitor+"] , Date["+ds.Tables[0].Rows[0][0].ToString()+"] ,  WTM["+ds.Tables[0].Rows[0][1].ToString()+"] , F-V-NAME["+ds.Tables[0].Rows[0][2].ToString()+"]</option>";
            }
        }
    
    }
}