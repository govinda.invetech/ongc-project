﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class updatepassword : System.Web.UI.Page
    {
        string cpf_no = "";
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }

            cpf_no = Session["Login_Name"].ToString();
            string old_pass = Request.QueryString["old_pass"];
            string newpass1 = Request.QueryString["newpass1"];
            string newpass2 = Request.QueryString["newpass2"];
            if (newpass1 != newpass2)
            {
                Response.Write("FALSE~SUCCESS~New Password and Retype Password Not Matched");
            }
            else {
                if (checkcurrentpassword(old_pass) == "FALSE")
                {
                    Response.Write("FALSE~SUCCESS~Invalid Current Password. ");
                }
                else {
                    string str = "UPDATE [E_USER_MSTR] SET [E_USER_PSWRD] = '"+newpass1+"' WHERE  [E_USER_CODE] = '"+cpf_no+"' ";
                    try
                    {
                        if (My.ExecuteSQLQuery(str))
                        {
                            Response.Write("TRUE~SUCCESS~Password Changed Successfully. Please use new password to login . ");
                        }
                        else 
                        {
                            Response.Write("FALSE~SUCCESS~Password Not Changed Successflly. ");
                        }
                    }
                    catch (Exception a) {

                        Response.Write("FALSE~SUCCESS~"+a.Message+"ERR403PASSCHANGE");
                    }
                }
            
            }
        }
        private string checkcurrentpassword(string current_password) {
            string my_str = "SELECT [E_USER_CODE]  FROM [E_USER_MSTR] where [E_USER_CODE]='"+cpf_no+"' and [E_USER_PSWRD]='"+current_password+"'";
       DataSet ds= My.ExecuteSELECTQuery(my_str);
       if (ds.Tables[0].Rows.Count == 0)
       {
           return "FALSE";
       }
       else {

           return "TRUE";
       }
           
        }
    }
    
}