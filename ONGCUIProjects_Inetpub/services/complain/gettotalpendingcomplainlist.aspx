﻿<%@ Page language="c#" Codebehind="gettotalpendingcomplainlist.aspx.cs" AutoEventWireup="True" Inherits="WebApplication1.gettotalpendingcomplainlist" %>
<!DOCTYPE html>
<html>
<head>
<title></title>   
     
     
		
</head>

<body>
<form id="Form1" method="post" runat="server" style="margin-right: 10px;">  
     <div style="margin-right:10px; float:right;">       
            <asp:Label ID="eto" runat="server" Text="From Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdatefrom" runat="server" Width="80px" Height="26px"></asp:TextBox>&nbsp;&nbsp;
             <asp:Label ID="Label1" runat="server" Text="To Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdateto" runat="server"  Width="80px" Height="26px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
           <asp:Button ID="btn2" runat="server" Text="Generate" Height="26px" OnClientClick="SetTarget();" OnClick="btn2_Click" />
             
                    
    </div>
 

    <div id="request_detail_div" runat="server" style=" overflow:scroll; padding:10px; margin-left:30px; height:200px; width:850px;">
    <asp:GridView ID="grid_detail_complaint" runat="server" AutoGenerateColumns="False" Width="830px" OnRowDataBound="grid_detail_complaint_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="ID" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lbl_id" runat="server" Text='<%#Eval("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Applicant_Name">
                <ItemTemplate> 
                    <asp:Label ID="lbl_applicant" runat="server" Text='<%#Eval("APP_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Designation">
                <ItemTemplate>
                    <asp:Label ID="lbl_desig" runat="server" Text='<%#Eval("APP_DESIG") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone_Extn">
                <ItemTemplate>
                    <asp:Label ID="lbl_extn" runat="server" Text='<%#Eval("APP_PHONE_EX_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location">
                <ItemTemplate>
                    <asp:Label ID="lbl_loc" runat="server" Text='<%#Eval("APP_LOCATION") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Problem">
                <ItemTemplate>
                    <asp:Label ID="lbl_prblm" runat="server" Text='<%#Eval("PROBLEM_DISCRIPTION") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Room_No">
                <ItemTemplate>
                    <asp:Label ID="lbl_rm" runat="server" Text='<%#Eval("ROOM_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Complain_Type">
                <ItemTemplate>
                    <asp:Label ID="lbl_compln_type" runat="server" Text='<%#Eval("TYPES") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Complain_Date">
                <ItemTemplate>
                    <asp:Label ID="lbl_compln_date" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd-MM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Label ID="lbl_act" runat="server" Visible="false" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status"></asp:Label>
                   <asp:DropDownList ID="ddl_action" runat="server" EnableViewState="true" ViewStateMode="Enabled" Width="100px" AutoPostBack="true"  OnSelectedIndexChanged="ddl_action_SelectedIndexChanged"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
<%--             <asp:TemplateField HeaderText="View/Edit">
                <ItemTemplate>
                    <a class='view_cmpln_dtl_fancy' href="Complaint_Registration.aspx?my_hint=APPROVE`<%#Eval("ID")%>" >View Detail</a>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>

    </asp:GridView>
        </div>
    
</form>
</body>
</html>