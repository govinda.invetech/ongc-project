﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services
{
    public partial class getcomplainofficername : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Department; 
            if (Request.QueryString["Department"] == null)
            {
                Response.Write("FALSE~ERR~Invalid Input");
                return;
            }
            Department = Request.QueryString["Department"];
            string page_id = "";
            if (Department == "TELEPHONE")
            {
                page_id = "62";          // page_id commented for temporary
            }else if (Department == "ELECTRICAL(AC)")
            {
                page_id = "63";
            }
            else if (Department == "ELECTRICAL(MAINTENANCE)")
            {
                page_id = "77";
            }
            else if (Department == "CIVIL")
            {
                page_id = "64";
            }
            else if (Department == "HOUSEKEEPING")
            {
                page_id = "65";
            }
            else if (Department == "MECHANICAL")
            {
               page_id = "66";
            }
            else if (Department == "OTHER")
            {
                page_id = "67";
            }
            else if(Department== "WalkieTalkie")
            {
                page_id = "99";
            }
            else if (Department == "PAPaging")
            {
                page_id = "98";
            }
            else
            {
                Response.Write("FALSE~ERR~Invalid Input");
                return;
            }

            string output = "<h4 class='content' style='border-bottom: 1px solid #B4B4B4; width: 100%;'>" + Department + " COMPLAINT MANAGER : </h4>";           
            string str = "SELECT [CY_MENU_EMP_RELATION].[cpf_number],[O_EMP_MSTR].[O_EMP_NM],[O_EMP_MSTR].[O_EMP_DESGNTN],[O_EMP_MSTR].[CY_EMP_EX_NO] FROM [CY_MENU_EMP_RELATION],[O_EMP_MSTR] where  [CY_MENU_EMP_RELATION].[child_id]='" + page_id + "' and [O_EMP_MSTR].O_CPF_NMBR=[CY_MENU_EMP_RELATION].cpf_number ";
            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            DataSet ds=My.ExecuteSELECTQuery(str);
            output = output + "<div class='div-fullwidth' style='margin-left: 60px;margin-top: 10px;margin-bottom: 30px;'>";
            if (ds.Tables[0].Rows.Count == 0)
            {
                output = output + " <a class='content black' style=\"margin-right:20px;margin-bottom: 5px; text-decoration:none; background-color: #f8f8f8; padding: 10px 10px 7px 2px;border: 1px dashed #A09A9A;\" href=\"javascript:void(0)\"><img src='Images/admin_icon.png' style='margin: -4px 5px 0 0; position: relative; float: left;' />No Complain officer Found</a>";
                
                output = output + "</div>";
                Response.Write("TRUE~SUCCESS~No Complaint Officer Found~" + output);
            }
            else {
                
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {

                    output = output + " <a  title=\"Designation : " + ds.Tables[0].Rows[i][2].ToString() + " , Phone Extension No : " + ds.Tables[0].Rows[i][3].ToString() + "\" class='content black' style=\"margin-right:20px; margin-bottom: 5px; text-decoration:none; background-color: #f8f8f8; padding: 10px 10px 7px 2px;border: 1px dashed #A09A9A;\" href=\"javascript:void(0)\"><img src='Images/admin_icon.png' style='margin: -4px 5px 0 0; position: relative; float: left;' />" + ds.Tables[0].Rows[i][1].ToString() + "</a>";
                }
                output = output + "</div>";
                Response.Write("TRUE~SUCCESS~Complaint Officer Found~" + output);
            }
        }
    }
}