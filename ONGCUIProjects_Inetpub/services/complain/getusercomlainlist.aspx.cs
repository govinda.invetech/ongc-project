﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services.complain
{
    public partial class getusercomlainlist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string cpf_no = "";
            string str = "";
            string type = "";
            if (Request.QueryString["type"] != null)
            {
                type = Request.QueryString["type"].ToString();
            }

            string strip_type = Request.QueryString["strip_type"].ToString();
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session not found");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();
            }


            str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where [ENTRY_BY]='" + cpf_no + "' and [VIEWED_RESPONSE]='" + strip_type + "'";


            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            DataSet ds = My.ExecuteSELECTQuery(str);
            if (ds.Tables[0].Rows.Count == 0)
            {

                Response.Write("FALSE~ERR~No " + strip_type + " Complaint Found");

            }
            else
            {
                string output = "";
                int count = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string index_no = ds.Tables[0].Rows[i][0].ToString();
                    string app_cpf_no = ds.Tables[0].Rows[i][1].ToString();
                    string app_name = ds.Tables[0].Rows[i][2].ToString();
                    string app_desig = ds.Tables[0].Rows[i][3].ToString();
                    string app_phonr_ex_no = ds.Tables[0].Rows[i][4].ToString();
                    string app_mobile_no = ds.Tables[0].Rows[i][5].ToString();
                    string app_location = ds.Tables[0].Rows[i][6].ToString();
                    string type_id = ds.Tables[0].Rows[i][8].ToString();
                    string problem = ds.Tables[0].Rows[i][9].ToString();
                    string complain_date = ds.Tables[0].Rows[i][14].ToString();
                    DateTime dt = System.Convert.ToDateTime(complain_date);

                    string room_no = ds.Tables[0].Rows[i][15].ToString();
                    string complain_type = gettypedetail(type_id);
                    count = count + 1;
                    output = output + "  <div class=\"div-fullwidth marginbottom gatepass-approvalbox\" ><div class=\"div-fullwidth\" style=\"padding: 10px; width: 98%;display:block\">";
                    output = output + " <h1 class=\"content-gatepass-approval count\">" + (i + 1).ToString() + ".</h1> <div class=\"div-fullwidth\" style=\"width: auto;\">  <div class=\"divider\"></div>";
                    output = output + "<h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px; margin-left: 5px;\"><span>Applicant</span> : " + app_name + " (" + app_cpf_no + ")</h1>   </div>";
                    output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\"> <div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Designation</span> : " + app_desig + "</h1>  </div>";
                    output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\">   <div class=\"divider\"></div> <h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\">  <span>Phone Extension No.</span> : " + app_phonr_ex_no + "</h1></div>";
                    output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\"> <div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Location</span> : " + app_location + "</h1> </div>";
                    output = output + "  <div class=\"div-fullwidth\" style=\"width: auto;\">  <div class=\"divider\"></div> <h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Problem </span> : " + problem + "</h1>   </div> ";
                    output = output + " <div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span>Room No. </span> : " + room_no + "</h1></div>";
                    output = output + " <div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span>Complain Type </span> : " + complain_type + "</h1></div>";
                    output = output + " <div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span>Complain Date </span> : " + dt.ToLongDateString().ToString() + "</h1></div>";
                    // a link
                    if (strip_type == "PENDING")
                    {
                        output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span><a class='view_cmpln_dtl_fancy' my_i_no='" + index_no + "' href=\"Complaint_Registration.aspx?my_hint=UPDATE`" + index_no + "\" >View(Edit)</a></span> </h1> </div></div></div> ";
                    }
                    else
                    {

                        output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span><a class='view_cmpln_dtl_fancy' my_i_no='" + index_no + "' href=\"Complaint_Registration.aspx?my_hint=ONLYVIEW`" + index_no + "\" >View Response</a></span> </h1> </div></div></div> ";
                    }
                }
                Response.Write("TRUE~SUCCESS~Total " + count.ToString() + " " + strip_type + " Complaint Found~" + output);

            }

        }
        private string gettypedetail(string type_id)
        {

            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [TYPE] FROM [CY_COMPLAIN_TYPE] where ID='" + type_id + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }

        }
    }
}