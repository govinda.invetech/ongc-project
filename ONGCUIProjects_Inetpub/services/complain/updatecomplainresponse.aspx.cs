﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;



namespace ONGCUIProjects.services
{
    public partial class updatecomplainresponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string index_no = "";
            string update_what = "";
            string cpf_no = "";
            string view_response = "";
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }

            if (Request.QueryString["index_no"] == null)
            {
                Response.Write("FALSE~ERR~Invalid Input");

            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();
                index_no = Request.QueryString["index_no"].ToString();
                update_what = Request.QueryString["update_what"].ToString();
                view_response = Request.QueryString["view_response"].ToString();
                string str = "UPDATE [CY_COMPLAIN_REGISTER_DETAIL] SET [VIEWED_BY] = '" + cpf_no + "' ,[VIEWED_RESPONSE] = '" + view_response + "',[VIEW_TIMESTAMP] = '" + System.DateTime.Now.ToString() + "',[view_comment] = '" + update_what + "' WHERE ID='" + index_no + "'";
                ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
                try
                {
                    if (My.ExecuteSQLQuery(str) == true)
                    {

                        Response.Write("TRUE~SUCCESS~Complaint Status Saved Successfully !");
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~Complaint Status Not Saved Successfully !");

                    }
                }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //

            }

        }
       

        [WebMethod]
        public static string CloseComplain(string index_no)
        {
            string Index_No = index_no;
            string str = "UPDATE [CY_COMPLAIN_REGISTER_DETAIL] SET [ENTRY_BY] ='admin_close' WHERE ID='" + index_no + "'";
            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            try
            {
                if (My.ExecuteSQLQuery(str) == true)
                {
                    return "{'type':'success','msg':'Complaint Close Successfully !'}";
                }
                else
                {
                    return "{'type':'success','msg':'Please try again !'}";
                }
            }
            catch (Exception ex)
            {
                return "{'type':'error','msg':'" + ex.Message + "!'}";
            }
        }
    }
}