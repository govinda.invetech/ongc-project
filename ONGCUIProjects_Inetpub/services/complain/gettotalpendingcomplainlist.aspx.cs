﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class gettotalpendingcomplainlist : System.Web.UI.Page
    {
        string department = "";
        string strip_type = "";
        MyApplication1 my = new MyApplication1();
        private bool button1WasClicked = false;
        protected void Page_Load(object sender, EventArgs e)
        {               
            if (button1WasClicked == false)
            {

                string cpf_no = "";
                string str;

                department = Request.QueryString["department"].ToString();
                strip_type = Request.QueryString["strip_type"].ToString();

                if (Session["Login_Name"] == null)
                {
                    Response.Write("FALSE~ERR~Session not found");
                    return;
                }
                else
                {
                    cpf_no = Session["Login_Name"].ToString();
                }
                if (department == "")
                {
                    Response.Write("FALSE~ERR~Invalid Input");
                    return;
                }
                if (strip_type == "ALL")
                {
                    str = "SELECT [CY_COMPLAIN_REGISTER_DETAIL].[ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,TABLEA.TYPES,";
                    str += "[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID],";
                    str += "[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL]";
                    str += "left join (select ID, ISNULL(TYPE,1)TYPES from CY_COMPLAIN_TYPE)TABLEA on TABLEA.ID=[CY_COMPLAIN_REGISTER_DETAIL].COMPLAIN_TYPE_ID ";
                    str += " where [COMPLAIN_DEPARTMENT]='" + department + "'";
                    ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
                    DataSet ds = My.ExecuteSELECTQuery(str);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("FALSE~ERR~No " + strip_type + " Complaint Found");
                    }
                    else
                    {
                        int count = 0;
                        count = ds.Tables[0].Rows.Count;
                        //grid_detail_complaint.DataSource = ds;
                        //grid_detail_complaint.DataBind();
                        Response.Write("TRUE~SUCCESS~Total " + count.ToString() + " " + strip_type + " Complaint Found~");//+ output
                    }
                }
                else
                {
                    str = "SELECT [CY_COMPLAIN_REGISTER_DETAIL].[ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,TABLEA.TYPES,";
                    str += "[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID],";
                    str += "[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL]";
                    str += "left join (select ID, ISNULL(TYPE,1)TYPES from CY_COMPLAIN_TYPE)TABLEA on TABLEA.ID=[CY_COMPLAIN_REGISTER_DETAIL].COMPLAIN_TYPE_ID ";
                    str += " where [COMPLAIN_DEPARTMENT]='" + department + "'  and  [VIEWED_RESPONSE]='" + strip_type + "'";
                    ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
                    DataSet ds = My.ExecuteSELECTQuery(str);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("FALSE~ERR~No " + strip_type + " Complaint Found");
                    }
                    else
                    {
                        int count = 0;
                        count = ds.Tables[0].Rows.Count;
                        //grid_detail_complaint.DataSource = ds;
                        //grid_detail_complaint.DataBind();

                        //string type_id = "";
                        //string output = "";

                        //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        //{
                        //    string index_no = ds.Tables[0].Rows[i][0].ToString();
                        //    string app_cpf_no = ds.Tables[0].Rows[i][1].ToString();
                        //    string app_name = ds.Tables[0].Rows[i][2].ToString();
                        //    string app_desig = ds.Tables[0].Rows[i][3].ToString();
                        //    string app_phonr_ex_no = ds.Tables[0].Rows[i][4].ToString();
                        //    string app_mobile_no = ds.Tables[0].Rows[i][5].ToString();
                        //    string app_location = ds.Tables[0].Rows[i][6].ToString();
                        //    if (ds.Tables[0].Rows[i][8].ToString() == "null")
                        //    {
                        //        type_id = "1";
                        //    }
                        //    else
                        //     type_id =ds.Tables[0].Rows[i][8].ToString();
                        //    string problem = ds.Tables[0].Rows[i][9].ToString();
                        //    string complain_date = ds.Tables[0].Rows[i][14].ToString();
                        //    DateTime dt = System.Convert.ToDateTime(complain_date);
                        //    string room_no = ds.Tables[0].Rows[i][15].ToString();
                        //    string complain_type = gettypedetail(type_id);
                        //    count = count + 1;
                        //    output = output + "  <div class=\"div-fullwidth marginbottom gatepass-approvalbox\" ><div class=\"div-fullwidth\" style=\"padding: 10px; width: 98%;display:block\">";
                        //    output = output + " <h1 class=\"content-gatepass-approval count\">" + (i + 1).ToString() + ".</h1> <div class=\"div-fullwidth\" style=\"width: auto;\">  <div class=\"divider\"></div>";
                        //    output = output + "<h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px; margin-left: 5px;\"><span>Applicant</span> : " + app_name + " (" + app_cpf_no + ")</h1>   </div>";
                        //    output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\"> <div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Designation</span> : " + app_desig + "</h1>  </div>";
                        //    output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\">   <div class=\"divider\"></div> <h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\">  <span>Phone Extension No.</span> : " + app_phonr_ex_no + "</h1></div>";
                        //    output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\"> <div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Location</span> : " + app_location + "</h1> </div>";
                        //    output = output + "  <div class=\"div-fullwidth\" style=\"width: auto;\">  <div class=\"divider\"></div> <h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"> <span>Problem </span> : " + problem + "</h1>   </div> ";
                        //    output = output + " <div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span>Room No. </span> : " + room_no + "</h1></div>";
                        //    output = output + " <div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span>Complain Type </span> : " + complain_type + "</h1></div>";
                        //    output = output + " <div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span>Complain Date </span> : " + dt.ToLongDateString().ToString() + "</h1></div>";
                        //    output = output + "<div class=\"div-fullwidth\" style=\"width: auto;\"><div class=\"divider\"></div><h1 class=\"content-gatepass-approval\" style=\"margin-top: 10px;\"><span><a class='view_cmpln_dtl_fancy' href=\"Complaint_Registration.aspx?my_hint=APPROVE`" + index_no + "\" >View Detail</a></span> </h1> </div></div></div> ";
                        //}
                        Response.Write("TRUE~SUCCESS~Total " + count.ToString() + " " + strip_type + " Complaint Found~");//+ output
                    }
                }
            }
        }
        private string gettypedetail(string type_id)
        {
            try
            {
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [TYPE] FROM [CY_COMPLAIN_TYPE] where ID='" + type_id + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
        }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        protected void btn2_Click(object sender, EventArgs e)
        {
            
            
        }

        protected void ddl_action_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl_status = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl_status.Parent.Parent;
            int idx = row.RowIndex;


            //Retrieve bookid and studentid from Gridview and status(dropdownlist)
            String lblbookid = ((Label)row.Cells[0].FindControl("lbl_act")).Text;
            DropDownList ddl = (DropDownList)row.Cells[0].FindControl("ddl_action");


            //Update Status            
            string query = "Update CY_COMPLAIN_REGISTER_DETAIL set VIEWED_RESPONSE='" + ddl.Text.ToString() + "' where ID='" + lblbookid + "'";
            ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
            bool res = My1.updateQuery(query);
            if (res)
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Status has been updated successfully.');", true);
        }
        public static DataTable BindStatuss()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("VIEWED_RESPONSE");
            dt.Rows.Add("NEW");
            dt.Rows.Add("ASSIGN");
            dt.Rows.Add("WORK IN PROGRESS");
            dt.Rows.Add("RESOLVED");
            return dt;

        }
        protected void grid_detail_complaint_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                //{
                DropDownList ddl_status_edits = (DropDownList)e.Row.FindControl("ddl_action");
                ddl_status_edits.DataSource = BindStatuss();
                ddl_status_edits.DataValueField = "VIEWED_RESPONSE";
                ddl_status_edits.DataTextField = "VIEWED_RESPONSE";
                ddl_status_edits.DataBind();
                DataRowView dr1 = e.Row.DataItem as DataRowView;
                ddl_status_edits.SelectedValue = dr1["VIEWED_RESPONSE"].ToString();

                //}
            }
        }     


    }
}