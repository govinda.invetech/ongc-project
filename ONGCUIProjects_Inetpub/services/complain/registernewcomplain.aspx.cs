﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace ONGCUIProjects.services
{
    public partial class registernewcomplain : System.Web.UI.Page
    {
        string app_design = "";
        string complain_type = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
             string module_hint = Request.QueryString["module_hint"];
             string index_no = Request.QueryString["index_no"];
             string complain_department = Request.QueryString["complain_department"];
             if(complain_department=="CIVIL")
             {
                 complain_type = "9";
             }
             else if (complain_department == "TELEPHONE")
             {
                 complain_type = "8";
             }
             else
             {
                 complain_type = Request.QueryString["complain_type"];
             }             
             string problem_discription = Request.QueryString["problem_discription"];
             string app_mob_no = Request.QueryString["app_mob_no"];
             string room_no = Request.QueryString["room_no"];
             string app_name = Request.QueryString["app_name"];
             string app_cpf_no = Request.QueryString["app_cpf_no"];
             if (Session["desigvalue"].ToString()!=null)
             app_design = Session["desigvalue"].ToString();//Request.QueryString["app_design"];
             else
             app_design = Request.QueryString["app_design"];
             string app_loc = Request.QueryString["app_loc"];
             string app_ph_ex_no = Request.QueryString["app_ph_ex_no"];
             string cpf_no = Session["Login_Name"].ToString();
             string str="";
             if (module_hint == "NEW") {
                 str = "INSERT INTO [CY_COMPLAIN_REGISTER_DETAIL]  ([APP_CPF_NO],[APP_NAME],[APP_DESIG] ,[APP_PHONE_EX_NO],[APP_MOBILE_NO],[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID],[PROBLEM_DISCRIPTION],[ENTRY_BY],[TIMESTAMP],[ROOM_NO]) ";
                 str = str + " VALUES ('" + app_cpf_no + "','" + app_name + "','" + app_design+ "','" + app_ph_ex_no + "','" + app_mob_no + "','" + app_loc + "','" + complain_department + "','" + complain_type + "','" + problem_discription + "','" + cpf_no + "',GETDATE(),'" + room_no + "')";

             }
             else if (module_hint == "UPDATE")
             {
                 str = "UPDATE [CY_COMPLAIN_REGISTER_DETAIL] SET [APP_CPF_NO] = '"+app_cpf_no+"',[APP_NAME] ='"+app_name+"',[APP_DESIG] ='"+app_design+"',[APP_PHONE_EX_NO] = '"+app_ph_ex_no+"',[APP_MOBILE_NO] = '"+app_mob_no+"',[APP_LOCATION] = '"+app_loc+"',[COMPLAIN_DEPARTMENT] = '"+complain_department+"',[COMPLAIN_TYPE_ID] = '"+complain_type+"',[PROBLEM_DISCRIPTION] ='"+problem_discription+"',[ENTRY_BY] = '"+cpf_no+"',[TIMESTAMP] =GETDATE(),[ROOM_NO] = '"+room_no+"' where [ID]='"+index_no+"'";

             }else{
               Response.Write("FALSE~ERR~Invalid Input");
               return;
             }         

          
            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            if (My.validatecpf_no(app_cpf_no) == "FALSE") {
               Response.Write("FALSE~ERR~Please Enter Correct CPF No.");
               return;
            }
           try
           {
               if (My.ExecuteSQLQuery(str) == true)
               {
                   if (module_hint == "NEW") {
                   Response.Write("TRUE~SUCCESS~Complaint Registered Successfully !");
                   }else{
                   Response.Write("TRUE~SUCCESS~Complaint Updated Successfully !");
                   }
               }
               else {
                   Response.Write("FALSE~ERR~Complaint Not Registered Successfully 404 Registernewcomplian.aspx!");
               }
           }
           catch (Exception a)
           {
               Response.Write("FALSE~ERR~" + a.Message);
           } 

        }
    }
}