﻿using System;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services
{
    public partial class getcompliandetailofselectedindexordepartment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string index_no = "";
                if (Request.QueryString["index_no"] != null)
                {
                    string type_id = "";
                    string cpf_no = "";
                    string str = "";
                    if (Session["Login_Name"] == null)
                    {

                        Response.Write("FALSE~ERR~Session not found");
                        return;
                    }

                    cpf_no = Session["Login_Name"].ToString();
                    index_no = Request.QueryString["index_no"].ToString();
                    //str = "SELECT [CY_COMPLAIN_REGISTER_DETAIL].[ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO],view_comment FROM [CY_COMPLAIN_REGISTER_DETAIL]  where [ID]='" + index_no + "'  ";

                    str = "SELECT [CY_COMPLAIN_REGISTER_DETAIL].[ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,TABLEA.TYPES,";
                    str += "[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID],";
                    str += "[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL]";
                    str += "left join (select ID, ISNULL(TYPE,1)TYPES from CY_COMPLAIN_TYPE)TABLEA on TABLEA.ID=[CY_COMPLAIN_REGISTER_DETAIL].COMPLAIN_TYPE_ID ";
                    str += " where [CY_COMPLAIN_REGISTER_DETAIL].[ID]='" + index_no + "'";
                    ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
                    DataSet ds = My.ExecuteSELECTQuery(str);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("FALSE~ERR~Complain Detail Not Found");
                    }
                    else
                    {
                        int i = 0;
                        string app_cpf_no = ds.Tables[0].Rows[i][1].ToString();
                        string app_name = ds.Tables[0].Rows[i][2].ToString();
                        string app_desig = ds.Tables[0].Rows[i][3].ToString();
                        string complain_type = ds.Tables[0].Rows[i][4].ToString();
                        string app_phonr_ex_no = ds.Tables[0].Rows[i][5].ToString();
                        string app_mobile_no = ds.Tables[0].Rows[i][6].ToString();
                        string app_location = ds.Tables[0].Rows[i][7].ToString();
                        string complaint_department = ds.Tables[0].Rows[0][8].ToString();
                        //if (ds.Tables[0].Rows[i]["COMPLAIN_TYPE_ID"].ToString()== "null" || ds.Tables[0].Rows[i]["COMPLAIN_TYPE_ID"].ToString() == " ")
                        //{
                        //    type_id = "1";
                        //}
                        //else
                        type_id = ds.Tables[0].Rows[i][9].ToString();
                        string problem = ds.Tables[0].Rows[i][10].ToString();
                        string room_no = ds.Tables[0].Rows[i][16].ToString();
                        string view_response = ds.Tables[0].Rows[0][12].ToString();
                        //string view_comment = ds.Tables[0].Rows[0][16].ToString();
                        
                        //string complain_type = gettypedetail(type_id);
                        string output = complaint_department + "~" + app_cpf_no + "~" + app_name + "~" + app_desig + "~" + app_phonr_ex_no + "~" + app_mobile_no + "~" + app_location + "~" + complain_type + "~" + problem + "~" + room_no + "~" + view_response + "~"; 
                        output = output.Replace("~~", "~").Replace("~~~", "~").Replace("~~~~", "~");
                        Response.Write("TRUE~SUCCESS~Detail Found~" + output);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string gettypedetail(string type_id)
        {
            try
            {
                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                SqlConnection conn = new SqlConnection(strConn);
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                string sStr = "SELECT [TYPE] FROM [CY_COMPLAIN_TYPE] where ID='" + type_id + "'";
                da = new SqlDataAdapter(sStr, conn);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    return "FALSE";
                }
                else
                {
                    return ds.Tables[0].Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}