﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class getdepartmentlistoremployeelist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = Request.QueryString["type"];
            string dep_name = Request.QueryString["dep_name"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = ""; 
             string output="";
            if (type == "DEPARTMENT") {
                output = "<option value=''>Select Department Name</option><option value='ALL'>ALL</option>";
                sStr = "SELECT DISTINCT([O_EMP_DEPT_NM])FROM [O_EMP_MSTR] where [O_EMP_DEPT_NM]<>'' ORDER BY [O_EMP_DEPT_NM] ASC";
            }
            else if (type == "EMPLOYEE")
            {
                output = "<option value=''>Select Emploee  Name</option>";
                if(dep_name=="ALL"){
                    sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM]  FROM [O_EMP_MSTR] ";
                }else{
                    sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM]  FROM [O_EMP_MSTR] where O_EMP_DEPT_NM='" + dep_name + "'";
                }
                
            }
            else if (type == "ALL_CPF")
            {
                output = "<option value=''>Select CPF NO.</option>";
                sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM]  FROM [O_EMP_MSTR] order by [O_CPF_NMBR] ASC";

            }
            else {
                return;
            }
           
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            
            if (ds.Tables[0].Rows.Count == 0)
            {
                if (type == "EMPLOYEE")
                {
                    Response.Write("TRUE~SUCCESS~Employee LIST Not Found ~"+output+"~");
                    return;
                }
                    
                else {
                    string theory = "Total " + gettotalusercount() + " Users in Database";
                    Response.Write("TRUE~SUCCESS~Loaded Successfully ~" + theory + "~" + output + "~NA");
                }
                
            }
            else
            {
                int count = 0;
                if (type == "DEPARTMENT") {
                 for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    count = count + 1;
                   
                    output = output + "<option  value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][0].ToString()  + "</option>";
                }
                 string theory = "Total  " + gettotalusercount() + " Users in Database";
                 Response.Write("TRUE~SUCCESS~Loaded Successfully ~" + theory + "~" + output + "~NA");
                }else  if (type == "ALL_CPF") {
                 for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    count = count + 1;
                 
                    output = output + "<option  value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][0].ToString()  + "</option>";
                }
                 string theory = "Total  " + gettotalusercount() + " Users in Database";
                 Response.Write("TRUE~SUCCESS~Loaded Successfully ~" + theory + "~" + output + "~NA");
                }
                else{
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        count = count + 1;
                       
                        output = output + "<option  value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + " ( " + ds.Tables[0].Rows[i][0].ToString() + " ) " + "</option>";
                    }
                    Response.Write("TRUE~SUCCESS~Loaded Successfully ~" +  output + "~NA");
                }
               
              


            }
           
            
        }
        private string gettotalusercount()
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT COUNT([O_INDX_NMBR]) FROM [O_EMP_MSTR]";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                string count_user = ds.Tables[0].Rows[0][0].ToString();
                return count_user;
            }

        }
    }
}