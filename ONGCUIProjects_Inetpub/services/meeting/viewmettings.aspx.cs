﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class viewmettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string employyee_name = "";
            string cpf_no = "";
            string todaydate;
              ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
              todaydate = My.ConvertDateintoSQLDateString(System.DateTime.Now);

          
          
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();
                employyee_name = Session["EmployeeName"].ToString();
            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            //string sStr = "SELECT O_TC_MTNG_MBR_DTL. O_MTNG_NO ,O_TC_MTNG_MBR_DTL.MEMBER_TASK, O_TC_MTNG_DTL.O_DLNG_MEMBR_NM  ,O_TC_MTNG_DTL.O_VENUE ,O_TC_MTNG_DTL.O_MTNG_DT ,O_TC_MTNG_DTL.O_MTNG_TM ,O_TC_MTNG_DTL.O_MTNG_DTLS,O_TC_MTNG_DTL.CY_MTNG_TITLE FROM O_TC_MTNG_MBR_DTL ,O_TC_MTNG_DTL  where O_TC_MTNG_MBR_DTL.O_MTNG_MBR_CPF_NO='" + cpf_no + "' and  O_TC_MTNG_DTL.O_INDX_NMBR=O_TC_MTNG_MBR_DTL.O_MTNG_NO and O_TC_MTNG_DTL.O_MTNG_DT>='" + todaydate + "' ORDER BY O_TC_MTNG_DTL.O_MTNG_DT ,O_TC_MTNG_DTL.O_MTNG_TM";
            string sStr = "sELECT [O_INDX_NMBR],[CY_MTNG_TYPE],[O_MTNG_DT],[O_MTNG_TM],[O_VENUE] FROM [ONGC].[dbo].[O_TC_MTNG_DTL]WHERE [O_MTNG_DT] >='" + todaydate + "' ORDER BY O_MTNG_DT ,O_MTNG_TM ";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            int my_count = 0;
            
            //string output = "<table id=\"tfhover\" class=\"tftable\" border=\"1\"><tbody><tr><th>Chaired By</th><th>Subject</th><th>Venue</th><th>Meeting Date</th><th>Meeting Time</th><th>Agenda</th><th></th></tr>";
            string output = "<table id=\"tfhover\" class=\"tftable\" border=\"1\"><tbody><tr><th>Meeting Type</th><th>Meeting Date</th><th>Meeting Time</th><th>Meeting Location</th><th></th></tr>";
            if (ds.Tables[0].Rows.Count == 0)
            {

                output = "FALSE";
            }
            else
            {

               
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string my_Date_time = System.Convert.ToDateTime(ds.Tables[0].Rows[i][2].ToString()).ToLongDateString().ToString();
                    my_count = my_count + 1;
                    string m_count = gettotalmeetingmember(ds.Tables[0].Rows[i][0].ToString());
                   // output = output + "<tr><td>" + ds.Tables[0].Rows[i][2] + "</td><td>" + ds.Tables[0].Rows[i][7] + "</td><td>" + ds.Tables[0].Rows[i][3] + "</td><td>" + my_Date_time + "</td><td>" + ds.Tables[0].Rows[i][5] + "</td><td>" + ds.Tables[0].Rows[i][6] + "</td><td><a id='view_detail' m_n=" + ds.Tables[0].Rows[i][0].ToString() + " href='javascript:void(0);' >view detail (" + m_count + ")</a></td></tr>";
                    output = output + "<tr><td>" + ds.Tables[0].Rows[i][1] + "</td><td>" + my_Date_time + "</td><td>" + ds.Tables[0].Rows[i][3] + "</td><td>" + ds.Tables[0].Rows[i][4] + "</td><td><a id='view_detail' m_n=" + ds.Tables[0].Rows[i][0].ToString() + " href='javascript:void(0);' >view Detail</a></td></tr>";
                }

                output = output + "</tbody></table>";
               

            }
            var old_meeting = getoldmeetingdetail(cpf_no, todaydate);
            Response.Write("TRUE~SUCCESS~Meeting Found Successfully~" + output + "~" + my_count.ToString()+"~" + old_meeting);
        }
        public string getoldmeetingdetail(string cpf, string todaydate)
        {
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            //string sStr = "SELECT O_TC_MTNG_MBR_DTL. O_MTNG_NO ,O_TC_MTNG_MBR_DTL.MEMBER_TASK, O_TC_MTNG_DTL.O_DLNG_MEMBR_NM  ,O_TC_MTNG_DTL.O_VENUE ,O_TC_MTNG_DTL.O_MTNG_DT ,O_TC_MTNG_DTL.O_MTNG_TM ,O_TC_MTNG_DTL.O_MTNG_DTLS ,O_TC_MTNG_DTL.CY_MTNG_TITLE FROM O_TC_MTNG_MBR_DTL ,O_TC_MTNG_DTL  where O_TC_MTNG_MBR_DTL.O_MTNG_MBR_CPF_NO='" + cpf + "' and  O_TC_MTNG_DTL.O_INDX_NMBR=O_TC_MTNG_MBR_DTL.O_MTNG_NO and O_TC_MTNG_DTL.O_MTNG_DT<'" + todaydate + "' ORDER BY O_TC_MTNG_DTL.O_MTNG_DT ,O_TC_MTNG_DTL.O_MTNG_TM";
            string sStr = "sELECT [O_INDX_NMBR],[CY_MTNG_TYPE],[O_MTNG_DT],[O_MTNG_TM],[O_VENUE] FROM [ONGC].[dbo].[O_TC_MTNG_DTL]WHERE [O_MTNG_DT] <'6/10/2013' ORDER BY O_MTNG_DT ,O_MTNG_TM ";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            //string output = "<table id=\"tfhover\" class=\"tftable\" border=\"1\"><tbody><tr><th>Chaired By</th><th>Title</th><th>Venue</th><th>Meeting Date</th><th>Meeting Time</th><th>Agenda</th><th></th></tr>";
            string output = "<table id=\"tfhover\" class=\"tftable\" border=\"1\"><tbody><tr><th>Meeting Type</th><th>Meeting Date</th><th>Meeting Time</th><th>Meeting Location</th><th></th></tr>";
            if (ds.Tables[0].Rows.Count == 0)
            {
              
                return "FALSE";
            }
            else
            {

                int my_count = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    my_count = my_count + 1;
                    string my_Date_time = System.Convert.ToDateTime(ds.Tables[0].Rows[i][2].ToString()).ToLongDateString().ToString();
                    string m_count = gettotalmeetingmember(ds.Tables[0].Rows[i][0].ToString());
                    output = output + "<tr><td>" + ds.Tables[0].Rows[i][1] + "</td><td>" + my_Date_time + "</td><td>" + ds.Tables[0].Rows[i][3] + "</td><td>" + ds.Tables[0].Rows[i][4] + "</td><td><a id='view_detail' m_n=" + ds.Tables[0].Rows[i][0].ToString() + " href='javascript:void(0);' >view detail (" + m_count + ")</a></td></tr>";
                }
                output = output + "</tbody></table>";
                return output + "~" + my_count.ToString();

            }
        
        }
        public string gettotalmeetingmember(string meeting_no) {

            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT COUNT([O_INDX_NMBR])FROM [O_TC_MTNG_MBR_DTL]  where  [O_MTNG_NO]='"+meeting_no+"'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
         
            if (ds.Tables[0].Rows.Count == 0)
            {

                return "FALSE";
            }
            else
            {

                return ds.Tables[0].Rows[0][0].ToString();
                

            }
        
        }
    }

}