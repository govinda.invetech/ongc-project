﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace ONGCUIProjects.services.meeting
{
    public partial class getlasttenderdetail : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string cpf_no = "";
            if (Session["Login_Name"] != null)
            {
                cpf_no = Session["Login_Name"].ToString();
                string tender_Type = Request.QueryString["tender_Type"];
                if (string.IsNullOrEmpty(tender_Type))
                {
                    Response.Write("FALSE~ERR~Invalid Tender Type");
                    return;
                }else{
                    // all code here
                    string str="SELECT [O_INDX_NMBR],[O_DLNG_MEMBR_NM],[O_DLNG_MEMBR_CPF_NMBR],[O_VENUE],[O_MTNG_DT],[O_MTNG_DTLS],[CY_MTNG_TITLE] FROM [O_TC_MTNG_DTL] where [CY_MTNG_TYPE]='"+tender_Type+"' and [CY_ENTRY_BY]='"+cpf_no+"' ORDER BY [O_INDX_NMBR] DESC";                   
                    DataSet ds=My.ExecuteSELECTQuery(str);
                    if(ds.Tables[0].Rows.Count==0){
                    Response.Write("FALSE~ERR~Last "+tender_Type+" Meeting Detail Not Found");                       
                    }else{
                           string index_no = ds.Tables[0].Rows[0][0].ToString();
                           string chaired_By_name=ds.Tables[0].Rows[0][1].ToString();
                           string chaired_By_cpf_no=ds.Tables[0].Rows[0][2].ToString();
                           string venue=ds.Tables[0].Rows[0][3].ToString();
                           string meeting_Date=ds.Tables[0].Rows[0][4].ToString();
                           string meeeting_details=ds.Tables[0].Rows[0][5].ToString();
                           string meeting_title=ds.Tables[0].Rows[0][6].ToString();
                           string cmplete_member_detail = getmemberdetail(index_no);
                           Response.Write("TRUE~SUCCESS~ Meeting Detail Found~" + chaired_By_name + "~" + chaired_By_cpf_no + "~" + venue + "~" + meeeting_details + "~" + meeting_title + "~" + cmplete_member_detail);
                    }
                
                
                }
            }
            else { 
             Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            }
        public string getmemberdetail(string index_no) {
            string mystr = "SELECT [O_MTNG_MBR_NM],[O_MTNG_MBR_CPF_NO] FROM [O_TC_MTNG_MBR_DTL] where [O_MTNG_NO]='"+index_no+"'";
            DataSet dss = My.ExecuteSELECTQuery(mystr);
            if (dss.Tables[0].Rows.Count == 0)
            {
                return "";
            }
            else {
                string for_print = "";
                int count = 0;
                for (int i = 0; i < dss.Tables[0].Rows.Count; i++) { 
                string m_cpf_no=dss.Tables[0].Rows[i][1].ToString();
                string m_member_name = dss.Tables[0].Rows[i][0].ToString();
                count = count + 1;
                string cmplete_div_value = "<div style=\"position:relative; float:left; width:98%;  background-color: #F5F5F5; color: black; margin-top:8px;padding: 10px; padding-top: 7px;border: 1px dashed rgb(163, 158, 158);\" div_value='"+m_member_name+"`"+m_cpf_no+"' cpf_no='"+m_cpf_no+"' class=\"my_cmplt_strp\">";
                cmplete_div_value = cmplete_div_value + "<p class=\"my_strip_count content\">["+(i+1).ToString()+".]</p><p class=\"content\">CPF No. : "+m_cpf_no+"</p> <p class=\"content\">NAME : "+m_member_name+"</p> <a href=\"javascript:void(0);\" id=\"delete_me\"><img src=\"images/cross.png\" style=\"position:relative;margin-top:5px; float:right;\"></a></div>";
                for_print = for_print + cmplete_div_value;
                }
                return for_print + "~" + count.ToString();
            }
        
        }
        }
    }
