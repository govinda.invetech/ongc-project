﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace ONGCUIProjects.services
{
    public partial class viewmeetingdetailinfancybox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string meeting_no = "";
            meeting_no = Request.QueryString["meeting_no"];
            if (meeting_no == "") {

                return;
            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_MTNG_MBR_NM] ,[O_MTNG_MBR_CPF_NO] FROM [O_TC_MTNG_MBR_DTL] where [O_MTNG_NO]='"+meeting_no+"'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            string output = "<div style='max-height:300px; overflow:auto;'><p class='content' style='width: 100%; border-bottom: 2px solid #34343b; line-height: 30px; margin-bottom: 5px;'>Member Details</p><table id=\"tfhover\" class=\"tftable\" border=\"1\"><tbody><tr><th >Serial No</th><th>Employee CPF No</th><th>Employee Name</th></tr>";
            if (ds.Tables[0].Rows.Count == 0)
            {

                output = "FALSE";
            }
            else
            {


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    output = output + "<tr><td>" + (i + 1).ToString() + "</td><td>" + ds.Tables[0].Rows[i][1] + "</td><td>" + ds.Tables[0].Rows[i][0] + "</td></tr></tbody>";
                }

                output = output + "</table></div>";
                Response.Write(output);

            }
        }
    }
}