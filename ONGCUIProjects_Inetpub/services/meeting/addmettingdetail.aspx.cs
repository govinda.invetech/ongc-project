﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services
{
    public partial class addmettingdetail : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {

            string employyee_name = "";
            string cpf_no = "";
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                string CPFNO = Session["Login_Name"].ToString();
                string Name = Session["EmployeeName"].ToString();
                cpf_no = Session["Login_Name"].ToString();
                employyee_name = Session["EmployeeName"].ToString();
                string hint = Request.QueryString["hint"];
                string file_no = Request.QueryString["file_no"];
                string file_date = Request.QueryString["file_date"];
                string subject = Request.QueryString["subject"];
                string meeting_date = Request.QueryString["meeting_date"];
                string meeting_time = Request.QueryString["meeting_time"];
                string location = Request.QueryString["location"];
                string member_full_list = Request.QueryString["member_full_list"];
                string fileno1 = Request.QueryString["fileno1"];
                string filedate1 = Request.QueryString["filedate1"];
                string ddlmeetingtype = Request.QueryString["ddlmeetingtype"];
                string subject1 = Request.QueryString["subject1"];
                string dealing_member_name = Request.QueryString["dealing_member_name"];
                string dealing_member_cpf_no = Request.QueryString["dealing_member_cpf_no"];
                string meeting_date1 = Request.QueryString["meeting_date1"];
                string meeting_time1 = Request.QueryString["meeting_time1"];
                string txtvenuename = Request.QueryString["txtvenuename"];
                string txtmeeting_agenda = Request.QueryString["txtmeeting_agenda"];
                string div_value_print = Request.QueryString["div_value_print"];

                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd;
                SqlDataAdapter da;
                if (hint == "TenderCommittee")
                {
                   string str = "Insert into O_TC_MTNG_DTL(O_FILE_NAME, O_FILE_DATE, O_SUBJECT, O_MTNG_DT,O_MTNG_TM,O_VENUE,CY_MTNG_TYPE,CY_TIMESTAMP,CY_ENTRY_BY,CY_ENTRY_BY_NAME) Values('" + file_no + "', '" + file_date + "', '" + subject + "', '" + meeting_date + "', '" + meeting_time + "', '" + location + "','" + hint + "', GETDATE(),'" + CPFNO + "','" + Name + "')";
                   cmd= new SqlCommand(str);
                   cmd.Connection = conn;
                   conn.Open();
                   int a=  cmd.ExecuteNonQuery();
                   conn.Close();
                     
                    if (a > 0)
                    {
                        member_full_list = member_full_list.Substring(0, member_full_list.Length - 1);
                        string[] member_full_list_array = member_full_list.Split('~');
                        string sstr = "Insert into O_TC_MTNG_MBR_DTL(O_MTNG_NO, O_MTNG_MBR_NM,O_MTNG_MBR_CPF_NO,O_MTNG_MBR_ROLE,O_SYS_DT_TM,IS_VIEW)Values";
                        string metting_no = getmeetingno();
                        for (int i = 0; i < member_full_list_array.Length; i++)
                        {
                            string[] my_sub_strip = member_full_list_array[i].Split('`');
                            string member_name = my_sub_strip[1];
                            string member_cpf_no = my_sub_strip[3];
                            string role = my_sub_strip[5];
                            string for_what = "";
                            sstr = sstr + " ('" + metting_no + "','" + member_name + "', '" + member_cpf_no + "', '" + role + "', GETDATE(),'FALSE'),";
                            // get_index_no = get_index_no + 1;
                        }

                        sstr = sstr.Substring(0, sstr.Length - 1);
                        cmd = new SqlCommand(sstr);
                        cmd.Connection = conn;
                        conn.Open();
                     int b=   cmd.ExecuteNonQuery();
                        conn.Close();
                        if (b > 0)
                        {
                            Response.Write("TRUE~SUCCESS~Meeting Schedule Added Successfully !");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~Meeting Schedule Not Added Successfully !");
                        }
                    }
                    else 
                    {
                        Response.Write("FALSE~ERR~Meeting Schedule Not Added Successfully !");
                    }
                   

                }
                else 
                {
                    string str1 = "Insert into O_TC_MTNG_DTL(O_FILE_NAME, O_FILE_DATE, O_SUBJECT, O_MTNG_DT,O_MTNG_TM,O_VENUE,CY_MTNG_TYPE,CY_TIMESTAMP,O_DLNG_MEMBR_NM,O_DLNG_MEMBR_CPF_NMBR,O_MTNG_DTLS,CY_ENTRY_BY,CY_ENTRY_BY_NAME) Values('" + fileno1 + "', '" + filedate1 + "', '" + subject1 + "', '" + meeting_date1 + "', '" + meeting_time1 + "', '" + txtvenuename + "','" + ddlmeetingtype + "', GETDATE(),'" + dealing_member_name + "','" + dealing_member_cpf_no + "','" + txtmeeting_agenda + "','" + CPFNO + "','" + Name + "')";
                    cmd = new SqlCommand(str1);
                    cmd.Connection = conn;
                    conn.Open();
                    int a = cmd.ExecuteNonQuery();
                    conn.Close();

                    if (a > 0)
                    {
                        div_value_print = div_value_print.Substring(0, div_value_print.Length - 1);
                        string[] div_value_print_array = div_value_print.Split('~');
                        string sstr1 = "Insert into O_TC_MTNG_MBR_DTL(O_MTNG_NO, O_MTNG_MBR_NM,O_MTNG_MBR_CPF_NO,O_SYS_DT_TM,IS_VIEW)Values";
                        string metting_no = getmeetingno();
                        for (int i = 0; i < div_value_print_array.Length; i++)
                        {
                            string[] my_sub_strip = div_value_print_array[i].Split('`');
                            string member_name = my_sub_strip[0];
                            string member_cpf_no = my_sub_strip[1];
                            string for_what = "";
                            sstr1 = sstr1 + " ('" + metting_no + "','" + member_name + "', '" + member_cpf_no + "', GETDATE(),'FALSE'),";
                            // get_index_no = get_index_no + 1;
                        }

                        sstr1 = sstr1.Substring(0, sstr1.Length - 1);
                        cmd = new SqlCommand(sstr1);
                        cmd.Connection = conn;
                        conn.Open();
                        int b = cmd.ExecuteNonQuery();
                        conn.Close();
                        if (b > 0)
                        {
                            Response.Write("TRUE~SUCCESS~Meeting Schedule Added Successfully !");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~Meeting Schedule Not Added Successfully !");
                        }
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~Meeting Schedule Not Added Successfully !");
                    }
                
                }
                
            //    string type = Request.QueryString["type"];
            //    string chaired_by_cpf_no = Request.QueryString["chaired_by_cpf_no"];
            //    string chaired_by_name = Request.QueryString["chaired_by_name"];
            //    string strip_data = Request.QueryString["strip_data"];
            //    //string meeting_date = Request.QueryString["meeting_date"];
            //    //string meeting_time = Request.QueryString["meeting_time"];
            //    string venue = Request.QueryString["venue"];
            //    string title = Request.QueryString["title"];
            //    string meeting_detail = Request.QueryString["meeting_detail"];
            //    if (string.IsNullOrEmpty(strip_data) || string.IsNullOrEmpty(meeting_date) || string.IsNullOrEmpty(meeting_time))
            //    {
            //        Response.Write("FALSE~ERR~Invalid Input");
            //        return;
            //    }
            //    if (validatstrip(strip_data) == "FALSE")
            //    {
            //        Response.Write("FALSE~ERR~Invalid Input Data");
            //        return;
            //    }
            //   meeting_date = My.ConvertDateStringintoSQLDateString(meeting_date);
            //   // string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            //   // SqlConnection conn = new SqlConnection(strConn);
            //  // SqlDataAdapter da = new SqlDataAdapter();
            //  //  DataSet ds = new DataSet();
            //  //  string str = "Insert into O_TC_MTNG_DTL(O_DLNG_MEMBR_NM, O_DLNG_MEMBR_CPF_NMBR, O_VENUE, O_MTNG_DT,O_MTNG_TM,O_MTNG_DTLS,O_SYS_DT_TM,CY_MTNG_TYPE,CY_MTNG_TITLE,CY_ENTRY_BY,CY_TIMESTAMP) Values('" + chaired_by_name + "', '" + chaired_by_cpf_no + "', '" + venue + "', '" + meeting_date + "', '" + meeting_time + "', '" + meeting_detail + "', GETDATE(),'" + type + "','" + title + "','" + cpf_no + "',GETDATE())";
            ////    SqlCommand cmd = new SqlCommand(str);
            //  //  cmd.Connection = conn;
            //    conn.Open();
            //   // cmd.ExecuteNonQuery();
            //    conn.Close();
            //    string meeting_no = getmeetingno();
            //    int get_index_no = getindex_no();
            //    strip_data = strip_data.Substring(0, strip_data.Length - 1);
            //    string[] strip_data_array = strip_data.Split('~');
            //    string sstr1 = "Insert into O_TC_MTNG_MBR_DTL(O_MTNG_NO, O_MTNG_MBR_NM,O_MTNG_MBR_CPF_NO,O_MTNG_MBR_ROLE,O_SYS_DT_TM)Values";
               
            //    for (int i = 0; i < strip_data_array.Length; i++)
            //    {
            //        string[] my_sub_strip = strip_data_array[i].Split('`');
            //        string member_name = my_sub_strip[0];
            //        string member_cpf_no = my_sub_strip[1];
            //        string role = my_sub_strip[2];
            //        string for_what = "";
            //      //  sstr1 = sstr1 + " ('" + metting_no + "','" + member_name + "', '" + member_cpf_no + "', '" + member_cpf_no + "', GETDATE(),'FALSE'),";
            //        get_index_no = get_index_no + 1;
            //    }
            //    //sstr = sstr + " ('" + get_index_no + "','" + meeting_no + "', '" + employyee_name + "', '" + cpf_no + "', GETDATE(),'FALSE'),";
            //    sstr1 = sstr1.Substring(0, sstr1.Length - 1);
            //    if (insertquery(sstr1) == "TRUE")
            //    {
            //        Response.Write("TRUE~SUCCESS~Meeting Schedule Added Successfully !");
            //    }
            //    else
            //    {
            //        Response.Write("FALSE~ERR~Meeting Schedule Not Added Successfully !");
            //    }

            }
            
        }
        private string getmeetingno()
        {
            
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "Select Max(O_INDX_NMBR) as MeetingNo from O_TC_MTNG_DTL";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }

        }
        public string validatstrip(string strip)
        {
            strip = strip.Substring(0, strip.Length - 1);
            string[] strip_arr = strip.Split('~');
            for (int i = 0; i < strip_arr.Length; i++) {
                string[] substrip = strip_arr[i].Split('`');
                if (My.validatecpf_no(substrip[1]) == "FALSE") {
                    return "FALSE";
                }               
            
            }
            return "TRUE";
        
        }
        private int getindex_no()
        {

            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "Select Max(O_INDX_NMBR) as MeetingNo from O_TC_MTNG_MBR_DTL";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return 0;

            }
            else
            {
                if (ds.Tables[0].Rows[0][0].ToString() == "")
                {
                    return 1;

                }
                else {
                    int a = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                    a = a + 1;
                    return a;
                }
               
            }

        }
        public string insertquery(string str)
        {
             string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(str);
            cmd.Connection = conn;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            return "TRUE";

        }
        private string checkcpfno_old(string cpf_no)
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_CPF_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }
        }
    }
}