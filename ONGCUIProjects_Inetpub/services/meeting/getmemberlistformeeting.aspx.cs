﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services.quiz
{
    public partial class getmemberlistformeeting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = Request.QueryString["type"];
            string session_cpf_no = "";
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                session_cpf_no = Session["Login_Name"].ToString();
               
            }
            string data = Request.QueryString["data"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "";
            string output = "";
          
            if (type == "DEPARTMENT")
            {
                if (data == "ALL")
                {
                    sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM],[O_EMP_DESGNTN] ,[O_EMP_DEPT_NM] FROM [O_EMP_MSTR] where [O_CPF_NMBR]<>'' ";
                }
                else
                {

                    sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM],[O_EMP_DESGNTN] ,[O_EMP_DEPT_NM] FROM [O_EMP_MSTR] where [O_EMP_DEPT_NM]='" + data + "' and [O_CPF_NMBR]<>'' ";
                }

            }
            else if (type == "DESIGNATION")
            {
                sStr = "SELECT [O_CPF_NMBR],[O_EMP_NM],[O_EMP_DESGNTN] ,[O_EMP_DEPT_NM] FROM [O_EMP_MSTR] where [O_EMP_DESGNTN]='" + data + "' and [O_CPF_NMBR]<>'' ";

            }
            else
            {

                Response.Write("FALSE~ERR~Invalid Type");
                return;
            }


            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                if (type == "DEPARTMENT")
                {
                    Response.Write("FALSE~SUCCESS~No Employee Found On Selected Departement Please Select Another Department~NA");
                    return;

                }
                else if (type == "DESIGNATION")
                {
                    Response.Write("FALSE~SUCCESS~No Employee Found On Selected Designation Please Select Another Designation~NA");
                    return;

                }

            }
            else
            {
                
                
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    output = output + " <label  class='strip-box'> <input class='select_strip' my_data='" + ds.Tables[0].Rows[i][0] + "`" + ds.Tables[0].Rows[i][1] + "'  style='position: relative ; float : left; margin-top: 5px;' type='checkbox' /><p  style='position: relative ; float : left; margin-left:10px; width: 92%;'>Name : " + ds.Tables[0].Rows[i][1] + "  , CPF No. " + ds.Tables[0].Rows[i][0] + "</p></label>";

                }

                Response.Write("TRUE~SUCCESS~Member Found Successfully !~" + output);

            }
            
        }
    }
}