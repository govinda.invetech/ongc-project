﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;

namespace ONGCUIProjects.services.meeting
{
    public partial class downloadpdf : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            string meeting_no = "";
            meeting_no = Request.QueryString["meeting_no"];
            if (meeting_no == "")
            {

                return;
            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            //DataSet ds = new DataSet();


            //string sStr = "SELECT [O_MTNG_MBR_NM] ,[O_MTNG_MBR_CPF_NO] FROM [O_TC_MTNG_MBR_DTL] where [O_MTNG_NO]='" + meeting_no + "'";
            //da = new SqlDataAdapter(sStr, conn);
            //da.Fill(ds);
            //string output = "<div style='max-height:300px; overflow:auto;'><p class='content' style='width: 100%; border-bottom: 2px solid #34343b; line-height: 30px; margin-bottom: 5px;'>Member Details</p><table id=\"tfhover\" class=\"tftable\" border=\"1\"><tbody><tr><th >Serial No</th><th>Employee CPF No</th><th>Employee Name</th></tr>";
            //if (ds.Tables[0].Rows.Count == 0)
            //{

            //    output = "FALSE";
            //}
            //else
            //{


            //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //    {
            //        output = output + "<tr><td>" + (i + 1).ToString() + "</td><td>" + ds.Tables[0].Rows[i][1] + "</td><td>" + ds.Tables[0].Rows[i][0] + "</td></tr></tbody>";
            //    }

            //    output = output + "</table></div>";
            //    Response.Write(output);

            //}



            string sQuery = "SELECT [O_FILE_NAME],[O_FILE_DATE],[O_SUBJECT],[O_DLNG_MEMBR_NM] ,[O_DLNG_MEMBR_CPF_NMBR],[O_VENUE],[O_MTNG_DT] ,[O_MTNG_TM],[CY_MTNG_TYPE],[CY_ENTRY_BY_NAME] FROM [ONGC].[dbo].[O_TC_MTNG_DTL] where [O_INDX_NMBR]='" + meeting_no + "'";
            DataSet ds = My.ExecuteSELECTQuery(sQuery);
            DataTable dtdataTable = ds.Tables[0];
            Font font = FontFactory.GetFont(FontFactory.TIMES, 7, Font.NORMAL);
            Font font1 = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            DateTime dtToday = System.DateTime.Today;
            string filename = dtToday.Day + "_" + dtToday.ToString("MM") + "_" + dtToday.Year + " - " + dtdataTable.Rows.Count + " Contacts";
            Document document = new Document(PageSize.A4);
            document.SetMargins(40f, 40f, 0f, 0f);
            //Document dd =new Document (new Rectangle(188f,200f),
          //  document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + filename + ".pdf"; ;
            FileInfo newFile = new FileInfo(path);
            PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
            Font Heading1font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.NORMAL);
            Font Heading2font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, Font.NORMAL);
            Font Heading5font = FontFactory.GetFont(FontFactory.TIMES, 10, Font.NORMAL);
            Font ColFont = FontFactory.GetFont(FontFactory.TIMES, 10, Font.NORMAL);
            Font ColFonttotal = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            Font ColFontHeader = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);

            //BaseFont bf = BaseFont.CreateFont( @"\css\kartika.ttf", BaseFont.IDENTITY_H, true);
            string sstr = Server.MapPath(@"\css\k010.TTF");
            BaseFont bf = BaseFont.CreateFont(sstr, BaseFont.IDENTITY_H, true);


            iTextSharp.text.Font hindiheading = new iTextSharp.text.Font(bf,15, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font hindiheading1 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.BOLD);
            Paragraph HindiHeading1 = new Paragraph("vkW;y ,.M uspqjy xSl dkWjiksjs”ku fyfeVsM\n", hindiheading);
            HindiHeading1.Alignment = Element.ALIGN_JUSTIFIED;
            HindiHeading1.IndentationLeft = 250;
            Paragraph HindiHeading2 = new Paragraph("eqEcbZ {ks=] mj.k l;a=] mj.k\n", hindiheading1);
            HindiHeading2.Alignment = Element.ALIGN_JUSTIFIED;
           HindiHeading2.IndentationLeft = 250;

          
            Paragraph Heading1 = new Paragraph("Oil and Natural Gas Corporation Ltd\n", Heading1font);
            Heading1.Alignment = Element.ALIGN_JUSTIFIED;
            Heading1.IndentationLeft = 250;

            Paragraph Heading2 = new Paragraph("Mumbai Region, Dronagiri Bhavan\n", Heading5font);
            Heading2.Alignment = Element.ALIGN_JUSTIFIED;
            Heading2.IndentationLeft = 250;
            Paragraph Heading3 = new Paragraph("Uran Plant, Uran, Distt. Raigad- 400702, Maharashtra\n", Heading5font);
            Heading3.Alignment = Element.ALIGN_JUSTIFIED;
            Heading3.IndentationLeft = 250;
            Paragraph Heading4 = new Paragraph("Tel:022-2723 4300/4303, Fax:022-27222811", Heading5font);
            Heading4.Alignment = Element.ALIGN_JUSTIFIED;
            Heading4.IndentationLeft = 250;

            document.Open();


            //Create a two column table
            PdfPTable table = new PdfPTable(2);
            float[] TableCwidths1 = new float[] { 11f, 12f };
            table.SetWidths(TableCwidths1);
            table.WidthPercentage = 100;
            table.DefaultCell.Border = 0;
            table.DefaultCell.Padding = 4;
            table.AddCell(new Paragraph("MM Department", Heading2font));
            table.AddCell(new Paragraph("Tel:022-2723 4300/4303, Fax:022-27222811", Heading5font));


            document.Add(new Paragraph(" "));
            document.Add(new Paragraph(" "));
            document.Add(HindiHeading1);
            document.Add(HindiHeading2);
            document.Add(Heading1);
            document.Add(Heading2);
            document.Add(Heading3);
            document.Add(table);

            string imageFilePath = Server.MapPath(".");// +"\\Images\\logo-ongc_small.jpg";
            string imageFilePath1 = (imageFilePath.Remove(imageFilePath.Length - 17)) + "\\Images\\logo-ongc_small.jpg";
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath1);
            jpg.ScaleToFit(80f, 60f);
            jpg.SpacingBefore = 1f;
            jpg.SpacingAfter = 1f;
            jpg.SetAbsolutePosition(40, 725);
            jpg.Alignment = Element.ALIGN_LEFT;
            document.Add(jpg);
            iTextSharp.text.pdf.draw.LineSeparator line1 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 100f, Color.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(new Chunk(line1));

            PdfPTable table1 = new PdfPTable(2);
            float[] TableCwidths2 = new float[] { 70f, 12f };
            table1.SetWidths(TableCwidths2);
            table1.WidthPercentage = 100;
            table1.DefaultCell.Border = 0;
            table1.DefaultCell.Padding = 0;
            table1.AddCell(new Paragraph(ds.Tables[0].Rows[0][0].ToString(), Heading5font));

            table1.AddCell(new Paragraph(DateTime.Parse(ds.Tables[0].Rows[0][6].ToString()).ToString("MMMM dd,yyyy"), Heading5font));

            document.Add(table1);
            Font file_no = FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL);

            //Paragraph file_no_text = new Paragraph("NO: MR/URAN/MM/SERV-CONT/AMC/PIDS/EXTN/OES/22/2012-13", file_no);
            //file_no_text.Alignment = Element.ALIGN_LEFT;
            //document.Add(file_no_text);

            //Paragraph file_no_date = new Paragraph("December 11,2012\n", Heading2font);
            //file_no_date.Alignment = Element.ALIGN_RIGHT; 
            //document.Add(file_no_date);



            document.Add(new Paragraph(" "));
            Font noticefont = FontFactory.GetFont(FontFactory.TIMES_BOLD, 11, Font.UNDERLINE);
            Paragraph notice_heading = new Paragraph("NOTICE FOR TENDER COMMITTEE MEETING\n", noticefont);
            notice_heading.Alignment = Element.ALIGN_CENTER;
            document.Add(notice_heading);


            document.Add(new Paragraph(" "));


            Font Heading3font = FontFactory.GetFont(FontFactory.TIMES, 10, Font.NORMAL);
            PdfPTable table2 = new PdfPTable(2);
            float[] TableCwidths3 = new float[] { 4f, 55f };
            table2.SetWidths(TableCwidths3);
            table2.WidthPercentage = 100;
            table2.DefaultCell.Border = 0;
            table2.DefaultCell.Padding = 4;
            table2.AddCell(new Paragraph("Sub:-", Heading3font));
            table2.AddCell(new Paragraph(ds.Tables[0].Rows[0][2].ToString()+".\n", Heading2font));
            document.Add(table2);


            //Font subfont = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.NORMAL);
            //Paragraph sub_text = new Paragraph("Sub Comprenhensive AMC for perimeter Intrusion Detection Security System (PIDS) installed at ONGC URAN Plant,Uran for Three Months w.e.f from 03.10.2012, through M/s. Spectron Engineers Pvt. Ltd.Mumbai.\n", subfont);
            //notice_heading.Alignment = Element.ALIGN_LEFT;
            //document.Add(sub_text);

            Font sub_below_font = FontFactory.GetFont(FontFactory.TIMES_BOLD,10, Font.NORMAL);
            Paragraph sub_below_txt = new Paragraph("A tender Committee meeting comprising of the following officers for the subject tender is proposed to be held on " + DateTime.Parse(ds.Tables[0].Rows[0][6].ToString()).ToString("dd-MM-yyyy") + "  at " + ds.Tables[0].Rows[0][7].ToString() + " hours in the office of " + ds.Tables[0].Rows[0][5].ToString() + ".\n", Heading3font);
            sub_below_txt.Alignment = Element.ALIGN_LEFT;
            document.Add(sub_below_txt);
            
            string sQuery1 = "SELECT [O_MTNG_MBR_NM],[O_MTNG_MBR_CPF_NO],[O_MTNG_MBR_ROLE]FROM [ONGC].[dbo].[O_TC_MTNG_MBR_DTL] where [O_MTNG_NO]='"+meeting_no+"'";
            DataSet ds1 = My.ExecuteSELECTQuery(sQuery1);
            DataTable dtdataTable1 = ds1.Tables[0];

            PdfPTable TelDir = new PdfPTable(dtdataTable1.Columns.Count);


            float[] TableCwidths = new float[] { 6f, 6f,6f };
            TelDir.SetWidths(TableCwidths);
            TelDir.WidthPercentage = 100;

            document.Add(new Paragraph(" "));

            //float[] TableCwidths = new float[] { 1.5f, 4f, 4f, 4f, 1f, 1.5f, 1.5f, 1.5f, 4f, 4f };
            //TelDir.SetWidths(TableCwidths);
            //TelDir.WidthPercentage = 100;



            //for (int Col = 0; Col < dtdataTable1.Columns.Count; Col++)
            //{
            //    Paragraph pr = new Paragraph(dtdataTable1.Columns[Col].ColumnName, ColFontHeader);
            //    if (Col == 0)
            //    { pr.Alignment = Element.ALIGN_CENTER; }
            //    else
            //    { pr.Alignment = Element.ALIGN_CENTER; }
            //    PdfPCell cell = new PdfPCell();
            //    cell.AddElement(pr);
            //    TelDir.AddCell(cell);
            //}

            for (int Row = 0; Row < dtdataTable1.Rows.Count; Row++)
            {
                for (int Col1 = 0; Col1 < dtdataTable1.Columns.Count; Col1++)
                {
                    PdfPCell cell = new PdfPCell();
                    Paragraph ph = new Paragraph(dtdataTable1.Rows[Row].ItemArray[Col1].ToString(), ColFont);
                    if (Col1 == 0)
                    {
                        ph.Alignment = Element.ALIGN_CENTER;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    else if (Col1 >= 1 && Col1 <= 3)
                    {
                        ph.Alignment = Element.ALIGN_LEFT;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    else
                    {
                        ph.Alignment = Element.ALIGN_CENTER;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    cell.AddElement(ph);
                    TelDir.AddCell(cell);
                }
            }
            document.Add(TelDir);
            

            Font last_para = FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL);
            Paragraph last_para_txt = new Paragraph("The TC members are requisted to kindly make it convenient to attend the TC meeting as proposed\n", file_no);
            last_para_txt.Alignment = Element.ALIGN_LEFT;
            document.Add(last_para_txt);

            Paragraph signature1 = new Paragraph("(" + ds.Tables[0].Rows[0][9].ToString() + ")\n", file_no);
            signature1.Alignment = Element.ALIGN_RIGHT;
            document.Add(signature1);

            Paragraph designation = new Paragraph("(MM Officer)\n", file_no);
            designation.Alignment = Element.ALIGN_RIGHT;
            document.Add(designation);

            document.Add(new Paragraph(" "));
            Font Distribution = FontFactory.GetFont(FontFactory.TIMES, 10, Font.UNDERLINE);
            Paragraph distributaion = new Paragraph("Distribution", Distribution);
            distributaion.Alignment = Element.ALIGN_LEFT;
            document.Add(distributaion);
            //iTextSharp.text.pdf.draw.LineSeparator line2 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 11f, Color.BLACK, Element.ALIGN_LEFT,3);
            //document.Add(new Chunk(line2));

            for (int Row = 0; Row < dtdataTable1.Rows.Count; Row++)
            {
                int serialno = Row + 1;
                Paragraph name1 = new Paragraph(serialno+"."+"       Shri. "+dtdataTable1.Rows[Row].ItemArray[0].ToString()+","+dtdataTable1.Rows[Row].ItemArray[1].ToString()+"\n", file_no);
                name1.Alignment = Element.ALIGN_LEFT;
                document.Add(name1);

            }
            document.Close();

        }
    }
}