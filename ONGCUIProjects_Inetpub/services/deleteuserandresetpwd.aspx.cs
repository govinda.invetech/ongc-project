﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace ONGCUIProjects.services
{
    public partial class deleteuserandresetpwd : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["type"] == null || Request.QueryString["cpf_no"] == null)
            {
                Response.Write("TRUE~SUCCESS~Invalid Input");
                return;
            }
            else
            {
                try
                {
                    string type = Request.QueryString["type"].ToString();
                    string cpf_no = Request.QueryString["cpf_no"].ToString();

                    string sQuery = "";
                    string sMessage = "";

                    switch (type)
                    {
                        case "DELETE":
                            
                            sQuery = "DELETE FROM [CY_O_EMP_MSTR] WHERE [O_CPF_NMBR] = '" + cpf_no + "'";
                            sMessage = "User Request Deleted Successfully !" + My.deleteEmployeeSP("sp_delete_o_emp_mstr",cpf_no); // delete store procedure call from other table 
                            break;
                        case "RESET_PWD":
                            sQuery = "UPDATE [E_USER_MSTR] SET [E_USER_PSWRD] = '" + cpf_no + "' WHERE  E_USER_CODE ='" + cpf_no + "'";
                            sMessage = "Password Reset Successfully !";
                            break;
                        case "REJECTED":
                            sQuery = "UPDATE [CY_O_EMP_MSTR] SET [cy_user_status] = 'REJECTED' WHERE [O_CPF_NMBR] = '" + cpf_no + "'";
                            sMessage = "User Request Rejected Successfully !";
                            break;
                    }
                    if (My.ExecuteSQLQuery(sQuery) == true)
                    {
                        Response.Write("TRUE~SUCCESS~"+sMessage);
                    }
                    else
                    {
                        Response.Write("TRUE~SUCCESS~My App has occured some error.");
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("TRUE~SUCCESS~"+ex.Message);
                }
            }
        }
    }
}