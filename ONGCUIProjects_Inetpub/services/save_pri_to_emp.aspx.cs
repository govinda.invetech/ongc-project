﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class save_pri_to_emp : System.Web.UI.Page
    {
        SqlConnection conn;
        private string checkDefaultPrivilege(int childId)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds3 = new DataSet();
            string sStr_3 = "SELECT default_checked FROM CY_CHILD_MENU WHERE id = '" + childId + "'";
            da = new SqlDataAdapter(sStr_3, conn);
            da.Fill(ds3);
            if (ds3.Tables[0].Rows[0][0].ToString() == "TRUE")
            {
                return "TRUE";
            }
            else
            {
                return "FALSE";
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            string cpf_no = Request.QueryString["cpf_no"];
            //string cpf_no = Session["pri_cpf"].ToString();
            string child_ids = Request.QueryString["child_ids"];
            string type = Request.QueryString["type"];
            string Sessionid = "1";
            if (string.IsNullOrEmpty(cpf_no) && string.IsNullOrEmpty(type))
            {
                Response.Write("Problem in Input");
                return;
            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            conn = new SqlConnection(strConn);

            string QueryString = "";
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds3 = new DataSet();
            string sStr_3 = "SELECT id FROM CY_CHILD_MENU WHERE default_checked = 'TRUE'";
            da = new SqlDataAdapter(sStr_3, conn);
            da.Fill(ds3);
            if (ds3.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < ds3.Tables[0].Rows.Count; i++)
                {
                    int defaultChildId = System.Convert.ToInt32(ds3.Tables[0].Rows[i][0].ToString());
                    QueryString = QueryString + "('" + cpf_no + "','" + defaultChildId + "','" + Sessionid + "'),";
                }
            }
            if (child_ids != "")
            {
                child_ids = child_ids.Substring(0, child_ids.Length - 1);
            }
            if (child_ids.Trim() != "")
            {
                string[] child_ids_arr = child_ids.Split('`');
                for (int i = 0; i < child_ids_arr.Count(); i++)
                {
                    int child_menu_id = System.Convert.ToInt32(child_ids_arr[i]);
                    if (checkDefaultPrivilege(child_menu_id) != "TRUE")
                    {
                        QueryString = QueryString + "('" + cpf_no + "','" + child_menu_id + "','" + Sessionid + "'),";
                    }
                }

            }
            string DeleteString = "DELETE FROM CY_MENU_EMP_RELATION WHERE cpf_number = '" + cpf_no + "'";
            SqlCommand DelCmd = new SqlCommand(DeleteString);
            DelCmd.Connection = conn;
            conn.Open();
            DelCmd.ExecuteNonQuery();
            conn.Close();
            if (QueryString.Trim() != "")
            {
                QueryString = "INSERT INTO CY_MENU_EMP_RELATION (cpf_number,child_id,entry_by) VALUES " + QueryString.Substring(0, QueryString.Length - 1);
                SqlCommand cmd = new SqlCommand(QueryString);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                Response.Write("TRUE~SUCCESS~Privilege Saved Successfully");
            }


        }
    }
}