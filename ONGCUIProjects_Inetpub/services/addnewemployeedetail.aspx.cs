﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class addnewemployeedetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();
            DateTime now = DateTime.Now;
            string type = Request["type"];
            if (type == "NEW" || type == "UPDATE")
            {

            }
            else
            {
                Response.Write("FALSE~SUCCESS~Invalid type");
                return;
            }
            string e_name = "";
            e_name = Request["e_name"];
            string cpf_no = "";
            cpf_no = Request["cpf_no"];
            string add1 = Request["add1"];
            string add2 = Request["add2"];
            string city = Request["city"];
            string state = Request["state"];
            string pin_no = Request["pin_no"];
            string home_ph_no = Request["home_ph_no"];
            string mbl_no = Request["mbl_no"];
            string gender = Request["gender"];
            string designation = Request["designation"];
            string supervisior = Request["supervisior"];
            string department = Request["department"];
            string worklocation = Request["worklocation"];
            string employee_exno = Request["employee_exno"];
            string dob = my.ConvertDateStringintoSQLDateString(Request["dob"].ToString());
            string email1 = Request["email1"];
            string email2 = Request["email2"];
            string email3 = "";
            string blood_group = Request["blood_group"];

            if (string.IsNullOrEmpty(e_name))
            {
                Response.Write("FALSE~SUCCESS~Please Enter Employee Name First~");
                return;
            }
            else if (string.IsNullOrEmpty(cpf_no))
            {
                Response.Write("FALSE~SUCCESS~Please Enter Cpf No. First~");
                return;
            }
            else
            {
                e_name = e_name.Replace("~", "'");
                cpf_no = cpf_no.Replace("~", "");
                add1 = add1.Replace("~", "");
                add2 = add2.Replace("~", "");
                city = city.Replace("~", "");
                state = state.Replace("~", "");
                pin_no = pin_no.Replace("~", "");
                home_ph_no = home_ph_no.Replace("~", "");
                mbl_no = mbl_no.Replace("~", "");
                gender = gender.Replace("~", "");
                designation = designation.Replace("~", "");
                if (type == "NEW")
                {
                    if (checkcpfno_cybuzz(cpf_no) == "TRUE")
                    {
                        Response.Write("FALSE~SUCCESS~Request for this cpf no has been already submitted~");
                        return;
                    }
                    else if (checkcpfno_old(cpf_no) == "TRUE")
                    {
                        Response.Write("FALSE~SUCCESS~This Cpf No Already being used by other Employee~");
                        return;
                    }
                }
                else
                {
                    if (checkcpfno_old(cpf_no) == "FALSE")
                    {
                        Response.Write("FALSE~SUCCESS~Invalid CPF No.~");
                        return;
                    }
                }

                //sql connection
                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                string sqlQuery = "";
                if (type == "NEW")
                {
                    sqlQuery = "INSERT INTO CY_O_EMP_MSTR([O_CPF_NMBR],[O_EMP_NM],[O_EMP_HM_ADDR1],[O_EMP_HM_ADDR2],[O_EMP_HM_CITY] ,[O_EMP_HM_STATE],[O_EMP_HM_PIN_CD],[O_EMP_HM_PHN],[O_EMP_MBL_NMBR] ,[O_EMP_GNDR],[O_EMP_DOB] ,[O_EMP_BLD_GRP]  ,[O_EMAIL_ID1],[O_EMAIL_ID3] ,[O_EMAIL_ID2] ,[timestamp],[O_EMP_DESGNTN],[O_EMP_SUP_NM],[O_EMP_DEPT_NM],[O_EMP_WRK_LCTN],[CY_EMP_EX_NO])  values";
                    sqlQuery = sqlQuery + " ('" + cpf_no + "','" + e_name + "','" + add1 + "' ,'" + add2 + "','" + city + "','" + state + "','" + pin_no + "','" + home_ph_no + "','" + mbl_no + "','" + gender + "','" + dob + "' ,'" + blood_group + "','" + email1 + "','" + email3 + "','" + email2 + "','" + now + "','" + designation + "','" + supervisior + "','" + department + "','" + worklocation + "','" + employee_exno + "')";
                }
                else
                {
                    sqlQuery = "UPDATE [O_EMP_MSTR]   SET [O_EMP_NM] = '" + e_name + "' ,[O_EMP_HM_ADDR1] ='" + add1 + "' ,[O_EMP_HM_ADDR2] ='" + add2 + "' ,[O_EMP_HM_CITY] ='" + city + "' ,[O_EMP_HM_STATE] ='" + state + "',[O_EMP_HM_PIN_CD] = '" + pin_no + "' ,[O_EMP_HM_PHN] = '" + home_ph_no + "' ,[O_EMP_MBL_NMBR] ='" + mbl_no + "',[O_EMP_GNDR] ='" + gender + "',[O_EMP_DOB] = '" + dob + "'   ,[O_EMP_BLD_GRP] = '" + blood_group + "'  ,[O_EMAIL_ID1] = '" + email1 + "'   ,[O_EMAIL_ID2] = '" + email2 + "' ,[O_EMAIL_ID3] = '" + email3 + "',[O_EMP_DESGNTN]='" + designation + "',[O_EMP_SUP_NM]='" + supervisior + "',[O_EMP_DEPT_NM]='" + department + "',[O_EMP_WRK_LCTN]='" + worklocation + "',[CY_EMP_EX_NO]='" + employee_exno + "' WHERE [O_CPF_NMBR]='" + cpf_no + "'";
                }

                if (my.ExecuteSQLQuery(sqlQuery))
                {
                    if (type == "NEW")
                    {
                        Response.Write("TRUE~SUCCESS~Your Request Submited Successfully~");
                    }
                    else
                    {
                        Response.Write("TRUE~SUCCESS~Your Information Updated Successfully !~");
                    }
                }
            }
        }
        // discription : check cpf no in cybuzz table 
        // created by : ashish gupta
        // date : 11/08/2012
        private string checkcpfno_cybuzz(string cpf_no)
        {
            string sCPF = Request["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_CPF_NMBR]FROM [CY_O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }

        }
        private string checkcpfno_old(string cpf_no)
        {
            string sCPF = Request["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_CPF_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }
        }

    }
}