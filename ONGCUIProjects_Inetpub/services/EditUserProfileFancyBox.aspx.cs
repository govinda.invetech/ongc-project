﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class EditUserProfileFancyBox : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {

            #region Load Designation
            string sDesigQuery = "SELECT [O_DSG_DTL] FROM [O_DSG_MSTR] ORDER BY [O_DSG_DTL] ASC";
            DataSet dsDesig = My.ExecuteSELECTQuery(sDesigQuery);
            if (dsDesig.Tables[0].Rows.Count != 0)
            {
                ddlDesignation.Items.Clear();
                ddlDesignation.Items.Add(new ListItem("Select Designation", ""));
                for (int i = 0; i < dsDesig.Tables[0].Rows.Count; i++)
                {
                    ddlDesignation.Items.Add(new ListItem(dsDesig.Tables[0].Rows[i][0].ToString(), dsDesig.Tables[0].Rows[i][0].ToString()));
                }
            }
            #endregion

            #region Load Department
            string sDeptQuery = "SELECT [name] FROM [CY_DEPT_DTLS] ORDER BY [name] ASC";
            DataSet dsDept = My.ExecuteSELECTQuery(sDeptQuery);
            if (dsDept.Tables[0].Rows.Count != 0)
            {
                ddlDepartment2.Items.Clear();
                ddlDepartment2.Items.Add(new ListItem("Select Department", ""));
                for (int i = 0; i < dsDept.Tables[0].Rows.Count; i++)
                {
                    ddlDepartment2.Items.Add(new ListItem(dsDept.Tables[0].Rows[i][0].ToString(), dsDept.Tables[0].Rows[i][0].ToString()));
                }
            }
            #endregion

            #region Load Location
            string sLctnQuery = "SELECT [O_LCTN_NM] FROM [O_LCTN_MSTR] ORDER BY [O_LCTN_NM] ASC";
            DataSet dsLctn = My.ExecuteSELECTQuery(sLctnQuery);
            if (dsLctn.Tables[0].Rows.Count != 0)
            {
                ddlWorkLocation.Items.Clear();
                ddlWorkLocation.Items.Add(new ListItem("Select Location", ""));
                for (int i = 0; i < dsLctn.Tables[0].Rows.Count; i++)
                {
                    ddlWorkLocation.Items.Add(new ListItem(dsLctn.Tables[0].Rows[i][0].ToString(), dsLctn.Tables[0].Rows[i][0].ToString()));
                }
            }
            #endregion

            #region Load Supervisor
            string sSupervisorQuery = "SELECT  [O_EMP_NM], [O_CPF_NMBR]  FROM [O_EMP_MSTR] ORDER BY [O_EMP_NM] ASC";
            DataSet dsSupervisor = My.ExecuteSELECTQuery(sSupervisorQuery);
            if (dsSupervisor.Tables[0].Rows.Count != 0)
            {
                ddlSupervisiorname.Items.Clear();
                ddlSupervisiorname.Items.Add(new ListItem("", ""));
                for (int i = 0; i < dsSupervisor.Tables[0].Rows.Count; i++)
                {
                    ddlSupervisiorname.Items.Add(new ListItem(dsSupervisor.Tables[0].Rows[i][0].ToString(), dsSupervisor.Tables[0].Rows[i][1].ToString()));
                }
            }
            #endregion

            if (string.IsNullOrEmpty(Request["CPFNo"]))
            {
            }
            else
            {
                string sCPFNo = Request["CPFNo"];
                string sEmpQuery = "SELECT O_EMP_NM, O_EMP_HM_ADDR1, O_EMP_HM_ADDR2, O_EMP_HM_CITY, O_EMP_HM_STATE, O_EMP_HM_PIN_CD, ";
                sEmpQuery += " O_EMP_HM_PHN, O_EMP_MBL_NMBR, O_EMP_GNDR, O_EMP_DOB , O_EMAIL_ID1, O_EMAIL_ID2, O_EMAIL_ID3  ";
                sEmpQuery += "  , O_EMP_DESGNTN,O_EMP_SUP_NM, O_EMP_DEPT_NM, O_EMP_WRK_LCTN,CY_EMP_EX_No,O_EMP_BLD_GRP FROM  O_EMP_MSTR where O_CPF_NMBR='" + sCPFNo + "'";

                DataSet dsEmpDtl = My.ExecuteSELECTQuery(sEmpQuery);
                if (dsEmpDtl.Tables[0].Rows.Count != 0)
                {
                    txtEmpCPFNo.Text = sCPFNo;
                    txtEmpCPFNo.Enabled = false;
                    txtEmpName.Text = dsEmpDtl.Tables[0].Rows[0][0].ToString();//O_EMP_NM, 
                    txtAddr1.Text = dsEmpDtl.Tables[0].Rows[0][1].ToString();//O_EMP_HM_ADDR1, 
                    txtAddr2.Text = dsEmpDtl.Tables[0].Rows[0][2].ToString();//O_EMP_HM_ADDR2, 
                    txtCity.Text = dsEmpDtl.Tables[0].Rows[0][3].ToString(); //O_EMP_HM_CITY, 
                    txtState.Text = dsEmpDtl.Tables[0].Rows[0][4].ToString();//O_EMP_HM_STATE, 
                    txtPinCode.Text = dsEmpDtl.Tables[0].Rows[0][5].ToString();//O_EMP_HM_PIN_CD,
                    txtHomeNo.Text = dsEmpDtl.Tables[0].Rows[0][6].ToString();//O_EMP_HM_PHN, 
                    txtMblNo.Text = dsEmpDtl.Tables[0].Rows[0][7].ToString();//O_EMP_MBL_NMBR, 
                    CmbGender.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][8].ToString()).Selected = true;//O_EMP_GNDR, 
                    txt_dob.Text = System.Convert.ToDateTime(dsEmpDtl.Tables[0].Rows[0][9]).ToString("dd-MM-yyyy");//O_EMP_DOB , 
                    txtEmail1.Text = dsEmpDtl.Tables[0].Rows[0][10].ToString();//O_EMAIL_ID1, 
                    txtEmail2.Text = dsEmpDtl.Tables[0].Rows[0][11].ToString();//O_EMAIL_ID2, 
                    //.Text = dsEmpDtl.Tables[0].Rows[0][12].ToString();//O_EMAIL_ID3 
                    if (ddlDesignation.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][13].ToString()) != null)
                    {
                        ddlDesignation.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][13].ToString()).Selected = true;//O_EMP_DESGNTN,
                    }
                    if (ddlSupervisiorname.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][14].ToString()) != null)
                    {
                        ddlSupervisiorname.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][14].ToString()).Selected = true;//O_EMP_SUP_NM,
                    }
                    if (ddlDepartment2.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][15].ToString()) != null)
                    {
                        ddlDepartment2.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][15].ToString()).Selected = true;//O_EMP_DEPT_NM, 
                    }
                    if (ddlWorkLocation.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][16].ToString()) != null)
                    {
                        ddlWorkLocation.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][16].ToString()).Selected = true;//O_EMP_WRK_LCTN,
                    }
                    txtemployeeexno.Text = dsEmpDtl.Tables[0].Rows[0][17].ToString();//CY_EMP_EX_No,
                    if (cmbBldGrp.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][18].ToString()) != null)
                    {
                        cmbBldGrp.Items.FindByValue(dsEmpDtl.Tables[0].Rows[0][18].ToString()).Selected = true;//O_EMP_BLD_GRP
                    }
                }
            }



        }
    }
}