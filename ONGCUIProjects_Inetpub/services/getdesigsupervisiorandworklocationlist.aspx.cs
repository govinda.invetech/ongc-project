﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class getdesigsupervisiorandworklocationlist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("TRUE~SUCCESS~List Found Successfully !~" + getdesignationlist() + "~" + locationlist() + "~" + supervisiorlist() + "~" + getpassapproveemployee() + "~" + getdepartmentlist());
        }

        // getdesugnation List 
        private string getdesignationlist()
        {
            string output = "<option value=''>Select Designation </option>";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_DSG_DTL]  FROM [O_DSG_MSTR] where [O_DSG_DTL]<>'' ORDER BY [O_DSG_DTL] ASC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    output = output + "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][0].ToString() + "</option>";
                }
            }
            return output;
        }

        // get locationlist
        private string locationlist()
        {
            string output = "<option value=''>Select Location </option>";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_LCTN_NM]  FROM [O_LCTN_MSTR] where [O_LCTN_NM]<>'' ORDER BY [O_LCTN_NM] ASC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    output = output + "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][0].ToString() + "</option>";
                }
            }
            return output;
        }

        // get supervisiorlist
        private string supervisiorlist()
        {
            string output = "<option value=''>Select Supervisior </option>";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT  [O_EMP_NM],[O_CPF_NMBR]  FROM [O_EMP_MSTR] where [O_EMP_NM]!='' ORDER BY [O_EMP_NM] ASC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    output = output + "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][0].ToString() + "</option>";
                }
            }
            return output;
        }

        // function getpassapproveemployee
        private string getpassapproveemployee()
        {
            string my_output = "";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT  [CY_MENU_EMP_RELATION].[cpf_number],[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] ,[O_EMP_MSTR] where [CY_MENU_EMP_RELATION].[child_id]='18'  and [O_EMP_MSTR].[O_CPF_NMBR] = [CY_MENU_EMP_RELATION].[cpf_number]";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                my_output = "<option value=''>No Employee Found For Approving Pass</option>";
                return my_output;
            }
            else
            {
                my_output = "<option value=''>Select Employee</option>";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    my_output = my_output + "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                }
                return my_output;
            }
        }
        // Department List 
        private string getdepartmentlist()
        {
            string output = "<option value=''>Select Department </option>";
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [name] FROM [CY_DEPT_DTLS] ORDER BY [name] ASC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    output = output + "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][0].ToString() + "</option>";
                }
            }
            return output;
        }
    }
}