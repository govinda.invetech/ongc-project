﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class getRequisitiondetailaspx : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cpf_no = "";
            string cy_group = Request.QueryString["cy_group"];
            if (cy_group == "") {
                Response.Write("FALSE~ERR~Invalid GROUP");
                return;
            }
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();
            }
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "select O_INDX_NMBR, [O_VISTR_NM], [O_VISTR_AGE], [O_VISTR_ORGNZTN],[O_PRPS_OF_VISIT], O_ENTRY_DT ,[CY_O_VISTR_ENTRY_TIME],[O_RMRKS],[CY_WITH_TOOL] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_PASS_TYPE_FLG ='V' and O_REQ_COMPLTN_FLG = 'A' and O_ACP1_CPF_NMBR = '" + cpf_no + "'  and CY_GROUP<>'' and CY_GROUP='" + cy_group + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("FALSE~SUCCESS~No Request Found~NA");
                return;
            }
            else
            {
                string print = "<table style=\"position:relative ; float:left; \" >";
                print = print + "<tr><th class='th2'>S.N.</th><th class='th2'>VISITOR NAME</th><th class='th2'>VISITOR AGE</th><th class='th2'>VISITOR ORGANIZATION</th> <th class='th2'>PURPOSE OF VISIT</th><th class='th2'>VISITING DATE </th><th class='th2'> VISITING TIME</th> <th class='th2'>REMARKS </th><th class='th2'>TOOLS</th><th class='th2'></th></tr>";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    print = print + "<tr class='reject_reuest'><td class='td2'> VISITOR [" + (i + 1).ToString() + "]" + "</td><td class='td2'>" + ds.Tables[0].Rows[i][1].ToString() + "</td><td class='td2'>" + ds.Tables[0].Rows[i][2].ToString() + "</td><td class='td2'>" + ds.Tables[0].Rows[i][3].ToString() + "</td><td class='td2'>" + ds.Tables[0].Rows[i][4].ToString() + "</td><td class='td2'>" + ds.Tables[0].Rows[i][5].ToString() + "</td><td class='td2'>" + ds.Tables[0].Rows[i][6].ToString() + "</td><td class='td2'>" + ds.Tables[0].Rows[i][7].ToString() + "</td><td class='td2'>" + ds.Tables[0].Rows[i][8].ToString() + "</td><td class='td2'><a id='cancel_request' g_n=" + cy_group + " i_n=" + ds.Tables[0].Rows[i][0].ToString() + " href='javascript:void(0);'><img src='images/cross.png'/></a></td>";

                }
                Response.Write("TRUE~SUCCESS~Requition detail Found Successfully !~" + print);


            }
        }
    }
}