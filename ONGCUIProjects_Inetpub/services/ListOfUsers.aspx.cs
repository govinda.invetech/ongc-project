﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class ListOfUsers : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string sOutput = "";
                DataSet dsRegUser = My.RegisteredUsers();
                if (dsRegUser.Tables[0].Rows.Count == 0)
                {
                    NoOfUser.InnerHtml = "No Employee Registered";
                }
                else
                {
                    for (int i = 0; i < dsRegUser.Tables[0].Rows.Count; i++)
                    {
                        string sSlNo = dsRegUser.Tables[0].Rows[i][0].ToString();
                        string sEmpName = dsRegUser.Tables[0].Rows[i][1].ToString();
                        string sCPFNo = dsRegUser.Tables[0].Rows[i][2].ToString();
                        string sDesignation = dsRegUser.Tables[0].Rows[i][3].ToString();
                        sOutput = sOutput + "<tr>";
                        sOutput = sOutput + "<td>" + sSlNo + "</td>";
                        sOutput = sOutput + "<td>" + sEmpName + "</td>";
                        sOutput = sOutput + "<td>" + sCPFNo + "</td>";
                        sOutput = sOutput + "<td>" + sDesignation + "</td>";
                        sOutput = sOutput + "</tr>";
                    }
                    sOutput = "<table id='EmployeeTableData' border=\"1\" class=\"tftable\"><tbody><tr><th style=\"width:1px;white-space:nowrap;\">Serial No</th><th>Employee Name</th><th>CPF No</th><th>Designation</th></tr>" + sOutput + "</table>";
                    NoOfUser.InnerHtml = "Total "+dsRegUser.Tables[0].Rows.Count.ToString()+" user registered";
                    RegisteredEmpList.InnerHtml = sOutput;
                }
                
            }
        }
    }
}