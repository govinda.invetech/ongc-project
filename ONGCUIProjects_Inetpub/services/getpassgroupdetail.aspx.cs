﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class getpassgroupdetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
              string cpf_no = "";
          
            string group_no = "";
            if (Request.QueryString["group_no"] == null) {
                Response.Write("FALSE~ERR~invalid group no. !");
                return;
            }else{
               group_no = Request.QueryString["group_no"];
            }


            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();


            }
          
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "Select [O_APLCNT_NM],[O_APLCNT_CPF_NMBR] ,[O_APLCNT_DESG] ,[O_APLCNT_LCTN],[O_APLCNT_EXT_NMBR] ,[O_REQSTN_TYPE]  ,[O_ENTRY_DT]  ,[O_EXIT_DT] ,[O_ENTRY_AREA],[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG] ,[O_MTNG_OFF_LCTN],[O_VISTR_NM] ,[O_VISTR_ORGNZTN],[O_VISTR_AGE] ,[O_VISTR_GNDR]  ,[O_PRPS_OF_VISIT] ,[O_RMRKS],[O_ACP1_NM],[O_ACP1_CPF_NMBR],[CY_O_VISTR_ENTRY_TIME],[CY_O_VISTR_EXIT_TIME],[CY_WITH_TOOL] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_APLCNT_NM = '"+cpf_no+"' and O_PASS_TYPE_FLG = 'V' and O_REQ_COMPLTN_FLG = 'A' and CY_GROUP= '"+group_no+"' ";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
       
            if (ds.Tables[0].Rows.Count == 0)
            {
                Response.Write("FALSE~SUCCESS~No Request Found~NA");
                return;
            }
            else
            {
                string req_cpf_no = cpf_no;
                string req_name = ds.Tables[0].Rows[0][1].ToString();
                string req_design = ds.Tables[0].Rows[0][2].ToString();
                string req_location = ds.Tables[0].Rows[0][3].ToString();
                string req_ex_no = ds.Tables[0].Rows[0][4].ToString();
                string type_of_reqistion = ds.Tables[0].Rows[0][5].ToString();
                string entry_date = ds.Tables[0].Rows[0][6].ToString();
                string exit_date = ds.Tables[0].Rows[0][7].ToString();
                string entry_area = ds.Tables[0].Rows[0][8].ToString();
                string meeting_officer_name = ds.Tables[0].Rows[0][9].ToString();
                string meeting_officer_design = ds.Tables[0].Rows[0][10].ToString();
                string meeting_officer_loc = ds.Tables[0].Rows[0][11].ToString();

                string reason_visit = ds.Tables[0].Rows[0][16].ToString();
                string purpose_visit = ds.Tables[0].Rows[0][17].ToString();
                string approval_cpf_no = ds.Tables[0].Rows[0][19].ToString();
                string entry_time = ds.Tables[0].Rows[0][20].ToString();
                string exit_time = ds.Tables[0].Rows[0][21].ToString();
                string complete_data = req_cpf_no + "~" + req_name + "~" + req_design + "~" + req_location + "~" + req_ex_no + "~" + entry_date + "~" + exit_date + "~" + entry_area + "~" + meeting_officer_name + "~" + meeting_officer_design + "~" + meeting_officer_loc + "~" + req_ex_no + "~" + reason_visit + "~" + purpose_visit + "~" + approval_cpf_no + "~" + entry_time + "~" + exit_time + "~" + type_of_reqistion;
                string my_div_strip = "";
                string print_Div = "<p style=\"position:relative; float:left; font-size:20px; color:Black\" id=\"total_theory\"></p>";
                  int my_counter =0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string name = ds.Tables[0].Rows[i][12].ToString();
                    string company_name = ds.Tables[0].Rows[i][13].ToString();
                    string age = ds.Tables[0].Rows[i][14].ToString();
                    string gender = ds.Tables[0].Rows[i][15].ToString();
                    string tools = ds.Tables[0].Rows[i][22].ToString();
                     my_counter = i + 1;
                    
                    string div_value = "name`" + name + "`age`" + age + "`cmny_name`" + company_name + "`gender`" + gender + "`tools`" + tools;
                    my_div_strip = "<div class='my_complete_div_strip' div_value= '" + div_value + "' style=\"position:relative; float:left; width:100%;  background-color: #F5F5F5; color: black; margin-top:8px;padding-top: 10px;padding-bottom: 10px;border: 1px dashed rgb(163, 158, 158);\">";
                    my_div_strip = my_div_strip + "<p id=\"div_strip_count\" class='strip_my_p'>" + my_counter + ". </p>";
                    my_div_strip = my_div_strip + "<p class='strip_my_p'>[Name : " + name + "]</p>";
                    my_div_strip = my_div_strip + "<p class='strip_my_p'>, [Age : " + age + "]</p>";
                    my_div_strip = my_div_strip + "<p class='strip_my_p'>, [Company : " + company_name + "]</p>";
                    my_div_strip = my_div_strip + "<p class='strip_my_p'>, [Gender : " + gender + "]</p>";
                    my_div_strip = my_div_strip + "<p class='strip_my_p'>, [Tools : " + tools + "]</p>";
                    my_div_strip = my_div_strip + "  <a id=\"delete_my_strip\" href=\"javascript:void(0);\" ><img style=\"position:relative; float:right; margin-right:10px; margin-top:4px;\" src=Images/cross.png ; /></a></div>";
                    print_Div = print_Div + my_div_strip;
                }
                Response.Write("TRUE~SUCCESS~Visitor detail Found Successfully !~" + complete_data + "~" + print_Div+"~"+my_counter.ToString());
              

            }
        }
    }
}