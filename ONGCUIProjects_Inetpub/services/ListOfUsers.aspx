﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListOfUsers.aspx.cs" Inherits="ONGCUIProjects.services.ListOfUsers" %>

<script type="text/javascript">
    $("#cmdExportToExcel").click(function (e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#RegisteredEmpList').html()));
        e.preventDefault();
    });
</script>
<form id="form1" runat="server">
<div style="position: relative; float: left; width: 700px;">
    <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A;">
        Registered Users<span style='float: right;'><input type="button" value="Export To Excel"
            id="cmdExportToExcel" class='g-button g-button-submit' /></span></h1>
    <p runat="server" id="NoOfUser" class="content" style="margin: 5px 0">
    </p>
    <div runat="server" id="RegisteredEmpList" style="position: relative; float: left;
        width: 100%; max-height: 400px; overflow: auto;">
    </div>
</div>
</form>
