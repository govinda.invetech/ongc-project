﻿using System;
using System.Linq;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class sendpassapproverequest : System.Web.UI.Page
    {
        /// last Modified on 23/01/2014 by shiv
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("FALSE~ERR~Session Expired ! Please Login Again.");
                return;
            }
            else
            {
                string RequesitionHint = Request["RequesitionHint"].ToString();
                string reqname = Request["RequesitionerName"].ToString();
                string reqcpf = Request["RequesitionerCPF"].ToString();
                string reqdesignation = Request["RequesitionerDesig"].ToString();
                string reqlocation = Request["RequesitionerLocation"].ToString();
                string visitorentrylocation = Request["visitorentrylocation"].ToString();
                string WhomToMeet = Request["WhomToMeet"].ToString();
                string Mtng_Officer_Desg = Request["WhomToMeetDesig"].ToString();
                string Mtng_Officer_CPF = Request["WhomToMeetCPF"].ToString();
                string Mtng_Officer_Lctn = Request["MeetingLocations"].ToString();
                string reqextentionno = Request["RequesitionerExtn"].ToString();
                // string reasonofvisit = Request["purposeofvisit"].ToString();
                string RequisitionType = Request["RequisitionType"].ToString();
                string purposeofvisit = Request["reasonofvisit"].ToString();
                string PassApproveAuthority_cpf_no = Request["PassApproveAuthority_cpf_no"].ToString();
                string PassApproveAuthority_name = Request["PassApproveAuthority_name"].ToString();
                string visitor_full_list = Request["VisitorFullList"].ToString();
                string visitor_start_date_time = Request["visitor_start_date_time"].ToString();
                string reasonofvisit = purposeofvisit;
                string visitor_end_date_time = Request["visitor_end_date_time"].ToString();
                try
                {
                    if (visitorentrylocation == "")
                    {
                        Response.Write("FALSE~ERR~Please Select Valid Visitor's Entry Location");
                        return;
                    }
                    else if (visitor_start_date_time == "")
                    {
                        Response.Write("FALSE~ERR~Please select Valid  Visitor's Entry Date Time");
                        return;
                    }
                    else if (visitor_end_date_time == "")
                    {
                        Response.Write("FALSE~ERR~Please select  Pass Valid till Date time");
                        return;
                    }
                    else if (WhomToMeet == "")
                    {
                        Response.Write("FALSE~ERR~Please fillvalid meeting officer");
                        return;
                    }
                    else if (PassApproveAuthority_name == "")
                    {
                        Response.Write("FALSE~ERR~Please select  valid Pass Approving Authority");
                        return;
                    }
                    else if (PassApproveAuthority_cpf_no == "")
                    {
                        Response.Write("FALSE~ERR~Please select  valid Pass Approving Authority");
                        return;
                    }
                    else
                    {
                        /*--Modification data time format for sql--*/
                        //System.Convert.ToDateTime(My.ConvertDateStringintoSQLDateString(visitor_end_date_time));

                        DateTime dtStartDate = My.ConvertDateStringInDateTime(visitor_start_date_time);
                        DateTime dEndDate = My.ConvertDateStringInDateTime(visitor_end_date_time);

                        //if (dtStartDate < System.DateTime.Today || dEndDate < System.DateTime.Today)
                        //{
                        //    Response.Write("FALSE~ERR~Entry Date and Valid untill can't be previous date.");
                        //    return;
                        //}

                        if (RequesitionHint == "NEW")
                        {
                            visitor_full_list = visitor_full_list.Substring(0, visitor_full_list.Length - 1);
                            if (validatedata(visitor_full_list) == "FALSE")
                            {
                                Response.Write("FALSE~ERR~~Invalid Visitor Data");
                                return;
                            }
                            int max_group_no = 0;
                            max_group_no = getmaxgroupno();
                            Mtng_Officer_Lctn = Mtng_Officer_Lctn.Substring(0, Mtng_Officer_Lctn.Length - 5);
                            string[] visitor_list_arr = visitor_full_list.Split('~');
                            string insQuery = "INSERT INTO [O_VSTR_GATE_PASS_RQSTN_DTL] ([O_APLCNT_NM],[O_APLCNT_CPF_NMBR],[O_APLCNT_DESG],[O_APLCNT_LCTN],[O_APLCNT_EXT_NMBR],[O_REQSTN_TYPE],[O_ENTRY_DT],[O_EXIT_DT],[O_ENTRY_AREA],[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG],[CY_O_MTNG_OFF_CPF],[O_MTNG_OFF_LCTN],[O_VISTR_NM],[O_VISTR_ORGNZTN],[O_VISTR_AGE],[O_VISTR_GNDR],[O_RMRKS],[O_PRPS_OF_VISIT],[O_ACP1_NM],[O_ACP1_CPF_NMBR],[O_SYS_DT_TM],[CY_GROUP],[CY_WITH_TOOL]) VALUES";
                            for (int i = 0; i < visitor_list_arr.Count(); i++)
                            {
                                string[] visitor_sub_data = visitor_list_arr[i].Split('`');
                                string v_name = visitor_sub_data[1];
                                string v_cmpny = visitor_sub_data[5];
                                string v_age = visitor_sub_data[3];
                                string v_gender = visitor_sub_data[7];
                                string v_purpose = visitor_sub_data[9];
                                string CY_WITH_TOOL = visitor_sub_data[11];
                                insQuery += "('" + reqname + "','" + reqcpf + "','" + reqdesignation + "','" + reqlocation + "','" + reqextentionno + "','" + RequisitionType + "','" + My.ConvertDateStringintoSQLDateString(visitor_start_date_time) + "','" + My.ConvertDateStringintoSQLDateString(visitor_end_date_time) + "','" + visitorentrylocation + "','" + WhomToMeet + "','" + Mtng_Officer_Desg + "','" + Mtng_Officer_CPF + "','" + Mtng_Officer_Lctn + "','" + v_name.ToUpper() + "','" + v_cmpny.ToUpper() + "','" + v_age + "','" + v_gender + "','" + v_purpose + "','" + purposeofvisit + "','" + PassApproveAuthority_name + "','" + PassApproveAuthority_cpf_no + "',GETDATE(),'" + max_group_no + "','" + CY_WITH_TOOL + "'),";

                            }
                            insQuery = insQuery.Substring(0, insQuery.Length - 1);
                            if (My.ExecuteSQLQuery(insQuery) == true)
                            {
                                Response.Write("TRUE~SUCCESS~Request sent successfully");
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *VISPASS328");
                            }
                        }
                        else
                        {
                            if (My.sDataStringTrim(1, RequesitionHint) == "UPDATE")
                            {
                                /*-------if Request is Pending[P] then previous data will be deleted by  below command so that  previous data must be Approved[P] or Rejected [R] -----O_REQ_COMPLTN_FLG-----*/
                                string sDelQuery = "DELETE FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [O_PASS_TYPE_FLG] = 'V'  AND [O_REQ_COMPLTN_FLG] = 'A' AND [CY_GROUP]= '" + My.sDataStringTrim(2, RequesitionHint) + "'";
                                if (My.ExecuteSQLQuery(sDelQuery) == true)
                                {
                                    if (visitor_full_list != "")
                                    {
                                        visitor_full_list = visitor_full_list.Substring(0, visitor_full_list.Length - 1);
                                        string[] visitor_list_arr = visitor_full_list.Split('~');
                                        string insQuery = "INSERT INTO [O_VSTR_GATE_PASS_RQSTN_DTL] ([O_APLCNT_NM],[O_APLCNT_CPF_NMBR],[O_APLCNT_DESG],[O_APLCNT_LCTN],[O_APLCNT_EXT_NMBR],[O_REQSTN_TYPE],[O_ENTRY_DT],[O_EXIT_DT],[O_ENTRY_AREA],[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG],[CY_O_MTNG_OFF_CPF],[O_MTNG_OFF_LCTN],[O_VISTR_NM],[O_VISTR_ORGNZTN],[O_VISTR_AGE],[O_VISTR_GNDR],[O_RMRKS],[O_PRPS_OF_VISIT],[O_ACP1_NM],[O_ACP1_CPF_NMBR],[O_SYS_DT_TM],[CY_GROUP],[CY_WITH_TOOL]) VALUES";
                                        for (int i = 0; i < visitor_list_arr.Count(); i++)
                                        {
                                            string[] visitor_sub_data = visitor_list_arr[i].Split('`');
                                            string v_name = visitor_sub_data[1];
                                            string v_cmpny = visitor_sub_data[5];
                                            string v_age = visitor_sub_data[3];
                                            string v_gender = visitor_sub_data[7];
                                            string v_purpose = visitor_sub_data[9];
                                            string CY_WITH_TOOL = visitor_sub_data[11];
                                            insQuery = insQuery + "('" + reqname + "','" + reqcpf + "','" + reqdesignation + "','" + reqlocation + "','" + reqextentionno + "','" + RequisitionType + "','" + My.ConvertDateStringintoSQLDateString(visitor_start_date_time) + "','" + My.ConvertDateStringintoSQLDateString(visitor_end_date_time) + "','" + visitorentrylocation + "','" + WhomToMeet + "','" + Mtng_Officer_Desg + "','" + Mtng_Officer_CPF + "','" + Mtng_Officer_Lctn + "','" + v_name.ToUpper() + "','" + v_cmpny.ToUpper() + "','" + v_age + "','" + v_gender + "','" + v_purpose + "','" + v_purpose + "','" + PassApproveAuthority_name + "','" + PassApproveAuthority_cpf_no + "',GETDATE(),'" + My.sDataStringTrim(2, RequesitionHint) + "','" + CY_WITH_TOOL + "'),";
                                        }
                                        insQuery = insQuery.Substring(0, insQuery.Length - 1);
                                        if (My.ExecuteSQLQuery(insQuery) == true)
                                        {
                                            string sQueryUpdate = "UPDATE [O_VSTR_GATE_PASS_RQSTN_DTL] SET [O_SYS_DT_TM] = GETDATE() WHERE [CY_GROUP] = '" + My.sDataStringTrim(2, RequesitionHint) + "'";
                                            if (My.ExecuteSQLQuery(sQueryUpdate) == true)
                                            {
                                                Response.Write("TRUE~SUCCESS~Request sent successfully");
                                            }
                                            else
                                            {
                                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *VISPASS328");
                                            }
                                        }
                                        else
                                        {
                                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *VISPASS328");
                                        }
                                    }
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *VISPASS328");
                                }
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *VISPASS328");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
            }
        }
        private string validatedata(string visitor_list)
        {
            string[] visitor_list_arr = visitor_list.Split('~');
            for (int i = 0; i < visitor_list_arr.Count(); i++)
            {
                string[] visitor_sub_data = visitor_list_arr[i].Split('`');
                if (visitor_sub_data[0] == "name" && visitor_sub_data[1] != "" && visitor_sub_data[2] == "age" && visitor_sub_data[3] != "" && visitor_sub_data[4] == "cmny_name" && visitor_sub_data[5] != "" && visitor_sub_data[6] == "gender" && visitor_sub_data[7] != "" && visitor_sub_data[9] != "" && visitor_sub_data[10] == "tools")
                {
                }
                else
                {
                    return "FALSE";
                }

            }
            return "TRUE";
        }
        // delete group no 
        private string deletegroupno(string groupno, string cpf_no)
        {
            string sStr = "DELETE from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_APLCNT_NM = '" + cpf_no + "' and O_PASS_TYPE_FLG = 'V' and O_REQ_COMPLTN_FLG = 'A' and CY_GROUP= '" + groupno + "'";
            if (My.ExecuteSQLQuery(sStr) == true)
            {
                return "TRUE";
            }
            else
            {
                return "FLASE";
            }
        }

        // get max group no 

        private int getmaxgroupno()
        {
            string sStr = "SELECT  MAX([CY_GROUP]) FROM [O_VSTR_GATE_PASS_RQSTN_DTL]";
            DataSet ds = My.ExecuteSELECTQuery(sStr);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return 1;
            }
            else
            {
                if (ds.Tables[0].Rows[0][0].ToString() == "")
                {
                    return 1;
                }
                else
                {
                    String aa = ds.Tables[0].Rows[0][0].ToString();
                    int a = int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1;
                    return a;
                }
            }
        }

    }
}