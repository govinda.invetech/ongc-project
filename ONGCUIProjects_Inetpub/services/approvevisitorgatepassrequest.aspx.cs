﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class approvevisitorgatepassrequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cpf_no = "";
            string cy_group = Request.QueryString["cy_group"];
            string index_no = Request.QueryString["index_no"];
            string type = Request.QueryString["type"];
            if (cy_group == "")
            {
                Response.Write("FALSE~ERR~Invalid GROUP");
                return;
            }
            if (index_no == "")
            {
                Response.Write("FALSE~ERR~Invalid index_no");
                return;
            }
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            else
            {
                cpf_no = Session["Login_Name"].ToString();
            }
            index_no = index_no.Substring(0, index_no.Length - 1);
            string[] index_no_arr = index_no.Split('`');
            for (int i = 0; i < index_no_arr.Length; i++)
            {
                if (checkcpfno_cybuzz(cy_group, index_no_arr[i]) == "FALSE")
                {
                    Response.Write("FALSE~SUCCESS~Invalid Group or Index No");
                    return;
                }
                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                SqlConnection conn = new SqlConnection(strConn);
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                string sStr = "";
                if (type == "APPROVE")
                {
                    sStr = "update [O_VSTR_GATE_PASS_RQSTN_DTL] set O_REQ_COMPLTN_FLG = 'P' where O_PASS_TYPE_FLG ='V' and O_REQ_COMPLTN_FLG = 'A' and O_ACP1_CPF_NMBR = '" + cpf_no + "' and CY_GROUP='" + cy_group + "'  and O_INDX_NMBR='" + index_no_arr[i] + "'";
                }
                else if (type == "REJECT")
                {
                    sStr = "update [O_VSTR_GATE_PASS_RQSTN_DTL] set O_REQ_COMPLTN_FLG = 'R' where O_PASS_TYPE_FLG ='V' and O_REQ_COMPLTN_FLG = 'A' and O_ACP1_CPF_NMBR = '" + cpf_no + "' and CY_GROUP='" + cy_group + "' and O_INDX_NMBR='" + index_no_arr[i] + "'";
                }
                SqlCommand cmd = new SqlCommand(sStr);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            if (type == "APPROVE")
            {
                Response.Write("TRUE~SUCCESS~Visitor Pass Requisition Approved");
            }
            else
            {
                Response.Write("TRUE~SUCCESS~Visitor Pass Requisition Rejected");
            }
        }
        // validate index and group

        private string checkcpfno_cybuzz(string group_no, string index_no)
        {
            string sCPF = Request.QueryString["cpf"];
            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [O_INDX_NMBR] ,[CY_GROUP] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] where O_INDX_NMBR='" + index_no + "' and CY_GROUP='" + group_no + "'";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }

        }
    }
}