﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services
{
    public partial class UpdateCompanyName : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["Login_Name"] == null)
                {
                    Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                    return;
                }

                string sPreviousName = Request["previousname"].ToString();
                string sNewName = Request["newname"].ToString();

                string sQuery = "UPDATE [O_VSTR_GATE_PASS_RQSTN_DTL] SET [O_VISTR_ORGNZTN] = '" + sNewName + "' WHERE [O_VISTR_ORGNZTN] = '" + sPreviousName + "'";

                if (My.ExecuteSQLQuery(sQuery) == true)
                {
                    Response.Write("TRUE~SUCCESS~Updated successfully");
                }
                else
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *Error Code : 1852");
                }
            }
            catch (Exception ex)
            {

                Response.Write("FALSE~ERR~" + ex.Message);
            }

        }
    }
}