﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class Notification : System.Web.UI.Page
    {
        string Departments = "";
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] != null)
            {
                string sCPFNo = Session["Login_Name"].ToString();
                string query = "";
                query = "select [child_id] from [CY_MENU_EMP_RELATION] where [cpf_number]='" + sCPFNo + "' and child_id in('62','63','64','65','67','77','99','98')";
                DataSet ds = My.ExecuteSELECTQuery(query);
                if(ds!=null && ds.Tables[0].Rows.Count>0)
                {

                    if (ds.Tables[0].Rows[0]["child_id"].ToString() == "62")
                    {
                        Departments = "TELEPHONE";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "63")
                    {
                        Departments = "ELECTRICAL(AC)";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "64")
                    {
                        Departments = "CIVIL";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "77")
                    {
                        Departments = "ELECTRICAL(MAINTENANCE)";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "65")
                    {
                        Departments = "HOUSEKEEPING";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "67")
                    {
                        Departments = "OTHER";
                    }

                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "99")
                    {
                        Departments = "WalkieTalkie";
                    }

                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "98")
                    {
                        Departments = "PAPaging";
                    }
                    else
                    {
                        Departments = "";
                    }
                     
                }
                
                string sNotification = My.getNotificationHtml(sCPFNo,Departments);
                Response.Write("TRUE~SUCCESS~" + sNotification);
            }
        }
    }
}