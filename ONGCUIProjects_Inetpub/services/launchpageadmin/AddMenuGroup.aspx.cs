﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.launchpageadmin
{
    public partial class AddMenuGroup : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                try
                {
                    string sCPFNo = Session["Login_Name"].ToString();

                    string sActionTypeData = Request["ActionType"].ToString();
                    string sMnuGrpTitle = Request["MnuGrpTitle"].ToString();
                    string sMnuGrpPriority = Request["MnuGrpPriority"].ToString();
                    string sMenuPosition = Request["MenuPosition"].ToString();

                    string sInsertQuery = "";
                    string sInsertUpdateMessage = "";

                    string sActionType = My.sDataStringTrim(1, sActionTypeData);

                    if (sActionType == "CREATE")
                    {
                        sInsertQuery = "INSERT INTO [CY_LP_MENU_GROUP]([GROUP_NAME],[PRIORITY],[POSITION],[ENTRY_BY],[TIMESTAMP])";
                        sInsertQuery += "VALUES (UPPER('" + sMnuGrpTitle.Replace("'", "''") + "'),'" + sMnuGrpPriority + "','" + sMenuPosition + "','" + sCPFNo + "',GETDATE())";
                        
                        sInsertUpdateMessage = "TRUE~SUCCESS~Menu Group Created Successfully";
                    }
                    else if (sActionType == "UPDATE")
                    {
                        sInsertQuery = "UPDATE [CY_LP_MENU_GROUP] SET [GROUP_NAME] = '" + sMnuGrpTitle.Replace("'", "''") + "',[POSITION]= '" + sMenuPosition + "',[PRIORITY]= '" + sMnuGrpPriority + "', [ENTRY_BY] ='" + sCPFNo + "', [TIMESTAMP] = GETDATE()  WHERE [ID] = '" + My.sDataStringTrim(2, sActionTypeData) + "'";
                        sInsertUpdateMessage = "TRUE~SUCCESS~Menu Group Updated Successfully";
                    }


                    if (My.ExecuteSQLQuery(sInsertQuery) == true)
                    {
                        if (My.ExecuteSQLQuery("UPDATE [CY_LP_MENU_GROUP] SET [PRIORITY]=[PRIORITY]+1 WHERE [PRIORITY] >'" + sMnuGrpPriority + "' AND [POSITION] = '" + sMenuPosition + "'") == true)
                        {
                            Response.Write(sInsertUpdateMessage);
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MNUGRP189");
                        }
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MNUGRP189");
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
            }
        }

    }
}