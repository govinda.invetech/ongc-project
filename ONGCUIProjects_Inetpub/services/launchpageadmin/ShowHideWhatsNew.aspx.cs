﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.quiz
{
    public partial class ShowHideWhatsNew : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                try
                {
                    string sActionType = Request.Form["action"].ToString();
                    string sWhatsNewID = Request.Form["id"].ToString();

                    string sUpdQuery = "UPDATE [CY_LP_WHATS_NEW] SET [LP_STATUS] = '" + sActionType.ToUpper() + "' WHERE [ID] = '" + sWhatsNewID + "'";

                    if (My.ExecuteSQLQuery(sUpdQuery) == true)
                    {
                        Response.Write("TRUE~SUCCESS~Deleted Successfully");
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *LPDEL188");
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
            }
        }
    }
}