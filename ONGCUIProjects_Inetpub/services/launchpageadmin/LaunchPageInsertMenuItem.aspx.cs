﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects.services
{
    public partial class LaunchPageInsertMenuItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
           
            string sMenuTitle = Request.QueryString["menu_title"];
            string sToolTip = Request.QueryString["tooltip"];
            string sHref = Request.QueryString["href"];
            string sMenuType = Request.QueryString["menu_type"];
            int iOrderBy = System.Convert.ToInt32(Request.QueryString["order_by"]);
            string sIsImp = Request.QueryString["is_important"];
            string cpf_no = Session["Login_Name"].ToString();
     
            if (cpf_no  == "")
            {
                Response.Write("FALSE~ERR~Invalid CPF Number loggined");
                return;
            }



            ONGCUIProjects.MyApplication1 My= new ONGCUIProjects.MyApplication1();
            Boolean bResult=false ;
        try
            {
             string sUpdate ="UPDATE [cy_launch_page_menu] SET [order_by]=[order_by]+1";
                sUpdate = sUpdate + " WHERE [order_by]>" + (iOrderBy - 1) + " AND [menu_type] ='" + sMenuType + "';";

                if (My.ExecuteSQLQuery(sUpdate) == true)
                {
                    string sInsert = "INSERT into [cy_launch_page_menu] ([menu_title],[tooltip],[href],[menu_type],[order_by],[is_important],[entry_by])  VALUES ";
                    sInsert = sInsert + "('" + sMenuTitle + "','" + sToolTip + "','" + sHref + "','" + sMenuType + "','" + iOrderBy + "','" + sIsImp + "','" + cpf_no + "')";
                    if (My.ExecuteSQLQuery(sInsert) == true)
                    {
                        Response.Write("TRUE~SUCCESS~Menu item saved successfully...!");
                    }
                }
                else //else case of execute update query
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *LP187");
                }
            }
            catch(Exception a )
                {
               Response.Write("FALSE~ERR~" + a.Message );
            } //  try catch ends

        } //page load
    } //class 
} // prj namespace class 