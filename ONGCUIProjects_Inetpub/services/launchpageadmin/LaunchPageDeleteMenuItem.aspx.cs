﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;


namespace ONGCUIProjects.services
{
    public partial class LaunchPageDeleteMenuItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }

            int menu_id =System.Convert.ToInt32( Request.QueryString["id"]);
            int iOrder =System.Convert.ToInt32(Request.QueryString["order_by"]);        
            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            string str = "SELECT [link_type] ,[href] FROM [cy_launch_page_menu] where [id]='" + menu_id + "'";
            DataSet ds = My.ExecuteSELECTQuery(str);
            if (ds.Tables[0].Rows.Count > 0) {
                if (ds.Tables[0].Rows[0][0].ToString() == "DOWNLOAD")
                {
                    string fullpath = Server.MapPath("~/uploadedlink/" + menu_id);
                    DirectoryInfo fipth = new DirectoryInfo(fullpath);
                    if (fipth.Exists)
                    {
                        Directory.Delete(fullpath, true);
                    }
                }
            }
           
            
            try{
                if (My.DeleteMenuItemofLAunchPage(menu_id, iOrder) == true)
        {
            Response.Write ("TRUE~SUCCESS~Menu Item deleted successfully");
        }
        else
        {
             Response.Write ("FALSE~ERR~Some internal error");
        }
            }
            catch(Exception ex)
            {
             Response.Write ("FALSE~ERR~" + ex.Message );
            }
        }
    }
}