﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

namespace ONGCUIProjects.services.launchpageadmin
{
    public partial class DeleteMenuOrWhatsNew : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sSettingType = Request.Form["setting"].ToString();
                string sDelID = Request.Form["delid"].ToString();
                string sDelQuery = "";
                string sDelDirQuery = "";
                string sUpdQuery = "";
                string sIsDwnld = "";
                try
                {
                    if (sSettingType == "MENU")
                    {
                        sDelQuery = "DELETE FROM [CY_LP_MENU_ITEM] WHERE [ID] = '" + sDelID + "'";

                        sDelDirQuery = "SELECT [GROUP_ID],[IS_DWNL],[ORDER_BY] FROM [CY_LP_MENU_ITEM] WHERE [ID] = '" + sDelID + "'";

                        DataSet dsDelMenu = My.ExecuteSELECTQuery(sDelDirQuery);
                        string sGroupID = dsDelMenu.Tables[0].Rows[0][0].ToString();
                        sIsDwnld = dsDelMenu.Tables[0].Rows[0][1].ToString();
                        int iOrder = Convert.ToInt32(dsDelMenu.Tables[0].Rows[0][2].ToString());

                        sUpdQuery = "UPDATE [CY_LP_MENU_ITEM] SET [ORDER_BY]=[ORDER_BY]-1 WHERE [ORDER_BY] >'" + iOrder + "' AND [GROUP_ID] = '" + sGroupID + "'";
                    }
                    else if (sSettingType == "WHATSNEW")
                    {
                        sDelQuery = "DELETE FROM [CY_LP_WHATS_NEW] WHERE [ID] = '" + sDelID + "'";
                        sDelDirQuery = "SELECT [IS_DWNL],[ORDER_BY] FROM [CY_LP_WHATS_NEW] WHERE [ID] = '" + sDelID + "'";
                        DataSet dsDelMenu = My.ExecuteSELECTQuery(sDelDirQuery);
                        sIsDwnld = dsDelMenu.Tables[0].Rows[0][0].ToString();
                        int iOrder = Convert.ToInt32(dsDelMenu.Tables[0].Rows[0][1].ToString());
                        sUpdQuery = "UPDATE [CY_LP_WHATS_NEW] SET [ORDER_BY]=[ORDER_BY]-1 WHERE [ORDER_BY] >'" + iOrder + "'";

                    }
                    else if (sSettingType == "PLANTHEAD")
                    {
                        sDelQuery = "DELETE FROM [CY_PLANT_HEAD_PAGE] WHERE [ID] = '" + sDelID + "'";
                        sDelDirQuery = "SELECT [IS_DWNL],[ORDER_BY] FROM [CY_PLANT_HEAD_PAGE] WHERE [ID] = '" + sDelID + "'";
                        DataSet dsDelMenu = My.ExecuteSELECTQuery(sDelDirQuery);
                        sIsDwnld = dsDelMenu.Tables[0].Rows[0][0].ToString();
                        int iOrder = Convert.ToInt32(dsDelMenu.Tables[0].Rows[0][1].ToString());
                        sUpdQuery = "UPDATE [CY_PLANT_HEAD_PAGE] SET [ORDER_BY]=[ORDER_BY]-1 WHERE [ORDER_BY] >'" + iOrder + "'";

                    }

                    if (My.ExecuteSQLQuery(sDelQuery) == true)
                    {
                        if (My.ExecuteSQLQuery(sUpdQuery) == true)
                        {

                            Response.Write("TRUE~SUCCESS~Deleted Successfully");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *LPDEL188");
                        }
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *LPDEL188");
                    }

                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
                if (sIsDwnld == "DOWNLOAD")
                {
                    string fullpath = Server.MapPath("~/uploadedlink/" + sDelID);
                    DirectoryInfo fipth = new DirectoryInfo(fullpath);
                    if (fipth.Exists)
                    {
                        Directory.Delete(fullpath, true);
                    }
                }
            }
        }
    }
}