﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.services.launchpageadmin
{
    public partial class AddUpdTOD : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                try
                {
                    //"type=" + ActionType + "&date=" + DispDate + "&todtext=" + TODText + "&id" + ID,
                    string sActionType = Request.Form["type"].ToString();
                    
                    if (sActionType == "EDIT")
                    {
                        string sSessionCPF = Session["Login_Name"].ToString();
                        string sThoughtText = Request.Form["todtext"].ToString();
                        string sDate = Request.Form["date"].ToString();
                        string sID = Request.Form["id"].ToString();

                        string dtDispDate = My.ConvertDateStringintoSQLDateString(sDate);

                        string str = "UPDATE [CY_LP_TOD] SET [publish_date] = '" + dtDispDate + "',[thought_text] = '" + sThoughtText.Replace("'","''") + "',[entry_by] = '" + sSessionCPF + "' WHERE [id] = '" + sID + "';";

                        if (My.ExecuteSQLQuery(str) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Updated successfully");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~Some internal error");
                        }
                    }
                    else if (sActionType == "ADD")
                    {
                        string sSessionCPF = Session["Login_Name"].ToString();
                        string sThoughtText = Request.Form["todtext"].ToString();
                        string sDate = Request.Form["date"].ToString();

                        string dtDispDate = My.ConvertDateStringintoSQLDateString(sDate);

                        string str = "INSERT INTO [CY_LP_TOD] ([publish_date],[thought_text],[entry_by])";
                        str = str + "VALUES ('" + dtDispDate + "','" + sThoughtText.Replace("'", "''") + "','" + sSessionCPF + "'); ";

                        if (My.ExecuteSQLQuery(str) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Saved successfully");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~Some internal error");
                        }
                    }
                    else if (sActionType == "DEL")
                    {
                        string sID = Request.Form["id"].ToString();

                        string str = "DELETE FROM [CY_LP_TOD] WHERE [id] = '" + sID + "';";

                        if (My.ExecuteSQLQuery(str) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Deleted successfully");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~Some internal error");
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    Response.Write("FALSE~ERR~" + ex.Message);
                }
            }
        }
    }
}