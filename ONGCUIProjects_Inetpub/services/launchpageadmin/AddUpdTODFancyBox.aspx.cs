﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.services.launchpageadmin
{
    public partial class AddUpdTODFancyBox : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sSessionCPF = Session["Login_Name"].ToString();
                if (Request.QueryString["ID"] == null)
                {
                    lblTODActionType.Text = "ADD";
                    txtTODDispDate.Text = "";
                    txtTODText.Text = "";
                }
                else
                {
                    string sTODID=Request.QueryString["ID"].ToString();
                    lblTODActionType.Text = "EDIT";
                    string sQuery = "SELECT [id],[publish_date],[thought_text] FROM [CY_LP_TOD] WHERE [id] = '" + sTODID + "'";
                    DataSet dsTOD = My.ExecuteSELECTQuery(sQuery);
                    DateTime dtDispDate = Convert.ToDateTime(dsTOD.Tables[0].Rows[0][1].ToString());
                    lblTODID.Text = sTODID;
                    txtTODDispDate.Text = dtDispDate.ToString("dd-MM-yyyy");
                    txtTODText.Text = dsTOD.Tables[0].Rows[0][2].ToString();
                    

                }
            }
        }
    }
}