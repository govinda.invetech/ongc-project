﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class UserLoginDetails : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            int iExpiryPeriod = 60;
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string cpf_no = Session["Login_Name"].ToString();
                DateTime dt2 = DateTime.Today;
                DateTime dtTodayDate = new DateTime(dt2.Year, dt2.Month, dt2.Day);
                #region Pending Request
                // all data connected to view CY_EMP_STATUS by shiv shakti on 31-03-2015
                string varname1 = "";
                varname1 = varname1 + "SELECT   [O_CPF_NMBR],[O_EMP_NM],[O_EMP_DESGNTN],[LOGIN_COUNT],[TIMESTAMP] " + "\n";
                varname1 = varname1 + ",[CY_STATUS] FROM [ONGC].[dbo].[CY_EMP_STATUS] " + "\n";              
                if (Request["type"] == null || Request["type"] == "ALL")
                varname1 = varname1 + "";
                else
                varname1 = varname1 + "WHERE [CY_STATUS] = '" + Request["type"] + "' " + "\n";
                varname1 = varname1 + "ORDER BY [O_EMP_NM] ASC";
                //--------------------------------------------------------------
                //if (Request["type"] == null)
                //{
                //    sQuery = "SELECT [O_CPF_NMBR],UPPER(LTRIM(RTRIM([O_EMP_NM]))) AS [EMP_NAME],UPPER([O_EMP_DESGNTN]) AS [DESIG] ,[LOGIN_COUNT],[TIMESTAMP],[E_USER_MSTR].[CY_STATUS] FROM [O_EMP_MSTR] ";
                //    sQuery += "INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE] ";
                //    sQuery += "LEFT JOIN (SELECT [USER_ID],COUNT([USER_ID]) AS [LOGIN_COUNT], MAX([TIMESTAMP]) AS [TIMESTAMP] ";
                //    sQuery += "FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID]) [USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] ORDER BY [EMP_NAME] ASC";//[EMP_NAME]
                //}
                //else
                //{
                //    string sType = Request["type"];
                //    ddlUserType.SelectedValue = sType;
                //    sQuery = "SELECT [O_CPF_NMBR],UPPER(LTRIM(RTRIM([O_EMP_NM]))) AS [EMP_NAME],UPPER([O_EMP_DESGNTN]) AS [DESIG] ,[LOGIN_COUNT],[TIMESTAMP],[E_USER_MSTR].[CY_STATUS] FROM [O_EMP_MSTR] ";
                //    sQuery += "INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE] ";
                //    sQuery += "LEFT JOIN (SELECT [USER_ID],COUNT([USER_ID]) AS [LOGIN_COUNT], MAX([TIMESTAMP]) AS [TIMESTAMP] ";
                //    sQuery += "FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID]) [USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] ";
                //    if (sType != "ALL")
                //    {
                //        sQuery += "WHERE [E_USER_MSTR].[CY_STATUS] = '" + sType + "'";
                //    }
                //    sQuery += "ORDER BY [EMP_NAME] ASC";//[EMP_NAME]
                //}
                DataSet dsUserDetails = My.ExecuteSELECTQuery(varname1);
                if (dsUserDetails.Tables[0].Rows.Count != 0)
                {
                    string output = "";
                    output += "<table border=\"1\" class=\"tftable\" id=\"tfhover\">";
                    output += "<tbody>";
                    output += "<tr>";
                    output += "<th style='text-align:center;'>Employee Name</th>";
                    output += "<th style='text-align:center; width:50px;'>CPF No.</th>";
                    output += "<th style='text-align:center;'>Designation</th>";
                    output += "<th style='text-align:center; width:120px;'>Last Login</th>";
                    output += "<th style='text-align:center; width:40px;'>Total Login</th>";
                    output += "<th style='text-align:center; width:60px;'>Edit Profile</th>";
                    output += "<th style='text-align:center; width:60px;'>Set Privilege</th>";
                    output += "<th style='text-align:center; width:60px;'>Reset Password</th>";
                    output += "<th style='text-align:center; width:60px;'>Enable/ Disable</th>";                   
                    output += "<th style='text-align:center; width:60px;'>Delete User</th>";
                    output += "<th style='text-align:center; width:60px;'>Deactive</th>";
                    output += "<th style='text-align:center; width:60px;'>Active</th>";
                   
                    output += "</tr>";
                    for (int i = 0; i < dsUserDetails.Tables[0].Rows.Count; i++)
                    {
                        string sCPFNo = dsUserDetails.Tables[0].Rows[i][0].ToString();
                        string sUserName = dsUserDetails.Tables[0].Rows[i][1].ToString();
                        string sUserDesig = dsUserDetails.Tables[0].Rows[i][2].ToString();
                        string sLoginCount = dsUserDetails.Tables[0].Rows[i][3].ToString();
                        string sLastLogin = dsUserDetails.Tables[0].Rows[i][4].ToString();
                        string sStatus = dsUserDetails.Tables[0].Rows[i][5].ToString();
                        
                        if (sLoginCount == "")
                        {
                            sLoginCount = "<a title='View Details' class='btn-login-dtls' href='javascript:void(0);'>0</a>";
                            sLastLogin = "Not Logged In Yet";
                        }
                        else
                        {
                            sLoginCount = "<a title='View Details' class='btn-login-dtls cmd_view_details' href='ViewAllLogin.aspx?CPF=" + sCPFNo + "'>" + sLoginCount + "</a>";
                            sLastLogin = System.Convert.ToDateTime(sLastLogin).ToString("dd-MM-yyyy HH:mm:ss");
                            #region Check User Last Login Exceed
                            DateTime dt1 = System.Convert.ToDateTime(dsUserDetails.Tables[0].Rows[i][4]);
                            DateTime dtLastLogin = new DateTime(dt1.Year, dt1.Month, dt1.Day);
                            TimeSpan t = dtTodayDate - dtLastLogin;
                            int iDaysDiff = System.Convert.ToInt32(t.TotalDays);

                            if (iDaysDiff > iExpiryPeriod && sStatus != "REQUEST")  //  85>90 (no) 
                            {
                                sStatus = "ADMINDEACTIVE";
                            }
                            #endregion
                        }
                        string sActiveDeactiveTRClass = "";
                        if (sStatus == "ACTIVE" || sStatus == "ADMINACTIVE")
                        {
                            sActiveDeactiveTRClass = "style='background-color:rgba(52, 187, 0, .85); color:white; font-weight:bold;'";
                        }
                        else if (sStatus == "REQUEST")
                        {
                            sActiveDeactiveTRClass = "style='background-color:orange; color:white; font-weight:bold;'";
                        }
                        else
                        {
                            sActiveDeactiveTRClass = "style='background-color:rgba(255, 60, 60, .85); color:white; font-weight:bold;'";
                        }
                        output += "<tr " + sActiveDeactiveTRClass + " forsearch=\"." + sCPFNo.ToUpper() + sUserName.ToUpper() + "\">";
                        output += "<td>" + sUserName + "</td>";
                        output += "<td>" + sCPFNo + "</td>";
                        output += "<td>" + sUserDesig + "</td>";
                        output += "<td style='text-align:center;'>" + sLastLogin + "</td>";
                        output += "<td style='text-align:center;'>" + sLoginCount + "</td>";
                        output += "<td style='text-align:center;'><a class='btn-edit-profile cmdEditEmpProfile' title='Edit Profile' href='services/EditUserProfileFancyBox.aspx?CPFNo=" + sCPFNo + "'></a></td>";
                        output += "<td style='text-align:center;'><a class='btn-set-privilege cmdSetPrivilege' title='Set Privilege' href='SetPrivilege.aspx?cpf_no=" + sCPFNo + "'></a></td>";
                        output += "<td style='text-align:center;'><a class='btn-reset-pwd cmdResetPwd' title='Reset Password' CurrentCPFNo='" + sCPFNo + "' href='javascript:void(0);'></a></td>";
                        output += "<td style='text-align:center;'>";
                        switch (sStatus)
                        {
                            case "ACTIVE":
                                output += "<a href=\"javascript:void(0);\" user=\"" + sCPFNo + "\" title='Disable Account' class=\"btn-disable-account cmdAction\" ActionType='ADMINDEACTIVE'></a>";
                                break;
                            case "ADMINACTIVE":
                                output += "<a href=\"javascript:void(0);\" user=\"" + sCPFNo + "\" title='Disable Account' class=\"btn-disable-account cmdAction\" ActionType='ADMINDEACTIVE'></a>";
                                break;
                            case "REQUEST":
                                output += "<a href=\"javascript:void(0);\" user=\"" + sCPFNo + "\" title='Accept Request' class=\"btn-accept-request cmdAction\" ActionType='ADMINACTIVE'></a>";
                                output += "<a href=\"javascript:void(0);\" user=\"" + sCPFNo + "\" title='Reject Request' class=\"btn-reject-request cmdAction\" ActionType='REJECT'></a>";
                                //output += "<input type=\"button\" user=\"" + sCPFNo + "\" class=\"g-button g-button-share cmdAction\" value=\"ACTIVATE\" ActionType='ADMINACTIVE' />";
                                //output += "<input type=\"button\" user=\"" + sCPFNo + "\" class=\"g-button g-button-red cmdAction\" style=\"width:80px;\" value=\"REJECT\" ActionType=\"REJECT\" />";
                                break;
                            case "REJECT":
                                output += "<a href=\"javascript:void(0);\" user=\"" + sCPFNo + "\" title='Disable Account' class=\"btn-disable-account cmdAction\" ActionType='ADMINDEACTIVE'></a>";
                                //output += "Already Rejected";
                                break;
                            case "ADMINDEACTIVE":
                                output += "<a href=\"javascript:void(0);\" user=\"" + sCPFNo + "\" title='Enable Account' class=\"btn-enable-account cmdAction\" ActionType='ADMINACTIVE'></a>";
                                break;
                            
                           
                        }
                        output += "</td>";
                        output += "<td style='text-align:center;'><a class='btn-delete-user cmdDeleteUser' title='Delete User' CurrentCPFNo='" + sCPFNo + "' href='javascript:void(0);'></a></td>";                    
                    
                        output += "<td style='text-align:center;'><a class='btn-enable-account cmdActiveUser' title='Deactive User' CurrentCPFNo='" + sCPFNo + "' href='javascript:void(0);'></a></td>";
                        output += "<td style='text-align:center;'><a class='btn-disable-account cmdDeactiveUser' title='Active User' CurrentCPFNo='" + sCPFNo + "' href='javascript:void(0);'></a></td>";
                        output += "</tr>";

                    }
                    output += "</tbody>";
                    output += "</table>";
                    table_data.InnerHtml = output;
                }
                #endregion
            }
        }
    }
}