﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransportBookingHistory.aspx.cs"
    Inherits="ONGCUIProjects.TransportBookingHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/script.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <!-- JS Links Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $(".cmdViewBooking").fancybox();
        });
    </script>
    <style type="text/css">
        .tooltip
        {
            color: #000000;
            outline: none;
            cursor: default;
            text-decoration: none;
            position: relative;
        }
        .tooltip span
        {
            margin-left: -999em;
            position: absolute;
        }
        .tooltip:hover span
        {
            border-radius: 5px 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            font-family: Calibri, Tahoma, Geneva, sans-serif;
            position: absolute;
            left: 1em;
            top: 1.5em;
            z-index: 99;
            margin-left: 0;
            width: 250px;
            color: White;
        }
        .info
        {
            background: black;
            padding: 0.3em 0.3em 0.3em 0.3em;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                Transport Booking History</h1>
            <a href="TransportBookingAddEdit.aspx" style="float: right; margin-right: 10px; margin-top: 8px;"
                class="g-button g-button-red">Request New Booking</a>
        </div>
        <div class="div-pagebottom">
        </div>
        <p runat="server" id="P1" class="content" style="color: red; font-size: 20px; text-align: center;">
        </p>
        <div runat="server" class="div-fullwidth" id="pending_detail_div" style="margin-top: 10px;">
        </div>
    </div>
    </form>
</body>
</html>
