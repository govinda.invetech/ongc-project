﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="ONGCUIProjects.Feedback" %>
<style type="text/css">
	.createlink-container{
	position: relative; 
	float:left;
	width: 400px;
	padding: 10px;
	}
.createlink-heading{
	position: relative;
	float: left;
	width: 100%;
	color: #58595A; 
	font-size: 22px; 
	font-family: MyriadPro-Regular; 
	font-weight: normal; 
	line-height: 40px; 
	text-align: justify; 
	border-bottom: 2px solid #58595A;
	}
.createlink-divfullwidth{
	position: relative;
	float: left;
	width: 100%;
	margin-top: 10px;
	}
.createlink-text{
	position: relative;
	float: left;
	color: #58595A; 
	font-size: 16px; 
	font-family: MyriadPro-Regular; 
	font-weight: normal;
	margin: 4px 10px 0 0;
	}
.createlink-inputbox{
	display: inline-block;
	height: 29px;
	margin: 0;
	padding-left: 8px;
	background: white;
	border: 1px solid #D9D9D9;
	border-top: 1px solid silver;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	-webkit-border-radius: 1px;
	-moz-border-radius: 1px;
	border-radius: 1px;
		}
.comboContainer {
	position: relative;
	float: left;
	width: 200px;
	margin-top: -8px;
}
.droparea {
	position: absolute;
	width: 200px;
	background-color: #F9F9F9;
	border: 1px solid #CCC;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	border-radius: 2px;
	margin-top: 42px;
	display: none;
	z-index: 1;
}
.droparea .position {
	position: relative;
	float: left;
	color: gray;
	text-decoration: none;
	border-bottom: 1px solid #F0F0F0;
	min-width: 190px;
	height: 20px;
	background-color: #F9F9F9;
	padding-left: 10px;
	padding-top: 5px;
	color: #4C4C4C;
}
.droparea .position:hover {
	position: relative;
	float: left;
	color: gray;
	text-decoration: none;
	border-bottom: 1px solid #F0F0F0;
	min-width: 190px;
	height: 20px;
	background-color: gainsboro;
	padding-left: 10px;
	padding-top: 5px;
	color: #4C4C4C;
}
.combobox{
	position: relative; 
	float: left; 
	display: block;
	width:200px; 
	height:30px;
	background-color: #f9f9f9; 
	border: 1px solid #ccc; 
	-moz-border-radius: 2px; 
	-webkit-border-radius: 2px; 
	border-radius: 2px; 
	margin-right:13px;
	margin-bottom:10px;
	text-decoration:none;
	margin-top:10px;
	}
.combobox:hover {
	position: relative;
	float: left;
	display: block;
	width: 200px;
	height: 30px;
	background-color: #F9F9F9;
	border: 1px solid gray;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	border-radius: 2px;
	box-shadow: 0 1px 1px silver;
	margin-right: 13px;
	margin-bottom: 10px;
	text-decoration: none;
	color: #4C4C4C;
	margin-top: 10px;
}
.content-ComboBox {
	position: relative;
	float: left;
	width: 90%;
	margin-left: 10px;
	height: 20px;
	margin: 0px;
	margin-top: 5px;
	margin-left: 5px;
	color: gray;
	text-decoration: none;
}
.arrowDown {
	position: absolute;
	right: 0px;
	background-image: url('../images/arrowDown2.png');
	background-repeat: no-repeat;
	width: 11px;
	height: 11px;
	opacity: 0.5;
	margin-top: 10px;
	margin-right: 5px;
}
    
</style>

<form id="form1" runat="server">
<div class="createlink-container">
<h1 class="createlink-heading">Write feedback for new portal</h1>
    <div class="createlink-divfullwidth">
    	<h1 class="createlink-text" style="width: 88px;">Comment</h1>
    	<textarea class="createlink-inputbox" style="margin-bottom:10px; font-family: Calibri; padding:3px; max-width: 300px; min-width: 300px; max-height: 150px; min-height: 150px; width: 284px; height: 153px;" id="txtComment" style=" width: 300px;" ></textarea>
    </div>

    <div class="createlink-divfullwidth" style="margin-top: 0px; border-bottom: 2px solid rgb(88, 89, 90); margin-bottom: 10px;">
    	<h1 style="width: 88px; margin-bottom: 5px;" class="createlink-text">Rate :</h1>
        <div style="width: auto; margin-top: 7px; margin-right: 28px; cursor: pointer;" class="createlink-divfullwidth">
        	<input id="1" type="radio" style="position: relative; float:left; margin-right: 10px;" name="Rate" value="1">
            <label for="1" class="createlink-text" style="margin-top: -4px; margin-right: 0; color: #3684D1;">1</label>
        </div>
        <div style="width: auto; margin-top: 7px; margin-right: 28px; cursor: pointer;" class="createlink-divfullwidth">
        	<input id="2" type="radio" style="position: relative; float:left; margin-right: 10px;" name="Rate" value="2">
            <label for="2" class="createlink-text" style="margin-top: -4px; margin-right: 0; color: #3684D1;">2</label>
        </div>
        <div style="width: auto; margin-top: 7px; margin-right: 28px; cursor: pointer;" class="createlink-divfullwidth">
        	<input id="3" type="radio" style="position: relative; float:left; margin-right: 10px;" name="Rate" value="3">
            <label for="3" class="createlink-text" style="margin-top: -4px; margin-right: 0; color: #3684D1;">3</label>
        </div>
        <div style="width: auto; margin-top: 7px; margin-right: 28px; cursor: pointer;" class="createlink-divfullwidth">
        	<input id="4" type="radio" style="position: relative; float:left; margin-right: 10px;" name="Rate" value="4">
            <label for="4" class="createlink-text" style="margin-top: -4px; margin-right: 0; color: #3684D1;">4</label>
        </div>
        <div style="width: auto; margin-top: 7px; margin-right: 28px; cursor: pointer;" class="createlink-divfullwidth">
        	<input id="5" type="radio" style="position: relative; float:left; margin-right: 10px;" name="Rate" value="5">
            <label for="5" class="createlink-text" style="margin-top: -4px; margin-right: 0; color: #3684D1;">5</label>
        </div>
     </div>
     <div class="createlink-divfullwidth" style="margin-top:0px;">
     <label class="createlink-text" style="margin-top: -4px; margin-right: 0; color: #3684D1;">(5-Best)</label>
    	<input type="button" class="g-button g-button-red" style=" float: right; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size: 14px; text-transform: capitalize; cursor: pointer;" onclick="$.fancybox.close();" value="Cancel"/>
      <input id="cmdSaveFeedBack" type="button" class="g-button g-button-submit" style="float: right; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size: 14px; margin-right: 10px; cursor: pointer;" value="Save"/>
      
    </div>
      

</div>
</form>
