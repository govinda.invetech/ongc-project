using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ONGCCustumEntity;
using ONGCBusinessProjects;

namespace ONGCUIProjects
{
	/// <summary>
	/// Summary description for Update_Emp_Master.
	/// </summary>
	public partial class Update_Emp_Master : System.Web.UI.Page
	{
		#region variables
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		string EString;
		string EmpCount;
		#endregion

		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BtnSaveEmpData.Attributes.Add("onclick","return ConfirmDelete()");
			if(!IsPostBack)
			{
				BindEmployee();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Get Employee Name 
		private void BindEmployee()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select O_CPF_NMBR,O_EMP_NM,O_EMP_DESGNTN, O_EMP_HM_ADDR1, O_EMP_HM_ADDR2, O_EMP_HM_CITY, O_EMP_HM_STATE, O_EMP_HM_PIN_CD, O_EMP_HM_PHN, O_EMP_MBL_NMBR, O_EMP_GNDR, O_EMP_DESGNTN, O_EMP_DOB, O_EMP_SUP_NM, O_EMP_DEPT_NM, O_EMP_WRK_LCTN, O_EMP_BLD_GRP, O_SYS_DT_TM, O_USER_CODE, O_BIT_CD, O_UPDATE_BIT, O_LEVEL, O_REP, O_REV, O_MGR, O_ACP_1, O_ACP_2, O_EMAIL_ID1, O_EMAIL_ID2, O_EMAIL_ID3 from O_EMP_MSTR where O_CPF_NMBR='"+Session["Login_Name"].ToString()+"'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				if(sqlDataReader.Read())
				{
					txtEmpName.Text = sqlDataReader["O_EMP_NM"].ToString();
					txtEmpCPFNo.Text = sqlDataReader["O_CPF_NMBR"].ToString();
					txtAddr1.Text = sqlDataReader["O_EMP_HM_ADDR1"].ToString();
					txtAddr2.Text = sqlDataReader["O_EMP_HM_ADDR2"].ToString();
					txtCity.Text = sqlDataReader["O_EMP_HM_CITY"].ToString();
					txtState.Text = sqlDataReader["O_EMP_HM_STATE"].ToString();
					txtPinCode.Text = sqlDataReader["O_EMP_HM_PIN_CD"].ToString();
					txtHomeNo.Text = sqlDataReader["O_EMP_HM_PHN"].ToString();
					txtMblNo.Text = sqlDataReader["O_EMP_MBL_NMBR"].ToString();
					txtDept.Text = sqlDataReader["O_EMP_DEPT_NM"].ToString();
					txtEmail1.Text = sqlDataReader["O_EMAIL_ID1"].ToString();
					txtEmail2.Text = sqlDataReader["O_EMAIL_ID2"].ToString();
					txtEmail3.Text = sqlDataReader["O_EMAIL_ID3"].ToString();			
				}
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Button CLick Refresh
		protected void BtnRefresh_Click(object sender, System.EventArgs e)
		{
			BindEmployee();
		}
		#endregion

		#region Check Email ID Exists
		private void CheckEmail()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select count(*) as Count_Rec from O_EMP_MSTR where O_EMAIL_ID1 = '' and O_EMAIL_ID2 = '' and O_EMAIL_ID3 = '' and O_CPF_NMBR='"+Session["Login_Name"].ToString()+"'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				if(sqlDataReader.Read())
				{
					EmpCount = sqlDataReader["Count_Rec"].ToString();
				}
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Button Click Save Emp Data
		protected void BtnSaveEmpData_Click(object sender, System.EventArgs e)
		{
			SqlConnection sqlConnection=new SqlConnection(strConn);
			sqlConnection.Open();
			SqlCommand sqlCommand=new SqlCommand("update O_EMP_MSTR set O_UPDATE_BIT='1', O_BIT_CD ='0', O_EMP_HM_ADDR1 = '" +txtAddr1.Text+ "', O_EMP_HM_ADDR2 = '" +txtAddr2.Text+"', O_EMP_HM_CITY = '" +txtCity.Text+"', O_EMP_HM_STATE = '" +txtState.Text+"', O_EMP_HM_PIN_CD = '" + txtPinCode.Text + "', O_EMP_HM_PHN = '" + txtHomeNo.Text + "', O_EMP_MBL_NMBR = '" + txtMblNo.Text + "', O_EMP_DEPT_NM = '" + txtDept.Text + "', O_EMAIL_ID1 = '" + txtEmail1.Text + "', O_EMAIL_ID2 = '" + txtEmail2.Text + "', O_EMAIL_ID3 = '" + txtEmail3.Text + "' where O_CPF_NMBR='"+txtEmpCPFNo.Text+"'",sqlConnection);
			sqlCommand.ExecuteNonQuery();
			sqlConnection.Close();

			CheckEmail();

			//if(EmpCount == "0")
			//{
				Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
			//}
			//else
			//{
				//
			//}
		}
		#endregion
	}
}
