﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="quiz_admin.aspx.cs" Inherits="ONGCUIProjects.quiz_admin" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Cache-Control" content="no-cache" /> 
    <title>Quiz Setting</title>
		<!-- CSS Links Starts -->
<link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
<link href="css/css3.css" type="text/css" rel="stylesheet" />
<link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
<!-- CSS Links Ends -->

<!-- JS Links Starts -->       
   <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
        <script type="text/javascript" language="javascript" src="js/script.js"></script>
        <script type="text/javascript" language="javascript" src="js/slider.js"></script>

        <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen">
       
        <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen">
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>  
       
        
       <%-- <script type="text/javascript" language="javascript" src="js/DateTimePicker.js"></script>--%>
        <script type="text/javascript" language="javascript" src="js/radiobutton.js"></script>
<!-- JS Links Starts -->      
		
		<script language="javascript">

		    $(document).ready(function () {

          
		        $("#lblQueType").hide();
		        var que_type = $("#lblQueType").text();
		        $('input[name="sample-radio"]').change(function () {
		            window.location = "quiz_admin.aspx?type=" + $(this).val();
		        });
		        $("#add_que").attr("href", "services/quiz/add_edit_fancybox.aspx?type=ADD&que_type=" + que_type);


		        //alert('input[name="que_type"][val="' + type + '"]');

		        $('input[name="sample-radio"][value="' + que_type + '"]').prop('checked', true);

		        getQuestionList(que_type);

		        function getQuestionList(type) {
		            $.ajax({
		                type: "GET",
		                data: "type=" + type,
		                url: "services/quiz/que_list.aspx",
		                success: function (msg) {
		                    var msg_arr = msg.split('~');
		                    if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {

		                        $("#no_of_que").html(msg_arr[3]);
		                        $('#que_container').html(msg_arr[4]);
		                        $.gritter.add({
		                            title: "Notification",
		                            image: "images/tick.png",
		                            text: msg_arr[2]
		                        });
		                        $(".cmd_edit").fancybox({
		                            modal: true,
		                            padding: 0
		                        });
		                        $(".cmdResultAnalysis").fancybox();
                                $(".click_to_edit").fancybox({
                                modal:true,
                                padding:0
                                });
		                    } else {
		                        $('#que_container').html(msg_arr[4]);
		                    }
		                }
		            });
		        }



		        var my_hint = "";
		        $("#cmd_fill_opt").live("click", function () {
		            my_hint = $('input[name=my_hint]:checked').val();
		            if (my_hint == undefined) {
		                $("#require_notification").text("Please select question type.");
		            } else {
		                var no_opt = $("#txt_no_opt").val();
		                $("#opt_div").html("");
		                if (no_opt != '') {
		                    for (var i = 0; i < no_opt; i++) {
		                        var opt_div_child = "<div class=\"for_opt_select\" style=\"position: relative; float: left; width: 50%; margin-top: 3px; margin-bottom: 3px;\">";
		                        opt_div_child = opt_div_child + "<h1 class=\"content\" style=\"width: 100%; margin: 0 0 5px 5px;\">Option : " + (i + 1) + "</h1><textarea  style=\"font-weight: bold; position: relative; float: left; margin-bottom: 0px; margin-top: 0px; margin-left: 5px; width: 83%;\" option_id=\"\"></textarea>";
		                        if (my_hint == "QUIZ") {
		                            opt_div_child = opt_div_child + "<input type=\"radio\" name=\"opt_true\" style=\"position:relative;float:left; border: 3px solid green; width: 19px; height: 19px; margin: 18px 0 0 10px;\">";
		                        }
		                        opt_div_child = opt_div_child + "</div>";
		                        $("#opt_div").append(opt_div_child);
		                    }
		                }
		            }
		        });



		        $("#cdm_post_que").live("click", function () {
                var que_id="";
                var type=$(this).val();
                if(type=="UPDATE"){
                que_id=$(this).attr('que_id');
                }
		            var txt_que = $("#txt_que").val().trim();
		            var str = "";

		            $.each($(".for_opt_select"), function (index, value) {
		                var opt_text = $(this).children('textarea').val().trim();
                        var opt_id=$(this).children('textarea').attr('option_id');
		                if (opt_text != '') {
		                    var opt_true_false;
		                    if ($(this).children('input[name=opt_true]:checked').val()) {
		                        opt_true_false = "TRUE";
		                    } else {
		                        opt_true_false = "FALSE";
		                    }
		                    str = str + encodeURIComponent(opt_text) + '`' + opt_true_false +'`'+opt_id+'`';
		                }
		            });
		            var option_selected = "";
		            if ($('input[name=opt_true]:checked').val()) {
		                option_selected = "TRUE";
		            } else {
		                option_selected = "FALSE";
		            }


		            str = str.substring(0, str.length - 1);
		            var start_date = $("#start_date").val().trim();
		            var end_date = $("#end_date").val().trim();
		            if (txt_que == '') {
		                $("#require_notification").text("Please fill question.");
		            } else if (str == '') {
		                $("#require_notification").text("Please fill options.");
		            } else if (start_date == '') {
		                $("#require_notification").text("Please fill start date.");
		            } else if (end_date == '') {
		                $("#require_notification").text("Please fill end date.");
		            } else {
		                if (my_hint == "QUIZ" && option_selected == "FALSE") {
		                    $("#require_notification").text("Please select correct option.");
		                } else {
		                    var data = "type="+type+"&que_id="+que_id+"&str=" + encodeURIComponent(txt_que) + "~FALSE~DEPT~" + start_date + "~" + end_date + "~YES~App_by~2012/12/17~" + my_hint + "~" + str;
		                    $(this).attr("disabled", true);
		                    $('#cmd_que_post_cancel').attr("disabled", true);
		                    $(this).val('Saving...');
		                    $.ajax({
		                        type: "GET",
		                        data: data,
		                        url: "services/quiz/add_update_que.aspx",
		                        success: function (msg) {

		                            var msg_arr = msg.split('~');
		                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
		                                alert("Question posted successfully.");
		                                $.fancybox.close();
		                                window.location = self.location;
		                            } else {
		                                alert("Problem in execution, Please contact Administrator.");
		                            }
		                            $('#cdm_post_que').attr("disabled", false);
		                            $('#cmd_que_post_cancel').attr("disabled", false);
		                            $('#cdm_post_que').val(type);
		                        }
		                    });
		                }
		            }

		        });

		        $(".cmd_delete").live("click", function () {

		            if (confirm("All details related to this question will also be deleted.\n\nAre you sure to delete this question?")) {

		                var que_id = $(this).attr("que_id");

		                $.ajax({
		                    type: "GET",
		                    data: "que_id=" + que_id,
		                    url: "services/quiz/DeleteQuestion.aspx",
		                    success: function (msg) {

		                        var msg_arr = msg.split('~');
		                        if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
		                            $.gritter.add({
		                                title: "Notification",
		                                image: "images/tick.png",
		                                text: msg_arr[2]
		                            });
		                            window.location = self.location;
		                        } else {
		                            alert("Problem in execution, Please contact Administrator.");
		                        }
		                    }
		                });

		            }
		            //window.location = self.location;
		        });

		        $("#add_que").fancybox({
		            modal: true,
		            padding: 0
		        });
		        $("#AddEditPreamble").fancybox({
		            modal: true,
		        });

                $("#cmdSavePreamble").live("click",function(){

                var sPreambleText=$("#txtPreambleText").val().trim();
                if(sPreambleText==""){
                $("#response").text("Please enter text");
                }else{
                $.ajax({
		                    type: "POST",
		                    data: "PreambleText=" + sPreambleText,
		                    url: "services/quiz/SavePreamble.aspx",
		                    success: function (msg) {

		                        var msg_arr = msg.split('~');
		                        if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
		                            $("#response").text(msg_arr[2]);
		                        } else {
		                            alert(msg_arr[2]);
		                        }
		                    }
		                });
              
                }
                });

		    });

        </script>
        <style type="text/css">
            .has-js .label_check,
            .has-js .label_radio { padding-left: 24px; font-family: MyriadPro-Regular; margin-right: 10px; cursor: pointer; font-weight: bold; color: #2E2E2E; font-size: 17px;}
            .has-js .label_radio { background: url(Images/documentstyles/style2/radio-off.png) no-repeat; }
            .has-js label.r_on { background: url(Images/documentstyles/style2/radio-on.png) no-repeat; }
            .has-js .label_check input,
            .has-js .label_radio input { position: absolute; left: -9999px; }
            
            .strip-questiondates{position: relative; float: left; width: 101%; margin: -10px -10px 5px -10px; background-color: #1D8D22; padding: 4px 5px 0px 5px;}
            .questiondates{position: relative; float: left; font-family: Lucida Sans Unicode, Lucida Grande, sans-serif; color: white; font-size: 11px; margin-bottom: 0px; margin-top: 0px; margin-left: 5px;}
            .questionstatusbox{position: absolute;right: 20px;top: 3px;margin-right: 10px; font-size: 12px; color: white;border: 1px solid white;padding: 0px 10px 2px 10px;font-family: Lucida Sans Unicode, Lucida Grande, sans-serif; border-radius: 10px; font-weight: bold;}
            .questiontype
            {
                position: relative; 
                float: left; 
                width: 100px; 
                transform: rotate(-90deg);
                -ms-transform: rotate(-90deg); /* IE 9 */
                -webkit-transform: rotate(-90deg); /* Safari and Chrome */
                -o-transform: rotate(-90deg); /* Opera */
                -moz-transform: rotate(-90deg); /* Firefox */
                text-align: center;
                font-family: Lucida Sans Unicode, Lucida Grande, sans-serif; 
                color: white; 
                font-size: 12px;
                background-color: Green;
                padding: 5px;
                }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class=" div-fullwidth iframeMainDiv">
            <div class="style2-page-container">
                <div class=" style2-pagetop">
                    <h1 class="heading" style=" width: auto; color: White; margin-top: 5px; font-family: Lucida Sans Unicode, Lucida Grande, sans-serif; text-shadow: 1px 1px 2px #161616;">Pulse of URAN Question Management</h1>
                    <a  id="add_que" href="services/quiz/add_edit_fancybox.aspx?type=ADD" class="style2-add_icon">Add New Question</a>
                    <a  id="AddEditPreamble" href="#preamble" class="style2-add_icon" style="background: none;color: white;">Add/Edit Preamble</a>
                </div>
                <div class="style2-pagemiddle">
                <div class="style2-page-controls">
                    <div style="position:relative;float:right; margin: 11px 10px 0 0;">
                        <p style="font-family: MyriadPro-Regular; font-weight: bold; color: #0D8F2C; font-size: 20px; margin-top: -3px;">Total <strong id="no_of_que">0</strong> questions posted till now.</p>
                    </div>
                    <div style="position:relative;float:left; margin: 11px 0 0 10px;">
                        <label class="label_radio" for="all">
                            <input name="sample-radio" id="all" value="ALL" type="radio" />
                            All Question
                        </label>
                        <label class="label_radio" for="survey">
                            <input name="sample-radio" id="survey" value="SURVEY" type="radio" />
                            Survey Question
                        </label>
                        <label class="label_radio" for="quiz">
                            <input name="sample-radio" id="quiz" value="QUIZ" type="radio" />
                            Quiz Question
                        </label>
                    </div>
                </div>
                <div class="style2-pagecut"></div>
    
                    <div id="que_container" style="position:relative;float:left;width:100%;">

                    </div>
                    <asp:Label ID="lblQueType" runat="server"></asp:Label>
                </div>
                <div class="style2-pagebottom-container">
                    <div class="bottomleft"></div>
                    <div class="bottommiddle"></div>
                    <div class="bottomright"></div>
                </div>
            </div>
        </div>
        <div style="display:none;">
        <div id="preamble" style="position:relative;float:left; width:600px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">Add/Edit Preamble</h1>
        <div class="div-fullwidth marginbottom">
            <p class="content">Preamble Description</p>
            <asp:TextBox ID="txtPreambleText" runat="server" TextMode="MultiLine" style="min-width: 100%; max-width: 100%; min-height:250px; max-height:250px;"></asp:TextBox>
        </div>
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
        <div class="div-fullwidth">
            <p id="response" class="content" style="width: auto; color:Red;"></p>
            <input type="button" id="cmdReset" class="g-button g-button-red" value="CLOSE" onclick="window.location=self.location; $.fancybox.close();" style="float:right;" />
            <input type="button" id="cmdSavePreamble" class="g-button g-button-submit" value="SAVE" style="margin-right:10px; margin-bottom:0px"/>
        </div>
    </div>
        </div>
    </form>
   
    
   
</body>
</html>
