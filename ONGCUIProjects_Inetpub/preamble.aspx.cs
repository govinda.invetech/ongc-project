﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class preamble : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }

            string sQuery = "SELECT [IMG_PATH], [PREAMBLE_TEXT] FROM [CY_ABT_PULSE_OF_URAN]";
            DataSet dsPreamble = My.ExecuteSELECTQuery(sQuery);
            if (dsPreamble.Tables[0].Rows.Count == 0)
            {

            }
            else
            {
                string sImgPath=dsPreamble.Tables[0].Rows[0][0].ToString();
                preamble_image.Attributes.Add("src", sImgPath);
                preamble_text.Text = dsPreamble.Tables[0].Rows[0][1].ToString();
            }
        }
    }
}