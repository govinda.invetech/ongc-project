#region NameSpaces
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ONGCCustumEntity;
using ONGCBusinessProjects;
using System.Data.SqlClient;
#endregion

namespace ONGCUIProjects
{
	public partial class CreateUser : System.Web.UI.Page
	{

		#region variables
	    string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		#endregion

		#region PageLoad
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(!IsPostBack)
			{			
				fillUserRole();
				Page.DataBind(); 
				ddlUserName.Items.Insert(0,"Select User");
				ddlRole.Items.Insert(0,"Select Role"); 
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Button Save Click
		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			ONGCBusinessProjects.CerateUser objBusCreateUser = new ONGCBusinessProjects.CerateUser();
			ONGCCustumEntity.CerateUser objCusCreateUser	 = new ONGCCustumEntity.CerateUser();
          
			objCusCreateUser.UserCode = txtUserID.Text.ToString();
			objCusCreateUser.UserName = ddlUserName.SelectedItem.Text.ToString();
			objCusCreateUser.UserPassword = txtPwd.Text.ToString();
			objCusCreateUser.UserDescription = txtDesc.Text.ToString();
			objCusCreateUser.RoleID = ddlRole.SelectedValue.ToString();
			
			if (objCusCreateUser.UserName == "Select User")
			{
				Response.Write("<Script language='JavaScript'>"+"alert('Please Select User...')"+"</Script>");
			}
			else if(objCusCreateUser.RoleID == "Select Role")
			{
				Response.Write("<Script language='JavaScript'>"+"alert('Please Select Role...')"+"</Script>");
			}
			else
			{
				bool chkUser = objBusCreateUser.CreateUser(objCusCreateUser); 

				if (chkUser == true )
				{				
					Response.Write("<Script language='JavaScript'>"+"alert('User Successfully Created...')"+"</Script>");
					Referesh();		
				}
				else
				{
					Response.Write("<Script language='JavaScript'>"+"alert('User Already Exist...')"+"</Script>");
				}
			}
		}
		#endregion

		#region Button Cancle Click
		protected void btnCancle_Click(object sender, System.EventArgs e)
		{
			Referesh();	
		}

		#endregion

		#region fillUserRole
		private void fillUserRole()
		{
			DataSet dsUserRole;
			ONGCBusinessProjects.CerateUser objBusCreateUsers = new ONGCBusinessProjects.CerateUser();
			dsUserRole = (DataSet)objBusCreateUsers.FillUserAndRole(); //returning dataset. 
								
			ddlUserName.DataSource = dsUserRole;
			ddlUserName.DataMember =  dsUserRole.Tables[0].ToString();
			ddlUserName.DataValueField = dsUserRole.Tables[0].Columns["O_CPF_NMBR"].ToString();
			ddlUserName.DataTextField =  dsUserRole.Tables[0].Columns["O_EMP_NM"].ToString();
			ddlUserName.DataBind();	
			ddlUserName.Items.Insert(0,"Select User");
			//ddlRole.ClearSelection();		
			ddlRole.DataSource = dsUserRole;
			ddlRole.DataMember= dsUserRole.Tables[1].ToString();
			ddlRole.DataValueField = dsUserRole.Tables[1].Columns["P_ROLE_ID"].ToString();
			ddlRole.DataTextField =  dsUserRole.Tables[1].Columns["P_ROLE_DSCRPTN"].ToString();
			ddlRole.DataBind();	
			ddlRole.Items.Insert(0,"Select Role"); 
		}
		#endregion		

		#region Referesh()
		private void Referesh()
		{
			ddlUserName.SelectedItem.Text = "Select User";
			ddlRole.SelectedItem.Text = "Select Role";
			txtUserID.Text = "";
			txtPwd.Text ="";
			txtDesc.Text = "";
		}
		#endregion
	}
}
