﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="complaintReg.aspx.cs" Inherits="ONGCUIProjects.complaintReg" %>

<!DOCTYPE html>
<html>
<head>
    <title>VisitorPass_Approval1</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script src="js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script src="js/jquery.callout.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/our.js"></script>
    <link rel="stylesheet" href="css/diggy.css?ver=4.5.3.3" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/jquery.callout.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="style/privilage.css" />
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/wave.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />


    <script type="text/javascript" language="javascript">
        $(document).ready(function () {


            //		        		        $(document).ajaxStop($.unblockUI);
            //		        		        getstrip("PENDING");
            //		        		        $("#ddlstrip_type").change(function () {
            //		        		            var my_abc = $(this).val();
            //		        		            getstrip(my_abc);
            //		        		        });

            //		        		        //function 
            //		        		        function getstrip(strip_type) {
            //		        		            $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
            //		        		            $.ajax({
            //		        		                type: "GET",
            //		        		                url: "services/complain/getusercomlainlist.aspx",
            //		        		                data: "strip_type=" + strip_type,
            //		        		                success: function (msg) {
            //		        		                    //alert(msg);
            //		        		                    var values = msg.split("~");

            //		        		                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
            //		        		                        $("#request_detail_div").html(values[3]);
            //		        		                        $(".view_cmpln_dtl_fancy").fancybox({
            //		                                        modal:true
            //		                                        });
            //		        		                        $("#theory_text").text(values[2]);
            //		        		                    } else if (values[0] == "FALSE" && values[1] == "ERR") {

            //		        		                        $.gritter.add({
            //		        		                            title: "Notification",
            //		        		                            image: "images/cross.png",
            //		        		                            text: values[2]
            //		        		                        });
            //		        		                        $("#request_detail_div").empty();
            //		        		                        $("#theory_text").text(values[2]);
            //		        		                    }

            //		        		                }
            //		        		            });
            //		        		        }
            //		        		        //end function


        });
    </script>

</head>

<body>
    <form id="Form1" method="post" runat="server" style="margin-right: 10px;">
        <div class="div-fullwidth iframeMainDiv" style="width: 99%;">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;">Complaint Registration</h1>

                <a href="Complaint_Registration.aspx?my_hint=NEW`NEW" id="register_new_complaint" class="g-button g-button-red" type="button" style="float: right; margin-top: 5px; margin-right: 5px; text-decoration: none; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;">Register New Complaint</a>
            </div>
            <div class="div-pagebottom"></div>
            <%--
            <h1 class="gatepass-content" style="margin-top: 2px;">Select  Type</h1>
            <asp:DropDownList ID="ddlstrip_type" runat="server" Width="130px">
                 
                 <asp:ListItem Selected="True">PENDING</asp:ListItem>
                 <asp:ListItem>SOLVED</asp:ListItem>                 
            </asp:DropDownList>
            --%>
            <p id="P1" class="gatepass-content" style="font-size: 12px; font-family: Arial; margin-top: 10px; display: none"></p>
            <!-- Strip Container Starts ------------------------------------------------------------------------------------------->
          
            <div class="div-fullwidth" id="request_detail_div" runat="server" style="margin-top: 10px; overflow:scroll; height:300px;">
                <!-- Visitor Gate Pass Strip Starts ----------------------------------------------------------------------------------->
                <asp:GridView ID="grdComplaintRegistration" runat="server"
                    AutoGenerateColumns="False"
                    OnRowDataBound="grdComplaintRegistration_RowDataBound" BackColor="LightGoldenrodYellow"
                    BorderColor="Tan" BorderWidth="1px" CellPadding="2"
                    GridLines="None" Font-Names="Calibri" Font-Size="Medium" ForeColor="Black">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:TemplateField HeaderText="S No." Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%#Eval("ID")%>' CssClass="" ToolTip="S No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applicant">
                            <ItemTemplate>
                                <div style="width:150px;"><asp:Label ID="Label2" runat="server" Text='<%#Eval("APP_NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation ">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("APP_DESIG")%>' CssClass="" ToolTip="Designation"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone Extension No.">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("APP_PHONE_EX_NO")%>' CssClass="" ToolTip="Phone Extension No."></asp:Label>
                            </ItemTemplate>
                            <%--    SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[] ,[APP_MOBILE_NO] ,[],[],[COMPLAIN_TYPE_ID] ,[] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[] ,[] --%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("APP_LOCATION")%>' CssClass="" ToolTip="Location"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Problem">
                            <ItemTemplate>
                            <div style="width:110px;"><asp:Label ID="Label6" runat="server" Text='<%#Eval("PROBLEM_DISCRIPTION")%>' CssClass="" ToolTip="Problem"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room No.">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("ROOM_NO")%>' CssClass="" ToolTip="Room No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Type">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("COMPLAIN_DEPARTMENT")%>' CssClass="" ToolTip="Complain Type"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Date">
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd/MM/yyyy}")%>' CssClass="" ToolTip="Complain Date"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblComplaintStatus" runat="server" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="View/Edit">
                            <ItemTemplate>
                                <asp:HyperLink ID="hylEditComplaints" runat="server" class='view_cmpln_dtl_fancy' my_i_no='<%#Eval("ID")%>'>View/Edit</asp:HyperLink>
                                <%--  <asp:HyperLink ID="hylEditComplaints" runat="server" NavigateUrl='<%# "Complaint_Registration.aspx?my_hint=UPDATE`" + Eval("ID") %>'  class='view_cmpln_dtl_fancy' my_i_no='<%#Eval("ID")%>'  >View/Edit</asp:HyperLink>  --%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"
                        BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <RowStyle Wrap="True" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
                <!-- Visitor Gate Pass Strip Ends ------------------------------------------------------------------------------------->

            </div>


            <div class="div-fullwidth" id="Div1" runat="server" style="margin-top: 10px; overflow:scroll; height:300px;">
                <!-- Visitor Gate Pass Strip Starts ----------------------------------------------------------------------------------->
                <asp:GridView ID="GridView1" runat="server"
                    AutoGenerateColumns="False"
                    OnRowDataBound="GridView1_RowDataBound" BackColor="LightGoldenrodYellow"
                    BorderColor="Tan" BorderWidth="1px" CellPadding="2"
                    GridLines="None" Font-Names="Calibri" Font-Size="Medium" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" Width="1200px" ForeColor="Black">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:TemplateField HeaderText="S No." Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%#Eval("ID")%>' CssClass="" ToolTip="S No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applicant">
                            <ItemTemplate>
                             <div style="width:140px;"><asp:Label ID="Label2" runat="server" Text='<%#Eval("APP_NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation ">
                            <ItemTemplate>
                                <div><asp:Label ID="Label3" runat="server" Text='<%#Eval("APP_DESIG")%>' CssClass="" ToolTip="Designation"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone Extension No.">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("APP_PHONE_EX_NO")%>' CssClass="" ToolTip="Phone Extension No."></asp:Label>
                            </ItemTemplate>
                            <%--    SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[] ,[APP_MOBILE_NO] ,[],[],[COMPLAIN_TYPE_ID] ,[] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[] ,[] --%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <div><asp:Label ID="Label5" runat="server" Text='<%#Eval("APP_LOCATION")%>' CssClass="" ToolTip="Location"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Problem">
                            <ItemTemplate>
                                <div><asp:Label ID="Label6" runat="server" Text='<%#Eval("PROBLEM_DISCRIPTION")%>' CssClass="" ToolTip="Problem"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room No.">
                            <ItemTemplate>
                                <div><asp:Label ID="Label7" runat="server" Text='<%#Eval("ROOM_NO")%>' CssClass="" ToolTip="Room No."></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Type">
                            <ItemTemplate>
                                <div><asp:Label ID="Label8" runat="server" Text='<%#Eval("COMPLAIN_DEPARTMENT")%>' CssClass="" ToolTip="Complain Type"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Date">
                            <ItemTemplate>
                                <div style="width:100px;"><asp:Label ID="Label9" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd-MM-yyyy}")%>' CssClass="" ToolTip="Complain Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <div><asp:Label ID="lblComplaintStatus" runat="server" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status"></asp:Label></div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                 <asp:Label ID="lblComplaintStatus" runat="server" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status" Visible="false"></asp:Label>
                                <asp:DropDownList ID="ddl_status" runat="server"></asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Status Date">
                            <ItemTemplate>
                                <div style="width:75px;"><asp:Label ID="Label22" runat="server"  Text='<%#Eval("VIEW_TIMESTAMP")%>' CssClass="" ToolTip="Complain_Status Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <%--<asp:CommandField ShowEditButton="true" HeaderText="ACTION" />--%>

                    </Columns>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"
                        BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <RowStyle Wrap="True" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
                <!-- Visitor Gate Pass Strip Ends ------------------------------------------------------------------------------------->

            </div>



           
             <!-- Strip Container Ends --------------------------------------------------------------------------------------------->
         
                 <div class="div-fullwidth" id="request_detail_div1" runat="server" style="margin-top: 10px;  overflow:scroll; height:300px;">
               
                <asp:GridView ID="GridView2" runat="server"
                    AutoGenerateColumns="False"
                    OnRowDataBound="GridView2_RowDataBound" BackColor="LightGoldenrodYellow"
                    BorderColor="Tan" BorderWidth="1px" CellPadding="2"
                    GridLines="None" Font-Names="Calibri" Font-Size="Medium"  ForeColor="Black" Width="1200px">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:TemplateField HeaderText="S No." Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%#Eval("ID")%>' CssClass="" ToolTip="S No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applicant">
                            <ItemTemplate>
                                <div style="width:130px;"><asp:Label ID="Label2" runat="server" Text='<%#Eval("APP_NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation ">
                            <ItemTemplate>
                                <div><asp:Label ID="Label3" runat="server" Text='<%#Eval("APP_DESIG")%>' CssClass="" ToolTip="Designation"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone Extension No.">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("APP_PHONE_EX_NO")%>' CssClass="" ToolTip="Phone Extension No."></asp:Label>
                            </ItemTemplate>
                            <%--    SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[] ,[APP_MOBILE_NO] ,[],[],[COMPLAIN_TYPE_ID] ,[] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[] ,[] --%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <div><asp:Label ID="Label5" runat="server" Text='<%#Eval("APP_LOCATION")%>' CssClass="" ToolTip="Location"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Problem">
                            <ItemTemplate>
                            <div><asp:Label ID="Label6" runat="server" Text='<%#Eval("PROBLEM_DISCRIPTION")%>' CssClass="" ToolTip="Problem"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room No.">
                            <ItemTemplate>
                                <div><asp:Label ID="Label7" runat="server" Text='<%#Eval("ROOM_NO")%>' CssClass="" ToolTip="Room No."></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Type">
                            <ItemTemplate>
                                <div><asp:Label ID="Label8" runat="server" Text='<%#Eval("COMPLAIN_DEPARTMENT")%>' CssClass="" ToolTip="Complain Type"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Date">
                            <ItemTemplate>
                                <div style="width:100px;"><asp:Label ID="Label9" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd-MM-yyyy}")%>' CssClass="" ToolTip="Complain Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                      <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <div><asp:Label ID="lblComplaintStatus" runat="server" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                         <asp:TemplateField HeaderText="Status Date">
                            <ItemTemplate>
                                <div><asp:Label ID="Label22" runat="server"  Text='<%#Eval("VIEW_TIMESTAMP")%>' CssClass="" ToolTip="Complain_Status Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"
                        BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <RowStyle Wrap="True" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
               

            </div>





           



        </div>
    </form>
</body>
</html>

