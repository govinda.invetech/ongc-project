﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ONGCUIProjects.Index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Uran Online Information System</title>
    <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" />
    <script type="text/javascript" src="js/jquery-1.7.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $('#txtloginid').focus();

            $("#cmdForgotPassword").click(function () {
                alert("Please contact administrator on extn no 4211");
            });

            function getInternetExplorerVersion()
            // Returns the version of Internet Explorer or a -1
            // (indicating the use of another browser).
            {
                var rv = -1; // Return value assumes failure.
                if (navigator.appName == 'Microsoft Internet Explorer') {
                    var ua = navigator.userAgent;
                    var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                    if (re.exec(ua) != null)
                        rv = parseFloat(RegExp.$1);
                }
                return rv;
            }
        });
    </script>
</head>
<body id="page" style="background-image: url(Images/pattern.png), url(Images/ongcplantbg-loginpage.jpg);
    height: 100%; overflow: hidden;">
    <script type="text/javascript" src="js/ChromeFrame.js"></script>
    <!-- Header Starts------------------------------------------------------------------------- -->
    <div class="header">
        <div class="logo-container">
            <div class="logo">
            </div>
            <!--
        <div class="logo-text">
        	<h1 class="logo-text-line1">Welcome to</h1>
            <h1 class="logo-text-line2">Oil and Natural Gas Corporation Limited</h1>
            <h1 class="logo-text-line3">URAN PLANT, MUMBAI</h1>
        </div>
        -->
        </div>
    </div>
    <!-- Header Ends----------------------------------------------------------------------------->
    <!--
<h1 class="ReportingSystemHeading">URAN Plant Online Information System</h1>
-->
    <div class="HeadingReportingSystem">
    </div>
    <!-- Login Box Starts ----------------------------------------------------------------------->
    <form name="Form1" method="post" action="default.aspx" id="Form1">
    <div class="login-box">
        <div class="login-box-content">
            <div style="position: relative; float: left; width: 100%;">
                <h1 style="position: absolute; top: 14px; -webkit-margin-before: 4px; left: 10px;
                    color: #34343b; font-family: Arial; font-weight: normal; z-index: 1; font-size: 20px;">
                    LOGIN ID :
                </h1>
                <input runat="server" id="txtloginid" class="login-id" type="text" value="" />
            </div>
            <div style="position: relative; float: left; width: 100%;">
                <h1 style="position: absolute; top: 18px; left: 10px; color: #34343b; font-family: Arial;
                    font-weight: normal; z-index: 1; font-size: 20px;">
                    PASSWORD :
                </h1>
                <input runat="server" id="txtpassword" class="login-password" type="password" value="" />
            </div>
            <h1 id="txtStatus" class="logo-text-line2" style="color: Red; top: 0px; left: 0px;
                font-size: 14px; margin-top: 10px; margin-bottom: 0px; text-shadow: 0 0 0 black;
                margin-top: 20px; max-height: 45px; overflow: auto;" runat="server">
                Welcome to</h1>
            <div class="login-controls-container">
                <input type="submit" class="signin-button" id="cmdsignin" href="javascript:void(0);"
                    value="Sign In"></input>
                <a class="registerme" id="cmdRegisterMe" href="NewUserReg.aspx?my_hint=NEW">Register
                    Me</a> <a class="forgotpassword" id="cmdForgotPassword" href="javascript:void(0);">Forgot
                        Password?</a>
            </div>
        </div>
    </div>
    </form>
    <!-- Login Box Ends ------------------------------------------------------------------------->
    <!-- Quick Links Starts --------------------------------------------------------------------->
    <div class="quick-links">
        <div style="position: relative; float: left; width: 100%;">
            <h1 class="heading-quicklinks">
                Quick Links..</h1>
        </div>
        <div class="quick-link-1">
            <a class="button-quicklinks" href="http://reports.ongc.co.in" target="_blank">
                <div class="ongc-reports">
                </div>
                <%--  <p class="linkname">ONGC Reports</p>--%>
            </a>
        </div>
        <div class="quick-link-2">
            <a class="button-quicklinks" href="https://webice.ongc.co.in/irj/portal" target="_blank">
                <div class="webice">
                </div>
                <%--  <p class="linkname">Webice</p>--%>
            </a>
        </div>
        <div class="quick-link-3">
            <a class="button-quicklinks" href="http://mail.ongc.co.in" target="_blank">
                <div class="ongc-mail">
                </div>
                <%--<p class="linkname">ONGC Mail</p>--%>
            </a>
        </div>
        <div class="quick-link-4">
            <a class="button-quicklinks" href="http://10.205.127.108/ONGC_Forum" target="_blank">
                <div class="learners-forum">
                </div>
                <%--<p class="linkname">Learners Forum</p>--%>
            </a>
        </div>
    </div>
    <!-- Quick Links Ends ----------------------------------------------------------------------->
    <!-- Footer Starts--------------------------------------------------------------------------->
    <div class="footer">
        <div class="footer-content">
            <p class="copyright">
                © 2009 - 2012 ONGC Limited. All rights reserved.</p>
            <p class="designedby">
                Designed & Developed by <a href="http://www.cybuzz.in" target="_blank">cyBuzz SC</a>
            </p>
        </div>
    </div>
    <!-- Footer Ends----------------------------------------------------------------------------->
    <script type="text/javascript">
        // You may want to place these lines inside an onload handler
        CFInstall.check({
            mode: "overlay",
            destination: self.location
        });
    </script>
</body>
</html>
