<%@ Page language="c#" Codebehind="Employee_Master.aspx.cs" AutoEventWireup="True" Inherits="WebApplication1.Employee_Master" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Employee_Master</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
            <script src="js/jquery-1.8.2.js"></script>
		    <script language="javascript">
		        $(document).ready(function () {
		            $('#Table2').hide();
		            $("#cboUnapproveEmployeeName").change(function () {
		                var as = $(this).val();
		                $.ajax({
		                    type: "GET",
		                    url: "services/hello.aspx",
		                    data: "cpf=" + as,
		                    success: function (msg) {
		                       
		                        var msg_arr = msg.split("~");
		                        if (msg_arr[0] == 'TRUE' && msg_arr[1] == 'SUCCESS') {
		                            var data_list = msg_arr[3].split('`');
		                            alert(msg_arr[3]);
		                            $('#Table2').show();
		                            $('#txtEmpName').val(data_list[2]);
		                        } else {

		                        }



		                    }
		                });
		            });

		        });
			function ConfirmDelete(msg)
			{
				return confirm("Are your sure to Insert ? \nThis can not be undone ...!");
			}
		</script>
	    <style type="text/css">
            .style1
            {
                width: 100%;
            }
            .style2
            {
                width: 227px;
            }
        </style>
	    </HEAD>
	<body>
		<FORM id="Form1" method="post" runat="server">
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U>Incident 
								Management - Employee Master</U></STRONG></FONT></FONT><STRONG><FONT face="Verdana" color="red" size="1">&nbsp;&nbsp;&nbsp;
					</FONT></STRONG>
			</P>
			<HR width="100%" SIZE="1">
			<STRONG><FONT face="Verdana" color="red" size="1">(Fields Marked With * are Mandatory 
					Fields)
            <br />
            <table class="style1">
                <tr>
                    <td class="style2">
                        Select Employee Name</td>
                    <td>
                        <asp:DropDownList ID="cboUnapproveEmployeeName" runat="server" 
                            onselectedindexchanged="cboUnapproveEmployeeName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <br />
            </FONT>
            </STRONG>&nbsp;<P align="center">
				<TABLE id="Table2" cellSpacing="8" cellPadding="4" width="100%" align="center" bgColor="whitesmoke"
					border="1" runat="server" visible="True">
					<TBODY>
						<TR>
							<TD borderColor="gray" align="center" bgColor="whitesmoke">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Employee Name <FONT color="red">
												(*)</FONT></STRONG></FONT></P>
							</TD>
							<TD borderColor="gray" align="center" bgColor="whitesmoke">
								<P align="left"><FONT face="Verdana" size="1">
										<asp:textbox id="txtEmpName" runat="server" Width="168px" Font-Names="Verdana" Font-Size="XX-Small"
											MaxLength="200" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></P>
							</TD>
							<TD borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG>
										<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>CPF No. <FONT color="red">
														(*)</FONT></STRONG></FONT>
									</STRONG></FONT>
			</P>
			</TD>
			<TD borderColor="gray" align="center" bgColor="whitesmoke">
				<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
							<asp:textbox id="txtEmpCPFNo" runat="server" Width="200px" Font-Names="Verdana" Font-Size="XX-Small"
								MaxLength="50" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
			</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Home Address Line 1</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtAddr1" runat="server" Width="176px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="8000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0" TextMode="MultiLine"
									Height="88px"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Home Address Line 2</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtAddr2" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="8000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0" TextMode="MultiLine"
									Height="88px"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>City</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtCity" runat="server" Width="176px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="100" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>State</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtState" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="100" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Pincode</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtPinCode" runat="server" Width="176px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="10" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Home Phone Number</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left">
						<asp:textbox id="txtHomeNo" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
							MaxLength="15" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Mobile Number</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtMblNo" runat="server" Width="176px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="11" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Gender</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:dropdownlist id="CmbGender" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
									CssClass="flattxt" Height="8px">
									<asp:ListItem Value="M">M</asp:ListItem>
									<asp:ListItem Value="F">F</asp:ListItem>
								</asp:dropdownlist></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Designation</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:dropdownlist id="cmbDesg" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="176px" Height="8px"></asp:dropdownlist></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="#000099">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Date of Birth</STRONG></FONT>
							</FONT></STRONG></FONT></P></TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:dropdownlist id="CmbDD_DOB" runat="server" Width="48px" Font-Names="Verdana" Font-Size="XX-Small"
									Height="20px">
									<asp:ListItem Value="DD">DD</asp:ListItem>
									<asp:ListItem Value="01">01</asp:ListItem>
									<asp:ListItem Value="02">02</asp:ListItem>
									<asp:ListItem Value="03">03</asp:ListItem>
									<asp:ListItem Value="04">04</asp:ListItem>
									<asp:ListItem Value="05">05</asp:ListItem>
									<asp:ListItem Value="06">06</asp:ListItem>
									<asp:ListItem Value="07">07</asp:ListItem>
									<asp:ListItem Value="08">08</asp:ListItem>
									<asp:ListItem Value="09">09</asp:ListItem>
									<asp:ListItem Value="10">10</asp:ListItem>
									<asp:ListItem Value="11">11</asp:ListItem>
									<asp:ListItem Value="12">12</asp:ListItem>
									<asp:ListItem Value="13">13</asp:ListItem>
									<asp:ListItem Value="14">14</asp:ListItem>
									<asp:ListItem Value="15">15</asp:ListItem>
									<asp:ListItem Value="16">16</asp:ListItem>
									<asp:ListItem Value="17">17</asp:ListItem>
									<asp:ListItem Value="18">18</asp:ListItem>
									<asp:ListItem Value="19">19</asp:ListItem>
									<asp:ListItem Value="20">20</asp:ListItem>
									<asp:ListItem Value="21">21</asp:ListItem>
									<asp:ListItem Value="22">22</asp:ListItem>
									<asp:ListItem Value="23">23</asp:ListItem>
									<asp:ListItem Value="24">24</asp:ListItem>
									<asp:ListItem Value="25">25</asp:ListItem>
									<asp:ListItem Value="26">26</asp:ListItem>
									<asp:ListItem Value="27">27</asp:ListItem>
									<asp:ListItem Value="28">28</asp:ListItem>
									<asp:ListItem Value="29">29</asp:ListItem>
									<asp:ListItem Value="30">30</asp:ListItem>
									<asp:ListItem Value="31">31</asp:ListItem>
								</asp:dropdownlist></FONT></STRONG>
						<asp:dropdownlist id="cmbMM_DOB" runat="server" Width="48px" Font-Names="Verdana" Font-Size="XX-Small"
							Height="20px">
							<asp:ListItem Value="MM">MM</asp:ListItem>
							<asp:ListItem Value="01">01</asp:ListItem>
							<asp:ListItem Value="02">02</asp:ListItem>
							<asp:ListItem Value="03">03</asp:ListItem>
							<asp:ListItem Value="04">04</asp:ListItem>
							<asp:ListItem Value="05">05</asp:ListItem>
							<asp:ListItem Value="06">06</asp:ListItem>
							<asp:ListItem Value="07">07</asp:ListItem>
							<asp:ListItem Value="08">08</asp:ListItem>
							<asp:ListItem Value="09">09</asp:ListItem>
							<asp:ListItem Value="10">10</asp:ListItem>
							<asp:ListItem Value="11">11</asp:ListItem>
							<asp:ListItem Value="12">12</asp:ListItem>
						</asp:dropdownlist>
						<asp:dropdownlist id="cmbYY_DOB" runat="server" Width="72px" Font-Names="Verdana" Font-Size="XX-Small">
							<asp:ListItem Value="YYYY">YYYY</asp:ListItem>
							<asp:ListItem Value="1946">1946</asp:ListItem>
							<asp:ListItem Value="1947">1947</asp:ListItem>
							<asp:ListItem Value="1948">1948</asp:ListItem>
							<asp:ListItem Value="1949">1949</asp:ListItem>
							<asp:ListItem Value="1950">1950</asp:ListItem>
							<asp:ListItem Value="1951">1951</asp:ListItem>
							<asp:ListItem Value="1952">1952</asp:ListItem>
							<asp:ListItem Value="1953">1953</asp:ListItem>
							<asp:ListItem Value="1954">1954</asp:ListItem>
							<asp:ListItem Value="1955">1955</asp:ListItem>
							<asp:ListItem Value="1956">1956</asp:ListItem>
							<asp:ListItem Value="1957">1957</asp:ListItem>
							<asp:ListItem Value="1958">1958</asp:ListItem>
							<asp:ListItem Value="1959">1959</asp:ListItem>
							<asp:ListItem Value="1960">1960</asp:ListItem>
							<asp:ListItem Value="1961">1961</asp:ListItem>
							<asp:ListItem Value="1962">1962</asp:ListItem>
							<asp:ListItem Value="1963">1963</asp:ListItem>
							<asp:ListItem Value="1964">1964</asp:ListItem>
							<asp:ListItem Value="1965">1965</asp:ListItem>
							<asp:ListItem Value="1966">1966</asp:ListItem>
							<asp:ListItem Value="1967">1967</asp:ListItem>
							<asp:ListItem Value="1968">1968</asp:ListItem>
							<asp:ListItem Value="1969">1969</asp:ListItem>
							<asp:ListItem Value="1970">1970</asp:ListItem>
							<asp:ListItem Value="1971">1971</asp:ListItem>
							<asp:ListItem Value="1972">1972</asp:ListItem>
							<asp:ListItem Value="1973">1973</asp:ListItem>
							<asp:ListItem Value="1974">1974</asp:ListItem>
							<asp:ListItem Value="1975">1975</asp:ListItem>
							<asp:ListItem Value="1976">1976</asp:ListItem>
							<asp:ListItem Value="1977">1977</asp:ListItem>
							<asp:ListItem Value="1978">1978</asp:ListItem>
							<asp:ListItem Value="1979">1979</asp:ListItem>
							<asp:ListItem Value="1980">1980</asp:ListItem>
							<asp:ListItem Value="1981">1981</asp:ListItem>
							<asp:ListItem Value="1982">1982</asp:ListItem>
							<asp:ListItem Value="1983">1983</asp:ListItem>
							<asp:ListItem Value="1984">1984</asp:ListItem>
							<asp:ListItem Value="1985">1985</asp:ListItem>
							<asp:ListItem Value="1986">1986</asp:ListItem>
							<asp:ListItem Value="1987">1987</asp:ListItem>
							<asp:ListItem Value="1988">1988</asp:ListItem>
							<asp:ListItem Value="1989">1989</asp:ListItem>
							<asp:ListItem Value="1990">1990</asp:ListItem>
							<asp:ListItem Value="1991">1991</asp:ListItem>
							<asp:ListItem Value="1992">1992</asp:ListItem>
							<asp:ListItem Value="1993">1993</asp:ListItem>
							<asp:ListItem Value="1994">1994</asp:ListItem>
							<asp:ListItem Value="1995">1995</asp:ListItem>
							<asp:ListItem Value="1996">1996</asp:ListItem>
							<asp:ListItem Value="1997">1997</asp:ListItem>
							<asp:ListItem Value="1998">1998</asp:ListItem>
							<asp:ListItem Value="1999">1999</asp:ListItem>
							<asp:ListItem Value="2000">2000</asp:ListItem>
							<asp:ListItem Value="2001">2001</asp:ListItem>
							<asp:ListItem Value="2002">2002</asp:ListItem>
							<asp:ListItem Value="2003">2003</asp:ListItem>
							<asp:ListItem Value="2004">2004</asp:ListItem>
							<asp:ListItem Value="2005">2005</asp:ListItem>
							<asp:ListItem Value="2006">2006</asp:ListItem>
							<asp:ListItem Value="2007">2007</asp:ListItem>
							<asp:ListItem Value="2008">2008</asp:ListItem>
							<asp:ListItem Value="2009">2009</asp:ListItem>
							<asp:ListItem Value="2010">2010</asp:ListItem>
							<asp:ListItem Value="2011">2011</asp:ListItem>
							<asp:ListItem Value="2012">2012</asp:ListItem>
							<asp:ListItem Value="2013">2013</asp:ListItem>
							<asp:ListItem Value="2014">2014</asp:ListItem>
							<asp:ListItem Value="2015">2015</asp:ListItem>
							<asp:ListItem Value="2016">2016</asp:ListItem>
							<asp:ListItem Value="2017">2017</asp:ListItem>
							<asp:ListItem Value="2018">2018</asp:ListItem>
							<asp:ListItem Value="2019">2019</asp:ListItem>
							<asp:ListItem Value="2020">2020</asp:ListItem>
							<asp:ListItem Value="2021">2021</asp:ListItem>
							<asp:ListItem Value="2022">2022</asp:ListItem>
							<asp:ListItem Value="2023">2023</asp:ListItem>
							<asp:ListItem Value="2024">2024</asp:ListItem>
							<asp:ListItem Value="2025">2025</asp:ListItem>
							<asp:ListItem Value="2026">2026</asp:ListItem>
							<asp:ListItem Value="2027">2027</asp:ListItem>
							<asp:ListItem Value="2028">2028</asp:ListItem>
							<asp:ListItem Value="2029">2029</asp:ListItem>
							<asp:ListItem Value="2030">2030</asp:ListItem>
						</asp:dropdownlist></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Supervisor Name</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbSupName" runat="server" Width="176px" Font-Names="Verdana" Font-Size="XX-Small"
							CssClass="flattxt" Height="8px"></asp:dropdownlist></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Department</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtDept" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
							MaxLength="100" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Work Location</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbLocation" runat="server" Width="176px" Font-Names="Verdana" Font-Size="XX-Small"
							CssClass="flattxt" Height="8px"></asp:dropdownlist></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Blood Group</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbBldGrp" runat="server" Font-Size="XX-Small" Font-Names="Verdana" Width="208px"
							Height="20px">
							<asp:ListItem Value="Select Blood Group">Select Blood Group</asp:ListItem>
							<asp:ListItem Value="A +ve">A +ve</asp:ListItem>
							<asp:ListItem Value="A -ve">A -ve</asp:ListItem>
							<asp:ListItem Value="A1 +ve">A1 +ve</asp:ListItem>
							<asp:ListItem Value="A1 -ve">A1 -ve</asp:ListItem>
							<asp:ListItem Value="A2 +ve">A2 +ve</asp:ListItem>
							<asp:ListItem Value="A2 -ve">A2 -ve</asp:ListItem>
							<asp:ListItem Value="AB +ve">AB +ve</asp:ListItem>
							<asp:ListItem Value="AB -ve">AB -ve</asp:ListItem>
							<asp:ListItem Value="B +ve">B +ve</asp:ListItem>
							<asp:ListItem Value="B -ve">B -ve</asp:ListItem>
							<asp:ListItem Value="O +ve">O +ve</asp:ListItem>
							<asp:ListItem Value="O -ve">O -ve</asp:ListItem>
							<asp:ListItem Value="Others">Others</asp:ListItem>
						</asp:dropdownlist></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Email Id 1</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtEmail1" runat="server" BorderColor="#E0E0E0" BorderStyle="Solid" CssClass="flattxt"
							MaxLength="1000" Font-Size="XX-Small" Font-Names="Verdana" Width="200px"></asp:textbox></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Email Id&nbsp;2</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtEmail2" runat="server" BorderColor="#E0E0E0" BorderStyle="Solid" CssClass="flattxt"
							MaxLength="1000" Font-Size="XX-Small" Font-Names="Verdana" Width="200px"></asp:textbox></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Email Id&nbsp;3</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtEmail3" runat="server" BorderColor="#E0E0E0" BorderStyle="Solid" CssClass="flattxt"
							MaxLength="1000" Font-Size="XX-Small" Font-Names="Verdana" Width="200px"></asp:textbox></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Employee Level</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtLevel" runat="server" BorderColor="#E0E0E0" BorderStyle="Solid" CssClass="flattxt"
							MaxLength="10" Font-Size="XX-Small" Font-Names="Verdana" Width="200px"></asp:textbox></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Incident Reporter</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbRep" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="40px" Height="8px">
							<asp:ListItem Value="Y">Y</asp:ListItem>
							<asp:ListItem Value="N">N</asp:ListItem>
						</asp:dropdownlist></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Incident&nbsp;Reviewer</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbRev" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="40px" Height="8px">
							<asp:ListItem Value="Y">Y</asp:ListItem>
							<asp:ListItem Value="N">N</asp:ListItem>
						</asp:dropdownlist></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Incident&nbsp;Manager</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbMan" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="40px" Height="8px">
							<asp:ListItem Value="Y">Y</asp:ListItem>
							<asp:ListItem Value="N">N</asp:ListItem>
						</asp:dropdownlist></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Incident&nbsp;Acceptance 
								Authority Level 1</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbAcp1" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="40px" Height="8px">
							<asp:ListItem Value="Y">Y</asp:ListItem>
							<asp:ListItem Value="N">N</asp:ListItem>
						</asp:dropdownlist></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Incident&nbsp;Acceptance 
								Authority Level&nbsp;2</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:dropdownlist id="cmbAcp2" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="40px" Height="8px">
							<asp:ListItem Value="Y">Y</asp:ListItem>
							<asp:ListItem Value="N">N</asp:ListItem>
						</asp:dropdownlist></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG></STRONG></FONT>&nbsp;</P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">&nbsp;</P>
				</TD>
			</TR>
			</TBODY></TABLE></P>
			<P align="center">
				<asp:button id="BtnCancel" runat="server" Width="150px" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Height="18px" Text="Cancel" CausesValidation="False"
					Font-Bold="True" BackColor="Silver" ForeColor="ControlText" Visible="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnSaveEmpData" runat="server" Font-Names="Verdana" 
                    Font-Size="XX-Small" BorderStyle="Solid"
					BorderColor="DarkGray" Text="Create New Employee" Font-Bold="True" BackColor="Silver" 
                    ForeColor="ControlText" onclick="BtnSaveEmpData_Click" Visible="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnRefresh" runat="server" Width="158px" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Height="18px" Text="Refresh" CausesValidation="False"
					Font-Bold="True" BackColor="Silver" ForeColor="ControlText" Visible="False"></asp:button></P>
		</FORM>
	</body>
</HTML>
