﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeedbackReview.aspx.cs" Inherits="ONGCUIProjects.FeedbackReview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    

    <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {

            $('#DivFeedbackData').on('click', '.cmdedit', function (event) {
                var id = $(this).attr('id');

                if (confirm("Are you sure you want to approve ? ")) {
                    var data = '{id: "' + id + '"}';

                    $.ajax({
                        type: "POST",
                        url: "FeedbackReview.aspx/UpdateStatus",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var result = jQuery.parseJSON(response.d);
                            if (result.STATUS == "TRUE") {
                                location.reload(true);/*--for Reload page--*/
                                alert(result.MESSAGE);
                            } else {
                                alert(result.MESSAGE);
                            }
                        }
                    });
                }

            });


        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="div-fullwidth iframeMainDiv">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;">Feedback Review</h1>

            </div>
            <div class="div-pagebottom"></div>
            <div runat="server" id="DivFeedbackData" class="div-fullwidth" style="margin-bottom: 10px;">
            </div>
        </div>
    </form>
</body>
</html>
