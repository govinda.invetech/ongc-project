﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuestionReports.aspx.cs"
    Inherits="ONGCUIProjects.QuestionReports" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
    <script type="text/javascript" src="js/jquery.multiselect.js"></script>
    <script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script type="text/javascript" language="javascript" src="js/highcharts.js"></script>
    <script type="text/javascript" language="javascript" src="js/print.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                var startDateTextBox = $('#txtFromDate');
                var endDateTextBox = $('#txtToDate');

                startDateTextBox.datepicker({
                    onClose: function (dateText, inst) {
                        if (endDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datepicker('getDate');
                            var testEndDate = endDateTextBox.datepicker('getDate');
                            if (testStartDate > testEndDate)
                                endDateTextBox.datepicker('setDate', testStartDate);
                        }
                        else {
                            endDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime) {
                        endDateTextBox.datepicker('option', 'minDate', startDateTextBox.datepicker('getDate'));
                    },
                    dateFormat: "dd-mm-yy"
                });
                endDateTextBox.datepicker({
                    onClose: function (dateText, inst) {
                        if (startDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datepicker('getDate');
                            var testEndDate = endDateTextBox.datepicker('getDate');
                            if (testStartDate > testEndDate)
                                startDateTextBox.datepicker('setDate', testEndDate);
                        }
                        else {
                            startDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime) {
                        startDateTextBox.datepicker('option', 'maxDate', endDateTextBox.datepicker('getDate'));
                    },
                    dateFormat: "dd-mm-yy"
                });
            });

            $('#cmdViewReport').click(function () {
                var dtFromDate = $('#txtFromDate').val();
                var dtToDate = $('#txtToDate').val();

                if (dtFromDate == "" || dtToDate == "") {
                    alert('Please select dates');
                } else {
                    window.location = "QuestionReports.aspx?FromDate=" + dtFromDate + "&ToDate=" + dtToDate;
                }
            });

            $('#cmdPrintReport').click(function () {
                var RemarkData = "";
                $('.RemarkText').each(function () {

                    var QueID = $(this).attr('QueId');
                    var QueRemark = $(this).val().trim();

                    if (QueRemark != "") {
                        RemarkData += QueID + '`' + QueRemark + "~";
                    }
                });
                if (RemarkData != "") {
                    $.ajax({
                        type: 'POST',
                        url: 'services/quiz/SaveRemark.aspx',
                        data: "RemarkData=" + RemarkData,
                        success: function (msg) {
                            var msg_arr = msg.split('~');
                            if (msg_arr[0] == 'TRUE' && msg_arr[1] == 'SUCCESS') {
                                $("#div_que_report").printElement({
                                    overrideElementCSS: [
                                    'css/styleGlobal.css',
                                    { href: 'css/styleGlobal.css', media: 'print' }
                                    ]
                                });
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                } else {
                    $("#div_que_report").printElement({
                        overrideElementCSS: [
                        'css/styleGlobal.css',
                        { href: 'css/styleGlobal.css', media: 'print' }
                        ]
                    });
                }
            });

            $('.GraphArea').each(function () {
                var data = $(this).attr('graphdata');
                var target = $(this).children('div').attr('id'); ;
                ColumnChart(data, target);
            });

            function ColumnChart(data, target) {

                var chart;
                var sCat = data;
                var myCat = sCat.split('~');

                var colors = Highcharts.getOptions().colors;

                categories = [];
                data = [];
                for (var i = 0; i < myCat.length; i++) {
                    var myData = myCat[i].split('`');
                    categories.push(myData[0])
                    data.push({ y: parseFloat(myData[1]), color: colors[i] })
                }


                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: target,
                        type: 'column'
                    },
                    title: {
                        text: false
                    },

                    xAxis: {
                        categories: categories

                    },
                    legend: {
                        enabled: false
                    },
                    yAxis: {
                        title: false
                    },
                    plotOptions: {
                        column: {
                            dataLabels: {
                                enabled: false,
                                color: colors[0],
                                style: {
                                    fontWeight: 'bold'
                                },
                                formatter: function () {
                                    return this.y + '%';
                                }
                            }
                        }
                    },
                    tooltip: {

                        formatter: function () {
                            var s = '<b>' + this.x + ':' + this.y + ' %</b>';
                            return s;
                        }
                    },

                    series: [{
                        name: name,
                        data: data
                    }],
                    exporting: {
                        enabled: true
                    },
                    credits: {
                        enabled: false
                    }
                });

            }
        });
    </script>
 
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-fullwidth iframeMainDiv" style="width: 98%;">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading-GatePass">
                Survey Question Reports</h1>
        </div>
        <div class="div-pagebottom">
        </div>
        <div style="position: relative; float: left; top: 5px; left: 0px; padding: 2px 0px;
            width: 100%; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
            background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc;
            margin-bottom: 20px;">
            <div style="position: relative; margin-right: auto; margin-left: auto; width: auto;">
            <p class="content" style="margin-left: 10px; margin-right:20px;">
                        Select Date </p>
                <div style="position: relative; float: left;">
                    <p class="content" style="margin-left: 10px;">
                        From:</p>
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                </div>
                <div style="position: relative; float: left;">
                    <p class="content" style="margin-left: 10px;">
                        To:</p>
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                </div>
                <input type='button' value='View Report' id='cmdViewReport' class='g-button g-button-submit'
                    style='margin-left: 10px; float: left;' />
                <input type='button' value='Print Report' id='cmdPrintReport' runat='server' class='g-button g-button-share'
                    style='margin-right: 10px; float: right;' />
            </div>
        </div>
        <div class="div-fullwidth marginbottom" id="div_que_report" runat="server">
           
        </div>
    </div>
    </form>
</body>
</html>
