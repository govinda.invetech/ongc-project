﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class EmployeeProfileMasters : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            txtMasterText.Text = " ";
            if (Request["type"] != null)
            {
                string sMasterType = Request["type"].ToString();
                string sQuery = "";
                string sValueNoFound = "";
                HeaderToShow.InnerHtml = "Employee Profile Masters";
                cmdAddMasterFancyBox.Text = "ADD " + sMasterType;
                cmdAddMasterFancyBox.Visible = true;
                FancyBoxHeading.InnerHtml = "ADD NEW " + sMasterType;
                InputBoxHeading.InnerHtml = sMasterType +" NAME";
                cmdAddMaster.Attributes.Add("MasterType", sMasterType);



                switch (sMasterType)
                {
                    case "DESIGNATION":
                        HeaderToShow.InnerHtml = "Designation Master";
                        sQuery = "SELECT [O_INDX_NMBR],[O_DSG_DTL] FROM [O_DSG_MSTR] ORDER BY [O_DSG_DTL] ASC";
                        sValueNoFound = "Designation Not Found. Please add a Designation.";
                        break;
                    case "LOCATION":
                        HeaderToShow.InnerHtml = "Location Master";
                        sQuery = "SELECT [O_INDX_NMBR],[O_LCTN_NM] FROM [O_LCTN_MSTR] ORDER BY [O_LCTN_NM] ASC";
                        sValueNoFound = "Location Not Found. Please add a Location.";
                        break;
                    case "DEPARTMENT":
                        HeaderToShow.InnerHtml = "Department Master";
                        sQuery = "SELECT [id],[name] FROM [CY_DEPT_DTLS] ORDER BY [name] ASC";
                        sValueNoFound = "Department Not Found. Please add a Department.";
                        break;
                }

                try
                {
                    if (sQuery != "")
                    {
                        DataSet dsData = My.ExecuteSELECTQuery(sQuery);

                        if (dsData.Tables[0].Rows.Count == 0)
                        {
                            div_master_list.InnerHtml = "<h1 style=\"width: 100%; text-align:center;\" class=\"heading\">" + sValueNoFound + "</h1>";

                        }
                        else
                        {

                            string sTableData = "<table class=\"IncTable\">";
                            sTableData = sTableData + "<tbody>";
                            sTableData = sTableData + "<tr>";
                            sTableData = sTableData + "<th>" + sMasterType.ToUpper() + " NAME</th>";
                            sTableData = sTableData + "<th style=\"width: 1px;\">ACTION</th>";
                            sTableData = sTableData + "</tr>";
                            
                            for (int i = 0; i < dsData.Tables[0].Rows.Count; i++)
                            {
                                string sMasterID = dsData.Tables[0].Rows[i][0].ToString();
                                string sMasterText = dsData.Tables[0].Rows[i][1].ToString();

                                sTableData = sTableData + "<tr>";
                                sTableData = sTableData + "<td>" + sMasterText + "</td>";
                                sTableData = sTableData + "<td><a class='logout cmdDeleteMaster' href='javascript:void(0);' style='margin:0px;' DelIDType='" + sMasterID + "~" + sMasterType + "'>DELETE</a></td>";
                                sTableData = sTableData + "</tr>";
                            }

                            sTableData = sTableData + "</tbody>";
                            sTableData = sTableData + "</table>";

                            div_master_list.InnerHtml = sTableData;
                        }
                    }
                }
                catch (Exception ex)
                {
                    div_master_list.InnerHtml = ex.Message;
                }
            }
        }
    }
}