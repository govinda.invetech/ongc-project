﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class FeedbackReview : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {

                Response.Write("FALSE~ERR~Session Expired ! Please Login Again");
                return;
            }
            string cpf_no = Session["Login_Name"].ToString();

            if (cpf_no == "")
            {
                Response.Write("FALSE~ERR~Invalid CPF Number loggined");
                return;
            }
            #region Get Feedback Report
            try
            {
                string sStr1 = " ";
                sStr1 += " SELECT CY_FEEDBACK.ID, [CPF_NO], [TIMESTAMP], [REMARK], [RATE],  ISNULL(COMMENT.NAME,'') AS NAME,STATUS FROM [cy_feedback]  ";
                sStr1 += " left join COMMENT                        ";
                sStr1 += " on COMMENT.ID=[CY_FEEDBACK].C_ID where CY_FEEDBACK.C_ID=0   ";
                sStr1 += " ORDER BY [TIMESTAMP] DESC ";
                DataSet FeedbackDS = new DataSet();
                FeedbackDS = My.ExecuteSELECTQuery(sStr1);


                if (FeedbackDS.Tables[0].Rows.Count == 0)
                {
                    DivFeedbackData.InnerHtml = "<p class=\"content\">No Feedback Received</p>";
                }
                else
                {
                    string FeedbackData = "";
                    FeedbackData = FeedbackData + "<table  class=\"tftable\" border=\"1\">";
                    FeedbackData = FeedbackData + "<tr>";
                    FeedbackData = FeedbackData + "<th style=\"width: 5%;\">Sl. No.</th>";
                    FeedbackData = FeedbackData + "<th>Employee Name (CPF No)</th>";
                    FeedbackData = FeedbackData + "<th>Date</th>";
                    FeedbackData = FeedbackData + "<th>Services</th>";
                    FeedbackData = FeedbackData + "<th>Remark</th>";
                    FeedbackData = FeedbackData + "<th style=\"width: 5%;\">Rating</th>";
                    FeedbackData = FeedbackData + "</tr>";
                    for (int i = 0; i < FeedbackDS.Tables[0].Rows.Count; i++)
                    {
                        string sID = FeedbackDS.Tables[0].Rows[i]["ID"].ToString();
                        string sCPFNo = FeedbackDS.Tables[0].Rows[i]["CPF_NO"].ToString();
                        DateTime sDate = Convert.ToDateTime(FeedbackDS.Tables[0].Rows[i]["TIMESTAMP"]);
                        string sRemark = FeedbackDS.Tables[0].Rows[i]["REMARK"].ToString();
                        string sRating = FeedbackDS.Tables[0].Rows[i]["RATE"].ToString();
                        string sService = FeedbackDS.Tables[0].Rows[i]["NAME"].ToString();
                        bool sStatus = Convert.ToBoolean(FeedbackDS.Tables[0].Rows[i]["STATUS"].ToString());
                        string sEmployeeName = My.GetEmployeeNameFromCPFNo(sCPFNo);


                        FeedbackData = FeedbackData + "<tr>";
                        FeedbackData = FeedbackData + "<td style=\"text-align: center;\">" + (i + 1) + "</td>";
                        FeedbackData = FeedbackData + "<td style=\"text-align: center;\"><a href='#' runat='server'>" + sEmployeeName + "</a> (" + sCPFNo + ")</td>";
                        FeedbackData = FeedbackData + "<td style=\"text-align: center;\">" + sDate.ToString("d MMMM, yyyy") + "</td>";
                        FeedbackData = FeedbackData + "<td style=\"text-align: left;\">" + sService + "</td>";
                        FeedbackData = FeedbackData + "<td style=\"text-align: left;\">" + sRemark + "</td>";
                        FeedbackData = FeedbackData + "<td style=\"text-align: center;\">" + sRating + "</td>";
                        if (!sStatus)
                            FeedbackData += "<td><input type='button' class='g-button g-button-share cmdedit' value='Approve' id=" + sID + " /></td>";
                        else
                            FeedbackData = FeedbackData + "<td style=\"text-align: center;\">Approved</td>";
                        FeedbackData = FeedbackData + "</tr>";
                    }
                    FeedbackData = FeedbackData + "</table>";
                    DivFeedbackData.InnerHtml = FeedbackData;
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends
            #endregion
        }


        [System.Web.Services.WebMethod]
        public static string UpdateStatus(string id)
        {
            string str = " ";
            str += "   UPDATE [dbo].[cy_feedback] ";
            str += "   SET STATUS =1              ";
            str += "   WHERE id='" + id + "'      ";


            try
            {
                if (MyApplication1.ExecuteSQLQuery_static(str) == true)
                {
                    return "{\"STATUS\":\"TRUE\",\"MESSAGE\":\"Approve has been  done.\"}";
                }

                else
                    return "{\"STATUS\":\"ERR\",\"MESSAGE\":\"Error in Execution . \"}";
            }
            catch (Exception e)
            {
                return "{\"STATUS\":\"ERR\",\"MESSAGE\":\"" + e.Message + "\"}";
            }
        }
    }
}