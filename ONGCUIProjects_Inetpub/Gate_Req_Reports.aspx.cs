using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;

namespace ONGCUIProjects
{
    /// <summary>
    /// Summary description for Gate_Req_Reports.
    /// </summary>
    public partial class Gate_Req_Reports : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        #region Page Load
        protected void Page_Load(object sender, System.EventArgs e)
        {

        }
        #endregion

        protected void cmdgenerate_Click1(object sender, EventArgs e)
        {
            string sSelectedReportType = ddltypeofreport.SelectedValue;
            string str = "";
            switch (sSelectedReportType)
            {
                case "DATEWISE":
                    string date1 = txtdatefrom.Text;
                    string date2 = txtdateto.Text;
                    if (date1 == "" || date2 == "")
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('Please Select Valid From Date Or To Date')" + "</Script>");
                        return;
                    }
                    else
                    {
                        str = "Select ROW_NUMBER() OVER(ORDER BY [O_INDX_NMBR]) AS [SERIAL], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No.], O_APLCNT_DESG as [Designation], O_APLCNT_LCTN as [Location], O_APLCNT_EXT_NMBR as [Extn. No.], O_REQSTN_TYPE as [Requisition Type], CONVERT(VARCHAR,[O_ENTRY_DT],105) as [Entry Date], CONVERT(VARCHAR,[O_EXIT_DT],105) as [Exit Date], O_ENTRY_AREA as [Entry Area], O_MTNG_OFF_NM as [Meeting Officer Name], O_MTNG_OFF_DESG as [Officer Designation], O_MTNG_OFF_LCTN as [Meeting Location], O_VISTR_NM as [Visitor Name], O_VISTR_ORGNZTN as [Visitor Organization], O_VISTR_AGE as [Visitor Age], O_VISTR_GNDR as [Visitor Gender], O_PRPS_OF_VISIT as [Purpose of Visit], O_RMRKS as [Comments], O_ACP1_NM as [Pass Approver], O_ACP1_CPF_NMBR as [Approver CPF No.], O_SYS_DT_TM as [System Date] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_REQ_COMPLTN_FLG = 'P' and O_PASS_TYPE_FLG = 'V' and  O_ENTRY_DT between '" + My.ConvertDateStringintoSQLDateString(date1) + "' and '" + My.ConvertDateStringintoSQLDateString(date2) + "' order by O_INDX_NMBR";
                    }
                    break;
                case "REQTYPE":
                    string sRequesitionType = ddlrequtiontype.SelectedValue;
                    if (sRequesitionType == "")
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('Please select requisition type.')" + "</Script>");
                        return;
                    }
                    else
                    {
                        str = "Select ROW_NUMBER() OVER(ORDER BY [O_INDX_NMBR]) AS [SERIAL], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No.], O_APLCNT_DESG as [Designation], O_APLCNT_LCTN as [Location], O_APLCNT_EXT_NMBR as [Extn. No.], O_REQSTN_TYPE as [Requisition Type], CONVERT(VARCHAR,[O_ENTRY_DT],105) as [Entry Date], CONVERT(VARCHAR,[O_EXIT_DT],105) as [Exit Date], O_ENTRY_AREA as [Entry Area], O_MTNG_OFF_NM as [Meeting Officer Name], O_MTNG_OFF_DESG as [Officer Designation], O_MTNG_OFF_LCTN as [Meeting Location], O_VISTR_NM as [Visitor Name], O_VISTR_ORGNZTN as [Visitor Organization], O_VISTR_AGE as [Visitor Age], O_VISTR_GNDR as [Visitor Gender], O_PRPS_OF_VISIT as [Purpose of Visit], O_RMRKS as [Comments], O_ACP1_NM as [Pass Approver], O_ACP1_CPF_NMBR as [Approver CPF No.], O_SYS_DT_TM as [System Date] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_REQ_COMPLTN_FLG = 'P' and O_PASS_TYPE_FLG = 'V' and  O_REQSTN_TYPE =  '" + sRequesitionType + "' order by O_INDX_NMBR";
                    }
                    break;
                case "AREAWISE":
                    string sEntryArea = ddlarea.SelectedValue;
                    if (sEntryArea == "")
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('Please select entry area.')" + "</Script>");
                        return;
                    }
                    else
                    {
                        str = "Select ROW_NUMBER() OVER(ORDER BY [O_INDX_NMBR]) AS [SERIAL], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No.], O_APLCNT_DESG as [Designation], O_APLCNT_LCTN as [Location], O_APLCNT_EXT_NMBR as [Extn. No.], O_REQSTN_TYPE as [Requisition Type], CONVERT(VARCHAR,[O_ENTRY_DT],105) as [Entry Date], CONVERT(VARCHAR,[O_EXIT_DT],105) as [Exit Date], O_ENTRY_AREA as [Entry Area], O_MTNG_OFF_NM as [Meeting Officer Name], O_MTNG_OFF_DESG as [Officer Designation], O_MTNG_OFF_LCTN as [Meeting Location], O_VISTR_NM as [Visitor Name], O_VISTR_ORGNZTN as [Visitor Organization], O_VISTR_AGE as [Visitor Age], O_VISTR_GNDR as [Visitor Gender], O_PRPS_OF_VISIT as [Purpose of Visit], O_RMRKS as [Comments], O_ACP1_NM as [Pass Approver], O_ACP1_CPF_NMBR as [Approver CPF No.], O_SYS_DT_TM as [System Date] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_REQ_COMPLTN_FLG = 'P' and O_PASS_TYPE_FLG = 'V' and  O_ENTRY_AREA =  '" + sEntryArea + "' order by O_INDX_NMBR";
                    }
                    break;
                case "OFFICERWISE":
                    string sOfficerCPF = ddlEmployee.SelectedValue;
                    if (sOfficerCPF == "")
                    {
                        Response.Write("<Script language='JavaScript'>" + "alert('Please select officer name.')" + "</Script>");
                        return;
                    }
                    else
                    {
                        str = "Select ROW_NUMBER() OVER(ORDER BY [O_INDX_NMBR]) AS [SERIAL], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No.], O_APLCNT_DESG as [Designation], O_APLCNT_LCTN as [Location], O_APLCNT_EXT_NMBR as [Extn. No.], O_REQSTN_TYPE as [Requisition Type], CONVERT(VARCHAR,[O_ENTRY_DT],105) as [Entry Date], CONVERT(VARCHAR,[O_EXIT_DT],105) as [Exit Date], O_ENTRY_AREA as [Entry Area], O_MTNG_OFF_NM as [Meeting Officer Name], O_MTNG_OFF_DESG as [Officer Designation], O_MTNG_OFF_LCTN as [Meeting Location], O_VISTR_NM as [Visitor Name], O_VISTR_ORGNZTN as [Visitor Organization], O_VISTR_AGE as [Visitor Age], O_VISTR_GNDR as [Visitor Gender], O_PRPS_OF_VISIT as [Purpose of Visit], O_RMRKS as [Comments], O_ACP1_NM as [Pass Approver], O_ACP1_CPF_NMBR as [Approver CPF No.], O_SYS_DT_TM as [System Date] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_REQ_COMPLTN_FLG = 'P' and O_PASS_TYPE_FLG = 'V' and  O_APLCNT_CPF_NMBR =  '" + sOfficerCPF + "' order by O_INDX_NMBR";
                    }
                    break;

            }

            if (str != "")
            {
                DataSet ds = My.ExecuteSELECTQuery(str);

                GridView1.DataSource = ds.Tables[0];
                GridView1.DataBind();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    report_container.Visible = true;
                    cmdExporttoExcel.Visible = true;
                    Label2.Text = ds.Tables[0].Rows.Count + " Record Found";
                }
                else
                {
                    Label2.Text = "No Data Found";
                    cmdExporttoExcel.Visible = false;
                }
            }
        }

        protected void cmdExporttoExcel_Click(object sender, EventArgs e)
        {

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=GatePassRequistionReport.xls");
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stwr = new StringWriter();
            HtmlTextWriter htew = new HtmlTextWriter(stwr);
            GridView1.RenderControl(htew);
            Response.Write(stwr.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void ddltypeofreport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string sSelectedReportType = ddltypeofreport.SelectedValue;
                switch (sSelectedReportType)
                {
                    case "":
                        pnlArea.Visible = false;
                        pnlDateWise.Visible = false;
                        pnlEmployee.Visible = false;
                        pnlReqType.Visible = false;
                        cmdExporttoExcel.Visible = false;
                        cmdgenerate.Visible = false;
                        report_container.Visible = false;
                        break;
                    case "DATEWISE":
                        pnlArea.Visible = false;
                        pnlDateWise.Visible = true;
                        pnlEmployee.Visible = false;
                        pnlReqType.Visible = false;
                        cmdExporttoExcel.Visible = false;
                        cmdgenerate.Visible = true;
                        report_container.Visible = false;
                        break;
                    case "REQTYPE":
                        pnlArea.Visible = false;
                        pnlDateWise.Visible = false;
                        pnlEmployee.Visible = false;
                        pnlReqType.Visible = true;
                        cmdExporttoExcel.Visible = false;
                        cmdgenerate.Visible = true;
                        report_container.Visible = false;
                        break;
                    case "AREAWISE":
                        pnlArea.Visible = true;
                        pnlDateWise.Visible = false;
                        pnlEmployee.Visible = false;
                        pnlReqType.Visible = false;
                        cmdExporttoExcel.Visible = false;
                        cmdgenerate.Visible = true;
                        report_container.Visible = false;
                        break;
                    case "OFFICERWISE":
                        pnlArea.Visible = false;
                        pnlDateWise.Visible = false;
                        pnlReqType.Visible = false;
                        cmdExporttoExcel.Visible = false;
                        cmdgenerate.Visible = true;
                        report_container.Visible = false;
                        string sEmpListQuery = "SELECT [O_CPF_NMBR] AS [CPF_NO],LTRIM(RTRIM([O_EMP_NM])) AS [EMPLOYEE_NAME] FROM [O_EMP_MSTR] ORDER BY [EMPLOYEE_NAME] ASC";
                        DataSet dsEmpList = My.ExecuteSELECTQuery(sEmpListQuery);
                        if (dsEmpList.Tables[0].Rows.Count == 0)
                        {
                            Response.Write("<Script language='JavaScript'>" + "alert('No Employee Found')" + "</Script>");
                            return;
                        }
                        else
                        {
                            pnlEmployee.Visible = true;
                            ddlEmployee.Items.Clear();
                            ddlEmployee.Items.Add(new ListItem("Select Employee", ""));

                            for (int i = 0; i < dsEmpList.Tables[0].Rows.Count; i++)
                            {
                                string sEmpCPF = dsEmpList.Tables[0].Rows[i][0].ToString();
                                string sEmpName = dsEmpList.Tables[0].Rows[i][1].ToString();
                                ddlEmployee.Items.Add(new ListItem(sEmpName, sEmpCPF));
                            }

                        }
                        break;

                }

            }
        }
        
    }
}
