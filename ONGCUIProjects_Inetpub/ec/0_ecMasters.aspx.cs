﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec
{
    public partial class _0_ecMasters : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();


        protected void Page_Load(object sender, EventArgs e)
        {
            string mastertype="";
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
               
            }

            if (String.IsNullOrEmpty(Request.QueryString["MasterType"]))
            {
                lblMasterTypeHeading.Text = "Please select type";
            }
            else
            {
                mastertype=Request.QueryString["MasterType"].ToString();
                lblmodule_hint.Text = Request.QueryString["MasterType"].ToString();
                lblMasterTypeHeading.Text = Request.QueryString["MasterType"].ToString() + " Details";
            }
           
                switch (mastertype)
                {
                    #region Location Details
                    case "Location":
                        try
                        {
                            string sLocationQuery = "SELECT [ID], [AREA_NAME], [LOCATION_NAME], [LOCATION_MANAGER], [ENTRY_BY], [TIMESTAMP] FROM [CY_EC_0_LOCATION_MSTR]  ORDER BY [AREA_NAME] ASC, [LOCATION_NAME] ASC";
                            DataSet dsLocationDetail = My.ExecuteSELECTQuery(sLocationQuery);

                            if (dsLocationDetail.Tables[0].Rows.Count == 0)
                            {
                                divTableData.InnerHtml = "<h1 style=\"width: 100%; text-align:center;\" class=\"heading\">No Area Location Added</h1>";

                            }
                            else
                            {

                                string sTableData = "<table class=\"IncTable\">";
                                sTableData = sTableData + "<tbody>";
                                sTableData = sTableData + "<tr>";
                                sTableData = sTableData + "<th>Area Name</th>";
                                sTableData = sTableData + "<th>Location Name</th>";
                                sTableData = sTableData + "<th>Area Manager Name (CPF No)</th>";
                                sTableData = sTableData + "<th style=\"width: 1px;\">Action</th>";
                                sTableData = sTableData + "</tr>";
                                ADDNEWDATA.Text = "Add New Location";

                                for (int i = 0; i < dsLocationDetail.Tables[0].Rows.Count; i++)
                                {
                                    string sLocationID = dsLocationDetail.Tables[0].Rows[i][0].ToString();
                                    string sAreaName = dsLocationDetail.Tables[0].Rows[i][1].ToString();
                                    string sLocationName = dsLocationDetail.Tables[0].Rows[i][2].ToString();
                                    string sManagerCPF = dsLocationDetail.Tables[0].Rows[i][3].ToString();
                                    string sEntryBy = dsLocationDetail.Tables[0].Rows[i][4].ToString();
                                    DateTime sEntryDateTime = System.Convert.ToDateTime(dsLocationDetail.Tables[0].Rows[i][5].ToString());
                                    string sManagerDtl = "";
                                    if (sManagerCPF != "")
                                    {
                                        sManagerDtl = My.GetEmployeeNameFromCPFNo(sManagerCPF) + " (" + sManagerCPF + ")";
                                    }
                                    else
                                    {
                                        sManagerDtl = "";
                                    }

                                    string sToolTip = "Entered by " + My.GetEmployeeNameFromCPFNo(sEntryBy) + " on " + sEntryDateTime.ToLongDateString() + " at " + sEntryDateTime.ToLongTimeString();

                                    sTableData = sTableData + "<tr title=\"" + sToolTip + "\">";
                                    sTableData = sTableData + "<td>" + sAreaName + "</td>";
                                    sTableData = sTableData + "<td>" + sLocationName + "</td>";
                                    sTableData = sTableData + "<td>" + sManagerDtl + "</td>";
                                    sTableData = sTableData + "<td><input My_ID=\"" + sLocationID + "\" type=\"button\" value=\"delete\" class=\"g-button g-button-red cmdDeleteLocation\" /></td>";
                                    sTableData = sTableData + "</tr>";
                                }

                                sTableData = sTableData + "</tbody>";
                                sTableData = sTableData + "</table>";

                                divTableData.InnerHtml = sTableData;
                            }
                        }
                        catch (Exception ex)
                        {
                            divTableData.InnerHtml = ex.ToString();
                        }
                        break;
                    #endregion
                    #region Incident Category
                    case "Category" :
                        string sCategory = "SELECT [ID],[CATEGORY_NAME],[ENTRY_BY],[TIMESTAMP] FROM [CY_EC_0_CATEGORY_MSTR] ORDER BY [CATEGORY_NAME] ASC";
                        DataSet dssCategory = My.ExecuteSELECTQuery(sCategory);
                        txt_safety_name.Visible = false;

                        if (dssCategory.Tables[0].Rows.Count == 0)
                        {
                            divTableData.InnerHtml = "<h1 style=\"width: 100%; text-align:center;\" class=\"heading\">No Category Added</h1>";

                        }
                        else
                        {

                            string sTableData = "<table class=\"IncTable\">";
                            sTableData = sTableData + "<tbody>";
                            sTableData = sTableData + "<tr>";
                            sTableData = sTableData + "<th>Category Name</th>";
                            sTableData = sTableData + "<th style=\"width: 1px;\">Action</th>";
                            sTableData = sTableData + "</tr>";
                            ADDNEWDATA.Text = "Add New Category";
                           

                            for (int i = 0; i < dssCategory.Tables[0].Rows.Count; i++)
                            {
                                string sCategoryID = dssCategory.Tables[0].Rows[i][0].ToString();
                                string sCategoryName = dssCategory.Tables[0].Rows[i][1].ToString();

                                string sEntryBy = dssCategory.Tables[0].Rows[i][2].ToString();
                                DateTime sEntryDateTime = System.Convert.ToDateTime(dssCategory.Tables[0].Rows[i][3].ToString());

                                string sToolTip = "";
                                if (string.IsNullOrEmpty(sEntryBy))
                                {
                                    sToolTip = "";
                                }
                                else
                                {
                                    sToolTip = "Entered by " + My.GetEmployeeNameFromCPFNo(sEntryBy) + " on " + sEntryDateTime.ToLongDateString() + " at " + sEntryDateTime.ToLongTimeString();
                                }

                                sTableData = sTableData + "<tr title=\"" + sToolTip + "\">";
                                sTableData = sTableData + "<td>" + sCategoryName + "</td>";
                                sTableData = sTableData + "<td><input  My_ID=\"" + sCategoryID + "\" type=\"button\" value=\"delete\" class=\"g-button g-button-red cmdDeleteLocation\" /></td>";
                                sTableData = sTableData + "</tr>";
                            }

                            sTableData = sTableData + "</tbody>";
                            sTableData = sTableData + "</table>";

                            divTableData.InnerHtml = sTableData;
                        }
                        break;
                    #endregion
                    #region Safety Precaution
                    case "Safety":
                        string sSafety = "SELECT [O_INDX_NMBR],[O_SFTY_MSR],[CY_ENTRY_BY],[O_SYS_DT_TM] FROM [CY_EC_0_SAFETY_MSTR] ORDER BY [O_SFTY_MSR] ASC";
                        DataSet dssSafety = My.ExecuteSELECTQuery(sSafety);
                        txt_my_data.Visible = false;

                        if (dssSafety.Tables[0].Rows.Count == 0)
                        {
                            divTableData.InnerHtml = "<h1 style=\"width: 100%; text-align:center;\" class=\"heading\">No Safety Added</h1>";

                        }
                        else
                        {

                            string sTableData = "<table class=\"IncTable\">";
                            sTableData = sTableData + "<tbody>";
                            sTableData = sTableData + "<tr>";
                            sTableData = sTableData + "<th>Safety Name</th>";
                            sTableData = sTableData + "<th style=\"width: 1px;\">Action</th>";
                            sTableData = sTableData + "</tr>";
                            ADDNEWDATA.Text = "Add New Safety";

                            for (int i = 0; i < dssSafety.Tables[0].Rows.Count; i++)
                            {
                                string sSafetyID = dssSafety.Tables[0].Rows[i][0].ToString();
                                string sSafetyName = dssSafety.Tables[0].Rows[i][1].ToString();

                                string sEntryBy = dssSafety.Tables[0].Rows[i][2].ToString();
                                DateTime sEntryDateTime = System.Convert.ToDateTime(dssSafety.Tables[0].Rows[i][3].ToString());
                                string sToolTip = "";
                                if (string.IsNullOrEmpty(sEntryBy))
                                {
                                     sToolTip = "";
                                }
                                else {
                                     sToolTip = "Entered by " + My.GetEmployeeNameFromCPFNo(sEntryBy) + " on " + sEntryDateTime.ToLongDateString() + " at " + sEntryDateTime.ToLongTimeString();
                                }
                               

                                sTableData = sTableData + "<tr title=\"" + sToolTip + "\">";
                                sTableData = sTableData + "<td>" + sSafetyName + "</td>";
                                sTableData = sTableData + "<td><input  My_ID=\"" + sSafetyID + "\" type=\"button\" value=\"delete\" class=\"g-button g-button-red cmdDeleteLocation\" /></td>";
                                sTableData = sTableData + "</tr>";
                            }

                            sTableData = sTableData + "</tbody>";
                            sTableData = sTableData + "</table>";

                            divTableData.InnerHtml = sTableData;
                        }
                        break;
                    #endregion
                    #region Incident Type
                    case "Type":
                        string sType = "SELECT[O_EC_ID] ,[O_EC_CTGRY] ,[CY_ENTRY_BY],[O_SYS_DT_TM] FROM [CY_EC_0_TYPE_MSTR] ORDER BY [O_EC_CTGRY] ASC";
                        DataSet dssType = My.ExecuteSELECTQuery(sType);
                        txt_safety_name.Visible = false;
                        if (dssType.Tables[0].Rows.Count == 0)
                        {
                            divTableData.InnerHtml = "<h1 style=\"width: 100%; text-align:center;\" class=\"heading\">No Category Added</h1>";

                        }
                        else
                        {

                            string sTableData = "<table class=\"IncTable\">";
                            sTableData = sTableData + "<tbody>";
                            sTableData = sTableData + "<tr>";
                            sTableData = sTableData + "<th>Type Name</th>";
                            sTableData = sTableData + "<th style=\"width: 1px;\">Action</th>";
                            sTableData = sTableData + "</tr>";
                            ADDNEWDATA.Text = "Add New Type";

                            for (int i = 0; i < dssType.Tables[0].Rows.Count; i++)
                            {
                                string sTypeID = dssType.Tables[0].Rows[i][0].ToString();
                                string sTypeName = dssType.Tables[0].Rows[i][1].ToString();

                                string sEntryBy = dssType.Tables[0].Rows[i][2].ToString();
                                DateTime sEntryDateTime = System.Convert.ToDateTime(dssType.Tables[0].Rows[i][3].ToString());

                                string sToolTip = "";
                                if (string.IsNullOrEmpty(sEntryBy))
                                {
                                    sToolTip = "";
                                }
                                else
                                {
                                    sToolTip = "Entered by " + My.GetEmployeeNameFromCPFNo(sEntryBy) + " on " + sEntryDateTime.ToLongDateString() + " at " + sEntryDateTime.ToLongTimeString();
                                }

                                sTableData = sTableData + "<tr title=\"" + sToolTip + "\">";
                                sTableData = sTableData + "<td>" + sTypeName + "</td>";
                                sTableData = sTableData + "<td><input My_ID=\"" + sTypeID + "\" type=\"button\" value=\"delete\" class=\"g-button g-button-red cmdDeleteLocation\" /></td>";
                                sTableData = sTableData + "</tr>";
                            }

                            sTableData = sTableData + "</tbody>";
                            sTableData = sTableData + "</table>";

                            divTableData.InnerHtml = sTableData;
                        }
                        break;
                    #endregion
                    default:
                        ADDNEWDATA.Visible = false;
                        break;
                } 
        }
    }
}