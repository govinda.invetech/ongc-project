﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec
{
    public partial class _1_ecReporting : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        int count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            #region Incident Details
            string sIncdentDetailsQuery = "SELECT [O_INDX_NMBR],[CY_TYPE_OF_EC],[O_EC_DESCRPTN], [O_EC_DT], [O_EC_TIME], [CY_STATUS_LEVEL] FROM [CY_EC_1_DETAILS] WHERE [CY_ENTRY_BY] = '" + Session["Login_Name"].ToString() + "' AND [CY_STATUS_LEVEL] != '10' ORDER BY [O_EC_DT] DESC, [O_EC_TIME] DESC";

            DataSet dsIncdentDetails = My.ExecuteSELECTQuery(sIncdentDetailsQuery);
            if (dsIncdentDetails.Tables[0].Rows.Count == 0)
            {
                divTableData.InnerHtml = "<h1 style=\"text-align:center;\">No Energy Conservations Reported yet</h1>";
            }
            else
            {
                string sIncdentTableData = "";
                for (int i = 0; i < dsIncdentDetails.Tables[0].Rows.Count; i++)
                {
                    string sIncdentStatus = dsIncdentDetails.Tables[0].Rows[i][5].ToString();
                    if (sIncdentStatus == "7" || sIncdentStatus == "10")
                  {
                       
                    }
                    else
                    {
                        count = count + 1;
                        string sIncdentID = dsIncdentDetails.Tables[0].Rows[i][0].ToString();
                        string sType = dsIncdentDetails.Tables[0].Rows[i][1].ToString();
                        string sIncdentDescription = dsIncdentDetails.Tables[0].Rows[i][2].ToString();
                        DateTime sIncdentDate = System.Convert.ToDateTime(dsIncdentDetails.Tables[0].Rows[i][3]);
                        string sIncdentTime = dsIncdentDetails.Tables[0].Rows[i][4].ToString();
                        sIncdentTableData = sIncdentTableData + "<tr>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sType + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDate.ToString("d MMM, yyyy") + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sIncdentTime + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDescription + "</td>";
                        sIncdentTableData = sIncdentTableData + "<td>" + My.EC_getStatusStringFromLevel(sIncdentStatus) + "</td>";
                        if (sIncdentStatus == "1")
                        {

                        }
                        else
                        {
                            sIncdentTableData = sIncdentTableData + "<td><a IncdID=\"" + sIncdentID + "\" class=\"IncdDtlFancyBox\" href=\"fancybox/ecDetails.aspx?IncdID=" + sIncdentID + "\">View Details</a></td>";
                        }
                        sIncdentTableData = sIncdentTableData + "</tr>";
                    }

                }
                if (count > 0)
                {
                    string sTableHeading = "<tr><th>Type</th><th>Date</th><th>Time</th><th>Description</th><th colspan=\"2\">Status</th></tr>";
                    divTableData.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading + sIncdentTableData + "</table>";
                }
                else
                {
                    divTableData.InnerHtml = "<h2>You have not any Energy Conservation or Near Miss</h2>";
                }
            }
            #endregion
        }
    }
}