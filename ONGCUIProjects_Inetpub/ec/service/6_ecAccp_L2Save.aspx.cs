﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec.service
{
    public partial class _6_ecAccp_L2Save : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            //"IncdntNo=" + sIncdntNo + "&LevelComment=" + sLevelComment + "&Decision=" + sDecision,
            string sIncdntNo = Request.Form["IncdntNo"].ToString();
            string sLevelComment = Request.Form["LevelComment"].ToString();
            string sDecision = Request.Form["Decision"].ToString();

            string sCurrentUserCPF = Session["Login_name"].ToString();
            string sCurrentUserName = Session["EmployeeName"].ToString();

            ecClass CurrentIncident = new ecClass(sIncdntNo);

            string sInsert = " INSERT INTO [CY_EC_6_ACPTN_LVL2_DTL]([O_EC_NMBR],[O_EC_DESC],[O_EC_DECSN],[O_SELF_CMNT],[O_ACTN_TKN],[O_REV_RMRKS],[O_USR_LGN_NM],[O_USR_CPF_NMBR],[O_SYS_DT_TM]) VALUES";
            sInsert = sInsert + "('" + sIncdntNo + "','" + CurrentIncident.RptDesc + "','" + sDecision + "','" + sLevelComment + "','" + CurrentIncident.IncMgrActnToBeTaken + "','" + CurrentIncident.RevRemark + "','" + sCurrentUserName + "','" + sCurrentUserCPF + "',getdate())";
            if (My.ExecuteSQLQuery(sInsert) == true)
            {
                switch (sDecision)
                {
                    case "CLOSE":
                        if (My.EC_UpdateIncidentStatus(sIncdntNo, 10) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Closed Successfully");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                        }
                        break;
                    case "REVISE":
                        if (My.EC_UpdateIncidentStatus(sIncdntNo, 11) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Incident forwarded for Revise Action");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                        }
                        break;
                }
            }
        }
    }
}