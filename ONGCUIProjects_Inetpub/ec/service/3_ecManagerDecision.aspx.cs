﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec.service
{
    public partial class _3_ecManagerDecision : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        #region Variables
        string sSelectQuery = "";
        string sInsertQuery = "";
        DataSet dsLastRecord;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncdntNo = Request.Form["IncdntNo"];

            string sSessionCPF = Session["Login_Name"].ToString();
            string sSessionName = Session["EmployeeName"].ToString();

            ecClass CurrentIncident = new ecClass(sIncdntNo);

            string sDecision = Request.Form["Decision"];
            string sIncdMgrComment = Request.Form["IncdMgrComment"];

            string sAuthorityCPFNo = Request.Form["AuthorityCPFNo"];
            string sAuthorityName = Request.Form["AuthorityName"];

            try
            {
                if (sDecision == "FWDUPLEVEL")
                {
                    #region Status = 4
                    if (CurrentIncident.StatusLevel == "4")
                    {
                        sInsertQuery = "INSERT INTO [CY_EC_3_ACTION_DTL] ([O_EC_NMBR], [O_ACTN_NM], [O_ACTN_CPF_NMBR], [O_EC_DESC], [O_ACTN_TAKN], [O_RMRKS], [O_ACPTNC_NM], [O_ACPTNC_CPF_NMBR], [O_SYS_DT_TM], [O_ACTN_TRGT_DT], [CY_ACTN_STATUS]) VALUES ";
                        sInsertQuery = sInsertQuery + "('" + sIncdntNo + "','" + sSessionName + "','" + sSessionCPF + "','" + CurrentIncident.RptDesc + "','" + CurrentIncident.IncMgrActnToBeTaken + "','" + sIncdMgrComment + "','" + sAuthorityName + "','" + sAuthorityCPFNo + "', getdate() ,'" + My.ConvertDateintoSQLDateString( CurrentIncident.IncTrgtDate) + "','FWDUPLEVEL')";
                        if (My.EC_CheckAndInsertRelation(sIncdntNo, sAuthorityCPFNo, "LEVEL1", sSessionCPF) == true)
                        {
                            if (My.EC_UpdateActionStatus(sIncdntNo, "FPRDONE", "FPRTOBE") == true)
                            {
                                if (My.ExecuteSQLQuery(sInsertQuery) == true)
                                {
                                    if (My.EC_UpdateIncidentStatus(sIncdntNo, 5) == true)
                                    {
                                        Response.Write("TRUE~SUCCESS~Forwarded to Acceptance Authority");
                                    }
                                    else
                                    {
                                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                                    }
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                                }
                            }

                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                            }
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                        }
                    }
                    #endregion

                    #region Status = 9
                    else if (CurrentIncident.StatusLevel == "9")
                    {
                        sSelectQuery = "";
                        sSelectQuery = "SELECT [O_ACPTNC_NM],[O_ACPTNC_CPF_NMBR] FROM [CY_EC_3_ACTION_DTL] WHERE [CY_ACTN_STATUS] = 'FWDUPLEVEL' AND [O_EC_NMBR] = '" + sIncdntNo + "'";
                        dsLastRecord=new DataSet();
                        dsLastRecord = My.ExecuteSELECTQuery(sSelectQuery);

                        string sLastFWDLevelName = dsLastRecord.Tables[0].Rows[0][0].ToString();
                        string sLastFWDLevelCPFNo = dsLastRecord.Tables[0].Rows[0][1].ToString();

                        sInsertQuery = "INSERT INTO [CY_EC_3_ACTION_DTL] ([O_EC_NMBR], [O_ACTN_NM], [O_ACTN_CPF_NMBR], [O_EC_DESC], [O_ACTN_TAKN], [O_RMRKS], [O_ACPTNC_NM], [O_ACPTNC_CPF_NMBR], [O_SYS_DT_TM], [O_ACTN_TRGT_DT], [CY_ACTN_STATUS]) VALUES ";
                        sInsertQuery = sInsertQuery + "('" + sIncdntNo + "','" + sSessionName + "','" + sSessionCPF + "','" + CurrentIncident.RptDesc + "','" + CurrentIncident.IncMgrActnToBeTaken + "','" + sIncdMgrComment + "','" + sLastFWDLevelName + "','" + sLastFWDLevelCPFNo + "', getdate() ,'" + CurrentIncident.IncTrgtDate + "','FWDUPLEVEL')";

                        if (My.EC_UpdateActionStatus(sIncdntNo, "FWDUPLEVELREVISE", "FWDUPLEVEL") == true)
                        {
                            if (My.ExecuteSQLQuery(sInsertQuery) == true)
                            {
                                if (My.EC_UpdateIncidentStatus(sIncdntNo, 5) == true)
                                {
                                    Response.Write("TRUE~SUCCESS~Re-Forwarded to Acceptance Authority");
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                                }
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                            }
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                        }


                    }
                    #endregion
                }
                else if (sDecision == "REVISE")
                {
                    #region Status = 4
                    if (CurrentIncident.StatusLevel == "4")
                    {
                        sSelectQuery = "";
                        sSelectQuery = "SELECT [O_ACPTNC_NM],[O_ACPTNC_CPF_NMBR] FROM [CY_EC_3_ACTION_DTL] WHERE [CY_ACTN_STATUS] = 'FPRTOBE' AND [O_EC_NMBR] = '" + sIncdntNo + "'";
                        dsLastRecord = new DataSet();
                        dsLastRecord = My.ExecuteSELECTQuery(sSelectQuery);

                        string sLastFPRName = dsLastRecord.Tables[0].Rows[0][0].ToString();
                        string sLastFPRCPFNo = dsLastRecord.Tables[0].Rows[0][1].ToString();

                        sInsertQuery = "INSERT INTO [CY_EC_3_ACTION_DTL] ([O_EC_NMBR], [O_ACTN_NM], [O_ACTN_CPF_NMBR], [O_EC_DESC], [O_ACTN_TAKN], [O_RMRKS], [O_ACPTNC_NM], [O_ACPTNC_CPF_NMBR], [O_SYS_DT_TM], [O_ACTN_TRGT_DT]) VALUES ";
                        sInsertQuery = sInsertQuery + "('" + sIncdntNo + "','" + sSessionName + "','" + sSessionCPF + "','" + CurrentIncident.RptDesc + "','" + CurrentIncident.IncMgrActnToBeTaken + "','" + sIncdMgrComment + "','" + sLastFPRName + "','" + sLastFPRCPFNo + "', getdate() ,'" + My.ConvertDateintoSQLDateString(CurrentIncident.IncTrgtDate) + "')";

                        if (My.EC_UpdateActionStatus(sIncdntNo, "FPRREVISE", "FPRTOBE") == true)
                        {
                            if (My.ExecuteSQLQuery(sInsertQuery) == true)
                            {
                                if (My.EC_UpdateIncidentStatus(sIncdntNo, 6) == true)
                                {
                                    Response.Write("TRUE~SUCCESS~Task Successfully Re-Assigned");
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                                }
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                            }
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                        }
                    }
                    #endregion

                    #region Status = 9
                    else if (CurrentIncident.StatusLevel == "9")
                    {
                        sSelectQuery = "";
                        sSelectQuery = "SELECT [O_ACPTNC_NM],[O_ACPTNC_CPF_NMBR] FROM [CY_EC_3_ACTION_DTL] WHERE [CY_ACTN_STATUS] = 'FPRDONE' AND [O_EC_NMBR] = '" + sIncdntNo + "'";
                        dsLastRecord = new DataSet();
                        dsLastRecord = My.ExecuteSELECTQuery(sSelectQuery);

                        string sLastFPRName = dsLastRecord.Tables[0].Rows[0][0].ToString();
                        string sLastFPRCPFNo = dsLastRecord.Tables[0].Rows[0][1].ToString();

                        sInsertQuery = "INSERT INTO [CY_EC_3_ACTION_DTL] ([O_EC_NMBR], [O_ACTN_NM], [O_ACTN_CPF_NMBR], [O_EC_DESC], [O_ACTN_TAKN], [O_RMRKS], [O_ACPTNC_NM], [O_ACPTNC_CPF_NMBR], [O_SYS_DT_TM], [O_ACTN_TRGT_DT]) VALUES ";
                        sInsertQuery = sInsertQuery + "('" + sIncdntNo + "','" + sSessionName + "','" + sSessionCPF + "','" + CurrentIncident.RptDesc + "','" + CurrentIncident.IncMgrActnToBeTaken + "','" + sIncdMgrComment + "','" + sLastFPRName + "','" + sLastFPRCPFNo + "', getdate() ,'" + My.ConvertDateintoSQLDateString( CurrentIncident.IncTrgtDate) + "')";

                        if (My.EC_UpdateActionStatus(sIncdntNo, "FWDUPLEVELREVISE", "FWDUPLEVEL") == true)
                        {
                            if (My.EC_UpdateActionStatus(sIncdntNo, "FPRREVISE", "FPRDONE") == true)
                            {
                                if (My.ExecuteSQLQuery(sInsertQuery) == true)
                                {
                                    if (My.EC_UpdateIncidentStatus(sIncdntNo, 6) == true)
                                    {
                                        Response.Write("TRUE~SUCCESS~Task Successfully Re-Assigned");
                                    }
                                    else
                                    {
                                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                                    }
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                                }
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                            }
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *MGRDECISION");
                        }

                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Response.Write("FALSE~ERR~" + ex.Message);
            }

        }
    }
}