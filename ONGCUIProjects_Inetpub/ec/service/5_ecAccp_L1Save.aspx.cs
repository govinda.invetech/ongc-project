﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec.service
{
    public partial class _5_ecAccp_L1Save : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            //"IncdntNo=" + sIncdntNo + "&LevelComment=" + sLevelComment + "&Decision=" + sDecision + "&AuthorityCPFNo=" + sAuthorityCPFNo + "&AuthorityName=" + sAuthorityName
            try
            {
                string sIncdntNo = Request.Form["IncdntNo"];

                ecClass CurrentIncdnt = new ecClass(sIncdntNo);

                string sLevelComment = Request.Form["LevelComment"];
                string sDecision = Request.Form["Decision"];
                string sAuthorityCPFNo = Request.Form["AuthorityCPFNo"];
                string sAuthorityName = Request.Form["AuthorityName"];

                string sSessionEmpCPF = Session["Login_Name"].ToString();
                string sSessionEmpName = Session["EmployeeName"].ToString();

                switch (sDecision)
                {
                    case "CLOSE":
                        if (My.EC_UpdateIncidentStatus(sIncdntNo, 7) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Closed Successfully");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                        }
                        break;
                    case "REVISE":
                        string sStr = "INSERT INTO [CY_EC_5_ACPTN_LVL1_DTL] ([O_EC_NMBR],[O_EC_DESC],[O_SELF_CMNT],[O_EC_DECSN],[O_ACPTN2_NM],[O_ACPTN2_CPF_NMBR],[O_ACPTN1_NM],[O_ACPTN1_CPF_NMBR],[O_SYS_DT_TM])VALUES ";
                        sStr = sStr + "('" + sIncdntNo + "','" + CurrentIncdnt.RptDesc + "','" + sLevelComment + "','" + sDecision + "','" + sAuthorityName + "','" + sAuthorityCPFNo + "','" + sSessionEmpName + "','" + sSessionEmpCPF + "',getdate())";
                        if (My.ExecuteSQLQuery(sStr) == true)
                        {
                            if (My.EC_UpdateIncidentStatus(sIncdntNo, 9) == true)
                            {
                                Response.Write("TRUE~SUCCESS~Incident forwarded for Revise Action");
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                            }

                        }
                        break;
                    case "FWDUPLEVEL":
                        string sStr1 = "INSERT INTO [CY_EC_5_ACPTN_LVL1_DTL] ([O_EC_NMBR],[O_EC_DESC],[O_SELF_CMNT],[O_EC_DECSN],[O_ACPTN2_NM],[O_ACPTN2_CPF_NMBR],[O_ACPTN1_NM],[O_ACPTN1_CPF_NMBR],[O_SYS_DT_TM])VALUES ";
                        sStr1 = sStr1 + "('" + sIncdntNo + "','" + CurrentIncdnt.RptDesc + "','" + sLevelComment + "','" + sDecision + "','" + sAuthorityName + "','" + sAuthorityCPFNo + "','" + sSessionEmpName + "','" + sSessionEmpCPF + "',getdate())";
                        if (My.ExecuteSQLQuery(sStr1) == true)
                        {
                            if (My.EC_CheckAndInsertRelation(sIncdntNo, sAuthorityCPFNo, "LEVEL2", sSessionEmpCPF) == true)
                            {
                                if (My.EC_UpdateIncidentStatus(sIncdntNo, 8) == true)
                                {
                                    Response.Write("TRUE~SUCCESS~Incident forwarded for Final Acceptance");
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                                }
                            }
                            else
                            {
                                Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                            }
                        }

                        break;
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                Response.Write("FALSE~ERR~" + ex.Message);
            }
        }
    }
}