﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec.service
{
    public partial class _3_ecManagerSave : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sO_ACTN_CPF_NMBR = Session["Login_Name"].ToString();
            string sO_ACTN_NM = Session["EmployeeName"].ToString();
            string sINCIDENT_NMBR = Request.Form["INCIDENT_NMBR"];
            string sINCDNT_DESC = Request.Form["INCDNT_DESC"];
            string sACTN_TAKN = Request.Form["ACTN_TAKN"];
            string sRMRKS = Request.Form["RMRKS"];
            string sACPTNC_NM = Request.Form["ACPTNC_NM"];
            string sACPTNC_CPF_NMBR = Request.Form["ACPTNC_CPF_NMBR"];
            string sACTN_TRGT_DT = Request.Form["ACTN_TRGT_DT"].ToString();
            string sFPRTeamMembers = Request.Form["FPRTeamMembers"];

            string sInsertQuery1 = "INSERT INTO [CY_EC_3_ACTION_DTL] ([O_EC_NMBR], [O_ACTN_NM], [O_ACTN_CPF_NMBR], [O_EC_DESC], [O_ACTN_TAKN], [O_RMRKS], [O_ACPTNC_NM], [O_ACPTNC_CPF_NMBR], [O_SYS_DT_TM], [O_ACTN_TRGT_DT]) VALUES ";
            sInsertQuery1 = sInsertQuery1 + "('" + sINCIDENT_NMBR + "','" + sO_ACTN_NM + "','" + sO_ACTN_CPF_NMBR + "','" + sINCDNT_DESC + "','" + sACTN_TAKN + "','" + sRMRKS + "','" + sACPTNC_NM + "','" + sACPTNC_CPF_NMBR + "', getdate() ,'" + My.ConvertDateStringintoSQLDateString(sACTN_TRGT_DT) + "')";
            try
            {
                if (My.ExecuteSQLQuery(sInsertQuery1) == true)
                {
                    if (My.EC_CheckAndInsertRelation(sINCIDENT_NMBR, sACPTNC_CPF_NMBR, "FPR", sO_ACTN_CPF_NMBR) == true)
                    {
                        if (sFPRTeamMembers != "")
                        {

                            string sInsertQuery2 = "";
                            string[] sFPRTeamMembersArr = sFPRTeamMembers.Split('`');


                            for (int i = 0; i < sFPRTeamMembersArr.Length; i++)
                            {
                                string sMemberCPFNo = My.sDataStringTrim(1, sFPRTeamMembersArr[i]);
                                string sMemberName = My.sDataStringTrim(2, sFPRTeamMembersArr[i]);
                                sInsertQuery2 = sInsertQuery2 + "('" + sINCIDENT_NMBR + "', '" + sMemberName + "', '" + sMemberCPFNo + "', getdate()),";
                            }
                            if (sInsertQuery2 == "")
                            {
                                Response.Write("TRUE~SUCCESS~Task Successfully Assigned to FPR");
                            }
                            else
                            {
                                sInsertQuery2 = "INSERT INTO CY_EC_3_ACTN_MEMBR_TEAM_DTL ([O_EC_NMBR], [O_EMP_NM], [O_CPF_NMBR], [O_SYS_DT_TM]) VALUES " + sInsertQuery2.Substring(0, sInsertQuery2.Length - 1);
                                if (My.ExecuteSQLQuery(sInsertQuery2) == true)
                                {
                                    if (My.EC_UpdateIncidentStatus(sINCIDENT_NMBR, 3) == true)
                                    {
                                        Response.Write("TRUE~SUCCESS~Task Successfully Assigned");
                                    }
                                    else
                                    {
                                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                                    }
                                }
                                else
                                {
                                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                                }
                            }

                        }
                        else
                        {
                            Response.Write("TRUE~SUCCESS~Task Successfully Assigned");
                        }
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                    }
                }
                else //else case of execute insert query
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends
        }
    }
}