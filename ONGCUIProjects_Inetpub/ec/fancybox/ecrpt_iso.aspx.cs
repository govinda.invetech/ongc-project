﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class ecrpt_iso : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncdentID = Request.QueryString["IncdID"];
            //                                  0            1           2            3             4                   5           6          
            string sIncdentQuery = "SELECT [O_INDX_NMBR],[O_ENG_NM],[O_ENG_DSG],[O_CPF_NMBR],[O_EC_DESCRPTN],[O_EC_DT],[O_EC_TIME],";
            //                                      7               8             9         10          11          12          13          14                  15                     16     
            sIncdentQuery = sIncdentQuery + "[O_EC_LCTN],[O_RUSH_EMRGNCY],[O_RMRKS],[O_SYS_DT],[O_SYS_TIME],[CY_O_AREA],[CY_O_DEPT],[CY_O_APP_ACC],[CY_O_IMD_REM_ACTION],[CY_TYPE_OF_EC],";
            //                                        17         18             19                  20
            sIncdentQuery = sIncdentQuery + "  [CY_PROPERTY],[CY_PROCESS],[CY_ENVIRONMENT],[CY_PERSONAL_INJURY] FROM [CY_EC_1_DETAILS] WHERE [O_INDX_NMBR] = '" + sIncdentID + "'";
            //dsEmployeeDtl.Tables[0].Columns["O_INDX_NMBR"].ToString();

            DataSet dsIncident = My.ExecuteSELECTQuery(sIncdentQuery);

            IncLocation.InnerText = dsIncident.Tables[0].Rows[0][7].ToString();
            IncArea.InnerText = dsIncident.Tables[0].Rows[0][12].ToString();
            IncDept.InnerText = dsIncident.Tables[0].Rows[0][13].ToString();

            DateTime dtIncdDate = Convert.ToDateTime(dsIncident.Tables[0].Rows[0][5].ToString());
            DateTime dtIncdTime = Convert.ToDateTime(dsIncident.Tables[0].Rows[0][6].ToString());

            IncDateNTime.InnerText = dtIncdDate.ToString("dd/MM/yyyy")+" "+dtIncdTime.ToString("HH:mm");
            IncReportedBy.InnerText = "[" + dsIncident.Tables[0].Rows[0][3].ToString() + "] [" + dsIncident.Tables[0].Rows[0][1].ToString() + "] [" + dsIncident.Tables[0].Rows[0][2].ToString() + "]";
            IncProperty.InnerText = dsIncident.Tables[0].Rows[0][17].ToString();
            IncProcess.InnerText = dsIncident.Tables[0].Rows[0][18].ToString();
            IncEnvironment.InnerText = dsIncident.Tables[0].Rows[0][19].ToString();
            IncPersonalInjury.InnerText = dsIncident.Tables[0].Rows[0][20].ToString();
            IncDesc.InnerText = dsIncident.Tables[0].Rows[0][4].ToString();
            IncApparentCause.InnerText = dsIncident.Tables[0].Rows[0][14].ToString();
            IncRemedialAction.InnerText = dsIncident.Tables[0].Rows[0][15].ToString();

            DateTime dtRptDate = Convert.ToDateTime(dsIncident.Tables[0].Rows[0][10].ToString());
            DateTime dtRptTime = Convert.ToDateTime(dsIncident.Tables[0].Rows[0][11].ToString());

            ReportingDateTime.InnerText = "Date & Time of Reporting : " + dtRptDate.ToString("dd/MM/yyyy") + " " + dtRptTime.ToString("HH:mm");

            string sInjuryDtlQuery = "SELECT [NAME_OF_PERSON], [DESIG], [CPF_NO], [NATURE_OF_INJURY] FROM [CY_EC_1_INJURY_RELATION] WHERE [EC_NO] = '" + sIncdentID + "'";
            DataSet dsInjuryDtl = My.ExecuteSELECTQuery(sInjuryDtlQuery);
            string sInjuryData = "";
            if (dsInjuryDtl.Tables[0].Rows.Count == 0)
            {
                sInjuryData = "No Injury";
            }
            else
            {

                sInjuryData = sInjuryData + "<table id=\"Table2\" class=\"rptTable\" border=\"1\" style=\"border-top-width:0; border-right-width: 0; border-left-width: 0; border-bottom-width: 0; border-width: 0;\">";
                sInjuryData = sInjuryData + "<tbody>";
                sInjuryData = sInjuryData + "<tr>";
                sInjuryData = sInjuryData + "<th style=\"font-size: 16px; font-weight: normal; padding-left: 7px; border-width: 0; border-bottom-width: 1px;\" colspan=\"5\">Details of Injured Person (s) (In case of Contractual employee, Name of contractor shall be mentioned in place of Designation & CPF NO.):</th>";
                sInjuryData = sInjuryData + "</tr>";
                sInjuryData = sInjuryData + "<tr>";
                sInjuryData = sInjuryData + "<td style=\"width: 40px; border-left-width: 0;\">";
                sInjuryData = sInjuryData + "<div class=\"div-fullwidth\">";
                sInjuryData = sInjuryData + "<p class=\"cell-content\">Sl</p>";
                sInjuryData = sInjuryData + "</div>";
                sInjuryData = sInjuryData + "</td>";
                sInjuryData = sInjuryData + "<td style=\"width: 300px;\">";
                sInjuryData = sInjuryData + "<div class=\"div-fullwidth\">";
                sInjuryData = sInjuryData + "<p class=\"cell-content\">Name</p>";
                sInjuryData = sInjuryData + "</div>";
                sInjuryData = sInjuryData + "</td>";
                sInjuryData = sInjuryData + "<td style=\"width: 208px;\">";
                sInjuryData = sInjuryData + "<div class=\"div-fullwidth\">";
                sInjuryData = sInjuryData + "<p class=\"cell-content\">Designation</p>";
                sInjuryData = sInjuryData + "</div>";
                sInjuryData = sInjuryData + "</td>";
                sInjuryData = sInjuryData + "<td style=\"width: 208px;\">";
                sInjuryData = sInjuryData + "<div class=\"div-fullwidth\">";
                sInjuryData = sInjuryData + "<p class=\"cell-content\">CPF No.</p>";
                sInjuryData = sInjuryData + "</div>";
                sInjuryData = sInjuryData + "</td>";
                sInjuryData = sInjuryData + "<td style=\"width: 208px;\">";
                sInjuryData = sInjuryData + "<div class=\"div-fullwidth\">";
                sInjuryData = sInjuryData + "<p class=\"cell-content\">Nature of Injury</p>";
                sInjuryData = sInjuryData + "</div>";
                sInjuryData = sInjuryData + "</td>";
                sInjuryData = sInjuryData + "</tr>";


                for (int i = 0; i < dsInjuryDtl.Tables[0].Rows.Count; i++)
                {
                    sInjuryData = sInjuryData + "<tr>";
                    sInjuryData = sInjuryData + "<td style=\"width: 40px; border-left-width: 0;\">";
                    sInjuryData = sInjuryData + "<div class=\"div-fullwidth\">";
                    sInjuryData = sInjuryData + "<p class=\"cell-content\">" + (i + 1) + "</p>";
                    sInjuryData = sInjuryData + "</div>";
                    sInjuryData = sInjuryData + "</td>";
                    sInjuryData = sInjuryData + "<td style=\"width: 300px;\">" + dsInjuryDtl.Tables[0].Rows[i][0] + "</td>";
                    sInjuryData = sInjuryData + "<td>" + dsInjuryDtl.Tables[0].Rows[i][1] + "</td>";
                    sInjuryData = sInjuryData + "<td>" + dsInjuryDtl.Tables[0].Rows[i][2] + "</td>";
                    sInjuryData = sInjuryData + "<td>" + dsInjuryDtl.Tables[0].Rows[i][3] + "</td>";
                    sInjuryData = sInjuryData + "</tr>";
                }
                sInjuryData = sInjuryData + "</tbody>";
                sInjuryData = sInjuryData + "</table>";

                IncInjuryDetail.InnerHtml = sInjuryData;
            }
        }
    }
}