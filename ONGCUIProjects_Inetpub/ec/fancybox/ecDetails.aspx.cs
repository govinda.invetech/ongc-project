﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec.fancybox
{
    public partial class ecDetails : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncdentID = Request.QueryString["IncdID"];

            ecClass CurrentIncident = new ecClass(sIncdentID);


            lblIncidentID.Text = sIncdentID;

            lblRushToHospital.Text = CurrentIncident.RptRushToHospital;

            txtRptEngName.Text = CurrentIncident.RptName;
            txtRptEngCPF.Text = CurrentIncident.RptCPF;
            txtRptEngDesig.Text = CurrentIncident.RptCPF;
            txtIncdntDesc.Text = CurrentIncident.RptDesc;
            txtIncdntDate.Text = CurrentIncident.RptDate.ToShortDateString();
            txtIncdntTime.Text = CurrentIncident.RptTime;
            txtIncdntLocation.Text = CurrentIncident.RptLocation;
            txtIncdntArea.Text = CurrentIncident.RptArea;
            txtIncdntDept.Text = CurrentIncident.RptDept;
            txtIncdntImmdRemedial.Text = CurrentIncident.ImmediateRemedial;
            txtProcess.Text = CurrentIncident.RptProcess;
            txtProperty.Text = CurrentIncident.RptProperty;
            txtPersonalInjury.Text = CurrentIncident.RptPersonInjury;
            txtEnvironment.Text = CurrentIncident.RptEnvironment;
            txtIncdntAppCause.Text = CurrentIncident.ApparentCause;
            lblIncdntType.Text = CurrentIncident.RptIncType;


            txtReccomendations.Text = CurrentIncident.RevRecmnd;
            txtPrimaFacieReason.Text = CurrentIncident.RevRootCause;
            txtRemarks.Text = CurrentIncident.RevRemark;

            if (CurrentIncident.RptIncType == "INCIDENT")
            {
                lblIncdntTypeHead.Text = "Review Incident";
                lblIncdntTypeLegend.Text = "Incident";
            }
            else if (CurrentIncident.RptIncType == "NEARMISS")
            {
                lblIncdntTypeHead.Text = " Review Near Miss";
                lblIncdntTypeLegend.Text = "Near Miss";
            }





            //string sAreaManager = My.GetAreaManagerFromAreaAndLocation(CurrentIncident.RptArea, CurrentIncident.RptLocation);

            //string sEmpDdlQuery = "SELECT [O_CPF_NMBR], [O_EMP_NM]+' ['+[O_CPF_NMBR]+']' AS [O_EMP_NAME_WITH_CPF] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '6'";
            //DataSet dsEmployeeDtl = My.ExecuteSELECTQuery(sEmpDdlQuery);
            //string sEmpOption = "";
            //for (int i = 0; i < dsEmployeeDtl.Tables[0].Rows.Count; i++)
            //{
            //    string sOptionSelected = "";
            //    if (My.sDataStringTrim(1, sAreaManager) == dsEmployeeDtl.Tables[0].Rows[i][0].ToString())
            //    {
            //        sOptionSelected = "selected";
            //    }
            //    sEmpOption = sEmpOption + "<option value=\"" + dsEmployeeDtl.Tables[0].Rows[i][0].ToString() + "\" " + sOptionSelected + ">" + dsEmployeeDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            //}
            //divEmpList.InnerHtml = "<p class=\"content\">Corrective Action By</p><select id=\"SelectCorrectiveActionBy\" style=\"position:relative; float:right; width:225px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">Select Employee</option>" + sEmpOption + "</select>";



            //string sSafetyDdlQuery = "SELECT [O_INDX_NMBR],[O_SFTY_MSR] FROM [CY_INC_0_SAFETY_MSTR] ORDER BY [O_SFTY_MSR] ASC";
            //DataSet dsSafetyDtl = My.ExecuteSELECTQuery(sSafetyDdlQuery);
            //string sSafetyOption = "";
            //for (int i = 0; i < dsSafetyDtl.Tables[0].Rows.Count; i++)
            //{
            //    sSafetyOption = sSafetyOption + "<option value=\"" + dsSafetyDtl.Tables[0].Rows[i][0].ToString() + "\">" + dsSafetyDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            //}
            //divSafety.InnerHtml = "<p class=\"content\">Select Safety Precaution</p><select id=\"SelectSafetyPrecaustion\" style=\"position: relative; height: auto; font-size: 16px; padding: 5px; width: 75%; float: right;\"><option value=\"\">Select Safety Precaution</option>" + sSafetyOption + "</select>";


            //string sTypeDdlQuery = "SELECT [O_INCDNT_ID],[O_INCDNT_CTGRY],[CY_ENTRY_BY],[O_SYS_DT_TM] FROM [CY_INC_0_TYPE_MSTR] ORDER BY [O_INCDNT_CTGRY] ASC";
            //DataSet dsTypeDtl = My.ExecuteSELECTQuery(sTypeDdlQuery);
            //string dsTypeOption = "";
            //for (int i = 0; i < dsTypeDtl.Tables[0].Rows.Count; i++)
            //{
            //    dsTypeOption = dsTypeOption + "<option value=\"" + dsTypeDtl.Tables[0].Rows[i][0].ToString() + "\">" + dsTypeDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            //}

            //divTypeList.InnerHtml = "<p class=\"content\">Select Type</p><select runat=\"server\" id=\"ddlIncdntType\" style=\"position:relative; width:250px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">Select Type</option>" + dsTypeOption + "</select>";

            //string sCategoryDdlQuery = "SELECT [ID],[CATEGORY_NAME] FROM [CY_INC_0_CATEGORY_MSTR] ORDER BY [CATEGORY_NAME] ASC";
            //DataSet dsCategoryDtl = My.ExecuteSELECTQuery(sCategoryDdlQuery);
            //string dsCategoryOption = "";
            //for (int i = 0; i < dsCategoryDtl.Tables[0].Rows.Count; i++)
            //{
            //    dsCategoryOption = dsCategoryOption + "<option value=\"" + dsCategoryDtl.Tables[0].Rows[i][0].ToString() + "\">" + dsCategoryDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            //}

            //divCategoryList.InnerHtml = "<p class=\"content\">Select Type</p><select runat=\"server\" id=\"ddlIncdntCategory\" style=\"position:relative; width:250px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">Select Category</option>" + dsCategoryOption + "</select>";

            divEmpList.InnerHtml = "<p class=\"content\">Corrective Action By</p><select id=\"SelectCorrectiveActionBy\" style=\"position:relative; float:right; width:225px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">" + CurrentIncident.RevName + "</option>" + CurrentIncident.RevName + "</select>";

            divSafety.InnerHtml = "<p class=\"content\">Select Safety Precaution</p><select id=\"SelectSafetyPrecaustion\" style=\"position: relative; height: auto; font-size: 16px; padding: 5px; width: 75%; float: right;\"><option value=\"\">" + CurrentIncident.RevPrecaution + "</option>" + CurrentIncident.RevPrecaution + "</select>";

            divTypeList.InnerHtml = "<p class=\"content\">Select Type</p><select runat=\"server\" id=\"ddlIncdntType\" style=\"position:relative; width:250px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">" + CurrentIncident.RevIncType + "</option>" + CurrentIncident.RevIncType + "</select>";

            divCategoryList.InnerHtml = "<p class=\"content\">Select Type</p><select runat=\"server\" id=\"ddlIncdntCategory\" style=\"position:relative; width:250px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">" + CurrentIncident.RevIncCategory + "</option>" + CurrentIncident.RevIncCategory + "</select>";



        }
    }
}