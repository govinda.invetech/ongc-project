﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec.fancybox
{
    public partial class _6_ecAccp_L2 : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            string sIncdntID = Request.QueryString["IncdID"].ToString();

            ecClass CurrentIncdnt = new ecClass(sIncdntID);
            
            lblIncdntNo.Text = sIncdntID;
            txtIncdntDesc.Text = CurrentIncdnt.RptDesc;
            txtIncdntActionTaken .Text= CurrentIncdnt.IncMgrActnToBeTaken;
            txtIncdntRevRem.Text = CurrentIncdnt.RevRemark;

        }
    }
}