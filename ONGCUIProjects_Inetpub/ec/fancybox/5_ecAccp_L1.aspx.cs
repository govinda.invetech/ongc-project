﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.ec.fancybox
{
    public partial class _5_ecAccp_L1 : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            string sIncdntID = Request.QueryString["IncdID"].ToString();

            string sStr = "SELECT [O_EC_DESC] FROM [CY_EC_2_REV_DTL] WHERE [O_EC_NMBR] = '" + sIncdntID + "'";
            DataSet dsReviewedIncd = My.ExecuteSELECTQuery(sStr);

            lblIncdntNo.Text = sIncdntID;
            txtIncdntDesc.Text = dsReviewedIncd.Tables[0].Rows[0][0].ToString();
            txtIncdntDesc.ReadOnly = true;


            string query = "SELECT [LEVEL] FROM [CY_EC_EMP_RELATION] WHERE [LEVEL] = 'LEVEL2' AND [EC_NO] = '" + sIncdntID + "'";
            DataSet dstemp = My.ExecuteSELECTQuery(query);
            if (dstemp.Tables[0].Rows.Count != 0)
            {
                lblIncdntStatus.Text = "11";
            }
        }
    }
}