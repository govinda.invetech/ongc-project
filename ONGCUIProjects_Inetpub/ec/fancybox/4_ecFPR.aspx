﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="4_ecFPR.aspx.cs" Inherits="ONGCUIProjects.ec.fancybox._4_ecFPR" %>
<script type="text/javascript">
    $("#lblIncdntNo").hide();
    $("#cmdSubmitRpt").click(function () {
        var sIncidentID = $("#lblIncdntNo").text();
        var sFPR_Remark = $("#txtActionTaken").val();
        var sIncidentDesc = $("#txtIncdntDesc").val();

        $.ajax({
            type: "POST",
            url: "service/4_ecFPRSave.aspx",
            data: "IncdntID=" + sIncidentID + "&IncdntDesc=" + sIncidentDesc + "&FPRRemark=" + sFPR_Remark,
            success: function (msg) {
                var msg_arr = msg.split("~");
                if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                    $("#response").text(msg_arr[2]);
                    $("#cmdSubmitRpt").hide();
                } else {
                    alert(msg_arr[2]);
                }
            }
        });
    });

</script>

<form id="form1" runat="server">
<asp:label runat="server" ID="lblIncdntNo"></asp:label>
    <div id="add_edit_que" style="position:relative;float:left; width:500px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">Action Taken</h1>
        <div class="div-fullwidth marginbottom">
            <p class="content">Incdent Description</p>
            <asp:TextBox ID="txtIncdntDesc" runat="server" TextMode="MultiLine" style="max-height:60px; max-width:100%; min-height:60px; min-width:100%"></asp:TextBox>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">Action Taken</p>
            <asp:TextBox ID="txtActionTaken" runat="server" TextMode="MultiLine" style="max-height:60px; max-width:100%; min-height:60px; min-width:100%"></asp:TextBox>
        </div>
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
        <div class="div-fullwidth">
        <p id="response" class="content" style="width: auto; color:Red;"></p>
            <input type="button" id="cmdClose" style="float:right;" class="g-button g-button-red" value="Close" onclick="window.location=self.location; $.fancybox.close();" />
            <input type="button" id="cmdSubmitRpt" style="float:right; margin:0px 10px 0px 0px"  class="g-button g-button-submit" value="SUBMIT REPORT" />
        </div>
    </div>
</form>
