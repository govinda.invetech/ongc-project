using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;

namespace ONGCUIProjects
{
    /// <summary>
    /// Summary description for Gate_Req_Reports.
    /// </summary>
    public partial class Feedback_report : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        
        protected void Page_Load(object sender, System.EventArgs e)
        {

        }   
        
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void ddltypeofreport_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddltypeofreport.SelectedValue == "DATEWISE")
            {
                date_wise_report.Visible = true;
                panel_date.Visible = true;
                pnlDateWise.Visible = true;
                div_average.Visible = false;
            }
            if (ddltypeofreport.SelectedValue == "AVERAGE REPORT")
            {
                pnlDateWise.Visible = false;
                date_wise_report.Visible = false;
                panel_date.Visible = false;
                div_average.Visible = true;
                loadgrid_average();
            }

        }    
              
        public DataTable convertcolumntorows(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns
            for (int i = 0; i <= dt.Rows.Count; i++)
            {

                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = dt.Rows[k - 1][j];
                dtnew.Rows.Add(dr);
            }

            return dtnew;
        }
        public void loadgrid_average()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("name", typeof(string));
                dt.Columns.Add("rate", typeof(string));

                string str = "";
                str = "select id,NAME from COMMENT  ";
                DataSet ds1 = (DataSet)My.ExecuteSELECTQuery(str);
                if (ds1 != null)
                {

                    for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = ds1.Tables[0].Rows[i]["name"];
                        string str1 = "";
                        str1 = "select sum(CONVERT(INT,rate)) as total_rating,count([C_ID]) as service_id from [cy_feedback] where [C_ID]='" + ds1.Tables[0].Rows[i]["id"] + "'";
                        DataSet ds2 = (DataSet)My.ExecuteSELECTQuery(str1);
                       
                        if (ds2 != null && ds2.Tables[0].Rows.Count > 0)
                        {
                            if (ds2.Tables[0].Rows[0]["total_rating"].ToString()=="")
                            {
                                dr[1] = 0;
                            }
                            else
                            {
                                int total_rating = Convert.ToInt32(ds2.Tables[0].Rows[0]["total_rating"]);
                                int service_id = Convert.ToInt32(ds2.Tables[0].Rows[0]["service_id"]);
                                int final_rating = (total_rating / service_id);
                                dr[1] = final_rating;
                            }
                            

                        }

                        dt.Rows.Add(dr);
                    }
                    gv_average.DataSource = dt;
                    gv_average.DataBind();
                }
            }
            catch (Exception ex)
            {
                WebMsgApp.WebMsgBox.Show("Error" + ex);
            }
        }
        
        protected void cmdExporttoExcel_Click1(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=FeedbackReport.xls");
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stwr = new StringWriter();
            HtmlTextWriter htew = new HtmlTextWriter(stwr);
            if (ddltypeofreport.SelectedValue == "DATEWISE")
            {
                gv_feedback_report.RenderControl(htew);
            }
            if (ddltypeofreport.SelectedValue == "AVERAGE REPORT")
            {
                gv_average.RenderControl(htew);
            }
            Response.Write(stwr.ToString());
            Response.End();
           
        }
        int j = 0;
        protected void date_wise_report_Click(object sender, EventArgs e)
        {
            try
            {
             
                div_average.Visible = false;

                if (txtdatefrom.Text != "" && txtdatefrom.Text != null && txtdateto.Text != "" && txtdateto.Text != null)
                {
                    //string date1 = My.convertdateforsql(txtdatefrom.Text);
                    //string date2 = My.convertdateforsql(txtdateto.Text);                  

                    string queryy = "";
                    queryy += "select (NAME+'/'+CPF_NO)as  [Emp_Name/Cpf_No], ";
                    queryy += "[EPABX Phone-Office],";
                    queryy += "[Landline Office],[Mobile-Services],Datacard,Internet,[ONGC Reports],";
                    queryy += "SAP,Webice,[Lotus Client],[Lotus WebMail],[Lotus SameTime],[IBM Connections/Myspace],";
                    queryy += "[Computer Performance],[Printer],[IT Maintenance Support],[SCADA System],[Video Conferencing],[FAX Services],";
                    queryy += "[VHF/Walkie Talkie],[HIS],[PA System],[Audio Visual Systems],[EPABX-Residence],";
                    queryy += "[Landline Residence],[Broadband],[Any Other]";
                    queryy += " from DATE_REPORT where TIME_STAMP between '" + My.convertdateforsql1(txtdatefrom.Text) + "'";
                    queryy += "and '" + My.convertdateforsql1(txtdateto.Text) + "'";
                    DataSet dss = (DataSet)My.ExecuteSELECTQuery(queryy);               
                   
                    DataTable mydt = new DataTable();
                    mydt = dss.Tables[0];
                    mydt.Columns.Add("Sr.NO", typeof(string)).SetOrdinal(0);
                   
                    if (dss != null)
                    {

                        for (int i = 1; i <= dss.Tables[0].Rows.Count; i++)
                        {

                            if (i % 2 != 0)
                            {
                                j++;
                                mydt.Rows[i - 1]["Sr.NO"] = j;
                            }                          
                            
                        }                     

                        gv_feedback_report.DataSource = mydt;
                        gv_feedback_report.DataBind();
                        int total_col = gv_feedback_report.Columns.Count;
                        for (int c = 0; c < gv_feedback_report.Rows.Count; c++)
                        {
                            if (gv_feedback_report.Rows[c].Cells[0].Text=="&nbsp;")
                            {                                
                               gv_feedback_report.Rows[c].BackColor = System.Drawing.Color.SkyBlue;                                
                                   
                            }
                            

                        }

                    }
                }
                else
                {
                    WebMsgApp.WebMsgBox.Show("Please select date first");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
           

         

        }

    }
}
