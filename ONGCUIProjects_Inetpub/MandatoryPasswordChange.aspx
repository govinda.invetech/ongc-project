﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MandatoryPasswordChange.aspx.cs" Inherits="ONGCUIProjects.MandatoryPasswordChange" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mandatory Password Change</title>
    <link rel="stylesheet" href="css/css3.css" type="text/css" media="screen" />
    <script type="text/javascript" src="js/jquery-1.7.2.js"></script>

<script type="text/javascript">
    $(document).ready(function (e) {
        $("input[type=password]").focus(function () {
            $("#txtMessage").hide();
            $("input[type=password]").css('border-color', '');
        });
        $("input[type=password]").bind("cut copy paste", function (e) {
            e.preventDefault();
        });
        /*$("#btnChangePassword").click(function () {
            var hint = 0;

            $("input[type=password]").each(function () {
                if ($(this).val() == "") {
                    $(this).css('border-color', 'red');
                    hint = 1;
                }
            });
            if (hint == 0) {
                var old_password = $("#txtOldPassword").val();
                var new_passwordone = $("#txtNewPassword").val();
                var new_passwordtwo = $("#txtReTypeNewPassword").val();
                if (new_passwordone != new_passwordtwo) {
                    $("#txtNewPassword").css('border-color', 'red');
                    $("#txtReTypeNewPassword").css('border-color', 'red');
                    $("#txtMessage").text("These Password don't match. Try again?");
                    $("#txtMessage").show();
                } else {
                    $.ajax({
                        type: "GET",
                        url: "services/updatepassword.aspx",
                        data: "old_pass=" + old_password + "&newpass1=" + new_passwordone + "&newpass2=" + new_passwordtwo,
                        success: function (msg) {
                            //alert(msg);
                            var values = msg.split("~");

                            if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                alert(values[2]);
                                window.location = "index.aspx";
                            } else if (values[0] == "FALSE" && values[1] == "ERR") {
                                alert(values[2]);
                            } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                                $("#txtMessage").text(values[2]);
                                $("#txtMessage").show();
                            }
                        }
                    });
                }
            } else {
                $("#txtMessage").text("Inputs should not be empty");
                $("#txtMessage").show();
            }

        });*/
    });
</script>
</head>
<body>
    <form id="form1" runat="server">
   <div style="left: 35%; top: 15%; position: fixed; width: 342px; overflow: hidden; padding: 10px;">
        <h1 class="heading" style="padding-left: 0; border-bottom: 2px solid #58595A; margin-bottom: 20px;">Change your password</h1>
        <div class="div-fullwidth">
            <div style=" position: relative; float: right; background-color: #F1F1F1; border: 1px solid #B1B1B1; width: 300px; padding: 20px;">
                <p class="content" style="width: 100%; margin-bottom: 5px;">Current Password</p>
                <asp:TextBox ID="txtOldPassword" runat="server"  style="width: 100%; margin-bottom: 10px;" TextMode="Password"></asp:TextBox>

                <p class="content" style="width: 100%; margin-bottom: 5px;">New Password</p>
                <asp:TextBox ID="txtNewPassword" runat="server"  style="width: 100%; margin-bottom: 10px;" TextMode="Password"></asp:TextBox>

                <p class="content" style="width: 100%; margin-bottom: 5px;">Retype New Password</p>
                <asp:TextBox ID="txtReTypeNewPassword" runat="server"  style="width: 100%; margin-bottom: 10px;" TextMode="Password"></asp:TextBox>
                
                <asp:Button ID="btnChangePassword" runat="server" Text="Change Password" 
                    CssClass="g-button g-button-submit" />
                <asp:Label ID="txtMessage" runat="server" Text="" style="width: 100%; color: Red;" CssClass="content"></asp:Label>
                
            </div>
        </div>

    </div>
    </form>
</body>
</html>
