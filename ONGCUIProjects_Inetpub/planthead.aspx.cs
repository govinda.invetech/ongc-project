﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class planthead : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            string sPH_CODE = System.Configuration.ConfigurationSettings.AppSettings["PlantHeadCode"].ToString();
            string sQuery = "SELECT [ID],[PH_EMP_CODE],[MENU_TITLE],[TOOLTIP],[HREF],[IS_DWNL],[ORDER_BY],[ENTRY_BY],[TIMESTAMP] FROM [CY_PLANT_HEAD_PAGE] WHERE [PH_EMP_CODE] = '" + sPH_CODE + "' ORDER BY [TIMESTAMP] DESC";
            DataSet dsPantHead = My.ExecuteSELECTQuery(sQuery);
            if (dsPantHead.Tables[0].Rows.Count > 0)
            {
                rptrPlantHead.DataSource = dsPantHead;
                rptrPlantHead.DataBind();
            }
            else
            {

            }
        }
    }
}