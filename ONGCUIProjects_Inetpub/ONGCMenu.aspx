<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="c#" CodeBehind="ONGCMenu.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.ONGCMenu" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE html>
<html>
<head>
    <title>Uran Online Information System</title>
    <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <link href="style/ONGCStyle.css" type="text/css" rel="stylesheet" />
    <link href="style/treeStyle.css" type="text/css" rel="stylesheet" />
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.7.2.min_a39dcc03.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="js/jqClock.min.js"></script>
    <link href="js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function (e) {
            var varNT = 0;
            var vardrop = 0;
            $('#cmdNewNotifications').click(function () {
                varNT = 1;
                $(".notification-box").fadeIn();
            });
            $(document).click(function () {
                if (varNT == 0) {
                    $(".notification-box").fadeOut();
                } else {
                    varNT = 0;
                }
                if (vardrop == 0) {
                    $(".settings_droparea").hide();
                } else {
                    vardrop = 0;
                }
            });
            $("#div_iframe").click(function () {
                if (varNT == 0) {
                    $(".notification-box").fadeOut();
                }
                else {
                    varNT = 0;
                }
            });
            $("a[href*='aspx']").click(function () {
                $("div#div_iframe").empty();
                var target = $(this).attr("href") + "?force=" + Math.floor((Math.random() * 9999) + 1);
                $("div#div_iframe").append('<iframe src="' + target + '" name="doc" frameborder="no" width="100%" scrolling="auto" height="100%"></iframe>');
               
            });
            // cmdSettings
            $("#cmdSettings").click(function () {
                $(".settings_droparea").show();
                vardrop = 1;
            });
            $("#cmdChangePassword").fancybox();

          
            var ReponseReceived = true;
            setInterval(getNotification, 15000);
            function getNotification() {
                if (ReponseReceived == true) {
                    ReponseReceived == false;
                    $.ajax({
                        url: "services/Notification.aspx",
                        success: function (msg) {
                            var something = msg.split('~');
                            if (something[0] == 'TRUE') {
                                if (something[1] == 'SUCCESS') {
                                    if (something[2] == "0") {
                                        $("#cmdNewNotifications").html("No Notification");
                                        $("#notification_heading").html(something[3]);
                                        $("#div_notification").hide();
                                    } else {
                                        $("#cmdNewNotifications").html("New Notifications<p class=\"notificationsCircle\" style=\"float:left;\"><span>" + something[2] + "</span></p>");
                                        $("#ul_notification").html("<ul class=\"notification-ul\">" + something[3] + "</ul>");
                                        $("#notification_heading").html("New Notification");
                                        $("#div_notification").show();
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });
    </script>
    <style type="text/css">
        .style1
        {
            width: 1259px;
            height: 78px;
        }
        .style2
        {
            width: 245px;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" style="overflow-x: hidden;">
    <p>
        <br />
    </p>
    <form id="Form1" method="post" runat="server" style="position: absolute; top: 0px;
    bottom: 0; right: 0; left: 0; margin-bottom: 25px;">
    <div class="header">
        <div class="logo-container">
            <div class="logo">
            </div>
            <div class="logo-text">
                <h1 class="logo-text-line2">
                    Oil and Natural Gas Corporation Limited</h1>
                <h1 class="logo-text-line3">
                    URAN PLANT, MUMBAI</h1>
            </div>
            <%--<asp:Label ID="txtServerTime" runat="server" Text="" class="ServerTime">Server Time : <span id="as"></span></asp:Label>--%>
            <asp:LoginStatus ID="cmdLogout" runat="server" CssClass="logout" LogoutAction="Redirect"
                LogoutPageUrl="~/Index.aspx" LoginText="Logout" OnLoggingOut="cmdLogout_LoggingOut" />
            <div style="position: relative; float: right; margin-right: 10px;">
                <asp:LinkButton ID="cmdSettings" runat="server" CssClass="settings" OnClientClick="return false;"><img src="Images/settings_icon.png" class="settings_icon" />Settings<img src="Images/arrow_down_white.png" class="settings_icon" style="float: right;" />
                </asp:LinkButton>
                <div class="settings_droparea">
                    <asp:LinkButton ID="cmdAccountSetting" runat="server" CssClass="accountsettings"
                        OnClick="cmdAccountSetting_Click"><div class="settings_icons2"></div>Update Profile</asp:LinkButton>
                    <a id="cmdChangePassword" class="changepassword" href="services/changepassword.aspx">
                        <div class="lock_icon">
                        </div>
                        Change Password</a>
                </div>
            </div>
            <asp:LinkButton ID="cmdHome" runat="server" CssClass="home" OnClick="cmdHome_Click">Home</asp:LinkButton>
        </div>
        <div class="logged-user-details">
            <h1>Welcome</h1>                
            <h1><asp:Label ID="lblSessionLoginName" runat="server" CssClass="username" Text=""></asp:Label></h1>
            <asp:Label ID="lastlogin" runat="server" Text="">Last logged in : 23/11/2012, 11:27 AM</asp:Label>
            <div style="position: relative; float: right;">
                <a runat="server" href="javascript:void(0)" class="buttonsNotification" id="cmdNewNotifications">
                    <p class="notificationsCircle" style="float: left;">
                        <span></span>
                    </p>
                </a>
                <div class="notification-box" id="notificationContainer">
                    <div class="jewelBeeperHeader">
                        <div class="beeperNubWrapper">
                            <div class="beeperNub">
                            </div>
                        </div>
                    </div>
                    <div class="notification_content">
                        <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                            <div class="clearfix uiHeaderTop">
                                <div>
                                    <h3 runat="server" id="notification_heading" class="uiHeaderTitle">
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="div_notification" class="uiScrollableArea fade uiScrollableAreaWithShadow"
                            style="width: 330px; height: 100%;">
                            <div class="uiScrollableAreaWrap scrollable" tabindex="0">
                                <div class="uiScrollableAreaBody" style="width: 330px;">
                                    <div id="ul_notification" class="uiScrollableAreaContent">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <!-- Begin Main Table1-->
        <div style="position: absolute; top: 106px; bottom: 0; right: 0; left: 0;">
            <div style="position: absolute; top: 0px; left: 0px; bottom: 0px; width: 275px; overflow-x: hidden;
                overflow-y: auto;">
                <asp:TreeView ID="TreeView1" Style="margin-top: -21px; margin-left: -2px;" runat="server"
                    SelectedLeafNodeImageUrl="Images/folder_opened.gif" NoExpandImageUrl="~/Images/folder.gif"
                    ExpandImageUrl="~/Images/folder_opened.gif" CollapseImageUrl="~/Images/folder.gif"
                    DefaultTarget="doc" SelectedNodeCssClass="SelectedTreeNode" SelectedExpandedParentNodeImageUrl="Images/folder_opened.gif"
                    ParentNodeImageUrl="Images/folder.gif" NodeCssClass="TreeNode" LeafNodeImageUrl="~/Images/folder.gif"
                    HoverNodeCssClass="HoverTreeNode" CssClass="TreeView" ExpandedParentNodeImageUrl="Images/folder_opened.gif"
                    Width="100%" Height="100%" ExpandDepth="0">
                </asp:TreeView>
            </div>
            <div id="div_iframe" style="position: absolute; top: 0; bottom: 0; right: 0; left: 275px;">
                <iframe runat="server" id="doc" name="doc" frameborder="no" width="100%" scrolling="auto"
                    height="100%"></iframe>
            </div>
        </div>
        <!-- End Main Table-->
    </div>
    <div class="footer">
        <div class="footer-content" style="padding-top: 5px;">
            <p class="copyright">
                </p>
            <p class="designedby">
               Maintained by Infocom Services, Uran
            </p>
        </div>
    </div>
    </form>
</body>
</html>
