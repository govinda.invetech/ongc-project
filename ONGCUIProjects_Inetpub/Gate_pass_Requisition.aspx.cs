using ONGCUIProjects;
using System;
using System.Collections;
using System.Data;

namespace WebApplication1
{
    public partial class Gate_pass_Requisition : System.Web.UI.Page
    {
        MyApplication1 My = new MyApplication1();
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                string sGroupID = "";
                if (string.IsNullOrEmpty(Request["GroupID"]))
                {
                }
                else
                {
                    sGroupID = Request["GroupID"].ToString();
                }
                string cpf_no = Session["Login_Name"].ToString();
                #region Requisitioner Details
                string sQuery = "SELECT  O_EMP_NM, O_CPF_NMBR, O_EMP_DESGNTN, O_EMP_WRK_LCTN,CY_EMP_EX_No FROM  O_EMP_MSTR where O_CPF_NMBR='" + cpf_no + "'";
                DataSet dsRequisitioner = My.ExecuteSELECTQuery(sQuery);
                if (dsRequisitioner.Tables[0].Rows.Count != 0)
                {
                    txtCPFNo.Text = dsRequisitioner.Tables[0].Rows[0][1].ToString();
                    txtDesg.Text = dsRequisitioner.Tables[0].Rows[0][2].ToString();
                    txtEmpName.Text = dsRequisitioner.Tables[0].Rows[0][0].ToString();
                    txtExtNo.Text = dsRequisitioner.Tables[0].Rows[0][4].ToString();
                    cmbLocation.Text = dsRequisitioner.Tables[0].Rows[0][3].ToString();
                }
                #endregion
                string sMeetingOffLocation = "";
                string sApproverCPF = "";
                if (sGroupID == "")
                {
                    RequisitioHint.Value = "NEW";
                    visitor_start_date.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    visitor_end_date.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                    CmbRequisitionType.Items.FindByValue("Entry On Week Days").Selected = true;
                    //cmbReason.Items.FindByValue("Official").Selected = true;/*--for change ddl-*/
                    cmbReason.Text = "";
                    cmbEntryArea.Items.FindByValue("").Selected = true;
                }
                else
                {
                    RequisitioHint.Value = "UPDATE~" + sGroupID;
                    string sPassReqDetails = "SELECT DISTINCT([CY_GROUP]),[O_REQSTN_TYPE],[O_ENTRY_DT],[O_EXIT_DT],[O_ENTRY_AREA] ,[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG],[CY_O_MTNG_OFF_CPF],[O_MTNG_OFF_LCTN],[O_PRPS_OF_VISIT],[O_ACP1_CPF_NMBR],[O_RMRKS] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [CY_GROUP] = '" + sGroupID + "'";
                    DataSet dsPassReqDetails = My.ExecuteSELECTQuery(sPassReqDetails);
                    string sRequestionType = dsPassReqDetails.Tables[0].Rows[0][1].ToString();
                    DateTime dtEntryDate = Convert.ToDateTime(dsPassReqDetails.Tables[0].Rows[0][2]);
                    DateTime dtExitDate = Convert.ToDateTime(dsPassReqDetails.Tables[0].Rows[0][3]);
                    string sEntryArea = dsPassReqDetails.Tables[0].Rows[0][4].ToString();
                    string sMeetingOffName = dsPassReqDetails.Tables[0].Rows[0][5].ToString();
                    string sMeetingOffDesig = dsPassReqDetails.Tables[0].Rows[0][6].ToString();
                    string sMeetingOffCPF = dsPassReqDetails.Tables[0].Rows[0][7].ToString();
                    sMeetingOffLocation = dsPassReqDetails.Tables[0].Rows[0][8].ToString();
                    string sMeetingPurpose = dsPassReqDetails.Tables[0].Rows[0]["O_PRPS_OF_VISIT"].ToString();
                    sApproverCPF = dsPassReqDetails.Tables[0].Rows[0][10].ToString();
                    string sRemark = dsPassReqDetails.Tables[0].Rows[0][11].ToString();
                    visitor_start_date.Text = dtEntryDate.ToString("dd-MM-yyyy");
                    visitor_end_date.Text = dtEntryDate.ToString("dd-MM-yyyy");
                    CmbRequisitionType.Items.FindByValue(sRequestionType).Selected = true;
                    //cmbReason.Items.FindByValue(sMeetingPurpose).Selected = true;/*--for reason ddl-*/
                    cmbReason.Text = sMeetingPurpose;
                    cmbEntryArea.Items.FindByValue(sEntryArea).Selected = true;
                    txtWhomToMeet.Text = sMeetingOffName;
                    txtWhomToMeetCPF.Text = sMeetingOffCPF;
                    txtMtng_Off_Desg.Text = sMeetingOffDesig;
                    txtRemarks.Text = sRemark;
                    string sVisitorQuery = "SELECT  [O_VISTR_NM],[O_VISTR_ORGNZTN],[O_VISTR_AGE],[O_VISTR_GNDR],[O_PRPS_OF_VISIT],[O_PASS_TYPE_FLG],[O_REQ_COMPLTN_FLG],[CY_WITH_TOOL],[O_INDX_NMBR] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [CY_GROUP] = '" + sGroupID + "'";
                    DataSet dsVisitorDetails = My.ExecuteSELECTQuery(sVisitorQuery);
                    hiddenNoofVisitor.Value = dsVisitorDetails.Tables[0].Rows.Count.ToString();
                    string my_div_strip = "";
                    for (int j = 0; j < dsVisitorDetails.Tables[0].Rows.Count; j++)
                    {
                        string sVisitorName = dsVisitorDetails.Tables[0].Rows[j][0].ToString();
                        string sVisitorOrg = dsVisitorDetails.Tables[0].Rows[j][1].ToString();
                        string sVisitorAge = dsVisitorDetails.Tables[0].Rows[j][2].ToString();
                        string sVisitorGender = dsVisitorDetails.Tables[0].Rows[j][3].ToString();
                        string sPurposeOfVisit = dsVisitorDetails.Tables[0].Rows[j][4].ToString();
                        string sPassType = dsVisitorDetails.Tables[0].Rows[j][5].ToString();
                        string sPassStatus = dsVisitorDetails.Tables[0].Rows[j][6].ToString();
                        string sTools = dsVisitorDetails.Tables[0].Rows[j][7].ToString();
                        string O_INDX_NMBR = dsVisitorDetails.Tables[0].Rows[j][8].ToString();

                        string tr_class = sPassStatus == "A" ? "VisitorEntryForm" : "";
                        string InputDisabled = "";
                        //if (sPassStatus != "A")
                        //{
                        //    InputDisabled = "disabled='disabled'";
                        //}
                        if (sPassStatus == "P")
                            InputDisabled = "disabled='disabled'";
                        //" + InputDisabled + "
                        //my_div_strip += "<tr class='VisitorEntryForm'  replaceme='KEY" + j + "'   >";
                        my_div_strip += "<tr class='modifyEntryForm'  replaceme='KEY" + j + "' >";
                        my_div_strip += "<td><input " + InputDisabled + "  name='visitorcompany'  class='txtVisitorCompany' CssClass='flattxt' Style='padding-top: 4px; width: 100%;' value='" + sVisitorOrg + "' /></td>";
                        my_div_strip += "<td><input " + InputDisabled + "  name='visitorname'     class='txtVisitorName' runat='server' CssClass='flattxt' Style='padding-top: 4px; width: 75%;' value='" + sVisitorName + "' /></td>";
                        my_div_strip += "<td><input " + InputDisabled + " name='visitorage'      class='txtVisitorAge' runat='server' CssClass='flattxt' Style='padding-top: 4px; width: 100%;' value='" + sVisitorAge + "' /></td>";
                        if (sVisitorGender == "M")
                        {
                            my_div_strip += "<td><select " + InputDisabled + " name='visitorsex' class='gendercombo' style='width:75%;'><option value=''>Select</option><option value='M' selected='selected'>Male</option><option value='F'>Female</option></select></td>";
                        }
                        else if (sVisitorGender == "F")
                        {
                            my_div_strip += "<td><select  " + InputDisabled + " name='visitorsex' class='gendercombo' style='width:75%;'><option value=''>Select</option><option value='M'>Male</option><option value='F' selected='selected'>Female</option></select></td>";
                        }

                        //if (sPurposeOfVisit == "Official")
                        //{
                        //    my_div_strip += "<td><select " + InputDisabled + "  name='purposeofvisit' class='purposeofvisit' style='width:75%;'><option value=''>Select</option><option value='Official' selected='selected'>Official</option><option value='Personal'>Personal</option></select></td>";
                        //}
                        //else if (sPurposeOfVisit == "Personal")
                        //{
                        //    my_div_strip += "<td><select  " + InputDisabled + "  name='purposeofvisit' class='purposeofvisit' style='width:75%;'><option value=''>Select</option><option value='Official'>Official</option><option value='Personal' selected='selected'>Personal</option></select></td>";
                        //}

                        my_div_strip += "<td><input " + InputDisabled + " class='purposeofvisit' runat='server' CssClass='flattxt'  Style='padding-top: 4px; width: 100%;' value='" + sPurposeOfVisit + "' /></td>";
                        my_div_strip += "<td><input " + InputDisabled + " class='txtTools' runat='server' CssClass='flattxt'  Style='padding-top: 4px; width: 100%;' value='" + sTools + "' /></td>";
                        if (sPassStatus != "P")
                            my_div_strip += "<td><input " + InputDisabled + "  type='button'  class='g-button g-button-submit cmdedit' value='Edit' groupid=" + sGroupID + "  index=" + O_INDX_NMBR + " /></td>";
                        else if (sPassStatus == "P")
                            my_div_strip += "<td><span><b>Approved</b></span></td>";
                        my_div_strip += "</tr>";
                    }
                    EditVisitorList.Value = my_div_strip;
                    //total_theory.InnerHtml = "Total " + dsVisitorDetails.Tables[0].Rows.Count + " Visitor";
                }


                if (My.getTotalPendingRequestofGetPass(cpf_no) == "FALSE")
                {
                    cmdViewApprovals.Visible = false;
                }
                else
                {
                }

                #region GetLocation
                string sStr = "SELECT [O_LCTN_NM]  FROM [O_LCTN_MSTR] where [O_LCTN_NM]<>'' ORDER BY [O_LCTN_NM] ASC";
                DataSet ds = My.ExecuteSELECTQuery(sStr);
                string output = "";
                if (ds.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    //ArrayList sMeetingLocationArr = new ArrayList(sMeetingOffLocation.Split(" and ",StringSplitOptions.None);
                    ArrayList sMeetingLocationArr = new ArrayList(sMeetingOffLocation.Split(new string[] { " and " }, StringSplitOptions.None));
                    output = "<select id='cmbMeetingLocation' multiple='multiple' style='position:relative; float:left; max-width:100%;'>";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string sMeetLocation = ds.Tables[0].Rows[i][0].ToString();
                        if (sMeetingLocationArr.Contains(sMeetLocation))
                        {
                            output += "<option value='" + sMeetLocation + "' selected>" + sMeetLocation + "</option>";
                        }
                        else
                        {
                            output += "<option value='" + sMeetLocation + "'>" + sMeetLocation + "</option>";
                        }
                    }
                    output += "</select>";
                }
                divMeetingLocation.InnerHtml = output;
                #endregion

                #region Get Pass Approve Employee
                string sStr1 = "SELECT  [CY_MENU_EMP_RELATION].[cpf_number],[O_EMP_MSTR].[O_EMP_NM] FROM [CY_MENU_EMP_RELATION] ,[O_EMP_MSTR] where [CY_MENU_EMP_RELATION].[child_id]='18' and [O_EMP_MSTR].[O_CPF_NMBR] = [CY_MENU_EMP_RELATION].[cpf_number] ORDER BY [O_EMP_MSTR].[O_EMP_NM] ASC";
                DataSet ds1 = My.ExecuteSELECTQuery(sStr1);
                string output1 = "";
                if (ds.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    output1 = "<select id='ddlPassApproveAuthority' style='width:185px;'><option value=''>Select Approver</option>";
                    for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                    {
                        if (sApproverCPF == ds1.Tables[0].Rows[i][0].ToString())
                        {
                            output1 += "<option value='" + ds1.Tables[0].Rows[i][0].ToString() + "' selected>" + ds1.Tables[0].Rows[i][1].ToString() + "</option>";
                        }
                        else
                        {
                            output1 += "<option value='" + ds1.Tables[0].Rows[i][0].ToString() + "'>" + ds1.Tables[0].Rows[i][1].ToString() + "</option>";
                        }
                    }
                    output1 += "</select>";
                }
                div_pass_approving_auth.InnerHtml = output1;
                #endregion

                #region Kuchh Nahin Bas Aise Hi
                string sVisitorStrips = "";
                sVisitorStrips += "<tr class='VisitorEntryForm' replaceme='[replaceme]'>";
                sVisitorStrips += "<td><input id='visitorcompany1' placeholder='Company Name' name='visitorcompany' class='txtVisitorCompany' CssClass='flattxt' Style='padding-top: 4px; width: 75%;' /></td>";
                sVisitorStrips += "<td><input name='visitorname' placeholder='Visitor Name' class='txtVisitorName' CssClass='flattxt' Style='padding-top: 4px; width: 75%;' /></td>";
                sVisitorStrips += "<td><input name='visitorage' placeholder='Age' class='txtVisitorAge' CssClass='flattxt' Style='padding-top: 4px; width: 100%;' /></td>";
                sVisitorStrips += "<td><select name='visitorsex' class='gendercombo' style='width:75%;'><option value=''>Select</option><option value='M' selected>Male</option><option value='F'>Female</option></select></td>";
                // sVisitorStrips += "<td><select name='visitorpurpose' class='txtVisitorPurpose' style='width:75%;'><option value=''>Select</option><option value='Official' selected>Official</option><option value='Personal'>Personal</option></select></td>";
                sVisitorStrips += "<td><input name='visitorpurpose' placeholder='Purpose of Visit' TextMode='MultiLine'  class='txtVisitorPurpose' CssClass='flattxt' Style='padding-top: 4px; width: 100%;' /></td>";
                sVisitorStrips += "<td><input name='visitortool' placeholder='Tools (if any)' class='txtVisitorTools' CssClass='flattxt' Style='padding-top: 4px; width: 100%;' /></td>";
                sVisitorStrips += "</tr>";
                HiddenField1.Value = sVisitorStrips;
                #endregion
            }
        }


        [System.Web.Services.WebMethod]
        public static string Updategatepassdata(string INDEX, string GROUPID, string TOOL, string PURPOSE, string GENDER, string COMPANY, string NAME, string AGE)
        {
            string str = "";
            str += " UPDATE [dbo].[O_VSTR_GATE_PASS_RQSTN_DTL] ";
            str += " SET [CY_WITH_TOOL] = '" + TOOL + "',[O_PRPS_OF_VISIT]='" + PURPOSE + "',       ";
            str += " [O_VISTR_GNDR] = '" + GENDER + "',[O_VISTR_ORGNZTN]='" + COMPANY + "',       ";
            str += " [O_VISTR_NM]= '" + NAME + "', [O_VISTR_AGE]='" + AGE + "'     ";
            str += " WHERE [CY_GROUP] = '" + GROUPID + "'      ";
            str += " AND [O_INDX_NMBR] = " + INDEX + "         ";

            try
            {
                if (MyApplication1.ExecuteSQLQuery_static(str) == true)
                    return "{\"STATUS\":\"TRUE\",\"MESSAGE\":\"Data has been updated Successfully.\"}";
                else
                    return "{\"STATUS\":\"ERR\",\"MESSAGE\":\"Error in Execution . \"}";
            }
            catch (Exception e)
            {
                return "{\"STATUS\":\"ERR\",\"MESSAGE\":\"" + e.Message + "\"}";
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// last Modified on 23/01/2014 by shiv
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

    }
}
