<%@ Page language="c#" Codebehind="Update_Emp_Mstr.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Update_Emp_Mstr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update_Emp_Mstr</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<FORM id="Form1" method="post" runat="server">
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U>Incident 
								Management - Update Employee Master</U></STRONG></FONT></FONT><STRONG><FONT face="Verdana" color="red" size="1">&nbsp;&nbsp;&nbsp;
					</FONT></STRONG>
			</P>
			<HR width="100%" SIZE="1">
			<STRONG><FONT face="Verdana" color="red" size="1">(Fields Marked With * are Mandatory 
					Fields)</FONT></STRONG>
			<P align="center">
				<TABLE id="Table2" cellSpacing="8" cellPadding="4" width="100%" align="center" bgColor="whitesmoke"
					border="1" runat="server">
					<TBODY>
						<TR>
							<TD borderColor="gray" align="center" bgColor="whitesmoke">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Employee Name</STRONG></FONT></P>
							</TD>
							<TD borderColor="gray" align="center" bgColor="whitesmoke">
								<P align="left"><FONT face="Verdana" size="1">
										<asp:textbox id="txtEmpName" runat="server" Width="200px" Font-Names="Verdana" Font-Size="XX-Small"
											MaxLength="50" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></P>
							</TD>
							<TD borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG>
										<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>CPF No.</STRONG></FONT>
									</STRONG></FONT>
			</P>
			</TD>
			<TD borderColor="gray" align="center" bgColor="whitesmoke">
				<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
							<asp:textbox id="txtEmpCPFNo" runat="server" Width="200px" Font-Names="Verdana" Font-Size="XX-Small"
								MaxLength="50" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
			</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Home Address Line 1</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtAddr1" runat="server" Width="200px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="8000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0" TextMode="MultiLine"
									Height="88px"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Home Address Line 2</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtAddr2" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="8000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0" TextMode="MultiLine"
									Height="88px"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>City</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtCity" runat="server" Width="192px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="100" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>State</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtState" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="100" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Pincode</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtPinCode" runat="server" Width="192px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="10" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Home Phone Number</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left">
						<asp:textbox id="txtHomeNo" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
							MaxLength="15" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Mobile Number</STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtMblNo" runat="server" Width="192px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="11" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Department</STRONG></FONT>
							</FONT></STRONG></FONT></P></TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
							<P align="left">
								<asp:textbox id="txtDept" runat="server" Width="208px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="100" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox>
						</FONT></STRONG></P></TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Email Id 1&nbsp;<FONT color="red">(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
							<P align="left">
								<asp:textbox id="txtEmail1" runat="server" Width="192px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="1000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox>
						</FONT></STRONG></P></TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="#000099">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Email Id&nbsp;2</STRONG></FONT>
							</FONT></STRONG></FONT></P></TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
							<P align="left">
								<asp:textbox id="txtEmail2" runat="server" Width="200px" Font-Names="Verdana" Font-Size="XX-Small"
									MaxLength="1000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox>
						</FONT></STRONG></FONT></STRONG></P></TD>
			</TR>
			<TR>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Email Id&nbsp;3</STRONG></FONT>
							</FONT></STRONG></FONT></P></TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtEmail3" runat="server" Width="192px" Font-Names="Verdana" Font-Size="XX-Small"
							MaxLength="1000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red"><FONT face="Verdana" color="#660000" size="1"><STRONG>Designation</STRONG></FONT></FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtDesignation" runat="server" BorderColor="#E0E0E0" BorderStyle="Solid" CssClass="flattxt"
							MaxLength="1000" Font-Size="XX-Small" Font-Names="Verdana" Width="200px"></asp:textbox></P>
				</TD>
			</TR>
			</TBODY></TABLE></P>
			<P align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnSaveEmpData" runat="server" Font-Names="Verdana" Font-Size="XX-Small" BorderStyle="Solid"
					BorderColor="DarkGray" Text="Update Personal Information" Font-Bold="True" BackColor="Silver"
					ForeColor="ControlText" onclick="BtnSaveEmpData_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnRefresh" runat="server" Width="158px" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Height="18px" Text="Refresh" CausesValidation="False"
					Font-Bold="True" BackColor="Silver" ForeColor="ControlText"></asp:button></P>
		</FORM>
	</body>
</HTML>
