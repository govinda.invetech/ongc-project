﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class ViewAllNews : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                lblSessionLoginName.Text = Session["EmployeeName"].ToString();
                String sCPFno = Session["Login_Name"].ToString();
                My.InsertPageHit(sCPFno, "VIEWALLNEWS");
                lastlogin.Text = "Last logged in : " + My.EmpLastLogin(sCPFno).ToString("F");

                string sType = "";
                if (string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    DateTime dtTodayDate = System.DateTime.Today;
                    sType = "m~" + dtTodayDate.Month.ToString() + "-" + dtTodayDate.Year.ToString();
                }
                else
                {
                    sType = Request.QueryString["type"].ToString();
                }


                string sQueryType = My.sDataStringTrim(1, sType);
                string sQuery = "";
                if (sQueryType == "d")
                {
                    txtType.Value = "DATE";
                    txtNewsDate.Value = My.sDataStringTrim(2, sType);
                    sQuery = "SELECT [MENU_TITLE],[TOOLTIP],[HREF],[TIMESTAMP] FROM [CY_LP_WHATS_NEW] WHERE CAST([TIMESTAMP] AS DATE) = CAST('" + My.ConvertDateStringintoSQLDateString(My.sDataStringTrim(2, sType)) + "' AS DATE) ORDER BY [TIMESTAMP] DESC";
                }
                else if (sQueryType == "m")
                {
                    txtType.Value = "MONTH";
                    txtNewsMonth.Value = My.sDataStringTrim(2, sType);
                    string[] sDataArr = My.sDataStringTrim(2, sType).Split('-');

                    sQuery = "SELECT [MENU_TITLE],[TOOLTIP],[HREF],[TIMESTAMP] FROM [CY_LP_WHATS_NEW] WHERE DATEPART(YEAR,[TIMESTAMP])='" + sDataArr[1] + "' AND DATEPART(MONTH,[TIMESTAMP])= '" + sDataArr[0] + "' ORDER BY [TIMESTAMP] DESC";
                }

                DataSet dsWhatsNew = My.ExecuteSELECTQuery(sQuery);

                if (dsWhatsNew.Tables[0].Rows.Count == 0)
                {
                    DivNewsContainer.InnerHtml = "<p style=\"font-weight: bold; font-size: xx-large; padding: 100px 0px; text-align: center; color: rgb(150, 15, 19); font-family: myriadpro-regular;\">There is no news.</p>";
                }
                else
                {
                    string NewsContent = "";
                    string dtNewsDateTemp = "NA";
                    for (int i = 0; i < dsWhatsNew.Tables[0].Rows.Count; i++)
                    {
                        string sNewsTitle = dsWhatsNew.Tables[0].Rows[i][0].ToString().Trim();
                        string sNewsToolTip = dsWhatsNew.Tables[0].Rows[i][1].ToString();
                        string sNewsHref = dsWhatsNew.Tables[0].Rows[i][2].ToString();
                        DateTime dtNewsDate = Convert.ToDateTime(dsWhatsNew.Tables[0].Rows[i][3].ToString());

                        if (dtNewsDateTemp == dtNewsDate.ToString("ddMMyyyy"))
                        {
                            NewsContent += "<a class=\"day-news\" target=\"_blank\" title=\"" + sNewsToolTip + "\" href=\"" + sNewsHref + "\">" + sNewsTitle + "</a>";
                        }
                        else
                        {
                            NewsContent += "</div><div class=\"day-news-container\"><p class=\"day-news-container-date\">Posted on <span>" + dtNewsDate.ToString("dddd, dd MMMM, yyyy") + "</span></p>";
                            NewsContent += "<a class=\"day-news\" target=\"_blank\" title=\"" + sNewsToolTip + "\" href=\"" + sNewsHref + "\">" + sNewsTitle + "</a>";
                        }

                        dtNewsDateTemp = dtNewsDate.ToString("ddMMyyyy");
                    }

                    DivNewsContainer.InnerHtml = NewsContent.Substring(6) + "</div>";
                }
            }
        }

        protected void cmdAccountSetting_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewUserReg.aspx?my_hint=UPDATE");
        }

        protected void cmdHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("launchpage.aspx");
        }

        protected void cmdLogout_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Session["Login_Name"] = "";
            Session["EmployeeName"] = "";
        }
    }
}