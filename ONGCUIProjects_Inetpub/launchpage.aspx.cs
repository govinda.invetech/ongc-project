﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;



namespace ONGCUIProjects
{
    public partial class launchpage : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            //HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
            //HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //HttpContext.Current.Response.Cache.SetNoStore();
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(0));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetValidUntilExpires(true);
            Response.Cache.SetNoStore();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] != null)
            {
                string employee_name = Session["EmployeeName"].ToString();
                lblEmpName.Text = employee_name;
            }
            else
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sCPFNo = Session["Login_Name"].ToString();

            My.InsertPageHit(sCPFNo, "LAUNCH_PAGE");

            lastlogin.Text = "Last logged in : " + My.EmpLastLogin(sCPFNo).ToString("F");

            string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();

            if (sCPFNo != "admin")
            {
                list_of_users.Visible = false;
            }

            if (sCPFNo == "guest")
            {
                divQuickLoginLink.Visible = false;
            }

            #region Thought of The Day
            DateTime sDate = System.DateTime.Today;
            lblTOD.Text = "आज का  उत्पादन /" + My.GetThoughtoftheDay(sDate);
            #endregion

            #region No of Users
            DataSet dsNoOfUser = My.RegisteredUsers();
            NoOfUser.InnerText = dsNoOfUser.Tables[0].Rows.Count.ToString();
            #endregion

            #region Today Question
            DateTime today = System.DateTime.Today;
            DataSet ds = new DataSet();
            //string sStr = "SELECT TOP 2 [id],[que_txt],[end_date],[start_date] FROM [CY_QUE_DTLS] WHERE  [is_approved] = 'YES' AND [start_date] < GETDATE() ORDER BY [start_date] DESC";
            string sStr = "SELECT [id],[que_txt],[end_date],[start_date] FROM [CY_QUE_DTLS] WHERE  [is_approved] = 'YES' AND [start_date] < GETDATE() AND [end_date] > GETDATE() ORDER BY [start_date] DESC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                // AllOpenQuestion.InnerHtml = "No question for Today.";
                sStr = "SELECT Top 1 [id],[que_txt],[end_date],[start_date] FROM [CY_QUE_DTLS] WHERE  [is_approved] = 'YES' AND [start_date] < GETDATE() ORDER BY [start_date] DESC, [end_date] DESC";
                //string sStr = "SELECT [id],[que_txt],[end_date],[start_date] FROM [CY_QUE_DTLS] WHERE  [is_approved] = 'YES' AND [start_date] < GETDATE() AND [end_date] > GETDATE() ORDER BY [start_date] DESC";
                da = new SqlDataAdapter(sStr, conn);
                da.Fill(ds);

                //click_to_answer.Visible = false;
                //TodayQuestionID.Text = "0";
            }
            //else
            //  {
            string AllTodayQue = "";

            if (ds.Tables[0].Rows.Count == 2)
            {
                DateTime dtEndDate = Convert.ToDateTime(ds.Tables[0].Rows[1][2].ToString());
                TodayQuestionID.InnerHtml = ds.Tables[0].Rows[1][0].ToString(); ;
                txtQueEndTimestamp.Value = dtEndDate.ToString("yyyyMMddHHmm00");
            }
            else
            {
                DateTime dtEndDate = Convert.ToDateTime(ds.Tables[0].Rows[0][2].ToString());
                TodayQuestionID.InnerHtml = ds.Tables[0].Rows[0][0].ToString(); ;
                txtQueEndTimestamp.Value = dtEndDate.ToString("yyyyMMddHHmm00");
            }


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                string today_que_id = ds.Tables[0].Rows[i][0].ToString();
                string today_que_text = ds.Tables[0].Rows[i][1].ToString();
                DateTime dtEndDate = Convert.ToDateTime(ds.Tables[0].Rows[i][2].ToString());
                DateTime dtStartDate = Convert.ToDateTime(ds.Tables[0].Rows[i][3].ToString());





                AllTodayQue += "<div class='todays-questions' style='position:relative;'>";
                AllTodayQue += "<h1 class='heading-todays-question-container'><img src='Images/Pulseofuran_logo.png' alt='' style='position: relative; margin-bottom: -25px; margin-top: -11px;' />";
                AllTodayQue += "<a class='cmdPreamble buttonGreen' href='preamble.aspx' style='position: absolute; left: 7px; top: 7px;'>Preamble</a>";

                if (System.DateTime.Now < dtEndDate)
                {
                    if (My.getAnswerFromCPFNo(today_que_id, sCPFNo) == "FALSE")
                    {
                        AllTodayQue += "<a runat='server' class='buttonGreen cmd_click_to_answer' href='services/quiz/UserAnsFancyBox.aspx?que_id=" + today_que_id + "' Style='position: absolute; right: 7px; top: 7px;'>Click to Answer...</a>";
                    }
                    else
                    {
                        AllTodayQue += "<a runat='server' class='buttonGreen cmd_click_to_answer' href='services/quiz/UserAnsFancyBox.aspx?que_id=" + today_que_id + "' Style='position: absolute; right: 7px; top: 7px;'>Responded</a>";
                    }
                }
                else
                {
                    AllTodayQue += "<a runat='server' class='buttonGreen cmd_click_to_answer' href='services/quiz/UserAnsFancyBox.aspx?que_id=" + today_que_id + "' Style='position: absolute; right: 7px; top: 7px;'>View Result</a>";
                }


                AllTodayQue += "</h1>";
                AllTodayQue += "<ul>";
                AllTodayQue += "<li>";
                AllTodayQue += "<div class='container_100pert' runat='server' id='div_today_que' style='float: none;'>";
                AllTodayQue += "<p><img class='question-icon1' src='images/question.png' /><span ID='lblTodayQue' runat='server' Text=''>" + today_que_text + "</span></p>";
                AllTodayQue += "</div>";
                AllTodayQue += "</li>";
                AllTodayQue += "<h1 class='start-date'>Start Date : <span runat='server' id='txtQStartDate' style='color: #34343b;'>" + dtStartDate.ToString("dd-MM-yyyy HH:mm") + " hrs</span></h1>";
                AllTodayQue += "<h1 class='end-date'>End Date : <span runat='server' id='txtQEndDate' style='color: #34343b;'>" + dtEndDate.ToString("dd-MM-yyyy HH:mm") + " hrs</span></h1>";
                AllTodayQue += "<div style='position: relative; width: 100%; margin-bottom: 25px;'></div>";
                AllTodayQue += "</ul>";
                AllTodayQue += "<a class='cmdViewPreviousQuestion buttonGreen' is_open='NO' style='margin-top: -22px;margin-bottom: -5px;' href='javascript:void(0);'>View Previous Results</a>";
                //AllTodayQue += "<span ID='TodayQuestionID' runat='server' Text='Label'></span>";
                AllTodayQue += "</div>";






                /*lblTodayQue.Text = today_que_text;
                TodayQuestionID.Text = today_que_id;
                txtQueEndTimestamp.Value = dtEndDate.ToString("yyyyMMddHHmm00");
                //"M d h:mm yy"
                txtQStartDate.InnerHtml = dtStartDate.ToString("dd-MM-yyyy HH:mm") + " hrs";
                txtQEndDate.InnerHtml = dtEndDate.ToString("dd-MM-yyyy HH:mm") + " hrs";
                if (System.DateTime.Now < dtEndDate)
                {
                    if (My.getAnswerFromCPFNo(today_que_id, sCPFNo) == "FALSE")
                    {
                        click_to_answer.Text = "Click to Answer...";
                    }
                    else
                    {
                        click_to_answer.Text = "Responded";
                    }
                }
                else
                {
                    click_to_answer.Text = "View Result";
                }
                click_to_answer.Attributes.Add("href", "services/quiz/UserAnsFancyBox.aspx?que_id=" + today_que_id);
                */
                txtQueEndTimestamp.Value = dtEndDate.ToString("yyyyMMddHHmm00");
            }
            AllOpenQuestion.InnerHtml = AllTodayQue;

            // }
            #endregion

            #region Menu Item
            string sMenuGrpQuery = "SELECT [ID],[GROUP_NAME],[POSITION],GROUP_NAME_HINDI FROM [CY_LP_MENU_GROUP]  ORDER BY [POSITION] ASC, [PRIORITY] ASC";
            DataSet dsMenuGrp = My.ExecuteSELECTQuery(sMenuGrpQuery);
            if (dsMenuGrp.Tables[0].Rows.Count == 0)
            {
                div_right_menu.Visible = false;
                div_left_menu.Visible = false;
            }
            else
            {
                div_right_menu.Visible = false;
                div_left_menu.Visible = false;

                string sRightMenuContent = "";
                string sLeftMenuContent = "";
                for (int i = 0; i < dsMenuGrp.Tables[0].Rows.Count; i++)
                {
                    string sGrpID = dsMenuGrp.Tables[0].Rows[i][0].ToString();
                    string sGrpTitle = dsMenuGrp.Tables[0].Rows[i][1].ToString();
                    string sGrpPosition = dsMenuGrp.Tables[0].Rows[i][2].ToString();
                    string sGrpTitleHindi = dsMenuGrp.Tables[0].Rows[i][3].ToString();

                    string sSubMenuTitle = "";
                    string sSubMenuTooltip = "";
                    string sSubMenuHref = "";

                    string sSubMenuQuery = "SELECT [MENU_TITLE],[TOOLTIP],[HREF] FROM [CY_LP_MENU_ITEM] WHERE [GROUP_ID] = '" + sGrpID + "' ORDER BY [PRIORITY] ASC";
                    DataSet dsSubMenu = My.ExecuteSELECTQuery(sSubMenuQuery);
                    if (dsSubMenu.Tables[0].Rows.Count != 0)
                    {
                        if (sGrpPosition == "RIGHT")
                        {
                            sRightMenuContent += "<div class=\"menu-container\">";                           
                            sRightMenuContent += "<ul id=\"ul2\" class=\"menu\">";
                            sRightMenuContent += "<div style=\" position: relative; float: left; width: 100%;\" class=\"div_menu_item\">";
                            sRightMenuContent += "<li class=\"menu-item\">";
                            sRightMenuContent += "<a href=\"javascript:void(0);\" class=\"menu-li-windows8\">"+sGrpTitleHindi+"<br>" + sGrpTitle + "</a>";
                            sRightMenuContent += "</li>";
                            sRightMenuContent += "<div class=\"submenu-container right\" style=\"display: none;\">";
                            sRightMenuContent += "<div style=\"overflow:scroll; height:270px;\"> ";
                            sRightMenuContent += "<ul style=\" padding-left: 0;\">";
                         ///////
                                for (int j = 0; j < dsSubMenu.Tables[0].Rows.Count; j++)
                                {

                                    sSubMenuTitle = dsSubMenu.Tables[0].Rows[j][0].ToString();
                                    sSubMenuTooltip = dsSubMenu.Tables[0].Rows[j][1].ToString();
                                    sSubMenuHref = dsSubMenu.Tables[0].Rows[j][2].ToString();
                                    sRightMenuContent += "<li title=\"" + sSubMenuTooltip + "\" class=\"menu-item\"><a class=\"submenu-a\" href=\"" + sSubMenuHref + "\" target=\"_blank\" style=\"width: 270px; border-left-width: 0;\">" + sSubMenuTitle + "</a></li>";


                                }
                            
                            sRightMenuContent += "</ul>";
                            sRightMenuContent += "<i class=\"submenu_arrowright\"></i>";
                            sRightMenuContent += " </div>";
                            sRightMenuContent += "</div>";
                            sRightMenuContent += " </div>";
                            sRightMenuContent += " </ul>";
                           
                            sRightMenuContent += " </div>";
                        }
                        else if (sGrpPosition == "LEFT")
                        {
                            sLeftMenuContent += "<div class=\"menu-container\">";
                            sLeftMenuContent += "<ul class=\"menu\">";
                            sLeftMenuContent += "<div style=\" position: relative; float: left; width: 100%;\" class=\"div_menu_item\">";
                            sLeftMenuContent += "<li class=\"menu-item\">";
                            sLeftMenuContent += "<a href=\"javascript:void(0);\" class=\"menu-li-windows8\">" + sGrpTitleHindi + "<br>" + sGrpTitle + "</a>";
                            sLeftMenuContent += "</li>";
                            sLeftMenuContent += "<div class=\"submenu-container left\" style=\"display: none;\">";
                            sLeftMenuContent += "<ul style=\" padding-left: 0;\">";
                            for (int j = 0; j < dsSubMenu.Tables[0].Rows.Count; j++)
                            {
                                sSubMenuTitle = dsSubMenu.Tables[0].Rows[j][0].ToString();
                                sSubMenuTooltip = dsSubMenu.Tables[0].Rows[j][1].ToString();
                                sSubMenuHref = dsSubMenu.Tables[0].Rows[j][2].ToString();
                                sLeftMenuContent += "<li title=\"" + sSubMenuTooltip + "\" class=\"menu-item\"><a class=\"submenu-a\" href=\"" + sSubMenuHref + "\" target=\"_blank\" style=\"width: 270px; border-left-width: 0;\">" + sSubMenuTitle + "</a></li>";
                            }
                            sLeftMenuContent += "</ul>";
                            sLeftMenuContent += "<i class=\"submenu_arrowleft\"></i>";
                            sLeftMenuContent += "</div>";
                            sLeftMenuContent += "</div>";
                            sLeftMenuContent += "</ul>";
                            sLeftMenuContent += "</div>";
                        }
                    }
                }
                if (sRightMenuContent != "")
                {
                    div_right_menu.Visible = true;
                    div_right_menu.InnerHtml = sRightMenuContent;
                }

                if (sLeftMenuContent != "")
                {
                    div_left_menu.Visible = true;
                    div_left_menu.InnerHtml = sLeftMenuContent;
                }
            }
            #endregion

            #region Whats New

            string sWhatsNewQuery = "SELECT [MENU_TITLE],[TOOLTIP],[HREF],[TIMESTAMP] FROM [CY_LP_WHATS_NEW] WHERE [LP_STATUS] = 'SHOW' ORDER BY [ORDER_BY] ASC, [TIMESTAMP] DESC";
            DataSet dsWhatsNew = My.ExecuteSELECTQuery(sWhatsNewQuery);
            if (dsWhatsNew.Tables[0].Rows.Count == 0)
            {
                ul_news_bullet.InnerHtml = "<li><img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\" /><a target=\"_blank\" href=\"javascript:void(0);\">No Recent News</a></li>";
            }
            else
            {
                string sNewsList = "";
                for (int i = 0; i < dsWhatsNew.Tables[0].Rows.Count; i++)
                {
                    string sNewsTitle = dsWhatsNew.Tables[0].Rows[i][0].ToString();
                    string sNewsTootlip = dsWhatsNew.Tables[0].Rows[i][1].ToString();
                    string sNewsHref = dsWhatsNew.Tables[0].Rows[i][2].ToString();
                    DateTime dtStartDate = Convert.ToDateTime(dsWhatsNew.Tables[0].Rows[i][3].ToString());
                    if (today.Day - dtStartDate.Day < 3 && today.Month - dtStartDate.Month == 0 && today.Year - dtStartDate.Year == 0)
                        sNewsList += "<li title=\"" + sNewsTootlip + "\"><img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\" /><a target=\"_blank\" href=\"" + sNewsHref + "\">" + sNewsTitle + "<span style='color:blue'> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp New</span>" + "</a></li>";
                    else
                        sNewsList += "<li title=\"" + sNewsTootlip + "\"><img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\" /><a target=\"_blank\" href=\"" + sNewsHref + "\">" + sNewsTitle + "</a></li>";
                }
                ul_news_bullet.InnerHtml = sNewsList;
            }
            #endregion

            #region Today's Birthday
            string sEmpBdayQuery = "";
            sEmpBdayQuery += "SELECT [O_EMP_MSTR].[O_CPF_NMBR],                                              ";
            sEmpBdayQuery += "       [O_EMP_MSTR].[O_EMP_NM],                                                ";
            sEmpBdayQuery += "       [O_EMP_MSTR].[O_EMP_MBL_NMBR],                                          ";
            sEmpBdayQuery += "       [O_EMP_MSTR].[O_EMP_DESGNTN]                                            ";
            sEmpBdayQuery += "FROM   [O_EMP_MSTR],                                                           ";
            sEmpBdayQuery += "       [E_USER_MSTR]                                                           ";
            sEmpBdayQuery += "WHERE  [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE]                 ";
            sEmpBdayQuery += "       AND DATEPART(MONTH, [O_EMP_MSTR].[O_EMP_DOB]) =                         ";
            sEmpBdayQuery += "           DATEPART(MONTH, GETDATE())                                          ";
            sEmpBdayQuery += "       AND DATEPART(DAY, [O_EMP_MSTR].[O_EMP_DOB]) = DATEPART(DAY, GETDATE())  ";
            sEmpBdayQuery += "       AND [O_EMP_MSTR].[O_EMP_DESGNTN] != ''                                  ";
            sEmpBdayQuery += "       AND [O_EMP_MSTR].[O_EMP_MBL_NMBR] != ''                                 ";
            sEmpBdayQuery += "       AND [O_ACTIVE_USER]=1 ";
            //string sEmpBdayQuery = "SELECT [O_CPF_NMBR],[O_EMP_NM],[O_EMP_MBL_NMBR],[O_EMP_DESGNTN] FROM [O_EMP_MSTR] WHERE DATEPART(MONTH,[O_EMP_DOB])=DATEPART(MONTH,GETDATE()) AND DATEPART(DAY,[O_EMP_DOB])=DATEPART(DAY,GETDATE()) AND O_EMP_DESGNTN!=''";
            //string sEmpBdayQuery = "SELECT [O_CPF_NMBR],[O_EMP_NM],[O_EMP_MBL_NMBR],[O_EMP_DESGNTN] FROM [O_EMP_MSTR] WHERE DATEPART(MONTH,[O_EMP_DOB])=DATEPART(MONTH,GETDATE()) AND DATEPART(DAY,[O_EMP_DOB])=DATEPART(DAY,GETDATE()) AND O_EMP_DESGNTN!='' AND O_EMP_MBL_NMBR!=''";
            DataSet dsEmpBday = My.ExecuteSELECTQuery(sEmpBdayQuery);
            if (dsEmpBday.Tables[0].Rows.Count == 0)
            {
                marquee_container.Visible = false;
                BirthdayHeading.Text = "No Birthday Today";
            }
            else
            {
                BirthdayHeading.Text = dsEmpBday.Tables[0].Rows.Count + " जन्मदिन Birthday Today";
                string sBdayDiv = "";
                for (int i = 0; i < dsEmpBday.Tables[0].Rows.Count; i++)
                {
                    string sEmpCPFNo = dsEmpBday.Tables[0].Rows[i][0].ToString();
                    string sEmpName = dsEmpBday.Tables[0].Rows[i][1].ToString();
                    string sEmpMblNo = dsEmpBday.Tables[0].Rows[i][2].ToString();
                    string sEmpDesig = dsEmpBday.Tables[0].Rows[i][3].ToString();

                    sBdayDiv += "<div style=\"display: inline-block; margin-right: 30px; padding: 8px;\">";
                    sBdayDiv += " <p>Name : <span>" + sEmpName + "(" + sEmpCPFNo + ")</span></p>";
                    sBdayDiv += "<p>Desig. : <span>" + sEmpDesig + "</span></p>";
                    sBdayDiv += "<p>Mobile : <span>" + sEmpMblNo + "</span></p>";
                    sBdayDiv += "<center><H1>***************</H1></center></div>";
                }
                marquee_container.InnerHtml = sBdayDiv;
            }
            #endregion
            #region Plant Head page handling
            //  ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            string sPH_CODE = System.Configuration.ConfigurationSettings.AppSettings["PlantHeadCode"].ToString();
            string sQuery = "SELECT [ID],[PH_EMP_CODE],[MENU_TITLE],[TOOLTIP],[HREF],[IS_DWNL],[ORDER_BY],[ENTRY_BY],[TIMESTAMP] FROM [CY_PLANT_HEAD_PAGE] WHERE [PH_EMP_CODE] = '" + sPH_CODE + "' ORDER BY [TIMESTAMP] DESC";
            DataSet dsPantHead = My.ExecuteSELECTQuery(sQuery);
            if (dsPantHead.Tables[0].Rows.Count > 0)
            {
                //rptrPlantHead.DataSource = dsPantHead;
                //rptrPlantHead.DataBind();
                string sNewsListPlantHead = "";
                for (int i = 0; i < dsPantHead.Tables[0].Rows.Count; i++)
                {
                    string sNewsTitle = dsPantHead.Tables[0].Rows[i]["MENU_TITLE"].ToString();
                    string sNewsTootlip = dsPantHead.Tables[0].Rows[i]["TOOLTIP"].ToString();
                    string sNewsHref = dsPantHead.Tables[0].Rows[i]["HREF"].ToString();
                    sNewsListPlantHead += "<li title=\"" + sNewsTootlip + "\"><img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\" /><a target=\"_blank\" href=\"" + sNewsHref + "\">" + sNewsTitle + "</a></li>";
                }
                ul_plant_head_bullet.InnerHtml = sNewsListPlantHead;
            }
            else
            {

            }
            #endregion


            bindimage();
        }

        protected void cmdAccountSetting_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewUserReg.aspx?my_hint=UPDATE");
        }

        protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Session["Login_Name"] = "";
            Session["EmployeeName"] = "";
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //    if (FileUpload1.PostedFile != null)
            //    {

            //        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            //        FileUpload1.SaveAs(Server.MapPath("images/" + FileName));

            //        string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
            //        SqlConnection con = new SqlConnection(strConn);
            //        string strQuery = "insert into UPLOAD_IMAGE (PERSON_NAME,IMAGE_NAME, IMAGE_PATH)" + " values(@PERSON_NAME,@IMAGE_NAME, @IMAGE_PATH)";
            //        SqlCommand cmd = new SqlCommand(strQuery);
            //        cmd.Parameters.AddWithValue("@PERSON_NAME", "ARYA");
            //        cmd.Parameters.AddWithValue("@IMAGE_NAME", FileName);
            //        cmd.Parameters.AddWithValue("@IMAGE_PATH", "images/" + FileName);

            //        cmd.CommandType = CommandType.Text;
            //        cmd.Connection = con;
            //        try
            //        {
            //            con.Open();
            //            cmd.ExecuteNonQuery();
            //        }
            //        catch (Exception ex)
            //        {
            //            Response.Write(ex.Message);
            //        }

            //        finally
            //        {
            //            con.Close();
            //            con.Dispose();
            //        }

            //    }
        }


        private void bindimage()
        {
            try
            {

                string sImagePath = "";
                string str = "SELECT PERSON_NAME,IMAGE_PATH FROM UPLOAD_IMAGE WHERE ID = (SELECT Max(ID) FROM UPLOAD_IMAGE)";
                ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
                DataSet ds = My.ExecuteSELECTQuery(str);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    sImagePath = ds.Tables[0].Rows[0]["IMAGE_PATH"].ToString();
                    namelbl.Text = ds.Tables[0].Rows[0]["PERSON_NAME"].ToString();

                    string sOutputPath = HttpContext.Current.Server.MapPath("~/profilepic/" + sImagePath + "");
                    imgManager.ImageUrl = "~/profilepic/" + sImagePath + "";// sOutputPath;
                }

                //string[] filPaths = Directory.GetFiles(sOutputPath);
                //for (int i = 0; i < filPaths.Length; i++)
                //{
                //    imgManager.ImageUrl = filPaths[i];                    
                //}
            }
            catch (Exception ex)
            {

            }
        }

    }
}