﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;

namespace ONGCUIProjects
{
    public partial class ViewContacts : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {

                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                //string sQuery = "SELECT [NAME],[DESIG],[DEPARTMENT],[EXTN],[RES],[OFF],[MOBILE],[EMAIL],[EMAIL2],[TYPE] FROM [CY_CONTACTS]";
                string sQuery = "SELECT * FROM [CY_VIEW_CONTACTS] WHERE [STATUS] = 'ACTIVE' OR [STATUS] = 'ADMINACTIVE' ORDER BY [NAME] ASC";
                DataSet dsContact = My.ExecuteSELECTQuery(sQuery);
                string sOutput = "";
                if (dsContact.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    sOutput = "<table cellpadding='0' cellspacing='0' border='1' class='display' id='example' style='table-layout: fixed;'>";
                    sOutput += "<thead>";
                    sOutput += "<tr>";
                    sOutput += "<th>Name</th>";
                    sOutput += "<th>Desigation</th>";
                    sOutput += "<th>Department</th>";
                    sOutput += "<th>Extn</th>";
                    sOutput += "<th>Residence</th>";
                    sOutput += "<th>Office</th>";
                    sOutput += "<th>Mobile</th>";
                    sOutput += "<th>ONGC Email</th>";
                    sOutput += "<th>Other Email</th>";
                    sOutput += "</tr>";
                    sOutput += "</thead>";
                    sOutput += "<tbody>";

                    for (int i = 0; i < dsContact.Tables[0].Rows.Count; i++)
                    {
                        string sName = dsContact.Tables[0].Rows[i]["NAME"].ToString().ToUpper();
                        string sDesig = dsContact.Tables[0].Rows[i]["DESIG"].ToString().ToUpper();
                        string sDept = dsContact.Tables[0].Rows[i]["DEPARTMENT"].ToString().ToUpper();
                        string sExtn = dsContact.Tables[0].Rows[i]["EXTN"].ToString();
                        string sRes = dsContact.Tables[0].Rows[i]["RES"].ToString();
                        string sOff = dsContact.Tables[0].Rows[i]["OFF"].ToString();
                        string sMobile = dsContact.Tables[0].Rows[i]["MOBILE"].ToString();
                        string sEmail = dsContact.Tables[0].Rows[i]["EMAIL"].ToString();
                        string sEmail2 = dsContact.Tables[0].Rows[i]["EMAIL2"].ToString();
                        string sType = dsContact.Tables[0].Rows[i]["TYPE"].ToString();
                        sOutput += "<tr>";
                        sOutput += "<td>" + sName + "</td>";
                        sOutput += "<td style='word-wrap: break-word;'>" + sDesig + "</td>";
                        sOutput += "<td style='word-wrap: break-word;'>" + sDept + "</td>";
                        sOutput += "<td>" + sExtn + "</td>";
                        sOutput += "<td>" + sRes + "</td>";
                        sOutput += "<td>" + sOff + "</td>";
                        sOutput += "<td>" + sMobile + "</td>";
                        sOutput += "<td style='word-wrap: break-word;'>" + sEmail + "</td>";
                        sOutput += "<td style='word-wrap: break-word;'>" + sEmail2 + "</td>";
                        //sOutput += "<td>" + sType + "</td>";
                        sOutput += "</tr>";
                    }
                    sOutput += "</tbody>";
                    sOutput += "</table>";
                }
                table_container.InnerHtml = sOutput;



        


            }

        }

        protected void cmdExportToPDF_Click(object sender, EventArgs e)
        {

            string sQuery = "SELECT [ID] AS [CPF/SL NO],UPPER(LTRIM(RTRIM([NAME]))) AS [NAME],[DESIG] AS [DESIGNATION],[DEPARTMENT],";
            sQuery += "[EXTN] AS [EXTN],[RES],[OFF],[MOBILE],[EMAIL] AS [ONGC MAIL],[EMAIL2] AS [OTHER MAIL] FROM [CY_VIEW_CONTACTS] WHERE [STATUS] = 'ACTIVE' OR [STATUS] = 'ADMINACTIVE' ORDER BY [NAME] ASC";
            DataSet ds = My.ExecuteSELECTQuery(sQuery);
            DataTable dtdataTable = ds.Tables[0];
            Font font = FontFactory.GetFont(FontFactory.TIMES, 7, Font.NORMAL);
            Font font1 = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            DateTime dtToday = System.DateTime.Today;
            string filename = dtToday.Day + "_" + dtToday.ToString("MM") + "_" + dtToday.Year + " - " + dtdataTable.Rows.Count + " Contacts";
            Document document = new Document(new Rectangle(288f, 144f), 22f, 22f, 25f, 25f);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + filename + ".pdf"; ;
            FileInfo newFile = new FileInfo(path);
            PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
            Font Heading1font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.NORMAL);
            Font Heading2font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, Font.NORMAL);
            Font ColFont = FontFactory.GetFont(FontFactory.TIMES, 7, Font.NORMAL);
            Font ColFonttotal = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            Font ColFontHeader = FontFactory.GetFont(FontFactory.TIMES_BOLD, 7, Font.NORMAL);
            Paragraph Heading1 = new Paragraph("Oil and Natural Gas Corporation Limited\n", Heading1font);
            Heading1.Alignment = Element.ALIGN_CENTER;
            Paragraph Heading2 = new Paragraph("Uran Plant, Uran", Heading2font);
            Heading2.Alignment = Element.ALIGN_CENTER;
            document.Open();
            document.Add(Heading1);
            document.Add(Heading2);
            Paragraph TableC_P1 = new Paragraph("\nTELEPHONE DIRECTORY (" + dtdataTable.Rows.Count + " Contacts)", font1);
            TableC_P1.Alignment = Element.ALIGN_CENTER;
            document.Add(TableC_P1);
            document.Add(new Paragraph(" "));
            PdfPTable TelDir = new PdfPTable(dtdataTable.Columns.Count);
            float[] TableCwidths = new float[] { 1.5f, 4f, 4f, 4f, 1f, 1.5f, 1.5f, 1.5f, 4f, 4f };
            TelDir.SetWidths(TableCwidths);
            TelDir.WidthPercentage = 100;
            for (int Col = 0; Col < dtdataTable.Columns.Count; Col++)
            {
                Paragraph pr = new Paragraph(dtdataTable.Columns[Col].ColumnName, ColFontHeader);
                if (Col == 0)
                { pr.Alignment = Element.ALIGN_CENTER; }
                else
                { pr.Alignment = Element.ALIGN_CENTER; }
                PdfPCell cell = new PdfPCell();
                cell.AddElement(pr);
                TelDir.AddCell(cell);
            }

            for (int Row = 0; Row < dtdataTable.Rows.Count; Row++)
            {
                for (int Col1 = 0; Col1 < dtdataTable.Columns.Count; Col1++)
                {
                    PdfPCell cell = new PdfPCell();
                    Paragraph ph = new Paragraph(dtdataTable.Rows[Row].ItemArray[Col1].ToString(), ColFont);
                    if (Col1 == 0)
                    {
                        ph.Alignment = Element.ALIGN_CENTER;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    else if (Col1 >= 1 && Col1 <= 3)
                    {
                        ph.Alignment = Element.ALIGN_LEFT;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    else
                    {
                        ph.Alignment = Element.ALIGN_CENTER;
                        ph.Alignment = Element.ALIGN_MIDDLE;
                    }
                    cell.AddElement(ph);
                    TelDir.AddCell(cell);
                }
            }
            document.Add(TelDir);
            document.Close();

        }

        protected void cmdExportToExcel_Click(object sender, EventArgs e)
        {
            string sQuery = "SELECT [ID] AS [CPF/SL NO],UPPER(LTRIM(RTRIM([NAME]))) AS [NAME],[DESIG] AS [DESIGNATION],[DEPARTMENT],";
            sQuery += "[EXTN] AS [EXTN],[RES],[OFF],[MOBILE],[EMAIL] AS [ONGC MAIL],[EMAIL2] AS [OTHER MAIL] FROM [CY_VIEW_CONTACTS] WHERE [STATUS] = 'ACTIVE' OR [STATUS] = 'ADMINACTIVE' ORDER BY [NAME] ASC";
            DataSet ds = My.ExecuteSELECTQuery(sQuery);

            DateTime dtToday = System.DateTime.Today;
            string filename = dtToday.Day + "_" + dtToday.ToString("MM") + "_" + dtToday.Year + " - " + ds.Tables[0].Rows.Count + " Contacts.xls";

            GridView TempGridView=new GridView();
            TempGridView.DataSource = ds;
            TempGridView.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stwr = new StringWriter();
            HtmlTextWriter htew = new HtmlTextWriter(stwr);
            TempGridView.RenderControl(htew);
            Response.Write(stwr.ToString());
            Response.End();
        }



    }
}