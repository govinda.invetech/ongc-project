﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class TransportBookingAddEdit : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sActionType = "";
                if (string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    txtHideActionType.Value = "NEW";
                    txtHideBookingID.Value = "-1";
                }
                else
                {
                    sActionType = Request.QueryString["type"].ToString();
                    txtHideActionType.Value = My.sDataStringTrim(1,sActionType);
                    txtHideBookingID.Value = My.sDataStringTrim(2, sActionType);
                }
                string cpf_no = Session["Login_Name"].ToString();

                #region Applicant Detail
                string str = "SELECT [O_EMP_NM] ,[O_CPF_NMBR]  ,[O_EMP_DESGNTN] ,[O_EMP_DEPT_NM],[CY_EMP_EX_NO],[O_EMP_MBL_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
                DataSet ds = My.ExecuteSELECTQuery(str);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Response.Write("FALSE~ERR~Applicant Detail Not Found");
                }
                else
                {
                    txtappname.Value = ds.Tables[0].Rows[0][0].ToString();
                    txtappcpf_no.Value = ds.Tables[0].Rows[0][1].ToString();
                    txtappdesign.Value = ds.Tables[0].Rows[0][2].ToString();
                    txtapplocation.Value = ds.Tables[0].Rows[0][3].ToString();
                    txtappphoneex_no.Value = ds.Tables[0].Rows[0][4].ToString();
                }
                #endregion

                #region Report To Detail
                reporting_name.Value = ds.Tables[0].Rows[0][0].ToString();
                reporting_contact1.Value = ds.Tables[0].Rows[0][5].ToString();
                #endregion

                #region If Update
                if (sActionType != "")
                {

                    string sBookingID = My.sDataStringTrim(2, sActionType);
                    string sViewEditQuery = "SELECT BOOKING_TYPE, BOOKING_START_DATETIME, BOOKING_END_DATETIME, REMARKS, PURPOSE, NO_OF_PERSON_TRAVELLING, INCHARGE_NAME, ";
                    sViewEditQuery += "INCHARGE_CPF_NO, INCHARGE_DESIG, APPROVER_NAME, APPROVER_CPF_NO, APPROVER_DESIG, ALLOCATER_NAME, ALLOCATER_CPF_NO, ";
                    sViewEditQuery += "ALLOCATER_DESIG, REPORTING_TIME, REPORT_TO_NAME, REPORT_TO_ADDERSS, REPORT_TO_CONTACT1, REPORT_TO_CONTACT2 ";
                    sViewEditQuery += "FROM CY_TRANSPORT_BOOKING_DETAIL WHERE id = '" + sBookingID + "'";

                    DataSet dsViewUpdate = My.ExecuteSELECTQuery(sViewEditQuery);

                    ddltypeofbooking.Value = dsViewUpdate.Tables[0].Rows[0][0].ToString();

                    DateTime dtBookingDateTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][1]);
                    txtbookingstartdatetime.Value = dtBookingDateTime.ToString("dd-MM-yyyy HH:mm");

                    DateTime dtEndDateTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][2]);
                    txtbookingEnddatetime.Value = dtEndDateTime.ToString("dd-MM-yyyy HH:mm");

                    txtremarks.Value = dsViewUpdate.Tables[0].Rows[0][3].ToString();
                    txtpurpose.Value = dsViewUpdate.Tables[0].Rows[0][4].ToString();
                    no_of_person_travel.Value = dsViewUpdate.Tables[0].Rows[0][5].ToString();


                    incharge_officer_name.Value = dsViewUpdate.Tables[0].Rows[0][6].ToString();
                    incharge_officer_cpf_no.Value = dsViewUpdate.Tables[0].Rows[0][7].ToString();
                    incharge_officer_Desig.Value = dsViewUpdate.Tables[0].Rows[0][8].ToString();


                    transport_approver_name.Value = dsViewUpdate.Tables[0].Rows[0][9].ToString();
                    transport_approver_cpf_no.Value = dsViewUpdate.Tables[0].Rows[0][10].ToString();
                    transport_approver_desig.Value = dsViewUpdate.Tables[0].Rows[0][11].ToString();


                    transport_allotment_name.Value = dsViewUpdate.Tables[0].Rows[0][12].ToString();
                    transport_allotment_cpf_no.Value = dsViewUpdate.Tables[0].Rows[0][13].ToString();
                    transport_allotment_desig.Value = dsViewUpdate.Tables[0].Rows[0][14].ToString();

                    DateTime dtReportingTimeTime = Convert.ToDateTime(dsViewUpdate.Tables[0].Rows[0][15]);
                    reporting_time.Value = dtReportingTimeTime.ToString("dd-MM-yyyy HH:mm");
                    reporting_name.Value = dsViewUpdate.Tables[0].Rows[0][16].ToString();
                    reporting_address.Value = dsViewUpdate.Tables[0].Rows[0][17].ToString();
                    reporting_contact1.Value = dsViewUpdate.Tables[0].Rows[0][18].ToString();
                    reporting_contact2.Value = dsViewUpdate.Tables[0].Rows[0][19].ToString();



                }
                #endregion

            }
        }
    }
}