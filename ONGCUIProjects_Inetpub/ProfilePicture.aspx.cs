﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;

namespace ONGCUIProjects
{
    public partial class ProfilePicture : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"].ToString() != "admin")//authorization only for admin 
            {
                foradmin3.Visible = false;
            }


        }

        [System.Web.Services.WebMethod]
        public static string DeleteImage(string imageurl)
        {
            if (HttpContext.Current.Session["Login_Name"].ToString() == "admin")//authorization only for admin 
            {
                string imgPath = imageurl.Replace("/profilepic", "");            
                string mappath = HttpContext.Current.Server.MapPath("~/profilepic");
                FileInfo info1 = new FileInfo(mappath + imgPath);
                try
                {
                    if (info1.Exists)
                    {
                        string dbimgPath = imageurl.Replace("/profilepic/", "");
                        RemoveImagefromDB(dbimgPath);
                        info1.Delete();
                        return "{\"status\":\"success\" ,\"message\":\"Image has been delete successfully !\"}";
                    }
                    else
                    {
                        return "{\"status\":\"notfound\" ,\"message\":\"Image can't be delete!\"}";
                    }
                }
                catch (Exception ex)
                {
                    return "{\"status\":\"error\" ,\"message\":\" " + ex.Message.ToString() + "!\"}";
                }
            }
            else
            {
                return "{\"status\":\"notadmin\" ,\"message\":\"Sorry you can't delete it!\"}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string DeleteImageFolder(string imageurl)
        {
            if (HttpContext.Current.Session["Login_Name"].ToString() == "admin")//authorization only for admin 
            {
                string imgPath = imageurl.Replace("/profilepic", "");
                string mappath = HttpContext.Current.Server.MapPath("~/profilepic/") + imgPath;
                FileInfo info1 = new FileInfo(mappath + imgPath);
                try
                {
                    if (Directory.Exists(mappath))
                    {
                        RemoveDirectories(mappath);
                        Directory.Delete(mappath);
                        return "{\"status\":\"success\" ,\"message\":\"folder has been delete successfully !\"}";
                    }
                    else
                    {
                        return "{\"status\":\"notfound\" ,\"message\":\"folder can't be delete!\"}";
                    }
                }
                catch (Exception ex)
                {
                    return "{\"status\":\"error\" ,\"message\":\" " + ex.Message.ToString() + "!\"}";
                }
            }
            else
            {
                return "{\"status\":\"notadmin\" ,\"message\":\"Sorry you can't delete it!\"}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetImagesFolder(string Type)
        {
            try
            {
                string JsonSting = "{\"folder\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~/profilepic");
                string[] filPaths = Directory.GetDirectories(sOutputPath);
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath + "\\", "");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"folderName\":\"" + filPaths[i] + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetImagesFromFolder(string Folder)
        {
            try
            {
                string JsonSting = "{\"photo\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~/profilepic/" + Folder + "");
                string[] filPaths = Directory.GetFiles(sOutputPath);
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath, "\\profilepic\\" + Folder + "");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"baseUrl\":\"" + filPaths[i] + "\",\"title\":\"" + filPaths[i].Replace(".jpg", "").Replace(".png", "") + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }
        }
        [System.Web.Services.WebMethod]
        public static string GetDocFromFolder(string Type)
        {
            try
            {
                string JsonSting = "{\"photo\":[";
                string sOutputPath = HttpContext.Current.Server.MapPath("~");
                string[] filPaths = Directory.GetFiles(sOutputPath + "docs");
                for (int i = 0; i < filPaths.Length; i++)
                {
                    filPaths[i] = filPaths[i].Replace(sOutputPath, "\\");
                    filPaths[i] = filPaths[i].Replace("\\", "/");
                    JsonSting += "{\"baseUrl\":\"" + filPaths[i] + "\",\"title\":\"" + filPaths[i].Replace("/docs/", "").Replace(".jpg", "").Replace(".png", "") + "\"},";
                }
                JsonSting = JsonSting.Substring(0, JsonSting.Length - 1) + "]}";
                return JsonSting;
            }
            catch
            {
                return "{}";
            }

        }

        public static bool createPath(string FilePath) //"files/Folder/SubFolder/example.extn"
        {
            try
            {
                string[] DirPathArr = FilePath.Split('/');
                string sOutputPath = HttpContext.Current.Server.MapPath("~");
                for (int i = 0; i < DirPathArr.Length; i++)
                {
                    if (i == DirPathArr.Length - 1)
                    {
                        sOutputPath += "/" + DirPathArr[i].ToString();
                        if (File.Exists(sOutputPath))
                        {
                            File.Delete(sOutputPath);
                        }
                    }
                    else
                    {
                        sOutputPath += "/" + DirPathArr[i].ToString();
                        if (!Directory.Exists(sOutputPath))
                        {
                            Directory.CreateDirectory(sOutputPath);
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (Session["Login_Name"].ToString() == "admin")//authorization only for admin 
            {

                Span1.Text = string.Empty;
                string filepath = "";
                Button btn = (Button)sender;
                string folderName = btn.Attributes["folder_name"];
                string sPERSON_NAME = txtUserName.Text;

                if (folderName == "new")
                {
                    try
                    {
                        filepath = Server.MapPath("\\profilepic");
                        Directory.CreateDirectory(filepath + "\\" + txtFolderName.Text);
                    }
                    catch
                    {
                        Span1.Text = "There is some error plz try again!";
                        return;
                    }
                }
                filepath = Server.MapPath("\\profilepic\\" + txtFolderName.Text + "");
                HttpFileCollection uploadedFiles = Request.Files;


                for (int i = 0; i < uploadedFiles.Count; i++)
                {
                    HttpPostedFile userPostedFile = uploadedFiles[i];
                    try
                    {
                        if (userPostedFile.ContentLength > 0)
                        {

                            string fileExtension = Path.GetExtension(userPostedFile.FileName).ToUpper();
                            if (fileExtension == ".JPG" || fileExtension == ".PNG" || fileExtension == ".JPEG" || fileExtension == ".TIF" || fileExtension == ".GIF")
                            {
                                userPostedFile.SaveAs(filepath + "\\" + Path.GetFileName(userPostedFile.FileName));

                                string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
                                SqlConnection con = new SqlConnection(strConn);
                                string path = txtFolderName.Text + "/" + userPostedFile.FileName;
                                string sInsert = "insert into UPLOAD_IMAGE (PERSON_NAME,IMAGE_NAME, IMAGE_PATH)" + " values('" + sPERSON_NAME + "','" + userPostedFile.FileName + "','" + path + "')";
                                if (My.ExecuteSQLQuery(sInsert) == true)
                                {
                                    /*Data is saved  DB*/
                                }
                            }
                            else
                            {
                                Response.Write("<script>alert('Sorry! uploading document should be jpg or png or jpeg or tif or gif!');</script>");
                                return;
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        Span1.Text += "Error: <br>" + Ex.Message;
                    }
                }
                Span1.Text = "Images upload successfully !";
            }
            else
            {
                Response.Write("<script>alert('Sorry! only Admin has authorization of uploading!');</script>");
            }
        }

        public static void RemoveDirectories(string strpath)
        {
            //This condition is used to delete all files from the Directory
            foreach (string file in Directory.GetFiles(strpath))
                File.Delete(file);
        }


        public static void RemoveImagefromDB(string strpath)
        {
            string str = "DELETE FROM UPLOAD_IMAGE WHERE IMAGE_PATH='" + strpath + "'";
            if (MyApplication1.ExecuteSQLQuery_static(str) == true)
            {
                
            }
        }


    }
}