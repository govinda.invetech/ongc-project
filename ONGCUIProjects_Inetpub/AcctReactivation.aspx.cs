﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace ONGCUIProjects
{
    public partial class AcctReactivation : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new MyApplication1();

        #region Variable
        string sStatus;
        string sCPFNo;
        string sMessageText;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            sCPFNo = Request.QueryString["cpf"];
            sStatus = Request.QueryString["error"];
            switch (sStatus)
            {
                case "DEACTIVE":
                    sMessageText = "Send account activation request by clicking below button";
                    break;
                case "REQUEST":
                    sMessageText = "You have already sent account activation request to Administrator";
                    cmdReActivate.Visible = false;
                    break;
                case "REJECT":
                    sMessageText = "Your account activation request has been rejected by Administrator. You can again request for account activation";
                    break;
            }
            lblResponseMessage.Text = sMessageText;
        }

        protected void cmdReActivate_Click(object sender, EventArgs e)
        {
            Boolean bRequest;
            bRequest = My.InsUpdEmpAccountStatus(sCPFNo, "REQUEST");
            string sMessage = "";
            if (bRequest == true)
            {
                cmdReActivate.Visible = false;
                switch (sStatus)
                {
                    case "DEACTIVE":
                        lblResponseMessage.Text = "Your request has been submitted";
                        break;
                    case "REJECT":
                        lblResponseMessage.Text = "Your request has been again submitted";
                        break;
                }
            }
            else
            {
                lblResponseMessage.Text = "Problem In Execution, Contact to Admin";
            }
            lblResponseMessage.Visible = true;
            Response.Write("<script type='text/javascript'>alert('Your account activation request submitted successfully.');</script>");
            Response.Write("<script type='text/javascript'>window.location='Index.aspx';</script>");
        }
    }
}