﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident.fancybox
{
    public partial class _3_IncdntManagerViewAction : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncdntID = Request.QueryString["IncdID"].ToString();

            IncidentClass CurrentIncdnt = new IncidentClass(sIncdntID);
            lblIncdntNo.Text = sIncdntID;

            lblIncdntStatus.Text = CurrentIncdnt.StatusLevel;

            string query = "SELECT [LEVEL] FROM [CY_INC_EMP_RELATION] WHERE [LEVEL] = 'LEVEL1' AND [INCDNT_NO] = '" + sIncdntID + "'";
            DataSet dstemp = My.ExecuteSELECTQuery(query);
            if (dstemp.Tables[0].Rows.Count != 0)
            {
                lblIncdntStatus.Text = "9";
            }

            

            txtIncdntDesc.Text = CurrentIncdnt.RptDesc;
            if (CurrentIncdnt.StatusLevel == "4")
            {
                p_comment_type.InnerHtml = "Executer Remark";
                txtFPRActionTaken.Text = CurrentIncdnt.IncFPRRemark;
            }
            else if (CurrentIncdnt.StatusLevel == "9")
            {
                p_comment_type.InnerHtml = "Acceptance Authority Remark";
                txtFPRActionTaken.Text = CurrentIncdnt.IncLevel1Comment;
            }
        }
    }
}