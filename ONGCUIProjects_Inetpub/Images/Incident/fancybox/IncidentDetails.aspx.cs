﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident.fancybox
{
    public partial class IncidentDetails : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '/Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncdentID = Request.QueryString["IncdID"];
            string level = Request.QueryString["level"];
            HiddenField1.Value = level.ToString();

            IncidentClass CurrentIncident = new IncidentClass(sIncdentID);


            lblIncidentID.Text = sIncdentID;

            lblRushToHospital.Text = CurrentIncident.RptRushToHospital;

            txtRptEngName.Text = CurrentIncident.RptName;
            txtRptEngCPF.Text = CurrentIncident.RptCPF;
            txtRptEngDesig.Text = CurrentIncident.RptCPF;
            txtIncdntDesc.Text = CurrentIncident.RptDesc;
            txtIncdntDate.Text = CurrentIncident.RptDate.ToShortDateString();
            txtIncdntTime.Text = CurrentIncident.RptTime;
            txtIncdntLocation.Text = CurrentIncident.RptLocation;
            txtIncdntArea.Text = CurrentIncident.RptArea;
            txtIncdntDept.Text = CurrentIncident.RptDept;
            txtIncdntImmdRemedial.Text = CurrentIncident.ImmediateRemedial;
            txtProcess.Text = CurrentIncident.RptProcess;
            txtProperty.Text = CurrentIncident.RptProperty;
            txtPersonalInjury.Text = CurrentIncident.RptPersonInjury;
            txtEnvironment.Text = CurrentIncident.RptEnvironment;
            txtIncdntAppCause.Text = CurrentIncident.ApparentCause;
            lblIncdntType.Text = CurrentIncident.RptIncType;


            txtReccomendations.Text = CurrentIncident.RevRecmnd;
            txtPrimaFacieReason.Text = CurrentIncident.RevRootCause;
            txtRemarks.Text = CurrentIncident.RevRemark;


            txtIncidentManager.Text = CurrentIncident.RevName;

            txtSafety.Text = CurrentIncident.RevPrecaution;

            txtIncidentType.Text = CurrentIncident.RevIncType;

            txtIncidentCategory.Text = CurrentIncident.RevIncCategory;


            //Below code is used to show incident manager
            Tab3txtIncdntArea.Text = CurrentIncident.RptArea;
            Tab3txtIncdntLocation.Text = CurrentIncident.RptLocation;
            Tab3txtAreaMgr.Text = CurrentIncident.IncMgrCPF + ". " + CurrentIncident.IncMgrName;
            Tab3txtMGRDesig.Text = CurrentIncident.IncMgrFwdDesig;
            txtActionToBeTaken.Text = CurrentIncident.IncMgrActnToBeTaken;
            txtIncdntMgrComment.Text = CurrentIncident.IncMgrRemark;
            txtTargetDate.Text = CurrentIncident.IncTrgtDate.ToString("dd-MM-yyyy");
            txtFPRName.Text = CurrentIncident.IncMgrFwdCPF+". "+CurrentIncident.IncMgrFwdName;
            txtFPRDesig.Text = CurrentIncident.IncMgrFwdDesig;
            //txtFPRCPF.Text = CurrentIncident.IncMgrFwdCPF;


            string sIncdentTableData = "";
            for (int i = 0; i < CurrentIncident.Incmgrlist.Count; i++)
            {
                if (i == 0)
                {

                }
                else
                {
                    sIncdentTableData += "<tr>";
                    sIncdentTableData += "<td>" + CurrentIncident.Incmgrlist[i].IncMgrActnToBeTaken + "</td>";
                    sIncdentTableData += "<td>" + CurrentIncident.Incmgrlist[i].IncMgrRemark + "</td>";
                    sIncdentTableData += "<td>" + CurrentIncident.Incmgrlist[i].IncMgrStatus + "</td>";
                    sIncdentTableData += "</tr>";

                }

            }
            string sTableHeading = "<tr><th>Action Taken</th><th>Comment</th><th>Decision</th></tr>";
            divTableData.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading + sIncdentTableData + "</table>";

            string sIncdentTableData1 = "";
            for (int i = 0; i < CurrentIncident.Incfprlist.Count; i++)
            {
                sIncdentTableData1 += "<tr>";
                sIncdentTableData1 += "<td>" + CurrentIncident.Incfprlist[i] + "</td>";
                sIncdentTableData1 += "</tr>";
            }
            string sTableHeading1 = "<tr><th>Action Taken</th></tr>";
            div1.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading1 + sIncdentTableData1 + "</table>";




            Tab5txtFPRName.Text = CurrentIncident.IncMgrFwdCPF + ". " + CurrentIncident.IncMgrFwdName;
            Tab5txtFPRDesig.Text=CurrentIncident.IncMgrFwdDesig;
            string sIncdentTableData2 = "";
            for (int i = 0; i < CurrentIncident.Inclvl1.Count; i++)
            {
                sIncdentTableData2 += "<tr>";
                sIncdentTableData2 += "<td>" + CurrentIncident.Inclvl1[i].IncLevel1Comment + "</td>";
                sIncdentTableData2 += "<td>" + CurrentIncident.Inclvl1[i].IncLevel1Decision + "</td>";
                sIncdentTableData2 += "</tr>";
            }
            string sTableHeading2 = "<tr><th>Comment</th><th>Decision</th></tr>";
            div2.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading2 + sIncdentTableData2 + "</table>";



            string sIncdentTableData3 = "";
            for (int i = 0; i < CurrentIncident.Inclvl2.Count; i++)
            {
                sIncdentTableData3 += "<tr>";
                sIncdentTableData3 += "<td>" + CurrentIncident.Inclvl2[i].IncLevel1Comment + "</td>";
                sIncdentTableData3 += "<td>" + CurrentIncident.Inclvl2[i].IncLevel1Decision + "</td>";
                sIncdentTableData3 += "</tr>";
            }
            string sTableHeading3 = "<tr><th>Comment</th><th>Decision</th></tr>";
            div3.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading3 + sIncdentTableData3 + "</table>";

        }
    }
}