﻿using System;
using System.Data;

namespace ONGCUIProjects.Incident
{
    public partial class _1a_NearMiss : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '../../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            #region FancyBox Values
            string sRptEngQuery = "SELECT [O_EMP_NM], [O_EMP_DESGNTN] from [O_EMP_MSTR] WHERE  [O_CPF_NMBR] ='" + Session["Login_Name"].ToString() + "'";

            DataSet dsEmpData = My.ExecuteSELECTQuery(sRptEngQuery);


            txtRptEngCPF.Text = Session["Login_Name"].ToString();
            txtRptEngName.Text = dsEmpData.Tables[0].Rows[0][0].ToString();
            txtRptEngDesig.Text = dsEmpData.Tables[0].Rows[0][1].ToString();



            //txtNearMissDate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
            //txtNearMissTime.Text = System.DateTime.Now.ToString("HH:mm");
            #endregion

        }
    }
}