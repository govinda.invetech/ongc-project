﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident.fancybox
{
    public partial class _2_IncidentReview : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncdentID = Request.QueryString["IncdID"];
            //                                  0            1           2            3             4                   5           6          
            string sIncdentQuery = "SELECT [O_INDX_NMBR],[O_ENG_NM],[O_ENG_DSG],[O_CPF_NMBR],[O_INCDNT_DESCRPTN],[O_INCDNT_DT],[O_INCDNT_TIME],";
            //                                      7               8             9         10          11          12          13          14                  15                     16     
            sIncdentQuery = sIncdentQuery + "[O_INCDNT_LCTN],[O_RUSH_EMRGNCY],[O_RMRKS],[O_SYS_DT],[O_SYS_TIME],[CY_O_AREA],[CY_O_DEPT],[CY_O_APP_ACC],[CY_O_IMD_REM_ACTION],[CY_TYPE_OF_INCIDENT],";
            //                                        17         18             19                  20
            sIncdentQuery = sIncdentQuery + "  [CY_PROPERTY],[CY_PROCESS],[CY_ENVIRONMENT],[CY_PERSONAL_INJURY] FROM [CY_INC_1_DETAILS] WHERE [O_INDX_NMBR] = '" + sIncdentID + "'";
            //dsEmployeeDtl.Tables[0].Columns["O_INDX_NMBR"].ToString();

            DataSet dsIncident = My.ExecuteSELECTQuery(sIncdentQuery);

            lblIncidentID.Text = dsIncident.Tables[0].Rows[0][0].ToString();

            lblRushToHospital.Text = dsIncident.Tables[0].Rows[0][8].ToString();

            txtRptEngName.Text = dsIncident.Tables[0].Rows[0][1].ToString();
            txtRptEngCPF.Text = dsIncident.Tables[0].Rows[0][3].ToString();
            txtRptEngDesig.Text = dsIncident.Tables[0].Rows[0][2].ToString();
            txtIncdntDesc.Text = dsIncident.Tables[0].Rows[0][4].ToString();
            txtIncdntDate.Text = Convert.ToDateTime(dsIncident.Tables[0].Rows[0][5].ToString()).ToShortDateString();
            txtIncdntTime.Text = dsIncident.Tables[0].Rows[0][6].ToString();
            txtIncdntLocation.Text = dsIncident.Tables[0].Rows[0][7].ToString();
            txtIncdntArea.Text = dsIncident.Tables[0].Rows[0][12].ToString();
            txtIncdntDept.Text = dsIncident.Tables[0].Rows[0][13].ToString();
            txtIncdntImmdRemedial.Text = dsIncident.Tables[0].Rows[0][15].ToString();
            txtProcess.Text = dsIncident.Tables[0].Rows[0][18].ToString();
            txtProperty.Text = dsIncident.Tables[0].Rows[0][17].ToString();
            txtPersonalInjury.Text = dsIncident.Tables[0].Rows[0][20].ToString();
            txtEnvironment.Text = dsIncident.Tables[0].Rows[0][19].ToString();
            txtIncdntAppCause.Text = dsIncident.Tables[0].Rows[0][14].ToString();
            lblIncdntType.Text = dsIncident.Tables[0].Rows[0][16].ToString();

            if (dsIncident.Tables[0].Rows[0][16].ToString() == "INCIDENT")
            {
                lblIncdntTypeHead.Text = "Review Incident";
                lblIncdntTypeLegend.Text = "Incident";
                string sInjuryDtlQuery = "SELECT [NAME_OF_PERSON], [DESIG], [CPF_NO], [NATURE_OF_INJURY] FROM [CY_INC_1_INJURY_RELATION] WHERE [INCIDENT_NO] = '" + sIncdentID + "'";
                DataSet dsInjuryDtl = My.ExecuteSELECTQuery(sInjuryDtlQuery);
                string sInjuryData = "";
                if (dsInjuryDtl.Tables[0].Rows.Count == 0)
                {
                    sInjuryData = "No Injury";
                }
                else
                {
                    for (int i = 0; i < dsInjuryDtl.Tables[0].Rows.Count; i++)
                    {
                        sInjuryData = sInjuryData + "<tr>";
                        sInjuryData = sInjuryData + "<td>" + (i + 1) + "</td>";
                        sInjuryData = sInjuryData + "<td style=\"width: 300px;\">" + dsInjuryDtl.Tables[0].Rows[i][0] + "</td>";
                        sInjuryData = sInjuryData + "<td>" + dsInjuryDtl.Tables[0].Rows[i][1] + "</td>";
                        sInjuryData = sInjuryData + "<td>" + dsInjuryDtl.Tables[0].Rows[i][2] + "</td>";
                        sInjuryData = sInjuryData + "<td>" + dsInjuryDtl.Tables[0].Rows[i][3] + "</td>";
                        sInjuryData = sInjuryData + "</tr>";
                    }
                }
                divInjuredPersonDetail.InnerHtml = "<p class='content'>Injured Persons</p><table class='tftable' border='1'>" + sInjuryData + "</table>";
            }
            else if (dsIncident.Tables[0].Rows[0][16].ToString() == "NEARMISS")
            {
                lblIncdntTypeHead.Text = " Review Near Miss";
                lblIncdntTypeLegend.Text = "Near Miss";
            }


            string sAreaManager = My.GetAreaManagerFromAreaAndLocation(dsIncident.Tables[0].Rows[0][12].ToString(), dsIncident.Tables[0].Rows[0][7].ToString());

           // string sEmpDdlQuery = "SELECT [O_CPF_NMBR], [O_EMP_NM]+' ['+[O_CPF_NMBR]+']' AS [O_EMP_NAME_WITH_CPF] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '6' INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE')";
            string sEmpDdlQuery = "SELECT [O_CPF_NMBR], [O_EMP_NM]+' ['+[O_CPF_NMBR]+']' AS [O_EMP_NAME_WITH_CPF] FROM [CY_MENU_EMP_RELATION] INNER JOIN [O_EMP_MSTR] ON [CY_MENU_EMP_RELATION].[cpf_number] = [O_EMP_MSTR].[O_CPF_NMBR] AND [CY_MENU_EMP_RELATION].[child_id] = '6' INNER JOIN [E_USER_MSTR] ON [O_EMP_MSTR].[O_CPF_NMBR]=[E_USER_MSTR].[E_USER_CODE] AND ([E_USER_MSTR].CY_STATUS='ACTIVE' OR [E_USER_MSTR].CY_STATUS='ADMINACTIVE') LEFT JOIN (SELECT [USER_ID],MAX([TIMESTAMP]) AS [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' GROUP BY [USER_ID])[USER_LOGINS] ON [USER_LOGINS].[USER_ID]= [O_CPF_NMBR] WHERE DATEDIFF(DAY,[USER_LOGINS].[TIMESTAMP],getdate())<60 ORDER BY [O_EMP_NM] ASC";
            DataSet dsEmployeeDtl = My.ExecuteSELECTQuery(sEmpDdlQuery);
            string sEmpOption = "";
            for (int i = 0; i < dsEmployeeDtl.Tables[0].Rows.Count; i++)
            {
                string sOptionSelected = "";
                if (My.sDataStringTrim(1, sAreaManager) == dsEmployeeDtl.Tables[0].Rows[i][0].ToString())
                {
                    sOptionSelected = "selected";
                }
                sEmpOption = sEmpOption + "<option value=\"" + dsEmployeeDtl.Tables[0].Rows[i][0].ToString() + "\" " + sOptionSelected + ">" + dsEmployeeDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            }
            divEmpList.InnerHtml = "<p class=\"content\">Corrective Action By</p><select class=\"validatefild\" id=\"SelectCorrectiveActionBy\" style=\"position:relative; float:right; width:225px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">Select Employee</option>" + sEmpOption + "</select>";



            string sSafetyDdlQuery = "SELECT [O_INDX_NMBR],[O_SFTY_MSR] FROM [CY_INC_0_SAFETY_MSTR] ORDER BY [O_SFTY_MSR] ASC";
            DataSet dsSafetyDtl = My.ExecuteSELECTQuery(sSafetyDdlQuery);
            string sSafetyOption = "";
            for (int i = 0; i < dsSafetyDtl.Tables[0].Rows.Count; i++)
            {
                sSafetyOption = sSafetyOption + "<option value=\"" + dsSafetyDtl.Tables[0].Rows[i][1].ToString() + "\">" + dsSafetyDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            }
            divSafety.InnerHtml = "<p class=\"content\">Select Safety Precaution</p><select class=\"validatefild\" id=\"SelectSafetyPrecaustion\" style=\"position: relative; height: auto; font-size: 16px; padding: 5px; width: 75%; float: right;\"><option value=\"\">Select Safety Precaution</option>" + sSafetyOption + "</select>";


            string sTypeDdlQuery = "SELECT [O_INCDNT_ID],[O_INCDNT_CTGRY],[CY_ENTRY_BY],[O_SYS_DT_TM] FROM [CY_INC_0_TYPE_MSTR] ORDER BY [O_INCDNT_CTGRY] ASC";
            DataSet dsTypeDtl = My.ExecuteSELECTQuery(sTypeDdlQuery);
            string dsTypeOption = "";
            for (int i = 0; i < dsTypeDtl.Tables[0].Rows.Count; i++)
            {
                dsTypeOption = dsTypeOption + "<option value=\"" + dsTypeDtl.Tables[0].Rows[i][1].ToString() + "\">" + dsTypeDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            }

            divTypeList.InnerHtml = "<p class=\"content\">Select Type</p><select runat=\"server\" class=\"validatefild\" id=\"ddlIncdntType\" style=\"position:relative; width:250px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">Select Type</option>" + dsTypeOption + "</select>";

            string sCategoryDdlQuery = "SELECT [ID],[CATEGORY_NAME] FROM [CY_INC_0_CATEGORY_MSTR] ORDER BY [CATEGORY_NAME] ASC";
            DataSet dsCategoryDtl = My.ExecuteSELECTQuery(sCategoryDdlQuery);
            string dsCategoryOption = "";
            for (int i = 0; i < dsCategoryDtl.Tables[0].Rows.Count; i++)
            {
                dsCategoryOption = dsCategoryOption + "<option value=\"" + dsCategoryDtl.Tables[0].Rows[i][1].ToString() + "\">" + dsCategoryDtl.Tables[0].Rows[i][1].ToString() + "</option>";
            }

            divCategoryList.InnerHtml = "<p class=\"content\">Select Category</p><select runat=\"server\" class=\"validatefild\" id=\"ddlIncdntCategory\" style=\"position:relative; width:250px; height:auto; font-size:16px; padding:5px;\"><option value=\"\">Select Category</option>" + dsCategoryOption + "</select>";
        }
    }
}