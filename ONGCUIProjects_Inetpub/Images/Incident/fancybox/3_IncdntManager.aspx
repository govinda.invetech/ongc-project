﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="3_IncdntManager.aspx.cs" Inherits="ONGCUIProjects.Incident.fancybox._3_IncdntManager" %>
<script type="text/javascript">

    $("#lblIncdntNo").hide();
    $("#txtTargetDate").datepicker({dateFormat:'dd-mm-yy'});

    $("#txtFPRName").keypress(function () {
        var FPRNameSearch_options, FPRNameSearch_a;
        jQuery(function () {
            var options = {
                serviceUrl: '../services/auto.aspx',
                onSelect: FPRNameSearch_onAutocompleteSelect,
                deferRequestBy: 0, //miliseconds
                params: { type: 'incdnt_fpr', limit: '10' },
                noCache: true //set to true, to disable caching
            };
            FPRNameSearch_a = $("#txtFPRName").autocomplete(options);
        });
    });
    var sFPRCPFNo = "";
    var FPRNameSearch_onAutocompleteSelect = function (FPRNameSearch_value, FPRNameSearch_data) {
        $(".autocomplete").hide();
        if (FPRNameSearch_value == "Enter something else..") {
            $("#txtFPRName").val("");
            sFPRCPFNo = "";
        } else {
            sFPRCPFNo = FPRNameSearch_data;
        }
    }

    $("#txtFPRTeam").keypress(function () {
        var FPRTeamSearch_options, FPRTeamSearch_a;
        jQuery(function () {
            var options = {
                serviceUrl: '../services/auto.aspx',
                onSelect: FPRTeamSearch_onAutocompleteSelect,
                deferRequestBy: 0, //miliseconds
                params: { type: 'emp_list', limit: '10' },
                noCache: true //set to true, to disable caching
            };
            FPRTeamSearch_a = $("#txtFPRTeam").autocomplete(options);
        });
    });

    var FPRTeamSearch_onAutocompleteSelect = function (FPRTeamSearch_value, FPRTeamSearch_data) {
        $(".autocomplete").hide();
        if (FPRTeamSearch_value == "Enter something else..") {
            $("#txtFPRTeam").val("");
            sFPRTeamCPFNo = "";
        } else if (FPRTeamSearch_value != "Enter something else..") {
            var MemberExist = "";
            if (FPRTeamSearch_data == sFPRCPFNo) {
                alert("Member already selected as FPR");
            } else {
                $.each($(".RemoveMember"), function (index, value) {
                    if ($(this).attr("MemberCPF") == FPRTeamSearch_data) {
                        MemberExist = "TRUE";
                        return;
                    }
                });
                if (MemberExist == "TRUE") {
                    alert("Member already exist!");
                } else {
                    var sFPRTeamMemCPFNo = FPRTeamSearch_data;
                    var sFPRTeamMenName = FPRTeamSearch_value;
                    var sMemberDetails = "";
                    sMemberDetails = "<div class=\"div-fullwidth TeamMembers\" style=\"border-bottom:1px solid #665A5A;\">";
                    sMemberDetails = sMemberDetails + "<p id=\"TeamMemberCPF\" class=\"content\" style=\"width:20%\">" + sFPRTeamMemCPFNo + "</p>";
                    sMemberDetails = sMemberDetails + "<p id=\"TeamMemberName\" class=\"content\">" + sFPRTeamMenName + "</p>";
                    sMemberDetails = sMemberDetails + "<a class=\"RemoveMember\" title=\"Remove Member\" MemberCPF=\"" + sFPRTeamMemCPFNo + "\" href=\"javascript:void(0);\" style=\"position: relative; float:right; margin-top:6px; margin-right: 5px;\"><img src=\"../../Images/cy_delete.png\" height=\"15px\" width=\"15px\" /></a>";
                    sMemberDetails = sMemberDetails + "</div>";
                    $("#div_team_members").append(sMemberDetails);
                }
            }
            $("#txtFPRTeam").val("");
            sFPRTeamCPFNo = "";
        }
    }

    $(".RemoveMember").live("click", function () {
        if (confirm("Are you sure to remove?")) {
            $(this).parent('div').remove();
        }
    });

    $("#cmdSubmit").click(function () {
        var sFormComplete = "TRUE";
        var sIncdntNo = $("#lblIncdntNo").text();
        var sIncdntDesc = $("#txtIncdntDesc").text().trim();

        var sActionToBeTaken = "";
        if ($("#txtIncMgrActToBeTaken").val().trim() == "") {
            $("#txtIncMgrActToBeTaken").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sActionToBeTaken = $("#txtIncMgrActToBeTaken").val().trim();
        }

        //var sComments = $("#txtIncMgrActToBeTaken").val().trim();
        var sComments = $("#txtIncMgrRem").val().trim() ;
        /*if ($("#txtIncMgrRem").val().trim() == "") {
            $("#txtIncMgrRem").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sComments = $("#txtIncMgrRem").val().trim();
        }*/



        //var sComments = $("#txtIncMgrRem").val().trim();
        var sTargetDate = "";
        if ($("#txtTargetDate").val().trim() == "") {
            $("#txtTargetDate").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sTargetDate = $("#txtTargetDate").val().trim();
        }

        //var sTargetDate = $("#txtTargetDate").val().trim();
        var sFPR_CPFNo = sFPRCPFNo;

        var sFPR_Name = "";
        if ($("#txtFPRName").val().trim() == "") {
            $("#txtFPRName").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sFPR_Name = $("#txtFPRName").val().trim();
        }


        //var sFPR_Name = $("#txtFPRName").val().trim();
        var sFPRTeamMembers = "";
        $.each($(".TeamMembers"), function (index, value) {
            sFPRTeamMembers = sFPRTeamMembers + $(this).children("p#TeamMemberCPF").text() + "~" + $(this).children("p#TeamMemberName").text() + "`";
        });
        sFPRTeamMembers = sFPRTeamMembers.substring(0, sFPRTeamMembers.length - 1);


        if (sFormComplete == "FALSE") {
            alert("Please fill all fields...!");
        }
        else {

            $("#response").text("Saving.....");
            $.ajax({
                type: "POST",
                url: "service/3_ManagerSave.aspx",
                data: "INCIDENT_NMBR=" + sIncdntNo + "&INCDNT_DESC=" + sIncdntDesc + "&ACTN_TAKN=" + sActionToBeTaken + "&RMRKS=" + sComments + "&ACPTNC_NM=" + sFPR_Name + "&ACPTNC_CPF_NMBR=" + sFPR_CPFNo + "&ACTN_TRGT_DT=" + sTargetDate + "&FPRTeamMembers=" + sFPRTeamMembers,
                success: function (msg) {
                    var msg_arr = msg.split("~");
                    if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                        $("#response").text(msg_arr[2]);
                        $("#cmdSubmit").hide();
                    } else {
                        alert(msg_arr[2]);
                    }
                }
            });
        }

        $(".txtvalidation").focusin(function () {
            if ($(this).css("border-color", "red")) {
                $(this).css("border-color", "");
            }
        });
    });
</script>
<form id="form1" runat="server">
<asp:label id="lblIncdntNo" runat="server" text="Label"></asp:label>
    <div style="position:relative;float:left; width:600px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">Assign Task to FPR</h1>
        <div class="fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content">Incident Description</p>
                <asp:TextBox ID="txtIncdntDesc" runat="server" TextMode="MultiLine" 
                    style="min-width: 98%; max-width: 98%; min-height:40px; max-height:40px;" 
                    Enabled="False"></asp:TextBox>
            </div>
            <div class="div-halfwidth">
                <p class="content">Reviewer Remark</p>
                <asp:TextBox ID="txtIncdntRevRem" runat="server" TextMode="MultiLine" 
                    style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;" 
                    Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content">Action to be Taken</p>
                <asp:TextBox class="txtvalidation" ID="txtIncMgrActToBeTaken" runat="server" TextMode="MultiLine" style="min-width: 98%; max-width: 98%; min-height:40px; max-height:40px;"></asp:TextBox>
            </div>
            <div class="div-halfwidth">
                <p class="content">Comments</p>
                <asp:TextBox class="txtvalidation" ID="txtIncMgrRem" runat="server" TextMode="MultiLine" style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;"></asp:TextBox>
            </div>
        </div>

        <div class="div-fullwidth marginbottom">
        <div class="div-fullwidth" style="width:auto; margin-right:9px;">
            <p class="content">Target Date</p>
            <asp:TextBox class="CheckValidation txtvalidation" ID="txtTargetDate" runat="server"></asp:TextBox>
        </div>
        <div class="div-fullwidth" style="width:auto;">
            <p class="content">First Person Responsible</p>
            <asp:TextBox class="CheckValidation txtvalidation" ID="txtFPRName" runat="server"></asp:TextBox>
        </div>
        </div>
        <div class="div-fullwidth" style="width:auto;">
            <p class="content">Select Employee's for FPR Team</p>
            <asp:TextBox class="CheckValidation" ID="txtFPRTeam" runat="server" style="width:353px; float:right;"></asp:TextBox>
        </div>
        <div id="div_team_members" class="div-fullwidth">
        
        </div>

        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px; margin-top:10px;"></h1>
        <div class="div-fullwidth">
            <p id="response" class="content" style="width: auto; color:Red;"></p>
            <input type="button" id="cmdReset" class="g-button g-button-red" value="Close" onclick="window.location=self.location; $.fancybox.close();" style="float:right;" />
            <input type="button" id="cmdSubmit" class="g-button g-button-submit" value="Save" style="margin-right:10px; margin-bottom:0px"/>
        </div>
    </div>
</form>