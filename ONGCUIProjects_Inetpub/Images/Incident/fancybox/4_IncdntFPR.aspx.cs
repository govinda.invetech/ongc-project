﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident.fancybox
{
    public partial class _4_IncdntFPR : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncdntID = Request.QueryString["IncdID"].ToString();

            string sStr = "SELECT [O_INCDNT_DESC] FROM [CY_INC_2_REV_DTL] WHERE [O_INCIDENT_NMBR] = '" + sIncdntID + "'";
            DataSet dsReviewedIncd = My.ExecuteSELECTQuery(sStr);

            lblIncdntNo.Text = sIncdntID;
            txtIncdntDesc.Text = dsReviewedIncd.Tables[0].Rows[0][0].ToString();
            txtIncdntDesc.ReadOnly = true;

        }
    }
}