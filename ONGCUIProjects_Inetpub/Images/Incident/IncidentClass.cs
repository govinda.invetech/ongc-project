﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace ONGCUIProjects.Incident
{
    public class IncidentClass
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        #region Variables

        private string _RptName = "";
        private string _RptDesig = "";
        private string _RptCPF = "";
        private string _RptDesc = "";
        private DateTime _RptDate;
        private string _RptTime = "";
        private string _RptLocation = "";
        private string _StatusLevel = "";
        private string _RptArea = "";
        private string _RptDept = "";
        private string _ApparentCause = "";
        private string _ImmediateRemedial = "";
        private string _RptIncType = "";
        private string _RptProperty = "";
        private string _RptProcess = "";
        private string _RptEnvironment = "";
        private string _RptPersonInjury = "";
        private string _RptRushToHospital = "";
        private string _RevIncCategory = "";
        private string _RevIncType = "";
        private string _RevName = "";
        private string _RevCPF = "";
        private string _RevRootCause = "";
        private string _RevRecmnd = "";
        private string _RevPrecaution = "";
        private string _IncMgrName = "";
        private string _IncMgrCPF = "";
        private string _RevRemark = "";
        private string _IncMgrActnToBeTaken = "";
        private string _IncMgrRemark = "";
        private string _IncMgrFwdName = "";
        private string _IncMgrFwdCPF = "";
        private string _IncMgrFwdDesig = "";
        private DateTime _IncTrgtDate;
        private string _IncMgrStatus = "";
        private string _IncFPRRemark = "";
        private string _IncLevel1Comment = "";
        private string _IncLevel1Decision = "";
        private string _IncLevel2FwdName = "";
        private string _IncLevel2FwdCPF = "";
        private List<Incmgrlistclass> _Incmgrlist = new List<Incmgrlistclass>();

        private List<string> _Incfprlist = new List<string>();

        private List<inclevel1> _Inclvl1 = new List<inclevel1>();

        private List<inclevel2> _Inclvl2 = new List<inclevel2>();

        #endregion

        public IncidentClass(string IncdntID)
        {

            #region Reporter Data
            string sCY_INC_1_DETAILS = "SELECT [O_ENG_NM],[O_ENG_DSG],[O_CPF_NMBR],[O_INCDNT_DESCRPTN],[O_INCDNT_DT],[O_INCDNT_TIME],[O_INCDNT_LCTN],[CY_STATUS_LEVEL],[CY_O_AREA],[CY_O_DEPT],[CY_O_APP_ACC],[CY_O_IMD_REM_ACTION],[CY_TYPE_OF_INCIDENT],[CY_PROPERTY],[CY_PROCESS],[CY_ENVIRONMENT],[CY_PERSONAL_INJURY],[O_RUSH_EMRGNCY] FROM [CY_INC_1_DETAILS] WHERE [O_INDX_NMBR] = '" + IncdntID + "'";
            DataSet dsCY_INC_1_DETAILS = My.ExecuteSELECTQuery(sCY_INC_1_DETAILS);
            if (dsCY_INC_1_DETAILS.Tables[0].Rows.Count != 0)
            {
                _RptName = dsCY_INC_1_DETAILS.Tables[0].Rows[0][0].ToString();
                _RptDesig = dsCY_INC_1_DETAILS.Tables[0].Rows[0][1].ToString();
                _RptCPF = dsCY_INC_1_DETAILS.Tables[0].Rows[0][2].ToString();
                _RptDesc = dsCY_INC_1_DETAILS.Tables[0].Rows[0][3].ToString();
                _RptDate = Convert.ToDateTime(dsCY_INC_1_DETAILS.Tables[0].Rows[0][4]);
                _RptTime = dsCY_INC_1_DETAILS.Tables[0].Rows[0][5].ToString();
                _RptLocation = dsCY_INC_1_DETAILS.Tables[0].Rows[0][6].ToString();
                _StatusLevel = dsCY_INC_1_DETAILS.Tables[0].Rows[0][7].ToString();
                _RptArea = dsCY_INC_1_DETAILS.Tables[0].Rows[0][8].ToString();
                _RptDept = dsCY_INC_1_DETAILS.Tables[0].Rows[0][9].ToString();
                _ApparentCause = dsCY_INC_1_DETAILS.Tables[0].Rows[0][10].ToString();
                _ImmediateRemedial = dsCY_INC_1_DETAILS.Tables[0].Rows[0][11].ToString();
                _RptIncType = dsCY_INC_1_DETAILS.Tables[0].Rows[0][12].ToString();
                _RptProperty = dsCY_INC_1_DETAILS.Tables[0].Rows[0][13].ToString();
                _RptProcess = dsCY_INC_1_DETAILS.Tables[0].Rows[0][14].ToString();
                _RptEnvironment = dsCY_INC_1_DETAILS.Tables[0].Rows[0][15].ToString();
                _RptPersonInjury = dsCY_INC_1_DETAILS.Tables[0].Rows[0][16].ToString();
                _RptRushToHospital = dsCY_INC_1_DETAILS.Tables[0].Rows[0][17].ToString();
            }
            #endregion

            #region Reviewer Data
            string sCY_INC_2_REV_DTL = "SELECT [CY_INCIDENT_CATEGORY],[O_REV_INCDNT_CTGRY],[O_REV_NM],[O_REV_CPF_NMBR],[O_REV_ROOT_CAUSE],[O_REV_RECMND],[O_REV_PRECTN_MEASR],[O_REV_ACTION_BY_NM],[O_REV_ACTION_BY_CPF_NMBR],[O_REV_RMRKS] FROM [CY_INC_2_REV_DTL] WHERE [O_INCIDENT_NMBR] = '" + IncdntID + "'";
            DataSet dsCY_INC_2_REV_DTL = My.ExecuteSELECTQuery(sCY_INC_2_REV_DTL);
            if (dsCY_INC_2_REV_DTL.Tables[0].Rows.Count != 0)
            {
                _RevIncCategory = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][0].ToString();
                _RevIncType = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][1].ToString();
                _RevName = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][2].ToString();
                _RevCPF = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][3].ToString();
                _RevRootCause = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][4].ToString();
                _RevRecmnd = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][5].ToString();
                _RevPrecaution = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][6].ToString();
                _IncMgrName = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][7].ToString();
                _IncMgrCPF = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][8].ToString();
                _RevRemark = dsCY_INC_2_REV_DTL.Tables[0].Rows[0][9].ToString();
            }
            #endregion

            #region Incident Manager Data
            string sCY_INC_3_ACTION_DTL = "SELECT TOP 1 [O_ACTN_TAKN], [O_RMRKS], [O_ACPTNC_NM], [O_ACPTNC_CPF_NMBR], [O_ACTN_TRGT_DT], [CY_ACTN_STATUS] FROM [CY_INC_3_ACTION_DTL] WHERE [O_INCIDENT_NMBR] = '" + IncdntID + "' ORDER BY [O_SYS_DT_TM]";
            DataSet dsCY_INC_3_ACTION_DTL = My.ExecuteSELECTQuery(sCY_INC_3_ACTION_DTL);
            if (dsCY_INC_3_ACTION_DTL.Tables[0].Rows.Count != 0)
            {

                _IncMgrActnToBeTaken = dsCY_INC_3_ACTION_DTL.Tables[0].Rows[0][0].ToString();
                _IncMgrRemark = dsCY_INC_3_ACTION_DTL.Tables[0].Rows[0][1].ToString();
                _IncMgrFwdName = dsCY_INC_3_ACTION_DTL.Tables[0].Rows[0][2].ToString();
                _IncMgrFwdCPF = dsCY_INC_3_ACTION_DTL.Tables[0].Rows[0][3].ToString();
                _IncMgrFwdDesig = My.getDesignationFromCPFNo(_IncMgrFwdCPF);
                _IncTrgtDate = Convert.ToDateTime(dsCY_INC_3_ACTION_DTL.Tables[0].Rows[0][4]);
                _IncMgrStatus = dsCY_INC_3_ACTION_DTL.Tables[0].Rows[0][5].ToString();

            }
            #endregion

            #region Incident Manager Data List
            string Incmgrquery = "SELECT [O_ACTN_TAKN], [O_RMRKS], [O_ACPTNC_NM], [O_ACPTNC_CPF_NMBR], [O_ACTN_TRGT_DT], [CY_ACTN_STATUS] FROM [CY_INC_3_ACTION_DTL] WHERE [O_INCIDENT_NMBR] = '" + IncdntID + "' ORDER BY [O_SYS_DT_TM] ";
            DataSet dsCY_INC_3_ACTION_DTL1 = My.ExecuteSELECTQuery(Incmgrquery);
            if (dsCY_INC_3_ACTION_DTL1.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < dsCY_INC_3_ACTION_DTL1.Tables[0].Rows.Count; i++)
                {
                    Incmgrlistclass ob = new Incmgrlistclass();
                    ob.IncMgrActnToBeTaken = dsCY_INC_3_ACTION_DTL1.Tables[0].Rows[i][0].ToString();
                    ob.IncMgrRemark = dsCY_INC_3_ACTION_DTL1.Tables[0].Rows[i][1].ToString();
                    ob.IncMgrFwdName = dsCY_INC_3_ACTION_DTL1.Tables[0].Rows[i][2].ToString();
                    ob.IncMgrFwdCPF = dsCY_INC_3_ACTION_DTL1.Tables[0].Rows[i][3].ToString();
                    ob.IncTrgtDate = Convert.ToDateTime(dsCY_INC_3_ACTION_DTL1.Tables[0].Rows[i][4]);
                    ob.IncMgrStatus = dsCY_INC_3_ACTION_DTL1.Tables[0].Rows[i][5].ToString();
                    _Incmgrlist.Add(ob);
                }
            }
            #endregion

            #region FPR Data
            string sCY_INC_4_ACTN_EXCTR_DTL = "SELECT TOP 1 [O_RMRKS] FROM [CY_INC_4_ACTN_EXCTR_DTL] WHERE [O_INCIDENT_NMBR] = '" + IncdntID + "' ORDER BY [O_SYS_DT_TM]";
            DataSet dsCY_INC_4_ACTN_EXCTR_DTL = My.ExecuteSELECTQuery(sCY_INC_4_ACTN_EXCTR_DTL);
            if (dsCY_INC_4_ACTN_EXCTR_DTL.Tables[0].Rows.Count != 0)
            {
                _IncFPRRemark = dsCY_INC_4_ACTN_EXCTR_DTL.Tables[0].Rows[0][0].ToString();
            }
            #endregion

            #region FPR Data list
            string sCY_INC_4_ACTN_EXCTR_DTL1 = "SELECT [O_RMRKS] FROM [CY_INC_4_ACTN_EXCTR_DTL] WHERE [O_INCIDENT_NMBR] = '" + IncdntID + "' ORDER BY [O_SYS_DT_TM]";
            DataSet dsCY_INC_4_ACTN_EXCTR_DTL1 = My.ExecuteSELECTQuery(sCY_INC_4_ACTN_EXCTR_DTL1);
            if (dsCY_INC_4_ACTN_EXCTR_DTL1.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < dsCY_INC_4_ACTN_EXCTR_DTL1.Tables[0].Rows.Count; i++)
                {
                    _Incfprlist.Add(dsCY_INC_4_ACTN_EXCTR_DTL1.Tables[0].Rows[i][0].ToString());
                }

            }
            #endregion

            #region Acceptance Level 1 Data
            string sCY_INC_5_ACPTN_LVL1_DTL = "SELECT TOP 1 [O_SELF_CMNT], [O_INC_DECSN], [O_ACPTN2_NM], [O_ACPTN2_CPF_NMBR] FROM [CY_INC_5_ACPTN_LVL1_DTL] WHERE [O_INCDNT_NMBR] = '" + IncdntID + "' ORDER BY [O_SYS_DT_TM] DESC";
            DataSet dsCY_INC_5_ACPTN_LVL1_DTL = My.ExecuteSELECTQuery(sCY_INC_5_ACPTN_LVL1_DTL);
            if (dsCY_INC_5_ACPTN_LVL1_DTL.Tables[0].Rows.Count != 0)
            {
                _IncLevel1Comment = dsCY_INC_5_ACPTN_LVL1_DTL.Tables[0].Rows[0][0].ToString();
                _IncLevel1Decision = dsCY_INC_5_ACPTN_LVL1_DTL.Tables[0].Rows[0][1].ToString();
                _IncLevel2FwdName = dsCY_INC_5_ACPTN_LVL1_DTL.Tables[0].Rows[0][2].ToString();
                _IncLevel2FwdCPF = dsCY_INC_5_ACPTN_LVL1_DTL.Tables[0].Rows[0][3].ToString();
            }
            #endregion

            #region Acceptance Level 1 Data List
            string sCY_INC_5_ACPTN_LVL1_DTL1 = "SELECT [O_SELF_CMNT], [O_INC_DECSN], [O_ACPTN2_NM], [O_ACPTN2_CPF_NMBR] FROM [CY_INC_5_ACPTN_LVL1_DTL] WHERE [O_INCDNT_NMBR] = '" + IncdntID + "' ORDER BY [O_SYS_DT_TM]";
            DataSet dsCY_INC_5_ACPTN_LVL1_DTL1 = My.ExecuteSELECTQuery(sCY_INC_5_ACPTN_LVL1_DTL1);
            if (dsCY_INC_5_ACPTN_LVL1_DTL1.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < dsCY_INC_5_ACPTN_LVL1_DTL1.Tables[0].Rows.Count; i++)
                {
                    inclevel1 ob = new inclevel1();
                    ob.IncLevel1Comment = dsCY_INC_5_ACPTN_LVL1_DTL1.Tables[0].Rows[i][0].ToString();
                    ob.IncLevel1Decision = dsCY_INC_5_ACPTN_LVL1_DTL1.Tables[0].Rows[i][1].ToString();
                    _Inclvl1.Add(ob);
                }

            }
            #endregion

            #region Acceptance Level 2 Data
            #endregion

            #region Acceptance Level 2 Data List
            string sCY_INC_5_ACPTN_LVL1_DTL2 = "SELECT [O_SELF_CMNT], [O_INC_DECSN] FROM [CY_INC_6_ACPTN_LVL2_DTL] WHERE [O_INCDNT_NMBR] = '" + IncdntID + "' ORDER BY [O_SYS_DT_TM]";
            DataSet dsCY_INC_6_ACPTN_LVL1_DTL2 = My.ExecuteSELECTQuery(sCY_INC_5_ACPTN_LVL1_DTL2);
            if (dsCY_INC_6_ACPTN_LVL1_DTL2.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < dsCY_INC_6_ACPTN_LVL1_DTL2.Tables[0].Rows.Count; i++)
                {
                    inclevel2 ob = new inclevel2();
                    ob.IncLevel1Comment = dsCY_INC_6_ACPTN_LVL1_DTL2.Tables[0].Rows[i][0].ToString();
                    ob.IncLevel1Decision = dsCY_INC_6_ACPTN_LVL1_DTL2.Tables[0].Rows[i][1].ToString();
                    _Inclvl2.Add(ob);
                }

            }
            #endregion

        }
        #region SetProperty

        public string RptName { get { return _RptName; } set { _RptName = value; } }
        public string RptDesig { get { return _RptDesig; } set { _RptDesig = value; } }
        public string RptCPF { get { return _RptCPF; } set { _RptCPF = value; } }
        public string RptDesc { get { return _RptDesc; } set { _RptDesc = value; } }
        public DateTime RptDate { get { return _RptDate; } set { _RptDate = value; } }
        public string RptTime { get { return _RptTime; } set { _RptTime = value; } }
        public string RptLocation { get { return _RptLocation; } set { _RptLocation = value; } }
        public string StatusLevel { get { return _StatusLevel; } set { _StatusLevel = value; } }
        public string RptArea { get { return _RptArea; } set { _RptArea = value; } }
        public string RptDept { get { return _RptDept; } set { _RptDept = value; } }
        public string ApparentCause { get { return _ApparentCause; } set { _ApparentCause = value; } }
        public string ImmediateRemedial { get { return _ImmediateRemedial; } set { _ImmediateRemedial = value; } }
        public string RptIncType { get { return _RptIncType; } set { _RptIncType = value; } }
        public string RptProperty { get { return _RptProperty; } set { _RptProperty = value; } }
        public string RptProcess { get { return _RptProcess; } set { _RptProcess = value; } }
        public string RptEnvironment { get { return _RptEnvironment; } set { _RptEnvironment = value; } }
        public string RptPersonInjury { get { return _RptPersonInjury; } set { _RptPersonInjury = value; } }
        public string RptRushToHospital { get { return _RptRushToHospital; } set { _RptRushToHospital = value; } }
        public string RevIncCategory { get { return _RevIncCategory; } set { _RevIncCategory = value; } }
        public string RevIncType { get { return _RevIncType; } set { _RevIncType = value; } }
        public string RevName { get { return _RevName; } set { _RevName = value; } }
        public string RevCPF { get { return _RevCPF; } set { _RevCPF = value; } }
        public string RevRootCause { get { return _RevRootCause; } set { _RevRootCause = value; } }
        public string RevRecmnd { get { return _RevRecmnd; } set { _RevRecmnd = value; } }
        public string RevPrecaution { get { return _RevPrecaution; } set { _RevPrecaution = value; } }
        public string IncMgrName { get { return _IncMgrName; } set { _IncMgrName = value; } }
        public string IncMgrCPF { get { return _IncMgrCPF; } set { _IncMgrCPF = value; } }
        public string RevRemark { get { return _RevRemark; } set { _RevRemark = value; } }
        public string IncMgrActnToBeTaken { get { return _IncMgrActnToBeTaken; } set { _IncMgrActnToBeTaken = value; } }
        public string IncMgrRemark { get { return _IncMgrRemark; } set { _IncMgrRemark = value; } }
        public string IncMgrFwdName { get { return _IncMgrFwdName; } set { _IncMgrFwdName = value; } }
        public string IncMgrFwdCPF { get { return _IncMgrFwdCPF; } set { _IncMgrFwdCPF = value; } }
        public string IncMgrFwdDesig { get { return _IncMgrFwdDesig; } set { _IncMgrFwdDesig = value; } }
        public DateTime IncTrgtDate { get { return _IncTrgtDate; } set { _IncTrgtDate = value; } }
        public string IncMgrStatus { get { return _IncMgrStatus; } set { _IncMgrStatus = value; } }
        public string IncFPRRemark { get { return _IncFPRRemark; } set { _IncFPRRemark = value; } }
        public string IncLevel1Comment { get { return _IncLevel1Comment; } set { _IncLevel1Comment = value; } }
        public string IncLevel1Decision { get { return _IncLevel1Decision; } set { _IncLevel1Decision = value; } }
        public string IncLevel2FwdName { get { return _IncLevel2FwdName; } set { _IncLevel2FwdName = value; } }
        public string IncLevel2FwdCPF { get { return _IncLevel2FwdCPF; } set { _IncLevel2FwdCPF = value; } }
        public List<Incmgrlistclass> Incmgrlist
        {
            get { return _Incmgrlist; }
            set { _Incmgrlist = value; }
        }
        public List<string> Incfprlist
        {
            get { return _Incfprlist; }
            set { _Incfprlist = value; }
        }
        public List<inclevel1> Inclvl1
        {
            get { return _Inclvl1; }
            set { _Inclvl1 = value; }
        }
        public List<inclevel2> Inclvl2
        {
            get { return _Inclvl2; }
            set { _Inclvl2 = value; }
        }
        #endregion
    }
    public class Incmgrlistclass
    {
        private string _IncMgrActnToBeTaken = "";
        private string _IncMgrRemark = "";
        private string _IncMgrFwdName = "";
        private string _IncMgrFwdCPF = "";
        private DateTime _IncTrgtDate;
        private string _IncMgrStatus = "";


        public string IncMgrActnToBeTaken { get { return _IncMgrActnToBeTaken; } set { _IncMgrActnToBeTaken = value; } }
        public string IncMgrRemark { get { return _IncMgrRemark; } set { _IncMgrRemark = value; } }
        public string IncMgrFwdName { get { return _IncMgrFwdName; } set { _IncMgrFwdName = value; } }
        public string IncMgrFwdCPF { get { return _IncMgrFwdCPF; } set { _IncMgrFwdCPF = value; } }
        public DateTime IncTrgtDate { get { return _IncTrgtDate; } set { _IncTrgtDate = value; } }
        public string IncMgrStatus { get { return _IncMgrStatus; } set { _IncMgrStatus = value; } }
    }

    public class inclevel1
    {
        private string _IncLevel1Comment = "";
        private string _IncLevel1Decision = "";
        private string _IncLevel2FwdName = "";
        private string _IncLevel2FwdCPF = "";


        public string IncLevel1Comment { get { return _IncLevel1Comment; } set { _IncLevel1Comment = value; } }
        public string IncLevel1Decision { get { return _IncLevel1Decision; } set { _IncLevel1Decision = value; } }
        public string IncLevel2FwdName { get { return _IncLevel2FwdName; } set { _IncLevel2FwdName = value; } }
        public string IncLevel2FwdCPF { get { return _IncLevel2FwdCPF; } set { _IncLevel2FwdCPF = value; } }
    }

    public class inclevel2
    {
        private string _IncLevel1Comment = "";
        private string _IncLevel1Decision = "";
        private string _IncLevel2FwdName = "";
        private string _IncLevel2FwdCPF = "";


        public string IncLevel1Comment { get { return _IncLevel1Comment; } set { _IncLevel1Comment = value; } }
        public string IncLevel1Decision { get { return _IncLevel1Decision; } set { _IncLevel1Decision = value; } }
        public string IncLevel2FwdName { get { return _IncLevel2FwdName; } set { _IncLevel2FwdName = value; } }
        public string IncLevel2FwdCPF { get { return _IncLevel2FwdCPF; } set { _IncLevel2FwdCPF = value; } }
    }





}