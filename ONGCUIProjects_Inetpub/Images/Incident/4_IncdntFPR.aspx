﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="4_IncdntFPR.aspx.cs" Inherits="ONGCUIProjects.Incident._4_IncdntFPR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <link rel="stylesheet" href="../css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="../css/css3.css" media="screen" type="text/css" />
        <script type="text/javascript" language="javascript" src="../js/jquery-1.7.2.min_a39dcc03.js"></script>
        <script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.timepicker.js"></script>
        <script type="text/javascript" src="../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <link rel="stylesheet" href="../js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../css/jquery.gritter.css" />
        <script type="text/javascript" src="../js/jquery.gritter.js"></script>
        <script src="../js/jquery.autocomplete-min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/AutoCompleteStyle.css" type="text/css" media="screen" />
        <script type="text/javascript">
            $(document).ready(function () {
                $(".IncdDtlFancyBox").fancybox({});
                $(".FPRActionTaken").fancybox({ modal: true });
            });
        </script>
    </head>
    <body>
        <form id="form1" runat="server">
            <div class="div-fullwidth iframeMainDiv">
                <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                    <h1 class="heading" style="width: auto;">Incident & Near Miss Details</h1>
                    <a class="g-button g-button-red" style="float:right; margin-right:10px; margin-top:5px; text-decoration:none;" id="cmdClosedIncident" href="7_Closed_Incident.aspx?type=Executer" >CLOSED</a>
                </div>
                <div class="div-pagebottom"></div>
                <div runat="server" id="div_incident_table" style="position:relative; float:left; width: 100%; margin-bottom: 20px;">
                </div>
            </div>
        </form>
    </body>
</html>