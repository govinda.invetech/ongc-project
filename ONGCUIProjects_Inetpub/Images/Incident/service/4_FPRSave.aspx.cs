﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident.service
{
    public partial class _4_FPRSave : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sO_USR_CODE = Session["Login_Name"].ToString();
            string sO_INCIDENT_NMBR = Request.Form["IncdntID"];
            string sO_INCDNT_DESC = Request.Form["IncdntDesc"];
            string sO_RMRKS = Request.Form["FPRRemark"];

            try
            {
                
                string sInsertQuery1 = "INSERT INTO [CY_INC_4_ACTN_EXCTR_DTL] ([O_INCIDENT_NMBR], [O_INCDNT_DESC], [O_RMRKS], [O_SYS_DT_TM], [O_USR_CODE]) VALUES ";
                sInsertQuery1 = sInsertQuery1 + "('" + sO_INCIDENT_NMBR + "', '" + sO_INCDNT_DESC + "', '" + sO_RMRKS + "', getdate(), '" + sO_USR_CODE + "')";
                if (My.ExecuteSQLQuery(sInsertQuery1) == true)
                {
                    if (My.UpdateIncidentStatus(sO_INCIDENT_NMBR, 4) == true)
                    {
                        //if (My.UpdateActionStatus(sO_INCIDENT_NMBR,"DONE") == true)
                        //{
                            Response.Write("TRUE~SUCCESS~Task completed Successfully");
                        //}
                        //else
                        //{
                        //    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                        //}
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                    }
                }
                else
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *FPRTASSIGN");
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends
        }
    }
}