﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects.Incident.service
{
    public partial class _2_ReviewSave : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }

            string sIncidentNmbr = Request.Form["IncidentNmbr"].ToString();

            string sRevIncdntCategory = Request.Form["RevIncdntCategory"].ToString();
            string sRevIncdntType = Request.Form["RevIncdntType"].ToString();
            string sRevRootCause = Request.Form["RevRootCause"].ToString();
            string sRevRecmnd = Request.Form["RevRecmnd"].ToString();
            string sRevSafetyPrectn = Request.Form["RevSafetyPrectn"].ToString();
            string sRevActionByNm = Request.Form["RevActionByNm"].ToString();
            string sRevActionByCPF = Request.Form["RevActionByCPF"].ToString();
            string sRevRemark = Request.Form["RevRemark"].ToString();
            string sIncdntDesc = Request.Form["IncdntDesc"].ToString();

            string sRevName = Session["EmployeeName"].ToString();
            string sRevCPF = Session["Login_Name"].ToString();

            try
            {
                string sInsert = "INSERT INTO [CY_INC_2_REV_DTL]([O_INCIDENT_NMBR],[CY_INCIDENT_CATEGORY],[O_REV_INCDNT_CTGRY],[O_REV_NM],[O_REV_CPF_NMBR],[O_REV_ROOT_CAUSE],[O_REV_RECMND],[O_REV_PRECTN_MEASR],[O_REV_ACTION_BY_NM],[O_REV_ACTION_BY_CPF_NMBR],[O_REV_RMRKS],[O_SYS_DT_TM],[O_INCDNT_DESC])VALUES";
                sInsert = sInsert + "('" + sIncidentNmbr + "','" + sRevIncdntCategory + "','" + sRevIncdntType + "','" + sRevName + "','" + sRevCPF + "','" + sRevRootCause + "','" + sRevRecmnd + "','" + sRevSafetyPrectn + "','" + sRevActionByNm + "','" + sRevActionByCPF + "','" + sRevRemark + "', getdate(),'" + sIncdntDesc + "')";

                if (My.ExecuteSQLQuery(sInsert) == true)
                {
                    if (My.CheckAndInsertRelation(sIncidentNmbr, sRevActionByCPF, "INCDNTMGR", sRevCPF) == true)
                    {
                        if (My.UpdateIncidentStatus(sIncidentNmbr, 2) == true)
                        {
                            Response.Write("TRUE~SUCCESS~Report saved successfully...!");
                        }
                        else
                        {
                            Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ILM187");
                        }
                    }
                    else
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ILM187");
                    }
                }
                else //else case of execute insert query
                {
                    Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *ILM187");
                }
            }
            catch (Exception a)
            {
                Response.Write("FALSE~ERR~" + a.Message);
            } //  try catch ends

        }
    }
}