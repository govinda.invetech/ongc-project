﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace ONGCUIProjects.Incident.service
{
    public partial class DeleteIncident : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '../../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string sIncidentID = Request["IncidentID"];
                try
                {
                    string sDelete = "DELETE FROM [CY_INC_1_DETAILS] WHERE O_INDX_NMBR = "+sIncidentID+";";
                    sDelete += "DELETE FROM [CY_INC_1_INJURY_RELATION] WHERE INCIDENT_NO = " + sIncidentID + ";";
                    sDelete += "DELETE FROM [CY_INC_2_REV_DTL] WHERE O_INCIDENT_NMBR = " + sIncidentID + ";";
                    sDelete += "DELETE FROM [CY_INC_3_ACTION_DTL] WHERE O_INCIDENT_NMBR = " + sIncidentID + ";";
                    sDelete += "DELETE FROM [CY_INC_3_ACTN_MEMBR_TEAM_DTL] WHERE O_INCIDENT_NMBR = " + sIncidentID + ";";
                    sDelete += "DELETE FROM [CY_INC_4_ACTN_EXCTR_DTL] WHERE O_INCIDENT_NMBR = " + sIncidentID + ";";
                    sDelete += "DELETE FROM [CY_INC_5_ACPTN_LVL1_DTL] WHERE O_INCDNT_NMBR = " + sIncidentID + ";";
                    sDelete += "DELETE FROM [CY_INC_6_ACPTN_LVL2_DTL] WHERE O_INCDNT_NMBR = " + sIncidentID + ";";
                    sDelete += "DELETE FROM [CY_INC_EMP_RELATION] WHERE INCDNT_NO = " + sIncidentID + ";";
                    if (My.ExecuteSQLQuery(sDelete) == true)
                    {
                        Response.Write("TRUE~SUCCESS~Incident / Near Miss Deleted Successfully...!");
                    }
                    else //else case of execute update query
                    {
                        Response.Write("FALSE~ERR~MyApp oocured error ! ERR CODE : *INCDNTDEL1298");
                    }
                }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //  try catch ends
            }
       }
    }
}