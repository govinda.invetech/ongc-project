﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident
{
    public partial class _5_IncdntLevel_1 : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new MyApplication1();

        #region Variables
        string sIncidentTableData = "";
        string sNearMissTableData = "";
        int count = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            string sIncDetailsQuery = "SELECT [INCDNT_NO] FROM [CY_INC_EMP_RELATION] WHERE [EMP_CPF_NO] = '" + Session["Login_Name"] + "' AND [LEVEL] = 'LEVEL1' ORDER BY [TIMESTAMP] DESC";
            DataSet dsIncDetails = My.ExecuteSELECTQuery(sIncDetailsQuery);

            for (int i = 0; i < dsIncDetails.Tables[0].Rows.Count; i++)
            {
                string sIncdID = dsIncDetails.Tables[0].Rows[i][0].ToString();

                IncidentClass CurrentIncdnt = new IncidentClass(sIncdID);

                DateTime sIncdDate = CurrentIncdnt.RptDate;
                string sIncdTime = CurrentIncdnt.RptTime;
                string sIncdCategory = CurrentIncdnt.RevIncCategory;
                string sIncdType = CurrentIncdnt.RevIncType;
                string sIncdDesc = CurrentIncdnt.RptDesc;
                string sIncdStatus = CurrentIncdnt.StatusLevel;

                if (sIncdStatus == "7" || sIncdStatus == "10")
                {

                }
                else
                {
                    count = count + 1;
                    sIncidentTableData = sIncidentTableData + "<tr>";
                    sIncidentTableData = sIncidentTableData + "<td>" + sIncdDate.ToString("d MMM, yyyy") + "</td>";
                    sIncidentTableData = sIncidentTableData + "<td>" + sIncdTime + "</td>";
                    sIncidentTableData = sIncidentTableData + "<td>" + sIncdType + "</td>";
                    sIncidentTableData = sIncidentTableData + "<td>" + sIncdCategory + "</td>";
                    sIncidentTableData = sIncidentTableData + "<td>" + sIncdDesc + "</td>";
                    sIncidentTableData = sIncidentTableData + "<td>" + My.getStatusStringFromLevel(sIncdStatus) + "</td>";
                    sIncidentTableData = sIncidentTableData + "<td><a IncdID=\"" + sIncdID + "\" class=\"IncdDtlFancyBox\" href=\"fancybox/IncidentDetails.aspx?IncdID=" + sIncdID + "&level=5\">View Details</a></td>";
                    if (sIncdStatus == "5" || sIncdStatus == "11")
                    {
                        sIncidentTableData = sIncidentTableData + "<td><a IncdID=\"" + sIncdID + "\" class=\"FPRActionTaken\" href=\"fancybox/5_Accp_L1.aspx?IncdID=" + sIncdID + "\">Take Decision</a></td>";
                    }
                    sIncidentTableData = sIncidentTableData + "</tr>";
                }


            }

            if (count > 0)
            {
                string sTableHeading = "<tr><th>Date</th><th>Time</th><th>Type</th><th>Category</th><th>Description</th><th>Status</th><th colspan=\"2\"></th></tr>";
                div_incident_table.InnerHtml = "<table class=\"tftable\" border=\"1\">" + sTableHeading + sIncidentTableData + "</table>";
            }
            else
            {
                div_incident_table.InnerHtml = "<h2>You have not any Incident or Near Miss</h2>";
            }
            
        }
    }
}