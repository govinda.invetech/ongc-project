﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects.Incident
{
    public partial class _7_Closed_Incident : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        string sIncdentDetailsQuery;
        string CPF;
        int count = 0;
       // IncidentClass CurrentIncident = new IncidentClass(sIncdID);
        protected void Page_Load(object sender, EventArgs e)
         {
             if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = '../Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            string type=Request.QueryString["type"].ToString();

            #region Incident Details
            CPF=Session["Login_Name"].ToString();


switch (type)
	{
    case "Reporting":
            sIncdentDetailsQuery = "SELECT [O_INDX_NMBR],[CY_TYPE_OF_INCIDENT],[O_INCDNT_DT],[O_INCDNT_TIME],[O_INCDNT_DESCRPTN],[CY_STATUS_LEVEL]FROM [CY_INC_1_DETAILS] where [CY_ENTRY_BY]='" + Session["Login_Name"].ToString() + "' and ([CY_STATUS_LEVEL]='7' or [CY_STATUS_LEVEL]='10')";

        break;

        case "Review":
        //sIncdentDetailsQuery = "SELECT [O_INDX_NMBR],[CY_TYPE_OF_INCIDENT],[O_INCDNT_DT],[O_INCDNT_TIME],[O_INCDNT_DESCRPTN],[CY_STATUS_LEVEL]FROM [CY_INC_1_DETAILS] where [CY_ENTRY_BY]='" + Session["Login_Name"].ToString() + "' and ([CY_STATUS_LEVEL]='7' or [CY_STATUS_LEVEL]='10')";
        sIncdentDetailsQuery = "select [O_INCIDENT_NMBR] from [CY_INC_2_REV_DTL]  where [O_REV_CPF_NMBR]='" + Session["Login_Name"].ToString() + "'";
        break;

        case "Manager":
        sIncdentDetailsQuery = "SELECT [INCDNT_NO] FROM [CY_INC_EMP_RELATION] where [EMP_CPF_NO]='" + Session["Login_Name"].ToString() + "' and [LEVEL]='INCDNTMGR'";

        break;
        case "Executer":
        sIncdentDetailsQuery = "SELECT [INCDNT_NO] FROM [CY_INC_EMP_RELATION] where [EMP_CPF_NO]='" + Session["Login_Name"].ToString() + "' and [LEVEL]='FPR'";

        break;
        case "Level1":
        sIncdentDetailsQuery = "SELECT [INCDNT_NO] FROM [CY_INC_EMP_RELATION] where [EMP_CPF_NO]='" + Session["Login_Name"].ToString() + "' and [LEVEL]='LEVEL1'";

        break;
        case "Level2":
        sIncdentDetailsQuery = "SELECT [INCDNT_NO] FROM [CY_INC_EMP_RELATION] where [EMP_CPF_NO]='" + Session["Login_Name"].ToString() + "' and [LEVEL]='LEVEL2'";
        break;
	}
string level = "";

switch (type)
{
    case "Reporting":
        level = "1";

        break;

    case "Review":
        level = "2";
        break;

    case "Manager":
        level = "3";

        break;
    case "Executer":
        level = "4";

        break;
    case "Level1":
        level = "5";

        break;
    case "Level2":
        level = "6";
        break;
}




if (type == "Reporting")
{
    DataSet dsIncdentDetails = My.ExecuteSELECTQuery(sIncdentDetailsQuery);
    if (dsIncdentDetails.Tables[0].Rows.Count == 0)
    {
        divTableData.InnerHtml = "<h1 style=\"text-align:center;\">No Incident Reported yet</h1>";
    }
    else
    {
        string sIncdentTableData = "";
        for (int i = 0; i < dsIncdentDetails.Tables[0].Rows.Count; i++)
        {
            count = count + 1;
            string sIncdentID = dsIncdentDetails.Tables[0].Rows[i][0].ToString();
            string sType = dsIncdentDetails.Tables[0].Rows[i][1].ToString();
            string sIncdentDescription = dsIncdentDetails.Tables[0].Rows[i][4].ToString();
            DateTime sIncdentDate = System.Convert.ToDateTime(dsIncdentDetails.Tables[0].Rows[i][2]);
            string sIncdentTime = dsIncdentDetails.Tables[0].Rows[i][3].ToString();
            string sIncdentStatus = dsIncdentDetails.Tables[0].Rows[i][5].ToString();
            sIncdentTableData = sIncdentTableData + "<tr>";
            sIncdentTableData = sIncdentTableData + "<td>" + sType + "</td>";
            sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDate.ToString("d MMM, yyyy") + "</td>";
            sIncdentTableData = sIncdentTableData + "<td>" + sIncdentTime + "</td>";
            sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDescription + "</td>";
            sIncdentTableData = sIncdentTableData + "<td>" + My.getStatusStringFromLevel(sIncdentStatus) + "</td>";
            if (sIncdentStatus == "1")
            {

            }
            else
            {
                sIncdentTableData = sIncdentTableData + "<td><a IncdID=\"" + sIncdentID + "\" class=\"IncdDtlFancyBox\" href=\"fancybox/IncidentDetails.aspx?IncdID=" + sIncdentID + "&level="+level+"\">View Details</a></td>";
            }
            sIncdentTableData = sIncdentTableData + "</tr>";
        }
        if (count > 0)
        {
            string sTableHeading = "<tr><th>Type</th><th>Date</th><th>Time</th><th>Description</th><th colspan=\"2\">Status</th></tr>";
            divTableData.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading + sIncdentTableData + "</table>";
        }
        else
        {
            divTableData.InnerHtml = "<h1 style=\"text-align:center;\">No Incident Reported yet</h1>";
        }
    }
}
else
{
    DataSet dsIncdentDetails = My.ExecuteSELECTQuery(sIncdentDetailsQuery);
    if (dsIncdentDetails.Tables[0].Rows.Count == 0)
    {
        divTableData.InnerHtml = "<h1 style=\"text-align:center;\">No Incident Reported yet</h1>";
    }
    else
    {
        string sIncdentTableData = "";
        for (int i = 0; i < dsIncdentDetails.Tables[0].Rows.Count; i++)
        {
            IncidentClass CurrentIncident = new IncidentClass(dsIncdentDetails.Tables[0].Rows[i][0].ToString());
            string sIncdentID = dsIncdentDetails.Tables[0].Rows[i][0].ToString();
            string sType = CurrentIncident.RevIncType;
            string sIncdentDescription = CurrentIncident.RptDesc;
            DateTime sIncdentDate = System.Convert.ToDateTime(CurrentIncident.RptDate);
            string sIncdentTime = CurrentIncident.RptTime;
            string sIncdentStatus = CurrentIncident.StatusLevel;

            if (sIncdentStatus == "7" || sIncdentStatus == "10")
            {
                count = count + 1;


                sIncdentTableData = sIncdentTableData + "<tr>";
                sIncdentTableData = sIncdentTableData + "<td>" + sType + "</td>";
                sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDate.ToString("d MMM, yyyy") + "</td>";
                sIncdentTableData = sIncdentTableData + "<td>" + sIncdentTime + "</td>";
                sIncdentTableData = sIncdentTableData + "<td>" + sIncdentDescription + "</td>";
                sIncdentTableData = sIncdentTableData + "<td>" + My.getStatusStringFromLevel(sIncdentStatus) + "</td>";


                if (CurrentIncident.StatusLevel == "1")
                {

                }
                else
                {
                    sIncdentTableData = sIncdentTableData + "<td><a IncdID=\"" + dsIncdentDetails.Tables[0].Rows[i][0].ToString() + "\" class=\"IncdDtlFancyBox\" href=\"fancybox/IncidentDetails.aspx?IncdID=" + dsIncdentDetails.Tables[0].Rows[i][0].ToString() + "&level="+level+"\">View Details</a></td>";
                }
                sIncdentTableData = sIncdentTableData + "</tr>";

            }
            else
            {

            }

        }
        if (count > 0)
        {
            string sTableHeading = "<tr><th>Type</th><th>Date</th><th>Time</th><th>Description</th><th colspan=\"2\">Status</th></tr>";
            divTableData.InnerHtml = "<table class=\"IncTable\" border=\"1\">" + sTableHeading + sIncdentTableData + "</table>";
        }
        else
        {
            divTableData.InnerHtml = "<h1 style=\"text-align:center;\">No Incident Reported yet</h1>";
        }
    }
}
            #endregion
        }
    }
        }