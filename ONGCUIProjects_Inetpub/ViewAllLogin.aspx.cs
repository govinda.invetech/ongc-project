﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class ViewAllLogin : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            string sCPFNo = Request["CPF"].ToString();
            string sQuery = "SELECT [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' AND [USER_ID] = '" + sCPFNo + "' ORDER BY [TIMESTAMP] DESC";
            DataSet dsLoginList = My.ExecuteSELECTQuery(sQuery);
            heading_user_name.InnerHtml = My.GetEmployeeNameFromCPFNo(sCPFNo);
            if (dsLoginList.Tables[0].Rows.Count != 0)
            {
                string sOutput = "<table class='tftable'>";
                for (int i = 0;i< dsLoginList.Tables[0].Rows.Count; i++)
                {
                    DateTime dtTimestamp = System.Convert.ToDateTime(dsLoginList.Tables[0].Rows[i][0]);
                    sOutput += "<tr>";
                    sOutput += "<td>" + dtTimestamp.ToString("dd MMM, yyyy") + "</td>";
                    sOutput += "<td>" + dtTimestamp.ToString("HH:mm") + "</td>";
                    sOutput += "</tr>";
                }
                sOutput += "</table>";
                div_login_logs.InnerHtml = sOutput;
            }
        }
    }
}