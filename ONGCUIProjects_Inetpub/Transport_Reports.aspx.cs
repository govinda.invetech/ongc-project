using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;

namespace ONGCUIProjects
{
	/// <summary>
	/// Summary description for Transport_Reports.
	/// </summary>
	public partial class Transport_Reports : System.Web.UI.Page
	{
		#region Variables
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		string EString;
		string strEmp;
		ArrayList strEmployeeAry = new ArrayList();
		#endregion

		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				LoadSelections();
				BindBookingDdl();
				BindLocation();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Date Wise Check Validation
		protected void ChkDateWise_CheckedChanged(object sender, System.EventArgs e)
		{
			if(ChkDateWise.Checked)
			{
				cmbStart_DD.Enabled=true;
				cmbStart_DD.SelectedIndex=0;
				cmbStart_MM.Enabled=true;
				cmbStart_MM.SelectedIndex=0;
				cmbStart_YY.Enabled=true;
				cmbStart_YY.SelectedIndex=0;
				cmbEnd_DD.Enabled=true;
				cmbEnd_DD.SelectedIndex=0;
				cmbEnd_MM.Enabled=true;
				cmbEnd_MM.SelectedIndex=0;
				cmbEnd_YY.Enabled=true;
				cmbEnd_YY.SelectedIndex=0;

				ChkBkngType.Checked=false;
				ChkBkngType.Enabled=false;

				ChkLocation.Checked=false;
				ChkLocation.Enabled=false;

				chkEmp.Checked=false;
				chkEmp.Enabled=false;

				BtnGenerateReport.Enabled=true;
			}
			else
			{
				cmbStart_DD.Enabled=false;
				cmbStart_DD.SelectedIndex=0;
				cmbStart_MM.Enabled=false;
				cmbStart_MM.SelectedIndex=0;
				cmbStart_YY.Enabled=false;
				cmbStart_YY.SelectedIndex=0;
				cmbEnd_DD.Enabled=false;
				cmbEnd_DD.SelectedIndex=0;
				cmbEnd_MM.Enabled=false;
				cmbEnd_MM.SelectedIndex=0;
				cmbEnd_YY.Enabled=false;
				cmbEnd_YY.SelectedIndex=0;

				ChkBkngType.Checked=false;
				ChkBkngType.Enabled=true;

				ChkLocation.Checked=false;
				ChkLocation.Enabled=true;

				chkEmp.Checked=false;
				chkEmp.Enabled=true;

				BtnGenerateReport.Enabled=false;
			}			
		}
		#endregion

		#region Load Selection
		private void LoadSelections()
		{
			ChkDateWise.Checked=false;
			cmbStart_DD.Enabled=false;
			cmbStart_MM.Enabled=false;
			cmbStart_YY.Enabled=false;
			cmbEnd_DD.Enabled=false;
			cmbEnd_MM.Enabled=false;
			cmbEnd_YY.Enabled=false;           

			ChkBkngType.Checked=false;
			CmbBookingType.Enabled=false;
			FrmDD.Enabled=false;
			FrmMM.Enabled=false;
			FrmYY.Enabled=false;
			ToDD.Enabled=false;
			ToMM.Enabled=false;
			ToYY.Enabled=false;

			ChkLocation.Checked=false;
			cmbLocation.Enabled=false;
			FromDD.Enabled=false;
			FromMM.Enabled=false;
			FromYY.Enabled=false;
			ToDtDD.Enabled=false;
			ToDtMM.Enabled=false;
			ToDtYY.Enabled=false;

			chkEmp.Checked=false;
			txtEmp.Enabled=false;
			BtnSearch.Enabled=false;
			LstEmp.Enabled=false;
			
			BtnGenerateReport.Enabled=false;
		}
		#endregion

		#region Booking Type Check Validation
		protected void ChkBkngType_CheckedChanged(object sender, System.EventArgs e)
		{
			if(ChkBkngType.Checked)
			{
				ChkDateWise.Checked=false;
				ChkDateWise.Enabled=false;
				
				CmbBookingType.Enabled=true;
				CmbBookingType.SelectedIndex=0;
				FrmDD.Enabled=true;
				FrmDD.SelectedIndex=0;
				FrmMM.Enabled=true;
				FrmMM.SelectedIndex=0;
				FrmYY.Enabled=true;
				FrmYY.SelectedIndex=0;
				ToDD.Enabled=true;
				ToDD.SelectedIndex=0;
				ToMM.Enabled=true;
				ToMM.SelectedIndex=0;
				ToYY.Enabled=true;
				ToYY.SelectedIndex=0;

				ChkLocation.Checked=false;
				ChkLocation.Enabled=false;

				chkEmp.Checked=false;
				chkEmp.Enabled=false;
			
				BtnGenerateReport.Enabled=true;
			}
			else
			{
				ChkDateWise.Checked=false;
				ChkDateWise.Enabled=true;
				
				CmbBookingType.Enabled=false;
				CmbBookingType.SelectedIndex=0;
				FrmDD.Enabled=false;
				FrmDD.SelectedIndex=0;
				FrmMM.Enabled=false;
				FrmMM.SelectedIndex=0;
				FrmYY.Enabled=false;
				FrmYY.SelectedIndex=0;
				ToDD.Enabled=false;
				ToDD.SelectedIndex=0;
				ToMM.Enabled=false;
				ToMM.SelectedIndex=0;
				ToYY.Enabled=false;
				ToYY.SelectedIndex=0;

				ChkLocation.Checked=false;
				ChkLocation.Enabled=true;

				chkEmp.Checked=false;
				chkEmp.Enabled=true;
			
				BtnGenerateReport.Enabled=false;				
			}
		}
		#endregion 

		#region Location Date Wise Check Validations
		protected void ChkLocation_CheckedChanged(object sender, System.EventArgs e)
		{
			if(ChkLocation.Checked)
			{
				ChkDateWise.Checked=false;
				ChkDateWise.Enabled=false;

				ChkBkngType.Checked=false;
				ChkBkngType.Enabled=false;

				cmbLocation.Enabled=true;
				cmbLocation.SelectedIndex=0;
				FromDD.Enabled=true;
				FromDD.SelectedIndex=0;				
				FromMM.Enabled=true;
				FromMM.SelectedIndex=0;
				FromYY.Enabled=true;
				FromYY.SelectedIndex=0;
				ToDtDD.Enabled=true;
				ToDtDD.SelectedIndex=0;
				ToDtMM.Enabled=true;
				ToDtMM.SelectedIndex=0;
				ToDtYY.Enabled=true;
				ToDtYY.SelectedIndex=0;

				chkEmp.Checked=false;
				chkEmp.Enabled=false;

				BtnGenerateReport.Enabled=true;
			}
			else
			{
				ChkDateWise.Checked=false;
				ChkDateWise.Enabled=true;

				ChkBkngType.Checked=false;
				ChkBkngType.Enabled=true;

				cmbLocation.Enabled=false;
				cmbLocation.SelectedIndex=0;
				FromDD.Enabled=false;
				FromDD.SelectedIndex=0;				
				FromMM.Enabled=false;
				FromMM.SelectedIndex=0;
				FromYY.Enabled=false;
				FromYY.SelectedIndex=0;
				ToDtDD.Enabled=false;
				ToDtDD.SelectedIndex=0;
				ToDtMM.Enabled=false;
				ToDtMM.SelectedIndex=0;
				ToDtYY.Enabled=false;
				ToDtYY.SelectedIndex=0;

				chkEmp.Checked=false;
				chkEmp.Enabled=true;

				BtnGenerateReport.Enabled=false;
			}
		}
		#endregion

		#region Employee Wise Check Validations
		protected void chkEmp_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkEmp.Checked)
			{
				txtEmp.Enabled=true;
				txtEmp.Text="";
				BtnSearch.Enabled=true;
				LstEmp.Enabled=true;
				LstEmp.Items.Clear();

				ChkDateWise.Checked=false;
				ChkDateWise.Enabled=false;

				ChkBkngType.Checked=false;
				ChkBkngType.Enabled=false;

				ChkLocation.Checked=false;
				ChkLocation.Enabled=false;

				BtnGenerateReport.Enabled=true;
			}
			else
			{
				txtEmp.Enabled=false;
				txtEmp.Text="";
				BtnSearch.Enabled=false;
				LstEmp.Enabled=false;
				LstEmp.Items.Clear();

				ChkDateWise.Checked=false;
				ChkDateWise.Enabled=true;

				ChkBkngType.Checked=false;
				ChkBkngType.Enabled=true;

				ChkLocation.Checked=false;
				ChkLocation.Enabled=true;

				BtnGenerateReport.Enabled=false;
			}
		}
		#endregion

		#region Bind BookingType Dropdown List
		public void BindBookingDdl()
		{
			try
			{
				DataSet dsBookingDtl=new DataSet();
				ONGCBusinessProjects.TransportBooking objbusFillBookingDdl	= new ONGCBusinessProjects.TransportBooking();
				dsBookingDtl=objbusFillBookingDdl.fillBookingDdl();
				CmbBookingType.DataSource=dsBookingDtl;
				CmbBookingType.DataMember=dsBookingDtl.Tables[0].ToString();
				CmbBookingType.DataValueField=dsBookingDtl.Tables[0].Columns["O_INDX_NMBR"].ToString();
				CmbBookingType.DataTextField=dsBookingDtl.Tables[0].Columns["O_TRNSP_NM"].ToString();
				CmbBookingType.DataBind();
				CmbBookingType.Items.Insert(0,"Select Booking Type");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Bind Location Dropdown List
		public void BindLocation()
		{
			try
			{
				DataSet dsLocation=new DataSet();
				ONGCBusinessProjects.TransportBooking objbusFillLocation		    = new ONGCBusinessProjects.TransportBooking();
				dsLocation=objbusFillLocation.fillLocationDdl();
				cmbLocation.DataSource=dsLocation;
				cmbLocation.DataMember=dsLocation.Tables[0].ToString();
				cmbLocation.DataValueField=dsLocation.Tables[0].Columns["O_INDX_NMBR"].ToString();
				cmbLocation.DataTextField=dsLocation.Tables[0].Columns["O_LCTN_NM"].ToString();
				cmbLocation.DataBind();
				cmbLocation.Items.Insert(0,"Select Location");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Generate Report
		protected void BtnGenerateReport_Click(object sender, System.EventArgs e)
		{
			// Date Wise generate Report
			if(ChkDateWise.Checked)
			{
				SqlConnection conn=new SqlConnection(strConn);
				conn.Open();
				string StrDateQuery ="select O_INDX_NMBR as [Sr. No], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No], O_APLCNT_DESG as [Applicant Designation], O_APLCNT_LCTN as [Applicant Location], O_APLCNT_EXT_NMBR as [Applicant Ext No], O_BKNG_START_DT_TM as [Booking Start Date], O_BKNG_END_DT_TM as [Booking End Date], O_BKNG_TYP as [Type Of Booking], O_RPRTNG_TM as [Vehicle Reporting Time], O_RPRT_TO_NM as [Report To], O_RPRT_TO_ADDR as [Reporting Person Address], O_RPRT_TO_CONT_NMBR1 as [Report To Contact Number1], O_RPRT_TO_CONT_NMBR2 as [Report To Contact Number2], O_NO_OF_PERSONS as [No Of Persons Travelling], O_BKNG_PRPS as [Purpose], O_RMRKS as [Comments], O_BKNG_APRVL_OFF_NM as [Approver Name], O_BKNG_APRVL_OFF_DESG as [Approver Designation], O_BKNG_APRVL_OFF_CPF_NMBR as [Approver CPF Number], O_SYS_DT_TM as [Booking Date] from O_TRNSPRT_BKNG_DTL Where Convert(varchar, Convert(datetime, O_BKNG_START_DT_TM, 102), 112) >= '" + cmbStart_YY.SelectedItem.Text+cmbStart_MM.SelectedItem.Text+cmbStart_DD.SelectedItem.Text + "' and Convert(varchar, Convert(datetime, O_BKNG_END_DT_TM, 102), 112) <= '" + cmbEnd_YY.SelectedItem.Text+cmbEnd_MM.SelectedItem.Text+cmbEnd_DD.SelectedItem.Text+ "'";
				SqlCommand DateWiseReport = new SqlCommand(StrDateQuery,conn);
				SqlDataReader sdr1= DateWiseReport.ExecuteReader();
				dgExcel.DataSource=sdr1;
				dgExcel.DataBind();
				conn.Close();
				StringWriter objStringWriter = new StringWriter();
				System.Web.UI.HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
				Response.ClearContent();
				Response.ClearHeaders();
				Response.ContentType = "application/vnd.ms-excel";
				Response.Charset = "";
				Response.AddHeader("Content-Disposition","attachment;filename=Transport_DateWise_Report.xls");
				this.EnableViewState = false;
				dgExcel.GridLines = GridLines.Both;
				dgExcel.HeaderStyle.Font.Bold=true;
				dgExcel.RenderControl(objHtmlTextWriter);
				Response.Write(objStringWriter.ToString());				
				Response.End();				
			}

			// Booking Type & Date Wise generate Report
			if(ChkBkngType.Checked)
			{
				SqlConnection conn=new SqlConnection(strConn);
				conn.Open();
				string StrDateQuery ="select O_INDX_NMBR as [Sr. No], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No], O_APLCNT_DESG as [Applicant Designation], O_APLCNT_LCTN as [Applicant Location], O_APLCNT_EXT_NMBR as [Applicant Ext No], O_BKNG_START_DT_TM as [Booking Start Date], O_BKNG_END_DT_TM as [Booking End Date], O_BKNG_TYP as [Type Of Booking], O_RPRTNG_TM as [Vehicle Reporting Time], O_RPRT_TO_NM as [Report To], O_RPRT_TO_ADDR as [Reporting Person Address], O_RPRT_TO_CONT_NMBR1 as [Report To Contact Number1], O_RPRT_TO_CONT_NMBR2 as [Report To Contact Number2], O_NO_OF_PERSONS as [No Of Persons Travelling], O_BKNG_PRPS as [Purpose], O_RMRKS as [Comments], O_BKNG_APRVL_OFF_NM as [Approver Name], O_BKNG_APRVL_OFF_DESG as [Approver Designation], O_BKNG_APRVL_OFF_CPF_NMBR as [Approver CPF Number], O_SYS_DT_TM as [Booking Date] from O_TRNSPRT_BKNG_DTL Where Convert(varchar, Convert(datetime, O_BKNG_START_DT_TM, 102), 112) >= '" + FrmYY.SelectedItem.Text+FrmMM.SelectedItem.Text+FrmDD.SelectedItem.Text + "' and Convert(varchar, Convert(datetime, O_BKNG_END_DT_TM, 102), 112) <= '" + ToYY.SelectedItem.Text+ToMM.SelectedItem.Text+ToDD.SelectedItem.Text+ "' and O_BKNG_TYP = '" + CmbBookingType.SelectedItem.Text + "'";
				SqlCommand DateWiseReport = new SqlCommand(StrDateQuery,conn);
				SqlDataReader sdr1= DateWiseReport.ExecuteReader();
				dgExcel.DataSource=sdr1;
				dgExcel.DataBind();
				conn.Close();
				StringWriter objStringWriter = new StringWriter();
				System.Web.UI.HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
				Response.ClearContent();
				Response.ClearHeaders();
				Response.ContentType = "application/vnd.ms-excel";
				Response.Charset = "";
				Response.AddHeader("Content-Disposition","attachment;filename=Transport_BookingTypeWise_Report.xls");
				this.EnableViewState = false;
				dgExcel.GridLines = GridLines.Both;
				dgExcel.HeaderStyle.Font.Bold=true;
				dgExcel.RenderControl(objHtmlTextWriter);
				Response.Write(objStringWriter.ToString());				
				Response.End();
			}

			// Location & date Wise Generate Report
			if(ChkLocation.Checked)
			{
				SqlConnection conn=new SqlConnection(strConn);
				conn.Open();
				string StrDateQuery ="select O_INDX_NMBR as [Sr. No], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No], O_APLCNT_DESG as [Applicant Designation], O_APLCNT_LCTN as [Applicant Location], O_APLCNT_EXT_NMBR as [Applicant Ext No], O_BKNG_START_DT_TM as [Booking Start Date], O_BKNG_END_DT_TM as [Booking End Date], O_BKNG_TYP as [Type Of Booking], O_RPRTNG_TM as [Vehicle Reporting Time], O_RPRT_TO_NM as [Report To], O_RPRT_TO_ADDR as [Reporting Person Address], O_RPRT_TO_CONT_NMBR1 as [Report To Contact Number1], O_RPRT_TO_CONT_NMBR2 as [Report To Contact Number2], O_NO_OF_PERSONS as [No Of Persons Travelling], O_BKNG_PRPS as [Purpose], O_RMRKS as [Comments], O_BKNG_APRVL_OFF_NM as [Approver Name], O_BKNG_APRVL_OFF_DESG as [Approver Designation], O_BKNG_APRVL_OFF_CPF_NMBR as [Approver CPF Number], O_SYS_DT_TM as [Booking Date] from O_TRNSPRT_BKNG_DTL Where Convert(varchar, Convert(datetime, O_BKNG_START_DT_TM, 102), 112) >= '" + FromYY.SelectedItem.Text+FromMM.SelectedItem.Text+FromDD.SelectedItem.Text + "' and Convert(varchar, Convert(datetime, O_BKNG_END_DT_TM, 102), 112) <= '" + ToDtYY.SelectedItem.Text+ToDtMM.SelectedItem.Text+ToDtDD.SelectedItem.Text+ "' and O_APLCNT_LCTN = '" + cmbLocation.SelectedItem.Text + "'";
				SqlCommand DateWiseReport = new SqlCommand(StrDateQuery,conn);
				SqlDataReader sdr1= DateWiseReport.ExecuteReader();
				dgExcel.DataSource=sdr1;
				dgExcel.DataBind();
				conn.Close();
				StringWriter objStringWriter = new StringWriter();
				System.Web.UI.HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
				Response.ClearContent();
				Response.ClearHeaders();
				Response.ContentType = "application/vnd.ms-excel";
				Response.Charset = "";
				Response.AddHeader("Content-Disposition","attachment;filename=Transport_LocationWise_Report.xls");
				this.EnableViewState = false;
				dgExcel.GridLines = GridLines.Both;
				dgExcel.HeaderStyle.Font.Bold=true;
				dgExcel.RenderControl(objHtmlTextWriter);
				Response.Write(objStringWriter.ToString());				
				Response.End();
			}

			// Employee Wise Generate Report
			if(chkEmp.Checked)
			{
				for(int i = 0 ; i <= LstEmp.Items.Count-1; i++)
				{
					if(LstEmp.Items[i].Selected)
					{
						strEmployeeAry.Add(LstEmp.Items[i].Text) ;
					}
				}
				BindEmpDataGrid();
			}		
		}
		#endregion

		#region Button Refresh Click Function
		protected void btnrefresh_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Transport_Reports.aspx");
		}
		#endregion

		#region Button Search Employee Click Function
		protected void BtnSearch_Click(object sender, System.EventArgs e)
		{
			SqlConnection conn=new SqlConnection(strConn);
			conn.Open();
			string EmployeeNames ="select O_CPF_NMBR,O_EMP_NM  from O_EMP_MSTR where O_EMP_NM like '%"+txtEmp.Text+"%' order by O_INDX_NMBR asc";
			SqlCommand SearchEmployee = new SqlCommand(EmployeeNames,conn);
			SqlDataReader sdr= SearchEmployee.ExecuteReader();
			if(sdr.Read())
			{
				LstEmp.DataSource=sdr;				
				LstEmp.DataTextField="O_EMP_NM";
				LstEmp.DataValueField="O_CPF_NMBR";
				LstEmp.DataBind();
			}
			else
			{
			
			}
			conn.Close();		
		}
		#endregion
	
		#region Bind Employee Data Grid
		private void BindEmpDataGrid()
		{	
			for(int i=0;i<strEmployeeAry.Count;i++)
			{
				if(i == strEmployeeAry.Count-1)
				{
					strEmp= strEmp+"'"+strEmployeeAry[i]+"'";
				}
				else
				{
					strEmp= strEmp+"'"+strEmployeeAry[i]+"',";
				}
			}
			if(strEmp != null)
			{
				SqlConnection conn=new SqlConnection(strConn);
				conn.Open();
				//Query For Search
				string strCommand1 = "select O_INDX_NMBR as [Sr. No], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No], O_APLCNT_DESG as [Applicant Designation], O_APLCNT_LCTN as [Applicant Location], O_APLCNT_EXT_NMBR as [Applicant Ext No], O_BKNG_START_DT_TM as [Booking Start Date], O_BKNG_END_DT_TM as [Booking End Date], O_BKNG_TYP as [Type Of Booking], O_RPRTNG_TM as [Vehicle Reporting Time], O_RPRT_TO_NM as [Report To], O_RPRT_TO_ADDR as [Reporting Person Address], O_RPRT_TO_CONT_NMBR1 as [Report To Contact Number1], O_RPRT_TO_CONT_NMBR2 as [Report To Contact Number2], O_NO_OF_PERSONS as [No Of Persons Travelling], O_BKNG_PRPS as [Purpose], O_RMRKS as [Comments], O_BKNG_APRVL_OFF_NM as [Approver Name], O_BKNG_APRVL_OFF_DESG as [Approver Designation], O_BKNG_APRVL_OFF_CPF_NMBR as [Approver CPF Number], O_SYS_DT_TM as [Booking Date] from O_TRNSPRT_BKNG_DTL where O_APLCNT_NM in ("+strEmp+")";
				string strCommand = strCommand1;
				//End Query For Search
				SqlCommand cmd1 = new SqlCommand(strCommand,conn);
				SqlDataReader sdr1= cmd1.ExecuteReader();
				dgExcel.DataSource=sdr1;
				dgExcel.DataBind();
				conn.Close();
				StringWriter objStringWriter = new StringWriter();
				System.Web.UI.HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
				Response.ClearContent();
				Response.ClearHeaders();
				Response.ContentType = "application/vnd.ms-excel";
				Response.Charset = "";
				Response.AddHeader("Content-Disposition","attachment;filename=Employee_Wise_Transport_Rpt.xls");
				this.EnableViewState = false;
				dgExcel.GridLines = GridLines.Both;
				dgExcel.HeaderStyle.Font.Bold=true;
				dgExcel.RenderControl(objHtmlTextWriter);
				Response.Write(objStringWriter.ToString());
				Response.End();
			}
		}
		#endregion
	}
}
