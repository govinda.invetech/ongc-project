﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisitorPassViewAndApprove.aspx.cs"
    Inherits="ONGCUIProjects.VisitorPassViewAndApprove" %>
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
<form id="form1" runat="server">
<div id="div_full_form" class="div-fullwidth" style="width: 100%; margin-top: 0;
    width: 900px;">
    <fieldset>
        <legend class="content" style="float: none; color: green">Requisitioner's Details</legend>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Name</h1>
                <asp:textbox id="txtEmpName" runat="server" enabled="False" Width="164px"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0; margin-right:10px; margin-left :10px;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Designation</h1>
                <asp:textbox id="txtDesg" runat="server" enabled="False" Width="164px"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    CPF No.</h1>
                <asp:textbox id="txtCPFNo" runat="server" enabled="False" Width="164px"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth">
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Location</h1>
                <asp:textbox id="cmbLocation" runat="server" style="width: 164px;" enabled="False"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;margin-right:10px; margin-left :10px;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Ext. No.</h1>
                <asp:textbox id="txtExtNo" runat="server" style="width: 164px;" enabled="False"></asp:textbox>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend class="content" style="float: none; color: green">Meeting Details</legend>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Whom to Meet</h1>
                <asp:textbox id="txtWhomToMeet" runat="server" style="width: 164px;" enabled="False"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;margin-right:10px; margin-left :10px;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Designation</h1>
                <asp:textbox id="txtMtng_Off_Desg" runat="server" style="width: 164px;" enabled="False"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    CPF No.</h1>
                <asp:textbox id="txtWhomToMeetCPF" runat="server" style="width: 164px;" enabled="False"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth">
            <div class="div-fullwidth" style="width: 284px; margin-top: 0; display: none;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Ext. No.</h1>
                <asp:textbox id="TextBox1" runat="server" style="width: 164px;" enabled="False"></asp:textbox>
            </div>
            <div class="div-fullwidth">
                <h1 class="gatepass-content2" style="width: 110px; color: #800000;">
                    Location</h1>
                
                <asp:textbox id="divMeetingLocation" runat="server" cssclass="flattxt" textmode="MultiLine"
                style="padding-top: 4px; width: 750px; max-width: 750px; height: 27px; max-height: 54px;
                overflow-y: auto; overflow-x: hidden;" enabled="False"></asp:textbox>
        </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend class="content" style="float: none; color: green">Pass Details</legend>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;">
                <h1 class="gatepass-content2" style="width: 110px; color: #800000;">
                    Entry Location :</h1>
                <asp:dropdownlist id="cmbEntryArea" runat="server" cssclass="flattxt" style="width: 163px;"
                    enabled="False">
                            <asp:ListItem Value="">Select Entry Area</asp:ListItem>
                            <asp:ListItem Value="Admin and White Area">Admin and White Area</asp:ListItem>
                            <asp:ListItem Value="White Area">White Area</asp:ListItem>
                            <asp:ListItem Value="Stores Complex">Stores Complex</asp:ListItem>
                            <asp:ListItem Value="Trombay Terminal">Trombay Terminal</asp:ListItem>
                            <asp:ListItem Value="Admin Area">Admin Area</asp:ListItem>
                            <asp:ListItem Value="Operational Area">Operational Area</asp:ListItem>
                            <asp:ListItem Value="All Area">All Area</asp:ListItem>
                        </asp:dropdownlist>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;margin-right:10px; margin-left :10px;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Entry Date :</h1>
                <asp:textbox id="visitor_start_date" runat="server" cssclass="flattxt" style="position: relative;
                    float: left;width: 164px;" enabled="False"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Valid Untill :</h1>
                <asp:textbox id="visitor_end_date" runat="server" cssclass="flattxt" style="position: relative;
                    float: left;width: 164px;" enabled="False"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: 284px">
                <h1 class="gatepass-content2" style="width: 110px;">
                    Reason Of Visit :</h1>
                <asp:dropdownlist id="cmbReason" runat="server" style="width: 163px;" enabled="False">
                            <asp:ListItem Value="">--- Select Reason Of Visit ---</asp:ListItem>
                            <asp:ListItem Value="Official">Official</asp:ListItem>
                            <asp:ListItem Value="Personal">Personal</asp:ListItem>
                        </asp:dropdownlist>
            </div>
            <div class="div-fullwidth" style="width: 284px; margin-top: 0;margin-right:10px; margin-left :10px;">
                <h1 class="gatepass-content2" style="width: 115px; margin-right:5px;">
                    Requisition Type :</h1>
                <asp:dropdownlist id="CmbRequisitionType" runat="server" style="width: 163px;" enabled="False">
                            <asp:ListItem Value="">Select Requisition Type</asp:ListItem>
                            <asp:ListItem Value="Entry On Week Days">Entry On Week Days</asp:ListItem>
                            <asp:ListItem Value="Entry On Holidays">Entry On Holidays</asp:ListItem>
                        </asp:dropdownlist>
            </div>
            <!--<div class="div-fullwidth" style="width: 284px; margin-top: 0; display :none;">
                <h1 class="gatepass-content2" style="width: auto; color: #800000;">
                    Approver :</h1>
                <div id="div_pass_approving_auth" runat="server" enabled="False">
                </div>
            </div>-->
        </div>
        <div class="div-fullwidth">
            <h1 class="gatepass-content2" style="width: 110px;">
                Purpose of Visit :</h1>
            <asp:textbox id="txtRemarks" runat="server" cssclass="flattxt" textmode="MultiLine"
                style="padding-top: 4px; width: 750px; max-width: 750px; height: 27px; max-height: 54px;
                overflow-y: auto; overflow-x: hidden;" enabled="False"></asp:textbox>
        </div>
    </fieldset>
    <fieldset>
        <legend class="content" 
            style="float: none; color: green; top: 0px; left: 10px;">Visitor Details</legend>
        <div runat="server" id="div_visitor_list" class="div-fullwidth"></div>
    </fieldset>
</div>
</form>
