<%@ Page language="c#" Codebehind="Incidence_Manager.aspx.cs" AutoEventWireup="false" Inherits="ONGCUIProjects.Incidence_Manager" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Incidence_Manager</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U>Incident 
								Manager</U></STRONG></FONT></FONT></P>
			<P>
			<P>
			<P>
				<HR width="100%" SIZE="1">
			<P></P>
			<P>
				<asp:button id="BtnShowRevInc" runat="server" Font-Size="XX-Small" Font-Names="Verdana" Width="224px"
					BorderColor="DarkGray" BackColor="Silver" ForeColor="ControlText" Height="18px" Font-Bold="True"
					Text="Show Fresh Reviewed Incidents" BorderStyle="Solid" CausesValidation="False"></asp:button>&nbsp;&nbsp;
				<asp:button id="BtnShowAccReturns" runat="server" Font-Size="XX-Small" Font-Names="Verdana"
					Width="224px" BorderColor="DarkGray" BackColor="Silver" ForeColor="ControlText" Height="18px"
					Font-Bold="True" Text="Show Acceptance Returns" BorderStyle="Solid" CausesValidation="False"></asp:button></P>
			<P></P>
			<FONT face="Verdana" color="red" size="1">
				<P align="center">
					<TABLE id="tblFreshRevInc" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
						<TBODY>
							<TR>
								<TD>
									<P><FONT face="Verdana" color="brown" size="2"><STRONG><U>
													<asp:Label id="lblRevDetls" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small">Action Out Following Reviewed Incidents:</asp:Label></P>
				</U></STRONG></FONT>
			<P></P>
			<P align="center">
				<asp:DataGrid id="DGrd_RevInc" runat="server" ForeColor="DarkRed" BackColor="WhiteSmoke" BorderColor="Gainsboro"
					Width="100%" Font-Names="Verdana" Font-Size="XX-Small" AutoGenerateColumns="False" CellPadding="5"
					HorizontalAlign="Center">
					<HeaderStyle Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Center"
						VerticalAlign="Middle" BackColor="WhiteSmoke"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="O_REV_ROOT_CAUSE" HeaderText="Root Cause">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="O_REV_RECMND" HeaderText="Recomendations">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="O_REV_PRECTN_MEASR" HeaderText="Safety Measures Required" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
						<asp:BoundColumn DataField="O_REV_INCDNT_CTGRY" HeaderText="Incident Category" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
						<asp:BoundColumn DataField="O_REV_ACTION_TARGET_DT" HeaderText="Target Date" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
						<asp:BoundColumn DataField="O_REV_TASK_STS" HeaderText="Status" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
						<asp:TemplateColumn>
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
							<ItemTemplate>
								<asp:LinkButton id="LnkButton" CommandName="Action" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"O_INDX_NMBR") %>' Text="Show Complaint" CausesValidation=False runat="server">Show Reviewed Incident</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:DataGrid></P>
			<P><FONT face="Verdana" color="red" size="1">
					<asp:Label id="lblActnRev" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small">View Incident Details:</asp:Label>&nbsp;&nbsp;&nbsp;
					<asp:textbox id="txtComplaintID" runat="server" Height="10px" Width="14px" Visible="False"></asp:textbox>&nbsp;&nbsp;&nbsp;</FONT><FONT face="Verdana" color="red" size="1"></P>
			</FONT><FONT face="Verdana" color="red" size="1">
				<TABLE id="IncidentDetail" cellSpacing="8" cellPadding="4" width="100%" align="center"
					bgColor="whitesmoke" border="1" runat="server">
					<TBODY>
						<TR>
							<TD style="WIDTH: 161px" borderColor="gray" align="center" bgColor="whitesmoke">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red"><FONT face="Verdana" color="#660000" size="1"><STRONG>Incident 
														Description</STRONG></FONT></FONT></STRONG></FONT></P>
							</TD>
							<TD style="WIDTH: 225px" borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
										<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
													<asp:textbox id="txtIncDesc" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
														Width="225px" Font-Names="Verdana" Font-Size="XX-Small" ReadOnly="True" MaxLength="8000"
														TextMode="MultiLine" CssClass="flattxt"></asp:textbox></FONT></STRONG></P>
									</FONT></STRONG>
							</TD>
							<TD style="WIDTH: 134px" borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG>
										<P align="left">Action Taken <FONT color="red">(*)</FONT>
									</STRONG></FONT></P></TD>
							<TD borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
										<P align="left">
											<asp:textbox id="txtActionTaken" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
												Width="225px" Font-Names="Verdana" Font-Size="XX-Small" MaxLength="8000" TextMode="MultiLine"
												CssClass="flattxt"></asp:textbox>
									</FONT></STRONG></P></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px" borderColor="#808080" align="center" bgColor="#f5f5f5">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red"><FONT face="Verdana" color="#660000" size="1"><STRONG>Reviewer's 
														Remarks</STRONG></FONT></FONT></STRONG></FONT></P>
							</TD>
							<TD style="WIDTH: 225px" borderColor="#808080" align="center" bgColor="#f5f5f5">
								<asp:textbox id="txRevRmrks" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
									Width="225px" Font-Names="Verdana" Font-Size="XX-Small" ReadOnly="True" MaxLength="8000"
									TextMode="MultiLine" CssClass="flattxt"></asp:textbox></TD>
							<TD style="WIDTH: 134px" borderColor="#808080" align="center" bgColor="#f5f5f5">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Comments</STRONG></FONT></P>
							</TD>
							<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
								<P align="left">
									<asp:textbox id="txtComments" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
										Width="225px" Font-Names="Verdana" Font-Size="XX-Small" MaxLength="8000" TextMode="MultiLine"
										CssClass="flattxt"></asp:textbox></P>
							</TD>
						</TR>
						<TR>
							<TD borderColor="gray" align="center" bgColor="whitesmoke" colSpan="2"><FONT face="Verdana" color="#660000" size="1"><STRONG>
										<P align="center"><FONT face="Verdana" color="#660000" size="1"><STRONG>Forward Data To 
													Acceptance Authority<FONT color="red">(*)</FONT></STRONG></FONT>
									</STRONG></FONT><STRONG><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT face="Verdana" color="#660000" size="1">
												</P></FONT></STRONG></FONT></STRONG>
			</FONT></STRONG></TD>
			<TD borderColor="gray" align="center" bgColor="whitesmoke" colSpan="2"><FONT face="Verdana" color="#660000" size="1"><STRONG></STRONG></FONT><STRONG><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<P align="center">
									<asp:dropdownlist id="cmbAccpName" runat="server" Width="225px" Font-Names="Verdana" Font-Size="XX-Small"
										CssClass="flattxt"></asp:dropdownlist></P>
							</FONT></STRONG></FONT></STRONG>
			</TD>
			</TR></TBODY></TABLE>
			<P align="center">
				<asp:button id="BtnActionTaken" runat="server" BorderStyle="Solid" Text="Action Taken" Font-Bold="True"
					ForeColor="ControlText" BackColor="Silver" BorderColor="DarkGray" Width="225px" Font-Names="Verdana"
					Font-Size="XX-Small"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnRefresh" runat="server" CausesValidation="False" BorderStyle="Solid" Text="Refresh"
					Font-Bold="True" Height="18px" ForeColor="ControlText" BackColor="Silver" BorderColor="DarkGray"
					Width="225px" Font-Names="Verdana" Font-Size="XX-Small"></asp:button></P>
			</FONT></TD></TR></TBODY></TABLE></P>
			<P align="center">
				<TABLE id="tblReturns" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
					<TBODY>
						<TR>
							<TD>
								<P><FONT face="Verdana" color="brown" size="2"><STRONG><U>
												<asp:Label id="lblreturn" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small">Action Out Following Acceptance Returns</asp:Label></P>
								</U></STRONG></FONT>
								<P></P>
								<P align="center">
									<asp:DataGrid id="Dgrd_AcptnReturn" runat="server" ForeColor="DarkRed" BackColor="WhiteSmoke"
										BorderColor="Gainsboro" Width="100%" Font-Names="Verdana" Font-Size="XX-Small" AutoGenerateColumns="False"
										CellPadding="5" HorizontalAlign="Center">
										<HeaderStyle Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Center"
											VerticalAlign="Middle" BackColor="WhiteSmoke"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="O_REV_ROOT_CAUSE" HeaderText="Root Cause">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="O_REV_RECMND" HeaderText="Recomendations">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="O_REV_PRECTN_MEASR" HeaderText="Safety Measures Required">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="O_REV_INCDNT_CTGRY" HeaderText="Incident Category">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="O_REV_ACTION_TARGET_DT" HeaderText="Target Date">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="O_REV_TASK_STS" HeaderText="Status">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:LinkButton id="LnkButton" CommandName="Action" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"O_INDX_NMBR") %>' Text="Show Complaint" CausesValidation=False runat="server">Show Reviewed Incident</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:DataGrid></P>
								<P><FONT face="Verdana" color="red" size="1">
										<asp:Label id="lblViewData" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small">View Incident Details:</asp:Label>&nbsp;&nbsp;&nbsp;
										<asp:textbox id="txtRevNo" runat="server" Height="10px" Width="14px" Visible="False"></asp:textbox>&nbsp;
										<asp:textbox id="txtIncNo" runat="server" Height="10px" Width="14px" Visible="False"></asp:textbox>
										<asp:textbox id="txtActNo" runat="server" Height="10px" Width="14px" Visible="False"></asp:textbox>
										<asp:textbox id="txtAcptNo" runat="server" Height="10px" Width="14px" Visible="False"></asp:textbox></FONT><FONT face="Verdana" color="red" size="1"></P>
								</FONT><FONT face="Verdana" color="red" size="1">
									<TABLE id="Table2" cellSpacing="8" cellPadding="4" width="100%" align="center" bgColor="whitesmoke"
										border="1" runat="server">
										<TBODY>
											<TR>
												<TD style="WIDTH: 161px" borderColor="gray" align="center" bgColor="whitesmoke">
													<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red"><FONT face="Verdana" color="#660000" size="1"><STRONG>Incident 
																			Description</STRONG></FONT></FONT></STRONG></FONT></P>
												</TD>
												<TD style="WIDTH: 225px" borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
															<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
																		<asp:textbox id="TxtIncDescr" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
																			Width="225px" Font-Names="Verdana" Font-Size="XX-Small" ReadOnly="True" MaxLength="8000"
																			TextMode="MultiLine" CssClass="flattxt"></asp:textbox></FONT></STRONG></P>
														</FONT></STRONG>
												</TD>
												<TD style="WIDTH: 134px" borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG>
															<P align="left">
															Last Action Taken </STRONG></FONT>
			</P>
			</TD>
			<TD borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
						<P align="left">
							<asp:textbox id="txtActTaken" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
								Width="225px" Font-Names="Verdana" Font-Size="XX-Small" MaxLength="8000" TextMode="MultiLine"
								CssClass="flattxt" ReadOnly="True"></asp:textbox>
					</FONT></STRONG></P></TD>
			</TR>
			<TR>
				<TD style="WIDTH: 161px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red"><FONT face="Verdana" color="#660000" size="1"><STRONG>Reviewer's 
											Remarks</STRONG></FONT></FONT></STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 225px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<asp:textbox id="txtRevRmrks" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
						Width="225px" Font-Names="Verdana" Font-Size="XX-Small" ReadOnly="True" MaxLength="8000"
						TextMode="MultiLine" CssClass="flattxt"></asp:textbox></TD>
				<TD style="WIDTH: 134px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Last Action Taken 
								Remarks</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="TxtActnRmrks" runat="server" BorderStyle="Solid" Height="88px" BorderColor="#E0E0E0"
							Width="225px" Font-Names="Verdana" Font-Size="XX-Small" MaxLength="8000" TextMode="MultiLine"
							CssClass="flattxt" ReadOnly="True"></asp:textbox></P>
				</TD>
			</TR>
			<TR>
				<TD borderColor="gray" align="center" bgColor="whitesmoke" colSpan="2"><FONT face="Verdana" color="#660000" size="1"><STRONG>
							<P align="center"><FONT face="Verdana" color="#660000" size="1"><STRONG>Forward Data To 
										Acceptance Authority</STRONG></FONT>
						</STRONG></FONT><STRONG><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT face="Verdana" color="#660000" size="1">
									</P></FONT></STRONG></FONT></STRONG></FONT></STRONG></TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke" colSpan="2"><FONT face="Verdana" color="#660000" size="1"><STRONG></STRONG></FONT><STRONG><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT face="Verdana" color="#660000" size="1">
									<P align="center">&nbsp;</P>
								</FONT></STRONG></FONT></STRONG>
				</TD>
			</TR>
			</TBODY></TABLE>
			<P align="center">
				<asp:button id="BtnRevsActnTaken" runat="server" BorderStyle="Solid" Text="Revised Action Taken"
					Font-Bold="True" ForeColor="ControlText" BackColor="Silver" BorderColor="DarkGray" Width="225px"
					Font-Names="Verdana" Font-Size="XX-Small"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnRevsRefresh" runat="server" CausesValidation="False" BorderStyle="Solid"
					Text="Refresh" Font-Bold="True" Height="18px" ForeColor="ControlText" BackColor="Silver"
					BorderColor="DarkGray" Width="225px" Font-Names="Verdana" Font-Size="XX-Small"></asp:button></P>
			</FONT></TD></TR></TBODY></TABLE></P>
			<P align="center">
				<asp:ValidationSummary id="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
				<asp:RequiredFieldValidator id="ActionTaken" runat="server" ErrorMessage="Please Fill complete action taken by you !!!!"
					Display="None" ControlToValidate="txtActionTaken"></asp:RequiredFieldValidator></P>
			</FONT>
		</form>
	</body>
</HTML>
