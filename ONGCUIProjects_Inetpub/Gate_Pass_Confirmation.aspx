<%@ Page language="c#" Codebehind="Gate_Pass_Confirmation.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Gate_Pass_Confirmation" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Gate_Pass_Confirmation</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<FORM id="Form1" method="post" runat="server">
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U>
								<asp:textbox id="txtComplaintID" style="Z-INDEX: 101; LEFT: 272px; POSITION: absolute; TOP: 296px"
									runat="server" Height="3px" Width="5px" Visible="False"></asp:textbox>Entry 
								Gate Pass Confirmation</U></STRONG></FONT></FONT></P>
			<P>
				<HR width="100%" SIZE="1">
			<P></P>
			<STRONG><FONT face="Verdana" color="red" size="1"><STRONG><FONT face="Verdana" color="red" size="1">
							<P><FONT face="Verdana" color="brown" size="2"><STRONG><U>Pass Requisitions:</U></STRONG></FONT></P>
							<P></P>
							<P align="center">
								<asp:datagrid id="DGrd_Complaints" runat="server" CellPadding="5" HorizontalAlign="Center" BackColor="WhiteSmoke"
									ForeColor="DarkRed" BorderColor="Gainsboro" Font-Size="XX-Small" Font-Names="Verdana" Width="100%"
									AutoGenerateColumns="False">
									<HeaderStyle Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Center"
										VerticalAlign="Middle" BackColor="WhiteSmoke"></HeaderStyle>
									<Columns>
										<asp:BoundColumn DataField="O_VISTR_NM" HeaderText="Visitor Name">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="O_MTNG_OFF_NM" HeaderText="Meeting Officer's Name">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="O_MTNG_OFF_DESG" HeaderText="Designation">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="O_MTNG_OFF_LCTN" HeaderText="Officer's Location">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton id="LnkButton" CommandName="ComplaintClose" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"O_INDX_NMBR") %>' Text="Show Request" runat="server">Show Details</asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid></P>
						</FONT></STRONG></FONT></STRONG>
			<P><STRONG><FONT face="Verdana" color="red" size="1">(Fields Marked With * are Mandatory 
						Fields)</FONT></STRONG>
			</P>
			<P align="center">
				<TABLE id="Table2" cellSpacing="8" cellPadding="4" width="100%" align="center" bgColor="whitesmoke"
					border="1">
					<TBODY>
						<TR>
							<TD style="WIDTH: 144px" borderColor="gray" align="center" bgColor="whitesmoke">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Name of the Applicant <FONT color="red">
												(*)</FONT></STRONG></FONT></P>
							</TD>
							<TD style="WIDTH: 283px" borderColor="gray" align="center" bgColor="whitesmoke">
								<P align="left"><FONT face="Verdana" size="1">
										<asp:textbox id="txtAppName" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
											Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="200" ReadOnly="True"></asp:textbox></FONT></P>
							</TD>
							<TD style="WIDTH: 232px" borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG>
										<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Designation&nbsp;<FONT color="red">(*)</FONT></STRONG></FONT>
									</STRONG></FONT>
			</P>
			</TD>
			<TD borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
						<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
									<asp:textbox id="txtDesg" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
										Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG>
					</FONT></STRONG></P></TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>CPF No.&nbsp;<FONT color="red">(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtCPFNo" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="50" ReadOnly="True"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Location <FONT color="red">
									(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtLocation" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Phone Extension&nbsp;<FONT color="red">(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtExtNo" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="10" ReadOnly="True"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Entry Date <FONT color="red">
									(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtEntryDate" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px; HEIGHT: 34px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Entry Time</STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px; HEIGHT: 34px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtEntryTime" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
				<TD style="WIDTH: 232px; HEIGHT: 34px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Exit Date <FONT color="red">
									(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD style="HEIGHT: 34px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left">
						<asp:textbox id="txtExitDate" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Exit Time</STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtExitTime" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Type&nbsp;of 
								Requisition <FONT color="red">(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtReqType" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Entry To <FONT color="red">
									(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="TxtEntryArea" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="#000099">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>ONGC&nbsp;Meeting 
											Officer's Name <FONT color="red">(*)</FONT></STRONG></FONT>
							</FONT></STRONG></FONT></P></TD>
				<TD borderColor="gray" align="center" bgColor="whitesmoke">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtOffName" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="200" ReadOnly="True"></asp:textbox></FONT></STRONG></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>ONGC Meeting 
								Officer's Designation&nbsp;<FONT color="red">(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtMtng_Off_Desg" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Meeting Officer's 
								Location&nbsp;<FONT color="red">(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<asp:textbox id="txtOffLocation" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
						Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Name <FONT color="red">
									(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<asp:textbox id="txtVistr_NM" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
						Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="200" ReadOnly="True"></asp:textbox></TD>
				<TD style="WIDTH: 232px" borderColor="#808080" align="center" bgColor="#f5f5f5"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red">
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's 
											Organization <FONT color="red">(*)</FONT></STRONG></FONT>
							</FONT></STRONG></FONT></P></TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtVistr_Org" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="200" ReadOnly="True"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Designation</STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<asp:textbox id="txtVistr_Desg" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
						Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></TD>
				<TD style="WIDTH: 232px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Age <FONT color="red">
									(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtVistr_Age" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="48px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="2" ReadOnly="True"></asp:textbox></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Gender <FONT color="red">
									(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtGender" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="100" ReadOnly="True"></asp:textbox></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's 
								Indentification Mark</STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<asp:textbox id="txtVistr_Idnt_Mark" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
						Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="500" ReadOnly="True"></asp:textbox></TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Visitor's Blood Group</STRONG></FONT></P>
				</TD>
				<TD style="WIDTH: 283px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left">
						<asp:textbox id="txtVistr_BldGrp" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
							Width="104px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="10" ReadOnly="True"></asp:textbox></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Purpose&nbsp;<FONT color="red">(*)</FONT></STRONG></FONT></P>
				</TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="TxtPurpose" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="2000" ReadOnly="True"
									Height="88px" TextMode="MultiLine"></asp:textbox></FONT></STRONG></P>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px" borderColor="#808080" align="center" bgColor="#f5f5f5"><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT color="red">
								<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1"></FONT></STRONG></P>
								<P align="left"><FONT face="Verdana" color="#660000" size="1"><STRONG>Remarks</STRONG></FONT>
							</FONT></STRONG></FONT></P></TD>
				<TD style="WIDTH: 283px" borderColor="#808080" align="center" bgColor="#f5f5f5">
					<P align="left"><STRONG><FONT face="Verdana" color="#660000" size="1">
								<asp:textbox id="txtRemarks" runat="server" CssClass="flattxt" Font-Size="XX-Small" Font-Names="Verdana"
									Width="225px" BorderColor="#E0E0E0" BorderStyle="Solid" MaxLength="8000" ReadOnly="True"
									Height="88px" TextMode="MultiLine"></asp:textbox></FONT></STRONG></P>
				</TD>
				<TD style="WIDTH: 232px" borderColor="#808080" align="center" bgColor="#f5f5f5"></TD>
				<TD borderColor="#808080" align="center" bgColor="#f5f5f5"></TD>
			</TR>
			</TBODY></TABLE></P>
			<P align="center">
				<asp:button id="BtnPassConfirmation" runat="server" Font-Size="XX-Small" Font-Names="Verdana"
					Width="264px" BorderColor="DarkGray" BorderStyle="Solid" Font-Bold="True" BackColor="Silver"
					ForeColor="ControlText" Text="Pass Generated" onclick="BtnPassConfirmation_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnRefresh" runat="server" Font-Size="XX-Small" Font-Names="Verdana" Width="256px"
					BorderColor="DarkGray" BorderStyle="Solid" Height="18px" CausesValidation="False" Font-Bold="True"
					BackColor="Silver" ForeColor="ControlText" Text="Refresh" onclick="BtnRefresh_Click"></asp:button></P>
		</FORM>
	</body>
</HTML>
