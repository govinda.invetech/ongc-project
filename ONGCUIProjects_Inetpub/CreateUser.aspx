<%@ Page language="c#" Codebehind="CreateUser.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.CreateUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CreateUser</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="style/ONGCStyle.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="middle" align="center">
						<TABLE id="Table3" style="WIDTH: 541px; HEIGHT: 370px" height="370" cellSpacing="0" cellPadding="0"
							width="541" border="1">
							<TR height="40">
								<TD align="center"><span class="LoginText">User Createation&nbsp;Form</span>
								</TD>
							</TR>
							<TR bgColor="#eeeeee">
								<TD style="WIDTH: 389px" align="center">
									<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD style="WIDTH: 242px; HEIGHT: 12px"><span class="subcontent">&nbsp;User ID</span></TD>
											<TD style="HEIGHT: 12px"><asp:textbox id="txtUserID" runat="server" BorderColor="#E0E0E0" BorderStyle="Solid" Font-Names="Verdana"
													Font-Size="XX-Small" CssClass="form_input" MaxLength="10" Width="120px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 242px; HEIGHT: 6px"><span class="subcontent">&nbsp;</span></TD>
											<TD style="HEIGHT: 6px"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 242px; HEIGHT: 30px"><span class="subcontent">&nbsp;User Name</span></TD>
											<TD style="HEIGHT: 30px"><asp:dropdownlist id="ddlUserName" runat="server" CssClass="form_input" Width="200px"></asp:dropdownlist></TD>
										</TR>
										<TR height="9">
											<TD style="WIDTH: 242px"></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 242px"><span class="subcontent">&nbsp;Password</span></TD>
											<TD><asp:textbox id="txtPwd" runat="server" BorderColor="#E0E0E0" Font-Names="Verdana" Font-Size="XX-Small"
													CssClass="form_input" MaxLength="6" Width="120px" TextMode="Password"></asp:textbox></TD>
										</TR>
										<tr height="9">
											<TD style="WIDTH: 242px"></TD>
											<TD></TD>
										</tr>
										<TR>
											<TD style="WIDTH: 242px"><SPAN class="subcontent">&nbsp;User Description</SPAN></TD>
											<TD vAlign="bottom" align="left"><asp:textbox id="txtDesc" runat="server" BorderColor="#E0E0E0" Font-Names="Verdana" Font-Size="XX-Small"
													CssClass="form_input" MaxLength="40" Width="200px"></asp:textbox></TD>
										</TR>
										<TR height="9">
											<TD style="WIDTH: 242px"></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 242px; HEIGHT: 19px"><SPAN class="subcontent">&nbsp;Role</SPAN></TD>
											<TD style="HEIGHT: 19px"><asp:dropdownlist id="ddlRole" runat="server" CssClass="form_input" Width="200px"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 242px" height="12"></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="right" colSpan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</TD>
										</TR>
										<TR height="9">
											<TD style="WIDTH: 242px" align="right"><asp:button id="btnSave" runat="server" BorderStyle="Solid" Font-Names="Verdana" Font-Size="XX-Small"
													CssClass="form_submit " Font-Bold="True" Text="Save" onclick="btnSave_Click"></asp:button></TD>
											<TD align="left">&nbsp;&nbsp;&nbsp;<asp:button id="btnCancle" runat="server" BorderStyle="Solid" Font-Names="Verdana" Font-Size="XX-Small"
													CssClass="form_submit " Font-Bold="True" Text="Cancel" CausesValidation="False" onclick="btnCancle_Click"></asp:button></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 37px" colSpan="2"><asp:regularexpressionvalidator id="revUserID" runat="server" ErrorMessage="Enter Valid UserID" ValidationExpression="^[0-9A-Za-z\s]+$"
													ControlToValidate="txtUserID" Display="None"></asp:regularexpressionvalidator><asp:regularexpressionvalidator id="revPassword" runat="server" ErrorMessage="Enter Valid Password" ValidationExpression="^([0-9A-Za-z]+)$"
													ControlToValidate="txtPwd" Display="None"></asp:regularexpressionvalidator><asp:regularexpressionvalidator id="revDes" runat="server" ErrorMessage="Enter Valid User Description" ValidationExpression="^[0-9A-Za-z\s]+$"
													ControlToValidate="txtDesc" Display="None"></asp:regularexpressionvalidator>
												<asp:RequiredFieldValidator id="rfvPassword" runat="server" ErrorMessage="Enter Password" ControlToValidate="txtPwd"
													Display="None"></asp:RequiredFieldValidator>
												<asp:RequiredFieldValidator id="rfvUserID" runat="server" ErrorMessage="Enter UserID" ControlToValidate="txtUserID"
													Display="None"></asp:RequiredFieldValidator></TD>
											<TD style="HEIGHT: 37px"></TD>
										</TR>
									</TABLE>
									<asp:validationsummary id="VsUser" runat="server" CssClass="flattxt" DisplayMode="List" ShowMessageBox="True"
										ShowSummary="False"></asp:validationsummary></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
