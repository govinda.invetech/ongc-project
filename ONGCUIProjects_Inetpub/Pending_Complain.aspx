﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pending_Complain.aspx.cs" Inherits="ONGCUIProjects.Pending_Complain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>    
      <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
         <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtdatefrom").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            $("#txtdateto").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            var DivWindowHeight = parseInt($(window).height()) - 104;
            $('#report_container').css('max-height', DivWindowHeight + 'px');
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">

         <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                    <h1 class="heading" style="width: auto;">Complaint Closure</h1>                   
                </div>
        <br />
         <br />
        <br />
   <div style="margin-left:5px; float:left;">  
          <asp:Label ID="lbl1" runat="server" Text="Select Status Type"></asp:Label>&nbsp;&nbsp;&nbsp;
          <asp:DropDownList ID="ddl_status" AutoPostBack="true" runat="server" Height="30px" Width="160px" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
              <asp:ListItem Selected="True" Value="ALL">ALL</asp:ListItem>
              <asp:ListItem Value="NEW">NEW</asp:ListItem>
                <asp:ListItem Value="ASSIGN">ASSIGNED</asp:ListItem>
                 <asp:ListItem Value="WORK IN PROGRESS">WORK IN PROGRESS</asp:ListItem> 
                 <asp:ListItem Value="RESOLVED">RESOLVED</asp:ListItem>     
          </asp:DropDownList>&nbsp;&nbsp; 
       </div>   
        <div style="margin-right:10px; float:right;"> 
            <asp:Label ID="eto" runat="server" Text="From Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdatefrom" runat="server" Width="80px" Height="26px"></asp:TextBox>&nbsp;&nbsp;
             <asp:Label ID="Label1" runat="server" Text="To Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdateto" runat="server"  Width="80px" Height="26px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
           <asp:Button ID="btn2" runat="server" Text="Generate" Height="26px" OnClick="btn2_Click"/>
             
                    
    </div>
 
         <br />
        <br />      
       
        
    <div id="request_detail_div"  style=" overflow:scroll; margin-top:50px; margin-left:20px; height:350px; ">
    <asp:GridView ID="grid_detail_complaint" runat="server" AutoGenerateColumns="False" Width="1200px"  OnRowDataBound="grid_detail_complaint_RowDataBound" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None">
        <AlternatingRowStyle BackColor="PaleGoldenrod" />
        <Columns>
            <asp:TemplateField HeaderText="Sr.No.">
                <ItemTemplate>
                    <asp:Label ID="lcl1" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="S No." Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId_m" runat="server" Text='<%#Eval("ID")%>' CssClass="" ToolTip="S No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applicant">
                            <ItemTemplate>
                                <div style="width:190px; text-align:center;"><asp:Label ID="Label2" runat="server" Text='<%#Eval("APP_NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation ">
                            <ItemTemplate>
                                <div style=" text-align:center;"><asp:Label ID="Label3" runat="server" Text='<%#Eval("APP_DESIG")%>' CssClass="" ToolTip="Designation"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone Extension No.">
                            <ItemTemplate>
                                <div style="text-align:center;"><asp:Label ID="Label4" runat="server" Text='<%#Eval("APP_PHONE_EX_NO")%>' CssClass="" ToolTip="Phone Extension No."></asp:Label></div>
                            </ItemTemplate>
                            <%--    SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[] ,[APP_MOBILE_NO] ,[],[],[COMPLAIN_TYPE_ID] ,[] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[] ,[] --%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                               <div style=" text-align:center;"> <asp:Label ID="Label5" runat="server" Text='<%#Eval("APP_LOCATION")%>' CssClass="" ToolTip="Location"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Problem">
                            <ItemTemplate>
                                <div style=" text-align:center;"><asp:Label ID="Label6" runat="server" Text='<%#Eval("PROBLEM_DISCRIPTION")%>' CssClass="" ToolTip="Problem"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room No.">
                            <ItemTemplate>
                                <div style="text-align:center;"><asp:Label ID="Label7" runat="server" Text='<%#Eval("ROOM_NO")%>' CssClass="" ToolTip="Room No."></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Type">
                            <ItemTemplate>
                                <div><asp:Label ID="Label8" runat="server" Text='<%#Eval("COMPLAIN_DEPARTMENT")%>' CssClass="" ToolTip="Complain Type"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Date">
                            <ItemTemplate>
                                <div style="width:130px; text-align:center;"><asp:Label ID="Label9" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd-MM-yyyy}")%>' CssClass="" ToolTip="Complain Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Label ID="lbl_act" runat="server" Visible="false" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status"></asp:Label>
                   <asp:DropDownList ID="ddl_action" runat="server"  Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddl_action_SelectedIndexChanged"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
<%--             <asp:TemplateField HeaderText="View/Edit">
                <ItemTemplate>
                    <a class='view_cmpln_dtl_fancy' href="Complaint_Registration.aspx?my_hint=APPROVE`<%#Eval("ID")%>" >View Detail</a>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>

        <FooterStyle BackColor="Tan" />
        <HeaderStyle BackColor="Tan" Font-Bold="True" />
        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
        <SortedAscendingCellStyle BackColor="#FAFAE7" />
        <SortedAscendingHeaderStyle BackColor="#DAC09E" />
        <SortedDescendingCellStyle BackColor="#E1DB9C" />
        <SortedDescendingHeaderStyle BackColor="#C2A47B" />

    </asp:GridView>
        </div>
    </form>
</body>
</html>
