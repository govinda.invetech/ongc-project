﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class QuestionReports : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            cmdPrintReport.Visible = false;
            if (Session["Login_Name"] == null)
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
            }
            else
            {
                if (string.IsNullOrEmpty(Request["FromDate"]) || string.IsNullOrEmpty(Request["FromDate"]))
                {

                }
                else
                {
                    string sFromDate = My.ConvertDateStringintoSQLDateString(Request["FromDate"]);
                    string sToDate = My.ConvertDateStringintoSQLDateString(Request["ToDate"]);

                    DateTime dtFromDate = Convert.ToDateTime(sFromDate);
                    DateTime dtToDate = Convert.ToDateTime(sToDate);

                    txtFromDate.Text = dtFromDate.ToString("dd-MM-yyyy");
                    txtToDate.Text = dtToDate.ToString("dd-MM-yyyy");

                    string sQuery = "SELECT [id],[que_txt],[start_date],[end_date],[que_type],[remark] FROM [CY_QUE_DTLS] WHERE [start_date] BETWEEN CAST('" + sFromDate + "' as datetime) AND CAST('" + sToDate + "' as datetime) ORDER BY [start_date] ASC";

                    DataSet dsQuestion = My.ExecuteSELECTQuery(sQuery);

                    if (dsQuestion.Tables[0].Rows.Count == 0)
                    {
                        cmdPrintReport.Visible = false;
                        div_que_report.InnerHtml = "<p class='content' style='float:none; text-align:center; width:100%; font-size: 30px; margin-top: 100px;' >No Question Found</p>";

                    }
                    else
                    {
                        cmdPrintReport.Visible = true;
                        string sTableData = "<p class='content' style='float:none; text-align:center; width:100%; font-size: 30px; margin-bottom: 5px;'>Results of Survey</p>";
                        sTableData += "<table class='print-table' border='1'>";
                        for (int i = 0; i < dsQuestion.Tables[0].Rows.Count; i++)
                        {

                            string sQuestionID = dsQuestion.Tables[0].Rows[i][0].ToString();
                            string sQuestionText = dsQuestion.Tables[0].Rows[i][1].ToString();
                            string sQuestionStartDate = dsQuestion.Tables[0].Rows[i][2].ToString();
                            string sQuestionEndDate = dsQuestion.Tables[0].Rows[i][3].ToString();
                            string sQuestionRemark = dsQuestion.Tables[0].Rows[i][5].ToString();
                            decimal iTotalReponse = My.getReponseCountFromQueID(sQuestionID);


                            sTableData += "<tr>";
                            sTableData += "<td colspan='3' style='font-size: 18px;'>" + (i + 1) + ". " + sQuestionText + "</td>";
                            sTableData += "</tr>";

                            sTableData += "<tr style='border-bottom:4px dashed #000;'>";
                            sTableData += "<td>";
                            sTableData += "<table class='print-table' border='1'>";
                            sTableData += "<tr>";
                            sTableData += "<td rowspan='2' colspan='2' style='text-align: center; border-bottom:2px solid #000;'>Options</td>";
                            sTableData += "<td colspan='2' style='text-align: center;'>Response</td>";
                            sTableData += "</tr>";
                            sTableData += "<tr style='border-bottom:2px solid #000;'>";
                            sTableData += "<td style='width:50px; text-align: center;' >Per(%)</td>";
                            sTableData += "<td style='width:50px; text-align: center;'>Count</td>";
                            sTableData += "</tr>";
                            string sGraphData = "";
                            string OptStr = "SELECT [id], [option_text], [is_true], [order_by] FROM CY_QUE_ANS_RELATION WHERE [que_id] = " + sQuestionID + "ORDER BY [order_by] ASC";
                            DataSet dsOption = My.ExecuteSELECTQuery(OptStr);
                            if (dsOption.Tables[0].Rows.Count != 0)
                            {


                                decimal dOptionData = 0;
                                for (int j = 0; j <= dsOption.Tables[0].Rows.Count - 1; j++)
                                {
                                    string OptionText = dsOption.Tables[0].Rows[j][1].ToString();
                                    string OptID = dsOption.Tables[0].Rows[j][3].ToString();
                                    string FullOption = OptionText;
                                    string IsTrue = dsOption.Tables[0].Rows[j][2].ToString();

                                    #region For Graph
                                    decimal OptionData = My.getOptionReponseCountFromQueIDAndOptID(sQuestionID, OptID);
                                    if (OptionData != 0)
                                    {
                                        dOptionData = Math.Round((OptionData * 100) / iTotalReponse, 1);
                                    }
                                    else if (OptionData == 0)
                                    {
                                        dOptionData = 0;
                                    }
                                    sGraphData = sGraphData + My.getOptionLetter(OptID) + "`" + dOptionData + "~";
                                    #endregion

                                    if (OptionText.Trim().Length > 90)
                                    {
                                        OptionText = OptionText.Substring(0, 90) + "...";
                                    }

                                    sTableData += "<tr>";
                                    sTableData += "<td style='width:8px; text-align: center;'>" + My.getOptionLetter(OptID) + "</td>";
                                    sTableData += "<td>" + OptionText + "</td>";
                                    sTableData += "<td style='text-align: center;'>" + dOptionData + "</td>";
                                    sTableData += "<td style='text-align: center;'>" + OptionData + "</td>";
                                    sTableData += "</tr>";

                                }

                            }

                            sTableData += "<tr style='border-top: 2px solid #000; border-bottom: 2px solid #000;'>";
                            sTableData += "<td colspan='2'>Total</td>";
                            sTableData += "<td style='text-align: center;'>100</td>";
                            sTableData += "<td style='text-align: center;'>" + iTotalReponse + "</td>";
                            sTableData += "</tr>";

                            sTableData += "</table>";
                            sTableData += "</td>";
                            sTableData += "<td class='GraphArea' GraphData='" + sGraphData + "' style='width:150px;'><div style='width:150px; height:150px;' id='GraphContainer" + i + "'></div></td>";
                            sTableData += "<td style='width:100px;'><textarea class='RemarkText' QueId='" + sQuestionID + "' style='position:relative;float:left; height:100%;width:100%;'>" + sQuestionRemark + "</textarea></td>";
                            sTableData += "</tr>";
                            if (((i + 1) % 4) == 0)
                            {
                                sTableData += "<tr style='page-break-after:always;'></tr>";
                            }

                        }
                        sTableData += "</table>";
                        div_que_report.InnerHtml = sTableData;
                    }


                }
            }
        }
    }
}