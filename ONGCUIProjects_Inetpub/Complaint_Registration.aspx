<%@ Page Language="c#" CodeBehind="Complaint_Registration.aspx.cs" AutoEventWireup="True"
    Inherits="WebApplication1.Complaint_Registration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
<meta content="C#" name="CODE_LANGUAGE">
<meta content="JavaScript" name="vs_defaultClientScript">
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
<script src="js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="js/jquery.gritter.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
<script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
<%--<script src="js/jquery.callout.js" type="text/javascript"></script>  comment by shiv on 27/03/2015--%>
<script type="text/javascript" src="js/our.js"></script>
<link rel="stylesheet" href="css/diggy.css?ver=4.5.3.3" type="text/css" media="screen" />
<link rel="stylesheet" href="css/jquery.callout.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="style/privilage.css" />
<link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/wave.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
<link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/colortip-1.0-jquery.css" />
<!-- Script Starts -->
<script type="text/javascript">
    $(document).ready(function () {

        $(document).ajaxStop($.unblockUI);
        $("#lblmodule_hint").hide();

        $('.forComplainRegSysFancyBox').css("width", "200px");
        var my_hint = $("#lblmodule_hint").text();
        var my_hint_arr = my_hint.split("`");

        if (my_hint_arr[0] == "NEW") {
            $("#approve_main_div").remove();
            $("#save_complain_response").remove();
            $("#clase_me").remove();
        } else if (my_hint_arr[0] == "ONLYVIEW") {
            $("#save_complain_response").remove();
            $("#register_complain").remove();
            $(".my_complaint_status").remove();
        } else if (my_hint_arr[0] == "APPROVE") {
            $("#register_complain").remove();
            $(".my_complaint_status").remove();
        } else if (my_hint_arr[0] == "UPDATE") {
            $("#approve_main_div").remove();
            $("#save_complain_response").remove();
            $(".my_complaint_status").remove();
        }


        //search applicant name
        var mainmenuSearch_options1, mainmenuSearch_a1;

        jQuery(function () {
            var options = {
                serviceUrl: 'services/auto.aspx',
                onSelect: mainmenuSearch_onAutocompleteSelect1,
                deferRequestBy: 0, //miliseconds
                params: { type: 'emp_for_guesthouse', limit: '10' },
                noCache: true //set to true, to disable caching
            };


            mainmenuSearch_a1 = $("#txtappname").autocomplete(options);

        });

        var mainmenuSearch_onAutocompleteSelect1 = function (mainmenuSearch_value1, mainmenuSearch_data11) {
            if (mainmenuSearch_data11 != -1) {
                var abcd = mainmenuSearch_data11.split("~");
                $("#txtappcpf_no").val(abcd[0]);
                $("#txtappdesign").val(abcd[1]);
                $("#txtapplocation").val(abcd[2]);
                $("#txtappphoneex_no").val(abcd[3]);
                $("#txtmobile_no").val(abcd[4]);
            } else {
                $("#txtappname").val('');
                $("#txtappcpf_no").val('');
                $("#txtappdesign").val('');
                $("#txtapplocation").val('');
                $("#txtappphoneex_no").val('');
                $("#txtmobile_no").val('');
            }
        }
        // end search of applicant name
        // START of ddl_complain_department change function
        $("#ddl_complain_department").change(function () {
            var department = $(this).val();           
            if (department != ""  && department != "ELECTRICAL(MAINTENANCE)") {
                getcomplainlist(department);
            }
            else
            {
                alert("THIS DEPARTMENT IS UNDER DEVELOPMENT! PLEASE SELECT ANOTHER DEPARTMENT");
                resetSelect();
            }
        });
        //ENd Of ddl_complain_department change function
        //get comlplaint list
        function getcomplainlist(department, complain_type) {
            $.blockUI({ message: '<h2><img src="images/busy.gif" /> Request Sending...</h2>' });
            $.ajax({
                type: "GET",
                url: "services/complain/getcomplaintypelist.aspx",

                data: "department=" + department,
               
                success: function (msg) {
                    debugger;
                    //alert(msg);
                    if (department == "TELEPHONE" || department == "CIVIL" || department == "HOUSEKEEPING" || department == "ELECTRICAL(MAINTENANCE)" || department == "WalkieTalkie" || department=="PAPaging") {
                        var select = document.getElementById('complaint_types');
                        select.style.visibility = 'hidden';
                    }
                    else {
                        var values = msg.split("~");
                        var select1 = document.getElementById('complaint_types');
                        select1.style.visibility = 'visible';
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $("#ddl_type_of_complain").html(values[3]);
                            $("#ddl_type_of_complain").val(complain_type);


                        } else if (values[0] == "FALSE" && values[1] == "ERR") {
                            $.gritter.add({
                                title: "Notification",
                                image: "images/exclamation.png",
                                text: values[2]
                            });
                        }
                    }
                }
            });
            $.blockUI({ message: '' });
            $.ajax({

                type: "GET",

                url: "services/complain/getcomplainofficername.aspx",

                data: "Department=" + department,

                success: function (msg) {
                    // alert(msg);
                    var values = msg.split("~");

                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                        $("#Div_complain_officer_detail").html(values[3]);


                    } if (values[0] == "FALSE" && values[1] == "ERR") {
                        $.gritter.add({
                            title: "Notification",
                            image: "images/exclamation.png",
                            text: values[2]
                        });


                    }

                }

            });
        }
        // send request click function
        $("#register_complain").click(function () {
            var complain_department = $("#ddl_complain_department").val();
            var complain_type = $("#ddl_type_of_complain").val();
            if (complain_department == "") {
                $.gritter.add({
                    title: "Notification",
                    text: "Please Select Complain Department."
                });
                return false;

            //} else if (complain_type == "") {
            //    $.gritter.add({
            //        title: "Notification",
            //        text: "Please Select Type of Complain ."
            //    });
            //    return false;

            }
            var app_name = $("#txtappname").val();
            var app_cpf_no = $("#txtappcpf_no").val();
            var app_design = $("#txtappdesign").val();           
            var app_location = $("#txtapplocation").val();
            var app_phone_ex_no = $("#txtappphoneex_no").val();
            var app_mob_no = $("#txtmobile_no").val();
            var complain_discription = $("#txtremarks").val(); 
            var room_no = $("#txtroom_no").val();
            if (complain_discription == "") {
                $.gritter.add({
                    title: "Notification",
                    text: "Please Fill Complain Discription First."
                });
                return false;
            }
            if (app_name == "" || app_cpf_no == "") {
                $.gritter.add({
                    title: "Notification",
                    text: "Please Search Applicant Name And Select it."
                });
                return false;
            }
            if (app_mob_no.length != 10) {
                $.gritter.add({
                    title: "Notification",
                    text: "Please Fill Complain Discription First."
                });
                return false;
            }
            $.blockUI({ message: '<h2><img src="images/busy.gif" /> Request Sending...</h2>' });
            $.ajax({

                type: "GET",

                url: "services/complain/registernewcomplain.aspx",

                data: "app_name=" + app_name + "&app_cpf_no=" + app_cpf_no + "&app_design=" + app_design + "&app_loc=" + app_location + "&app_ph_ex_no=" + app_phone_ex_no + "&app_mob_no=" + app_mob_no + "&problem_discription=" + complain_discription + "&complain_type=" + complain_type + "&complain_department=" + complain_department + "&room_no=" + room_no + "&module_hint=" + my_hint_arr[0] + "&index_no=" + my_hint_arr[1],
                success: function (msg) {
                    // alert(msg);
                    var values = msg.split("~");

                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                        $.gritter.add({
                            title: "Notification",
                            image: "images/tick.png",
                            text: values[2]
                        });

                        if (my_hint_arr[0] == "NEW") {
                            alert(values[2]);
                            window.location = self.location;
                        }

                    } else if (values[0] == "FALSE" && values[1] == "ERR") {
                        $.gritter.add({
                            title: "Notification",
                            image: "images/cross.png",
                            text: values[2]
                        });
                    }

                }

            });
        });


        $("#lbl_select_type_err").hide();
        $(document).click(function () {
            $("#lbl_select_type_err").hide();
        });
        $("#txtremarks").keydown(function () {
            var complain_department = $("#ddl_complain_department").val();
            var complain_type = $("#ddl_type_of_complain").val();
            $("#lbl_select_type_err").hide();
            if (complain_department == "") {
                $("#lbl_select_type_err").text("Please Select Complain Department.");
                $("#lbl_select_type_err").show();
                return false;

            } else if (complain_type == "") {

                $("#lbl_select_type_err").text("Please Select Type of Complain");
                $("#lbl_select_type_err").show();
                return false;

            }
            var mainmenuSearch_options11, mainmenuSearch_a11;

            jQuery(function () {

                var options = {

                    serviceUrl: 'services/auto.aspx',

                    onSelect: mainmenuSearch_onAutocompleteSelect11,

                    deferRequestBy: 0, //miliseconds

                    params: { type: 'prblm_desc', limit: '10', complain_type_id: complain_type, extra_data: complain_department },

                    noCache: true //set to true, to disable caching

                };

                mainmenuSearch_a1 = $("#txtremarks").autocomplete(options);

            });

        })
        var mainmenuSearch_onAutocompleteSelect11 = function (mainmenuSearch_value11, mainmenuSearch_data111) {
            $(".autocomplete").hide();
        }
        // refresh click

        if (my_hint_arr[0] == "UPDATE" || my_hint_arr[0] == "ONLYVIEW" || my_hint_arr[0] == "APPROVE") {

            $.ajax({
                type: "GET",
                url: "services/complain/getcompliandetailofselectedindexordepartment.aspx",
                data: "index_no=" + my_hint_arr[1],
                success: function (msg) {
                    //alert(msg);
                    var values = msg.split("~");

                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                        $("#ddl_complain_department").val(values[3]);
                        getcomplainlist(values[3], values[14]);
                        //alert(values[14]);                        
                        $("#txtappcpf_no").val(values[4]);
                        $("#txtappname").val(values[5]);
                        $("#txtappdesign").val(values[6]);
                        $("#txtappphoneex_no").val(values[7]);
                        $("#txtmobile_no").val(values[8]);
                        $("#txtapplocation").val(values[9]);
                        $("#ddl_type_of_complain").val(values[10]);
                        $("#txtroom_no").val(values[12]);
                        $("#txtremarks").val(values[11]);
                        $("#txtcomment").val(values[13]);
                        var view_response = values[13];
                       
                        $("#register_complain").val("UPDATE COMPLAINT");
                        $('input[name=optterm]').each(function () {
                            if ($(this).val() == view_response) {
                                $(this).attr('checked', 'checked');
                            }
                        });

                    } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {

                        $.gritter.add({
                            title: "Notification",
                            image: "images/cross.png",
                            text: values[2]
                        });
                        $("#theory_text").text("No Request Found");
                    } else if (values[0] == "FALSE" && values[1] == "ERR") {

                        alert(values[2]);
                    }
                }
            });
        }
        $("#close_complain").click(function () {
            $.ajax({
                type: "POST",
                url: "services/complain/updatecomplainresponse.aspx/CloseComplain",
                data: "{index_no: " + my_hint_arr[1] + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var result_json = $.parseJSON(result);
                    window.location = self.location;
                   // self.location = "ONGCMenu.aspx?p_id=21";
                  //  location.reload(ONGCMenu.aspx?p_id=21);
                   // console.log(result);
                }
            });
        });
        // save complain response
        $("#save_complain_response").click(function () {
           
            var view_response = $("#ddl_complain_response").val();
            if (view_response == "") {
                $.gritter.add({
                    title: "Notification",
                    text: "Please Select Complain Status."
                });
                return false;
            }
          
            var view_comment = $("#txtcomment").val();
            $.ajax({
                type: "GET",
                url: "services/complain/updatecomplainresponse.aspx",
                data: "update_what=" + view_comment + "&index_no=" + my_hint_arr[1] + "&view_response=" + view_response,
                success: function (msg) {
                   //alert(msg);
                    var values = msg.split("~");

                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                        $.gritter.add({
                            title: "Notification",
                            image: "images/tick.png",
                            text: values[2]
                        });

                    } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {

                        $.gritter.add({
                            title: "Notification",
                            image: "images/cross.png",
                            text: values[2]
                        });

                    } else if (values[0] == "FALSE" && values[1] == "ERR") {

                        alert(values[2]);
                    }

                }
            });

        });
        $("#clase_me").live('click', function () {
            window.location = self.location;
        });
    });

</script>
<script type="text/javascript">
    function resetSelect() {
        document.getElementById('ddl_complain_department').selectedIndex = 0;
    }
</script>
<!-- Script Ends -->
<form id="form1" runat="server" class="MainInterface-iframe">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">Complain Registration System</h1>
            <a href="complaintReg.aspx" class="g-button g-button-red my_complaint_status" type="button"
                style="float: right; margin-top: 5px; margin-right: 5px; text-decoration: none; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;">My Complain Status</a>
             <asp:label id="lblmodule_hint" runat="server" text="Label"></asp:label>

            <a href="complaintReg1.aspx" id="mgr_cmlnt" runat="server" class="g-button g-button-red my_complaint_status" type="button"
                style="float: right; margin-top: 5px; margin-right: 5px; text-decoration: none; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;">Manager Complain Status</a>
           
        </div>
        <div class="div-pagebottom">
        </div>
        <div class="div_full_width" id="div_edit_old_request" style="display: none;">
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth marginbottom">
                <h1 class="content" style="width: 220px;">Select Complaint Department</h1>                
                <select id="ddl_complain_department" style="width: 250px; padding: 5px;">
                    <option value="">Select Complaint Department </option>
                    <option value="CIVIL">Civil</option>
                    <option value="ELECTRICAL(AC)">Electrical (AC)</option>
                    <option value="ELECTRICAL(MAINTENANCE)">Electrical (Maintenance)</option>
                    <option value="HOUSEKEEPING">House Keeping</option>
                    <option value="TELEPHONE">Telephone</option>
                      <option value="WalkieTalkie">Walkie-Talkie</option>
                    <option value="PAPaging"> PA Paging System</option>
                  <%--  <option value="OTHER">OTHER</option>--%>
                </select>             
                              
            </div>
            
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth marginbottom" style="width: auto; margin-right: 20px; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Applicant Name</h1>
                <asp:textbox id="txtappname" runat="server" font-names="Verdana" class="contactdir-inputbox forComplainRegSysFancyBox autocompleteclass"
                    style="background-image: url('Images/search-icon.jpg'); background-repeat: no-repeat; padding-left: 35px; width: 250px;">
                </asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: auto; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Designation</h1>
                <%--<input id="txtappdesign" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled" />--%>
                <asp:textbox id="txtappdesign" runat="server" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled" > </asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth marginbottom" style="width: auto; margin-right: 20px; margin-top: 0;">
                <h1 class="content" style="width: 220px;">CPF No.</h1>
                <%--<input id="txtappcpf_no" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled" />--%>
                <asp:textbox id="txtappcpf_no" runat="server" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: auto; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Location</h1>
                <%--<input id="txtapplocation" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled" />--%>
                <asp:textbox id="txtapplocation" runat="server" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth marginbottom" style="width: auto; margin-right: 20px; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Phone Ex. no.</h1>
                <%--<input id="txtappphoneex_no" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled" />--%>
                <asp:textbox id="txtappphoneex_no" runat="server" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: auto; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Mobile No.</h1>
                <%--<input id="txtmobile_no" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled" />--%>
                <asp:textbox id="txtmobile_no" runat="server" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" disabled="disabled"></asp:textbox>
            </div>
        </div>
        <div id="complaint_types" class="div-fullwidth marginbottom">
            <div class="div-fullwidth marginbottom" style="width: auto; margin-right: 20px; margin-top: 0;">
                <h1 id="h_complain" class="content" style="width: 220px;">Type Of Complaint</h1>
                <select id="ddl_type_of_complain" class="forComplainRegSysFancyBox" style="width: 250px; padding: 5px;">
                </select>
                
                 <%--<asp:DropDownList  id="ddl_type_of_complain" runat="server" class="forComplainRegSysFancyBox" style="width: 250px; padding: 5px;">
                 </asp:DropDownList>--%>
            </div>           
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: auto; margin-right: 20px; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Mention Room No (if Any)</h1>
                <input id="txtroom_no" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" />
            </div>

             <div class="div-fullwidth" style="width: auto; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Problem Description</h1>
                <input id="txtremarks" value="" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;" />
            </div>
        </div>
        <div class="div-fullwidth marginbottom" id="approve_main_div">
            <div class="div-fullwidth" style="width: auto; margin-right: 20px; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Complaint Response
                </h1>
                <asp:textbox id="txtcomment" runat="server" type="text" class="contactdir-inputbox forComplainRegSysFancyBox"
                    style="width: 250px;"></asp:textbox>
            </div>
              <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: auto; margin-top: 0;">
                <h1 class="content" style="width: 220px;">Complaint Status :
                </h1>
                <select id="ddl_complain_response" style="width: 250px; padding: 5px;">
                    <option value="">Select Complaint Status </option>
                    <option value="ASSIGN">Assign</option>
                    <option value="WORK IN PROGRESS">Work in progress</option>
                    <option value="RESOLVED">Resolved</option>                   
                </select>         
               <%-- <input type="checkbox" value="SOLVED" class="webkit-me" name="optterm" />
                <p class="content" style="margin-top: -5px; text-align: right; margin-left: 5px; font-size: 30px; width: 121px;">
                    SOLVED
                </p>--%>
            </div>
           </div>
        </div>
        <div class="div-fullwidth marginbottom" style="background-color: #D1D1D1; padding: 5px; width: 98.8%; border: 1px solid #BEBCBC;">
            <input type="button" value="POST RESPONSE" class="g-button g-button-submit " style="position: relative; float: left; margin-right: 10px;"
                id="save_complain_response" />
            <input id="register_complain" style="position: relative; float: left; margin-right: 10px;"
                type="button" class="g-button g-button-share" value="REGISTER COMPLAINT" />
           <%-- <input id="close_complain" style="position: relative; float: left;" type="button" class="g-button g-button-green"
                value="Close"/>--%>
            <input id="clase_me" style="position: relative; float: left;" type="button" class="g-button g-button-red"
                value="X" />
           
        </div>
        <div id="Div_complain_officer_detail" class="div-fullwidth">
        </div>
        <div class="div-fullwidth">
            <label id="lbl_select_type_err" style="color: red">
                Please Select Complaint Type</label>
        </div>
    </div>
</form>
<script type="text/javascript" src="js/colortip-1.0-jquery.js"></script>
<script type="text/javascript" src="js/colortip.js"></script>
