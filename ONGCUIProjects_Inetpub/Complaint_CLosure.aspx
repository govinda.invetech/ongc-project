<%@ Page language="c#" Codebehind="Complaint_CLosure.aspx.cs" AutoEventWireup="True" Inherits="WebApplication1.Complaint_CLosure" %>

<!DOCTYPE html>
<HTML>
	<HEAD>
		<title>VisitorPass_Approval1</title>
			<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <script src="js/jquery-1.8.2.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>     
        <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />       
        <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
        <script src="js/jquery.callout.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/our.js"></script>
        <link rel="stylesheet" href="css/diggy.css?ver=4.5.3.3" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/jquery.callout.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style/privilage.css" />
        <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/wave.css" type="text/css" media="screen" />        
        <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />      

		<script language="javascript">
		    $(document).ready(function () {
		        $(document).ajaxStop($.unblockUI);
		        $("#lblmodule_hint_my").hide();

		        var my_hint_my = $("#lblmodule_hint_my").text();
		        var department = my_hint_my;
		        getstrip("NEW", department);           

		        $("#ddlstrip_type").change(function () {
		            var my_abc = $(this).val();
		            getstrip(my_abc, department);
		        });

		        //function 
		        function getstrip(strip_type, department) {
		            $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
		            $.ajax({
		                type: "GET",
		                url: "/services/complain/gettotalpendingcomplainlist.aspx",
		                data: "department=" + department + "&strip_type=" + strip_type,
		                success: function (msg) {
		                    //alert(msg);
		                    var values = msg.split("~");

		                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
		                        $("#request_detail_div").html(values[3]);
		                        $(".view_cmpln_dtl_fancy").fancybox({
                                modal:true
                                
                                });
		                        $("#theory_text").text(values[2]);
		                    } else if (values[0] == "FALSE" && values[1] == "ERR") {

		                        $.gritter.add({
		                            title: "Notification",
		                            image: "images/notification_icon.png",
		                            text: values[2]
		                        });
		                        $("#request_detail_div").empty();
		                        $("#theory_text").text(values[2]);
		                    }

		                }
		            });
		        }


		    });
          </script> 
        
      
      
	</HEAD>
    
	<body>
		<form id="Form1" method="post" runat="server" style="margin-right: 10px;">

          
        <div class="div-fullwidth iframeMainDiv" style="width: 99%;">
                <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                    <h1 class="heading" style="width: auto;">Complaint Closure</h1>
                    <h1 id="theory_text" class="heading-content-right" style="color: Red;"></h1>
                    <asp:Label ID="lblmodule_hint_my" runat="server" Text="Label"></asp:Label>
                </div>
                <div class="div-pagebottom"></div>
            <h1 class="gatepass-content" style="margin-top: 2px;">Select Status  Type</h1>
            <asp:DropDownList ID="ddlstrip_type" runat="server" Width="160px">
                 
                <%-- <asp:ListItem Selected="True">PENDING</asp:ListItem>--%>
                <asp:ListItem Selected="True">NEW</asp:ListItem>
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>ASSIGN</asp:ListItem>
                 <asp:ListItem>WORK IN PROGRESS</asp:ListItem> 
                 <asp:ListItem>RESOLVED</asp:ListItem>                  
            </asp:DropDownList>
            <p id="P1" class="gatepass-content" style="font-size:12px; font-family: Arial; margin-top: 10px; display:none" ></p>

      
 

<!-- Strip Container Starts ------------------------------------------------------------------------------------------->
            <div class="div-fullwidth" id="request_detail_div" style="margin-top: 10px;">
<!-- Visitor Gate Pass Strip Starts ----------------------------------------------------------------------------------->
             
<!-- Visitor Gate Pass Strip Ends ------------------------------------------------------------------------------------->

            </div>
<!-- Strip Container Ends --------------------------------------------------------------------------------------------->
						
           </div>            
		</form>
	</body>
</HTML>

