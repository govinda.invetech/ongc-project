$(function(){
		   
	// Tabs
	
	$('#tab-box').tabs(/*{ fx: { opacity: 'toggle' } }*/);
	
	$("#slider").easySlider({
		auto: true,
		continuous: true 
	});
	
	// Search Form
	
	$('input').each(function () {
		if ($(this).val() == '') {
			$(this).val($(this).attr('name'));
		}
	}).focus(function () {
		$(this).removeClass('inputerror');
		if ($(this).val() == $(this).attr('name')) {
			$(this).val('');
		}
	}).blur(function () {
		if ($(this).val() == '') {
			$(this).val($(this).attr('name'));
		}
	});
	
	// Portfolio Page - Adds an individual class to each item... i.e. the first list item = 1 the second = 2 the third = 3 etc...
	
	$("#portfolio-content li").each(function (i) {
			i = i+1;
			$(this).addClass("portfolio-item"+i);
	});
	
	// Portfolio Page - When you hover, the description pops up

	$('.portfolio-description').css("display", "none");
	
	$('.portfolio-description').animate({
		"opacity" : "0.7"
	});
	
	$('.portfolio-item').hover(function(){
		$(this).children('.portfolio-description').slideDown();
	}, function(){
		$(this).children('.portfolio-description').slideUp();
	});

});