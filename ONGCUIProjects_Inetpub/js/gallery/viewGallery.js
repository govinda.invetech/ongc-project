$(document).ready(function () {
    $('#divCreateFolder').hide();
    $('.docClass').hide();
    $('.links').hide();


    $('#btnimage').on('click', function () {
        loadImage();
    });

    function loadImage() {
        var linksContainer = $('.docchildClass');
        linksContainer.empty();
        var tablecondensed = $('#tbodyFolderContainer');
        tablecondensed.empty();

        $('#divCreateFolder').hide();
        $('.docClass').hide();

        var colorArr = [];
        colorArr[0] = "active";
        colorArr[1] = "success";
        colorArr[2] = "warning";
        colorArr[3] = "danger";
        colorArr[4] = "info";

        $('.links').show();


        $('#tbodyFolderContainer').show();

        var folderContainer = $('#tbodyFolderContainer');
        var trfolder = $('.trFolder');
        $.ajax({
            data: '{Type:"images"}',
            type: "POST",
            url: "UpdateGallery.aspx/GetImagesFolder",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ParsedJson = jQuery.parseJSON(response.d);
                folderContainer.empty();
                for (var i = 0; i < ParsedJson.folder.length; i++) {
                    trFolder = trfolder.clone(true, true);
                    var foldername = ParsedJson.folder[i].folderName;
                    trFolder = trfolder.clone(true, true);
                    trFolder.find('.txtFolderName').hide();
                    trFolder.find('.folderName').text(foldername);
                    trFolder.find('.folderImage').attr('name', foldername);
                    trFolder.find('.txtFolderName').val(foldername);
                    trFolder.find('.cmdEditFolderName').attr('name', foldername);
                    trFolder.find('.cmdFolderDelete').attr('name', foldername);
                    trFolder.attr("class", colorArr[i]);
                    folderContainer.append(trFolder);
                }
            }
        });
    }

    
    $('#btnHideNew').on('click', function (event) {
        $('#divCreateFolder').hide(1300);
    });

    $('#create-folder-button').on('click', function (event) {
        $('.docchildClass').hide();
        $('#divCreateFolder').show(1300);
    });

    $('#borderless-checkbox').on('change', function () {
        var borderless = $(this).is(':checked');
        $('#blueimp-gallery').data('useBootstrapModal', !borderless);
        $('#blueimp-gallery').toggleClass('blueimp-gallery-controls', borderless);
    });

    $('#fullscreen-checkbox').on('change', function () {
        $('#blueimp-gallery').data('fullScreen', $(this).is(':checked'));
    });

    $('.folderImage').on('click', function (event) {
        var linksContainer = $('.links');
        $.ajax({
            data: '{Folder:"' + $(this).attr('name') + '"}',
            type: "POST",
            url: "UpdateGallery.aspx/GetImagesFromFolder",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ParsedJson = jQuery.parseJSON(response.d);
                linksContainer.empty();
                $.each(ParsedJson.photo, function (index, photo) {
                    baseUrl = photo.baseUrl;
                    $('<div/>')
                    .append($('<button>').prop('type', "button").attr('baseurl', baseUrl).prop('class', ""))
                        .append($('<a>').prop('href', baseUrl).prop('title', photo.title).attr('data-gallery', ''))
                        .append($('<img>').prop('src', baseUrl).prop('class', "img-thumbnail"))
                        .prop('class', "col-xs-6 col-md-3")
                        .appendTo(linksContainer);
                });
                event.preventDefault();
                blueimp.Gallery($('.links a'), $('#blueimp-gallery').data());
                $('#blueimp-gallery').data('fullScreen', $(this).is(':checked'));
            }
        });
    });

    $('#image-doc-button').on('click', function (event) {
        var linksContainer = $('.docchildClass');
        linksContainer.empty();

        $('.docchildClass').show();
        $('#divCreateFolder').hide();
        $('.docClass').hide();
        // Load demo images from flickr:
        $.ajax({
            data: '{Type:"images"}',
            type: "POST",
            url: "UpdateGallery.aspx/GetDocFromFolder",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ParsedJson = jQuery.parseJSON(response.d);
                var i = 0;
                $.each(ParsedJson.photo, function (index, photo) {
                    i = i + 1;
                    $('<h5 style = "color:white">' + i + '. &nbsp <a href="' + photo.baseUrl + '"  target="_Blank"    title="' + photo.title + '"  ><blink>Click here for download   ' + photo.title + '</blink></a></h5>').appendTo(linksContainer);
                });
               
            }
        });
        $('.docClass').show();
        $('.links').hide();
        $('#tbodyFolderContainer').hide();

    });



});



