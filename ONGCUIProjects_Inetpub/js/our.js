               
function salutation() {
    message=new Date();
    h=message.getHours();
    var var1="";
    if((h < 12) && (h >= 6))
    {
        var1="Hi! Good Morning ";
    }
    if((h >= 12) && (h < 18))
    {
        var1="Hi! Good Afternoon ";
    };  
    if((h >= 18) && (h <= 23))
    {
        var1="Good evening ";
    };  
    if((h >= 0) && (h < 4))
    {
        var1="It's getting late ";
    }
    if((h >= 4) && (h <= 6))
    {
        var1="Nothing like getting up early! Hi ";
    }
    $(".salutation").text(var1);
}
                
function showGritter(tittleTag,msgToShow) {
    
        
    var tittle="";
    if (tittleTag=="ERR")
    {
        tittle="ERROR Occurred !";
    }
    else
    {
        tittle="Notification !";                         
    }
    
    $.extend($.gritter.options, { 
        position: 'bottom-left' // defaults to 'top-right' but can be 'bottom-left', 'bottom-right', 'top-left', 'top-right' (added in 1.7.1)
    //fade_in_speed: 'medium', // how fast notifications fade in (string or int)
    //fade_out_speed: 2000, // how fast the notices fade out
    //time: 6000 // hang on the screen for...
    });
    
    $.gritter.add({
                    
        title: tittle,
                    
        text: msgToShow
    });
}



function zeroPad(num,count)
{
    var numZeropad = num + '';
    while(numZeropad.length < count) {
        numZeropad = "0" + numZeropad;
    }
    return numZeropad;
}
                
                
                
function getPngName($card_type)
{
    $return_value="";
                    
    if ($card_name=="RED")
    {
        $return_value="red";
    }
    else if ($card_name=="BLUE")
    {
        $return_value="blue";
    }
    else if ($card_name=="PINK")
    {
        $return_value="pink";
    }
    else if ($card_name=="YELLOW")
    {
        $return_value="yellow";
    }
                    
    return $return_value;
                    
}    // END of function getPngName


function ImagePathStudent($adm_no,$school_code)
{
    $str="http://www.mountcarmeldelhi.com/getphoto.php?adm_no=" + $adm_no + "&sch_code=" + $school_code;
    
    return $str;
}

function OpenStudentProfile($adm_no)
{
    $str="Studentprofile.php?adm_no=" + $adm_no;
    
    return $str;
}

function ImagePathStaff($staff_id,$school_code)
{
    $str="http://www.mountcarmeldelhi.com/getphoto.php?staff_id=" + $staff_id + "&sch_code=" + $school_code;
    
    return $str;
}

function OpenStaffProfile($staff_id)
{
    $str="StaffProfile.php?staff_id=" + $staff_id;
    
    return $str;
}

function OpenStudentProfileKids($adm_no)
{
    $str="../../Studentprofile.php?adm_no=" + $adm_no;
    
    return $str;
}