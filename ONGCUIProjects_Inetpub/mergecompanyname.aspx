﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mergecompanyname.aspx.cs"
    Inherits="ONGCUIProjects.mergecompanyname" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/script.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('select.mergewith').each(function () {
                $(this).val($(this).parent('td').siblings('td[mytype="NAMETD"]').text());
            });

            $('select.mergewith').change(function () {
                $(this).parent('td').siblings('td[mytype="ACTIONTD"]').children('a').attr('newname', $(this).val());
            });

            $('a.cmdUpdate').click(function () {
                var PreviousName = $(this).attr('previousname');
                var NewName = $(this).attr('newname');
                $.ajax({
                    type: "GET",
                    url: "services/UpdateCompanyName.aspx",
                    data: "previousname=" + PreviousName + "&newname=" + NewName,             
                    success: function (msg) {
                        // alert(msg);
                        var values = msg.split("~");
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                            window.location = self.location;
                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                        }
                    }
                });
            });
        });
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server" class="MainInterface-iframe">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 runat="server" id="HeaderToShow" class="heading" style="width: auto;">
                Manage Visitor Company Name
            </h1>
        </div>
        <div class="div-pagebottom">
        </div>
        <div runat="server" class="div-fullwidth" id="div_master_list" style="margin: 10px 0px;">          
        </div>
    </div>
    </form>
</body>
</html>
