﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ONGCUIProjects
{
    public partial class Gate_Pass_History : System.Web.UI.Page
    {

        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            FUN();
        }

        public void FUN()
        {
            if (Session["Login_Name"] == null)
            {

            }
            else
            {

                string sSessionCPF = Session["Login_Name"].ToString();
                string sQuery = "SELECT DISTINCT([CY_GROUP]),[O_MTNG_OFF_NM],[O_MTNG_OFF_DESG],[O_ENTRY_DT],[O_SYS_DT_TM] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [O_APLCNT_CPF_NMBR] = '" + sSessionCPF + "' ORDER BY [O_SYS_DT_TM] DESC";
                DataSet dsRequestDetails = My.ExecuteSELECTQuery(sQuery);

                if (dsRequestDetails.Tables[0].Rows.Count == 0)
                {

                }
                else
                {
                    string sOutput = "";
                    for (int i = 0; i < dsRequestDetails.Tables[0].Rows.Count; i++)
                    {

                        string sGroupID = dsRequestDetails.Tables[0].Rows[i][0].ToString();
                        string sMeetingOfficerName = dsRequestDetails.Tables[0].Rows[i][1].ToString();
                        string sMeetingOfficerDesig = dsRequestDetails.Tables[0].Rows[i][2].ToString();
                        DateTime dtEntryDate = Convert.ToDateTime(dsRequestDetails.Tables[0].Rows[i][3]);

                        string sVisitorQuery = "";
                        if (ddlType.SelectedValue.ToString() == "ALL")
                        {
                            sVisitorQuery += " SELECT [O_VISTR_NM],[O_VISTR_ORGNZTN],[O_VISTR_AGE],[O_VISTR_GNDR],[O_PRPS_OF_VISIT],[O_PASS_TYPE_FLG],[O_REQ_COMPLTN_FLG],[CY_WITH_TOOL],[O_INDX_NMBR],[O_ACP1_CPF_NMBR] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [CY_GROUP] = '" + sGroupID + "' ";
                        }
                        else if (ddlType.SelectedValue.ToString() == "P")
                        {
                            sVisitorQuery += " SELECT [O_VISTR_NM],[O_VISTR_ORGNZTN],[O_VISTR_AGE],[O_VISTR_GNDR],[O_PRPS_OF_VISIT],[O_PASS_TYPE_FLG],[O_REQ_COMPLTN_FLG],[CY_WITH_TOOL],[O_INDX_NMBR],[O_ACP1_CPF_NMBR] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [CY_GROUP] = '" + sGroupID + "' and [O_REQ_COMPLTN_FLG] IN ('" + ddlType.SelectedValue.ToString() + "','I')";
                        }
                        else
                        {
                            sVisitorQuery += " SELECT [O_VISTR_NM],[O_VISTR_ORGNZTN],[O_VISTR_AGE],[O_VISTR_GNDR],[O_PRPS_OF_VISIT],[O_PASS_TYPE_FLG],[O_REQ_COMPLTN_FLG],[CY_WITH_TOOL],[O_INDX_NMBR],[O_ACP1_CPF_NMBR] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [CY_GROUP] = '" + sGroupID + "' and [O_REQ_COMPLTN_FLG]='" + ddlType.SelectedValue.ToString() + "'";
                        }
                        DataSet dsVisitorDetails = My.ExecuteSELECTQuery(sVisitorQuery);

                        sOutput += "<div g_n='" + sGroupID + "' class='div-fullwidth marginbottom gatepass-approvalbox' style='padding: 10px; width: 98%;'>";
                        sOutput += "<h1 class='content-gatepass-approval count'>" + (i + 1) + "</h1>";
                        sOutput += "<div class='div-fullwidth' style='width: auto;'>";
                        sOutput += "<div class='divider'></div>";
                        sOutput += "<h1 class='content-gatepass-approval' style='margin-top: 10px;'><span>Whom to Meet</span> : <a title='Desig : " + sMeetingOfficerDesig + "' href='javascript:void(0);'>" + sMeetingOfficerName + "</a></h1>";
                        sOutput += "</div>";
                        sOutput += "<div class='div-fullwidth' style='width: auto;'>";
                        sOutput += "<div class='divider'></div>";
                        sOutput += "<h1 class='content-gatepass-approval' style='margin-top: 10px;'><span>Visiting Date</span> : " + dtEntryDate.ToString("dd-MMM-yyyy") + "</h1>";
                        sOutput += "</div>";
                        sOutput += "<div class='div-fullwidth' style='width: auto;'>";
                        sOutput += "<div class='divider'></div>";
                        sOutput += "<h1 class='content-gatepass-approval' style='margin-top: 10px;'><span>Total Visitor</span> : " + dsVisitorDetails.Tables[0].Rows.Count + "</h1>";
                        sOutput += "</div>";

                        if (CheckEditable(sGroupID) == "TRUE")
                        {
                            sOutput += "<a href='Gate_pass_Requisition.aspx?GroupID=" + sGroupID + "' class='home' style='margin-top:0px;'>EDIT</a>";
                        }

                        if (dsVisitorDetails.Tables[0].Rows.Count != 0)
                        {
                            sOutput += "<table id='tfhover' class='tftable' border='1' style='margin-left: 10px; margin-top: 10px; margin-right: 10px; width: 98%;'>";
                            sOutput += "<tr>";
                            sOutput += "<th>Visitor's Name</th>";
                            sOutput += "<th style='width: 40px;'>Age</th>";
                            sOutput += "<th style='width: 50px;'>Gender</th>";
                            sOutput += "<th style='width: 140px;'>Organization</th>";
                            sOutput += "<th style='width: 150px;'>Purpose Of Visit</th>";
                            sOutput += "<th style='width: 150px;'>Tools</th>";
                            sOutput += "<th style='width: 70px;'>Status</th>";
                            sOutput += "</tr>";
                            for (int j = 0; j < dsVisitorDetails.Tables[0].Rows.Count; j++)
                            {
                                string sVisitorName = dsVisitorDetails.Tables[0].Rows[j][0].ToString();
                                string sVisitorOrg = dsVisitorDetails.Tables[0].Rows[j][1].ToString();
                                string sVisitorAge = dsVisitorDetails.Tables[0].Rows[j][2].ToString();
                                string sVisitorGender = dsVisitorDetails.Tables[0].Rows[j][3].ToString() == "M" ? "Male" : "Female";
                                string sPurposeOfVisit = dsVisitorDetails.Tables[0].Rows[j]["O_PRPS_OF_VISIT"].ToString(); 
                                string sPassType = dsVisitorDetails.Tables[0].Rows[j][5].ToString();
                                string sPassStatusTemp = dsVisitorDetails.Tables[0].Rows[j][6].ToString();


                                string sPassStatus = (sPassStatusTemp == "A") ? "Pending" : (sPassStatusTemp == "P") ? "Approved" :(sPassStatusTemp=="I")? "Approved" : "Rejected";
                                string sTools = dsVisitorDetails.Tables[0].Rows[j][7].ToString();
                                string O_INDX_NMBR = dsVisitorDetails.Tables[0].Rows[j]["O_INDX_NMBR"].ToString();
                                string CPF_NMBR = dsVisitorDetails.Tables[0].Rows[j]["O_ACP1_CPF_NMBR"].ToString();// User Id

                                sOutput += "<tr>";
                                sOutput += "<td>" + sVisitorName + "</td>";
                                sOutput += "<td>" + sVisitorAge + "</td>";
                                sOutput += "<td>" + sVisitorGender + "</td>";
                                sOutput += "<td>" + sVisitorOrg + "</td>";
                                sOutput += "<td>" + sPurposeOfVisit + "</td>";
                                sOutput += "<td>" + sTools + "</td>";
                                sOutput += "<td>" + sPassStatus + "</td>";
                                if (sPassStatusTemp == "R")
                                    sOutput += "<td><input  type='button'  class='g-button g-button-share cmdedit'  value='Approve' groupid=" + sGroupID + "  index=" + O_INDX_NMBR + "  cpfNo=" + CPF_NMBR + "  /></td>";
                                sOutput += "</tr>";
                            }
                            sOutput += "</table>";
                        }
                        sOutput += "</div>";
                    }
                    div_gate_pass_details.InnerHtml = sOutput;
                }
            }
        }
        public string CheckEditable(string GroupID)/*--if one of entry is approve then edit button is not shown--*/                  //p
        {
            /*      A-Pending
                    P-Approved
                    R-Rejected   */

            string sQuery = "SELECT [O_VISTR_NM] FROM [O_VSTR_GATE_PASS_RQSTN_DTL] WHERE [O_PASS_TYPE_FLG] = 'V' AND ( [O_REQ_COMPLTN_FLG] = 'R' OR [O_REQ_COMPLTN_FLG] = 'A') AND [CY_GROUP] = '" + GroupID + "'"; /*--Those who approved---*/
            DataSet ds = My.ExecuteSELECTQuery(sQuery);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return "TRUE";// Create edit button
            }
            else
            {
                return "FALSE";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FUN();
        }


        [System.Web.Services.WebMethod]
        public static string UpdateStatus(string INDEX, string GROUPID, string CPF_NO)
        {
            string str = " ";
            str += "  UPDATE [O_VSTR_GATE_PASS_RQSTN_DTL] SET O_REQ_COMPLTN_FLG = 'P'                                         ";
            //str += "  UPDATE [O_VSTR_GATE_PASS_RQSTN_DTL] SET O_REQ_COMPLTN_FLG = 'R'                                         ";
            str += "  WHERE O_PASS_TYPE_FLG ='V' AND O_REQ_COMPLTN_FLG = 'R'                                                  ";
            str += "  and O_ACP1_CPF_NMBR = '" + CPF_NO + "' and CY_GROUP='" + GROUPID + "' and O_INDX_NMBR='" + INDEX + "'   ";

            try
            {
                if (MyApplication1.ExecuteSQLQuery_static(str) == true)
                {
                    return "{\"STATUS\":\"TRUE\",\"MESSAGE\":\"Approve has been  done.\"}";
                }

                else
                    return "{\"STATUS\":\"ERR\",\"MESSAGE\":\"Error in Execution . \"}";
            }
            catch (Exception e)
            {
                return "{\"STATUS\":\"ERR\",\"MESSAGE\":\"" + e.Message + "\"}";
            }
        }
    }
}