using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ONGCCustumEntity;
using ONGCBusinessProjects;


namespace WebApplication1
{
	public partial class Employee_Master : System.Web.UI.Page
	{
		#region Variables
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		string EString;
		string EmpCount;
		#endregion

		#region PageLoad
		protected void Page_Load(object sender, System.EventArgs e)
		{
            cboUnapproveEmployeeName.SelectedIndexChanged += new EventHandler (cboUnapproveEmployeeName_SelectedIndexChanged);
			BtnSaveEmpData.Attributes.Add("onclick","return ConfirmDelete()");
			if(!IsPostBack)
			{
				BindEmpDdl();
				BindLocation();
				BindDesignation();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Check Employee CPF Exists
		private void CheckEmpCPFExists()
		{
			try
			{
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select count(*) as Count_Rec from O_EMP_MSTR where O_CPF_NMBR='"+txtEmpCPFNo.Text+"'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				if(sqlDataReader.Read())
				{
					EmpCount = sqlDataReader["Count_Rec"].ToString();
				}
				sqlDataReader.Close();
				sqlConnection.Close();
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Button Click for Creating New Employee
		protected void BtnSaveEmpData_Click(object sender, System.EventArgs e)
		{
			CheckEmpCPFExists();

			//if(cmbBldGrp.SelectedItem.Text=="Select Blood Group")
			//{
			//	Response.Write("<Script language='JavaScript'>"+"alert('Please Select The Correct Blood Group')"+"</Script>");
			//}
			//else if(CmbDD_DOB.SelectedItem.Text=="DD")
			//{
			//	Response.Write("<Script language='JavaScript'>"+"alert('Please Choose the Correct Date of Birth (DD)')"+"</Script>");
			//}
			//else if(cmbMM_DOB.SelectedItem.Text=="MM")
			//{
			//	Response.Write("<Script language='JavaScript'>"+"alert('Please Choose the Correct Date of Birth (MM)')"+"</Script>");
			//}
			//else if(cmbYY_DOB.SelectedItem.Text=="YYYY")
			//{
			//	Response.Write("<Script language='JavaScript'>"+"alert('Please Choose the Correct Date of Birth (YYYY)')"+"</Script>");
			//}
			//else if(cmbSupName.SelectedItem.Text=="Select Supervisor")
			//{
			//	Response.Write("<Script language='JavaScript'>"+"alert('Please Choose the Correct Name of the employee supervisor')"+"</Script>");
			//}
			//else if(cmbLocation.SelectedItem.Text=="Select Location")
			//{
			//	Response.Write("<Script language='JavaScript'>"+"alert('Please Choose the Correct Work Location of the employee')"+"</Script>");
			//}
			//else if(cmbDesg.SelectedItem.Text=="Select Designation")
			//{
			//	Response.Write("<Script language='JavaScript'>"+"alert('Please Choose the Correct Designation of the employee')"+"</Script>");
			//}
			if(EmpCount != "0")
			{
				Response.Write("<Script language='JavaScript'>"+"alert('This Employee already exists in the database. Cannot Create a New Entry for the same CPF Number')"+"</Script>");
			}
			else
			{
                
				InsertRecord();
				BlankTxtBox();
			}
		}
		#endregion

		#region Insert Record
		private void InsertRecord()
		{
			try
			{
				ONGCBusinessProjects.ONGC_EmpMstr objBusEmployeeMst	= new ONGCBusinessProjects.ONGC_EmpMstr();
				ONGCCustumEntity.ONGC_EmpMstr objCusEmployeeMst		= new ONGCCustumEntity.ONGC_EmpMstr();
				objCusEmployeeMst.o_CPF_NMBR						= txtEmpCPFNo.Text;
				objCusEmployeeMst.o_EMP_NM							= txtEmpName.Text;
				objCusEmployeeMst.o_EMP_HM_ADDR1					= txtAddr1.Text;
				objCusEmployeeMst.o_EMP_HM_ADDR2					= txtAddr2.Text;
				objCusEmployeeMst.o_EMP_HM_CITY						= txtCity.Text;
				objCusEmployeeMst.o_EMP_HM_STATE					= txtState.Text;
				objCusEmployeeMst.o_EMP_HM_PIN_CD					= txtPinCode.Text;
				objCusEmployeeMst.o_EMP_HM_PHN						= txtHomeNo.Text;
				objCusEmployeeMst.o_EMP_MBL_NMBR					= txtMblNo.Text;
				objCusEmployeeMst.o_EMP_GNDR						= CmbGender.SelectedValue.ToString();
				objCusEmployeeMst.o_EMP_DESGNTN						= cmbDesg.SelectedItem.Text;
				objCusEmployeeMst.o_EMP_DOB							= cmbMM_DOB.SelectedItem.Text+"/"+CmbDD_DOB.SelectedItem.Text+"/"+cmbYY_DOB.SelectedItem.Text;
				objCusEmployeeMst.o_EMP_SUP_NM						= cmbSupName.SelectedItem.Text;
				objCusEmployeeMst.o_EMP_DEPT_NM						= txtDept.Text;
				objCusEmployeeMst.o_EMP_WRK_LCTN					= cmbLocation.SelectedItem.Text;
				objCusEmployeeMst.o_EMP_BLD_GRP						= cmbBldGrp.SelectedItem.Text;
				// objCusEmployeeMst.o_USER_CODE						= Session["Login_Name"].ToString();
				objCusEmployeeMst.o_BIT_CD							= "1";
				objCusEmployeeMst.o_UPDATE_BIT						= "0";
				objCusEmployeeMst.o_LEVEL							= txtLevel.Text;
				objCusEmployeeMst.o_REP								= cmbRep.SelectedItem.Text;
				objCusEmployeeMst.o_REV								= cmbRev.SelectedItem.Text;
				objCusEmployeeMst.o_MGR								= cmbMan.SelectedItem.Text;
				objCusEmployeeMst.o_ACP_1							= cmbAcp1.SelectedItem.Text;
				objCusEmployeeMst.o_ACP_2							= cmbAcp2.SelectedItem.Text;
				objCusEmployeeMst.o_EMAIL_ID1						= txtEmail1.Text;
				objCusEmployeeMst.o_EMAIL_ID2						= txtEmail2.Text;
				objCusEmployeeMst.o_EMAIL_ID3						= txtEmail3.Text;
				bool Message = (bool)objBusEmployeeMst.InsertEmployeeMst(objCusEmployeeMst);
				if(Message == true)
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Records SuccessFully Saved')"+"</Script>");
				}
				else
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Role Already exist')"+"</Script>");
				}
			}
			catch(Exception ex)
			{
				EString = ex.ToString();
			}
		}
		#endregion

		#region BlankTextBox
		private void BlankTxtBox()
		{
			txtEmpName.Text="";
			txtEmpCPFNo.Text="";
			txtAddr1.Text="";
			txtAddr2.Text="";
			txtCity.Text="";
			txtState.Text="";
			txtPinCode.Text="";
			txtHomeNo.Text="";
			txtMblNo.Text="";
			CmbGender.SelectedIndex=0;
			cmbDesg.SelectedIndex=0;
			CmbDD_DOB.SelectedIndex=0;
			cmbMM_DOB.SelectedIndex=0;
			cmbYY_DOB.SelectedIndex=0;
			cmbSupName.SelectedIndex=0;
			txtDept.Text="";
			cmbLocation.SelectedIndex=0;
			cmbBldGrp.SelectedIndex=0;
			txtEmail1.Text="";
			txtEmail2.Text="";
			txtEmail3.Text="";
			txtLevel.Text="";
			cmbRep.SelectedIndex=0;
			cmbRev.SelectedIndex=0;
			cmbMan.SelectedIndex=0;
			cmbAcp1.SelectedIndex=0;
			cmbAcp2.SelectedIndex=0;
		}
		#endregion

		#region Bind Employee Dropdown List
		public void BindEmpDdl()
		{
			/*try
			{
				DataSet dsEmployeeDtl=new DataSet();
				ONGCBusinessProjects.TransportBooking objbusFillEmpDdl		    = new ONGCBusinessProjects.TransportBooking();
				dsEmployeeDtl=objbusFillEmpDdl.fillEmployeeDdl();
				cmbSupName.DataSource=dsEmployeeDtl;
				cmbSupName.DataMember=dsEmployeeDtl.Tables[0].ToString();
				cmbSupName.DataValueField=dsEmployeeDtl.Tables[0].Columns["O_INDX_NMBR"].ToString();
				cmbSupName.DataTextField=dsEmployeeDtl.Tables[0].Columns["O_EMP_NM"].ToString();
				cmbSupName.DataBind();
				cmbSupName.Items.Insert(0,"Select Supervisor");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
             */
            try
            {
                DataSet dsEmployeeDtl = new DataSet();
                ONGCBusinessProjects.TransportBooking objbusFillEmpDdl = new ONGCBusinessProjects.TransportBooking();
                dsEmployeeDtl = objbusFillEmpDdl.fillPendingEmployeesList();
                cboUnapproveEmployeeName.DataSource = dsEmployeeDtl;
                cboUnapproveEmployeeName.DataMember = dsEmployeeDtl.Tables[0].ToString();
                cboUnapproveEmployeeName.DataValueField = dsEmployeeDtl.Tables[0].Columns["O_CPF_NMBR"].ToString();
                cboUnapproveEmployeeName.DataTextField = dsEmployeeDtl.Tables[0].Columns["O_EMP_NM"].ToString();
                cboUnapproveEmployeeName.DataBind();
                cboUnapproveEmployeeName.Items.Insert(0, "Select Employee Name");
            }
            catch (Exception ex)
            {
                EString = ex.ToString();
            }
		}
		#endregion

		#region Bind Location Dropdown List
		public void BindLocation()
		{
			try
			{
				DataSet dsLocation=new DataSet();
				ONGCBusinessProjects.TransportBooking objbusFillLocation		    = new ONGCBusinessProjects.TransportBooking();
				dsLocation=objbusFillLocation.fillLocationDdl();
				cmbLocation.DataSource=dsLocation;
				cmbLocation.DataMember=dsLocation.Tables[0].ToString();
				cmbLocation.DataValueField=dsLocation.Tables[0].Columns["O_INDX_NMBR"].ToString();
				cmbLocation.DataTextField=dsLocation.Tables[0].Columns["O_LCTN_NM"].ToString();
				cmbLocation.DataBind();
				cmbLocation.Items.Insert(0,"Select Location");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Bind Designation Dropdown List
		public void BindDesignation()
		{
			try
			{
				DataSet dsDesignation=new DataSet();
				ONGCBusinessProjects.TransportBooking objbusFillDesignation		    = new ONGCBusinessProjects.TransportBooking();
				dsDesignation=objbusFillDesignation.fillDesignationDdl();
				cmbDesg.DataSource=dsDesignation;
				cmbDesg.DataMember=dsDesignation.Tables[0].ToString();
				cmbDesg.DataValueField=dsDesignation.Tables[0].Columns["O_INDX_NMBR"].ToString();
				cmbDesg.DataTextField=dsDesignation.Tables[0].Columns["O_DSG_DTL"].ToString();
				cmbDesg.DataBind();
				cmbDesg.Items.Insert(0,"Select Designation");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

        protected void cboUnapproveEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string j;
            j = "";
        }

        

	}
}
