﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ONGCCustumEntity;
using ONGCBusinessProjects;
using System.Data.SqlClient;

namespace ONGCUIProjects
{
    public partial class _default : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();

        #region Variables
        string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
        int iExpiryPeriod = 60;
        #endregion

        #region PageLoad
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (Request.Form.Count > 0)
            {
                txtName.Text = Request.Form["txtloginid"];
                txtPwd.Text = Request.Form["txtpassword"];
                btnLogin_Click(sender, e);
            }
            else
            {
                Response.Redirect("Index.aspx?from=default");
                if (!IsPostBack)
                {
                    rowrole.Visible = false;
                    rowmessage.Visible = false;
                    rowrelogin.Visible = false;
                    btnLoginRole.Visible = false;
                    tblChangepass.Visible = false;
                }
                else
                {
                    txtPwd.Text = "admin";
                    string j = "";
                }
            }
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        #region Private Function For Login Page Only

        private Boolean IsFirstLogin(string UserID)
        {
            string sQuery = "SELECT TOP 1 [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [USER_ID] = '" + UserID + "' AND [TYPE] = 'SUCCESS'";
            DataSet ds = my.ExecuteSELECTQuery(sQuery);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Boolean InsertLoginRecord(string UserID, string Pswd, string Type)
        {
            string QueryString = "INSERT INTO [CY_LOGIN_LOGS] ([USER_ID],[USER_PWD],[TYPE]) VALUES ('" + UserID + "','" + Pswd + "','" + Type + "')";
            return my.ExecuteSQLQuery(QueryString);
        }

        private string CheckLoginDetails(string UserID, string Pswd)
        {
            string sStr = "SELECT [E_USER_CODE],[E_USER_PSWRD],[CY_STATUS] FROM [E_USER_MSTR] WHERE [E_USER_CODE] = '" + UserID + "' AND [E_USER_PSWRD]='" + Pswd + "'";
            DataSet ds = my.ExecuteSELECTQuery(sStr);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                return ds.Tables[0].Rows[0][2].ToString();
            }
        }

        private void DoLoginSuccessStep(String sUid, String sPwd)
        {
            InsertLoginRecord(sUid, sPwd, "SUCCESS");
            Session["Login_Name"] = sUid;
            Session["EmployeeName"] = my.GetEmployeeNameFromCPFNo(sUid);
            Response.Redirect("launchpage.aspx", false);
        }

        private int CheckLastLogin(string UserID)
        {
            DateTime dt1 = CheckEmpLastLogin(UserID);
            DateTime dt2 = DateTime.Today;

            DateTime dtLastLogin = new DateTime(dt1.Year, dt1.Month, dt1.Day);
            DateTime dtTodayDate = new DateTime(dt2.Year, dt2.Month, dt2.Day);
            TimeSpan t = dtTodayDate - dtLastLogin;
            int iDaysDiff = System.Convert.ToInt32(t.TotalDays);
            return iDaysDiff;
        }

        private void DoExceededCaseWorkAsStatus(string Status,string UserID)
        {
            Response.Redirect("AcctReactivation.aspx?error=" + Status + "&cpf=" + UserID + "", false);
        }

        public DateTime CheckEmpLastLogin(string sCPFNo)
        {
            //23/11/2012, 11:27 AM
            DateTime dtLastLogin;
            string sQuery = "SELECT TOP 1 [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [USER_ID] = '" + sCPFNo + "' AND [TYPE] = 'SUCCESS' ORDER BY [TIMESTAMP] DESC";
            DataSet dsLastLogin = my.ExecuteSELECTQuery(sQuery);
            if (dsLastLogin.Tables[0].Rows.Count == 0)
            {
                dtLastLogin = DateTime.Now;
            }
            else
            {
                dtLastLogin = System.Convert.ToDateTime(dsLastLogin.Tables[0].Rows[0][0]);
            }

            return dtLastLogin;
        }
        #endregion

        #region buttonLogin Click
        protected void btnLogin_Click(object sender, System.EventArgs e)
        {
            try
            {
                String sUserId, sPwd;
                sUserId = txtName.Text.Trim(); ;
                sPwd = txtPwd.Text;
                if (sUserId == "WORLDISROUND")
                {//If Activation Authority Acccount is expired
                    Session["Login_Name"] = "worldisround";
                    Response.Redirect("ActivateRejectAccount.aspx", false);
                }
                else
                {
                    Boolean InsertRecord = InsertLoginRecord(txtName.Text, txtPwd.Text, "TRIAL");
                    string sCheckCredential = CheckLoginDetails(sUserId, sPwd);
                    if (sCheckCredential == "FALSE")
                    { //login Id pass mismatch
                        InsertLoginRecord(txtName.Text, txtPwd.Text, "MISMATCH");
                        Response.Redirect("Index.aspx?error=login&message=Username or password mismatch.", false);
                    }
                    else
                    {//Login Id pass matched
                        // now check all the points like exceeded 90 days, deactivate his/her account ect etc...
                        int iLoginDiff = CheckLastLogin(sUserId);  // gives no of days difference ex. gives 85 days
                       // if (iLoginDiff > iExpiryPeriod)  //  85>90 (no) 
                        if(true)
                        {    //exceeded case
                            // means user loggined again today but more than 90 days
                            
                            //if (sCheckCredential == "ADMINACTIVE")
                            if (true)
                            {
                                my.InsUpdEmpAccountStatus(sUserId, "ACTIVE");
                                if (IsFirstLogin(sUserId) == true)
                                {
                                    Session["Login_Name"] = sUserId;
                                    Response.Redirect("MandatoryPasswordChange.aspx", false);
                                }
                                else
                                {
                                    DoLoginSuccessStep(sUserId, sPwd);
                                }
                            }
                            else
                            {
                                string sAccountStatus = my.sAccountStatus(sUserId);
                                DoExceededCaseWorkAsStatus(sAccountStatus, sUserId);
                            }
                        }
                        else
                        {//normal login case means user is trying with in 90 days, let him login normally

                            if (sCheckCredential == "ADMINDEACTIVE")
                            {
                                Response.Redirect("Index.aspx?error=login&message=Your account has been deactivated by Adminstrator", false);
                            }
                            else if (sCheckCredential == "ADMINACTIVE")
                            {
                                my.InsUpdEmpAccountStatus(sUserId, "ACTIVE");
                                if (IsFirstLogin(sUserId) == true)
                                {
                                    Session["Login_Name"] = sUserId;
                                    Response.Redirect("MandatoryPasswordChange.aspx", false);
                                }
                                else
                                {
                                    DoLoginSuccessStep(sUserId, sPwd);
                                }
                            }
                            else if (sCheckCredential == "ACTIVE")
                            {
                                if (IsFirstLogin(sUserId) == true)
                                {
                                    Session["Login_Name"] = sUserId;
                                    Response.Redirect("MandatoryPasswordChange.aspx", false);
                                }
                                else
                                {
                                    DoLoginSuccessStep(sUserId, sPwd);
                                }
                            }
                            else
                            {
                                DoExceededCaseWorkAsStatus(sCheckCredential, sUserId);
                            }

                        } //else case of 90 days if case
                    }// else of logindetails IF case
                }
            }
            catch (Exception ex) { Response.Redirect("Index.aspx?error=DB&message=" + ex.Message.ToString()); }
        }
        #endregion

        protected void cmdRegisterMe_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewUserReg.aspx?my_hint=NEW");

        }
    }
}