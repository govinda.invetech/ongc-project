using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace WebApplication1
{
	public partial class Safety_Measure_Master : System.Web.UI.Page
	{

		#region Variables
		string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
		string EString;
		#endregion

		#region Page Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BtnSaveSftyMeasure.Attributes.Add("onclick","return ConfirmDelete()");
			if(!IsPostBack)
			{
				BindSafetyReasons();
			}
		}
		#endregion

		#region Insert Record
		private void InsertRecord()
		{
			try
			{
				ONGCBusinessProjects.ONGC_Mstr objBusSftyMeasure	= new ONGCBusinessProjects.ONGC_Mstr();
				ONGCCustumEntity.ONGC_Mstr objCusSftyMeasure		= new ONGCCustumEntity.ONGC_Mstr();
				objCusSftyMeasure.o_SFTY_MSR						= txtSftyMeasure.Text;
				bool Message = (bool)objBusSftyMeasure.InsertSftyMeasure(objCusSftyMeasure);
				if(Message == true)
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Records SuccessFully Saved')"+"</Script>");
				}
				else
				{
					Response.Write("<Script language='JavaScript'>"+"alert('Role Already exist')"+"</Script>");
				}
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region BlankTextBox
		private void BlankTxtBox()
		{
			txtSftyMeasure.Text="";
		}
		#endregion
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Button Click - Save Data
		protected void BtnSaveSftyMeasure_Click(object sender, System.EventArgs e)
		{
			InsertRecord();
			BlankTxtBox();
		}
		#endregion

		#region SafetyReasons
		private void BindSafetyReasons()
		{
			try
			{
				DataSet dsSafetyReasons=new DataSet();
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("select O_INDX_NMBR,O_SFTY_MSR from O_SFTY_PRCTN_MSTR order by O_INDX_NMBR",sqlConnection);
				SqlDataAdapter sda =new SqlDataAdapter(sqlCommand);
				sda.Fill(dsSafetyReasons);
				ddlSafetyMeasures.DataSource=dsSafetyReasons;
				ddlSafetyMeasures.DataMember=dsSafetyReasons.Tables[0].ToString();
				ddlSafetyMeasures.DataValueField=dsSafetyReasons.Tables[0].Columns["O_INDX_NMBR"].ToString();
				ddlSafetyMeasures.DataTextField=dsSafetyReasons.Tables[0].Columns["O_SFTY_MSR"].ToString();
				ddlSafetyMeasures.DataBind();
				ddlSafetyMeasures.Items.Insert(0,"Select Safety Measures");
				
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
		}
		#endregion

		#region Button Click - Delete Safety Measures
		protected void btnDeleteMaster_Click(object sender, System.EventArgs e)
		{
			try
			{
				// Step 1 - Delete Safety / Precaution Resons From the Master 
				SqlConnection sqlConnection=new SqlConnection(strConn);
				sqlConnection.Open();
				SqlCommand sqlCommand=new SqlCommand("Delete From O_SFTY_PRCTN_MSTR where O_SFTY_MSR = '" + ddlSafetyMeasures.SelectedItem.Text + "'",sqlConnection);
				SqlDataReader sqlDataReader=sqlCommand.ExecuteReader();
				sqlDataReader.Close();
				sqlConnection.Close();

				Response.Write("<Script language='JavaScript'>"+"alert('Safety Measure : "+ ddlSafetyMeasures.SelectedItem.Text +" Deleted Sucessfully')"+"</Script>");
			}
			catch(Exception ex)
			{
				EString=ex.ToString();
			}
			BindSafetyReasons();
		}
		#endregion
		
		}

	}

