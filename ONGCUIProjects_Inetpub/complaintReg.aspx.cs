﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace ONGCUIProjects
{
    public partial class complaintReg : System.Web.UI.Page
    {
        MyApplication1 my = new MyApplication1();
        string Departments = "";
        string cpf_no = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            cpf_no = Session["Login_Name"].ToString();
            if (!IsPostBack)
            {
                string stry = "update [CY_COMPLAIN_REGISTER_DETAIL] set notification_status='YES' where APP_CPF_NO='" + cpf_no + "' and VIEWED_RESPONSE='RESOLVED' ";
                my.updateQuery(stry);
            }
            string str = "";           
            
            //string str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where [ENTRY_BY]='" + cpf_no + "' ORDER BY [TIMESTAMP] DESC";
            if (Session["Login_Name"].ToString() == "ADMIN" || Session["Login_Name"].ToString() == "admin")
            {
                str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] ORDER BY [TIMESTAMP] DESC";
                ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
                DataSet ds1 = My1.ExecuteSELECTQuery(str);
                if (!IsPostBack)
                {
                    grdComplaintRegistration.DataSource = ds1;
                    grdComplaintRegistration.DataBind();
                    request_detail_div.Visible = true;
                    request_detail_div1.Visible = false;
                    Div1.Visible = false;
                }
            }
            else
            {

                string query = "";
                query = "select [child_id] from [CY_MENU_EMP_RELATION] where [cpf_number]='" + cpf_no + "' and child_id in('62','63','64','65','67','77')";
                ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
                DataSet ds = My.ExecuteSELECTQuery(query);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {

                    if (ds.Tables[0].Rows[0]["child_id"].ToString() == "62")
                    {
                        Departments = "TELEPHONE";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "63")
                    {
                        Departments = "ELECTRICAL(AC)";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "64")
                    {
                        Departments = "CIVIL";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "77")
                    {
                        Departments = "ELECTRICAL(MAINTENANCE)";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "65")
                    {
                        Departments = "HOUSEKEEPING";
                    }
                    else if (ds.Tables[0].Rows[0]["child_id"].ToString() == "67")
                    {
                        Departments = "OTHER";
                    }
                    else
                    {
                        Departments = "";
                    }

                }
                if (Departments != "")
                {
                    str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,VIEW_TIMESTAMP ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where [APP_CPF_NO]='" + cpf_no + "' ORDER BY [TIMESTAMP] DESC";
                    ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
                    DataSet ds1 = My1.ExecuteSELECTQuery(str);
                    if (!IsPostBack)
                    {
                        GridView1.DataSource = ds1;
                        GridView1.DataBind();
                        Div1.Visible = true;
                        request_detail_div.Visible = false;
                        request_detail_div1.Visible = false;
                    }
                }
                else
                {
                    str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,VIEW_TIMESTAMP,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where   APP_CPF_NO='" + cpf_no + "'   ORDER BY [TIMESTAMP] DESC";
                    ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
                    DataSet ds1 = My1.ExecuteSELECTQuery(str);
                    if (!IsPostBack)
                    {
                        GridView2.DataSource = ds1;
                        GridView2.DataBind();
                        request_detail_div1.Visible = true;
                        request_detail_div.Visible = false;
                        Div1.Visible = false;
                    }
                }
            }                    
            //ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
            //DataSet ds1 = My1.ExecuteSELECTQuery(str);
            //grdComplaintRegistration.DataSource = ds1;
            //grdComplaintRegistration.DataBind();
           
        }
        protected void grdComplaintRegistration_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           // DataRowView drview = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatus=(Label)e.Row.FindControl("lblComplaintStatus");
                Label lblId=(Label)e.Row.FindControl("lblId");
                HyperLink hylEdit=(HyperLink)e.Row.FindControl("hylEditComplaints");
                if (lblStatus.Text == "NEW" && Session["Login_Name"].ToString() == "ADMIN" || Session["Login_Name"].ToString() == "admin" || Departments == "TELEPHONE"
                    || Departments == "ELECTRICAL(AC)" || Departments == "CIVIL" || Departments == "ELECTRICAL(MAINTENANCE)" || Departments == "HOUSEKEEPING" || Departments == "OTHER")
                    hylEdit.NavigateUrl = "Complaint_Registration.aspx?my_hint=UPDATE`" + lblId.Text + "";
                else
                    hylEdit.NavigateUrl = "Complaint_Registration.aspx?my_hint=ONLYVIEW`" + lblId.Text + "";
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatus = (Label)e.Row.FindControl("lblComplaintStatus");
                Label lblId = (Label)e.Row.FindControl("lblId");
                HyperLink hylEdit = (HyperLink)e.Row.FindControl("hylEditComplaints");
                if (lblStatus.Text == "NEW" && Session["Login_Name"].ToString() == "ADMIN" || Session["Login_Name"].ToString() == "admin" || Departments == "TELEPHONE"
                    || Departments == "ELECTRICAL(AC)" || Departments == "CIVIL" || Departments == "ELECTRICAL(MAINTENANCE)" || Departments == "HOUSEKEEPING" || Departments == "OTHER")
                    hylEdit.NavigateUrl = "Complaint_Registration.aspx?my_hint=UPDATE`" + lblId.Text + "";
                //else
                    //hylEdit.NavigateUrl = "Complaint_Registration.aspx?my_hint=ONLYVIEW`" + lblId.Text + "";
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    DropDownList ddl_status_edit = (DropDownList)e.Row.FindControl("ddl_status");
                    ddl_status_edit.DataSource = BindStatus();
                    ddl_status_edit.DataValueField = "VIEWED_RESPONSE";
                    ddl_status_edit.DataTextField = "VIEWED_RESPONSE";
                    ddl_status_edit.DataBind();
                    ddl_status_edit.Items.Add(new ListItem( "--select status--", "0",true));
                    DataRowView dr = e.Row.DataItem as DataRowView;
                    ddl_status_edit.SelectedValue = dr["VIEWED_RESPONSE"].ToString();

                }
            }
        }
        public static DataTable BindStatus()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("VIEWED_RESPONSE");
            dt.Rows.Add("NEW");
            dt.Rows.Add("ASSIGN");
            dt.Rows.Add("WORK IN PROGRESS");
            dt.Rows.Add("RESOLVED");
            return dt;
            
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                //string userid = GridView1.DataKeys[e.RowIndex].Value.ToString();               
                GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
                Label lblID = (Label)row.FindControl("lblId");
                DropDownList ddlstatus = (DropDownList)row.FindControl("ddl_status");
                GridView1.EditIndex = -1;
                if (ddlstatus.SelectedValue != "")
                {
                    string qeries = "update CY_COMPLAIN_REGISTER_DETAIL set VIEWED_RESPONSE='" + ddlstatus.SelectedValue + "',VIEW_TIMESTAMP='" + DateTime.Now.ToString() +"' where ID='" + lblID.Text + "' ";
                    ONGCUIProjects.MyApplication1 My1 = new ONGCUIProjects.MyApplication1();
                    bool res = My1.updateQuery(qeries);
                    if (res)
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Status has been updated successfully.');", true);
                }
                string str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where   [APP_CPF_NO]='" + cpf_no + "' ORDER BY [TIMESTAMP] DESC";
                    ONGCUIProjects.MyApplication1 My2 = new ONGCUIProjects.MyApplication1();
                    DataSet ds1 = My2.ExecuteSELECTQuery(str);                   
                        GridView1.DataSource = ds1;
                        GridView1.DataBind();
                        Div1.Visible = true;
                        request_detail_div.Visible = false;
                        request_detail_div1.Visible = false;
               
            }
            catch(Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Please try again,Error massage " + ex.Message + "');", true);
            }

        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            string str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where   [APP_CPF_NO]='" + cpf_no + "' ORDER BY [TIMESTAMP] DESC";
            ONGCUIProjects.MyApplication1 My2 = new ONGCUIProjects.MyApplication1();
            DataSet ds1 = My2.ExecuteSELECTQuery(str);
            GridView1.DataSource = ds1;
            GridView1.DataBind();
            Div1.Visible = true;
            request_detail_div.Visible = false;
            request_detail_div1.Visible = false;
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            string str = "SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[APP_PHONE_EX_NO] ,[APP_MOBILE_NO] ,[APP_LOCATION],[COMPLAIN_DEPARTMENT],[COMPLAIN_TYPE_ID] ,[PROBLEM_DISCRIPTION] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[TIMESTAMP] ,[ROOM_NO] FROM [CY_COMPLAIN_REGISTER_DETAIL] where   [APP_CPF_NO]='" + cpf_no + "' ORDER BY [TIMESTAMP] DESC";
            ONGCUIProjects.MyApplication1 My2 = new ONGCUIProjects.MyApplication1();
            DataSet ds1 = My2.ExecuteSELECTQuery(str);
            GridView1.DataSource = ds1;
            GridView1.DataBind();
            Div1.Visible = true;
            request_detail_div.Visible = false;
            request_detail_div1.Visible = false;
        }
    }
}