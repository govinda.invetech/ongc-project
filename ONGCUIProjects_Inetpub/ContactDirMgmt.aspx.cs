﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace ONGCUIProjects
{
    public partial class ContactDirMgmt : System.Web.UI.Page
    {
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();

        protected void Page_Load(object sender, EventArgs e)
        {
            cmdDeleteContact.Visible = false;
            if (Session["Login_Name"] == null)
            {

                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            else
            {
                string cpf_no = Session["Login_Name"].ToString();

                string sContactDept = Request.QueryString["ContactDept"];

                string ContactID = Request.QueryString["ContactID"];

                #region Get Total Contact Count in DB
                lblDBTotal.Text = " (Total " + My.GetTotalContactCount() + " Contacts)";
                #endregion
                string sSelectedDesig = "";
                string sSelectedDept = "";
                #region Get Contact Detail From Contact ID
                if (ContactID != null)
                {
                    SaveNewContact.Value = "UPDATE";
                    cmdDeleteContact.Attributes.Add("contactid", ContactID);
                    SaveNewContact.Attributes.Add("contactid", ContactID);
                    txtCPFNo.Attributes.Add("disabled", "disabled");
                    if (My.sDataStringTrim(2, ContactID) == "REGEMP")
                    {
                        txtName.Attributes.Add("disabled", "disabled");
                        txtEmail.Attributes.Add("disabled", "disabled");
                        txtEmail2.Attributes.Add("disabled", "disabled");
                        cmdDeleteContact.Visible = false;
                    }
                    else
                    {
                        cmdDeleteContact.Visible = true;
                    }

                    //string sQueryString = "SELECT [id],[name],[desig],[dept_name],[extn],[resi_d],[off_d],[mobile],[email],[email_2],[contact_type] FROM [CY_CONTACT_DTLS] WHERE [id] = '" + My.sDataStringTrim(1, ContactID) + "' AND [contact_type]= '" + My.sDataStringTrim(2, ContactID) + "'";
                    string sQueryString = "SELECT [ID],[NAME],[DESIG],[DEPARTMENT],[EXTN],[RES],[OFF],[MOBILE],[EMAIL],[EMAIL2],[TYPE] FROM [CY_VIEW_CONTACTS] WHERE [ID] = '" + My.sDataStringTrim(1, ContactID) + "' AND [TYPE]= '" + My.sDataStringTrim(2, ContactID) + "'";
                    DataSet dsContact = My.ExecuteSELECTQuery(sQueryString);

                    txtCPFNo.Value = dsContact.Tables[0].Rows[0][0].ToString();
                    txtName.Value = dsContact.Tables[0].Rows[0][1].ToString();
                    txtExtn.Value = dsContact.Tables[0].Rows[0][4].ToString();
                    txtResi.Value = dsContact.Tables[0].Rows[0][5].ToString();
                    txtOff.Value = dsContact.Tables[0].Rows[0][6].ToString();
                    txtMob.Value = dsContact.Tables[0].Rows[0][7].ToString();
                    txtEmail.Value = dsContact.Tables[0].Rows[0][8].ToString();
                    txtEmail2.Value = dsContact.Tables[0].Rows[0][9].ToString();

                    sSelectedDesig = dsContact.Tables[0].Rows[0][2].ToString();
                    sSelectedDept = dsContact.Tables[0].Rows[0][3].ToString();
                    txtContactTypeToAdd.Value = dsContact.Tables[0].Rows[0][10].ToString();

                }
                #endregion

                #region Get Department List
                try
                {
                    string sStr1 = "SELECT COUNT([ID]),[DEPARTMENT] FROM [CY_VIEW_CONTACTS] GROUP BY [DEPARTMENT] ORDER BY [DEPARTMENT] ASC";
                    DataSet ContactCatDS = new DataSet();
                    ContactCatDS = My.ExecuteSELECTQuery(sStr1);

                    string sHtml = "<select id=\"contact_category_select\" class=\"contactdir-select\" >";
                    sHtml = sHtml + "<option class=\"contactdir-select option\" value='0'>SELECT DEPARTMENT</option>";
                    if (ContactCatDS.Tables[0].Rows.Count != 0)
                    {
                        for (int i = 0; i < ContactCatDS.Tables[0].Rows.Count; i++)
                        {
                            string sCategoryName = ContactCatDS.Tables[0].Rows[i][1].ToString();
                            string sCategoryValue=ContactCatDS.Tables[0].Rows[i][1].ToString();
                            if (sCategoryName == "")
                            {
                                sCategoryName = "UN-CATEGORISED";
                                sCategoryValue = "";
                            }
                            string selected = "";
                            if (sContactDept == sCategoryName)
                            {
                                selected = "selected";
                            }
                            sHtml = sHtml + "<option class='contactdir-select option' value='" + sCategoryValue + "' " + selected + ">" + sCategoryName + " - [" + ContactCatDS.Tables[0].Rows[i][0].ToString() + "]</option>";
                        }
                    }
                    sHtml = sHtml + "</select>";
                    divContactCategorySelect.InnerHtml = sHtml;

                }
                catch (Exception a)
                {
                    Response.Write("FALSE~ERR~" + a.Message);
                } //  try catch ends
                #endregion

                #region Get Category Contact List
                if (sContactDept == "0" || sContactDept == null)
                {
                    //AddEditDivContainer.InnerHtml = "<h1 runat='server' id='AddEditHeading' class='heading' style='font-size: 25px;'>To Add/Edit Contact Select Department</h1>";
                    //DivCategoryContact.Visible = false;
                }
                else
                {
                    try
                    {
                        string sStr2 = "";
                        sStr2 = "SELECT [ID],[NAME],[DESIG],[EXTN],[RES],[OFF],[MOBILE],[EMAIL],[EMAIL2],[TYPE] FROM [CY_VIEW_CONTACTS] WHERE [DEPARTMENT] = '" + sContactDept + "' ORDER BY [NAME]";
                        DataSet ContactListDS = My.ExecuteSELECTQuery(sStr2);
                        string sContactList = "";
                        if (ContactListDS.Tables[0].Rows.Count == 0)
                        {
                            sContactList = "<p class='content' style='width: 100%; text-align: center; color: red'>No Contact in category</p>";
                        }
                        else
                        {
                            sContactList = "<ul style='padding-left: 0;'>";
                            for (int i = 0; i < ContactListDS.Tables[0].Rows.Count; i++)
                            {
                                string sContactID = ContactListDS.Tables[0].Rows[i][0].ToString();
                                string sContactName = ContactListDS.Tables[0].Rows[i][1].ToString();
                                string sDesignation = ContactListDS.Tables[0].Rows[i][2].ToString();
                                string sContactExtn = ContactListDS.Tables[0].Rows[i][3].ToString();
                                string sContactResi = ContactListDS.Tables[0].Rows[i][4].ToString();
                                string sContactOff = ContactListDS.Tables[0].Rows[i][5].ToString();
                                string sContactMob = ContactListDS.Tables[0].Rows[i][6].ToString();
                                string sContactEmail = ContactListDS.Tables[0].Rows[i][7].ToString();
                                string sContactEmail2 = ContactListDS.Tables[0].Rows[i][8].ToString();
                                string sContactType = ContactListDS.Tables[0].Rows[i][9].ToString();

                                sContactList += "<li class='contact-list' ContactID='" + sContactID + '~' + sContactType + "' >";
                                if (sContactType == "NONEMP" || sContactType == "LOCATION")
                                {
                                    sContactList += "<a class=\"cmd-delete-contact deleteContact\" href=\"javascript:void(0);\"></a>";
                                    sContactList += "<a class=\"cmd-edit-contact editContact\" href=\"javascript:void(0);\"></a>";
                                }
                                else
                                {
                                    sContactList += "<a class=\"cmd-edit-contact editContact\" href=\"javascript:void(0);\"></a>";
                                }
                                sContactList += "<p class=\"contact-heading-p-name\">" + sContactName + "</p>";
                                if (sContactType != "LOCATION")
                                {
                                    sContactList += "<div class='div-fullwidth' style='padding: 2px 5px;'><p class='contact-p-head'>Designation<span style='float:right;'>:</span></p><p class='contact-p-data'>" + sDesignation + "</p></div>";
                                }
                                sContactList += "<div class='div-halfwidth' style='padding: 2px 5px; width:48%;'><p class='contact-p-head'>Extn No<span style='float:right;'>:</span></p><p class='contact-p-data'>" + sContactExtn + "</p></div>";
                                sContactList += "<div class='div-halfwidth' style='padding: 2px 5px; width:47%;'><p class='contact-p-head'>Office<span style='float:right;'>:</span></p><p class='contact-p-data'>" + sContactOff + "</p></div>";
                                if (sContactType != "LOCATION")
                                {
                                    sContactList += "<div class='div-halfwidth' style='padding: 2px 5px; width:48%;'><p class='contact-p-head'>Residence<span style='float:right;'>:</span></p><p class='contact-p-data'>" + sContactResi + "</p></div>";
                                    sContactList += "<div class='div-halfwidth' style='padding: 2px 5px; width:47%;'><p class='contact-p-head'>Mobile<span style='float:right;'>:</span></p><p class='contact-p-data'>" + sContactMob + "</p></div>";
                                    sContactList += "<div class='div-fullwidth' style='padding: 2px 5px;'><p class='contact-p-head'>ONGC Mail<span style='float:right;'>:</span></p><p class='contact-p-data'>" + sContactEmail + "</p></div>";
                                    sContactList += "<div class='div-fullwidth' style='padding: 2px 5px;'><p class='contact-p-head'>Other Email<span style='float:right;'>:</span></p><p class='contact-p-data'>" + sContactEmail2 + "</p></div>";
                                }
                                sContactList += "</li>";

                            }
                            sContactList = sContactList + "</ul>";
                        }
                        DivCategoryContact.InnerHtml = sContactList;
                    }
                    catch (Exception a)
                    {
                        Response.Write("FALSE~ERR~" + a.Message);
                    }
                }
                #endregion

                #region Designation List
                string sStr = "SELECT [O_DSG_DTL]  FROM [O_DSG_MSTR] where [O_DSG_DTL]<>'' ORDER BY [O_DSG_DTL] ASC";
                DataSet ds = My.ExecuteSELECTQuery(sStr);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    string sDesigList = "<option value=''>Select Designation</option>";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string sDesig = ds.Tables[0].Rows[i][0].ToString().Trim();
                        string selected = "";
                        if (sSelectedDesig == sDesig)
                        {
                            selected = "selected";
                        }
                        sDesigList += "<option value='" + sDesig + "'" + selected + ">" + sDesig + "</option>";
                    }
                    designation_div.InnerHtml = "<h1 class='content' style='margin-right: 2px; width: 95px;'>Designation</h1><select id='txtDesignation' class='flattxt' style='width:352px;'>" + sDesigList + "</select>";
                }
                #endregion

                #region Department List for Add
                string sDeptQuery = "SELECT [id], [name] FROM [CY_DEPT_DTLS] ORDER BY [name] ASC";
                DataSet dsDepartment = My.ExecuteSELECTQuery(sDeptQuery);
                if (dsDepartment.Tables[0].Rows.Count != 0)
                {
                    string sDeptList = "";
                    sDeptList = "<option value='0'>Select Department</option>";
                    for (int i = 0; i < dsDepartment.Tables[0].Rows.Count; i++)
                    {
                        string sDept = dsDepartment.Tables[0].Rows[i][1].ToString().Trim();
                        string selected = "";
                        if (sSelectedDept == sDept)
                        {
                            selected = "selected";
                        }
                        sDeptList += "<option value='" + sDept + "'" + selected + ">" + sDept + "</option>";
                    }
                    department_div.InnerHtml = "<h1 class='content' style='margin-right: 2px; width: 95px;'>Department</h1><select id='txtDepartment' class='flattxt' style='width:352px;'>" + sDeptList + "</select>";
                }
                #endregion

            }
        }
    }
}