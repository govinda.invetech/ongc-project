<%@ Page language="c#" Codebehind="View_TC_Meeting.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.View_TC_Meeting" %>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<title>View_TC_Meeting</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
         <script src="js/jquery-1.8.2.js"></script>   
         <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>  
        <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />        
        <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
	
		<script language="javascript">
		    $(document).ready(function () {
		        $(document).ajaxStop($.unblockUI);
		        $("#closed_table").hide();
		        $("#open_table").hide();
		        $.blockUI({ message: '<h4><img src="images/busy.gif" /> Please wait...</h4>' });
		        $.ajax({
		            type: "GET",
		            url: "services/meeting/viewmettings.aspx",
		            data: "",
		            success: function (msg) {
		                //alert(msg);
		                $("#closed_table").show();
		                $("#open_table").show();
		                var values = msg.split("~");

		                if (values[0] == "TRUE" && values[1] == "SUCCESS") {
		                    if (values[3] == "FALSE") {
		                        $("#lblopenmeeting").text("No Open Meeting Found");

		                    } else {
		                        $("#open_table").html(values[3]);
		                        $("#lblopenmeeting").text("Total " + values[4] + " Open Meeting Found");

		                    }
		                    if (values[5] == "FALSE") {
		                        $("#lblclosedmeeting").text("No Closed Meeting Found");

		                    } else {
		                        $("#closed_table").html(values[5]);
		                        $("#lblclosedmeeting").text("Total " + values[6]+" Closed Meeting Found");
		                    }
		                } else if (values[0] == "FALSE" && values[1] == "ERR") {
		                    $.gritter.add({
		                        title: "Notification",

		                        text: values[2]
		                    });
		                } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
		                    $.gritter.add({
		                        title: "Notification",

		                        text: values[2]
		                    });
		                }

		            }
		        });
		        //view detail click function
		        $("#view_detail").live('click', function () {
		            var meeting_no = $(this).attr('m_n');
		            $.ajax({
		                type: "GET",
		                //url: "services/meeting/viewmeetingdetailinfancybox.aspx",
		                url: "services/meeting/downloadpdf.aspx",
		                data: "meeting_no=" + meeting_no,
		                success: function (msg) {
		                    //alert(msg);
		                    //$.fancybox(msg);
                            alert("Download Completed at Desktop")
		                }
		            });
		        });
		    });
                </script>
                  
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
            <div class="div-fullwidth iframeMainDiv">
            <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                    <h1 class="heading" style="width: auto;">Tender Meeting Schedules</h1>
                    <h1 id="theory_text" class="heading-content-right" style="color: Red;"></h1>
            </div>
            <div class="div-pagebottom"></div>

            <p id="lblopenmeeting" class="content" style="width: 100%; color: Green; margin-bottom: 10px;">Open Meeting</p>
			<div id="open_table" style="position:relative; float:left; width: 100%; margin-bottom: 20px;"></div>

            <p id="lblclosedmeeting" class="content" style="width: 100%;color:Red; margin-bottom: 10px;">Closed Meeting</p>
			<div  id="closed_table"style="position:relative; float:left; width: 100%;"></div>

            </div>
		</form>
	</body>
</HTML>
