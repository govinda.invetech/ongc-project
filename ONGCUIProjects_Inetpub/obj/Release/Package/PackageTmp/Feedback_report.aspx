<%@ Page Language="c#" CodeBehind="Feedback_report.aspx.cs" AutoEventWireup="True"
    Inherits="ONGCUIProjects.Feedback_report" ValidateRequest="false" %>

<!DOCTYPE html>
<html>
<head id="Head1">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtdatefrom").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            $("#txtdateto").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            var DivWindowHeight = parseInt($(window).height()) - 104;
            $('#report_container').css('max-height', DivWindowHeight + 'px');
        });
    </script>
    <style type="text/css">
        th {
            background-color: Silver;
        }
    </style>
</head>
<body bgcolor="#BCC7D8">
    <form id="form1" runat="server" class="MainInterface-iframe">
        <div class="div-fullwidth iframeMainDiv">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid #009f3c;">
                <h1 class="heading" style="width: auto;" id="txtHeading">Feedback Reports</h1>
                <asp:Label ID="Label2" runat="server" class="heading-content-right" Style="color: Red;"></asp:Label>
            </div>
            <div class="div-pagebottom">
            </div>
            <div class="div-fullwidth marginbottom">

                <asp:Label ID="Label3" runat="server" Text="Report Type" class=" content"></asp:Label>
                <asp:DropDownList ID="ddltypeofreport" runat="server" CssClass="div-fullwidth"
                    Style="width: auto; margin-right: 10px;" AutoPostBack="True"
                    OnSelectedIndexChanged="ddltypeofreport_SelectedIndexChanged">
                    <asp:ListItem Value="">Select Type</asp:ListItem>
                    <asp:ListItem Value="DATEWISE">Date wise</asp:ListItem>                   
                    <asp:ListItem Value="AVERAGE REPORT">Average Report</asp:ListItem>

                </asp:DropDownList>
                <asp:Panel ID="pnlArea" runat="server" CssClass="div-fullwidth" Style="width: auto;" Visible="False">
                    <asp:Label ID="Label4" runat="server" Text="Area" class=" content"></asp:Label>
                    <asp:DropDownList ID="ddlservice" runat="server" Style="width: auto;">
                    </asp:DropDownList>
                </asp:Panel>

                <asp:Panel ID="pnlDateWise" runat="server" CssClass="div-fullwidth" Style="width: auto;" Visible="False">
                    <div style="position: relative; float: left; margin-left: 1px; width: auto; top: 0px; left: 0px;">
                        <asp:Label ID="eto" runat="server" Text="From Date" CssClass="content"></asp:Label>
                        &nbsp;
                        <asp:TextBox ID="txtdatefrom" runat="server" Width="100px" placeholder="From Date"
                            Text=""></asp:TextBox>
                    </div>
                    <div style="position: relative; float: left; margin-left: 1px; width: auto; top: 0px; left: 0px;">
                        <asp:Label ID="Label1" runat="server" Text="To Date" CssClass="content"></asp:Label>
                        &nbsp;
                        <asp:TextBox ID="txtdateto" runat="server" Width="100px" placeholder="To Date" Text=""></asp:TextBox>
                    </div>

                  
                </asp:Panel>


                 

                <asp:Button ID="cmdExporttoExcel" runat="server" Text="Export To Excel" Style="position: relative; float: right; margin-left: 10px"
                   
                    CssClass="g-button g-button-submit" OnClick="cmdExporttoExcel_Click1" />
                <asp:Button ID="date_wise_report" runat="server" Text="Generate" CssClass="g-button g-button-submit" OnClick="date_wise_report_Click" />
                  
               
            </div>
            <asp:Panel ID="panel_date" runat="server" Visible="false">
                <div style="overflow:scroll; width:100%;">
                 <asp:GridView ID="gv_feedback_report" runat="server" CssClass="IncTable">  
                              
                <%--  <Columns>
                       <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label runat="server" Font-Bold="true" Text="SR. No."></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_sr" AutoPostBack="true" Text='<%#Container.DataItemIndex+1%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>


                       <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Emp_Name/Cpf_No"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_emp_cpf" runat="server" Text='<%#Eval("Emp_Name/Cpf_No")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="EPABX Phone-Office"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_EPABX" runat="server" Text='<%#Eval("EPABX Phone-Office")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Landline Office"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Landline" runat="server" Text='<%#Eval("Landline Office")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Mobile-Services"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Mobile" runat="server" Text='<%#Eval("Mobile-Services")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Datacard"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Data" runat="server" Text='<%#Eval("Datacard")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Internet"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Inte" runat="server" Text='<%#Eval("Internet")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="ONGC Reports"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_ONGC" runat="server" Text='<%#Eval("ONGC Reports")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="SAP"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_S" runat="server" Text='<%#Eval("SAP")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Webice"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Webi" runat="server" Text='<%#Eval("Webice")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Lotus Client"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Lotus" runat="server" Text='<%#Eval("Lotus Client")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Lotus WebMail"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_WebMail" runat="server" Text='<%#Eval("Lotus WebMail")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Lotus SameTime"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_SameTime" runat="server" Text='<%#Eval("Lotus SameTime")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="IBM Connections/Myspace"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_IBM" runat="server" Text='<%#Eval("IBM Connections/Myspace")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Computer Performance"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Performance" runat="server" Text='<%#Eval("Computer Performance")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Printer"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Printer" runat="server" Text='<%#Eval("Printer")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="IT Maintenance Support"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_IT" runat="server" Text='<%#Eval("IT Maintenance Support")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                       <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="SCADA System"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_SCADA" runat="server" Text='<%#Eval("SCADA System")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                       <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Video Conferencing"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Video" runat="server" Text='<%#Eval("Video Conferencing")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                       <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="FAX Services"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_FAX" runat="server" Text='<%#Eval("FAX Services")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                       <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="VHF/Walkie Talkie"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Walkie" runat="server" Text='<%#Eval("VHF/Walkie Talkie")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                       <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="HIS"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_HI" runat="server" Text='<%#Eval("HIS")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="PA System"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_System" runat="server" Text='<%#Eval("PA System")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Audio Visual Systems"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Visual" runat="server" Text='<%#Eval("Audio Visual Systems")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="EPABX-Residence"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_EPABX" runat="server" Text='<%#Eval("EPABX-Residence")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Landline Residence"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Residence" runat="server" Text='<%#Eval("Landline Residence")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Broadband"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Broad" runat="server" Text='<%#Eval("Broadband")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server"     Font-Bold="true" Text="Any Other"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_Any" runat="server" Text='<%#Eval("Any Other")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                  </Columns>--%>
                </asp:GridView>
                 </div>
            </asp:Panel>
            

             <div runat="server" id="div_average" style="overflow: auto; width:100%; ">
              <asp:GridView ID="gv_average" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="IncTable">
                  <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                               <center><asp:Label runat="server" Font-Bold="true" Text="SR. No."></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbl_index" AutoPostBack="true" Text='<%#Container.DataItemIndex+1%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>


                       <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server" Font-Bold="true" Text="Services"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="text-align:center;"><asp:Label ID="lblStn_names" runat="server" Text='<%#Eval("NAME")%>'></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                       <HeaderTemplate>
                                <center><asp:Label runat="server" Font-Bold="true" Text="Average Services"></asp:Label></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="text-align:center;"><asp:Label ID="lblStn_averages" runat="server" Text='<%#Eval("rate")%>'></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                  </Columns>
                  </asp:GridView>
                 </div>
        </div>
    </form>
</body>
</html>
