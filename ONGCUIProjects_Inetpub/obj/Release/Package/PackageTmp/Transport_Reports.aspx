<%@ Page language="c#" Codebehind="Transport_Reports.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Transport_Reports" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Transport_Reports</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<FORM id="Form1" method="post" runat="server">
			<P align="center"><STRONG><FONT face="Verdana" color="maroon" size="4"><U>Transport Management 
							Reports</U></FONT></STRONG></P>
			<STRONG><FONT face="Verdana" color="#800000" size="2">
					<P align="left">
						<TABLE id="Table3" style="WIDTH: 626px; HEIGHT: 48px" cellSpacing="8" cellPadding="4" width="626"
							align="center" bgColor="whitesmoke" border="1">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 68px" align="center" colSpan="3"><FONT face="Verdana" color="#660000"><STRONG><FONT face="Verdana" color="#660000"><STRONG>
														<P align="left"><asp:checkbox id="ChkDateWise" runat="server" Width="568px" Font-Size="XX-Small" Font-Names="Verdana"
																Text="Date Wise Transport Booking Report Selection" AutoPostBack="True" oncheckedchanged="ChkDateWise_CheckedChanged"></asp:checkbox></P>
														<P align="left"><FONT size="1">From Date: </FONT>
															<asp:dropdownlist id="cmbStart_DD" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
																Height="20px">
																<asp:ListItem>DD</asp:ListItem>
																<asp:ListItem Value="01">01</asp:ListItem>
																<asp:ListItem Value="02">02</asp:ListItem>
																<asp:ListItem Value="03">03</asp:ListItem>
																<asp:ListItem Value="04">04</asp:ListItem>
																<asp:ListItem Value="05">05</asp:ListItem>
																<asp:ListItem Value="06">06</asp:ListItem>
																<asp:ListItem Value="07">07</asp:ListItem>
																<asp:ListItem Value="08">08</asp:ListItem>
																<asp:ListItem Value="09">09</asp:ListItem>
																<asp:ListItem Value="10">10</asp:ListItem>
																<asp:ListItem Value="11">11</asp:ListItem>
																<asp:ListItem Value="12">12</asp:ListItem>
																<asp:ListItem Value="13">13</asp:ListItem>
																<asp:ListItem Value="14">14</asp:ListItem>
																<asp:ListItem Value="15">15</asp:ListItem>
																<asp:ListItem Value="16">16</asp:ListItem>
																<asp:ListItem Value="17">17</asp:ListItem>
																<asp:ListItem Value="18">18</asp:ListItem>
																<asp:ListItem Value="19">19</asp:ListItem>
																<asp:ListItem Value="20">20</asp:ListItem>
																<asp:ListItem Value="21">21</asp:ListItem>
																<asp:ListItem Value="22">22</asp:ListItem>
																<asp:ListItem Value="23">23</asp:ListItem>
																<asp:ListItem Value="24">24</asp:ListItem>
																<asp:ListItem Value="25">25</asp:ListItem>
																<asp:ListItem Value="26">26</asp:ListItem>
																<asp:ListItem Value="27">27</asp:ListItem>
																<asp:ListItem Value="28">28</asp:ListItem>
																<asp:ListItem Value="29">29</asp:ListItem>
																<asp:ListItem Value="30">30</asp:ListItem>
																<asp:ListItem Value="31">31</asp:ListItem>
															</asp:dropdownlist><asp:dropdownlist id="cmbStart_MM" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
																Height="20px">
																<asp:ListItem>MM</asp:ListItem>
																<asp:ListItem Value="01">01</asp:ListItem>
																<asp:ListItem Value="02">02</asp:ListItem>
																<asp:ListItem Value="03">03</asp:ListItem>
																<asp:ListItem Value="04">04</asp:ListItem>
																<asp:ListItem Value="05">05</asp:ListItem>
																<asp:ListItem Value="06">06</asp:ListItem>
																<asp:ListItem Value="07">07</asp:ListItem>
																<asp:ListItem Value="08">08</asp:ListItem>
																<asp:ListItem Value="09">09</asp:ListItem>
																<asp:ListItem Value="10">10</asp:ListItem>
																<asp:ListItem Value="11">11</asp:ListItem>
																<asp:ListItem Value="12">12</asp:ListItem>
															</asp:dropdownlist><asp:dropdownlist id="cmbStart_YY" runat="server" Width="72px" Font-Size="XX-Small" Font-Names="Verdana">
																<asp:ListItem>YYYY</asp:ListItem>
																<asp:ListItem Value="2005">2005</asp:ListItem>
																<asp:ListItem Value="2006">2006</asp:ListItem>
																<asp:ListItem Value="2007">2007</asp:ListItem>
																<asp:ListItem Value="2008">2008</asp:ListItem>
																<asp:ListItem Value="2009">2009</asp:ListItem>
																<asp:ListItem Value="2010">2010</asp:ListItem>
																<asp:ListItem Value="2011">2011</asp:ListItem>
																<asp:ListItem Value="2012">2012</asp:ListItem>
																<asp:ListItem Value="2013">2013</asp:ListItem>
																<asp:ListItem Value="2014">2014</asp:ListItem>
																<asp:ListItem Value="2015">2015</asp:ListItem>
																<asp:ListItem Value="2016">2016</asp:ListItem>
																<asp:ListItem Value="2017">2017</asp:ListItem>
																<asp:ListItem Value="2018">2018</asp:ListItem>
																<asp:ListItem Value="2019">2019</asp:ListItem>
																<asp:ListItem Value="2020">2020</asp:ListItem>
																<asp:ListItem Value="2021">2021</asp:ListItem>
																<asp:ListItem Value="2022">2022</asp:ListItem>
																<asp:ListItem Value="2023">2023</asp:ListItem>
																<asp:ListItem Value="2024">2024</asp:ListItem>
																<asp:ListItem Value="2025">2025</asp:ListItem>
																<asp:ListItem Value="2026">2026</asp:ListItem>
																<asp:ListItem Value="2027">2027</asp:ListItem>
																<asp:ListItem Value="2028">2028</asp:ListItem>
																<asp:ListItem Value="2029">2029</asp:ListItem>
																<asp:ListItem Value="2030">2030</asp:ListItem>
															</asp:dropdownlist>&nbsp;<FONT size="1">To Date:
																<asp:dropdownlist id="cmbEnd_DD" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
																	Height="20px">
																	<asp:ListItem Value="DD">DD</asp:ListItem>
																	<asp:ListItem Value="01">01</asp:ListItem>
																	<asp:ListItem Value="02">02</asp:ListItem>
																	<asp:ListItem Value="03">03</asp:ListItem>
																	<asp:ListItem Value="04">04</asp:ListItem>
																	<asp:ListItem Value="05">05</asp:ListItem>
																	<asp:ListItem Value="06">06</asp:ListItem>
																	<asp:ListItem Value="07">07</asp:ListItem>
																	<asp:ListItem Value="08">08</asp:ListItem>
																	<asp:ListItem Value="09">09</asp:ListItem>
																	<asp:ListItem Value="10">10</asp:ListItem>
																	<asp:ListItem Value="11">11</asp:ListItem>
																	<asp:ListItem Value="12">12</asp:ListItem>
																	<asp:ListItem Value="13">13</asp:ListItem>
																	<asp:ListItem Value="14">14</asp:ListItem>
																	<asp:ListItem Value="15">15</asp:ListItem>
																	<asp:ListItem Value="16">16</asp:ListItem>
																	<asp:ListItem Value="17">17</asp:ListItem>
																	<asp:ListItem Value="18">18</asp:ListItem>
																	<asp:ListItem Value="19">19</asp:ListItem>
																	<asp:ListItem Value="20">20</asp:ListItem>
																	<asp:ListItem Value="21">21</asp:ListItem>
																	<asp:ListItem Value="22">22</asp:ListItem>
																	<asp:ListItem Value="23">23</asp:ListItem>
																	<asp:ListItem Value="24">24</asp:ListItem>
																	<asp:ListItem Value="25">25</asp:ListItem>
																	<asp:ListItem Value="26">26</asp:ListItem>
																	<asp:ListItem Value="27">27</asp:ListItem>
																	<asp:ListItem Value="28">28</asp:ListItem>
																	<asp:ListItem Value="29">29</asp:ListItem>
																	<asp:ListItem Value="30">30</asp:ListItem>
																	<asp:ListItem Value="31">31</asp:ListItem>
																</asp:dropdownlist><asp:dropdownlist id="cmbEnd_MM" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
																	Height="20px">
																	<asp:ListItem Value="MM">MM</asp:ListItem>
																	<asp:ListItem Value="01">01</asp:ListItem>
																	<asp:ListItem Value="02">02</asp:ListItem>
																	<asp:ListItem Value="03">03</asp:ListItem>
																	<asp:ListItem Value="04">04</asp:ListItem>
																	<asp:ListItem Value="05">05</asp:ListItem>
																	<asp:ListItem Value="06">06</asp:ListItem>
																	<asp:ListItem Value="07">07</asp:ListItem>
																	<asp:ListItem Value="08">08</asp:ListItem>
																	<asp:ListItem Value="09">09</asp:ListItem>
																	<asp:ListItem Value="10">10</asp:ListItem>
																	<asp:ListItem Value="11">11</asp:ListItem>
																	<asp:ListItem Value="12">12</asp:ListItem>
																</asp:dropdownlist><asp:dropdownlist id="cmbEnd_YY" runat="server" Width="72px" Font-Size="XX-Small" Font-Names="Verdana">
																	<asp:ListItem Value="YYYY">YYYY</asp:ListItem>
																	<asp:ListItem Value="2005">2005</asp:ListItem>
																	<asp:ListItem Value="2006">2006</asp:ListItem>
																	<asp:ListItem Value="2007">2007</asp:ListItem>
																	<asp:ListItem Value="2008">2008</asp:ListItem>
																	<asp:ListItem Value="2009">2009</asp:ListItem>
																	<asp:ListItem Value="2010">2010</asp:ListItem>
																	<asp:ListItem Value="2011">2011</asp:ListItem>
																	<asp:ListItem Value="2012">2012</asp:ListItem>
																	<asp:ListItem Value="2013">2013</asp:ListItem>
																	<asp:ListItem Value="2014">2014</asp:ListItem>
																	<asp:ListItem Value="2015">2015</asp:ListItem>
																	<asp:ListItem Value="2016">2016</asp:ListItem>
																	<asp:ListItem Value="2017">2017</asp:ListItem>
																	<asp:ListItem Value="2018">2018</asp:ListItem>
																	<asp:ListItem Value="2019">2019</asp:ListItem>
																	<asp:ListItem Value="2020">2020</asp:ListItem>
																	<asp:ListItem Value="2021">2021</asp:ListItem>
																	<asp:ListItem Value="2022">2022</asp:ListItem>
																	<asp:ListItem Value="2023">2023</asp:ListItem>
																	<asp:ListItem Value="2024">2024</asp:ListItem>
																	<asp:ListItem Value="2025">2025</asp:ListItem>
																	<asp:ListItem Value="2026">2026</asp:ListItem>
																	<asp:ListItem Value="2027">2027</asp:ListItem>
																	<asp:ListItem Value="2028">2028</asp:ListItem>
																	<asp:ListItem Value="2029">2029</asp:ListItem>
																	<asp:ListItem Value="2030">2030</asp:ListItem>
																</asp:dropdownlist></FONT></P>
													</STRONG></FONT></STRONG></FONT>
									</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 108px" align="center" colSpan="3"><STRONG><FONT face="Verdana" color="#660000">
												<P align="left"><asp:checkbox id="ChkBkngType" runat="server" Width="568px" Font-Size="XX-Small" Font-Names="Verdana"
														Text="Transport Booking Type From &amp; To Date Wise Report Selection" AutoPostBack="True" oncheckedchanged="ChkBkngType_CheckedChanged"></asp:checkbox></P>
												<P align="left"><FONT size="1">Booking Type:
														<asp:dropdownlist id="CmbBookingType" runat="server" Width="200px" Font-Size="XX-Small" Font-Names="Verdana"
															AutoPostBack="True" CssClass="flattxt"></asp:dropdownlist>&nbsp;</FONT></P>
												<P align="left"><FONT size="1"><FONT size="1">From Date: </FONT>
														<asp:dropdownlist id="FrmDD" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
															Height="20px">
															<asp:ListItem Value="DD">DD</asp:ListItem>
															<asp:ListItem Value="01">01</asp:ListItem>
															<asp:ListItem Value="02">02</asp:ListItem>
															<asp:ListItem Value="03">03</asp:ListItem>
															<asp:ListItem Value="04">04</asp:ListItem>
															<asp:ListItem Value="05">05</asp:ListItem>
															<asp:ListItem Value="06">06</asp:ListItem>
															<asp:ListItem Value="07">07</asp:ListItem>
															<asp:ListItem Value="08">08</asp:ListItem>
															<asp:ListItem Value="09">09</asp:ListItem>
															<asp:ListItem Value="10">10</asp:ListItem>
															<asp:ListItem Value="11">11</asp:ListItem>
															<asp:ListItem Value="12">12</asp:ListItem>
															<asp:ListItem Value="13">13</asp:ListItem>
															<asp:ListItem Value="14">14</asp:ListItem>
															<asp:ListItem Value="15">15</asp:ListItem>
															<asp:ListItem Value="16">16</asp:ListItem>
															<asp:ListItem Value="17">17</asp:ListItem>
															<asp:ListItem Value="18">18</asp:ListItem>
															<asp:ListItem Value="19">19</asp:ListItem>
															<asp:ListItem Value="20">20</asp:ListItem>
															<asp:ListItem Value="21">21</asp:ListItem>
															<asp:ListItem Value="22">22</asp:ListItem>
															<asp:ListItem Value="23">23</asp:ListItem>
															<asp:ListItem Value="24">24</asp:ListItem>
															<asp:ListItem Value="25">25</asp:ListItem>
															<asp:ListItem Value="26">26</asp:ListItem>
															<asp:ListItem Value="27">27</asp:ListItem>
															<asp:ListItem Value="28">28</asp:ListItem>
															<asp:ListItem Value="29">29</asp:ListItem>
															<asp:ListItem Value="30">30</asp:ListItem>
															<asp:ListItem Value="31">31</asp:ListItem>
														</asp:dropdownlist><asp:dropdownlist id="FrmMM" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
															Height="20px">
															<asp:ListItem Value="MM">MM</asp:ListItem>
															<asp:ListItem Value="01">01</asp:ListItem>
															<asp:ListItem Value="02">02</asp:ListItem>
															<asp:ListItem Value="03">03</asp:ListItem>
															<asp:ListItem Value="04">04</asp:ListItem>
															<asp:ListItem Value="05">05</asp:ListItem>
															<asp:ListItem Value="06">06</asp:ListItem>
															<asp:ListItem Value="07">07</asp:ListItem>
															<asp:ListItem Value="08">08</asp:ListItem>
															<asp:ListItem Value="09">09</asp:ListItem>
															<asp:ListItem Value="10">10</asp:ListItem>
															<asp:ListItem Value="11">11</asp:ListItem>
															<asp:ListItem Value="12">12</asp:ListItem>
														</asp:dropdownlist><asp:dropdownlist id="FrmYY" runat="server" Width="72px" Font-Size="XX-Small" Font-Names="Verdana">
															<asp:ListItem Value="YYYY">YYYY</asp:ListItem>
															<asp:ListItem Value="2005">2005</asp:ListItem>
															<asp:ListItem Value="2006">2006</asp:ListItem>
															<asp:ListItem Value="2007">2007</asp:ListItem>
															<asp:ListItem Value="2008">2008</asp:ListItem>
															<asp:ListItem Value="2009">2009</asp:ListItem>
															<asp:ListItem Value="2010">2010</asp:ListItem>
															<asp:ListItem Value="2011">2011</asp:ListItem>
															<asp:ListItem Value="2012">2012</asp:ListItem>
															<asp:ListItem Value="2013">2013</asp:ListItem>
															<asp:ListItem Value="2014">2014</asp:ListItem>
															<asp:ListItem Value="2015">2015</asp:ListItem>
															<asp:ListItem Value="2016">2016</asp:ListItem>
															<asp:ListItem Value="2017">2017</asp:ListItem>
															<asp:ListItem Value="2018">2018</asp:ListItem>
															<asp:ListItem Value="2019">2019</asp:ListItem>
															<asp:ListItem Value="2020">2020</asp:ListItem>
															<asp:ListItem Value="2021">2021</asp:ListItem>
															<asp:ListItem Value="2022">2022</asp:ListItem>
															<asp:ListItem Value="2023">2023</asp:ListItem>
															<asp:ListItem Value="2024">2024</asp:ListItem>
															<asp:ListItem Value="2025">2025</asp:ListItem>
															<asp:ListItem Value="2026">2026</asp:ListItem>
															<asp:ListItem Value="2027">2027</asp:ListItem>
															<asp:ListItem Value="2028">2028</asp:ListItem>
															<asp:ListItem Value="2029">2029</asp:ListItem>
															<asp:ListItem Value="2030">2030</asp:ListItem>
														</asp:dropdownlist>&nbsp;<FONT size="1">To Date:
															<asp:dropdownlist id="ToDD" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
																Height="20px">
																<asp:ListItem Value="DD">DD</asp:ListItem>
																<asp:ListItem Value="01">01</asp:ListItem>
																<asp:ListItem Value="02">02</asp:ListItem>
																<asp:ListItem Value="03">03</asp:ListItem>
																<asp:ListItem Value="04">04</asp:ListItem>
																<asp:ListItem Value="05">05</asp:ListItem>
																<asp:ListItem Value="06">06</asp:ListItem>
																<asp:ListItem Value="07">07</asp:ListItem>
																<asp:ListItem Value="08">08</asp:ListItem>
																<asp:ListItem Value="09">09</asp:ListItem>
																<asp:ListItem Value="10">10</asp:ListItem>
																<asp:ListItem Value="11">11</asp:ListItem>
																<asp:ListItem Value="12">12</asp:ListItem>
																<asp:ListItem Value="13">13</asp:ListItem>
																<asp:ListItem Value="14">14</asp:ListItem>
																<asp:ListItem Value="15">15</asp:ListItem>
																<asp:ListItem Value="16">16</asp:ListItem>
																<asp:ListItem Value="17">17</asp:ListItem>
																<asp:ListItem Value="18">18</asp:ListItem>
																<asp:ListItem Value="19">19</asp:ListItem>
																<asp:ListItem Value="20">20</asp:ListItem>
																<asp:ListItem Value="21">21</asp:ListItem>
																<asp:ListItem Value="22">22</asp:ListItem>
																<asp:ListItem Value="23">23</asp:ListItem>
																<asp:ListItem Value="24">24</asp:ListItem>
																<asp:ListItem Value="25">25</asp:ListItem>
																<asp:ListItem Value="26">26</asp:ListItem>
																<asp:ListItem Value="27">27</asp:ListItem>
																<asp:ListItem Value="28">28</asp:ListItem>
																<asp:ListItem Value="29">29</asp:ListItem>
																<asp:ListItem Value="30">30</asp:ListItem>
																<asp:ListItem Value="31">31</asp:ListItem>
															</asp:dropdownlist><asp:dropdownlist id="ToMM" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
																Height="20px">
																<asp:ListItem Value="MM">MM</asp:ListItem>
																<asp:ListItem Value="01">01</asp:ListItem>
																<asp:ListItem Value="02">02</asp:ListItem>
																<asp:ListItem Value="03">03</asp:ListItem>
																<asp:ListItem Value="04">04</asp:ListItem>
																<asp:ListItem Value="05">05</asp:ListItem>
																<asp:ListItem Value="06">06</asp:ListItem>
																<asp:ListItem Value="07">07</asp:ListItem>
																<asp:ListItem Value="08">08</asp:ListItem>
																<asp:ListItem Value="09">09</asp:ListItem>
																<asp:ListItem Value="10">10</asp:ListItem>
																<asp:ListItem Value="11">11</asp:ListItem>
																<asp:ListItem Value="12">12</asp:ListItem>
															</asp:dropdownlist><asp:dropdownlist id="ToYY" runat="server" Width="72px" Font-Size="XX-Small" Font-Names="Verdana">
																<asp:ListItem Value="YYYY">YYYY</asp:ListItem>
																<asp:ListItem Value="2005">2005</asp:ListItem>
																<asp:ListItem Value="2006">2006</asp:ListItem>
																<asp:ListItem Value="2007">2007</asp:ListItem>
																<asp:ListItem Value="2008">2008</asp:ListItem>
																<asp:ListItem Value="2009">2009</asp:ListItem>
																<asp:ListItem Value="2010">2010</asp:ListItem>
																<asp:ListItem Value="2011">2011</asp:ListItem>
																<asp:ListItem Value="2012">2012</asp:ListItem>
																<asp:ListItem Value="2013">2013</asp:ListItem>
																<asp:ListItem Value="2014">2014</asp:ListItem>
																<asp:ListItem Value="2015">2015</asp:ListItem>
																<asp:ListItem Value="2016">2016</asp:ListItem>
																<asp:ListItem Value="2017">2017</asp:ListItem>
																<asp:ListItem Value="2018">2018</asp:ListItem>
																<asp:ListItem Value="2019">2019</asp:ListItem>
																<asp:ListItem Value="2020">2020</asp:ListItem>
																<asp:ListItem Value="2021">2021</asp:ListItem>
																<asp:ListItem Value="2022">2022</asp:ListItem>
																<asp:ListItem Value="2023">2023</asp:ListItem>
																<asp:ListItem Value="2024">2024</asp:ListItem>
																<asp:ListItem Value="2025">2025</asp:ListItem>
																<asp:ListItem Value="2026">2026</asp:ListItem>
																<asp:ListItem Value="2027">2027</asp:ListItem>
																<asp:ListItem Value="2028">2028</asp:ListItem>
																<asp:ListItem Value="2029">2029</asp:ListItem>
																<asp:ListItem Value="2030">2030</asp:ListItem>
															</asp:dropdownlist></FONT></FONT>
											</FONT></STRONG>
					</P>
					</TD></TR>
					<TR>
						<TD align="center" colSpan="3"><STRONG><FONT face="Verdana" color="#660000">
									<P align="left"><asp:checkbox id="ChkLocation" runat="server" Width="568px" Font-Size="XX-Small" Font-Names="Verdana"
											Text="Location Wise From &amp; To Date Report Selection" AutoPostBack="True" oncheckedchanged="ChkLocation_CheckedChanged"></asp:checkbox></P>
									<P align="left"><FONT size="1">Location:&nbsp;&nbsp;
											<asp:dropdownlist id="cmbLocation" runat="server" Width="200px" Font-Size="XX-Small" Font-Names="Verdana"
												CssClass="flattxt"></asp:dropdownlist></FONT></P>
									<P align="left"><FONT size="1"><FONT size="1">From Date: </FONT>
											<asp:dropdownlist id="FromDD" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
												Height="20px">
												<asp:ListItem Value="DD">DD</asp:ListItem>
												<asp:ListItem Value="01">01</asp:ListItem>
												<asp:ListItem Value="02">02</asp:ListItem>
												<asp:ListItem Value="03">03</asp:ListItem>
												<asp:ListItem Value="04">04</asp:ListItem>
												<asp:ListItem Value="05">05</asp:ListItem>
												<asp:ListItem Value="06">06</asp:ListItem>
												<asp:ListItem Value="07">07</asp:ListItem>
												<asp:ListItem Value="08">08</asp:ListItem>
												<asp:ListItem Value="09">09</asp:ListItem>
												<asp:ListItem Value="10">10</asp:ListItem>
												<asp:ListItem Value="11">11</asp:ListItem>
												<asp:ListItem Value="12">12</asp:ListItem>
												<asp:ListItem Value="13">13</asp:ListItem>
												<asp:ListItem Value="14">14</asp:ListItem>
												<asp:ListItem Value="15">15</asp:ListItem>
												<asp:ListItem Value="16">16</asp:ListItem>
												<asp:ListItem Value="17">17</asp:ListItem>
												<asp:ListItem Value="18">18</asp:ListItem>
												<asp:ListItem Value="19">19</asp:ListItem>
												<asp:ListItem Value="20">20</asp:ListItem>
												<asp:ListItem Value="21">21</asp:ListItem>
												<asp:ListItem Value="22">22</asp:ListItem>
												<asp:ListItem Value="23">23</asp:ListItem>
												<asp:ListItem Value="24">24</asp:ListItem>
												<asp:ListItem Value="25">25</asp:ListItem>
												<asp:ListItem Value="26">26</asp:ListItem>
												<asp:ListItem Value="27">27</asp:ListItem>
												<asp:ListItem Value="28">28</asp:ListItem>
												<asp:ListItem Value="29">29</asp:ListItem>
												<asp:ListItem Value="30">30</asp:ListItem>
												<asp:ListItem Value="31">31</asp:ListItem>
											</asp:dropdownlist><asp:dropdownlist id="FromMM" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
												Height="20px">
												<asp:ListItem Value="MM">MM</asp:ListItem>
												<asp:ListItem Value="01">01</asp:ListItem>
												<asp:ListItem Value="02">02</asp:ListItem>
												<asp:ListItem Value="03">03</asp:ListItem>
												<asp:ListItem Value="04">04</asp:ListItem>
												<asp:ListItem Value="05">05</asp:ListItem>
												<asp:ListItem Value="06">06</asp:ListItem>
												<asp:ListItem Value="07">07</asp:ListItem>
												<asp:ListItem Value="08">08</asp:ListItem>
												<asp:ListItem Value="09">09</asp:ListItem>
												<asp:ListItem Value="10">10</asp:ListItem>
												<asp:ListItem Value="11">11</asp:ListItem>
												<asp:ListItem Value="12">12</asp:ListItem>
											</asp:dropdownlist><asp:dropdownlist id="FromYY" runat="server" Width="72px" Font-Size="XX-Small" Font-Names="Verdana">
												<asp:ListItem Value="YYYY">YYYY</asp:ListItem>
												<asp:ListItem Value="2005">2005</asp:ListItem>
												<asp:ListItem Value="2006">2006</asp:ListItem>
												<asp:ListItem Value="2007">2007</asp:ListItem>
												<asp:ListItem Value="2008">2008</asp:ListItem>
												<asp:ListItem Value="2009">2009</asp:ListItem>
												<asp:ListItem Value="2010">2010</asp:ListItem>
												<asp:ListItem Value="2011">2011</asp:ListItem>
												<asp:ListItem Value="2012">2012</asp:ListItem>
												<asp:ListItem Value="2013">2013</asp:ListItem>
												<asp:ListItem Value="2014">2014</asp:ListItem>
												<asp:ListItem Value="2015">2015</asp:ListItem>
												<asp:ListItem Value="2016">2016</asp:ListItem>
												<asp:ListItem Value="2017">2017</asp:ListItem>
												<asp:ListItem Value="2018">2018</asp:ListItem>
												<asp:ListItem Value="2019">2019</asp:ListItem>
												<asp:ListItem Value="2020">2020</asp:ListItem>
												<asp:ListItem Value="2021">2021</asp:ListItem>
												<asp:ListItem Value="2022">2022</asp:ListItem>
												<asp:ListItem Value="2023">2023</asp:ListItem>
												<asp:ListItem Value="2024">2024</asp:ListItem>
												<asp:ListItem Value="2025">2025</asp:ListItem>
												<asp:ListItem Value="2026">2026</asp:ListItem>
												<asp:ListItem Value="2027">2027</asp:ListItem>
												<asp:ListItem Value="2028">2028</asp:ListItem>
												<asp:ListItem Value="2029">2029</asp:ListItem>
												<asp:ListItem Value="2030">2030</asp:ListItem>
											</asp:dropdownlist>&nbsp;<FONT size="1">To Date:
												<asp:dropdownlist id="ToDtDD" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
													Height="20px">
													<asp:ListItem Value="DD">DD</asp:ListItem>
													<asp:ListItem Value="01">01</asp:ListItem>
													<asp:ListItem Value="02">02</asp:ListItem>
													<asp:ListItem Value="03">03</asp:ListItem>
													<asp:ListItem Value="04">04</asp:ListItem>
													<asp:ListItem Value="05">05</asp:ListItem>
													<asp:ListItem Value="06">06</asp:ListItem>
													<asp:ListItem Value="07">07</asp:ListItem>
													<asp:ListItem Value="08">08</asp:ListItem>
													<asp:ListItem Value="09">09</asp:ListItem>
													<asp:ListItem Value="10">10</asp:ListItem>
													<asp:ListItem Value="11">11</asp:ListItem>
													<asp:ListItem Value="12">12</asp:ListItem>
													<asp:ListItem Value="13">13</asp:ListItem>
													<asp:ListItem Value="14">14</asp:ListItem>
													<asp:ListItem Value="15">15</asp:ListItem>
													<asp:ListItem Value="16">16</asp:ListItem>
													<asp:ListItem Value="17">17</asp:ListItem>
													<asp:ListItem Value="18">18</asp:ListItem>
													<asp:ListItem Value="19">19</asp:ListItem>
													<asp:ListItem Value="20">20</asp:ListItem>
													<asp:ListItem Value="21">21</asp:ListItem>
													<asp:ListItem Value="22">22</asp:ListItem>
													<asp:ListItem Value="23">23</asp:ListItem>
													<asp:ListItem Value="24">24</asp:ListItem>
													<asp:ListItem Value="25">25</asp:ListItem>
													<asp:ListItem Value="26">26</asp:ListItem>
													<asp:ListItem Value="27">27</asp:ListItem>
													<asp:ListItem Value="28">28</asp:ListItem>
													<asp:ListItem Value="29">29</asp:ListItem>
													<asp:ListItem Value="30">30</asp:ListItem>
													<asp:ListItem Value="31">31</asp:ListItem>
												</asp:dropdownlist><asp:dropdownlist id="ToDtMM" runat="server" Width="48px" Font-Size="XX-Small" Font-Names="Verdana"
													Height="20px">
													<asp:ListItem Value="MM">MM</asp:ListItem>
													<asp:ListItem Value="01">01</asp:ListItem>
													<asp:ListItem Value="02">02</asp:ListItem>
													<asp:ListItem Value="03">03</asp:ListItem>
													<asp:ListItem Value="04">04</asp:ListItem>
													<asp:ListItem Value="05">05</asp:ListItem>
													<asp:ListItem Value="06">06</asp:ListItem>
													<asp:ListItem Value="07">07</asp:ListItem>
													<asp:ListItem Value="08">08</asp:ListItem>
													<asp:ListItem Value="09">09</asp:ListItem>
													<asp:ListItem Value="10">10</asp:ListItem>
													<asp:ListItem Value="11">11</asp:ListItem>
													<asp:ListItem Value="12">12</asp:ListItem>
												</asp:dropdownlist><asp:dropdownlist id="ToDtYY" runat="server" Width="72px" Font-Size="XX-Small" Font-Names="Verdana">
													<asp:ListItem Value="YYYY">YYYY</asp:ListItem>
													<asp:ListItem Value="2005">2005</asp:ListItem>
													<asp:ListItem Value="2006">2006</asp:ListItem>
													<asp:ListItem Value="2007">2007</asp:ListItem>
													<asp:ListItem Value="2008">2008</asp:ListItem>
													<asp:ListItem Value="2009">2009</asp:ListItem>
													<asp:ListItem Value="2010">2010</asp:ListItem>
													<asp:ListItem Value="2011">2011</asp:ListItem>
													<asp:ListItem Value="2012">2012</asp:ListItem>
													<asp:ListItem Value="2013">2013</asp:ListItem>
													<asp:ListItem Value="2014">2014</asp:ListItem>
													<asp:ListItem Value="2015">2015</asp:ListItem>
													<asp:ListItem Value="2016">2016</asp:ListItem>
													<asp:ListItem Value="2017">2017</asp:ListItem>
													<asp:ListItem Value="2018">2018</asp:ListItem>
													<asp:ListItem Value="2019">2019</asp:ListItem>
													<asp:ListItem Value="2020">2020</asp:ListItem>
													<asp:ListItem Value="2021">2021</asp:ListItem>
													<asp:ListItem Value="2022">2022</asp:ListItem>
													<asp:ListItem Value="2023">2023</asp:ListItem>
													<asp:ListItem Value="2024">2024</asp:ListItem>
													<asp:ListItem Value="2025">2025</asp:ListItem>
													<asp:ListItem Value="2026">2026</asp:ListItem>
													<asp:ListItem Value="2027">2027</asp:ListItem>
													<asp:ListItem Value="2028">2028</asp:ListItem>
													<asp:ListItem Value="2029">2029</asp:ListItem>
													<asp:ListItem Value="2030">2030</asp:ListItem>
												</asp:dropdownlist></FONT></FONT>
								</FONT></STRONG></P></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="3"><FONT size="1"><STRONG><FONT face="Verdana" color="#660000">
										<P align="left"><asp:checkbox id="chkEmp" runat="server" Width="568px" Font-Size="XX-Small" Font-Names="Verdana"
												Text="Employee Wise Transport Booking Report Selection" AutoPostBack="True" oncheckedchanged="chkEmp_CheckedChanged"></asp:checkbox></P>
										<P align="left"><FONT size="1">Search Employee Name:&nbsp;&nbsp;
												<asp:textbox id="txtEmp" runat="server" Width="216px" Font-Size="XX-Small" Font-Names="Verdana"
													CssClass="flattxt" MaxLength="200" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<asp:button id="BtnSearch" runat="server" Width="158px" Font-Size="XX-Small" Font-Names="Verdana"
													Text="Search Employee" Height="18px" BorderStyle="Solid" BorderColor="DarkGray" Font-Bold="True"
													ForeColor="ControlText" BackColor="Silver" CausesValidation="False" onclick="BtnSearch_Click"></asp:button></FONT></P>
										<FONT size="1">
											<P><FONT face="Verdana" color="#ff0000" size="1"><STRONG>(Press Ctrl Key and Select 
														Multiple Employees for Report Generation)</STRONG></FONT></P>
										</FONT><FONT size="1"><FONT size="1">
												<P align="center"><asp:listbox id="LstEmp" runat="server" Width="531px" Font-Size="XX-Small" Font-Names="Verdana"
														Height="183px" Font-Bold="True" SelectionMode="Multiple"></asp:listbox></P>
											</FONT></FONT></FONT></STRONG></FONT>
						</TD>
					</TR>
					</TBODY></TABLE></P>
					<P align="center"><FONT size="1"><asp:button id="BtnGenerateReport" runat="server" Width="256px" Font-Size="XX-Small" Font-Names="Verdana"
								Text="Generate Report" Height="30px" BorderStyle="Solid" BorderColor="DarkGray" Font-Bold="True" ForeColor="ControlText"
								BackColor="Silver" CausesValidation="False" onclick="BtnGenerateReport_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="btnrefresh" runat="server" Width="256px" Font-Size="XX-Small" Font-Names="Verdana"
								Text="Refresh" Height="30px" BorderStyle="Solid" BorderColor="DarkGray" Font-Bold="True"
								ForeColor="ControlText" BackColor="Silver" CausesValidation="False" onclick="btnrefresh_Click"></asp:button></FONT></P>
				</FONT></STRONG>
			<TABLE id="Table1" style="WIDTH: 240px; HEIGHT: 69px" height="69" cellSpacing="0" cellPadding="0"
				width="240" border="0">
				<TR height="25%">
					<TD></TD>
					<TD><asp:datagrid id="dgExcel" runat="server" Width="232px" Font-Size="XX-Small" Font-Names="Verdana"
							BorderColor="Black" Font-Bold="True" HorizontalAlign="Center">
							<SelectedItemStyle Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Center"
								VerticalAlign="Middle"></SelectedItemStyle>
							<ItemStyle Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Center"
								VerticalAlign="Middle"></ItemStyle>
							<HeaderStyle Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Center" ForeColor="Sienna"
								BorderStyle="Dashed" VerticalAlign="Middle" BackColor="Silver"></HeaderStyle>
						</asp:datagrid></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>
