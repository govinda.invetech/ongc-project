﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="launchpage.aspx.cs" Inherits="ONGCUIProjects.launchpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="max-age=0" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Uran Online Information System</title>
    <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
    <link href="css/stylelaunchpage.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.7.2.min_a39dcc03.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="js/jqClock.min.js"></script>
    <link href="js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.paginate.js"></script>
    <script src="js/jquery.cycle.lite.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#list_of_users").fancybox();
            $("#TodayQuestionID").hide();
            $(".previous-questions-container1").hide('slow');
            $('.cmdPreamble').fancybox();
            $("#cmdChangePassword").fancybox();

            //------------------change by shiv shakti for plant head page handling at 26/03/2015 ------------- //
            $(".planthead").hide();
            $(".cmdPlantHeadpage").click(function () {
                $(".planthead").show();
                $(".whats-news").hide();
            });
            //------------------,,,,,,,,,,,,,,,,,,,,,,,,,,, ------------- //
            function loading_show() {
                $('#loading').html("<img src='Images/loader.gif'/>").fadeIn('fast');
            }
            function loading_hide() {
                $('#loading').fadeOut('fast');
            }
            function loadData(page) {
                var TodayQue = $("#TodayQuestionID").text();
                loading_show();
                $.ajax
                     ({
                         type: "GET",
                         url: "services/quiz/QueListLaunchPage.aspx",
                         data: "TodayQuestion=" + TodayQue + "&page=" + page,
                         success: function (msg) {
                             $(".previous-questions-container1").ajaxComplete(function (event, request, settings) {
                                 loading_hide();
                                 $(".previous-questions-container1").html(msg);

                             });
                         }
                     });
            }
            $('.previous-questions-container1 .pagination li.active').live('click', function () {
                var page = $(this).attr('p');
                loadData(page);
            });

            $(".cmdViewPreviousQuestion").click(function () {
                if ($(this).attr('is_open') == "YES") {
                    $(this).attr('is_open', 'NO');
                    $(this).text("View Previous Results");
                    $(".previous-questions-container1").hide('slow');
                } else {
                    $(this).attr('is_open', 'YES');
                    $(this).text("Hide Previous Results");
                    loadData(1);
                    $(".previous-questions-container1").show('slow');
                }
            });
            $("#hidden_a").fancybox();
            $(".cmd_click_to_answer").fancybox({
                padding: 0,
                modal: true,
                overlayOpacity: 0.7
            })
            // setting click
            var varNT = 0;
            var vardrop = 0;
            $(document).click(function () {
                if (vardrop == 0) {
                    $(".settings_droparea").hide();
                } else {
                    vardrop = 0;
                }
            });
            $("#cmdSettings").click(function () {
                $(".settings_droparea").show();
                vardrop = 1;
            });

            //end of setting click

            $("#FeedBackFancyBox").fancybox({
                padding: 0,
                modal: true,
                overlayOpacity: 0.7
            })



           

            $("#cmdSaveFeedBack").live("click", function () {
              
                var comment = $("#txtComment").val().trim();
                var rate = $("input[name=Rate]:checked").val();
                if (comment == "") {
                    alert("Please write some comment");
                } else if (rate == undefined) {
                    alert("Please select rating");
                } else {
                    $(this).attr("disabled", true);
                    $(this).val("Saving..");
                    $.ajax({
                        type: "GET",
                        data: "Comment=" + comment + "&Rate=" + rate,
                        url: "services/SaveFeedback.aspx",
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                alert(msg_arr[2]);
                                window.location = self.location;
                                $(this).remove();
                            }
                        }
                    });
                }
            });

            $('#cmdNewNotifications').click(function () {
                varNT = 1;
                $(".notification-box").fadeIn();
            });

            $(document).click(function () {
                if (varNT == 0) {
                    $(".notification-box").fadeOut();
                } else {
                    varNT = 0;
                }
            });

            $("#f_click_to_answer").live('click', function () {
                var abc = $(this).attr('my_value');
                $.fancybox.close();
                $("#hidden_a").attr('href', abc);
                $("#hidden_a").fancybox({
                    padding: 0,
                    modal: true,
                    overlayOpacity: 0.7
                }).trigger('click');
            });

            $('.submenu-container').css('display', 'none');
            $(".div_menu_item").mouseenter(function () {
                $('.submenu-container').css('display', 'none');
                $(this).children('div.submenu-container').show();
            });
            $(".div_menu_item").mouseleave(function () {
                $(this).children('div.submenu-container').hide();
            });

            var ReponseReceived = true;
            setInterval(getNotification, 500);
            getNotification();
            function getNotification() {
                if (ReponseReceived == true) {
                    ReponseReceived == false;
                    $.ajax({
                        url: "services/Notification.aspx",
                        success: function (msg) {
                            var something = msg.split('~');
                            if (something[0] == 'TRUE') {
                                if (something[1] == 'SUCCESS') {
                                    if (something[2] == "0") {
                                        $("#cmdNewNotifications").html("No Notification");
                                        $("#notification_heading").html(something[3]);
                                        $("#div_notification").hide();
                                    } else {
                                        $("#cmdNewNotifications").html("New Notifications<p class=\"notificationsCircle\" style=\"float:left;\"><span>" + something[2] + "</span></p>");
                                        $("#ul_notification").html("<ul class=\"notification-ul\">" + something[3] + "</ul>");
                                        $("#notification_heading").html("New Notification");
                                        $("#div_notification").show();                                                        

                                       
                                     
                                    }
                                }
                            }
                        }
                    });
                }
            }
            //setInterval(myFunction, 1000);
            function myFunction() {
                var CurrQueID = $("#TodayQuestionID").text();
                var timestamp = $("#txtQueEndTimestamp").val();
                var curr = new Date();
                var cYear = curr.getFullYear().toString();
                var cMonth = checkInteger(curr.getMonth() + 1).toString();
                var cDate = checkInteger(curr.getDate()).toString();
                var cHours = checkInteger(curr.getHours()).toString();
                var cMinutes = checkInteger(curr.getMinutes()).toString();
                var cSeconds = checkInteger(curr.getSeconds()).toString();
                var cString = cYear + cMonth + cDate + cHours + cMinutes + cSeconds;
                $.ajax({
                    type: "POST",
                    url: "services/quiz/AutoUpdateQuestion.aspx",
                    data: "CurrentQueID=" + CurrQueID,
                    success: function (msg) {
                        if (msg == "TRUE") {
                            window.location = self.location;
                        }
                    }
                });
                if (cString == timestamp) {
                    window.location = self.location;
                }
            }
            function checkInteger(i) {
                if (i < 10) {
                    return i = "0" + i;
                } else {
                    return i;
                }
            }
            $('#AllOpenQuestion').cycle({
                fx: 'fade',
                speed: 500,
                timeout: 0,
                next: '#next2',
                prev: '#prev2'
            });
        });
    </script>
    <!--[if lt IE 7]>
      <![if gte IE 6]>
      <!-- ACDN-RJS -->
    <%--  <script type="text/javascript" src="js/fixpng.js"></script>  comment by shiv on 27-03-2015 --%>
    <style type="text/css">
        .pagination ul li.inactive, .previous-questions-container1 .pagination ul li.inactive:hover {
            background-color: #ededed;
            color: #bababa;
            border: 1px solid #bababa;
            cursor: default;
        }

        .pagination {
            width: 100%;
            height: 25px;
        }

            .pagination ul li {
                list-style: none;
                float: left;
                border: 1px solid #006699;
                padding: 2px 6px 2px 6px;
                margin: 0 3px 0 3px;
                font-family: arial;
                font-size: 14px;
                color: #006699;
                font-weight: bold;
                background-color: #f2f2f2;
                width: auto;
            }

                .pagination ul li:hover {
                    color: #fff;
                    background-color: #006699;
                    cursor: pointer;
                }

        .iePNG {
            filter: expression(fixPNG(this));
        }

        .tooltip {
            color: #000000;
            outline: none;
            cursor: default;
            text-decoration: none;
            position: relative;
        }

            .tooltip span {
                margin-left: -999em;
                position: absolute;
            }

            .tooltip:hover span {
                border-radius: 5px 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
                -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                font-family: Calibri, Tahoma, Geneva, sans-serif;
                position: absolute;
                left: 0px;
                top: 3.5em;
                z-index: 99;
                margin-left: 0;
                width: 100;
                color: White;
            }

        .info {
            background: black;
            padding: 0.3em 0.3em 0.3em 0.3em;
        }
    </style>
    <![endif]> <![endif]-->
</head>
<body id="page" style="height: 1000px;">
    <form id="form1" runat="server">
        <asp:HiddenField ID="txtQueEndTimestamp" runat="server" />
        <span id='TodayQuestionID' runat='server' style='display: none;'></span>
        <div id="loadfancybox" style="display: none;">
            <a id='hidden_a' href="javascript:void(0);">fdfd</a>
        </div>
        <!-- Header Starts--------------------------------------------------------------------------->
        <div class="header">
            <div class="logo-container">
                <div class="logo">
                </div>
                <div class="logo-text">
                     <h1 class="logo-text-line3">ऑयल एंड नेचुरल गैस कॉरपोरेशन लिमिटेड  उरण संयंत्र, मुंबई</h1>
                    <h1 class="logo-text-line2">Oil and Natural Gas Corporation Limited                        
                    </h1>                 
                    <h1 class="logo-text-line3">URAN PLANT, MUMBAI </h1>                                      
                   
                   
                </div>
                <%--<asp:Label ID="txtServerTime" runat="server" Text="" class="ServerTime">Server Time : <span id="as"></span></asp:Label>--%>
                <asp:LoginStatus ID="LoginStatus1" runat="server" CssClass="logout" LogoutAction="Redirect"
                    LogoutPageUrl="~/Index.aspx" LoginText="Logout" OnLoggingOut="LoginStatus1_LoggingOut" />
                <div style="position: relative; float: right; margin-right: 10px;">
                    <asp:LinkButton ID="cmdSettings" runat="server" CssClass="settings" OnClientClick="return false;"><img src="Images/settings_icon.png" class="settings_icon" />व्यवस्था/SETTING<img src="Images/arrow_down_white.png" class="settings_icon" style="float: right;" />
                    </asp:LinkButton>
                    <div class="settings_droparea">
                        <asp:LinkButton ID="cmdAccountSetting" runat="server" CssClass="accountsettings"
                            OnClick="cmdAccountSetting_Click">
                        <div class="settings_icons2"></div>
                        Update Profile
                        </asp:LinkButton>
                        <a id="cmdChangePassword" class="changepassword" href="services/changepassword.aspx">
                            <div class="lock_icon">
                            </div>
                            Change Password </a>
                    </div>
                </div>
                <a href="Feedback.aspx" id="FeedBackFancyBox" class="feedback"> प्रतिपुष्टि/FEEDBACK </a>
            </div>
            <div class="logged-user-details">
                <h1>Welcome
                </h1>
                <asp:Label ID="lblEmpName" runat="server" CssClass="username" Text="Label"></asp:Label>
                <asp:Label ID="lastlogin" runat="server" Text="">Last logged in : 23/11/2012, 11:27 AM</asp:Label>
                <a runat="server" id="list_of_users" href="services/ListOfUsers.aspx" class="TotalRegistered">
                    <p style="position: relative; float: left;">
                        Total Registered Users
                    </p>
                    <span runat="server" id="NoOfUser" class="TotalRegisteredCount"></span></a>
                <div style="position: relative; float: right;">
                    <a runat="server" href="javascript:void(0)" class="buttonsNotification" id="cmdNewNotifications">
                        <p class="notificationsCircle" style="float: left;">
                            <span></span>
                        </p>
                    </a>
                    <div class="notification-box" id="notificationContainer">
                        <div class="jewelBeeperHeader">
                            <div class="beeperNubWrapper">
                                <div class="beeperNub">
                                </div>
                            </div>
                        </div>
                        <div class="notification_content">
                            <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                                <div class="clearfix uiHeaderTop">
                                    <div>
                                        <h3 runat="server" id="notification_heading" class="uiHeaderTitle"></h3>
                                    </div>
                                </div>
                            </div>
                            <div runat="server" id="div_notification" class="uiScrollableArea fade uiScrollableAreaWithShadow"
                                style="width: 330px; height: 100%;">
                                <div class="uiScrollableAreaWrap scrollable" tabindex="0">
                                    <div class="uiScrollableAreaBody" style="width: 330px;">
                                        <div id="ul_notification" class="uiScrollableAreaContent">
                                            <ul id="conditionList"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Ends----------------------------------------------------------------------------->
        <div class="menu-horizontal" style='background-color: rgb(241,182,182);'>
            <marquee scrolldelay="180" scrollamount="10" width="100%" ">
               <asp:Label ID="lblTOD" runat="server"></asp:Label>
            </marquee>
        </div>
        <!-- Middle Area Starts---------------------------------------------------------------------->
        <div class="middle-area">
            <!-- Left Part Starts----------------------------------------Whats New-------------------------------->
            <div class="leftpart">
                <div class="menu-container-static">
                    <ul id="ul3" class="menu">
                        <div style="position: relative; float: left; width: 100%;" class="div_menu_item cmdPlantHeadpage">
                            <li class="menu-item">
                                <img src="Images/desk.png" class="tile-icon" style="margin-top: 8px; width: 50px;" />
                                <a href="#" class="menu-li-windows8" style="width: 210px;">कार्यकारी निदेशक-संयंत्र प्रबंधक उरण के कक्ष से<br />From The Desk Of ED-PMU</a>
                                <%--  <a href="planthead.aspx" target="_blank" class="menu-li-windows8"
                              style="width: 210px;">From the desk of ED-PMU</a>  --%>
                            </li>
                        </div>
                    </ul>
                </div>
               <%-- <div class="menu-container-static">
                    <ul id="ul4" class="menu">
                        <div style="position: relative; float: left; width: 100%;" class="div_menu_item">
                            <li style="width: 300px;" class="menu-item">
                                <img class="tile-icon" src="Images/forun_icon.png" style="width: 50px; margin-left: 40px;">
                                <a style="width: auto;" class="menu-li-windows8" target="_blank" href="http://10.205.127.108/ONGC_Forum/">Learners Forum</a> </li>
                        </div>
                    </ul>
                </div>--%>
                <div runat="server" id="div_left_menu">
                </div>
                <div class="EmpBirthdayContainer">
                    <h1 class="heading-menu">
                        <img src="Images/cake.png" style="position: relative; float: left; margin: -10px 5px 0 -10px;" />
                        <asp:Label ID="BirthdayHeading" runat="server" Text="">Today's Birthday</asp:Label>
                    </h1>
                    <marquee runat="server" id="marquee_container" style="height: 75px; filter: wave(strength=30, phase=1, add=20, freq=1);"
                        direction="up" scrollamount="3" width="100%" onmouseover="this.stop();"
                        onmouseout="this.start();">
                  </marquee>
                </div>
            </div>
            <!-- Left Part Ends-------------------------------------------------------------------------->
            <!-- Middle Part Ends----------------------------------------------------------------------->
            <div class="question-answer-container">
                <div style="display: none;">
                    <!-- Todays Questions Starts----------------------------------------------------------------->
                    <div id='AllOpenQuestion' runat="server">
                    </div>
                </div>
                <!-- Todays Questions Ends--------------------------------------------------------------------->
                <div class="previous-questions-container1">
                    <div class="previous-question-container-content">
                    </div>
                </div>
                <!-- Whats News Starts----------------------------------------------------------------------->
                <div class="whats-news" style="margin-top: 0px;">
                    <h1 class="heading-whats-news">Whats New<a href="ViewAllNews.aspx" class="view-all-news">View All</a>
                        <ul runat="server" id="ul_news_bullet" class="news-container" style="height: 400px;">
                        </ul>
                </div>
                <!-- Whats News Ends------------------------------------------------------------------------->
                <!-- plant head  change on 26/03/2015 ----------------------------------------------------------------------->
                <div class="planthead" style="margin-top: 0px;">
                    <h1 class="heading-whats-news">
                        <center>

                            <asp:Image ID="imgManager" runat="server" Visible="True" ImageUrl="~/images/Narendra Asija.jpg" />
                        </center>
                    </h1>
                    <h1 class="heading-whats-news">
                        <center>
                            <asp:Label ID="namelbl" Text="Sh. Narendra Asija" runat="server"></asp:Label>
                            <%-- Sh. S. K. Pandey   --%>
                        </center>
                    </h1>
                    <h1 class="heading-whats-news">
                        <center>
                            ED - Plant Manager Uran
                        </center>
                    </h1>
                    <ul runat="server" id="ul_plant_head_bullet" class="news-container" style="height: 400px;">
                    </ul>



                    <!-- To Make New Post Copy From Here -->
                </div>
                <!-- plant head------------------------------------------------------------------------->
            </div>
            <!-- Middle Part Ends----------------------------------------------------------------------->
            <!-- Right Part Starts----------------------------------------------------------------------->
            <div class="rightpart">
                <!--<a class="settings" style="float: left; margin-top: 20px; margin-bottom: 3px; text-align: center; font-size: 15px; position: relative; width: 300px; padding: 20px 0px;" target="_blank" href="http://10.205.127.82/ONGCUIProjects/Login.aspx">Existing Online Reporting System</a>-->
                <div class="menu-container">
                    <ul class="menu" id="ul2">
                        <div class="div_menu_item" style="position: relative; float: left; width: 100%;">
                            <li class="menu-item"><a href="ONGCMenu.aspx" class="menu-horizoltal-items" style="width: 275px;">
                                <img src="images/report.png" style="position: relative;
                                <p style="position: relative; float: left; margin-top: 10px;  top: 0px; left: 0px;">
                                   उरण ऑनलाइन प्रतिवेदन व्यवस्था<br /><p style="float:right; margin-right:14px;">Uran Online Reporting System</p>
                                </p>
                                <!-- <p style="position: relative; float:left; margin-top: 10px;">URAN Online Reporting System</p>-->
                            </a></li>
                            <div runat="server" id="divQuickLoginLink" style="display: none;" class="submenu-container right">
                                <div style="overflow:scroll;">
                                <ul style="padding-left: 0;">
                                    <li class="menu-item" title=""><a style="width: 270px; border-left-width: 0;" href="ONGCMenu.aspx?p_id=3"
                                        class="submenu-a">Incident Reporting</a></li>
                                    <li class="menu-item" title=""><a style="width: 270px; border-left-width: 0;" href="ONGCMenu.aspx?p_id=12"
                                        class="submenu-a">Guest House Booking</a></li>
                                    <li class="menu-item" title=""><a style="width: 270px; border-left-width: 0;" href="ONGCMenu.aspx?p_id=14"
                                        class="submenu-a">Transport Booking</a></li>
                                    <li class="menu-item" title=""><a style="width: 270px; border-left-width: 0;" href="ONGCMenu.aspx?p_id=16"
                                        class="submenu-a">Visitor Pass Requisition</a></li>
                                    <li class="menu-item" title=""><a style="width: 270px; border-left-width: 0;" href="ONGCMenu.aspx?p_id=21"
                                        class="submenu-a">Complaint Registration</a></li>
                                    <li class="menu-item" title=""><a style="width: 270px; border-left-width: 0;" href="ONGCMenu.aspx?p_id=25"
                                        class="submenu-a">Schedule Meeting</a></li>
                                    <li class="menu-item" title=""><a style="width: 270px; border-left-width: 0;" href="Feedback_NewForm.aspx"
                                        class="submenu-a">Infocom Feedback Form</a></li>
                                </ul>
                                <i class="submenu_arrowright"></i>
                               </div>
                            </div>
                        </div>
                    </ul>
                </div>
                <!-- Other Links Starts---------------------------------------------------------------------->
                <div runat="server" id="div_right_menu">
                </div>
                <%--<div class="menu-container">
            <ul class="menu" id="ul2">
                <div class="div_menu_item" style="position: relative; float: left; width: 100%;">
                    <li class="menu-item"><a href="ImageGallery.aspx" class="menu-horizoltal-items" style="width: 275px;">
                        <img src="images/report.png" style="position: relative; float: left; margin-right: 10px;" />
                        <p style="position: relative; float: left; margin-top: 10px;">
                            VIP Visit
                        </p>
                        <!-- <p style="position: relative; float:left; margin-top: 10px;">URAN Online Reporting System</p>-->
                    </a></li>
                    
                </div>
            </ul>
        </div>--%>
            </div>
            <!-- Right Part Ends------------------------------------------------------------------------->
        </div>
        <!-- Middle Area Ends------------------------------------------------------------------------>
        <!-- Footer Starts--------------------------------------------------------------------------->
        <div class="footer">
            <div class="footer-content">
                <p class="copyright">
                 <%--   © 2016 ONGC Limited. All rights reserved.--%>
                </p>
                <p class="designedby">
                    Maintained by Infocom Services, Uran
                </p>
            </div>
        </div>
        <!-- Footer Ends----------------------------------------------------------------------------->
        
    </form>
</body>
</html>
