﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="changepassword.aspx.cs" Inherits="ONGCUIProjects.services.changepassword" %>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<title>ONGCMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		

        <script type="text/javascript" src="js/jquery-1.7.2.min_a39dcc03.js"></script>        
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function (e) {
                $("input[type=password]").focus(function () {
                    $("#txtMessage").hide();
                    $("input[type=password]").css('border-color', '');

                });
                $("input[type=password]").bind("cut copy paste", function (e) {
                    e.preventDefault();
                });
                $(".buttonChangePassword").click(function () {
                    var hint = 0;
                   
                    $("input[type=password]").each(function () {
                        if ($(this).val() == "") {
                            $(this).css('border-color', 'red');
                            hint = 1;
                        }
                    });
                    if (hint == 0) {
                        var old_password = $("#old_password").val();
                        var new_passwordone = $("#new_passwordone").val();
                        var new_passwordtwo = $("#new_passwordtwo").val();
                        if (new_passwordone != new_passwordtwo) {
                            $("#new_passwordone").css('border-color', 'red');
                            $("#new_passwordtwo").css('border-color', 'red');
                            $("#txtMessage").text("These Password don't match. Try again?");
                            $("#txtMessage").show();
                        } else {
                            $.ajax({
                                type: "GET",
                                url: "services/updatepassword.aspx",
                                data: "old_pass=" + old_password + "&newpass1=" + new_passwordone + "&newpass2=" + new_passwordtwo,
                                success: function (msg) {
                                    //alert(msg);
                                    var values = msg.split("~");

                                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                        alert(values[2]);
                                        window.location = "index.aspx";
                                    } else if (values[0] == "FALSE" && values[1] == "ERR") {
                                        window.location = "index.aspx";

                                    } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                                        $("#txtMessage").text(values[2]);
                                        $("#txtMessage").show();
                                    }

                                }
                            });
                        }
                    } else {
                        $("#txtMessage").text("You can't leave  empty");
                        $("#txtMessage").show();
                    }

                });

                $(".buttonCancelChangePassword").click(function () {
                    window.location = self.location;
                });
            });
       </script>
	</HEAD>
    <body>
    <div style=" position: relative; float: left; width: 980px; overflow: hidden; padding: 10px;">
        <h1 class="heading" style="padding-left: 0; border-bottom: 2px solid #58595A; margin-bottom: 20px;"><img src="Images/lock_icon2.png" class="lock_icon2" alt="" />Change your password</h1>
        <div class="div-fullwidth">
            <div style=" position: relative; float: left; width: 50%;">
                <p class="content" style="position: relative; float: left; width: 100%; margin-bottom: 5px;">Please enter your new password. We highly recommend you create a unique password - one that you don't use for any other websites.</p>
                <img src="Images/baloons.jpg" style=" position: relative; float: left;" alt="" />
            </div>
            <div style=" position: relative; float: right; background-color: #F1F1F1; border: 1px solid #B1B1B1; width: 300px; padding: 20px;">
                <p class="content" style="width: 100%; margin-bottom: 5px;">Current Password</p>
                <input id="old_password" type="password" style="width: 100%; margin-bottom: 20px;" />

                <p class="content" style="width: 100%; margin-bottom: 5px;">New Password</p>
                <input  id="new_passwordone" type="password" style="width: 100%; margin-bottom: 20px;" />

                <p class="content" style="width: 100%; margin-bottom: 5px;">Retype New Password</p>
                <input  id="new_passwordtwo"type="password" style="width: 100%; margin-bottom: 5px;" />
                
                <p class="content" style="width: 100%; color: Red;" id="txtMessage"></p>
                <a class="g-button g-button-submit buttonChangePassword" href="javascript:void(0);" style="float: left; margin-bottom: 0; margin-top: 10px;">Change Password</a>
                <a class="g-button g-button-red buttonCancelChangePassword" href="javascript:void(0);" style="float: left; text-transform: capitalize; margin-bottom: 0; margin-top: 10px;">Cancel</a>
                
            </div>
        </div>

    </div>
     </body>
     </HTML>