﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddUpdTODFancyBox.aspx.cs" Inherits="ONGCUIProjects.services.launchpageadmin.AddUpdTODFancyBox" %>
<script type="text/javascript">
    $("#txtTODDispDate").datepicker({ dateFormat: "dd-mm-yy" });
    var ID = $("#lblTODID").text();
    var ActionType = $("#lblTODActionType").text();
    if (ActionType == "EDIT") {
        $("#cmdSubmit").val("UPDATE");
    } else if (ActionType == "ADD") {
        $("#cmdSubmit").val("SAVE");
    }
    $("#cmdSubmit").click(function () {
        var DispDate = $("#txtTODDispDate").val().trim();
        var TODText = $("#txtTODText").val().trim();
        if (DispDate == "" || TODText == "") {
            $("#response").text("Please fill date and text");
        } else {
            $.ajax({
                type: "POST",
                url: "services/launchpageadmin/AddUpdTOD.aspx",
                data: "type=" + ActionType + "&date=" + DispDate + "&todtext=" + TODText + "&id=" + ID,
                success: function (msg) {
                    var msg_arr = msg.split("~");
                    if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                        $("#response").text(msg_arr[2]);
                    } else {
                        alert(msg_arr[2]);
                    }
                }
            });
        }
    });
</script>
<form id="form1" runat="server">
<asp:label runat="server" ID="lblTODID" style="display:none;"></asp:label>
<asp:label runat="server" ID="lblTODActionType" style="display:none;"></asp:label>
    <div style="position:relative;float:left; width:400px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">Add/Update Thought of the Day </h1>
        <div class="div-fullwidth marginbottom">
            <p class="content">Display Date</p>
            <asp:TextBox ID="txtTODDispDate" runat="server" TextMode="SingleLine"></asp:TextBox>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">Thought of the Day</p>
            <asp:TextBox ID="txtTODText" runat="server" TextMode="MultiLine" style="min-width: 100%; max-width: 100%; min-height:150px; max-height:150px;"></asp:TextBox>
        </div>
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
        <div class="div-fullwidth">
            <p id="response" class="content" style="width: auto; color:Red;"></p>
            <input type="button" id="cmdReset" class="g-button g-button-red" value="CLOSE" onclick="window.location=self.location; $.fancybox.close();" style="float:right;" />
            <input type="button" id="cmdSubmit" class="g-button g-button-submit" value="SAVE" style="margin-right:10px; margin-bottom:0px"/>
        </div>
    </div>
</form>