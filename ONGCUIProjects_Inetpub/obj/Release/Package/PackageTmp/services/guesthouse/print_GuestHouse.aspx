﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print_GuestHouse.aspx.cs" Inherits="ONGCUIProjects.services.guesthouse.print_GuestHouse" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  
        <link rel="stylesheet" href="../../css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="../../css/css3.css" media="screen" type="text/css" />
        <script type="text/javascript" language="javascript" src="../../js/jquery-1.7.2.min_a39dcc03.js"></script>
        <script type="text/javascript" language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" language="javascript" src="../../js/print.js"></script>
        <script type="text/javascript">
            $("#cmdPrintRpt").click(function () {
                $("#PrintArea").printElement({
                    overrideElementCSS: [
                            '../../css/styleGlobal.css',
                            { href: '../../css/styleGlobal.css', media: 'print' }
                            ]
                });
            });
        </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="position: relative; width: 750px;">
    <div id="PrintArea" style=" position: relative; width: 100%; overflow-x: hidden; margin-right:auto; margin-left:auto;">
            
        <table class='rpt-table'>
            <tr>
                <td><img src="../../Images/logo-ongc.jpg" /></td>
                <td class="header-name" colspan='5'>
                    <p>OIL AND NATURAL GAS CORPORATION LTD</p>
                    <p>URAN PLANT, URAN – 400 702</p>
                </td>
            </tr>
            <tr>
            <td colspan='6'><hr/></td>
            </tr>
            <tr>
                <td class="header-title" colspan='6'><p>GUEST HOUSE ALLOTMENT DETAILS</p></td>
            </tr>
        </table>
        <table class='rpt-table'>
            <tr class='data-tr'>
                <td class='index-width data-heading'>1</td>
                <td class='heading-width data-heading'>Name</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td colspan='3' class='data-value' runat="server" id="td_app_name"> Karn Ashish Karn Ashish Karn</td>
            </tr>
            <tr class='data-tr'>
                <td class='index-width data-heading'>2</td>
                <td class='heading-width data-heading'>Designation/CPF No</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td colspan='3' class='data-value' runat="server" id="td_app_desig_cpf"> Karn Ashish Karn Ashish Karn</td>
            </tr>
            <tr class='data-tr'>
                <td class='index-width data-heading'>3</td>
                <td class='heading-width data-heading'>Ex No.</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td colspan='3' class='data-value' runat="server" id="td_app_contact"> Karn Ashish Karn Ashish Karn</td>
            </tr>
           <tr class='data-tr'>
                <td class='index-width data-heading'>4</td>
                <td class='heading-width data-heading'>Section</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td colspan='3' class='data-value' runat="server" id="td_app_section"> Karn Ashish Karn Ashish Karn</td>
            </tr>
            <tr class='data-tr'>
                <td class='index-width data-heading'>5</td>
                <td class='heading-width data-heading'>Purpose</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td colspan='3' class='data-value' runat="server" id="td_purpose"> Karn Ashish Karn Ashish Karn</td>
            </tr>          
            <tr class='data-tr' id='tr_td_date_time' runat="server">
                <td class='index-width data-heading'>6</td>
                <td class='heading-width data-heading'>Start Date and Time</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td colspan='3' class='data-value' runat="server" id="td_date_time"> Karn Ashish Karn Ashish Karn</td>
            </tr>
            <tr class='data-tr' id='tr_food_detail' runat="server">
                <td class='index-width data-heading'>6</td>
                <td class='heading-width data-heading'>Food Detail</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td colspan='3' class='data-value' runat="server" id="food_detail"> Karn Ashish Karn Ashish Karn</td>
            </tr>
          
            <tr class='data-tr'>
                <td class='index-width data-heading'>7</td>
                <td colspan='5' class='heading-width data-heading'>Approving Details</td>
            </tr>
            <tr class='data-tr'>
                <td></td>
                <td class='heading-width data-heading'>a. In-Charge</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td class='data-value' runat="server" id="td_inc_name">Name</td>
                <td class='data-value' runat="server" id="td_inc_desig">Designation</td>
                <td class='data-value' runat="server" id="td_inc_timestamp">Timestamp</td>
            </tr>
            <tr class='data-tr'>
                <td></td>
                <td class='heading-width data-heading'>b. Approved By</td>
                <td class='heading-sep-width data-heading'>:</td>
                <td class='data-value' runat="server" id="td_approver_name">Name</td>
                <td class='data-value' runat="server" id="td_approver_desig">Designation</td>
                <td class='data-value' runat="server" id="td_approver_timestamp">Timestamp</td>
            </tr>
           <tr class='data-tr'>
            <td></td>
                <td class='heading-width data-heading'>c. Alloter Remarks</td>
                <td class='heading-sep-width data-heading'>:</td>                
               
                 <td class='data-value' runat="server" id="alloter_remarks">Name</td>
                <td class='data-value' runat="server" id="td_room_no"></td>
                <td class='data-value' runat="server" id="td3"></td>
            </tr>
           
          
        </table>
        </div>
    <div class="div-fullwidth" 
            style="float:none;text-align:center; margin:30px 0px 10px 0px;">
        <a id="cmdPrintRpt" href='javascript:void(0);' class="print-button">PRINT</a>
       
    </div>
    </div>
    </form>
</body>
</html>

