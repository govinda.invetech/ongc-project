﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GuestHouseDetailToApprove.aspx.cs"
    Inherits="ONGCUIProjects.services.guesthouse.GuestHouseDetailToApprove" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <script type="text/javascript">
        $(document).ready(function () {
            var sActionType = $("#txtHideActionType").val();
            var sBookingID = $("#txtHideBookingID").val();
            var sBookingType = $("#HiddenField1").val();
            var txtStartdatetime = $("#txtbookingstartdatetime").val();
            var txtEnddatetime = $("#txtbookingEnddatetime").val();
            type_selection_change(sBookingType);
            var sSessionCPF = $("#txtSessionCPF").val();
            var sBookingID = $("#txtBookingID").val();

            var sIncharge = $("#txtInchargeStatus").val();
            var sInchargeArr = sIncharge.split('~');
            var sInchargeCPF = sInchargeArr[0];
            var sInchargeStatus = sInchargeArr[1];

            var sApprover = $("#txtApproverStatus").val();
            var sApproverArr = sApprover.split('~');
            var sApproverCPF = sApproverArr[0];
            var sApproverStatus = sApproverArr[1];

            var sAlloter = $("#txtAlloterStatus").val();
            var sAlloterArr = sAlloter.split('~');
            var sAlloterCPF = sAlloterArr[0];
            var sAlloterStatus = sAlloterArr[1];

            if (sBookingType != "Room") {
                $("#Div_Room_no").hide();
            }
            if (sInchargeStatus == "APPROVE") {
                $("#div_remarks_room_detail").show();
            } else {
                $("#div_remarks_room_detail").hide();
            }

            if (sAlloterStatus == "APPROVE") {
                $("#div_vehicle_allotment").remove();
            }

            if (sApproverStatus == "REJECT") {
                $("#div_vehicle_detail").hide();
            }
            $(".approve_reject").click(function () {
                var type = $(this).attr('hint');
                var type_desig = $(this).attr('type_desig');
                $(this).parent('td');

                switch (type_desig) {
                    case 'INCHARGE':
                        if (sInchargeCPF == sSessionCPF) {
                            if (confirm("Are you sure to " + type.toLowerCase() + " this request?")) {
                                UpdateStatus();
                            }
                        } else {
                            alert('You are not authorised to Approve/Reject this request.');
                        }
                        break;
                    case 'APPROVER':
                        if (sApproverCPF == sSessionCPF) {
                            if (sInchargeStatus == 'APPROVE') {
                                if (confirm("Are you sure to " + type.toLowerCase() + " this request?")) {
                                    UpdateStatus();
                                }
                            } else {
                                alert('This request is still pending or rejected from Incharge.');
                            }
                        } else {
                            alert('You are not authorised to Approve/Reject this request.');
                        }
                        break;
                    case 'ALLOTER':
                        if (sAlloterCPF == sSessionCPF) {
                            if (sInchargeStatus == 'APPROVE') {
                                if (type == "APPROVE") {
                                    var txtRoomNo = $("#txtRoomNo").val();
                                    var txtAlloterRemrks = $("#txtAlloterRemarks").val();

                                    if (txtRoomNo == "" && sBookingType == "Room") {
                                        alert("Please enter a Room no.");
                                        return false;
                                    }
                                    if (txtAlloterRemrks == "") {
                                        alert("Please alloter remarks before approve.");
                                        return false;
                                    }

                                    if (sApproverStatus == "PENDING") {
                                        if (confirm("Are you sure to allot the " + sBookingType + " to this request without approver confirmation?")) {
                                            AllotGuesthouse();
                                        }
                                    } else {
                                        if (confirm("Are you sure to allot the " + sBookingType + " this request?")) {
                                            AllotGuesthouse();
                                        }
                                    }
                                } else {
                                    if (confirm("Are you sure " + sBookingType + " not availble for this request?")) {
                                        UpdateStatus();
                                    }
                                }
                            } else {
                                alert('This request is still pending or rejected from Incharge.');
                            }
                        } else {
                            alert('You are not authorised to allot the ' + sBookingType + ' to this request.');
                        }
                        break;
                }

                function UpdateStatus() {
                    $.ajax({
                        type: "POST",
                        url: "services/guesthouse/approveorrejectrequestofguesthouse.aspx",
                        data: "type=" + type + "&type_desig=" + type_desig + "&BookingID=" + sBookingID + "&sBookingType=" + sBookingType + "&txtStartdatetime=" + txtStartdatetime + "&txtEnddatetime=" + txtEnddatetime,
                        success: function (msg) {
                            // alert(msg);
                            var values = msg.split("~");
                            if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                $('input[type="button"][type_desig="' + type_desig + '"]').parent('td').html(values[2]);
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/tick.png",
                                    text: values[3]
                                });
                            } else if (values[0] == "FALSE" && values[1] == "ERR") {
                                alert(values[2]);
                            }
                        }
                    });
                }
                function AllotGuesthouse() {
                    $.ajax({
                        type: "POST",
                        url: "services/guesthouse/approveorrejectrequestofguesthouse.aspx",
                        data: "type=" + type + "&type_desig=" + type_desig + "&BookingID=" + sBookingID + "&txtAlloterRemrks=" + txtAlloterRemrks + "&txtRoomNo=" + txtRoomNo + "&sBookingType=" + sBookingType+"&txtStartdatetime="+txtStartdatetime+"&txtEnddatetime="+txtEnddatetime,
                        success: function (msg) {
                            // alert(msg);
                            var values = msg.split("~");
                            if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                $('input[type="button"][type_desig="' + type_desig + '"]').parent('td').html(values[2]);
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/tick.png",
                                    text: values[3]
                                });
                            } else if (values[0] == "FALSE" && values[1] == "ERR") {
                                alert(values[2]);
                            }
                        }
                    });
                }
            });
            function type_selection_change(type) {
                if (type == "Food") {
                    $(".div_type_of_food").show();
                    $(".datetime_div").hide();
                } else {
                    $(".div_type_of_food").hide();
                    $(".datetime_div").show();
                }
            }
        });
    </script>
    <!-- Script Ends -->

    <form id="form1" runat="server" class="MainInterface-iframe">
    <asp:HiddenField ID="txtHideActionType" runat="server" />
    <asp:HiddenField ID="txtHideBookingID" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="div-fullwidth iframeMainDiv">
        <div id="div_full_form" class="div-fullwidth" style="width: 100%;">
            <fieldset style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Applicant Details</legend>
                <div class="div-fullwidth marginbottom">
                    <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                        <h1 class="content">
                            Applicant Name</h1>
                        <input runat="server" id="txtappname" type="text" value=" " class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                        <h1 class="content">
                            Designation</h1>
                        <input runat="server" id="txtappdesign" type="text" value=" " class="contactdir-inputbox"
                            style="width: 180px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 26%; margin-top: 0;">
                        <h1 class="content">
                            CPF No.</h1>
                        <input runat="server" id="txtappcpf_no" type="text" value=" " class="contactdir-inputbox"
                            style="width: 150px;" disabled="disabled" />
                    </div>
                </div>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                        <h1 class="content" style="width: 121px;">
                            Department</h1>
                        <input runat="server" id="txtapplocation" type="text" value="  " class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content">
                            Phone Ex. no.</h1>
                        <input runat="server" id="txtappphoneex_no" type="text" value="  " class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
           <fieldset>
                <legend class="content" style="float: none; color: green">Other Detail</legend>
                <div class="div-fullwidth marginbottom">
                    <div class="div-halfwidth">
                        <h1 class="content" style='width:120px;'>
                            Type of Booking</h1>
                        <input type="text" runat="server" id="ddltypeofbooking" style="width: 200px;" />
                    </div>
                    <div id="Div1" class="div-halfwidth div_type_of_food" runat="server" style="display: none">
                        <h1 class="content" style='width:120px;'>
                            Type of Food</h1>
                        <input type="text" runat="server" id="type_of_food" style="padding: 5px;" />
                    </div>
                    
                </div>
                <div class="div-fullwidth">
                    <div id="Div2" class="div-halfwidth datetime_div" runat="server">
                        <h1 class="content" style='width:120px;'>
                            Booking From</h1>
                        <input runat="server" id="txtbookingstartdatetime" type="text" class="contactdir-inputbox"
                            value=" " style="width: 160px;" />
                    </div>
                    <div id="Div4" class="div-halfwidth datetime_div" runat="server">
                        <h1 class="content" style='width:120px;'>
                            Booking To</h1>
                        <input value=" " runat="server" id="txtbookingEnddatetime" type="text" class="contactdir-inputbox"
                            style="width: 160px;" />
                    </div>
                </div>
                <div class="div-fullwidth">
                    <div id="Div7" class="div-halfwidth div_type_of_food" runat="server">
                        <h1 class="content" style='width:120px;'>
                            Date</h1>
                        <input runat="server" id="txtFoodDeliveryDate" type="text" class="contactdir-inputbox"
                            value=" " style="width: 160px;" />
                    </div>
                    <div id="Div5" class="div-halfwidth div_type_of_food" runat="server" style="display: none">
                        <div class="div-halfwidth">
                            <h1 class="content">Veg</h1>
                            <input runat="server" value=" " id="no_of_veg" type="text" style="position:relative;float:left; width: 60px; margin-right:20px;" />
                        </div>
                        <div class="div-halfwidth">
                            <h1 class="content">Non-Veg</h1>
                            <input runat="server" value=" " id="no_of_nonveg" type="text" style=" position:relative;float:left; width: 60px;margin-right:20px;" />
                        </div>
                    </div>
                </div>
                <div class="div-fullwidth" style='margin-top:10px;'>
                    <div class="div-halfwidth">
                        <h1 class="content" style='width:120px;'>
                            Remarks</h1>
                        <input value=" " runat="server" id="txtremarks" type="text" class="contactdir-inputbox"
                            style="width: 290px;" />
                    </div>
                    <div class="div-halfwidth">
                        <h1 class="content" style='width:120px;'>
                            Purpose</h1>
                        <input value=" " runat="server" id="txtpurpose" type="text" class="contactdir-inputbox"
                            style="width: 285px;" />
                    </div>
                </div>
            </fieldset>
            <fieldset style="width: 97%;" id="div_vehicle_allotment">
                <legend class="content" style="float: none; color: green">Authority Actions</legend>
                <div runat="server" id="div_approving_authorities">
                </div>
            </fieldset>
            <fieldset style="width: 97%;" id="div_remarks_room_detail">
                <legend class="content" style="float: none; color: green">Remark Details</legend>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                        <h1 class="content">
                            Alloter Remarks</h1>
                        <input runat="server" id="txtAlloterRemarks" type="text" value=" " class="contactdir-inputbox"
                            style="width: 180px;" />
                    </div>
                    <div id="Div_Room_no" class="div-fullwidth" style="width: 35%; margin-top: 0;">
                        <h1 class="content">
                            Room no</h1>
                        <input runat="server" id="txtRoomNo" type="text" value=" " class="contactdir-inputbox"
                            style="width: 180px;" />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="txtBookingID"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="txtSessionCPF"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="txtInchargeStatus"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="txtApproverStatus"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="txtAlloterStatus"></asp:HiddenField>
    </form>
