﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditUserProfileFancyBox.aspx.cs"
    Inherits="ONGCUIProjects.services.EditUserProfileFancyBox" %>

<script type="text/javascript">
    $('#txt_dob').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "1950:+0",
        maxDate: '0'
    });
</script>
<form id="form1" runat="server">
<div>
    <div style="width: 99%; border: 2px solid red;" class="div-fullwidth">
        <div style="top: 0px; left: 0px" class="div-fullwidth base">
            <h1 style="width: 100%; margin-bottom: 20px; font-size: 16px;" class="registration-content">
                Fields Marked with <span style="color: Red;">( * )</span> are Mandatory Fields</h1>
            <div class="div-halfwidth">
                <h1 style="width: 165px;" class="userapproval-content">
                    Employee Name <span style="color: Red;">( * )</span> :</h1>
                <asp:textbox id="txtEmpName" runat="server" width="240px"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    CPF No <span style="color: Red;">( * )</span> :</h1>
                <asp:textbox id="txtEmpCPFNo" runat="server" width="240px"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Designation :</h1>
                <asp:dropdownlist id="ddlDesignation" runat="server" width="240px">
                    </asp:dropdownlist>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Gender :</h1>
                <asp:dropdownlist id="CmbGender" runat="server" width="240px">
                        <asp:ListItem Value="M">Male</asp:ListItem>
                        <asp:ListItem Value="F">Female</asp:ListItem>
                    </asp:dropdownlist>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    DOB <span style="color: Red;">( * )</span>:</h1>
                <asp:textbox id="txt_dob" runat="server" width="240px"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Department :</h1>
                <asp:dropdownlist id="ddlDepartment2" runat="server" width="240px">
                    </asp:dropdownlist>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 style="width: 165px;" class="userapproval-content">
                    Mobile No <span style="color: Red;">( * )</span> :</h1>
                <asp:textbox id="txtMblNo" runat="server" width="240px"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Employee Ext. No. :</h1>
                <asp:textbox id="txtemployeeexno" runat="server" width="240px"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 style="width: 165px;" class="userapproval-content">
                    ONGC Mail <span style="color: Red;">( * )</span> :</h1>
                <asp:textbox id="txtEmail1" runat="server" width="240px"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Email ID 2 :</h1>
                <asp:textbox id="txtEmail2" runat="server" width="240px"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Blood Group :</h1>
                <asp:dropdownlist id="cmbBldGrp" runat="server" width="240px">
                        <asp:ListItem Value="A +ve">A +ve</asp:ListItem>
                        <asp:ListItem Value="A -ve">A -ve</asp:ListItem>
                        <asp:ListItem Value="B +ve">B +ve</asp:ListItem>
                        <asp:ListItem Value="B -ve">B -ve</asp:ListItem>
                        <asp:ListItem Value="O +ve">O +ve</asp:ListItem>
                        <asp:ListItem Value="O -ve">O -ve</asp:ListItem>
                        <asp:ListItem Value="AB +ve">AB +ve</asp:ListItem>
                        <asp:ListItem Value="AB -ve">AB -ve</asp:ListItem>
                    </asp:dropdownlist>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Work Location :</h1>
                <asp:dropdownlist id="ddlWorkLocation" runat="server" width="240px">
                    </asp:dropdownlist>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Address Line 1 :</h1>
                <asp:textbox id="txtAddr1" runat="server" width="240px" textmode="MultiLine"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Address Line 2 :</h1>
                <asp:textbox id="txtAddr2" runat="server" width="240px" textmode="MultiLine"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    City :</h1>
                <asp:textbox id="txtCity" runat="server" width="240px"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    State :</h1>
                <asp:textbox id="txtState" runat="server" width="240px"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Pin Code :</h1>
                <asp:textbox id="txtPinCode" runat="server" width="240px"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Office Phone No :</h1>
                <asp:textbox id="txtHomeNo" runat="server" width="240px"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth base">
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    Supervisior Name :</h1>
                <asp:dropdownlist id="ddlSupervisiorname" runat="server" width="240px">
                    </asp:dropdownlist>
            </div>
        </div>
        <div class="div-fullwidth" style="text-align: center; margin-top: 10px; margin-bottom: 10px;">
            <input type="button" id="cmdUpdate" value="UPDATE" class="g-button g-button-submit"
                style="float: none;" />
            <input type="button" id="cmd_cancel" value="CANCEL" class="g-button g-button-red"
                style="float: none;" />
        </div>
    </div>
</div>
</form>
