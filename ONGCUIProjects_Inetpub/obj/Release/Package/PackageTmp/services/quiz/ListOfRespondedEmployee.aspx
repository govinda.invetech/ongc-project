﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListOfRespondedEmployee.aspx.cs"
    Inherits="ONGCUIProjects.services.quiz.ListOfRespondedEmployee" %>
    <script type="text/javascript">
        $("#cmdExportToExcel").click(function (e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#RegisteredEmpList').html()));
            e.preventDefault();
        });
    </script>
<form id="form1" runat="server">
<div style="position: relative; float: left; width: 700px;">
    <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A;">
        Question Reponse from Employees 
        <span style='float: right;'><input type="button" value="Export To Excel" id="cmdExportToExcel" class='g-button g-button-submit' /></span>
    </h1>
    <div runat="server" id="RegisteredEmpList" style="position: relative; float: left;
        width: 100%; max-height: 400px; overflow: auto;">
    </div>
</div>
</form>
