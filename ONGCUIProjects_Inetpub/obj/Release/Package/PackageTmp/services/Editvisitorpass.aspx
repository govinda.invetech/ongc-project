﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Editvisitorpass.aspx.cs"
    Inherits="ONGCUIProjects.services.Editvisitorpass" %>

<script type="text/javascript">

</script>
<form id="form1" runat="server">
<div>
    <div style="width: 99%; border: 2px solid red;" class="div-fullwidth">
        <div style="top: 0px; left: 0px" class="div-fullwidth base">
            <h1 style="width: 100%; margin-bottom: 20px; font-size: 16px;" class="registration-content">
                Fields Marked with <span style="color: Red;">( * )</span> are Mandatory Fields</h1>
            <div class="div-halfwidth">
                <h1 style="width: 165px;" class="userapproval-content">
                   Tools <span style="color: Red;">( * )</span> :</h1>
                <asp:textbox id="txttool" runat="server" width="240px"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <h1 class="userapproval-content">
                    CPF No <span style="color: Red;">( * )</span> :</h1>
                <asp:textbox id="txtIndexNumber" runat="server" width="240px"></asp:textbox>
            </div>
        </div>
        
   
        <div class="div-fullwidth" style="text-align: center; margin-top: 10px; margin-bottom: 10px;">
            <input type="button" id="cmdUpdate" value="UPDATE" class="g-button g-button-submit"
                style="float: none;" />
            <input type="button" id="cmd_cancel" value="CANCEL" class="g-button g-button-red"
                style="float: none;" />
        </div>
    </div>
</div>
</form>
