﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactDirMgmt.aspx.cs"
    Inherits="ONGCUIProjects.ContactDirMgmt" %>

<!DOCTYPE html>
<html>
<head>
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script src="js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            var my_hint = $("#txtContactTypeToAdd").val().trim();
            if (my_hint != "") {
                if (my_hint == "REGEMP") {
                    $('input[name="ContactType"][value="NONEMP"]').prop('checked', true);
                } else {
                    $('input[name="ContactType"][value="' + my_hint + '"]').prop('checked', true);
                }
                ShowHideFields(my_hint);
            }
            $('input[type="radio"][name="ContactType"]').change(function () {
                $("#SaveNewContact").removeAttr("disabled");
                var ContactTypeToAdd = $(this).val();
                ShowHideFields(ContactTypeToAdd);
            });
            function ShowHideFields(ContactType) {
                $('#contact_to_add').hide();

                if (ContactType == "LOCATION") {
                    $('.emp_contact_to_add').hide();
                } else {
                    $('.emp_contact_to_add').show();
                }
                $('#contact_to_add').show();
            }

            /*--------------Start booking type autocomplete-----------------------*/
            var ContactSearch_options, ContactSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: ContactSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'contact', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                ContactSearch_a = $("#txtSearchContact").autocomplete(options);
            });

            var ContactSearch_onAutocompleteSelect = function (ContactSearch_value, ContactSearch_data) {
                if (ContactSearch_data == -1) {
                    $("#txtSearchContact").val('');
                } else {
                    var data_arr = ContactSearch_data.split('~');
                    window.location = "ContactDirMgmt.aspx?ContactDept=" + encodeURIComponent(data_arr[1]) + "&ContactID=" + data_arr[0] + "~" + data_arr[2];
                }
            }
            /*---------------End of booking type autocomplete box-------------------*/

            $("#txtCPFNo").focusin(function () {
                $("#SaveNewContact").removeAttr("disabled");
                $("#notifyCpfExist").text("");
                $("#notifyCpfExist").hide();
            });
            $("#txtCPFNo").focusout(function () {
                var sCPF = $(this).val().trim();
                if (sCPF != "") {
                    $.ajax({
                        type: "POST",
                        url: "services/contact/checkCPF.aspx",
                        data: "cpfno=" + sCPF,
                        success: function (msg) {
                            if (msg != "TRUE") {
                                $("#SaveNewContact").attr("disabled", "disabled");
                                $("#notifyCpfExist").text(msg);
                                $("#notifyCpfExist").show();
                                return false;
                            } else {
                                $("#SaveNewContact").removeAttr("disabled");
                                $("#notifyCpfExist").text("");
                                $("#notifyCpfExist").hide();
                            }
                        }
                    });
                }
            });
            $("#SaveNewContact").click(function () {
                var ContactType = "";
                var sToAddCPF = "";
                var sName = "";
                var sDesig = "";
                var sResi = "";
                var sMob = "";
                var sEmail = "";
                var sEmail2 = "";
                var ContactDept = "";
                var sExtn = "";
                var sOff = "";
                if (my_hint == "") {
                    ContactType = $('input[name="ContactType"]:checked').val();
                } else {
                    ContactType = my_hint;
                }

                if (ContactType == "NONEMP" || ContactType == "REGEMP") {
                    sToAddCPF = $("#txtCPFNo").val().trim();
                    if (sToAddCPF == "") {
                        alert("Please enter CPF no.");
                        return false;
                    }
                }
                sName = $("#txtName").val().trim();
                if (sName == "") {
                    alert("Please enter name.");
                    return false;
                }


                if (ContactType == "NONEMP" || ContactType == "REGEMP") {
                    sDesig = $("#txtDesignation").val().trim();
                    if (sDesig == "Select Designation") {
                        sDesig = "";
                    }

                    sResi = $("#txtResi").val().trim();
                    if (sResi != "") {
                        if (isNaN(sResi)) {
                            alert("Please enter a valid residence no.");
                            return false;
                        }
                    }

                    sMob = $("#txtMob").val().trim();
                    if (sMob != "") {
                        if (sMob.length == 10) {
                            if (isNaN(sMob)) {
                                alert("Please enter a valid mobile no.");
                                return false;
                            }
                        } else {
                            alert("Please enter a valid mobile no.");
                            return false;
                        }
                    }

                    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

                    sEmail = $("#txtEmail").val().trim();
                    if (sEmail != "") {
                        if (emailPattern.test(sEmail) == false) {
                            alert("Please enter a valid email id.");
                            return false;
                        }
                    }

                    sEmail2 = $("#txtEmail2").val().trim();
                    if (sEmail2 != "") {
                        if (emailPattern.test(sEmail2) == false) {
                            alert("Please enter a valid email id.");
                            return false;
                        }
                    }
                }
                ContactDept = $("#txtDepartment").val();
                if (ContactDept == "0") {
                    alert("Please select a department.");
                    return false;
                }



                sExtn = $("#txtExtn").val().trim();
                if (sExtn != "") {
                    if (isNaN(sExtn)) {
                        alert("Please enter a valid extn no.");
                        return false;
                    }
                }


                sOff = $("#txtOff").val().trim();
                if (sOff != "") {
                    if (isNaN(sOff)) {
                        alert("Please enter a valid office no.");
                        return false;
                    }
                }

                var str = "cpfno=" + sToAddCPF + "&name=" + sName + "&desig=" + encodeURIComponent(sDesig) + "&extn=" + sExtn + "&resi_d=" + sResi + "&off_d=" + sOff + "&mobile=" + sMob + "&email=" + encodeURIComponent(sEmail) + "&email2=" + encodeURIComponent(sEmail2) + "&dept=" + encodeURIComponent(ContactDept) + "&ContactType=" + ContactType;
                var type = $(this).val();
                if (type == "UPDATE") {
                    var contactId = $(this).attr("contactid");
                    if (ContactType == "REGEMP") {
                        $.ajax({
                            type: "POST",
                            url: "services/contact/AddContact.aspx",
                            data: str,
                            success: function (msg) {
                                var msg_arr = msg.split("~");
                                if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                    alert(msg_arr[2]);
                                    window.location = "ContactDirMgmt.aspx?ContactDept=" + encodeURIComponent(ContactDept);
                                } else {
                                    alert(msg_arr[2]);
                                }
                            }
                        });
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "services/contact/DeleteContact.aspx",
                            data: "ContactID=" + contactId,
                            success: function (msg) {
                                var msg_arr = msg.split("~");
                                if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                    $.ajax({
                                        type: "POST",
                                        url: "services/contact/AddContact.aspx",
                                        data: str,
                                        success: function (msg) {
                                            var msg_arr = msg.split("~");
                                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                                alert(msg_arr[2]);
                                                window.location = "ContactDirMgmt.aspx?ContactDept=" + encodeURIComponent(ContactDept);
                                            } else {
                                                alert(msg_arr[2]);
                                            }
                                        }
                                    });
                                } else {
                                    alert(msg_arr[2]);
                                }
                            }
                        });
                    }

                } else if (type == "SAVE") {
                    $.ajax({
                        type: "POST",
                        url: "services/contact/AddContact.aspx",
                        data: str,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                alert(msg_arr[2]);
                                window.location = self.location;
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }

            });

            $("#contact_category_select").change(function () {
                ContactDept = $(this).val();
                window.location = "ContactDirMgmt.aspx?ContactDept=" + encodeURIComponent(ContactDept);
            });


            $("#reset_fields").click(function () {
                window.location = "ContactDirMgmt.aspx";
            });

            $(".deleteContact").click(function () {
                var DeleteContactID = $(this).parent('li').attr("ContactID");
                if (confirm("Are you sure to delete this contact?")) {
                    DeleteContact(DeleteContactID);
                }
            });

            $("#cmdDeleteContact").click(function () {
                var DeleteContactID = $(this).attr("ContactID");
                if (confirm("Are you sure to delete this contact?")) {
                    DeleteContact(DeleteContactID);
                }
            });

            function DeleteContact(ContactID) {
                $.ajax({
                    type: "POST",
                    url: "services/contact/DeleteContact.aspx",
                    data: "ContactID=" + ContactID,
                    success: function (msg) {
                        var msg_arr = msg.split("~");
                        if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                            ContactDept = $("#contact_category_select").val();
                            window.location = "ContactDirMgmt.aspx?ContactDept=" + encodeURIComponent(ContactDept);
                        } else {
                            alert(msg_arr[2]);
                        }
                    }
                });
            }

            $(".editContact").click(function () {
                var sEditContactID = $(this).parent('li').attr("ContactID");
                ContactDept = $("#contact_category_select").val();
                window.location = "ContactDirMgmt.aspx?ContactDept=" + encodeURIComponent(ContactDept) + "&ContactID=" + sEditContactID;
            });
        });
    </script>
    <!-- Script Ends -->
    <style type="text/css">
        .tooltip
        {
            color: #000000;
            outline: none;
            cursor: default;
            text-decoration: none;
            position: relative;
        }
        .tooltip span
        {
            margin-left: -999em;
            position: absolute;
        }
        .tooltip:hover span
        {
            border-radius: 5px 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            font-family: Calibri, Tahoma, Geneva, sans-serif;
            position: absolute;
            left: 1em;
            top: 1.5em;
            z-index: 99;
            margin-left: 0;
            width: 250px;
            color: white;
        }
        .info
        {
            background: black;
            padding: 0.3em 0.3em 0.3em 0.3em;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="txtContactTypeToAdd" runat="server" Value="" />
    <div class=" div-fullwidth iframeMainDiv">
        <div class="contactdir-container">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;">
                    <img src="Images/contact_icon.png" style="position: relative; float: left; margin-right: 5px;
                        margin-top: 3px;" />Contact Directory Management
                </h1>
                <asp:Label ID="lblDBTotal" runat="server" class="heading-content-right" Style="color: Red;
                    font-size: 22px;"></asp:Label>
            </div>
            <div class="div-pagebottom">
            </div>
            <div runat="server" class="div-fullwidth" id="divContainer">
                <div runat="server" class="div-halfwidth" id="divLeftContainer">
                    <div runat="server" class="div-fullwidth" id="divContactCategorySelect">
                    </div>
                    <div runat="server" class="div-fullwidth" id="DivCategoryContact" style='margin-bottom: 10px;'>
                    </div>
                </div>
                <div runat="server" class="div-fullwidth" style="float: right; width: 450px; padding: 10px;
                    box-shadow: 1px 1px 2px #ccc; border: 1px solid rgb(175, 172, 172); background-color: #f9f9f9;">
                    <h1 runat="server" id='AddEditHeading' class="heading" style="font-size: 25px; border-bottom: 3px solid #960F13;
                        width: 98%; margin-bottom: 10px;">
                        Add/Edit Contact</h1>
                    <div class="div-fullwidth">
                        <input runat="server" id="txtSearchContact" type="text" value="" placeholder="Search Employee Name or CPF No or Location"
                            class="autocompleteclass" style="background-image: url('../images/search-icon.jpg');
                            background-repeat: no-repeat; padding-left: 35px; width: 100%;" />
                    </div>
                    <div style="position: relative; float: left; top: 5px; left: 0px; padding: 2px 0px;
                        width: 99.7%; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
                        background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc;
                        margin-bottom: 20px;">
                        <h1 class="content" style="margin-left: 10px;">
                            Contact Type :
                        </h1>
                        <input type="radio" name="ContactType" id="rdbtnEmployee" value="NONEMP" style="float: left;
                            margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="rdbtnEmployee"
                                class="content" style="cursor: pointer;">Employee</label>
                        <input type="radio" name="ContactType" id="rdbtnLocation" value="LOCATION" style="float: left;
                            margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="rdbtnLocation"
                                class="content" style="cursor: pointer;">Location</label>
                        <input type="radio" name="ContactType" id="rdbtnRegEmp" value="REGEMP" style="display: none;" />
                    </div>
                    <div id="contact_to_add" style="display: none;">
                        <div class="div-fullwidth marginbottom emp_contact_to_add">
                            <div class="div-fullwidth">
                                <h1 class="content" style="margin-right: 2px; width: 95px;">
                                    CPF No.</h1>
                                <input runat="server" id="txtCPFNo" type="text" class="contactdir-inputbox" style="width: 120px;" />
                                <p id="notifyCpfExist" class="content" style="font-size: 11px; color: red; margin-left: 98px;
                                    display: none;">
                                </p>
                            </div>
                        </div>
                        <div class="div-fullwidth marginbottom">
                            <h1 class="content" style="margin-right: 2px; width: 95px;">
                                Name</h1>
                            <input runat="server" id="txtName" type="text" class="contactdir-inputbox" style="width: 353px;" />
                        </div>
                        <div runat="server" id="designation_div" class="div-fullwidth marginbottom emp_contact_to_add">
                            <h1 class="content" style="margin-right: 2px; width: 95px;">
                                Designation</h1>
                            <select runat="server" id="txtDesignation" class="flattxt" style="width: 352px;">
                            </select>
                        </div>
                        <div runat="server" id="department_div" class="div-fullwidth marginbottom">
                            <h1 class="content" style="margin-right: 2px; width: 95px;">
                                Department</h1>
                            <select runat="server" id="txtDepartment" class="flattxt" style="width: 352px;">
                            </select>
                        </div>
                        <div class="div-fullwidth marginbottom">
                            <div class="div-halfwidth">
                                <h1 class="content" style="margin-right: 2px; width: 95px;">
                                    Extn. No.</h1>
                                <input runat="server" id="txtExtn" type="text" class="contactdir-inputbox" style="width: 120px" />
                            </div>
                            <div class="div-halfwidth">
                                <h1 class="content" style="margin-right: 2px; width: 95px;">
                                    Office Ph.</h1>
                                <input runat="server" id="txtOff" type="text" class="contactdir-inputbox" style="width: 128px;" />
                            </div>
                        </div>
                        <div class="div-fullwidth marginbottom emp_contact_to_add">
                            <div class="div-halfwidth">
                                <h1 class="content" style="margin-right: 2px; width: 95px;">
                                    Resi. Ph.</h1>
                                <input runat="server" id="txtResi" type="text" class="contactdir-inputbox" style="width: 120px;" />
                            </div>
                            <div class="div-halfwidth">
                                <h1 class="content" style="margin-right: 2px; width: 95px;">
                                    Mobile No.</h1>
                                <input runat="server" id="txtMob" type="text" class="contactdir-inputbox" style="width: 128px;" />
                            </div>
                        </div>
                        <div class="div-fullwidth marginbottom emp_contact_to_add">
                            <h1 class="content" style="margin-right: 2px; width: 95px;">
                                ONGC Email</h1>
                            <input runat="server" id="txtEmail" type="text" class="contactdir-inputbox" style="width: 353px;" />
                        </div>
                        <div class="div-fullwidth marginbottom emp_contact_to_add">
                            <h1 class="content" style="margin-right: 2px; width: 95px;">
                                Other Email</h1>
                            <input runat="server" id="txtEmail2" type="text" class="contactdir-inputbox" style="width: 353px;" />
                        </div>
                        <h1 class="heading" style="font-size: 25px; border-bottom: 3px solid #960F13; width: 98%;">
                        </h1>
                        <div style="position: relative; float: left; width: 100%; margin-top: 10px;">
                            <input id="reset_fields" type="button" class="g-button g-button-share" style="float: right;"
                                value="REFRESH" />
                            <input runat="server" id="SaveNewContact" type="button" class="g-button g-button-submit"
                                value="SAVE" style="margin-right: 10px; float: right;" />
                            <input runat="server" id="cmdDeleteContact" type="button" class="g-button g-button-red"
                                value="DELETE" style="margin-right: 10px; float: right;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
