﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetPrivilege.aspx.cs" Inherits="ONGCUIProjects.SetPrivilege" %>
<script type="text/javascript">
    if ($.browser.msie && $.browser.version == 8) {
        $('.style1-pagetop-container').css("width", "860px");
        $('.style1-pagetop-container').css("margin-left", "0");
        $('.topmiddle').css("width", "800px");
        $('.style1-pagemiddle-container').css("width", "860px");
        $('.style1-pagemiddle-container').css("margin-left", "0");
        $('.middlemid').css("width", "800px");
        $('.style1-pagebottom-container').css("width", "860px");
        $('.style1-pagebottom-container').css("margin-left", "0");
        $('.bottommiddle').css("width", "800px");
        $('#Div1').css("width", "340px");
    }

    $("#select_all").live("click", function () {
        if ($(this).attr('checked')) {
            $('.inputPriviledge').attr("checked", "checked");
        } else {
            $('.inputPriviledge').removeAttr("checked");
            $(this).removeAttr("checked");
        }
    });

    $(".inputPriviledge").live("click", function () {
        $("#select_all").removeAttr("checked");
    });
</script>
<div class="setpriviledge-container">
    <form id="form1" runat="server" style="position: relative; float: left;">
        <div class="style1-pagetop-container">
            <div class="topleft"></div>
            <div class="topmiddle">
                <h1 class="content" style="margin-top: 27px; font-size: 25px;">User Priviledge Management</h1>
                <asp:Button id="cmd_cancel" runat="server" text="CLOSE" onclientclick="return false;" class="g-button g-button-red" style="float: right; text-decoration: none; cursor: pointer; margin-top: 27px; "/>
                <asp:Button id="save_privilage" runat="server" text="SAVE" onclientclick="return false;" class="g-button g-button-share" style="float: right; text-decoration: none; cursor: pointer; margin-right: 10px; margin-top: 27px;" />
                <div class="g-button g-button-share" style="float: right; text-decoration: none; cursor: pointer; margin-right: 10px; margin-top: 27px;">
                    <input type="checkbox" id="select_all" style="margin-top:7px;position: relative;float: left;">
                    <label class="content" for="select_all" style=" margin-top: 0px; margin-left: 5px; color: white;">Select All</label>
                </div>
            </div>
            <div class="topright"></div>
        </div>
        <div class="style1-pagemiddle-container">
            <div class="middleleft"></div>
            <div class="middlemid">
                <h2 style="margin-top: 5px; margin-bottom: 5px; text-align: center; font-family: 'Bookman Old Style',serif;"></h2>
                <div runat="server" id="privilage_list" class="priviledgelist-container">
    
                </div>
                <div id="Div1" style="text-align: justify; margin-bottom: 10px; float: right; height: 408px; width: 370px; border: 1px solid #ccc; margin-left: 2px; background-color: white; line-height: 1.5; font-size: 13pt;">
                    <ul style="padding-left:30px;padding-right:20px;">
                        <li class="setpriviledge_divRight_li">Default Privileges are already assigned to the selected employee. Those are checked and disabled. You can’t change these settings.</li>
                        <li class="setpriviledge_divRight_li">If you are setting privilege for selected user for first time some mandatory privilege are already selected. You can change these settings.</li>
                        <li class="setpriviledge_divRight_li">If you are editing the privilege for selected user, assigned privileges are already checked.</li>
                        <li class="setpriviledge_divRight_li">Select Privilege and click “SAVE”.</li>
                    </ul>
                </div>
                    <asp:Label id="lbl_cpf_no" runat="server"></asp:Label>
            </div>
            <div class="middleright"></div>
        </div>
        <div class="style1-pagebottom-container">
            <div class="bottomleft"></div>
            <div class="bottommiddle"></div>
            <div class="bottomright"></div>
        </div>
    </form>
</div>
<p>
    &nbsp;</p>
