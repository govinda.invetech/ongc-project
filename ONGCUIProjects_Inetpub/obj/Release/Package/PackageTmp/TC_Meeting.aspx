<%@ Page Language="c#" CodeBehind="TC_Meeting.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.TC_Meeting" %>

<!DOCTYPE html>
<html>
<head>
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- Script Starts -->
    <script type="text/javascript">
		    $(document).ready(function () {
            $("#firstdiv").hide();
            $("#seconddiv").hide();
             $("#set_metting").hide();
            
            var meeting_type="";
            $('input[type="radio"][name="my_hint"]').change(function(){
                meeting_type=$(this).val();
                if(meeting_type=='TenderCommittee'){
                 $("#insert_main_div").empty();
                  $("#total_theory").html('');
                  $("#set_metting").show();
                    $("#firstdiv").show();
                    $("#seconddiv").hide();
                }else if(meeting_type=='OtherMeeting'){
                 $("#insert_main_div").empty();
                  $("#total_theory").html('');
                  $("#set_metting").show();
                    $("#firstdiv").hide();
                    $("#seconddiv").show();
                }
            });

		        $(document).ajaxStop($.unblockUI);
		         $("#file_date").datepicker({
                minDate: '0',
                });
		        $("#meeting_date").datepicker({
                minDate: '0',
                });
                 $("#txtdate2").datepicker({
                minDate: '0',
                });
                 $("#filedate1").datepicker({
                minDate: '0',
                });
                 $("#meeting_date1").datepicker({
                minDate: '0',
                });
		       // $("#txtmeeting_time").timepicker();
		        $("#add_memberfancy_box").fancybox();
		   

		        var mainmenuSearch_options1, mainmenuSearch_a1;

		        jQuery(function () {

		            var options = {

		                serviceUrl: 'services/auto.aspx',

		                onSelect: mainmenuSearch_onAutocompleteSelect1,

		                deferRequestBy: 0, //miliseconds

		                params: { type: 'emp_list', limit: '10' },

		                noCache: true //set to true, to disable caching

		            };

		            mainmenuSearch_a1 = $("#search_member_name").autocomplete(options);

		        });



		        var mainmenuSearch_onAutocompleteSelect1 = function (mainmenuSearch_value1, mainmenuSearch_data11) {
		            $("#search_member_name").val(mainmenuSearch_value1);
		            //$("#add_member_list").attr('member_cpf_no', mainmenuSearch_data11);
                    temp_count=temp_count+1;
                     var this_hint = 0;
		            var my_cpf_no = mainmenuSearch_data11;

		            var my_name = mainmenuSearch_value1;
                   
		            if (my_cpf_no == undefined || my_name == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Search member name and  Add Correct Member Name"
		                });
		                return false;
		            }
                    if($(".my_cmplt_strp").size()>0){
		            $(".my_cmplt_strp").each(function () {
		                if ($(this).attr('cpf_no') == my_cpf_no) {
                        alert("Kuch hua");
		                    this_hint = 1;
		                }
		            });
                    }
		            if (this_hint == 1) {
		                $("#add_member_list").attr('member_cpf_no', '');
		              
		                $("#search_member_name").val('');

		                return false;

		            }
		            var my_value = my_name + '`' + my_cpf_no;
		            var abcd = " <div class='my_cmplt_strp' cpf_no='"+my_cpf_no+"' div_value='" + my_value + "'  style='position:relative; float:left; width:98%;  background-color: #F5F5F5; color: black; margin-top:8px;padding: 10px; padding-top: 7px;border: 1px dashed rgb(163, 158, 158);' >";
		            abcd = abcd + " <p class='my_strip_count content'>[" + temp_count + ".]</p>";
		            abcd = abcd + "<p class=\"content\">CPF No. : " + my_cpf_no + "</p>";
		            abcd = abcd + " <p class=\"content\">NAME : " + my_name + "</p>";

		            abcd = abcd + " <a id='delete_me' href='javascript:void(0);'><img style='position:relative;margin-top:5px; float:right;'src=images/cross.png /></a></div>";
		            $("#insert_main_div").append(abcd);		           
		            $("#add_member_list").attr('member_cpf_no', '');		           
		            $("#search_member_name").val('');
                     theory_text();

		        }

                //autocomplete Member name
var Current_tr="";
 $("input.txtMemberName:not(.ui-autocomplete-input)").live("focus", function (event) {
 //alert("abhay");
  Current_tr = $(this).parent('td').parent('tr').attr('replaceme');
                    var CompanySearch_options, CompanySearch_a;
                    var options = {
                        serviceUrl: 'services/auto.aspx',
                        onSelect: CompanySearch_onAutocompleteSelect,
                        deferRequestBy: 0, //miliseconds
                        params: { type: 'emp_for_guesthouse', limit: '10' },
                        noCache: true //set to true, to disable caching
                    };
                    CompanySearch_a = $(this).autocomplete(options);
                });

         
                 var CompanySearch_onAutocompleteSelect = function (CompanyVisitorSearch_value, CompanyVisitorSearch_data) {
                    if (CompanyVisitorSearch_data != -1) {
                        //Ranjeet Kumar~22~M
                        var CompanyVisitorSearch_data_arr = CompanyVisitorSearch_data.split('~');
                        $('tr.MemberDetails[replaceme="' + Current_tr + '"]').children('td').children('input[name="Designation"]').val(CompanyVisitorSearch_data_arr[1]);
                    }
                    $('div.autocomplete').hide();
                }


                // tender Meeting Type Serach
                var mainmenuSearch_options11, mainmenuSearch_a11;

		        jQuery(function () {

		            var options = {

		                serviceUrl: 'services/auto.aspx',

		                onSelect: mainmenuSearch_onAutocompleteSelect11,

		                deferRequestBy: 0, //miliseconds

		                params: { type: 'TENDER_MEETING', limit: '10' },

		                noCache: true //set to true, to disable caching

		            };

		            mainmenuSearch_a11 = $("#ddlmeetingtype").autocomplete(options);

		        });



		        var mainmenuSearch_onAutocompleteSelect11 = function (mainmenuSearch_value11, mainmenuSearch_data111) {
		           if(mainmenuSearch_data111==-1){
                    $("#ddlmeetingtype").val("");
		        
                   }else{
                    $("#ddlmeetingtype").val(mainmenuSearch_value11);
		         getmeetingtendertypedetail(mainmenuSearch_value11);
                   }
                   

		        }
                /**************End Tendet Meeting Type**************/


                /**************Starts chaired by search**************/
                  var mainmenuSearch_options111, mainmenuSearch_a111;

		        jQuery(function () {

		            var options = {

		                serviceUrl: 'services/auto.aspx',

		                onSelect: mainmenuSearch_onAutocompleteSelect111,

		                deferRequestBy: 0, //miliseconds

		                params: { type: 'emp_list', limit: '10' },

		                noCache: true //set to true, to disable caching

		            };

		            mainmenuSearch_a111 = $("#dealing_member_name").autocomplete(options);

		        });



		        var mainmenuSearch_onAutocompleteSelect111 = function (mainmenuSearch_value111, mainmenuSearch_data1111) {
		           if(mainmenuSearch_data1111==-1){
                    $("#dealing_member_name").val("");
                    $("#dealing_member_cpf_no").val("");
		        
                   }else{                  
                    $("#dealing_member_cpf_no").val(mainmenuSearch_data1111);
                   }
                   

		        }

                /**************End chaired by search**************/

                //start focus function
                $("#ddlmeetingtype").focus(function(){
                 $("#ddlmeetingtype").val("");
                });
                 $("#dealing_member_name").focus(function(){
                 $("#dealing_member_name").val("");
                   $("#dealing_member_cpf_no").val("");
                });
                //end focus function
		        var temp_count = 0;
		       
		        /*$("#add_member_list").click(function () {
		            temp_count=temp_count+1;
                     var this_hint = 0;
		            var my_cpf_no = $(this).attr('member_cpf_no');

		            var my_name = $("#search_member_name").val();
                   
		            if (my_cpf_no == undefined || my_name == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Search member name and  Add Correct Member Name"
		                });
		                return false;
		            }
                    if($(".my_cmplt_strp").size()>0){
		            $(".my_cmplt_strp").each(function () {
		                if ($(this).attr('cpf_no') == my_cpf_no) {

		                    this_hint = 1;
		                }
		            });
                    }
		            if (this_hint == 1) {
		                $("#add_member_list").attr('member_cpf_no', '');
		              
		                $("#search_member_name").val('');

		                return false;

		            }
		            var my_value = my_name + '`' + my_cpf_no;
		            var abcd = " <div class='my_cmplt_strp' cpf_no='"+my_cpf_no+"' div_value='" + my_value + "'  style='position:relative; float:left; width:98%;  background-color: #F5F5F5; color: black; margin-top:8px;padding: 10px; padding-top: 7px;border: 1px dashed rgb(163, 158, 158);' >";
		            abcd = abcd + " <p class='my_strip_count content'>[" + temp_count + ".]</p>";
		            abcd = abcd + "<p class=\"content\">CPF No. : " + my_cpf_no + "</p>";
		            abcd = abcd + " <p class=\"content\">NAME : " + my_name + "</p>";

		            abcd = abcd + " <a id='delete_me' href='javascript:void(0);'><img style='position:relative;margin-top:5px; float:right;'src=images/cross.png /></a></div>";
		            $("#insert_main_div").append(abcd);		           
		            $("#add_member_list").attr('member_cpf_no', '');		           
		            $("#search_member_name").val('');
                     theory_text();
		        });*/

		        $("#delete_me").live('click', function () {
		            $(this).parent(".my_cmplt_strp").remove();
		            var hello_count = 1;
		            $(".my_strip_count").each(function () {
		                $(this).text("[" + hello_count + "]");
		                hello_count = hello_count + 1;
		            });
		            theory_text();
		           

		        });

		        //click set meeting
		        $("#set_metting").click(function () {





                 if(meeting_type=='TenderCommittee'){
                // alert("1st checkbox");
                    var file_no = $("#file_no").val();
                    //alert(file_no)
                    var file_date=$("#file_date").val();
                    var subject=$("#subject").val();
		            var meeting_date = $("#meeting_date").val();
		            var meeting_time = $("#meeeting_time").val();
		            var location = $("#location").val();
                    var member_full_list="";
		             $(".MemberDetails").each(function () {
                    var MemberName = $(this).children('td').children('input[name=MemberName]').val().trim();
                    var MemberDesig = $(this).children('td').children('input[name=Designation]').val().trim();
                    var MemberRole= $(this).children('td').children('input[name=Role]').val().trim();
                    if (MemberName != "") {
                        member_full_list += "name`" + MemberName + "`desig`" + MemberDesig + "`role`" + MemberRole + "~";
                    }
                });
                
                    if (file_no == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter valid File Name"
		                });
		                return false;
		            } 
		            if (file_date == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Valid Date"
		                });
		                return false;
		            }
                 
		            if (subject == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Valid Subject"
		                });
		                return false;
		            }

                    if (meeting_date == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Valid Date"
		                });
		                return false;
		            }

                    if (meeting_time == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Valid Time"
		                });
		                return false;
		            }

                    if (location == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Location"
		                });
		                return false;
		            }
                     if (member_full_list == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Member Name"
		                });
		                return false;
		            }
		           // $.blockUI({ message: '<h2><img src="images/busy.gif" /> Request Sending...</h2>' });
		            $.ajax({
		                type: "GET",
		                url: "services/meeting/addmettingdetail.aspx",
		                data: "hint="+ meeting_type + "&file_no=" + file_no  + "&file_date=" + file_date + "&subject=" + subject + "&meeting_date=" + meeting_date  + "&meeting_time=" + meeting_time  + "&location=" + location  + "&member_full_list=" + member_full_list,
		                success: function (msg) {
		                    //alert(msg);

		                    var values = msg.split("~");

		                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
		                        $.gritter.add({
		                            title: "Notification",
		                            image: "images/tick.png",
		                            text: values[2]
		                        });

		                        $("#file_no").val('');
		                        $("#file_date").val('');
		                        $("#subject").val('');
		                        $("#meeting_date").val('');
		                        $("#meeeting_time").val('');
		                        $("#location").val('');
                                 $("#MemberDetails").empty();
   
		                    } else if (values[0] == "FALSE" && values[1] == "ERR") {
		                        $.gritter.add({
		                            title: "Notification",

		                            text: values[2]
		                        });
		                    } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
		                        $.gritter.add({
		                            title: "Notification",

		                            text: values[2]
		                        });
		                    }
		                }
		            });
                }




                else if(meeting_type=='OtherMeeting'){

                    var fileno1 = $("#fileno1").val();
                    var filedate1=$("#filedate1").val();
                    var ddlmeetingtype=$("#ddlmeetingtype").val();
		            var subject1 = $("#subject1").val();
		            var dealing_member_name = $("#dealing_member_name").val();
		            var dealing_member_cpf_no = $("#dealing_member_cpf_no").val();
		            var meeting_date1 = $("#meeting_date1").val();
                    var meeting_time1 = $("#meeting_time1").val();
                    var txtvenuename = $("#txtvenuename").val();
                    var txtmeeting_agenda = $("#txtmeeting_agenda").val();
		            


		            if (fileno1 == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter File No"
		                });
		                return false;
		            } 
                    if (filedate1 == "" ) {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please select valid date"
		                });
		                return false;
		            } if (ddlmeetingtype == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Metting Type"
		                });
		                return false;
		            }
		            if (subject1 == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter subject"
		                });
		                return false;
		            }
                 
		            if (dealing_member_name == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please select Valid Name"
		                });
		                return false;
		            }


//                     if (dealing_member_cpf_no == "") {
//		                $.gritter.add({
//		                    title: "Notification",

//		                    text: "Please Enter Valid Date"
//		                });
//		                return false;
//		            }

                     if (meeting_date1 == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please select Valid Meeting Date"
		                });
		                return false;
		            }

                     if (meeting_time1 == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Valid Time"
		                });
		                return false;
		            }

                    if (txtvenuename == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Venue"
		                });
		                return false;
		            }

                    if (txtmeeting_agenda == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Enter Meeting Agenda"
		                });
		                return false;
		            }

                   // alert(fileno1+filedate1+ddlmeetingtype+subject1+dealing_member_name+dealing_member_cpf_no+meeting_date1+meeting_time1+txtvenuename+txtmeeting_agenda);

		            var div_value_print = "";
		            $(".my_cmplt_strp").each(function () {
		                div_value_print = div_value_print + $(this).attr('div_value') + "~";
		            });
		            if (div_value_print == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Add Member For Meeting"
		                });
		                return false;
		            }
                    //alert(div_value_print);
                   
		            //$.blockUI({ message: '<h2><img src="images/busy.gif" /> Request Sending...</h2>' });
		            $.ajax({
		                type: "GET",
		                url: "services/meeting/addmettingdetail.aspx",
		                data: "fileno1=" + fileno1 + "&filedate1=" + filedate1 + "&ddlmeetingtype=" + ddlmeetingtype + "&subject1=" + subject1 + "&dealing_member_name=" + dealing_member_name + "&dealing_member_cpf_no=" + dealing_member_cpf_no + "&meeting_date1=" + meeting_date1+"&meeting_time1="+meeting_time1+"&txtvenuename="+txtvenuename+"&txtmeeting_agenda="+txtmeeting_agenda+"&div_value_print="+div_value_print,
		                success: function (msg) {
		                    //alert(msg);

		                    var values = msg.split("~");

		                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
		                        $.gritter.add({
		                            title: "Notification",
		                            image: "images/tick.png",
		                            text: values[2]
		                        });

		                        $("#fileno1").val('');
		                        $("#filedate1").val('');
		                        $("#ddlmeetingtype").val('');
		                        $("#subject1").val('');
		                        $("#dealing_member_name").val('');
		                        $("#dealing_member_cpf_no").val('');
                                $("#meeting_date1").val('');
                                $("#meeting_time1").val('');
		                        $("#txtvenuename").val('');
                                $("#txtmeeting_agenda").val('');
                                $("#total_theory").html('');
                                $("#insert_main_div").empty();
		                    } else if (values[0] == "FALSE" && values[1] == "ERR") {
		                        $.gritter.add({
		                            title: "Notification",

		                            text: values[2]
		                        });
		                    } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
		                        $.gritter.add({
		                            title: "Notification",

		                            text: values[2]
		                        });
		                    }
		                }
		            });
                }
		        });
		        
		        // Addtobucket

		        var my_strip_hint = 0;
		        $("#Addtobucket").live('click', function () {

		            var output = "";
		            $(".select_strip").each(function () {
		                if ($(this).attr('checked')) {
		                    output = output + $(this).attr('my_data') + "~";

		                }


		            });
		            if (output == "") {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Select Member First"
		                });

		            } else {
		                // validate

		                var for_print = "";
		                output = output.substr(0, output.length - 1);
		                output_arr = output.split("~");

		                for (i = 0; i < output_arr.length; i++) {
		                    var my_output = output_arr[i].split("`");
		                    var cpf_no = my_output[0];
		                    var validation_hint = 0;
		                    $(".my_bucket_strip").each(function () {
		                        if ($(this).attr('mycpf_no') == cpf_no) {

		                            validation_hint = 1;


		                        }

		                    });
		                    if (validation_hint == 0) {

		                        for_print = for_print + "<label class='strip-box'> <input class='my_bucket_strip' type='checkbox' style='position: relative ; float : left; display: none;' my_name='" + my_output[1] + "' mycpf_no='" + my_output[0] + "' my_data='" + my_output[0] + '`' + my_output[1] + "' '><p style='position: relative ; float: left; width: 94%;'>Name : " + my_output[1] + "  , CPF No. " + my_output[0] + "</p> <a id='delete_me_fancy' style='position:relative; float:right;height: 15px; width: 15px; display: block;background-image:url(../images/cross.png); margin-top: 4px;' href='javascript:void(0);'></a></label>";
		                    }




		                }

		                if (my_strip_hint == 0 && for_print != "") {
		                    $("#bucket_div").html(for_print);
		                    my_strip_hint = my_strip_hint + 1;
		                } else if (my_strip_hint == 1 && for_print != "") {
		                    $("#bucket_div").append(for_print);
		                }
		                var size = $(".my_bucket_strip").size();
		                $("#bucket_total").text("Bucket Area ( Total " + size + " Member Added )");
		            }
		        });

		        //delete_me
		        $("#delete_me_fancy").live('click', function () {

		            if (confirm("Are you sure to delete it ?")) {

		                $(this).parent(".strip-box").remove();
		            }
		            var size = $(".my_bucket_strip").size();
		            $("#bucket_total").text("Bucket Area ( Total " + size + " Member Added )");
		        });

		        //save_strip

		        $("#save_strip").live('click', function () {
		            if ($(".my_bucket_strip").size() == 0) {
		                $.gritter.add({
		                    title: "Notification",

		                    text: "Please Add Member in Bucket Area First"
		                });
		                return false;
		            }
		            $(".my_bucket_strip").each(function () {
		                var fancy_Cpf_no = $(this).attr('mycpf_no');
		                var facny_my_name = $(this).attr('my_name');
		                var my_hint = 0;
		                $(".my_cmplt_strp").each(function () {
                        
		                    if ($(this).attr('cpf_no') == fancy_Cpf_no) {

		                        my_hint = 1;
		                    }
		                });

		                if (my_hint == 0) {
		                    temp_count = temp_count + 1;
		                    var my_value = facny_my_name + '`' + fancy_Cpf_no;
		                    var abcd = " <div class='my_cmplt_strp' cpf_no='"+fancy_Cpf_no+"' div_value='" + my_value + "'  style='position:relative; float:left; width:98%;  background-color: #F5F5F5; color: black; margin-top:8px;padding: 10px; padding-top: 7px;border: 1px dashed rgb(163, 158, 158);' >";
		                    abcd = abcd + " <p class='my_strip_count content' style='position:relative; float:left; margin-left:5px;'>[" + temp_count + ".]</p>";
		                    abcd = abcd + "<p class=\"content\">CPF No. : " + fancy_Cpf_no + "</p>";
		                    abcd = abcd + " <p class=\"content\">NAME : " + facny_my_name + "</p>";

		                    abcd = abcd + " <a id='delete_me' href='javascript:void(0);'><img style='position:relative;margin-top:5px; float:right;'src=images/cross.png /></a></div>";
		                    $("#insert_main_div").append(abcd);
		                  

		                }
		            }); 
                    $("#bucket_div").empty();
		            my_strip_hint = 0;
		            
                    $.gritter.add({
		                    title: "Notification",

		                    text: "Member Added In Meeting Schedule Successfully. Add Another Member then Save or Close. "
		                });
                        theory_text();
		        });
                function theory_text(){
                var size=$(".my_cmplt_strp").size() ;
                 $("#total_theory").text("Total " + size + " Member Added");
                 var a=1;
                 $(".my_strip_count").each(function(){
                 $(this).text("["+a+".]");
                a=a+1;
                 });
                }
                // function getmeetingtendertypedetail
                function getmeetingtendertypedetail(tender_type){
                  $.blockUI({ message: '<h2><img src="images/busy.gif" /> Please Wait...</h2>' });
		            $.ajax({
		                type: "GET",
		                url: "services/meeting/getlasttenderdetail.aspx",
		                data: "tender_Type="+tender_type,
		                success: function (msg) {
		                  //alert(msg);

		                    var values = msg.split("~");

		                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
		                      $("#dealing_member_name").val(values[3]);
                                $("#dealing_member_cpf_no").val(values[4]);
                                $("#txtvenuename").val(values[5]);
                                $("#txtmeeting_detail").val(values[6]);
                                $("#txt_title").val(values[7]);
                                $("#insert_main_div").html(values[8]);
                                $("#total_theory").text("Total "+values[9]+" Member Added");
		                    } else if (values[0] == "FALSE" && values[1] == "ERR") {
		                        $.gritter.add({
		                            title: "Notification",

		                            text: values[2]
		                        });
		                    } 
		                }
		            });

                }
		    });
    </script>
    <style>
        table
        {
            border: none;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                <img src="Images/meeting_icon.png" style="position: relative; float: left; margin: 0px 10px 0 0;" />Schedule
                Meeting</h1>
        </div>
        <div class="div-pagebottom">
        </div>
        <div id="div_for_approve" style="position: relative; float: left; left: 0px; padding: 2px 10px 2px 10px;
            width: 98%; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
            background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc;
            margin-bottom: 10px;">
            <div style="position: relative; float: left; text-align: center;">
                <p class="content" style='margin-top: 6px; font-weight: bold;'>
                    Select Meeting Type :
                </p>
                <input type="radio" name="my_hint" id="optTC" value="TenderCommittee" style="float: left;
                    margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="optTC"
                        class="content" style="cursor: pointer; margin-right: 20px; margin-top: 6px;
                        font-weight: bold;">TENDER COMMITTEE</label>
                <input type="radio" name="my_hint" id="optOther" value="OtherMeeting" style="float: left;
                    margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="optOther"
                        class="content" style="cursor: pointer; margin-top: 6px; font-weight: bold;">OTHER
                        MEETING</label>
            </div>
        </div>
        <div id="firstdiv" class='div-fullwidth' style='float: right; width: 97%; padding: 10px;
            box-shadow: 1px 1px 2px #ccc; border: 1px solid rgb(175, 172, 172); background-color: #f9f9f9;'>
            <div class='div-fullwidth marginbottom'>
                <div class="div-fullwidth" style='width: 70%'>
                    <p class="content" style='width: 150px;'>
                        File No :</p>
                    <input id="file_no" type="text" style="position: relative; float: left; width: 75%;" />
                </div>
                <div class="div-fullwidth" style='width: 25%; float: right;'>
                    <p class="content" style="width: 50px;">
                        Date :</p>
                    <input id="file_date" type="text" style="position: relative; float: right; width: 150px;" />
                </div>
            </div>
            <div class='div-fullwidth marginbottom'>
                <div class="div-fullwidth" style='width: 100%'>
                    <p class="content" style='width: 150px;'>
                        Subject :</p>
                    <textarea class='content' id="subject" type="text" style="margin-right: 0px; width: 50%;
                        max-width: 83%; height: 80px; max-height: 80px"></textarea>
                </div>
            </div>
            <div class='div-fullwidth marginbottom'>
                <div class="div-fullwidth">
                    <p class="content" style='width: 150px;'>
                        Meeting Date</p>
                    <input id="meeting_date" type="text" style="position: relative; float: left; margin-right: 10px;" />
                    <p class="content" style="width: 50px;">
                        Time</p>
                    <input id="meeeting_time" type="text" style="position: relative; float: left;" />
                    <p class="content" style="width: 50px; top: 0px; left: 17px;">
                        Location</p>
                    <input id="location" type="text" style="position: relative; float: left; width: 245px;
                        top: 0px; left: 25px;" /></div>
            </div>
            <div>
                <table id="Table1" class="tftable" border="1">
                    <tbody>
                        <tr id="VisitorForm">
                            <th style="width: 33%;">
                                Member Name
                            </th>
                            <th style="width: 33%;">
                                Designation
                            </th>
                            <th style="width: 33%;">
                                Role
                            </th>
                        </tr>
                        <tr class="MemberDetails" replaceme="1">
                            <td>
                                <input placeholder="Member Name" name="MemberName" class="txtMemberName" cssclass="flattxt"
                                    style="padding-top: 4px; width: 100%;" autocomplete="off">
                            </td>
                            <td>
                                <input name="Designation" placeholder="Designation" class="txtDesignation" cssclass="flattxt"
                                    style="padding-top: 4px; width: 100%;">
                            </td>
                            <td>
                                <input name="Role" placeholder="Role" class="txtRole" cssclass="flattxt" style="padding-top: 4px;
                                    width: 100%;">
                            </td>
                        </tr>
                        <tr class="MemberDetails" replaceme="2">
                            <td>
                                <input placeholder="Member Name" name="MemberName" class="txtMemberName" cssclass="flattxt"
                                    style="padding-top: 4px; width: 100%;" autocomplete="off">
                            </td>
                            <td>
                                <input name="Designation" placeholder="Designation" class="txtDesignation" cssclass="flattxt"
                                    style="padding-top: 4px; width: 100%;">
                            </td>
                            <td>
                                <input name="Role" placeholder="Role" class="txtRole" cssclass="flattxt" style="padding-top: 4px;
                                    width: 100%;">
                            </td>
                        </tr>
                        <tr class="MemberDetails" replaceme="3">
                            <td>
                                <input placeholder="Member Name" name="MemberName" class="txtMemberName" cssclass="flattxt"
                                    style="padding-top: 4px; width: 100%;" autocomplete="off">
                            </td>
                            <td>
                                <input name="Designation" placeholder="Designation" class="txtDesignation" cssclass="flattxt"
                                    style="padding-top: 4px; width: 100%;">
                            </td>
                            <td>
                                <input name="Role" placeholder="Role" class="txtRole" cssclass="flattxt" style="padding-top: 4px;
                                    width: 100%;">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="div_team_members" class="div-fullwidth">
                </div>
            </div>
        </div>
        <div style="position: relative; float: left; width: 100%">
            <div id="seconddiv" class="div-fullwidth" style="background-color: #f8f8f8; border: 1px solid #ccc;
                padding: 0 10px 10px 10px; width: 97%; min-width: 870px;">
                <p class="content" style="width: 100%; border-bottom: 2px solid #34343b; line-height: 35px;
                    color: #E65959; font-size: 22px; font-weight: bold; margin-bottom: 10px;">
                    Fill Basic Details<a id="add_memberfancy_box" href="addmemberinmeeting.aspx" class="buttonAddMultipleMeetingMembers"><img
                        src="Images/documentstyles/style2/plus_icon.png" style="position: relative; float: left;
                        width: 30px; margin: 2px 3px 0 0;" />Add Multiple Meeting Members</a></p>
                <div style="position: relative; float: left; width: 520px;">
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            File No :
                        </p>
                        <input id="fileno1" value="" type="text" style="position: relative; float: right;
                            width: 50%;" />
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Date :
                        </p>
                        <input id="filedate1" value="" type="text" style="position: relative; float: right;
                            width: 50%;" />
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Search or Enter Meeting Type :
                        </p>
                        <input id="ddlmeetingtype" value="" type="text" class="autocompleteclass" style="background-image: url('../images/search-icon.jpg');
                            background-repeat: no-repeat; padding-left: 35px; width: 50%; float: right;" />
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            SUBJECT :
                        </p>
                        <input id="subject1" value="" type="text" style="position: relative; float: right;
                            width: 50%;" />
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Chaired By (NAME):
                        </p>
                        <input id="dealing_member_name" value="" type="text" class="autocompleteclass" style="background-image: url('../images/search-icon.jpg');
                            background-repeat: no-repeat; padding-left: 35px; width: 50%; float: right;" />
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Chaired By (CPF NO):
                        </p>
                        <input id="dealing_member_cpf_no" value="" type="text" style="position: relative;
                            float: right; width: 50%;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Meeting Date :
                        </p>
                        <div style="position: relative; float: right; width: 50%;">
                            <input id="meeting_date1" value="" type="text" style="position: relative; float: right;
                                width: 100%;" />
                        </div>
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Meeting Time :
                        </p>
                        <div style="position: relative; float: right; width: 50%;">
                            <input id="meeting_time1" value="" type="text" style="position: relative; float: right;
                                width: 100%;" />
                        </div>
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Venue of Proposed TC Meeting :
                        </p>
                        <input id="txtvenuename" value="" type="text" class="autocompleteclass" style="background-image: url('../images/search-icon.jpg');
                            background-repeat: no-repeat; padding-left: 35px; width: 50%; float: right;" />
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <p class="content">
                            Meeting Agenda :
                        </p>
                        <input id="txtmeeting_agenda" value="" type="text" style="position: relative; float: right;
                            width: 50%;" />
                    </div>
                </div>
                <div style="position: relative; float: right; width: 300px; border: 1px solid #ccc;
                    padding: 0 10px 10px 10px;">
                    <p class="content" style="text-align: right; border-bottom: 2px solid green; line-height: 35px;
                        width: 100%; margin-bottom: 10px;">
                        Add Meeting Members</p>
                    <div style="position: relative; float: left; width: 100%;">
                        <input id="search_member_name" type="text" style="background-image: url('../images/search-icon.jpg');
                            background-repeat: no-repeat; padding-left: 35px; width: 235px;" />&nbsp;
                    </div>
                </div>
            </div>
            <div class="div-fullwidth" style="width: 99%; min-width: 890px; margin-bottom: 30px;">
                <p id="total_theory" class="content" style="margin-top: 18px; font-size: 22px; color: green;">
                </p>
                <input id="set_metting" type="button" value="Set this Meeting" class="buttonBigGreen"
                    style="text-transform: capitalize; float: right;" />
                <div id="insert_main_div" style="position: relative; float: left; width: 100%; height: auto;
                    color: white; margin-top: 8px;">
                </div>
            </div>
        </div>
    </form>
    <center>
    </center>
</body>
</html>
