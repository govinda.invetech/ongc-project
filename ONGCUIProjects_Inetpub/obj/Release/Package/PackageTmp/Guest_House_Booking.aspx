<%@ Page language="c#" Codebehind="Guest_House_Booking.aspx.cs" AutoEventWireup="True" Inherits="WebApplication1.Guest_House_Booking" %>

<!DOCTYPE html>
<HTML>
	<HEAD>
		
		<link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
        <link href="css/css3.css" type="text/css" rel="stylesheet" />
        <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
        <!-- CSS Links Ends -->

        <!-- JS Links Starts -->       
        <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
        <script type="text/javascript" language="javascript" src="js/script.js"></script>
        <script type="text/javascript" language="javascript" src="js/slider.js"></script>

        <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
        <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>

        <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen">

        <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen">
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>     
		<script language="javascript">
		    $(document).ready(function () {
		        $(".cmdViewBooking").fancybox();

		    });
		       
          </script>  
          <style type="text/css">
                .tooltip {
			color: #000000; outline: none;
			cursor: default; text-decoration: none;
			position: relative;
		}
		.tooltip span {
			margin-left: -999em;
			position: absolute;
		}
		.tooltip:hover span {
			border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; 
			box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                        -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
			font-family: Calibri, Tahoma, Geneva, sans-serif;
			position: absolute; left: 1em; top: 1.5em; z-index: 99;
			margin-left: 0; width: 250px;
			color:White;
		}
		.info { background: black; padding: 0.3em 0.3em 0.3em 0.3em;	}
            </style>   
	</HEAD>
    
	<body>
		<form id="Form1" method="post" runat="server" class="MainInterface-iframe">
            <div class="div-fullwidth iframeMainDiv">
			    <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                    <h1 class="heading" style="width: auto;"><img src="Images/guesthouse_icon.png" style="position: relative; float: left; margin: 2px 5px 0 0;" />Guest House Booking History</h1>
                    <a href="guesthousedetailforapprove.aspx?my_hint=NEW" id="add_new_guest_house_request"  style="float:right; margin-right:10px; margin-top:5px;" class="g-button g-button-red" >Add New Guest House Request</a>
                </div>
                <div class="div-pagebottom"></div>
                <p runat="server" id="P1" class="content" style="color:red; font-size: 20px;" ></p>

                <div runat="server"  class="div-fullwidth" id="pening_detail_div" style="margin-top: 10px; ">

             


                </div>
                
            </div>	             
		</form>
         <script type="text/javascript" src="js/colortip-1.0-jquery.js"></script>
    <script type="text/javascript" src="js/colortip.js"></script>
	</body>
</HTML>
