﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Transport_Master.aspx.cs"
    Inherits="ONGCUIProjects.Transport_Master" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <!-- JS Links Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#cmdAddNewVehicle').fancybox({ modal: true });

            $("#txtAlloterName").bind('keyup', function (e) {
                $("#txtAlloterName").val(($("#txtAlloterName").val()).toLowerCase());
            });

            var AlloterSearch_options, AlloterSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: AlloterSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'TransportAlloter', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                AlloterSearch_a = $("#txtAlloterName").autocomplete(options);
            });

            var TransportAlloterCPF = "";
            var AlloterSearch_onAutocompleteSelect = function (AlloterSearch_value, AlloterSearch_data) {
                if (AlloterSearch_data == -1) {
                    $("#transport_allotment_name").val("");
                } else {
                    var AlloterSearchData_Arr = AlloterSearch_data.split("~");
                    TransportAlloterCPF = AlloterSearchData_Arr[0];
                }
            }

            $('#cmdSaveVehicle').click(function () {
                $('#Response').text('');
                var VehicleType = $('#txtVehicleType').val().trim();
                var VehicleNo = $('#txtVehicleNo').val().trim();
                var DriverName = $('#txtDriverName').val().trim();
                var DriverContact = $('#txtDriverContact').val().trim();

                if (VehicleType == "" || VehicleNo == "") {
                    $('#Response').text('Please fill mandatory fields');
                } else {
                    $.ajax({
                        type: "POST",
                        url: "services/transport/AddDeleteVehicle.aspx",
                        data: "type=ADD&VehicleType=" + VehicleType + "&VehicleNo=" + VehicleNo + "&DriverName=" + DriverName + "&DriverContact=" + DriverContact + "&AlloterCPF=" + TransportAlloterCPF,
                        success: function (msg) {
                            var msg_arr = msg.split('~');
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                $('#Response').text(msg_arr[2]);
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });

            $('.cmdDeleteVehicle').click(function () {
                var VehicleDelID = $(this).attr('id');
                if (confirm("Are you sure to delete this vehicle?")) {
                    $.ajax({
                        type: "POST",
                        url: "services/transport/AddDeleteVehicle.aspx",
                        data: "type=DEL&DelID=" + VehicleDelID,
                        success: function (msg) {
                            var msg_arr = msg.split('~');
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                alert(msg_arr[2]);
                                window.location = self.location;
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="txtHideActionType" runat="server" />
    <asp:HiddenField ID="txtHideBookingID" runat="server" />
    <div class="div-fullwidth iframeMainDiv" style="min-width: 905px;">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                <img src="Images/transport.png" style="position: relative; float: left; margin: 8px 5px 0 0;
                    opacity: 0.78;" />Transport / Driver Master</h1>
            <a href="#AddNewVehicle" id="cmdAddNewVehicle" style="float: right; margin-right: 10px;
                margin-top: 5px;" class="g-button g-button-red">Add New Transport</a>
        </div>
        <div class="div-pagebottom">
        </div>
        <div runat="server" id="div_full_form" class="div-fullwidth">
        </div>
    </div>
    <div style="display: none;">
        <div id="AddNewVehicle" style="position: relative; float: left; width: 400px;">
            <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A;">
                Add New Vehicle</h1>
            <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px;
                width: 100%;">
                <p class="content" style="width: 140px;">
                    Allotter Name<span style="color: Red;">(*)</span></p>
                <input name="txtAreaName" type="text" id="txtAlloterName" style="width: 250px;" />
            </div>
            <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px;
                width: 100%;">
                <p class="content" style="width: 140px;">
                    Vehicle Type<span style="color: Red;">(*)</span></p>
                <input name="txtAreaName" type="text" id="txtVehicleType" style="width: 250px;" />
            </div>
            <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px;
                width: 100%;">
                <p class="content" style="width: 140px;">
                    Vehicle No<span style="color: Red;">(*)</span></p>
                <input name="txtLocationName" type="text" id="txtVehicleNo" style="width: 250px;" />
            </div>
            <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px;
                width: 100%;">
                <p class="content" style="width: 140px;">
                    Driver Name</p>
                <input name="txtEmployee" type="text" id="txtDriverName" style="width: 250px;" />
            </div>
            <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px;
                width: 100%;">
                <p class="content" style="width: 140px;">
                    Driver Contact</p>
                <input name="txtEmployee" type="text" id="txtDriverContact" style="width: 250px;" />
            </div>
            <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">
            </h1>
            <div class="div-fullwidth">
                <p class="content" id="Response" style="width: auto; color: Red;">
                </p>
                <a class="g-button g-button-red" id="cmdClose" href="javascript:void(0)" onclick="window.location = self.location;"
                    style="float: right;">CLOSE</a> <a class="g-button g-button-submit" id="cmdSaveVehicle"
                        href="javascript:void(0)" style="float: right; margin-right: 10px">SAVE</a>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
