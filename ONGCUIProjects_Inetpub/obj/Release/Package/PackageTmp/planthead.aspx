﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="planthead.aspx.cs" Inherits="ONGCUIProjects.planthead" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="//fonts.googleapis.com/css?family=Droid+Serif:400,400italic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600" rel="stylesheet" type="text/css">
    <!--[if IE]>
                <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <link href="css/styleprofile.css" type="text/css" rel="stylesheet">
    <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="https://quote.fm/assets/ver_default/css/ie9.css" />
        <![endif]-->
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="https://quote.fm/assets/ver_default/css/ie8-and-down.css" />
        <![endif]-->
    <!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="https://quote.fm/assets/ver_default/css/ie7.css" />
        <![endif]-->
    <!--[if (gte IE 6)&(lte IE 8)]>
          <script type="text/javascript" src="https://quote.fm/assets/ver_default/js/misc/selectivizr-min.js"></script>
        <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>From the Desk of GGM - Plant Manager Uran</title>
</head>
<body class="loggedout">
    <div class="wrap">
        <section class="profile-intro" style="background-image: url('ongcplantbg-loginpage.jpg')">
                <div class="profile-intro-wrap">
                    <div class="profile-img">
                        <img src="Images/sksahu.JPG" alt="Sh. S. K. Sahu"/>
                    </div>
                    <div class="profile-info">
                        <h2 style="text-decoration: underline; font-style: italic; font-family: Monotype Corsiva;">Sh. S. K. Sahu</h2>
                        <p style="text-decoration: underline; font-style: italic; font-family: Monotype Corsiva;">GGM - Plant Manager Uran</p>
                    </div>
                </div>
            </section>
        <div id="content" class="has-bigintro">
            <!-- <a class="new_quotes" href="javascript:;">20 new recommendations</a> -->
            <div class="timeline" id="quotelist">
                <div class="items">
                    <!-- To Make New Post Copy The Section Tag Below and Paste -->
                    <form runat="server" id="frmPlantHead">
                    <asp:Repeater ID="rptrPlantHead" runat="server">
                        <ItemTemplate>
                            <section id="quote-91462" class="event-244758 quotewrapper ">        
                            <div class="recommendationbox">
                                <div class="recommendationbox-content">
                                    <blockquote class="quote">
                                        <a target="_blank" class="quotelink a_quote" href='<%# Eval("HREF") %>'>
                                            <span class="quote-lvl1">
                                                <span class="quote-lvl2">
                                                    <asp:Label runat="server" class="quote-lvl3"><%# Eval("MENU_TITLE") %></asp:Label>
                                                </span>
                                            </span>
                                        </a>                
                                    </blockquote>
                                </div>                               
                            </div>        
                        </section>
                        </ItemTemplate>
                    </asp:Repeater>
                    <!-- To Make New Post Copy From Here -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
