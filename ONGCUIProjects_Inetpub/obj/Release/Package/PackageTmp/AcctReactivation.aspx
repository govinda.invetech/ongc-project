﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcctReactivation.aspx.cs" Inherits="ONGCUIProjects.AcctReactivation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Account Reactivation</title>
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
        
        <img src="Images/account-locked.png" style=" position: relative; float: left; margin-left: 35%; margin-top: 30px;" />
        <div align="center" style="font-family: 'Bookman Old Style'; font-size: xx-large; width: 100%; position: relative; float: left;  margin-top: 20px;'">
            <p>Your account has been deactivated due to non activity since 60 days.</p>
            <asp:Label ID="lblResponseMessage" runat="server">You can request for Re-Activation.</asp:Label>
            <br />
        </div>
        <div align="center">
            <asp:LinkButton ID="cmdReActivate" style="padding: 15px 10px 15px 10px;" runat="server" 
                CssClass="g-button g-button-share" Font-Bold="True" Font-Size="XX-Large" 
                Height="32px" onclick="cmdReActivate_Click" Width="424px" 
                margin-top="20px">Activate My Account</asp:LinkButton>
    
        </div>
        </div>
    </form>
    
    </body>
</html>
