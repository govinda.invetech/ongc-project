﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateMenuFancyBox.aspx.cs" Inherits="ONGCUIProjects.CreateMenuFancyBox" %>
<script type="text/javascript">
    $("#lblMenuType").hide();
    $("#FileUploadControl").change(function () {
        if (this.files[0].size > 4194304) {
            alert("File Size Can Not be Greater Than 4 MB");
            $("#FileUploadControl").val("");
        } else if (this.files[0].size == 0) {
            alert("File Size Can Not be Zero");
            $("#FileUploadControl").val("");
        }
    });    
    

</script>
<form id="form1" runat="server" method="post" enctype="multipart/form-data">
<div runat="server" id="divCreateMenu" class="createlink-container">
<div runat="server" id="ThoughtOfDay">
<div class="createlink-divfullwidth">

    </div>
</div>
<div runat="server" id="MenuItems">

	<h1 class="createlink-heading">Create New Link</h1>
<!--Title Starts--------------------------------------------------------------------------------------------------->
    <div class="createlink-divfullwidth">
    	<h1 class="createlink-text" style="width: 88px;">Title</h1> 
        <asp:TextBox  class="createlink-inputbox" ID="txtTitle" style=" width: 300px;" runat="server"></asp:TextBox>

    </div>
<!--Title Ends----------------------------------------------------------------------------------------------------->

<!--Tooltip Starts------------------------------------------------------------------------------------------------->
    <div class="createlink-divfullwidth">
    	<h1 class="createlink-text" style="width: 88px;">Tooltip</h1>
         <asp:TextBox class="createlink-inputbox" ID="txtToolTip" style=" width: 300px;" runat="server"></asp:TextBox>

    </div>

    <div class="createlink-divfullwidth">
    	<h1 class="createlink-text" style="width: 88px;">Position</h1>        
        <asp:TextBox class="createlink-inputbox" ID="txtOrder" style=" width: 100px;" runat="server"></asp:TextBox>
    </div>
<!--Tooltip Ends--------------------------------------------------------------------------------------------------->
  
<!--Link Type Starts----------------------------------------------------------------------------------------------->
    <div style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 0px;width: 99.7%;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom:20px;">
                <h1 class="createlink-text" style="margin-left:10px;">Link Type : </h1>
                <asp:radiobutton runat="server" name=""  ID="OptLink" Text="URL" 
                 style="position: relative; float:left; margin-right: 10px; top: 0px; left: 0px;" 
                 GroupName="OptLinkType" CssClass="content" ></asp:radiobutton>
                 <asp:radiobutton runat="server"  ID="OptDwld" Text="Download" 
                style="position: relative; float:left; margin-right: 10px;" 
                GroupName="OptLinkType" CssClass="content" ></asp:radiobutton>
                
            </div>
   
<div class="createlink-divfullwidth" id="DivLink" style="display:none;">
    	<h1 class="createlink-text" style="width: 88px;">Link URL</h1>    	
          <asp:TextBox class="createlink-inputbox" ID="txtURL" style=" width: 300px;" runat="server"></asp:TextBox>
    </div>
    <div id="DivDownloadBox" class="createlink-divfullwidth" style="margin-bottom: 10px; display:none">
      	 <h1 class="createlink-text" style="width: 88px;">Select File</h1>
        <asp:fileupload  runat="server" ID="FileUploadControl"></asp:fileupload>
      </div>

</div>      
  
    <asp:Button  ID="CmdSaveMenuItem" runat="server" class="g-button g-button-submit"  style="float: right; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size: 14px; margin-right: 10px; cursor: pointer;  display:none"  Text="SAVE"/>
        
</div>
<asp:Label ID="lblMenuType" runat="server"  ></asp:Label>

</form>
