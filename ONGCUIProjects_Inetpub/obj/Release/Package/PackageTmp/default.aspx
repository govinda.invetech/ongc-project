﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="ONGCUIProjects._default" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoginFooter" Src="UserControls/LoginFooter.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Login</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="style/ONGCStyle.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		    function cmdButton1_Clicked() {
		        document.all('txtName').focus();
		        return false;
		    }

		</script>
	</HEAD>
	<body leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<!-- Begin Table1 for Outer Frame -->
			<TABLE id="Table1" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="top" style="HEIGHT: 79px">
						<P align="center"><IMG height="200" src="Images/Ongc2.jpg"><IMG height="200" src="Images/Ongc4.jpg">&nbsp;<IMG height="200" src="Images/Ongc1.jpg"></P>
					</TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="center">
						<P>
							<!-- Begin Table for Middle Frame --></P>
						<P>&nbsp;
							<TABLE id="UserLogin" height="287" cellSpacing="0" cellPadding="0" width="350" border="1"
								runat="server" style="WIDTH: 350px; HEIGHT: 287px">
								<TR id="TR1" height="40" runat="server">
									<TD align="center">
										<P align="left"><span class="LoginText"> ONGC Online Reporting System - Login</span>&nbsp;</P>
									</TD>
								</TR>
								<TR bgColor="#eeeeee">
									<TD>
										<!-- Begin Table3 for Server controls -->
										<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR id="rowlogin" runat="server">
												<TD><span class="subcontent"><FONT color="#660000">CPF Number</FONT></span></TD>
												<TD><asp:textbox id="txtName" runat="server" MaxLength="10" CssClass="form_input" Width="157px" tabIndex="1"></asp:textbox><asp:requiredfieldvalidator id="rfvtxtName" runat="server" ControlToValidate="txtName" Display="None" ErrorMessage="Enter Name"
														tabIndex="22"></asp:requiredfieldvalidator></TD>
											</TR>
											<TR height="9">
												<TD></TD>
												<TD></TD>
											</TR>
											<TR id="rowmessage" height="20" runat="server">
												<TD style="HEIGHT: 14px" vAlign="top" align="center" colSpan="2"><span class="subcontent"><SPAN style="FONT-SIZE: 12pt; FONT-FAMILY: 'Times New Roman'; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA"><font color="#ff0033">Login 
																Name and Password are invalid</font></SPAN></span></TD>
											</TR>
											<TR id="rowpwd" runat="server">
												<TD style="HEIGHT: 20px"><span class="subcontent"><FONT color="#660000">Password</FONT></span></TD>
												<TD style="HEIGHT: 20px"><asp:textbox id="txtPwd" runat="server" MaxLength="10" CssClass="form_input" TextMode="Password"
														Width="157px" tabIndex="2"></asp:textbox><asp:requiredfieldvalidator id="rfvtxtPwd" runat="server" ControlToValidate="txtPwd" Display="None" ErrorMessage="Enter Password"
														tabIndex="23"></asp:requiredfieldvalidator>&nbsp;</TD>
											</TR>
											<tr height="9">
												<TD></TD>
												<TD></TD>
											</tr>
											<TR id="rowrole" runat="server">
												<TD style="HEIGHT: 14px"><span class="subcontent"><FONT color="#660000">Role</FONT></span></TD>
												<TD style="HEIGHT: 14px"><asp:dropdownlist id="ddlRole" runat="server" CssClass="form_input" Width="223px"></asp:dropdownlist></TD>
											</TR>
											<tr height="9">
												<TD></TD>
												<TD></TD>
											</tr>
											<TR id="rowbtnlogin" runat="server">
												<TD style="HEIGHT: 18px"></TD>
												<TD style="HEIGHT: 18px" vAlign="bottom" align="left"><asp:button id="btnLogin" runat="server" CssClass="form_submit " Text="LOG IN" tabIndex="3" onclick="btnLogin_Click"></asp:button><asp:button id="btnLoginRole" runat="server" CssClass="form_submit" Text="LOG IN" tabIndex="4" ></asp:button>
													<asp:button id="btnChangePass" runat="server" CssClass="form_submit " Width="104px" Text="Change Password"
														tabIndex="5" ></asp:button>
                                                    <asp:Button ID="cmdRegisterMe" runat="server" onclick="cmdRegisterMe_Click" 
                                                        Text="Register Me" />
                                                </TD>
											</TR>
											<TR id="rowrelogin" height="100" runat="server">
												<TD style="HEIGHT: 14px" vAlign="bottom" align="center" colSpan="2"><span class="subcontent"><asp:button id="btnRelogin" runat="server" CssClass="form_submit" Text="LOG IN" CausesValidation="False"
															tabIndex="6" ></asp:button></span></TD>
											</TR>
											<!-- End Table3 for Server controls --></TABLE>
										<asp:label id="ErrMessage" runat="server" CssClass="subcontent" ForeColor="Red" tabIndex="24"></asp:label><asp:validationsummary id="vSummery" runat="server" CssClass="subcontent" Width="184px" DisplayMode="List"
											Height="41px" tabIndex="25"></asp:validationsummary></TD>
								</TR>
								<!-- End Table2 for Middle Frame --></TABLE>
						</P>
						<P align="left">
						</P>
						<TABLE id="tblChangepass" height="200" cellSpacing="0" cellPadding="0" width="350" border="1"
							runat="server">
							<TR id="TR2" height="40" runat="server">
								<TD align="center"><SPAN class="LoginText">Change Password</SPAN></TD>
							</TR>
							<TR bgColor="#eeeeee">
								<TD><!-- Begin Table3 for Server controls -->
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR id="Tr3" runat="server">
											<TD style="HEIGHT: 18px">
												<P align="left"><SPAN class="subcontent"><FONT color="#660000"> Old Password</FONT></SPAN></P>
											</TD>
											<TD style="HEIGHT: 18px">
												<asp:textbox id="txtoldpass" runat="server" CssClass="form_input" MaxLength="10" Width="157px"
													TextMode="Password" tabIndex="7"></asp:textbox></TD>
										</TR>
										<TR height="9">
											<TD></TD>
											<TD></TD>
										</TR>
										<TR id="Tr4" runat="server">
											<TD style="HEIGHT: 20px"><SPAN class="subcontent"><FONT color="#660000"> New Password</FONT></SPAN></TD>
											<TD style="HEIGHT: 20px">
												<asp:textbox id="txtNewPass" runat="server" CssClass="form_input" MaxLength="10" TextMode="Password"
													Width="157px" tabIndex="8"></asp:textbox>&nbsp;</TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 16px"></TD>
											<TD style="HEIGHT: 16px"></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 20px"><SPAN class="subcontent"><FONT color="#660000">Confirm Password</FONT></SPAN></TD>
											<TD style="HEIGHT: 20px">
												<asp:textbox id="txtConfPass" runat="server" Width="157px" CssClass="form_input" MaxLength="10"
													TextMode="Password" tabIndex="9"></asp:textbox></TD>
										</TR>
										<TR height="9">
											<TD></TD>
											<TD></TD>
										</TR>
										<TR id="Tr5" runat="server">
											<TD style="HEIGHT: 17px"></TD>
											<TD style="HEIGHT: 17px" vAlign="bottom" align="left">
												<P align="left">
													<asp:button id="btnChangePwd" runat="server" CssClass="form_submit " Width="104px" Text="Change Password"
														tabIndex="10" ></asp:button></P>
											</TD>
										</TR> <!-- End Table3 for Server controls --></TABLE>
									<asp:ValidationSummary id="ValidationSummary1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="XX-Small"
										ShowMessageBox="True" ShowSummary="False" tabIndex="44"></asp:ValidationSummary>
									<asp:RequiredFieldValidator id="Rfv_NewPass" runat="server" ErrorMessage="Please Insert New Password to change"
										Display="None" ControlToValidate="txtNewPass" Font-Bold="True" Font-Names="Verdana" Font-Size="XX-Small"
										tabIndex="17"></asp:RequiredFieldValidator>
									<asp:RequiredFieldValidator id="rfv_oldpass" runat="server" ErrorMessage="Please Insert Old Password To change"
										Display="None" ControlToValidate="txtoldpass" Font-Bold="True" Font-Names="Verdana" Font-Size="XX-Small"
										tabIndex="18"></asp:RequiredFieldValidator>
									<asp:RequiredFieldValidator id="Conf_pass" runat="server" ErrorMessage="Please Confirm New Password To change"
										Display="None" ControlToValidate="txtConfPass" Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True"
										tabIndex="14"></asp:RequiredFieldValidator>
								</TD>
							</TR> <!-- End Table2 for Middle Frame --></TABLE>
					</TD>
				</TR>
				<TR>
					<TD vAlign="bottom">
						<uc1:LoginFooter id="LoginFooter1" runat="server"></uc1:LoginFooter></TD>
				</TR>
				<!-- End Table1 for Outer Frame --></TABLE>
		</form>
	</body>
</HTML>
