﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="1b_ecIncident.aspx.cs" Inherits="ONGCUIProjects.ec._1b_ecIncident" %>

<script type="text/javascript">

    $("#txtIncdntDate").datetimepicker({
        dateFormat: 'dd-mm-yy',
        altField: "#txtIncdntTime"
    });



    /*************Start of Area Search Autocomplte***********/
    $("input#txtIncdntArea:not(.ui-autocomplete-input)").live("focus", function (event) {

        var IncidentAreaSearch_options, IncidentAreaSearch_a;
        var options = {
            serviceUrl: '../services/auto.aspx',
            onSelect: IncidentAreaSearch_onAutocompleteSelect,
            deferRequestBy: 0, //miliseconds
            params: { type: 'ec_area_name', limit: '10' },
            noCache: true //set to true, to disable caching
        };
        IncidentAreaSearch_a = $("#txtIncdntArea").autocomplete(options);
    });

    var IncidentAreaSearch_onAutocompleteSelect = function (IncidentAreaSearch_value, IncidentAreaSearch_data) {
        if (IncidentAreaSearch_value == "Enter something else..") {
            $("#txtIncdntArea").val("");
            $("#txtIncdntDept").val("");
            $("#txtIncdntArea").focus();
        } else {
            $("#txtIncdntLocation").val("");
            $("#txtIncdntLocation").focus();
        }
    }
    /*************End of Area Search Autocomplte***********/








    $("input#txtIncdntLocation:not(.ui-autocomplete-input)").live("focus", function (event) {
        var sLocationArea = $("#txtIncdntArea").val().trim();
        var IncidentLocationSearch_options, IncidentLocationSearch_a;
        var options = {
            serviceUrl: '../services/auto.aspx',
            onSelect: IncidentLocationSearch_onAutocompleteSelect,
            deferRequestBy: 0, //miliseconds
            params: { type: 'ec_area_location_name', limit: '10', extra_data: sLocationArea },
            noCache: true //set to true, to disable caching
        };
        IncidentLocationSearch_a = $("#txtIncdntLocation").autocomplete(options);
        var sLocationArea = "";

    });

    var IncidentLocationSearch_onAutocompleteSelect = function (IncidentLocationSearch_value, IncidentLocationSearch_data) {
        if (IncidentLocationSearch_value == "Enter something else..") {
            $("#txtIncdntLocation").val("");
            $("#txtIncdntLocation").focus();
        } else {
            if (IncidentLocationSearch_data == "undefined") {
                IncidentLocationSearch_data = "";
            }
            $("#txtIncdntDept").val(IncidentLocationSearch_data);
            $("#txtIncdntDept").focus();
        }
        $(".autocomplete").hide();
    }



    var EmployeeSearch_options, EmployeeSearch_a;
    jQuery(function () {
        var options = {
            serviceUrl: '../services/auto.aspx',
            onSelect: EmployeeSearch_onAutocompleteSelect,
            deferRequestBy: 0, //miliseconds
            params: { type: 'emp_for_guesthouse', limit: '10' },
            noCache: true //set to true, to disable caching
        };
        EmployeeSearch_a = $("#txtInjuryPersonName").autocomplete(options);
    });

    var EmployeeSearch_onAutocompleteSelect = function (EmployeeSearch_value, EmployeeSearch_data) {
        alert(EmployeeSearch_data);
        var sEmpDetailArr = EmployeeSearch_data.split('~');
        $('#txtInjuryCPFNo').val(sEmpDetailArr[0].trim());
        $('#txtInjuryDesignation').val(sEmpDetailArr[1].trim());
    }

    $("input[name=rushedtohospital]").change(function () {
        if ($(this).val() == "N") {
            alert("You have choosen wrong form please select 'Report New Near Miss'");
            $('input[name="rushedtohospital"][value="Y"]').prop('checked', true);
        }
    });

    $("#cmdReset").click(function () {
        window.location = self.location;
    });

    $(".CheckValidation").focusin(function () {
        if ($(this).css("border-color", "red")) {
            $(this).css("border-color", "");
        }
    });

    $(".RemoveInjuryPerson").live("click", function () {
        $(this).parent("div").remove();
    });

    $("#cmdSubmitRpt").click(function () {
        var chkRequiredValidation = "TRUE";
        var sInjuryPersonDetails = "";
        $(".getInjuryDetail").each(function () {
            var sPersonName = $(this).children("p#InjuryPersonName").attr("value").trim();
            var sPersonDesig = $(this).children("p#InjuryDesignation").attr("value").trim();
            var sPersonCPFNo = $(this).children("p#InjuryCPFNo").attr("value").trim();
            var sPersonInjuryNature = $(this).children("p#InjuryNature").attr("value").trim();
            sInjuryPersonDetails = sInjuryPersonDetails + sPersonName + "~" + sPersonDesig + "~" + sPersonCPFNo + "~" + sPersonInjuryNature + "`";
        });

        var sRptEngCPF;
        var sRptEngName;
        var sRptEngDesig;
        var sIncdntDate;
        var sIncdntTime;
        var sIncdntLocation;
        var sIncdntArea;
        var sIncdntDept;
        var sProperty;
        var sProcess;
        var sEnvironment;
        var sPersonalInjury;
        var sIncdntDesc;
        var sIncdntAppCause;
        var sIncdntImmdRemedial;
        var sRushToEmg;

        if ($("#txtRptEngCPF").val().trim() == "") {
            $("#txtRptEngCPF").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sRptEngCPF = $("#txtRptEngCPF").val().trim();
        }

        if ($("#txtRptEngName").val().trim() == "") {
            $("#txtRptEngName").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sRptEngName = $("#txtRptEngName").val().trim();
        }

        if ($("#txtIncdntDate").val().trim() == "") {
            $("#txtIncdntDate").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sIncdntDate = $("#txtIncdntDate").val().trim();
        }

        if ($("#txtIncdntTime").val().trim() == "") {
            $("#txtIncdntTime").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sIncdntTime = $("#txtIncdntTime").val().trim();
        }

        if ($("#txtIncdntLocation").val().trim() == "") {
            $("#txtIncdntLocation").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sIncdntLocation = $("#txtIncdntLocation").val().trim();
        }

        if ($("#txtIncdntArea").val().trim() == "") {
            $("#txtIncdntArea").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sIncdntArea = $("#txtIncdntArea").val().trim();
        }

        if ($("#txtIncdntDesc").val().trim() == "") {
            $("#txtIncdntDesc").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sIncdntDesc = $("#txtIncdntDesc").val().trim();
        }

        if ($("#txtIncdntAppCause").val().trim() == "") {
            $("#txtIncdntAppCause").css("border-color", "red");
            chkRequiredValidation = "FALSE";
        } else {
            sIncdntAppCause = $("#txtIncdntAppCause").val().trim();
        }

        sRptEngDesig = $("#txtRptEngDesig").val().trim();
        sIncdntDept = $("#txtIncdntDept").val().trim();
        sProperty = $("#txtProperty").val().trim();
        sProcess = $("#txtProcess").val().trim();
        sEnvironment = $("#txtEnvironment").val().trim();
        sPersonalInjury = $("#txtPersonalInjury").val().trim();
        sIncdntImmdRemedial = $("#txtIncdntImmdRemedial").val().trim();
        sRushToEmg = $("input[name=rushedtohospital]:checked").val().trim();

        if (chkRequiredValidation == "FALSE") {
            alert("Please fill all mandatory fields");
        } else if (sInjuryPersonDetails == "") {
            alert("Please add injury person");
        } else {
            $("#response").text("Saving.....");
            $.ajax({
                type: "POST",
                url: "service/1_ecReportSave.aspx",
                data: "type=INCIDENT&RptEngCPF=" + sRptEngCPF + "&RptEngName=" + sRptEngName + "&RptEngDesig=" + sRptEngDesig + "&IncdntDate=" + sIncdntDate + "&IncdntTime=" + sIncdntTime + "&IncdntLocation=" + sIncdntLocation + "&IncdntArea=" + sIncdntArea + "&IncdntDept=" + sIncdntDept + "&Property=" + sProperty + "&Process=" + sProcess + "&Environment=" + sEnvironment + "&PersonalInjury=" + sPersonalInjury + "&IncdntDesc=" + sIncdntDesc + "&IncdntAppCause=" + sIncdntAppCause + "&IncdntImmdRemedial=" + sIncdntImmdRemedial + "&InjuredPerson=" + sInjuryPersonDetails + "&RushToEmg=" + sRushToEmg + "&InjuryPersonDetails=" + sInjuryPersonDetails,
                success: function (msg) {
                    var msg_arr = msg.split("~");
                    if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                        $("#response").text(msg_arr[2]);
                        $("#cmdSubmitRpt").hide();
                    } else {
                        alert(msg_arr[2]);
                    }
                }
            });
        }
    });
    function cmdAddInjuryPerson_onclick() {
        var CheckRequired = "TRUE";
        var sPersonName;
        var sPersonCPFNo;

        if ($("#txtInjuryPersonName").val().trim() == "") {
            $("#txtInjuryPersonName").css("border-color", "red");
            CheckRequired = "FALSE";
        } else {
            sPersonName = $("#txtInjuryPersonName").val().trim();
        }
        if ($("#txtInjuryCPFNo").val().trim() == "") {
            $("#txtInjuryCPFNo").css("border-color", "red");
            CheckRequired = "FALSE";
        } else {
            sPersonCPFNo = $("#txtInjuryCPFNo").val().trim();
        }
        if (CheckRequired == "FALSE") {
            alert("Please Fill mandatory fields");
        } else {
            var sPersonDesig = $("#txtInjuryDesignation").val().trim();
            var sPersonInjuryNature = $("#txtInjuryNature").val().trim();
            var sInjuredPersonData = "";
            sInjuredPersonData = sInjuredPersonData + "<div class=\"div-fullwidth getInjuryDetail\">";
            sInjuredPersonData = sInjuredPersonData + "<p value=\"" + sPersonName + "\" id=\"InjuryPersonName\" class=\"content\">[ Name : " + sPersonName + " ],</p>";
            sInjuredPersonData = sInjuredPersonData + "<p value=\"" + sPersonDesig + "\" id=\"InjuryDesignation\" class=\"content\">[ Desig : " + sPersonDesig + " ],</p>";
            sInjuredPersonData = sInjuredPersonData + "<p value=\"" + sPersonCPFNo + "\" id=\"InjuryCPFNo\" class=\"content\">[ CPF No : " + sPersonCPFNo + " ],</p>";
            sInjuredPersonData = sInjuredPersonData + "<p value=\"" + sPersonInjuryNature + "\" id=\"InjuryNature\"  class=\"content\">[ Nature of Injury : " + sPersonInjuryNature + " ]</p>";
            sInjuredPersonData = sInjuredPersonData + "<a class=\"RemoveInjuryPerson\" href=\"javascript:void(0);\">DEL</a>";
            sInjuredPersonData = sInjuredPersonData + "</div>";
            $("#divInjuredPersonDetail").append(sInjuredPersonData);
            $("#txtInjuryPersonName").val("");
            $("#txtInjuryDesignation").val("");
            $("#txtInjuryCPFNo").val("");
            $("#txtInjuryNature").val("");
        }
    }
</script>
<form id="form1" runat="server">
<div id="ReportNewIncident" style="width: 708px;">
    <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">
        Report New Incident<span style="color: Red; float: right; font-size: 15px;">* Mandatory
            Fields</span></h1>
    <div class="div-fullwidth marginbottom">
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Reporter Name</p>
            <asp:textbox class="CheckValidation" id="txtRptEngName" runat="server" Enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto; margin: 0 8px;">
            <p class="content">
                CPF No</p>
            <asp:textbox class="CheckValidation" id="txtRptEngCPF" runat="server" Enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Desig</p>
            <asp:textbox id="txtRptEngDesig" runat="server" Enabled="False"></asp:textbox>
        </div>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Rushed to Hospital</p>
        <input style="position: relative; float: left; margin: 6px 5px 0 0;" id="rushedtohospital_yes"
            type="radio" name="rushedtohospital" value="Y" checked="checked" />
        <label class="content" for="rushedtohospital_yes">
            Yes</label>
        <input style="position: relative; float: left; margin: 6px 5px 0 0;" id="rushedtohospital_no"
            type="radio" name="rushedtohospital" value="N" />
        <label class="content" for="rushedtohospital_no">
            No</label>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Incident Date and Time(24-hour)<span style="color: Red;">*</span></p>
        <asp:textbox class="CheckValidation" id="txtIncdntDate" runat="server"></asp:textbox>
        <asp:textbox class="CheckValidation" id="txtIncdntTime" runat="server"></asp:textbox>
    </div>
    <div class="div-fullwidth marginbottom">
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Area<span style="color: Red;">*</span></p>
            <asp:textbox class="CheckValidation" id="txtIncdntArea" runat="server"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto; margin: 0 10px;">
            <p class="content">
                Location<span style="color: Red;">*</span></p>
            <asp:textbox class="CheckValidation" id="txtIncdntLocation" runat="server"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Department</p>
            <asp:textbox class="CheckValidation" id="txtIncdntDept" runat="server" Enabled="False"></asp:textbox>
        </div>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content" style="width: 100%;">
            Type of Damage</p>
        <div class="div-halfwidth">
            <p class="content">
                Property</p>
            <asp:textbox id="txtProperty" runat="server" textmode="MultiLine" style="max-height: 40px;
                max-width: 98%; min-height: 40px; min-width: 98%"></asp:textbox>
        </div>
        <div class="div-halfwidth">
            <p class="content">
                Process</p>
            <asp:textbox id="txtProcess" runat="server" textmode="MultiLine" style="max-height: 40px;
                max-width: 100%; min-height: 40px; min-width: 100%"></asp:textbox>
        </div>
        <div class="div-halfwidth">
            <p class="content">
                Environment</p>
            <asp:textbox id="txtEnvironment" runat="server" textmode="MultiLine" style="max-height: 40px;
                max-width: 98%; min-height: 40px; min-width: 98%"></asp:textbox>
        </div>
        <div class="div-halfwidth">
            <p class="content">
                Personal Injury</p>
            <asp:textbox id="txtPersonalInjury" runat="server" textmode="MultiLine" style="max-height: 40px;
                max-width: 100%; min-height: 40px; min-width: 100%"></asp:textbox>
        </div>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Brief Description of the Accident<span style="color: Red;">*</span></p>
        <asp:textbox id="txtIncdntDesc" class="CheckValidation" runat="server" textmode="MultiLine"
            style="min-width: 448px; min-height: 40px; max-width: 448px; max-height: 40px;
            float: right;"></asp:textbox>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Apparent Cause of the Accident<span style="color: Red;">*</span></p>
        <asp:textbox id="txtIncdntAppCause" class="CheckValidation" runat="server" textmode="MultiLine"
            style="min-width: 448px; min-height: 40px; max-width: 448px; max-height: 40px;
            float: right;"></asp:textbox>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Immediate Remedial Action</p>
        <asp:textbox id="txtIncdntImmdRemedial" runat="server" textmode="MultiLine" style="min-width: 448px;
            min-height: 40px; max-width: 448px; max-height: 40px; float: right;"></asp:textbox>
    </div>
    <div class="div-fullwidth marginbottom">
        <p class="content">
            Detail of Injured Person(s)<span style="color: Red;">*</span></p>
        <table class="tftable" border="1">
            <tbody>
                <tr>
                    <th>
                        Name of Person
                    </th>
                    <th>
                        Designation
                    </th>
                    <th>
                        CPF No
                    </th>
                    <th>
                        Nature of Injury
                    </th>
                    <th>
                    </th>
                </tr>
                <tr>
                    <td style="padding: 2px;">
                        <asp:textbox class="CheckValidation" id="txtInjuryPersonName" runat="server"></asp:textbox>
                    </td>
                    <td style="padding: 2px;">
                        <asp:textbox id="txtInjuryDesignation" runat="server"></asp:textbox>
                    </td>
                    <td style="padding: 2px;">
                        <asp:textbox class="CheckValidation" id="txtInjuryCPFNo" runat="server"></asp:textbox>
                    </td>
                    <td style="padding: 2px;">
                        <asp:textbox id="txtInjuryNature" runat="server" textmode="MultiLine" style="max-width: 165px;
                            max-height: 29px; min-width: 165px; min-height: 29px;"></asp:textbox>
                    </td>
                    <td style="padding: 2px;">
                        <input id="cmdAddInjuryPerson" type="button" value="" class="button_add" onclick="return cmdAddInjuryPerson_onclick()" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divInjuredPersonDetail" class="div-fullwidth marginbottom">
    </div>
    <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">
    </h1>
    <div class="div-fullwidth">
        <p id="response" class="content" style="color: Red;">
        </p>
        <input id="cmdReset" type="button" value="CLOSE" class="g-button g-button-red" style="float: right;" />
        <input id="cmdSubmitRpt" type="button" value="SAVE" class="g-button g-button-submit"
            style="float: right; margin-right: 10px;" />
    </div>
</div>
</form>
