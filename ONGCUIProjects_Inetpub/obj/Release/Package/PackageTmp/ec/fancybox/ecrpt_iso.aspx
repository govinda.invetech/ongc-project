﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ecrpt_iso.aspx.cs" Inherits="ONGCUIProjects.rpt_iso" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>ISO Format</title>
        <script type="text/javascript" language="javascript" src="../../js/jquery-1.7.2.min_a39dcc03.js"></script>
        <script type="text/javascript" language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" language="javascript" src="../../js/print.js"></script>
        <script type="text/javascript">
            $("#cmdPrintISO").click(function () {
                $("#PrintArea").printElement({
                    overrideElementCSS: [
                            '../../css/styleGlobal.css',
                            { href: '../../css/styleGlobal.css', media: 'print' }
                            ]
                });
            });
        </script>
    </head>
    <body style="width:auto;">
        <form id="form1" runat="server">
        <div style="position: relative; width: 750px;">
            <div id="PrintArea" style=" position: relative; width: 100%; overflow-x: hidden; margin-right:auto; margin-left:auto;">
                <div class="div-fullwidth marginbottom">
                    <img src="../Images/logo-ongc.jpg" alt="" style="position: relative; float: left;" />
                    <div style=" position: relative; float: right; width: 400px;">
                        <p class="report rptContent" style="text-align: right; margin-bottom: 10px;">Form-A</p>
                        <p class="report rptContent" style="text-align: right;">ISO No : URN/ISO/REC/SF-11</p> 
                    </div>
                </div>
                <div class="div-fullwidth">
                    <table id="tfhover" class="rptTable" border="1">
                        <tbody>
                            <tr>
                                <th style="text-align: center; font-size: 20px;" colspan="3">INCIDENT REPORT</th>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">01</td>
                                <td style="padding: 0; width: 250px;">
                                    <div class="div-fullwidth">
                                        <p style="background-color: #ccc; padding: 2px 4px; border-bottom: 1px solid black;">Location</p>
                                        <p style="background-color: #ccc; padding: 2px 4px; border-bottom: 1px solid black;">Area</p>
                                        <p style="background-color: #ccc; padding: 2px 4px;">Department</p>
                                    </div>
                                </td>
                                <td style="padding: 0;">
                                    <div class="div-fullwidth">
                                        <p runat="server" id="IncLocation" style="padding: 2px 4px; border-bottom: 1px solid black; height: 20px;"></p>
                                        <p runat="server" id="IncArea" style="padding: 2px 4px; border-bottom: 1px solid black; height: 20px;"></p>
                                        <p runat="server" id="IncDept" style="padding: 2px 4px; height: 20px;"></p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">02</td>
                                <td style="padding: 0; width: 250px;">
                                    <div class="div-fullwidth">
                                        <p class="cell-content">Date & Time</p>
                                    </div>
                                </td>
                                <td runat="server" id="IncDateNTime"></td>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">03</td>
                                <td style="padding: 0; width: 250px;">
                                    <div class="div-fullwidth">
                                        <p class="cell-content">Reported by<br />(Name & Designation)</p>
                                    </div>
                                </td>
                                <td runat="server" id="IncReportedBy"></td>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">04</td>
                                <td style="padding: 0; border-left-width: 0;" colspan=3>
                                    <table id="Table1" class="rptTable" border="1" style="border-top-width:0; border-right-width: 0; border-left-width: 0; border-bottom-width: 0; border-width: 0;">
                                        <tbody>
                                            <tr>
                                                <th style="font-size: 16px; font-weight: normal; padding-left: 7px; border-width: 0; border-bottom-width: 1px;" colspan=3>Type of Damages</th>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">i)</p>
                                                    </div>
                                                </td>
                                                <td style="width: 208px;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Property</p>
                                                    </div>
                                                </td>
                                                <td  runat="server" id="IncProperty"></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">ii)</p>
                                                    </div>
                                                </td>
                                                <td style="width: 208px;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Process</p>
                                                    </div>
                                                </td>
                                                <td runat="server" id="IncProcess"></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">iii)</p>
                                                    </div>
                                                </td>
                                                <td style="width: 208px;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Environment</p>
                                                    </div>
                                                </td>
                                                <td runat="server" id="IncEnvironment"></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0; border-bottom-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">iv)</p>
                                                    </div>
                                                </td>
                                                <td style="width: 205px; border-bottom-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Personal Injury</p>
                                                    </div>
                                                </td>
                                                <td runat="server" id="IncPersonalInjury" style="border-bottom-width: 0;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">05</td>
                                <td runat="server" id="IncInjuryDetail" style="padding: 0; border-width: 0;" colspan="3">
                                    <%--<table id="Table2" class="rptTable" border="1" style="border-top-width:0; border-right-width: 0; border-left-width: 0; border-bottom-width: 0; border-width: 0;">
                                        <tbody>
                                            <tr>
                                                <th style="font-size: 16px; font-weight: normal; padding-left: 7px; border-width: 0; border-bottom-width: 1px;" colspan=5>Details of Injured Person (s) (In case of Contractual employee, Name of contractor shall be mentioned in place of Designation & CPF NO.):</th>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Sl</p>
                                                    </div>
                                                </td>
                                                <td style="width: 300px;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Name</p>
                                                    </div>
                                                </td>
                                                <td style="width: 208px;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Designation</p>
                                                    </div>
                                                </td>
                                                <td style="width: 208px;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">CPF No.</p>
                                                    </div>
                                                </td>
                                                <td style="width: 208px;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">Nature of Injury</p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">i)</p>
                                                    </div>
                                                </td>
                                                <td style="width: 300px;"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">ii)</p>
                                                    </div>
                                                </td>
                                                <td style="width: 300px;"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px; border-left-width: 0; border-bottom-width: 0;">
                                                    <div class="div-fullwidth">
                                                        <p class="cell-content">iii)</p>
                                                    </div>
                                                </td>
                                                <td style="width: 300px; border-bottom-width: 0;"></td>
                                                <td style="border-bottom-width: 0;"></td>
                                                <td style="border-bottom-width: 0;"></td>
                                                <td style="border-bottom-width: 0;"></td
                                            </tr>
                                        </tbody>
                                    </table>--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">06</td>
                                <td style="padding: 0; border-width: 0;" colspan=3>
                                    <div class="div-fullwidth">
                                        <p style="background-color: #ccc; padding: 2px 4px; border-bottom: 1px solid black; border-top: 1px solid black;">Brief description of the Accident</p>
                                    </div>
                                    <p runat="server" id="IncDesc" style=" position: relative; float: left; width: 99%; height: 50px; padding: 5px;"></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">07</td>
                                <td style="padding: 0; border-width: 0;" colspan=3>
                                    <div class="div-fullwidth">
                                        <p style="background-color: #ccc; padding: 2px 4px; border-bottom: 1px solid black; border-top: 1px solid black;">Apparent cause of the Accident</p>
                                    </div>
                                    <p runat="server" id="IncApparentCause" style=" position: relative; float: left; width: 99%; height: 50px; padding: 5px;"></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40px; text-align: center;">08</td>
                                <td style="padding: 0; border-width: 0;" colspan="3">
                                    <div class="div-fullwidth">
                                        <p style="background-color: #ccc; padding: 2px 4px; border-bottom: 1px solid black; border-top: 1px solid black;">Immediate remedial action</p>
                                    </div>
                                    <p runat="server" id="IncRemedialAction" style=" position: relative; float: left; width: 99%; height: 50px; padding: 5px;"></p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan=3>
                                    <div class="div-fullwidth">
                                        <div class="div-halfwidth" style="width: 49%; height: 85px;">
                                            <p style=" position: relative; float: left; width: 98%; padding: 10px;"></p>
                                        </div>
                                        <div class="div-halfwidth" style="border-left: 1px solid black; float: right;">
                                            <div class="div-fullwidth" style="border-bottom: 1px solid black;">
                                                <p style=" position: relative; float: left; width: 100px; padding: 40px 0 0 10px; border-right: 1px solid black;">Signature</p>
                                                <p style=" position: relative; float: left; width: 330px; padding: 2px 0 2px 10px;"></p>
                                            </div>
                                            <div class="div-fullwidth">
                                                <p style=" position: relative; float: left; width: 100px; padding: 2px 0 2px 10px; border-right: 1px solid black;">Name</p>
                                                <p style=" position: relative; float: left; width: 330px; padding: 2px 0 2px 10px;"></p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan=3>
                                    <div class="div-fullwidth">
                                        <div class="div-halfwidth" style="width: 49%;">
                                            <p runat="server" id="ReportingDateTime" style=" position: relative; float: left; width: 98%; padding: 2px 0 2px 10px;">Date & Time of Reporting</p>
                                        </div>
                                        <div class="div-halfwidth" style="border-left: 1px solid black; float: right;">
                                            <div class="div-fullwidth">
                                                <p style=" position: relative; float: left; width: 100px; padding: 2px 0 2px 10px; border-right: 1px solid black;">Designation</p>
                                                <p style=" position: relative; float: left; width: 330px; padding: 2px 0 2px 10px;"></p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style=" position: relative; float: left; width: 200px;">
                        <p style="position: relative; float: left; width: 100%;">Copy to</p>
                        <ul style="position: relative; float: left; width: 100%; padding-left: 30px; border-right: 1px solid black;">
                            <li style=" list-style: none">1. GM-Head HSE, Uran</li>
                            <li style=" list-style: none">2. GM-Head Operations</li>
                            <li style=" list-style: none">3. I/c OHC</li>
                        </ul>
                    </div>
                </div>
                
            </div>
            <div class="div-fullwidth" style="text-align:center;">
                    <input id="cmdPrintISO"   type="button" value="Print" class="rptPrintButton" />
                </div>
                </div>
        </form>
    </body>
    
</html>