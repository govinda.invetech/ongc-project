﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ecDetails.aspx.cs" Inherits="ONGCUIProjects.ec.fancybox.ecDetails" %>
<script type="text/javascript">
    $("#lblRushToHospital").hide();
    $("#lblIncidentID").hide();
    $("#lblIncdntType").hide();
    if ($("#lblIncdntType"))
        var optrushtohospital = $("#lblRushToHospital").text();
    $('input[name="rushedtohospital"][value="' + optrushtohospital + '"]').prop('checked', true);

    if ($("#lblIncdntType").text() == "NEARMISS") {
        $(".ForIncident").remove();
    }

 </script>
<form id="form1" runat="server">
<asp:label id="lblIncdntType" runat="server" text="Label"></asp:label>
    <asp:label id="lblRushToHospital" runat="server" text="Label"></asp:label>
    <asp:label id="lblIncidentID" runat="server" text="Label"></asp:label>
    <div id="add_edit_que" style="position:relative;float:left; width:800px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"><asp:label id="lblIncdntTypeHead" runat="server"></asp:label></h1>
        <fieldset style="padding: 10px">
            <legend style="font-family: Arial, Helvetica, sans-serif; font-size: 11pt">Update Review Details</legend>
            <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth"  runat="server" id="divTypeList">
                
            </div>
            <div class="div-halfwidth" runat="server" id="divCategoryList">
            </div>
            <div class="div-halfwidth" runat="server" id="divEmpList">
            </div>
            </div>
            <div class="div-fullwidth marginbottom" runat="server" id="divSafety">
            </div>
            <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content" style="width: 115px;">Recommendation</p>
                <asp:TextBox ID="txtReccomendations" runat="server" TextMode="MultiLine" 
                    style="float: right; margin-right: 10px; width: 60%;" Enabled="False"></asp:TextBox>
            </div>
            <div class="div-halfwidth">
                <p class="content" style="width: 150px;">Prima Facie Reason</p>
                <asp:TextBox ID="txtPrimaFacieReason" runat="server" TextMode="MultiLine" 
                    style="width: 58.5%;" Enabled="False"></asp:TextBox>
            </div>
            </div>
            <div class="div-halfwidth marginbottom">
                <p class="content" style="width: 115px;">Remarks</p>
                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" 
                    style="width: 66%;" Enabled="False"></asp:TextBox>
            </div>
           
        </fieldset>
        <fieldset style="padding: 10px">
            <legend style="font-family: Arial, Helvetica, sans-serif; font-size: 11pt"><asp:label id="lblIncdntTypeLegend" runat="server"></asp:label> Details</legend>   
            <div class="div-fullwidth" style="width:auto;">
                <p class="content">Reporter Name</p>
                <asp:TextBox class="CheckValidation" ID="txtRptEngCPF" runat="server" 
                    ReadOnly="True" Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth" style="width:auto; margin: 0 4px;">
                <p class="content">CPF No</p>
                <asp:TextBox class="CheckValidation" ID="txtRptEngName" runat="server" 
                    ReadOnly="True" Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth" style="width:auto;">
                <p class="content">Desig</p>
                <asp:TextBox ID="txtRptEngDesig" runat="server" ReadOnly="True" Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth marginbottom">
                <p class="content">Rushed to Hospital</p>
                <input style="position:relative;float:left; margin:6px 5px 0 0;" id="Radio1" 
                    type="radio" name="rushedtohospital" value="Y" checked disabled="disabled" />
                <label class="content" for="rushedtohospital_yes">Yes</label>
                <input style="position:relative;float:left;margin:6px 5px 0 0;" id="Radio2" 
                    type="radio" name="rushedtohospital" value="N" disabled="disabled" />
                <label class="content" for="rushedtohospital_no">No</label>
            </div>
            <div class="div-fullwidth marginbottom">
                <div class="div-halfwidth" style="width:auto;">
                    <p class="content">Incident Date</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntDate" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth" style="width:auto;">
                    <p class="content">Time(24-hour)</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntTime" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
            <div class="div-fullwidth marginbottom">
                <div class="div-fullwidth" style="width:auto;">
                    <p class="content">Area</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntArea" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-fullwidth" style="width:auto; margin: 0 14px;">
                    <p class="content">Location</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntLocation" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-fullwidth" style="width:auto;">
                    <p class="content">Department</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntDept" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
            <div class="div-fullwidth marginbottom ForIncident">
                <p class="content" style="width:100%;">Type of Damage</p>
                <div class="div-halfwidth">
                    <p class="content">Property</p>
                    <asp:TextBox ID="txtProperty" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:98%; min-height:40px; min-width:98%" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth">
                    <p class="content">Process</p>
                    <asp:TextBox ID="txtProcess" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:100%; min-height:40px; min-width:100%" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth">
                    <p class="content">Environment</p>
                    <asp:TextBox ID="txtEnvironment" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:98%; min-height:40px; min-width:98%" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth">
                    <p class="content">Personal Injury</p>
                    <asp:TextBox ID="txtPersonalInjury" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:100%; min-height:40px; min-width:100%" 
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
            <div class="div-fullwidth marginbottom">
                <p class="content">Brief Description of the Accident</p>
                <asp:TextBox ID="txtIncdntDesc" runat="server" TextMode="MultiLine" 
                    style="min-width: 520px; min-height: 40px; max-width: 520px; max-height: 40px; float: right;" 
                    Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth marginbottom  ForIncident">
                <p class="content">Apparent Cause of the Accident</p>
                <asp:TextBox ID="txtIncdntAppCause" runat="server" TextMode="MultiLine" 
                    style="min-width: 520px; min-height: 40px; max-width: 520px; max-height: 40px; float: right;" 
                    Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth marginbottom  ForIncident">
                <p class="content">Immediate Remedial Action</p>
                <asp:TextBox ID="txtIncdntImmdRemedial" runat="server" TextMode="MultiLine" 
                    style="min-width: 520px; min-height: 40px; max-width: 520px; max-height: 40px; float: right;" 
                    Enabled="False"></asp:TextBox>
            </div>
            <div id="divInjuredPersonDetail" class="div-fullwidth marginbottom ForIncident">

            </div>
        </fieldset>
        
    </div>
</form>