﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="2_ecReview.aspx.cs" Inherits="ONGCUIProjects.ec.fancybox._2_ecReview" %>
<script type="text/javascript">
    $("#lblRushToHospital").hide();
    $("#lblIncidentID").hide();
    $("#lblIncdntType").hide();
    if ($("#lblIncdntType"))
        var optrushtohospital = $("#lblRushToHospital").text();
    $('input[name="rushedtohospital"][value="' + optrushtohospital + '"]').prop('checked', true);

    if ($("#lblIncdntType").text() == "NEARMISS") {
        $(".ForIncident").remove();
    }

    $("#cmdSubmitRpt").click(function () {
        var sFormComplete = "TRUE";
        var sIncidentNmbr = $("#lblIncidentID").text();


        var sRevIncdntType = "";
        if ($("#ddlIncdntType option:selected").text() == "Select Type") {
            $("#ddlIncdntType").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevIncdntType = $("#ddlIncdntType option:selected").text();
        }
        // var sRevIncdntType = $("#ddlIncdntType option:selected").text();

        var sRevIncdntCategory = "";
        if ($("#ddlIncdntCategory option:selected").text() == "Select Category") {
            $("#ddlIncdntCategory").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevIncdntCategory = $("#ddlIncdntCategory option:selected").text();
        }
        //var sRevIncdntCategory = $("#ddlIncdntCategory option:selected").text();

        var sRevRootCause = "";
        if ($("#txtPrimaFacieReason").val().trim() == "") {
            $("#txtPrimaFacieReason").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevRootCause = $("#txtPrimaFacieReason").val().trim();
        }

        //var sRevRootCause = $("#txtPrimaFacieReason").val().trim();
        var sRevRecmnd = "";
        if ($("#txtReccomendations").val().trim() == "") {
            $("#txtReccomendations").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevRecmnd = $("#txtReccomendations").val().trim();
        }

        //var sRevRecmnd = $("#txtReccomendations").val().trim();
        var sRevSafetyPrectn = "";
        if ($("#SelectSafetyPrecaustion  option:selected").text().trim() == "Select Safety Precaution") {
            $("#SelectSafetyPrecaustion").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevSafetyPrectn = $("#SelectSafetyPrecaustion  option:selected").text().trim();
        }


        //var sRevSafetyPrectn = $("#SelectSafetyPrecaustion  option:selected").text().trim();

        var sRevActionByStr = "";
        if ($("#SelectCorrectiveActionBy  option:selected").text().trim() == "Select Employee") {
            $("#SelectCorrectiveActionBy").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevActionByStr = $("#SelectCorrectiveActionBy  option:selected").val().trim();
        }
        //var sRevActionByStr = $("#SelectCorrectiveActionBy  option:selected").text().trim();


        var sRevActionByNmArr = sRevActionByStr.split('[');
        sRevActionByNm = sRevActionByNmArr[0];

        var sRevActionByCPF = "";
        if ($("#SelectCorrectiveActionBy  option:selected").val().trim() == "Select Safety Precaution") {
            $("#SelectCorrectiveActionBy").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevActionByCPF = $("#SelectCorrectiveActionBy  option:selected").val().trim();
        }


        //var sRevActionByCPF = $("#SelectCorrectiveActionBy  option:selected").val().trim();
        var sRevRemark = "";
        if ($("#txtRemarks").val().trim() == "") {
            $("#txtRemarks").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sRevRemark = $("#txtRemarks").val().trim();
        }


        // var sRevRemark = $("#txtRemarks").val().trim();
        var sIncdntDesc = "";
        if ($("#txtIncdntDesc").val().trim() == "") {
            $("#txtIncdntDesc").css("border-color", "red");
            sFormComplete = "FALSE";
        } else {
            sIncdntDesc = $("#txtIncdntDesc").val().trim();
        }

















        //var sIncdntDesc = $("#txtIncdntDesc").text().trim();

        //alert(sFormComplete);
        //alert("IncidentNmbr=" + sIncidentNmbr + "&RevIncdntCategory=" + sRevIncdntCategory + "&RevRootCause=" + sRevRootCause + "&RevRecmnd=" + sRevRecmnd + "&RevSafetyPrectn=" + sRevSafetyPrectn + "&RevActionByNm=" + sRevActionByNm + "&RevActionByCPF=" + sRevActionByCPF + "&RevRemark=" + sRevRemark + "&IncdntDesc=" + sIncdntDesc);

        if (sFormComplete == "FALSE") {
            alert("Please fill all fields...!");
        }
        else {
            $.ajax({
                type: "POST",
                url: "service/2_ecReviewSave.aspx",
                data: "IncidentNmbr=" + sIncidentNmbr + "&RevIncdntCategory=" + sRevIncdntCategory + "&RevIncdntType=" + sRevIncdntType + "&RevRootCause=" + sRevRootCause + "&RevRecmnd=" + sRevRecmnd + "&RevSafetyPrectn=" + sRevSafetyPrectn + "&RevActionByNm=" + sRevActionByNm + "&RevActionByCPF=" + sRevActionByCPF + "&RevRemark=" + sRevRemark + "&IncdntDesc=" + sIncdntDesc,
                success: function (msg) {
                    var msg_arr = msg.split("~");
                    if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                        alert(msg_arr[2]);
                        window.location = self.location;
                    } else {
                        alert(msg_arr[2]);
                    }
                }
            });
        }


        $(".validatefild").focusin(function () {
            if ($(this).css("border-color", "red")) {
                $(this).css("border-color", "");
            }
        });

        $(".textvalidation").focusin(function () {
            if ($(this).css("border-color", "red")) {
                $(this).css("border-color", "");
            }
        });


    });

 </script>
<form id="form1" runat="server">
<asp:label id="lblIncdntType" runat="server" text="Label"></asp:label>
    <asp:label id="lblRushToHospital" runat="server" text="Label"></asp:label>
    <asp:label id="lblIncidentID" runat="server" text="Label"></asp:label>
    <div id="add_edit_que" style="position:relative;float:left; width:800px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"><asp:label id="lblIncdntTypeHead" runat="server"></asp:label></h1>
        <fieldset style="padding: 10px">
            <legend style="font-family: Arial, Helvetica, sans-serif; font-size: 11pt">Update Review Details</legend>
            <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth a"  runat="server" id="divTypeList">
                <%--<p class="content">Select Category</p>
                <select runat="server" id="ddlIncdntCategory" style="position:relative; width:250px; height:auto; font-size:16px; padding:5px;">
                    <option value="">Select Category</option>
                    <option value="Event">Event</option>
                    <option value="Near Miss">Near Miss</option>
                    <option value="Major Accident">Major Accident</option>
                    <option value="Minor Accident">Minor Accident</option>
                    <option value="Information">Information</option>
                </select>--%>
            </div>
            <div class="div-halfwidth" runat="server" id="divCategoryList">
            </div>
            <div class="div-halfwidth" runat="server" id="divEmpList">
            </div>
            </div>
            <div class="div-fullwidth marginbottom a" runat="server" id="divSafety">
            </div>
            <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content" style="width: 115px;">Recommendation</p>
                <asp:TextBox class="textvalidation" ID="txtReccomendations" runat="server" TextMode="MultiLine" style="float: right; margin-right: 10px; width: 60%;"></asp:TextBox>
            </div>
            <div class="div-halfwidth a">
                <p class="content" style="width: 150px;">Prima Facie Reason</p>
                <asp:TextBox class="textvalidation" ID="txtPrimaFacieReason" runat="server" TextMode="MultiLine" style="width: 58.5%;"></asp:TextBox>
            </div>
            </div>
            <div class="div-halfwidth marginbottom">
                <p class="content" style="width: 115px;">Remarks</p>
                <asp:TextBox class="textvalidation" ID="txtRemarks" runat="server" TextMode="MultiLine" style="width: 66%;"></asp:TextBox>
            </div>
            <div class="div-halfwidth">
            <input type="button" id="cmdClose" style="float:right; margin-top:10px;" class="g-button g-button-red" value="Close" onclick="$.fancybox.close();" />
            <input type="button" id="cmdSubmitRpt" style="float:right; margin:10px 10px 0px 0px"  class="g-button g-button-submit" value="SUBMIT REPORT" />
        </div>
        </fieldset>
        <fieldset style="padding: 10px">
            <legend style="font-family: Arial, Helvetica, sans-serif; font-size: 11pt"><asp:label id="lblIncdntTypeLegend" runat="server"></asp:label> Details</legend>   
            <div class="div-fullwidth" style="width:auto;">
                <p class="content">Reporter Name</p>
                <asp:TextBox class="CheckValidation" ID="txtRptEngCPF" runat="server" 
                    ReadOnly="True" Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth" style="width:auto; margin: 0 4px;">
                <p class="content">CPF No</p>
                <asp:TextBox class="CheckValidation" ID="txtRptEngName" runat="server" 
                    ReadOnly="True" Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth" style="width:auto;">
                <p class="content">Desig</p>
                <asp:TextBox ID="txtRptEngDesig" runat="server" ReadOnly="True" Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth marginbottom">
                <p class="content">Rushed to Hospital</p>
                <input style="position:relative;float:left; margin:6px 5px 0 0;" id="Radio1" 
                    type="radio" name="rushedtohospital" value="Y" checked disabled="disabled" />
                <label class="content" for="rushedtohospital_yes">Yes</label>
                <input style="position:relative;float:left;margin:6px 5px 0 0;" id="Radio2" 
                    type="radio" name="rushedtohospital" value="N" disabled="disabled" />
                <label class="content" for="rushedtohospital_no">No</label>
            </div>
            <div class="div-fullwidth marginbottom">
                <div class="div-halfwidth" style="width:auto;">
                    <p class="content">Incident Date</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntDate" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth" style="width:auto;">
                    <p class="content">Time(24-hour)</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntTime" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
            <div class="div-fullwidth marginbottom">
                <div class="div-fullwidth" style="width:auto;">
                    <p class="content">Area</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntArea" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-fullwidth" style="width:auto; margin: 0 14px;">
                    <p class="content">Location</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntLocation" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-fullwidth" style="width:auto;">
                    <p class="content">Department</p>
                    <asp:TextBox class="CheckValidation" ID="txtIncdntDept" runat="server" 
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
            <div class="div-fullwidth marginbottom ForIncident">
                <p class="content" style="width:100%;">Type of Damage</p>
                <div class="div-halfwidth">
                    <p class="content">Property</p>
                    <asp:TextBox ID="txtProperty" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:98%; min-height:40px; min-width:98%" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth">
                    <p class="content">Process</p>
                    <asp:TextBox ID="txtProcess" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:100%; min-height:40px; min-width:100%" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth">
                    <p class="content">Environment</p>
                    <asp:TextBox ID="txtEnvironment" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:98%; min-height:40px; min-width:98%" 
                        Enabled="False"></asp:TextBox>
                </div>
                <div class="div-halfwidth">
                    <p class="content">Personal Injury</p>
                    <asp:TextBox ID="txtPersonalInjury" runat="server" TextMode="MultiLine" 
                        style="max-height:40px; max-width:100%; min-height:40px; min-width:100%" 
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
            <div class="div-fullwidth marginbottom">
                <p class="content">Brief Description of the Accident</p>
                <asp:TextBox ID="txtIncdntDesc" runat="server" TextMode="MultiLine" 
                    style="min-width: 520px; min-height: 40px; max-width: 520px; max-height: 40px; float: right;" 
                    Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth marginbottom  ForIncident">
                <p class="content">Apparent Cause of the Accident</p>
                <asp:TextBox ID="txtIncdntAppCause" runat="server" TextMode="MultiLine" 
                    style="min-width: 520px; min-height: 40px; max-width: 520px; max-height: 40px; float: right;" 
                    Enabled="False"></asp:TextBox>
            </div>
            <div class="div-fullwidth marginbottom  ForIncident">
                <p class="content">Immediate Remedial Action</p>
                <asp:TextBox ID="txtIncdntImmdRemedial" runat="server" TextMode="MultiLine" 
                    style="min-width: 520px; min-height: 40px; max-width: 520px; max-height: 40px; float: right;" 
                    Enabled="False"></asp:TextBox>
            </div>
            <div runat="server" id="divInjuredPersonDetail" class="div-fullwidth marginbottom ForIncident">

            </div>
        </fieldset>
        
    </div>
</form>