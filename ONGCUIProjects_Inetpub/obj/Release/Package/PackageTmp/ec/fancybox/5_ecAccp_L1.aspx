﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="5_ecAccp_L1.aspx.cs" Inherits="ONGCUIProjects.ec.fancybox._5_ecAccp_L1" %>
<script type="text/javascript">
    $("#lblIncdntNo").hide();
    $("#lblIncdntStatus").hide();
    if ($("#lblIncdntStatus").text() != "11") {
        $('input[name="my_hint"]').change(function () {
            if ($(this).val() == "FWDUPLEVEL") {
                $("#div_select_accpt_level").show();
            } else {
                $("#div_select_accpt_level").hide();
            }
        });
    }
    $("#txtAutorityName").keypress(function () {
        var AccpLevelAuthNameSearch_options, AccpLevelAuthNameSearch_a;
        jQuery(function () {
            var options = {
                serviceUrl: '../services/auto.aspx',
                onSelect: AccpLevelAuthNameSearch_onAutocompleteSelect,
                deferRequestBy: 0, //miliseconds
                params: { type: 'ec_level_2', limit: '10' },
                noCache: true //set to true, to disable caching
            };
            AccpLevelAuthNameSearch_a = $("#txtAutorityName").autocomplete(options);
        });
    });
    var sAuthorityCPFNo = "";
    var AccpLevelAuthNameSearch_onAutocompleteSelect = function (AccpLevelAuthNameSearch_value, AccpLevelAuthNameSearch_data) {
        $(".autocomplete").hide();
        if (AccpLevelAuthNameSearch_data == -1) {
            $("#txtAutorityName").val("");
            sAuthorityCPFNo = "";
        } else {
            sAuthorityCPFNo = AccpLevelAuthNameSearch_data;
        }
    }

    $("#cmdSubmit").click(function () {
        var sIncdntNo = $("#lblIncdntNo").text();
        var sLevelComment = $("#txtLevelComment").val().trim();
        var sDecision = $('input[name="my_hint"]:checked').val();
        var sAuthorityName = $("#txtAutorityName").val().trim();
        if (sDecision == undefined) {
            alert("Please select decision");
            return;
        } else {
            if ($("#lblIncdntStatus").text() != "11") {
                if (sDecision == "FWDUPLEVEL") {
                    if (sAuthorityCPFNo == "") {
                        alert("Please select an Acceptance Authority");
                        return;
                    }
                }
            }
            $.ajax({
                type: "POST",
                url: "service/5_ecAccp_L1Save.aspx",
                data: "IncdntNo=" + sIncdntNo + "&LevelComment=" + sLevelComment + "&Decision=" + sDecision + "&AuthorityCPFNo=" + sAuthorityCPFNo + "&AuthorityName=" + sAuthorityName,
                success: function (msg) {
                    var msg_arr = msg.split("~");
                    if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                        $("#response").text(msg_arr[2]);
                        $("#cmdSubmit").hide();
                    } else {
                        alert(msg_arr[2]);
                    }
                }
            });
        }
    });

</script>
<form id="form1" runat="server">
    <asp:label id="lblIncdntNo" runat="server" text="Label"></asp:label>
    <asp:label id="lblIncdntStatus" runat="server" text="Label"></asp:label>
    <div style="position:relative;float:left; width:588px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">View Action Details</h1>
        <div class="div-fullwidth marginbottom">
            <p class="content">Incident Description</p>
            <asp:TextBox ID="txtIncdntDesc" runat="server" TextMode="MultiLine" ReadOnly="True" style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;"></asp:TextBox>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">Comments</p>
            <asp:TextBox ID="txtLevelComment" runat="server" TextMode="MultiLine" style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;"></asp:TextBox>
        </div>
        <div style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 0px;width: 99.7%;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom:20px;">
                <h1 class="content" style="margin-left:10px;">Select Decision : </h1>
                <input type="radio" name="my_hint" id="rev" value="REVISE" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;" /><label for="rev" class="content" style="cursor: pointer;">Revise Action</label>
                <input type="radio" name="my_hint" id="close" value="CLOSE" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;"/><label for="close" class="content" style="cursor: pointer;">Close</label>
                <input type="radio" name="my_hint" id="uplevel" value="FWDUPLEVEL" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;"/><label for="uplevel" class="content" style="cursor: pointer;">Final Acceptance</label>
            </div>
        <div id="div_select_accpt_level" class="div-fullwidth marginbottom" style="width:auto; display:none;">
            <p class="content">Select Acceptance Authority</p>
            <asp:TextBox class="CheckValidation" ID="txtAutorityName" runat="server"></asp:TextBox>
        </div>
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
        <div class="div-fullwidth">
            <p id="response" class="content" style="width: auto; color:Red;"></p>
            <input type="button" id="cmdReset" class="g-button g-button-red" value="Close" onclick="window.location=self.location; $.fancybox.close();" style="float:right;" />
            <input type="button" id="cmdSubmit" class="g-button g-button-submit" value="Save" style="margin-right:10px; margin-bottom:0px"/>
        </div>
    </div>
</form>
