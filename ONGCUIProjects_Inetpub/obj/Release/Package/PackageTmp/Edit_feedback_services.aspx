<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="c#" CodeBehind="Edit_feedback_services.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Edit_feedback_services" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE html>
<html>
<head>
    <title>Uran Online Information System</title>
    <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <link href="style/ONGCStyle.css" type="text/css" rel="stylesheet" />
    <link href="style/treeStyle.css" type="text/css" rel="stylesheet" />
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.7.2.min_a39dcc03.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="js/jqClock.min.js"></script>
    <link href="js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function (e) {
            var varNT = 0;
            var vardrop = 0;
            $('#cmdNewNotifications').click(function () {
                varNT = 1;
                $(".notification-box").fadeIn();
            });
            $(document).click(function () {
                if (varNT == 0) {
                    $(".notification-box").fadeOut();
                } else {
                    varNT = 0;
                }
                if (vardrop == 0) {
                    $(".settings_droparea").hide();
                } else {
                    vardrop = 0;
                }
            });
            $("#div_iframe").click(function () {
                if (varNT == 0) {
                    $(".notification-box").fadeOut();
                }
                else {
                    varNT = 0;
                }
            });
            $("a[href*='aspx']").click(function () {
                $("div#div_iframe").empty();
                var target = $(this).attr("href") + "?force=" + Math.floor((Math.random() * 9999) + 1);
                $("div#div_iframe").append('<iframe src="' + target + '" name="doc" frameborder="no" width="100%" scrolling="auto" height="100%"></iframe>');

            });
            // cmdSettings
            $("#cmdSettings").click(function () {
                $(".settings_droparea").show();
                vardrop = 1;
            });
            $("#cmdChangePassword").fancybox();


            var ReponseReceived = true;
            setInterval(getNotification, 15000);
            function getNotification() {
                if (ReponseReceived == true) {
                    ReponseReceived == false;
                    $.ajax({
                        url: "services/Notification.aspx",
                        success: function (msg) {
                            var something = msg.split('~');
                            if (something[0] == 'TRUE') {
                                if (something[1] == 'SUCCESS') {
                                    if (something[2] == "0") {
                                        $("#cmdNewNotifications").html("No Notification");
                                        $("#notification_heading").html(something[3]);
                                        $("#div_notification").hide();
                                    } else {
                                        $("#cmdNewNotifications").html("New Notifications<p class=\"notificationsCircle\" style=\"float:left;\"><span>" + something[2] + "</span></p>");
                                        $("#ul_notification").html("<ul class=\"notification-ul\">" + something[3] + "</ul>");
                                        $("#notification_heading").html("New Notification");
                                        $("#div_notification").show();
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });
    </script>
    <style type="text/css">
        .style1 {
            width: 1259px;
            height: 78px;
        }

        .style2 {
            width: 245px;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" style="overflow-x: hidden;">


    <form id="Form1" method="post" runat="server" style="position: absolute; top: 0px; bottom: 0; right: 0; left: 0; margin-bottom: 25px;">

        <table style="margin-left: 20px; margin-top: 10px;">
            <tr>
                <td>
                    <asp:Label ID="lbl_ci" runat="server" Text="Select Department:-" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_dept" runat="server" Width="200px" ForeColor="Black" Height="40px" AutoPostBack="true" OnSelectedIndexChanged="ddl_dept_SelectedIndexChanged"></asp:DropDownList>
                </td>
                <td></td>

                <td></td>

                <td>
                    <asp:Button ID="btn_all" runat="server" Text="Save Hide Records" Font-Bold="true" ForeColor="White" BackColor="Blue" Width="150px" Height="40px" OnClick="btn_all_Click" />
                </td>
            </tr>

        </table>



        <div style="float: left; margin-left: 120px; margin-top: 100px; margin-bottom: 10px; height: 500px; overflow: scroll;">
            <asp:GridView ID="grid_dept" runat="server" AutoGenerateColumns="False" Width="500px" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkbox_Headers" runat="server" AutoPostBack="true" OnCheckedChanged="chkbox_Headers_CheckedChanged" />
                            <asp:Label runat="server" Font-Bold="true" Text="Select All."></asp:Label>

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="hdn_ids" runat="server" Value='<%#Eval("ID")%>' />
                            <asp:CheckBox ID="chkbox_confgrd" Checked='<%#Eval("IS_HIDE")%>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Infocom Facility/Services">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>



                </Columns>

                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />

            </asp:GridView>
        </div>
    </form>
</body>
</html>
