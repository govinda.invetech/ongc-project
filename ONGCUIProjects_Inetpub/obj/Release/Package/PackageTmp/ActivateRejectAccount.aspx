﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivateRejectAccount.aspx.cs" Inherits="ONGCUIProjects.ActivateRejectAccount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="css/styleGlobal.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/script.js"></script>
        <script type="text/javascript" language="javascript" src="js/slider.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen">
        <link rel="stylesheet" href="css/diggy.css?ver=4.5.3.3" type="text/css" media="screen">
        <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen">
        <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
        
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".cmdAction").click(function () {
                    var sActionType = $(this).attr('ActionType');
                    var sUser = $(this).attr("user");
                    $.ajax({
                        type: "GET",
                        url: "services/AccountAction.aspx",
                        data: "type=" + sActionType + "&user=" + sUser,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                alert(msg_arr[2]);
                                window.location = self.location;
                            } else {
                                alert("Problem in Execution, Please contact Admin...!");
                            }
                        }
                    });
                });
            });
        </script>
    <style type="text/css">
        table.tftable td 
        {
            padding:3px;
        }
        
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="div-fullwidth iframeMainDiv">
                <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                    <h1 class="heading" style="width: auto;">Account Reactivation</h1>
                  
                </div>
                <div class="div-pagebottom"></div>
            <div runat="server" ID="DivActivationData" class="div-fullwidth marginbottom">
                
            </div>
        </div>
    </form>
</body>
</html>
