<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAllNews.aspx.cs" Inherits="ONGCUIProjects.ViewAllNews" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
	<head>
		<title>Uran Online Information System</title>
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
        <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery-1.7.2.min_a39dcc03.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen">
        <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link href="js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/jquery.paginate.css" media="screen" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.paginate.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function (e) {

                var sQueryHint = $("#txtType").val();
                ShowHideInput(sQueryHint);

                $('input[name="my_hint"][value="' + sQueryHint + '"]').prop('checked', true);

                $('input[name="my_hint"]').change(function () {
                    sQueryHint = $(this).val();
                    ShowHideInput(sQueryHint);
                });

                function ShowHideInput(sQueryType) {
                    if (sQueryType == "MONTH") {
                        $("#txtNewsDate").hide();
                        $("#txtNewsDate").val("");
                        $("#txtNewsMonth").show();
                    } else if (sQueryType == "DATE") {
                        $("#txtNewsDate").show();
                        $("#txtNewsMonth").hide();
                        $("#txtNewsMonth").val("");
                    }
                }
                $("#txtNewsDate").datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true
                });

                $("#txtNewsMonth").datepicker({
                    dateFormat: "mm-yy",
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    currentText: "Current Month",
                    onClose: function (dateText, inst) {
                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                        $(this).datepicker('setDate', new Date(year, month, 1));
                    },
                    
                });
                $("#txtNewsMonth").focus(function () {

                    if (sQueryHint == "MONTH") {
                        $(".ui-datepicker-calendar").remove();
                        $(".ui-datepicker-prev").remove();
                        $(".ui-datepicker-next").remove();
                        $("#ui-datepicker-div").css("width", "192px");
                        $(".ui-datepicker-title").css("margin-left", "4px");
                        $(".ui-datepicker-title").css("margin-right", "0px");
                        $(".ui-datepicker-title").children('select').css("width", "85px");
                        $("#ui-datepicker-div").position({
                            my: "center top",
                            at: "center bottom",
                            of: $(this)
                        });
                    }
                });


                $("#cmdGo").click(function () {
                    var NewsDate = "";
                    var Hint = "";
                    if (sQueryHint == "MONTH") {
                        NewsDate = $("#txtNewsMonth").val().trim();
                        Hint = "m";
                    } else if (sQueryHint == "DATE") {
                        NewsDate = $("#txtNewsDate").val().trim();
                        Hint = "d";
                    }
                    if (NewsDate == "") {
                        alert("Please select a date");
                        return false;
                    } else {
                        window.location = "ViewAllNews.aspx?type=" + Hint + "~" + NewsDate;
                    }
                });


                var varNT = 0;
                var vardrop = 0;
                $('#cmdNewNotifications').click(function () {
                    varNT = 1;
                    $(".notification-box").fadeIn();
                });
                $(document).click(function () {
                    if (varNT == 0) {
                        $(".notification-box").fadeOut();
                    } else {
                        varNT = 0;
                    }
                    if (vardrop == 0) {
                        $(".settings_droparea").hide();
                    } else {
                        vardrop = 0;
                    }
                });
                $("#div_iframe").click(function () {
                    if (varNT == 0) {
                        $(".notification-box").fadeOut();
                    } else {
                        varNT = 0;
                    }
                });
                // cmdSettings
                $("#cmdSettings").click(function () {
                    $(".settings_droparea").show();
                    vardrop = 1;
                });
                $("#cmdChangePassword").fancybox();


                setInterval(getNotification, 2000);
                var ReponseReceived = true;
                getNotification();
                function getNotification() {
                    if (ReponseReceived == true) {
                        ReponseReceived == false;
                        $.ajax({
                            url: "services/Notification.aspx",
                            success: function (msg) {
                                var something = msg.split('~');
                                if (something[0] == 'TRUE') {
                                    if (something[1] == 'SUCCESS') {
                                        if (something[2] == "0") {
                                            $("#cmdNewNotifications").html("No Notification");
                                            $("#notification_heading").html("No Notification");
                                            $("#div_notification").hide();
                                        } else {
                                            $("#cmdNewNotifications").html("New Notifications<p class=\"notificationsCircle\" style=\"float:left;\"><span>" + something[2] + "</span></p>");
                                            $("#ul_notification").html(something[3]);
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            });
        </script>
        <style type="text/css">
            .div-select{
                position: relative;
                left: 0px;
                padding: 2px 0px;
                width: 99.7%;
                background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
                background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); 
                border: 1px solid #ccc; 
            }
        </style>
	</head>
	<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="txtType" runat="server" />
    <!-- Header Starts--------------------------------------------------------------------------->
            <div class="header">
                <div class="logo-container">
                    <div class="logo"></div>
                    <div class="logo-text">
                        <h1 class="logo-text-line2">Oil and Natural Gas Corporation Limited</h1>
                        <h1 class="logo-text-line3">URAN PLANT, MUMBAI</h1>
                    </div>
                    <%--<asp:Label ID="txtServerTime" runat="server" Text="" class="ServerTime">Server Time : <span id="as"></span></asp:Label>--%>
                    <asp:LoginStatus ID="cmdLogout" runat="server" CssClass="logout" 
                        LogoutAction="Redirect" LogoutPageUrl="~/Index.aspx" LoginText="Logout" 
                        onloggingout="cmdLogout_LoggingOut" />
                    <div style="position: relative; float: right; margin-right: 10px;">
                        <asp:LinkButton ID="cmdSettings" runat="server" CssClass="settings" onclientclick="return false;"><img src="Images/settings_icon.png" class="settings_icon" />Settings<img src="Images/arrow_down_white.png" class="settings_icon" style="float: right;" />
                        </asp:LinkButton>
                        <div class="settings_droparea">
                            <asp:LinkButton ID="cmdAccountSetting" runat="server" CssClass="accountsettings" onclick="cmdAccountSetting_Click" ><div class="settings_icons2"></div>Update Profile</asp:LinkButton>
                            <a id="cmdChangePassword" class="changepassword" href="services/changepassword.aspx"><div class="lock_icon"></div>Change Password</a>
                        </div>
                    </div>
                    <asp:LinkButton ID="cmdHome" runat="server" CssClass="home" onclick="cmdHome_Click" >Home</asp:LinkButton>
                </div>
                <div class="logged-user-details">
                    <h1>Welcome</h1>
                    <asp:Label ID="lblSessionLoginName" runat="server" CssClass="username" Text=""></asp:Label>
                    <asp:Label ID="lastlogin" runat="server" Text=""></asp:Label>
                    <div style="position: relative; float:right;">
                        <a runat="server" href="javascript:void(0)" class="buttonsNotification" id="cmdNewNotifications">New Notifications<p class="notificationsCircle" style="float:left;"><span>12</span></p></a>
                        <div class="notification-box" id="notificationContainer">
                            <div class="jewelBeeperHeader">
                                <div class="beeperNubWrapper">
                                    <div class="beeperNub"></div>
                                </div>
                            </div>
                            <div class="notification_content">
                                <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                                    <div class="clearfix uiHeaderTop">
                                        <div>
                                            <h3 runat="server" id="notification_heading" class="uiHeaderTitle">Notifications</h3>
                                        </div>
                                    </div>
                                </div>
                                <div runat="server" id="div_notification" class="uiScrollableArea fade uiScrollableAreaWithShadow" style="width: 330px; height: 100%;">
                                    <div class="uiScrollableAreaWrap scrollable" tabindex="0">
                                        <div class="uiScrollableAreaBody" style="width:330px;">
                                            <div class="uiScrollableAreaContent">
                                                <ul runat="server" id="ul_notification" class="notification-ul">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header Ends----------------------------------------------------------------------------->

            <div class="container">
                <div class="body-content" style="background-color: #f9f9f9; padding: 10px; margin-bottom:30px; box-shadow: 0 0 40px rgb(104, 104, 104);">
                    <h1 class="heading" style="border-bottom: 2px solid rgb(122, 122, 122); font-size: 25px; font-weight: bold; width: 99%; margin-bottom: 10px;">NEWS ARCHIVE
                      <div class="div-select" style='width:500px; float:right;'>
                                <h1 class="content" style="margin-left:10px; line-height:normal;">Select</h1>
                                <input type="radio" name="my_hint" id="optDate" value="DATE" style="float: left; margin: 5px 6px 6px; border: 3px solid rgb(62, 146, 167); height: 19px; width: 19px;" /><label for="optDate" class="content" style="cursor: pointer;line-height:normal;">Date</label>
                                <input type="radio" name="my_hint" id="optMonth" value="MONTH" style="float: left; margin: 5px 6px 6px; border: 3px solid rgb(62, 146, 167); height: 19px; width: 19px;"/><label for="optMonth" class="content" style="cursor: pointer;line-height:normal;">Month</label>
                                <input runat="server" type="text" style="width:200px;" value="" id="txtNewsDate" />
                                <input runat="server" type="text" style="width:200px; display:none;" value="" id="txtNewsMonth" />
                                <input id="cmdGo" type="button" style="margin-top: 1px;  margin-right:5px;" value="GO" class="g-button g-button-submit" />
                            </div>
                      </h1>  
                            

                    
                    <div runat="server" id="DivNewsContainer" class="AllNewsContainer">
                        
                    </div>
                    
                </div>
                
            </div>
		    <!-- Footer Starts--------------------------------------------------------------------------->
            <div class="footer">
                <div class="footer-content">
                    <p class="copyright"><%--� 2012 ONGC Limited. All rights reserved.--%></p>
                    <p class="designedby">
                        Maintained by Infocom Services, Uran
                    </p>
                </div>
            </div>
            <!-- Footer Ends----------------------------------------------------------------------------->

            </form>
	</body>
</html>