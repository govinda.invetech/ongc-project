<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="c#" CodeBehind="Feedback_infoReview.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Feedback_infoReview" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE html>
<html>
<head>
    <title>Uran Online Information System</title>
    <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <link href="style/ONGCStyle.css" type="text/css" rel="stylesheet" />
    <link href="style/treeStyle.css" type="text/css" rel="stylesheet" />
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.7.2.min_a39dcc03.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="js/jqClock.min.js"></script>
    <link href="js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function (e) {
            var varNT = 0;
            var vardrop = 0;
            $('#cmdNewNotifications').click(function () {
                varNT = 1;
                $(".notification-box").fadeIn();
            });
            $(document).click(function () {
                if (varNT == 0) {
                    $(".notification-box").fadeOut();
                } else {
                    varNT = 0;
                }
                if (vardrop == 0) {
                    $(".settings_droparea").hide();
                } else {
                    vardrop = 0;
                }
            });
            $("#div_iframe").click(function () {
                if (varNT == 0) {
                    $(".notification-box").fadeOut();
                }
                else {
                    varNT = 0;
                }
            });
            $("a[href*='aspx']").click(function () {
                $("div#div_iframe").empty();
                var target = $(this).attr("href") + "?force=" + Math.floor((Math.random() * 9999) + 1);
                $("div#div_iframe").append('<iframe src="' + target + '" name="doc" frameborder="no" width="100%" scrolling="auto" height="100%"></iframe>');

            });
            // cmdSettings
            $("#cmdSettings").click(function () {
                $(".settings_droparea").show();
                vardrop = 1;
            });
            $("#cmdChangePassword").fancybox();


            var ReponseReceived = true;
            setInterval(getNotification, 15000);
            function getNotification() {
                if (ReponseReceived == true) {
                    ReponseReceived == false;
                    $.ajax({
                        url: "services/Notification.aspx",
                        success: function (msg) {
                            var something = msg.split('~');
                            if (something[0] == 'TRUE') {
                                if (something[1] == 'SUCCESS') {
                                    if (something[2] == "0") {
                                        $("#cmdNewNotifications").html("No Notification");
                                        $("#notification_heading").html(something[3]);
                                        $("#div_notification").hide();
                                    } else {
                                        $("#cmdNewNotifications").html("New Notifications<p class=\"notificationsCircle\" style=\"float:left;\"><span>" + something[2] + "</span></p>");
                                        $("#ul_notification").html("<ul class=\"notification-ul\">" + something[3] + "</ul>");
                                        $("#notification_heading").html("New Notification");
                                        $("#div_notification").show();
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });
    </script>
    <style type="text/css">
        .style1 {
            width: 1259px;
            height: 78px;
        }

        .style2 {
            width: 245px;
        }

 .gridcss
{
background:#df5015;
font-weight:bold;
color:White;
}
    </style>
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" style="overflow-x: hidden;">


    <form id="Form1" method="post" runat="server" style="position: absolute; top: 0px; bottom: 0; right: 0; left: 0; margin-bottom: 25px;">

        <table style="margin-left: 20px; margin-top: 10px;">
            <tr>
                <td>
                    <asp:Label ID="lbl_ci" runat="server" Text="Select CPF No:-" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_user" runat="server" Width="200px" ForeColor="Black"  Height="40px" AutoPostBack="true" OnSelectedIndexChanged="ddl_user_SelectedIndexChanged"></asp:DropDownList>
                </td>
                <td></td>
                <td>
                    <asp:Label ID="lbl_id" runat="server" Text="Select Feedback:-" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_id" runat="server" Width="200px" ForeColor="Black"  Height="40px" AutoPostBack="true" OnSelectedIndexChanged="ddl_id_SelectedIndexChanged"></asp:DropDownList>
                </td>
                <td></td>
                  <td>
                    <asp:Button ID="btn_view" runat="server" Text="View FeedBack" Font-Bold="true" Height="40px"  OnClick="btn_view_Click" />
                </td>
                <td>
                    <asp:Button ID="btn_all" runat="server" Text="Approve All" Font-Bold="true" ForeColor="White" BackColor="Blue" Width="100px" Height="40px" OnClick="btn_all_Click" />
                </td>
            </tr>

        </table>



        <div style="float: left; margin-left: 10px; margin-top: 100px; margin-bottom: 10px; height: 500px; width:980px; overflow: scroll;">

            <table border="1">
                <tr>
                    <td>
                        <asp:Label ID="lbl_name" runat="server" Text="Emp_Name:"></asp:Label>
                    </td>
                    <td></td>
                    <td>
                    <asp:Label ID="lbls_nme" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbl_no" runat="server" Text="CPF_NO.:"></asp:Label>
                    </td>
                    <td></td>
                    <td>
                        <asp:Label ID="lbl_cpf" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbl_date" runat="server" Text="Date:"></asp:Label>
                    </td>
                    <td></td>
                    <td>
                        <asp:Label ID="lbl_dates" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <asp:GridView ID="grid_service" runat="server" Width="900px"  OnRowDataBound="grid_service_RowDataBound">
              <%--  <Columns>                  


                    <asp:TemplateField>
                        <HeaderTemplate>
                            <center><asp:Label runat="server"     Font-Bold="true" Text="CPF_NO"></asp:Label></center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbl_CPF_NO" runat="server" Text='<%#Eval("CPF_NO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <center><asp:Label runat="server"     Font-Bold="true" Text="Service Name"></asp:Label></center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbl_NAME" runat="server" Text='<%#Eval("NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <center><asp:Label runat="server"     Font-Bold="true" Text="Date"></asp:Label></center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbl_date" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd-MM-yyyy}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                        <HeaderTemplate>
                            <center><asp:Label runat="server"     Font-Bold="true" Text="Remark"></asp:Label></center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbl_Remark" runat="server" Text='<%#Eval("remark")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <center><asp:Label runat="server"     Font-Bold="true" Text="Rating"></asp:Label></center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbl_RATE" runat="server" Text='<%#Eval("RATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    

                </Columns>--%>

            </asp:GridView>

         
        </div>
    </form>
</body>
</html>
