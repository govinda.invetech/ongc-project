<%@ Page language="c#" Codebehind="Safety_Measure_Master.aspx.cs" AutoEventWireup="True" Inherits="WebApplication1.Safety_Measure_Master" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Safety_Measure_Master</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript">
			function ConfirmDelete(msg)
			{
				return confirm("Are your sure to Insert ? \nThis can not be undone ...!");
			}
		</script>
	</HEAD>
	<body>
		<FORM id="Form1" method="post" runat="server">
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U></U></STRONG></FONT></FONT>&nbsp;</P>
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U>Incident 
								Management - Safety Measure Master</U></STRONG></FONT></FONT></P>
			<P align="center">
				<TABLE id="Table2" style="WIDTH: 654px; HEIGHT: 56px" cellSpacing="8" cellPadding="4" width="654"
					align="center" bgColor="whitesmoke" border="1">
					<TR>
						<TD style="WIDTH: 335px" borderColor="gray" align="center" bgColor="whitesmoke">
							<P align="center"><FONT face="Verdana" color="#660000" size="1"><STRONG>Safety / Precaution 
										Measure</STRONG></FONT></P>
						</TD>
						<TD style="WIDTH: 281px" borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" size="1">
								<P align="center">
									<asp:textbox id="txtSftyMeasure" runat="server" Width="280px" Font-Names="Verdana" Font-Size="XX-Small"
										MaxLength="2000" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0" Height="88px"
										TextMode="MultiLine"></asp:textbox></P>
							</FONT>
						</TD>
					</TR>
				</TABLE>
			</P>
			<P align="center">
				<asp:button id="BtnCancel" runat="server" Width="150px" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Text="Cancel" CausesValidation="False" Font-Bold="True"
					BackColor="Silver" ForeColor="ControlText" Height="18px"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnSaveSftyMeasure" runat="server" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Text="Create New Safety Measure" Font-Bold="True"
					BackColor="Silver" ForeColor="ControlText" onclick="BtnSaveSftyMeasure_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnRefresh" runat="server" Width="158px" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Text="Refresh" CausesValidation="False" Font-Bold="True"
					BackColor="Silver" ForeColor="ControlText" Height="18px"></asp:button></P>
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U></U></STRONG></FONT></FONT>&nbsp;</P>
			<P align="center">
				<TABLE id="Table1" style="WIDTH: 654px; HEIGHT: 56px" cellSpacing="8" cellPadding="4" width="654"
					align="center" bgColor="whitesmoke" border="1">
					<TR>
						<TD style="WIDTH: 335px" borderColor="gray" align="center" bgColor="whitesmoke">
							<P align="center"><FONT face="Verdana" color="#660000" size="1"><STRONG>Select Safety / 
										Precaution Measure</STRONG></FONT></P>
						</TD>
						<TD style="WIDTH: 281px" borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" size="1">
								<P align="center">
									<asp:dropdownlist id="ddlSafetyMeasures" runat="server" Font-Size="XX-Small" Font-Names="Verdana"
										Width="221px" ForeColor="Maroon" Font-Bold="True"></asp:dropdownlist></P>
							</FONT>
						</TD>
					</TR>
				</TABLE>
			</P>
			<P align="center">
				<asp:button id="btnDeleteMaster" runat="server" Height="40px" BorderColor="DarkGray" BorderStyle="Solid"
					Font-Size="XX-Small" Font-Names="Verdana" ForeColor="ControlText" BackColor="Silver" Font-Bold="True"
					Text="Delete Selected Safety / Precaution Measures" onclick="btnDeleteMaster_Click"></asp:button></P>
		</FORM>
	</body>
</HTML>
