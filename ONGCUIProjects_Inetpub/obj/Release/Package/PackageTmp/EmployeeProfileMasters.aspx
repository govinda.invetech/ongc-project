﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeProfileMasters.aspx.cs"
    Inherits="ONGCUIProjects.EmployeeProfileMasters" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/script.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen"/>
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css"/>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#cmdAddMasterFancyBox").fancybox({ modal: true });
            $("#txtMasterText").focusin(function () {
                $("#Response").text("");
            });
            $("#cmdAddMaster").live("click", function () {
                var MasterType = $(this).attr("MasterType");
                var MasterValue = $("#txtMasterText").val().trim();
                if (MasterValue == "") {
                    alert("Please enter " + $("#InputBoxHeading").text().toLowerCase());
                } else {
                    $.ajax({
                        type: "POST",
                        url: "services/AddDeleteProfileMaster.aspx",
                        data: "ActionType=ADD&ActionData=" + encodeURIComponent(MasterValue.toUpperCase()) + "~" + MasterType,
                        success: function (msg) {
                            var msg_arr = msg.split('~');
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                $("#Response").text(msg_arr[2]);
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });
            $(".cmdDeleteMaster").live("click", function () {
                var DelIDType = $(this).attr("DelIDType");
                if (confirm("Are you sure to delete?")) {
                    $.ajax({
                        type: "POST",
                        url: "services/AddDeleteProfileMaster.aspx",
                        data: "ActionType=DEL&ActionData=" + DelIDType,
                        success: function (msg) {
                            var msg_arr = msg.split('~');
                            alert(msg_arr[2]);
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                window.location = self.location;
                            }
                        }
                    });
                }
            });
        });
		       
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server" class="MainInterface-iframe">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 runat="server" id="HeaderToShow" class="heading" style="width: auto;">Employee Profile Masters</h1>
            <input type="button" value="DESIGNATION" class="g-button g-button-submit" onclick="window.location='EmployeeProfileMasters.aspx?type=DESIGNATION'" style="margin-right: 10px; margin-top: 4px;" />
            <input type="button" value="LOCATION" class="g-button g-button-submit" onclick="window.location='EmployeeProfileMasters.aspx?type=LOCATION'" style="margin-right: 10px; margin-top: 4px;" />
            <input type="button" value="DEPARTMENT" class="g-button g-button-submit" onclick="window.location='EmployeeProfileMasters.aspx?type=DEPARTMENT'" style="margin-right: 10px; margin-top: 4px;" />
            <asp:LinkButton ID="cmdAddMasterFancyBox" runat="server" class="g-button g-button-share" href="#AddMasterFancyBoxContent" style="margin-right: 10px; margin-top: 4px; float: right;" onclientclick="return false;" Visible="False"></asp:LinkButton>
        </div>
        <div class="div-pagebottom">
        </div>
        <div runat="server" class="div-fullwidth" id="div_master_list" style="margin: 10px 0px;">
            <div style="left: 40%; top: 50%; position: fixed;">
                <p class="content" style="font-size:20pt;">Please select an option</p>
            </div>
        </div>
    </div>
    <div style="display: none">
        <div id="AddMasterFancyBoxContent" style="width:500px;">
            <h1 class="heading" ID="FancyBoxHeading" runat="server" style="padding: 0; border-bottom: 2px solid #58595A;"></h1>
            <div style="position: relative; float: left; margin-top: 10px; margin-bottom: 10px; width: 100%;">
                <p ID="InputBoxHeading" runat="server" class="content" style="margin-right:0px;"></p>
                <p class="content" style="margin-right:10px;">(<span style='color:red;'>*</span>)</p>
                <asp:TextBox ID="txtMasterText" runat="server" EnableTheming="True" Width="310px" style="float:right;"></asp:TextBox>
            </div>
            <h1 class="heading" style="padding: 0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
            <div class="div-fullwidth">
                <p id="Response" class="content" style="width: auto; color: Red;"></p>
                <input type="button" class="g-button g-button-red" onclick="window.location=self.location;" style="float: right;" value="CANCEL" />
                <input runat="server" id="cmdAddMaster" type="button" class="g-button g-button-submit" style="float: right; margin-right: 10px" value="SAVE" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
