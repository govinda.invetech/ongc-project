﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewContacts.aspx.cs" Inherits="ONGCUIProjects.ViewContacts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Uran Online Information System</title>
    <link rel="shortcut icon" href="Images/logo-ongc_small.jpg">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/demo_table_jui.css" type="text/css" rel="stylesheet" />
    <link href="css/smoothness/jquery-ui-1.8.4.custom.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery_ashish.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="js/ZeroClipboard.js"></script>
    <script type="text/javascript" language="javascript" src="js/TableTools.js"></script>
    <!-- JS Links Ends -->
    <script type="text/javascript">
        $(document).ready(function () {
            var windowheight = $(window).height() - 172;
            $('#example').dataTable({
                "bJQueryUI": true,
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": parseInt(windowheight),
                "bPaginate": false,
            });

            $('#example_filter').children('label').children('input[type="text"]').focus();
            $('#example_filter').children('label').children('input[type="text"]').css('width', '250px');
        });
    </script>
</head>
<body style="font-family: MyriadPro-Regular; font-size: 13px;">
    <form id="form1" runat="server">
    <div class="div-fullwidth iframeMainDiv" style="min-width: 905px;">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                <img src="Images/contact_icon.png" style="position: relative; float: left; margin: 8px 5px 0 0;
                    opacity: 0.78;" />View Contact List</h1>
            <%--<a href="javascript:void(0);" id=""  style="float:right; margin-right:10px; margin-top: 5px;" class="g-button g-button-red">Export TO Excel</a>--%>
            <asp:Button ID="cmdExportToPDF" runat="server" Style="float: right; margin-top: 3px;"
                Text="Export to PDF" CssClass="g-button g-button-share" OnClick="cmdExportToPDF_Click" />
            <asp:Button ID="cmdExportToExcel" runat="server" Style="float: right; margin-right: 10px;
                margin-top: 3px;" Text="Export to Excel" CssClass="g-button g-button-share" OnClick="cmdExportToExcel_Click" />
        </div>
        <div class="div-pagebottom">
        </div>
        <div runat="server" id="table_container" class="div-fullwidth">
        </div>
    </div>
    </form>
</body>
</html>
