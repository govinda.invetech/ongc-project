<%@ Page language="c#" Codebehind="Guest_House_Booking_App.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Guest_House_Booking_App" %>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<title>VisitorPass_Approval1</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <script src="js/jquery-1.8.2.js"></script>
         <script src="js/jquery-1.8.2.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <script type="text/javascript" src="js/jquery.gritter.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>     
        <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />     
        <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
         <link rel="stylesheet" type="text/css" href="css/colortip-1.0-jquery.css"/>   
		<script language="javascript">
		    $(document).ready(function () {
		        $(document).ajaxStop($.unblockUI);
		        $(".cmdApproveReject").fancybox();
		        //getpending or booking history

		    });		       
          </script>    
           <style type="text/css">
                .tooltip {
			color: #000000; outline: none;
			cursor: default; text-decoration: none;
			position: relative;
		}
		.tooltip span {
			margin-left: -999em;
			position: absolute;
		}
		.tooltip:hover span {
			border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; 
			box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                        -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
			font-family: Calibri, Tahoma, Geneva, sans-serif;
			position: absolute; left: 1em; top: 1.5em; z-index: 99;
			margin-left: 0; width: 250px;
			color:White;
		}
		.info { background: black; padding: 0.3em 0.3em 0.3em 0.3em;	}
            </style> 
	</HEAD>
    
	<body>
		<form id="Form1" method="post" runat="server" class="MainInterface-iframe">
            <div class="div-fullwidth iframeMainDiv">
			    <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
                    <h1 class="heading" style="width: auto;"><img src="Images/guesthouse_icon.png" style="position: relative; float: left; margin: 2px 5px 0 0;" />Guest House Booking Details to Approve</h1>
                </div>
                <div class="div-pagebottom"></div>
                <p runat="server" id="P1" class="content" style="color:red; font-size: 20px;"></p>

                <div  runat="server" class="div-fullwidth" id="pening_detail_div" >

             


                </div>
               
            </div>	             
		</form>
        <script type="text/javascript" src="js/colortip-1.0-jquery.js"></script>
    <script type="text/javascript" src="js/colortip.js"></script>
	</body>
</HTML>
