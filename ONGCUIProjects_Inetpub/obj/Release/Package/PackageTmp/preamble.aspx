﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="preamble.aspx.cs" Inherits="ONGCUIProjects.preamble" %>

    <form id="form1" runat="server">
    <div class="preamble_window">
        
        <h1 class="preamble-window_heading" style="padding:0; border-bottom: 2px solid white; margin-bottom: 10px; text-align: center;">Preamble - Pulse of Uran</h1>
        
        <div class="preamble_content_heading">
            <div class="preamble_content">
                <img runat="server" id="preamble_image" class="preamble_pic" style="display: none;" />
                <!--
                <asp:label runat="server" id="preamble_text" text="Label"></asp:label>
                -->
                <p class="preambleContentDemoPurpose">For any successful enterprise the most treasured assets are Human resource. Each individual is important towards our progress contributing for making “Unnat URAN! Ujjwal URAN!!” and their collective efforts provide the momentum not only for the growth and prosperity of our plant, organization but also for the country as a whole.</p>
                <p class="preambleContentDemoPurpose">To understand and to recognize the opinion of the employees of Uran Plant about the work culture & practices being followed in the plant, concept of “Pulse of Uran” will be introduced shortly on Uran Online Information Services.</p>
                <p class="preambleContentDemoPurpose">The objective of this exercise is to capture the perception of work culture & practices being followed in the Plant including safety & security. This is intended to aid management decision for the benefit of the employees.</p>
                <p class="preambleContentDemoPurpose">Pulse of Uran is being introduced to understand the spirits and aspirations of the Plant personnel and intellectual health of the plant as a whole in the same fashion as by sensing the “Pulse” a doctor can understand about the health of an individual.</p> 
                <p class="preambleContentDemoPurpose">In this respect a new question will be posted every dayat 11am with multiple options on the portal. The participants will have to respond by selecting the most appropriate option and participation will be mandatory for all the Plant Personnel. The result of the survey will be displayed after the closing time i.e. 4pm.</p>
                <p class="preambleContentDemoPurpose">Indeed, this will be a unique platform in ONGC wherein members of Uran family can express their opinion in a free and frank manner.</p>
                <ul class="preamble-ul-DemoPurpose" style="display:none;">
                    <li>The Survey Question will be posted daily by the Administrator with different options for answer.</li>
                    <li>The participants can respond to the appropriate answer options within the time frame displayed.</li>
                    <li>The result of the Survey shall be displayed after the closing time.</li>
                    <li>The responses shall be anonymous and honest opinion is highly solicited. </li>
                </ul>
            </div>
        </div>
        
    
</div>

    </form>
