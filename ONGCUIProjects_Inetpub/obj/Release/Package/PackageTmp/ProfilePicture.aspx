﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProfilePicture.aspx.cs" Inherits="ONGCUIProjects.ProfilePicture" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ONGC Image Gallery</title>
    <meta name="description" content="Bootstrap Image Gallery is an extension to blueimp Gallery, a touch-enabled, responsive and customizable image and video gallery. It displays images and videos in the modal dialog of the Bootstrap framework, features swipe, mouse and keyboard navigation, transition effects, fullscreen support and on-demand content loading and can be extended to display additional content types.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/gallery/blueimp-gallery.min.css">
    <link rel="stylesheet" href="css/gallery/bootstrap-image-gallery.css">
    <link rel="stylesheet" href="css/gallery/demo.css">

    <link href="style/ONGCStyle.css" type="text/css" rel="stylesheet" />
    <link href="style/treeStyle.css" type="text/css" rel="stylesheet" />
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />

    <%-- <link href="http://hayageek.github.io/jQuery-Upload-File/4.0.2/uploadfile.css" rel="stylesheet">--%>
    <style>
        .input-group {
            color: black;
        }
    </style>
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top navbar-inverse">

        <div class="container-fluid">
            <form runat="server" class="form-inline" id="fileupload" method="POST">
                

                <div class="alert alert-danger" role="alert">

                    <div class="form-group">
                        <%-- id="image-gallery-button"--%>
                        <button id="btnimage" style="" type="button" class="btn btn-success btn-lg">
                            <i class="glyphicon glyphicon-picture"></i>
                            View Image Albums
                   
                        </button>
                    </div>
                    <div class="form-group" runat="server" id="foradmin3">
                        <button id="create-folder-button" type="button" class="btn btn-success btn-lg">
                            <i class="glyphicon glyphicon-picture"></i>
                            Create New Album
                   
                        </button>
                    </div>

                </div>

                <div id="divCreateFolder" class="well well-lg">
                    <div class="input-group">
                        <img src="/Images/folderImg.ico" alt="Image Folder" style="width: 100px; height: 100px;">
                    </div>
                    <div class="input-group">
                        <asp:TextBox ID="txtFolderName" class="form-control" placeholder="Enter Album Name" aria-describedby="sizing-addon2" runat="server"></asp:TextBox>
                         <asp:TextBox ID="txtUserName" class="form-control" placeholder="Enter Person Name" aria-describedby="sizing-addon2" runat="server"></asp:TextBox>
                    </div>
                    <div class="input-group">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <span class="btn btn-success btn-file">
                                <input type="file" id="myfile" multiple="multiple" name="myfile" runat="server" /></span>
                            <span class="fileupload-preview"></span>
                        </div>
                    </div>
                    <div class="input-group">
                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-warning" folder_name="new" Text="Upload" OnClick="btnUpload_Click" />
                    </div>
                    <div class="input-group">
                        <asp:Label ID="Span1" runat="server"></asp:Label>
                    </div>
                    <div class="input-group">
                        <input type="button" id="btnHideNew" class="btn btn-default pull-right" value="x" />
                    </div>
                </div>
            </form>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3  ">
                </div>
                <div class="col-xs-9 s">
                    <div class="row docchildClass">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row" style="overflow-y:auto;">
                <div class="col-xs-3 ">
                    <table id="tblgallery" class="table table-condensed" >
                        <tbody id="tbodyFolderContainer">
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-9 links">
                    <div class="row links">
                    </div>
                </div>
            </div>
        </div>
        <div id="blueimp-gallery" class="blueimp-gallery">
            <!-- The container for the modal slides -->
            <div class="slides"></div>
            <!-- Controls for the borderless lightbox -->
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
            <!-- The modal dialog, which will be used to wrap the lightbox content -->
            <div class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body next"></div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default pull-left prev">
                                <i class="glyphicon glyphicon-chevron-left"></i>
                                Previous
                       
                            </button>

                            <button type="button" class="btn btn-primary next">
                                Next
                           
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none;">
            <table>
                <tr class="warning trFolder">
                    <td>
                        <table>
                            <tr>
                                <img src="/Images/folderIm.png" alt="Image Folder" class="folderImage" style="width: 100px; height: 100px;">
                            </tr>
                            <tr>
                                <td>
                                    <span name="" style="color: black;" class="folderName"></span>
                                </td>
                                <td>
                                    <input type="text" class="input-group txtFolderName" />
                                </td>
                                <td>
                                    <span>&nbsp</span>
                                </td>
                                <td>
                                    <button type="button" style="display: none;" name="123" class="btn btn-xs btn-warning glyphicon glyphicon-pencil cmdEditFolderName"></button>
                                </td>
                                <td>
                                    <span>&nbsp</span>
                                </td>
                                <td>
                                    <button type="button" cmdtype="cmdFolderDelet" name="456" class="btn btn-xs btn-danger glyphicon glyphicon-trash cmdFolderDelete"></button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <script src="js/gallery/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/gallery/jquery.blueimp-gallery.min.js"></script>
    <script src="js/gallery/bootstrap-image-gallery.js"></script>
    <script src="js/gallery/profilepic.js"></script>

</body>
</html>

