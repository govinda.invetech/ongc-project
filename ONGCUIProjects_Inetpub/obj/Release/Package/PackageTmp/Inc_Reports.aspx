﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inc_Reports.aspx.cs" Inherits="ONGCUIProjects.Inc_Reports" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtdatefrom").datepicker();
            $("#txtdateto").datepicker();
            var windowheight = $(window).height() - 125;
            $("#divTableData").height(windowheight);


            $("a.cmdDeleteRecord").click(function () {
                var CurrentTR = $(this).closest('tr');
                var IncidentId = $(this).attr('recordNumber');
                if (confirm("Are you sure to delete this Incident / Near Miss?")) {
                    $.ajax({
                        type: "POST",
                        url: "Incident/service/DeleteIncident.aspx",
                        data: "IncidentID=" + IncidentId,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                CurrentTR.remove();
                                alert(msg_arr[2]);
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });
        });
    </script>
    <style type="text/css">
        th
        {
            background-color: Silver;
        }
    </style>
</head>
<body bgcolor="#BCC7D8">
    <form id="form1" runat="server" class="MainInterface-iframe">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid #009f3c;">
            <h1 class="heading" style="width: auto;" id="txtHeading">
                Incident Report</h1>
            <asp:Label ID="Label2" runat="server" class="heading-content-right" Style="color: Red;"></asp:Label>
        </div>
        <div class="div-pagebottom">
        </div>
        <div class="div-fullwidth marginbottom">
            <asp:Panel ID="pnlDateWise" runat="server" CssClass="div-fullwidth" Style="width: auto;">
                <div style="position: relative; float: left; margin-left: 1px; width: auto; top: 0px;
                    left: 0px; width:300px;">
                    <asp:Label ID="Label4" runat="server" Text="From Date" CssClass="content"></asp:Label>
                    &nbsp;
                    <asp:TextBox ID="txtdatefrom" runat="server" placeholder="From Date" Text=""></asp:TextBox>
                </div>
                <div style="position: relative; float: left; margin-left: 1px; width: auto; top: 0px;
                    left: 0px;">
                    <asp:Label ID="Label5" runat="server" Text="To Date" CssClass="content"></asp:Label>
                    &nbsp;
                    <asp:TextBox ID="txtdateto" runat="server" placeholder="To Date" Text=""></asp:TextBox>
                </div>
            </asp:Panel>
            <asp:Button ID="cmdExporttoExcel" runat="server" Text="Export To Excel" Style="position: relative;
                float: right; margin-left: 10px" OnClick="cmdExporttoExcel_Click"
                CssClass="g-button g-button-share" Visible="False" />
                <asp:Button ID="cmdgenerate" runat="server" Text="generate" OnClick="cmdgenerate_Click1"
                CssClass="g-button g-button-submit" />
        </div>
        <div class="div-fullwidth" style="max-height: 1100px; overflow: auto;">
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999"
                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                Height="269px">
                <AlternatingRowStyle BorderColor="#336666" />
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                <RowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <asp:Button ID="cmdgeneratepdf" runat="server" Text="Generate PDF" Style="position: relative;
            float: right; margin-right: 10px" OnClick="cmdgeneratepdf_Click" Visible="False" />
        <%--<asp:Button ID="cmdExporttoExcel" runat="server" Text="Export To Excel" Style="position: relative;
            float: right; margin-right: 10px" OnClick="cmdExporttoExcel_Click" Visible="False" />--%>
        <div runat="server" id="divTableData" class="div-fullwidth" style="overflow: auto;">
        </div>
   </div>
    </form>
</body>
</html>
