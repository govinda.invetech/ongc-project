﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gate_Pass_History.aspx.cs" Inherits="ONGCUIProjects.Gate_Pass_History" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
    <script type="text/javascript" src="js/jquery.multiselect.js"></script>
    <script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.tftable').on('click', '.cmdedit', function (event) {
                var index = $(this).attr('index');
                var groupid = $(this).attr('groupid');
                var cpfNo = $(this).attr('cpfNo');


                if (confirm("Are you sure you want to approve ? ")) {
                    var data = '{INDEX: "' + index + '",GROUPID: "' + groupid + '",CPF_NO: "' + cpfNo + '"}';

                    $.ajax({
                        type: "POST",
                        url: "Gate_Pass_History.aspx/UpdateStatus",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var result = jQuery.parseJSON(response.d);
                            if (result.STATUS == "TRUE") {
                                location.reload(true);/*--for Reload page--*/
                                alert(result.MESSAGE);
                            } else {
                                alert(result.MESSAGE);
                            }
                        }
                    });
                }

            });


        });
    </script>
</head>
<body>
    <form id="form1" runat="server">

        <div class="div-fullwidth iframeMainDiv" style="width: 98%;">

            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading-GatePass">Gate Pass History</h1>
                <a href="Gate_Pass_Requisition.aspx" class="g-button g-button-red" type="button" style="position: relative; float: right; text-decoration: none; margin-top: 6px; margin-right: 10px;">REQUEST NEW GATE PASS</a>
            </div>

            <div class="div-pagebottom"></div>
            <div class="div-fullwidth marginbottom">
                <div class="div-halfwidth">
                    <asp:Label ID="Label1" runat="server" Text="Select Type" class="gatepass-content"
                        Style="width: 125px;"></asp:Label>
                    <asp:DropDownList ID="ddlType" runat="server" Style="position: relative; float: left; width: 200px;" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                        <asp:ListItem Text="ALL" Value="ALL" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="P" Text="Approved"></asp:ListItem>
                        <asp:ListItem Value="A" Text="Pending"></asp:ListItem>
                        <asp:ListItem Value="R" Text="Rejected"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div id="div_gate_pass_details" runat="server">
            </div>
        </div>
    </form>
</body>
</html>
