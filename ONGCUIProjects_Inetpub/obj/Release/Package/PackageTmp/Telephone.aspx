﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Telephone.aspx.cs" Inherits="ONGCUIProjects.Telephone1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />

      <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />
    <style type="text/css">
        .Label{
            text-decoration:none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
             <div class="header">
            <div class="logo-container">
                <div class="logo">
                </div>
                <div class="logo-text">
                    <h1 class="logo-text-line2">Oil and Natural Gas Corporation Limited
                    </h1>
                    <h1 class="logo-text-line3">URAN PLANT, MUMBAI
                    </h1>
                </div>
             
           
           
        </div>
    
            <div class="logged-user-details">
              
                    
                    <span runat="server" id="NoOfUser" class="TotalRegisteredCount"></span>
                <div style="position: relative; float: right;">
                    <a runat="server" href="javascript:void(0)" class="buttonsNotification" id="cmdNewNotifications">
                        <p class="notificationsCircle" style="float: left;">
                            <span></span>
                        </p>
                    </a>
                    <div class="notification-box" id="notificationContainer">
                        <div class="jewelBeeperHeader">
                            <div class="beeperNubWrapper">
                                <div class="beeperNub">
                                </div>
                            </div>
                        </div>
                        <div class="notification_content">
                            <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                                <div class="clearfix uiHeaderTop">
                                    <div>
                                        <h3 runat="server" id="notification_heading" class="uiHeaderTitle"></h3>
                                </div>
                            </div>
                            <div runat="server" id="div_notification" class="uiScrollableArea fade uiScrollableAreaWithShadow"
                                style="width: 330px; height: 100%;">
                                <div class="uiScrollableAreaWrap scrollable" tabindex="0">
                                    <div class="uiScrollableAreaBody" style="width: 330px;">
                                        <div id="ul_notification" class="uiScrollableAreaContent">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Ends----------------------------------------------------------------------------->
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">Telephone Directories</h1>
            <%-- <a href="TransportBookingHistory.aspx" id="add_new_transport_Request" style="float: right; margin-right: 10px; margin-top: 5px;"
                    class="g-button g-button-red">View Bookings</a>--%>
        </div>
        <div class="div-pagebottom">
        </div>

    <div>
    <center>
        <fieldset>
            <table style="width:300px;">
                <tr style="background-color:palegreen; width:200px;height:60px">
                    <td>
                    <a href="uploadedlink/1456/TELEDIR_URAN_2018.pdf" class="Label" target="_blank"><asp:Label ID="lbl1" runat="server"  Text="Uran Telephone Directory" Font-Size="X-Large" ForeColor="Blue"></asp:Label></a>
                    </td>
                </tr>
                <tr><td>   </td></tr>
                 <tr style="background-color:palegreen; width:200px;height:60px">
                    <td>
                    <a href="uploadedlink/934/Teldir-Trombay-.pdf" class="Label" target="_blank"><asp:Label ID="Label1" runat="server"  Text="Trombay Telephone Directory" Font-Size="X-Large" ForeColor="Blue"></asp:Label></a>
                    </td>
                </tr>
            </table>
        </fieldset>
    </center>
    </div>
    </form>
</body>
</html>
