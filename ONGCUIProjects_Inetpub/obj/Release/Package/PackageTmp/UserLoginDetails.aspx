﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserLoginDetails.aspx.cs" Inherits="ONGCUIProjects.UserLoginDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('.cmd_view_details').fancybox({
                modal: true,
                helpers: {
                    title: null
                }
            });

            $("#txtSearchEmployee").keyup(function () {
                var stext = $("#txtSearchEmployee").val().toUpperCase();
                ($("#tfhover").children('tbody').children('tr[forSearch*="."]')).hide();
                if ($("#tfhover").children('tbody').children('tr[forSearch*=' + stext + ']')) {
                    $("#tfhover").children('tbody').children('tr[forSearch*=' + stext + ']').show();
                }
            });

            $('.cmdEditEmpProfile').fancybox({
                modal: true
            });

            $('#cmd_cancel').live("click", function () {
                $.fancybox.close();

            });

            $("#cmdUpdate").live("click", function () {
                var employee_name = $("#txtEmpName").val();
                var mbl_no = $("#txtMblNo").val();
                var cpf_no = $("#txtEmpCPFNo").val();
                var email = $("#txtEmail1").val();
                var add1 = $("#txtAddr1").val();
                var add2 = $("#txtAddr2").val();
                var city = $("#txtCity").val();
                var state = $("#txtState").val();
                var pincode = $("#txtPinCode").val();
                var homephoeno = $("#txtHomeNo").val();
                var mobile_no = $("#txtMblNo").val();
                var department = $("#ddlDepartment2").val();
                var email2 = $("#txtEmail2").val();
                var email3 = $("#txtEmail3").val();
                var dob = $("#txt_dob").val();
                var worklocation = $("#ddlWorkLocation").val();
                var designation = $("#ddlDesignation").val();
                var supervisior = $("#ddlSupervisiorname").val();
                var employee_ex_no = $("#txtemployeeexno").val();
                var designnation = $("#cmbDesg").val();
                var blood_group = $("#cmbBldGrp").val();
                var gender = $("#CmbGender").val();
                if (employee_name == "") {
                    $("#txtEmpName").css('border-color', 'red');
                    return false;
                }
                if (cpf_no == "") {
                    $("#txtEmpCPFNo").css('border-color', 'red');
                    return false;
                }
                if (mbl_no == "") {
                    $("#txtMblNo").css('border-color', 'red');
                    $("#Mbl_Err_Msg").show();
                    return false;
                }
                // For Email
                if (email == "") {
                    $("#txtEmail1").css('border-color', 'red');
                    return false;
                } else {
                    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                    if (emailPattern.test(email) == false) {
                        $("#txtEmail1").css('border-color', 'red');
                        $.gritter.add({
                            title: "Notification",
                            image: "images/exclamation.png",
                            text: "Please Enter A Valid Email"
                        });
                        return false;
                    } else {
                    }
                }

                //for check that mobile no is valid or not
                if (isNaN(mbl_no)) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Mobile No."
                    });
                    return false;

                } else if (mbl_no.length != 10) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Mobile No."
                    });
                    return false;
                }
                if (isNaN(homephoeno)) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Home PHone No."
                    });
                    return false;
                }
                if (isNaN(pincode)) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Pin Code"
                    });
                    return false;
                }
                // for blood group
                blood_group = jQuery.trim(blood_group);
                if (blood_group == "SELECT") {
                    blood_group = "";
                }
                blood_group = encodeURIComponent(blood_group);
                alert("e_name=" + employee_name + "&cpf_no=" + cpf_no + "&add1=" + add1 + "&add2=" + add2 + "&city=" + city + "&state=" + state + "&pin_no=" + pincode + "&home_ph_no=" + homephoeno + "&mbl_no=" + mbl_no + "&gender=" + gender + "&dob=" + dob + "&email1=" + email + "&email2=" + email2 + "&blood_group=" + blood_group + "&department=" + encodeURIComponent(department) + "&worklocation=" + worklocation + "&type=UPDATE&designation=" + encodeURIComponent(designation) + "&supervisior=" + supervisior + "&employee_ex_no=" + employee_ex_no);
                // calling service
                $.ajax({
                    type: "GET",
                    url: "services/Approveuserandupdatedetail.aspx",
                    data: "e_name=" + employee_name + "&cpf_no=" + cpf_no + "&add1=" + add1 + "&add2=" + add2 + "&city=" + city + "&state=" + state + "&pin_no=" + pincode + "&home_ph_no=" + homephoeno + "&mbl_no=" + mbl_no + "&gender=" + gender + "&dob=" + dob + "&email1=" + email + "&email2=" + email2 + "&email3=" + email3 + "&blood_group=" + blood_group + "&department=" + encodeURIComponent(department) + "&worklocation=" + worklocation + "&type=UPDATE&designation=" + encodeURIComponent(designation) + "&supervisior=" + supervisior + "&employee_ex_no=" + employee_ex_no,
                    success: function (msg) {
                        // alert(msg);
                        var values = msg.split("~");
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                        }
                    }
                });
            });

            $('.cmdSetPrivilege').fancybox({
                modal: true,
                padding: 0
            });

            $('#save_privilage').live("click", function () {
                var cpf_no = $(this).attr("CurrentCPFNo");
                var child_ids = '';
                $.each($(".module_list"), function (index, value) {
                    if ($(this).children('input').attr("checked")) {
                        child_ids = child_ids + $(this).children('input').attr('id') + "`";
                    }
                });
                $(this).attr("disabled", true);
                $(this).val('Saving...');
                $.ajax({
                    type: "GET",
                    url: "services/save_pri_to_emp.aspx",
                    data: "cpf_no=" + cpf_no + "&child_ids=" + child_ids,
                    success: function (msg) {
                        var values = msg.split("~");
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $('#save_privilage').attr("disabled", false);
                            $('#save_privilage').val('Save');
                            alert(values[2]);
                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                        }
                    }
                });
            });

            $(".cmdResetPwd").live("click", function () {
                var cpf_no = $(this).attr("CurrentCPFNo");
                if (cpf_no == "") {
                    alert("Please Enter CPF No.");
                } else {
                    if (confirm("Are you sure to reset password of selected user ?")) {
                        $.ajax({
                            type: "GET",
                            url: "services/deleteuserandresetpwd.aspx",
                            data: "cpf_no=" + cpf_no + "&type=RESET_PWD",
                            success: function (msg) {
                                var values = msg.split("~");
                                if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/tick.png",
                                        text: values[2]
                                    });
                                } else {
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/cross.png",
                                        text: values[2]
                                    });
                                }
                            }
                        });
                    }
                }
            });

            $(".cmdAction").click(function () {
                var x = $(this);
                var question = "";
                if ($(this).hasClass('btn-disable-account')) {
                    question = "Are you sure to disable this account?";
                } else if ($(this).hasClass('btn-enable-account')) {
                    question = "Are you sure to enable this account?";
                }
                if (confirm(question)) {
                    var sActionType = $(this).attr('ActionType');
                    var sUser = $(this).attr("user");
                    var currenttr = $(this).parent("td").parent("tr");
                    var currentbutton = $(this);

                    $.ajax({
                        type: "GET",
                        url: "services/AccountAction.aspx",
                        data: "type=" + sActionType + "&user=" + sUser,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                //alert(msg_arr[2]);
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/tick.png",
                                    text: msg_arr[2]

                                });
                                //window.location = self.location;
                                if (msg_arr[2] == "Account Activated Successfully") {
                                    currenttr.css('background-color', 'rgba(52, 187, 0, .85)');
                                    currentbutton.removeClass("btn-enable-account").addClass("btn-disable-account");
                                    currentbutton.attr('actiontype', 'ADMINDEACTIVE');
                                    x.parent('td').html('Accepted');

                                } else if (msg_arr[2] == "Account Rejected Successfully") {
                                    currenttr.css('background-color', 'rgba(255, 60, 60, .85)');
                                    currentbutton.removeClass("btn-disable-account").addClass("btn-enable-account");
                                    currentbutton.attr('actiontype', 'ADMINACTIVE');
                                    x.parent('td').html('Rejected');
                                }
                            } else {
                                alert("Problem in Execution, Please contact Admin...!");
                            }
                        }
                    });
                }

            });

            $('.cmdDeleteUser').click(function () {
                var currenttr1 = $(this).parent("td").parent("tr");
                var cpf_no = $(this).attr("CurrentCPFNo");
                if (cpf_no == "") {
                    alert("Please Enter CPF No.");
                } else {
                    if (confirm("Are you sure to delete the selected user ?")) {
                        $.ajax({
                            type: "GET",
                            url: "services/DeleteUserFromDatabase.aspx",
                            data: "cpfno=" + cpf_no,
                            success: function (msg) {
                                var values = msg.split("~");
                                if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                    currenttr1.remove();
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/tick.png",
                                        text: values[2]
                                    });
                                } else {
                                    alert(values[2]);
                                }
                            }
                        });
                    }
                }
            });

            $('.cmdActiveUser').click(function () {
                var cpf_no = $(this).attr("CurrentCPFNo");
                var currenttr2 = $(this).parent("td").parent("tr");
                var x1 = $(this);
                var currentbutton1 = $(this);
                if (cpf_no == "") {
                    alert("Please Enter CPF No.");
                } else {
                    if (confirm("Are you sure to Deactive the selected user ?")) {
                        $.ajax({
                            type: "GET",
                            url: "services/DeActiveUser.aspx",
                            data: "cpfno=" + cpf_no,
                            success: function (msg) {
                                var values = msg.split("~");
                                if (values[0] == "TRUE" && values[1] == "SUCCESS") {                                   
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/tick.png",
                                        text: values[2]

                                    });

                                    if (values[2] == "Account DEACTIVE Successfully") {                                      
                                        currenttr2.css('background-color', 'rgba(199, 0, 0, 1)');
                                        currentbutton1.removeClass("btn-enable-account");
                                        x1.parent('td').html('DEACTIVE');
                                    }


                                } else {
                                    alert(values[2]);
                                }
                            }
                        });
                    }
                }
            });


          
            ////////---------===========---------------------===================================////////
            $('.cmdDeactiveUser').click(function () {
                var cpf_no = $(this).attr("CurrentCPFNo");
                var currenttr3 = $(this).parent("td").parent("tr");
                var x2 = $(this);
                var currentbutton2 = $(this);
                if (cpf_no == "") {
                    alert("Please Enter CPF No.");
                } else {
                    if (confirm("Are you sure to Active the selected user ?")) {
                        $.ajax({
                            type: "GET",
                            url: "services/ActiveUser.aspx",
                            data: "cpfno=" + cpf_no,
                            success: function (msg) {
                                var values = msg.split("~");
                                if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/tick.png",
                                        text: values[2]

                                    });

                                    if (values[2] == "Account ACTIVE Successfully") {                                      
                                        currenttr3.css('background-color', 'rgba(27, 204, 0, 1)');
                                        currentbutton2.removeClass("btn-disable-account");
                                        x2.parent('td').html('ACTIVE');
                                       
                                    }


                                } else {
                                    alert(values[2]);
                                }
                            }
                        });
                    }
                }
            });

          
            ///////==========------------------------===================================--------///////

            $('#ddlUserType').change(function () {
                window.location = 'UserLoginDetails.aspx?type=' + $(this).val();
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="div-fullwidth iframeMainDiv">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;">Manage Users</h1>

                <input id="txtSearchEmployee" type="text" style="display: block; width: 250px; position: relative; float: right; margin-right: 10px; margin-top: 4px;"
                    placeholder="Search Employee Name or CPF No..." />
                <div style="float: right; margin-top: 3px; margin-right: 10px;">
                    <p class="content" style="width: auto; margin-top: 5px;">
                        Manage Users
                    </p>


                    <asp:DropDownList ID="ddlUserType" runat="server" Width="150px">
                        <asp:ListItem Value="ALL">All</asp:ListItem>
                        <asp:ListItem Value="REJECT">Rejected</asp:ListItem>
                        <asp:ListItem Value="ACTIVE">Active</asp:ListItem>
                        <asp:ListItem Value="REQUEST">Request</asp:ListItem>
                    </asp:DropDownList>

                </div>
            </div>
            <div class="div-pagebottom">
            </div>
            <div runat="server" class="div-fullwidth" id="table_data" style="margin-bottom: 10px; overflow:scroll; height:500px;">
            </div>
        </div>
    </form>
</body>
</html>
