<%@ Page language="c#" Codebehind="Discussion_Forum.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Discussion_Forum" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Discussion_Forum</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P align="center"><FONT face="Verdana" color="brown" size="2"><STRONG><U>Knowledge Base - 
							Discussion Forum</U></STRONG></FONT></P>
			<P>
				<TABLE id="Table2" cellSpacing="8" cellPadding="4" width="100%" align="center" bgColor="whitesmoke"
					border="1" runat="server">
					<TR>
						<TD style="WIDTH: 161px" borderColor="gray" align="center" bgColor="whitesmoke">
							<P align="center"><FONT face="Verdana" size="1"><STRONG><FONT face="Verdana" color="brown" size="1"><STRONG>Enter 
												Your Suggestions Here</STRONG></FONT></STRONG></FONT></P>
						</TD>
						<TD style="WIDTH: 225px" borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1"><STRONG><FONT face="Verdana" color="#660000" size="1">
											<P align="left">
												<asp:textbox id="TxtIncDescr" runat="server" MaxLength="8000" TextMode="MultiLine" CssClass="flattxt"
													Height="88px" BorderStyle="Solid" BorderColor="#E0E0E0" Width="360px" Font-Names="Verdana"
													Font-Size="XX-Small"></asp:textbox></P>
										</FONT></STRONG></FONT></STRONG>
						</TD>
						<TD borderColor="gray" align="center" bgColor="whitesmoke"><STRONG><FONT face="Verdana" color="#660000" size="1">
									<P align="left">
										<asp:button id="BtnEnter" runat="server" BorderStyle="Solid" BorderColor="DarkGray" Width="225px"
											Font-Names="Verdana" Font-Size="XX-Small" Text="Submit Your Suggestions" Font-Bold="True"
											ForeColor="ControlText" BackColor="Silver" onclick="BtnEnter_Click"></asp:button></P>
									<P align="left">
										<asp:button id="BtnRefresh" runat="server" BorderStyle="Solid" BorderColor="DarkGray" Width="225px"
											Font-Names="Verdana" Font-Size="XX-Small" Text="Refresh Page" Font-Bold="True" ForeColor="ControlText"
											BackColor="Silver" CausesValidation="False" onclick="BtnRefresh_Click"></asp:button></P>
								</FONT></STRONG>
						</TD>
					</TR>
				</TABLE>
			</P>
			<P>
				<asp:datagrid id="Dgrd_Suggestion" runat="server" BorderColor="Gainsboro" Width="100%" Font-Names="Verdana"
					Font-Size="XX-Small" ForeColor="Brown" BackColor="WhiteSmoke" ItemStyle-Wrap="True" AutoGenerateColumns="False"
					CellPadding="5" GridLines="Horizontal" HorizontalAlign="Justify" EnableViewState="False">
					<HeaderStyle Font-Size="XX-Small" Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Left"
						VerticalAlign="Middle" BackColor="WhiteSmoke"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="O_LGN_NM" HeaderText="Messages"></asp:BoundColumn>
						<asp:BoundColumn DataField="O_DSCSN_DTL" ReadOnly="True"></asp:BoundColumn>
					</Columns>
				</asp:datagrid></P>
			<P>&nbsp;</P>
			<asp:ValidationSummary id="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
			<asp:RequiredFieldValidator id="Sugg" runat="server" ErrorMessage="You Cannot Click no Submit Without Entering Your Suggestions"
				ControlToValidate="TxtIncDescr" Display="None"></asp:RequiredFieldValidator></form>
	</body>
</HTML>
