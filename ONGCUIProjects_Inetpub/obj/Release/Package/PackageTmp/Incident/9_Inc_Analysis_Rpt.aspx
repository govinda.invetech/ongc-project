﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="9_Inc_Analysis_Rpt.aspx.cs" Inherits="ONGCUIProjects.Incident._9_Inc_Analysis_Rpt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- CSS Links Starts -->
    <link href="../css/styleGlobal.css" rel="stylesheet" type="text/css" />
    <link href="../css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <link rel="stylesheet" href="../js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="../css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="../js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../css/AutoCompleteStyle.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">Incident Analytical Report</h1>
        </div>
        <div class="div-pagebottom"></div>
        
    </div>
    </form>
</body>
</html>
