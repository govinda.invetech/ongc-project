﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="6_Accp_L2.aspx.cs" Inherits="ONGCUIProjects.Incident.fancybox._6_Accp_L2" %>
<script type="text/javascript">
    $("#lblIncdntNo").hide();


    $("#cmdSubmit").click(function () {
        var sIncdntNo = $("#lblIncdntNo").text();
        var sLevelComment = $("#txtLevelComment").val().trim();
        var sDecision = $('input[name="my_hint"]:checked').val();
        if (sDecision == undefined) {
            alert("Please select decision");
            return;
        } else {
            if (sDecision == "CLOSE") {
                if (confirm("Are you sure to close this incident")) {
                    CallAjax();
                }
            } else {
                CallAjax();
            }
        }

        function CallAjax() {
            $.ajax({
                type: "POST",
                url: "service/6_Accp_L2Save.aspx",
                data: "IncdntNo=" + sIncdntNo + "&LevelComment=" + sLevelComment + "&Decision=" + sDecision,
                success: function (msg) {
                    var msg_arr = msg.split("~");
                    if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                        $("#response").text(msg_arr[2]);
                        $("#cmdSubmit").hide();
                    } else {
                        alert(msg_arr[2]);
                    }
                }
            });
        }
    });

</script>
<form id="form1" runat="server">
    <asp:label id="lblIncdntNo" runat="server" text="Label"></asp:label>
    <div style="position:relative;float:left; width:588px;">
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;">View Action Details</h1>
        <div class="div-fullwidth marginbottom">
            <p class="content">Incident Description</p>
            <asp:TextBox ID="txtIncdntDesc" runat="server" TextMode="MultiLine" ReadOnly="True" style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;"></asp:TextBox>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">Incident Action Taken</p>
            <asp:TextBox ID="txtIncdntActionTaken" runat="server" TextMode="MultiLine" ReadOnly="True" style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;"></asp:TextBox>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">Incident Reviewer Remark</p>
            <asp:TextBox ID="txtIncdntRevRem" runat="server" TextMode="MultiLine" ReadOnly="True" style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;"></asp:TextBox>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">Comments</p>
            <asp:TextBox ID="txtLevelComment" runat="server" TextMode="MultiLine" style="min-width: 100%; max-width: 100%; min-height:40px; max-height:40px;"></asp:TextBox>
        </div>
        <div style="position: relative;float: left;top: 5px;left: 0px;padding: 2px 0px;width: 99.7%;background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom:20px;">
                <h1 class="content" style="margin-left:10px;">Select Decision : </h1>
                <input type="radio" name="my_hint" id="rev" value="REVISE" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;" /><label for="rev" class="content" style="cursor: pointer;">Revise Action</label>
                <input type="radio" name="my_hint" id="close" value="CLOSE" style="float: left;margin: 6px;border: 3px solid #3E92A7;height: 19px;width: 19px;"/><label for="close" class="content" style="cursor: pointer;">Close</label>
            </div>
        <h1 class="heading" style="padding:0; border-bottom: 2px solid #58595A; margin-bottom: 10px;"></h1>
        <div class="div-fullwidth">
            <p id="response" class="content" style="width: auto; color:Red;"></p>
            <input type="button" id="cmdReset" class="g-button g-button-red" value="Close" onclick="window.location=self.location; $.fancybox.close();" style="float:right;" />
            <input type="button" id="cmdSubmit" class="g-button g-button-submit" value="Save" style="margin-right:10px; margin-bottom:0px"/>
        </div>
    </div>
</form>
