﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncidentDetails.aspx.cs"
    Inherits="ONGCUIProjects.Incident.fancybox.IncidentDetails" %>

<script type="text/javascript">

    $("#lblRushToHospital").hide();
    $("#lblIncidentID").hide();
    $("#lblIncdntType").hide();
    if ($("#lblIncdntType"))
        var optrushtohospital = $("#lblRushToHospital").text();
    $('input[name="rushedtohospital"][value="' + optrushtohospital + '"]').prop('checked', true);
    var level = parseInt($("#HiddenField1").val() - 1);

    $("#tabs").tabs({
        selected: level
    });

    if ($("#lblIncdntType").text() == "NEARMISS") {
        $(".ForIncident").remove();
    }

    
</script>
<style type="text/css">
    .label-heading
    {
        width: 135px;
    }
    .ui-tabs .ui-tabs-nav li a
    {
        padding: .5em .5em;
    }
    .ui-tabs
    {
        float: left;
    }
    .marginLeft
    {
        margin-left: 20px;
    }
</style>
<form id="form1" runat="server">
<asp:hiddenfield id="HiddenField1" runat="server" />
<asp:label id="lblIncdntType" runat="server" text="Label"></asp:label>
<asp:label id="lblRushToHospital" runat="server" text="Label"></asp:label>
<asp:label id="lblIncidentID" runat="server" text="Label"></asp:label>
<div id="tabs" style='width: 810px;'>
    <ul>
        <li><a href="#tabs-1">Reporter Details</a></li>
        <li><a href="#tabs-2">Reviewer Details</a></li>
        <li><a href="#tabs-3">Incident Manager</a></li>
        <li><a href="#tabs-4">FPR Details</a></li>
        <li><a href="#tabs-5">Acceptance Level 1</a></li>
        <li><a href="#tabs-6">Acceptance Level 2</a></li>
    </ul>
    <div id="tabs-1" style="position: relative; float: left; padding: .5em;">
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Reporter Name</p>
            <asp:textbox class="CheckValidation" id="txtRptEngCPF" runat="server" readonly="True"
                enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto; margin: 0 4px;">
            <p class="content">
                CPF No</p>
            <asp:textbox class="CheckValidation" id="txtRptEngName" runat="server" readonly="True"
                enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth" style="width: auto;">
            <p class="content">
                Desig</p>
            <asp:textbox id="txtRptEngDesig" runat="server" readonly="True" enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">
                Rushed to Hospital</p>
            <input style="position: relative; float: left; margin: 6px 5px 0 0;" id="Radio1"
                type="radio" name="rushedtohospital" value="Y" checked disabled="disabled" />
            <label class="content" for="rushedtohospital_yes">
                Yes</label>
            <input style="position: relative; float: left; margin: 6px 5px 0 0;" id="Radio2"
                type="radio" name="rushedtohospital" value="N" disabled="disabled" />
            <label class="content" for="rushedtohospital_no">
                No</label>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth" style="width: auto;">
                <p class="content">
                    Incident Date</p>
                <asp:textbox class="CheckValidation" id="txtIncdntDate" runat="server" enabled="False"></asp:textbox>
            </div>
            <div class="div-halfwidth" style="width: auto;">
                <p class="content">
                    Time(24-hour)</p>
                <asp:textbox class="CheckValidation" id="txtIncdntTime" runat="server" enabled="False"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-fullwidth" style="width: auto;">
                <p class="content">
                    Area</p>
                <asp:textbox class="CheckValidation" id="txtIncdntArea" runat="server" enabled="False"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: auto; margin: 0 14px;">
                <p class="content">
                    Location</p>
                <asp:textbox class="CheckValidation" id="txtIncdntLocation" runat="server" enabled="False"></asp:textbox>
            </div>
            <div class="div-fullwidth" style="width: auto;">
                <p class="content">
                    Department</p>
                <asp:textbox class="CheckValidation" id="txtIncdntDept" runat="server" enabled="False"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom ForIncident">
            <p class="content" style="width: 100%;">
                Type of Damage</p>
            <div class="div-halfwidth">
                <p class="content">
                    Property</p>
                <asp:textbox id="txtProperty" runat="server" textmode="MultiLine" style="max-height: 40px;
                    max-width: 98%; min-height: 40px; min-width: 98%" enabled="False"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <p class="content">
                    Process</p>
                <asp:textbox id="txtProcess" runat="server" textmode="MultiLine" style="max-height: 40px;
                    max-width: 100%; min-height: 40px; min-width: 100%" enabled="False"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <p class="content">
                    Environment</p>
                <asp:textbox id="txtEnvironment" runat="server" textmode="MultiLine" style="max-height: 40px;
                    max-width: 98%; min-height: 40px; min-width: 98%" enabled="False"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <p class="content">
                    Personal Injury</p>
                <asp:textbox id="txtPersonalInjury" runat="server" textmode="MultiLine" style="max-height: 40px;
                    max-width: 100%; min-height: 40px; min-width: 100%" enabled="False"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <p class="content">
                Brief Description of the Accident</p>
            <asp:textbox id="txtIncdntDesc" runat="server" textmode="MultiLine" style="min-width: 520px;
                min-height: 40px; max-width: 520px; max-height: 40px; float: right;" enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth marginbottom  ForIncident">
            <p class="content">
                Apparent Cause of the Accident</p>
            <asp:textbox id="txtIncdntAppCause" runat="server" textmode="MultiLine" style="min-width: 520px;
                min-height: 40px; max-width: 520px; max-height: 40px; float: right;" enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth marginbottom  ForIncident">
            <p class="content">
                Immediate Remedial Action</p>
            <asp:textbox id="txtIncdntImmdRemedial" runat="server" textmode="MultiLine" style="min-width: 520px;
                min-height: 40px; max-width: 520px; max-height: 40px; float: right;" enabled="False"></asp:textbox>
        </div>
        <div id="divInjuredPersonDetail" class="div-fullwidth marginbottom ForIncident">
        </div>
    </div>
    <div id="tabs-2" style="position: relative; float: left; padding: .5em;">
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth" runat="server" id="divTypeList">
                <p class="content">
                    Type</p>
                <asp:textbox class="CheckValidation" id="txtIncidentType" runat="server" enabled="False"></asp:textbox>
            </div>
            <div class="div-halfwidth" runat="server" id="divCategoryList">
                <p class="content">
                    Area</p>
                <asp:textbox class="CheckValidation" id="txtIncidentCategory" runat="server" enabled="False"></asp:textbox>
            </div>
            <div class="div-halfwidth" runat="server" id="divEmpList">
                <p class="content">
                    Area</p>
                <asp:textbox class="CheckValidation" id="txtIncidentManager" runat="server" enabled="False"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom" runat="server" id="divSafety">
            <p class="content" style="width: 115px;">
                Safety</p>
            <asp:textbox id="txtSafety" runat="server" textmode="MultiLine" style="float: right;
                margin-right: 10px; width: 60%;" enabled="False"></asp:textbox>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content" style="width: 115px;">
                    Recommendation</p>
                <asp:textbox id="txtReccomendations" runat="server" textmode="MultiLine" style="float: right;
                    margin-right: 10px; width: 60%;" enabled="False"></asp:textbox>
            </div>
            <div class="div-halfwidth">
                <p class="content" style="width: 150px;">
                    Prima Facie Reason</p>
                <asp:textbox id="txtPrimaFacieReason" runat="server" textmode="MultiLine" style="width: 58.5%;"
                    enabled="False"></asp:textbox>
            </div>
        </div>
        <div class="div-halfwidth marginbottom">
            <p class="content" style="width: 115px;">
                Remarks</p>
            <asp:textbox id="txtRemarks" runat="server" textmode="MultiLine" style="width: 66%;"
                enabled="False"></asp:textbox>
        </div>
    </div>
    <div id="tabs-3" style="padding: .5em;">
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content label-heading">
                    Area</p>
                <asp:textbox class="CheckValidation" id="Tab3txtIncdntArea" runat="server" enabled="False" style="width:245px;"></asp:textbox>
            </div>
            <div style="margin-left: 0px;" class="div-halfwidth">
                <p style="" class="content label-heading">
                    Location</p>
                <asp:textbox class="CheckValidation" id="Tab3txtIncdntLocation" runat="server" enabled="False" style="width:245px;"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content label-heading">
                    Area Manager</p>
                <asp:textbox class="CheckValidation" id="Tab3txtAreaMgr" runat="server" enabled="False" style="width:245px;"></asp:textbox>
            </div>
            <div style="margin-left: 0px;" class="div-halfwidth">
                <p style="" class="content label-heading">
                    Designation</p>
                <asp:textbox class="CheckValidation" id="Tab3txtMGRDesig" runat="server" enabled="False" style="width:245px;"></asp:textbox>
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content label-heading">
                    Action to be Taken</p>
                <asp:textbox class="CheckValidation" id="txtActionToBeTaken" runat="server" enabled="False"
                    style="width: 245px" textmode="MultiLine" />
            </div>
            <div style="margin-left: 0px;" class="div-halfwidth">
                <p style="" class="content label-heading">
                    Comments</p>
                <asp:textbox class="CheckValidation" id="txtIncdntMgrComment" runat="server" enabled="False"
                    style="width: 245px" textmode="MultiLine" />
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content label-heading">
                    FPR CPF No & Name</p>
                <asp:textbox class="CheckValidation" id="txtFPRName" runat="server" enabled="False" style="width: 245px;" />
            </div>
            <div style="margin-left: 0px;" class="div-halfwidth">
                <p style="" class="content label-heading">
                    Designation</p>
                <asp:textbox class="CheckValidation" id="txtFPRDesig" runat="server" enabled="False" style="width: 245px;" />
            </div>
        </div>
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content label-heading">
                    Target Date</p>
                <asp:textbox class="CheckValidation" id="txtTargetDate" runat="server" enabled="False"
                    style="width: 245px;" />
            </div>
        </div>
        
        <div runat="server" id="divTableData" class="div-fullwidth">
        </div>
    </div>
    <div id="tabs-4" style="position: relative; float: left; padding: .5em; width: 98%">
        <div runat="server" id="div1" class="div-fullwidth">
        </div>
    </div>
    <div id="tabs-5" style="padding: .5em;">
        <div class="div-fullwidth marginbottom">
            <div class="div-halfwidth">
                <p class="content label-heading">
                    FPR CPF No & Name</p>
                    <asp:textbox class="CheckValidation" id="Tab5txtFPRName" runat="server" enabled="False"
                    style="width: 245px;" />
                
            </div>
            <div style="margin-left: 0px;" class="div-halfwidth">
                <p style="" class="content label-heading">
                    Designation</p>
                    <asp:textbox class="CheckValidation" id="Tab5txtFPRDesig" runat="server" enabled="False"
                    style="width: 245px;" />
                
            </div>
        </div>
        <div runat="server" id="div2" class="div-fullwidth marginbottom">
        </div>
    </div>
    <div id="tabs-6" style="position: relative; float: left; padding: .5em;">
        <div runat="server" id="div3" class="div-fullwidth">
        </div>
    </div>
</div>
</form>
