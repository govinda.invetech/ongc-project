﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="complaintReg1.aspx.cs" Inherits="ONGCUIProjects.complaintReg1" %>

<!DOCTYPE html>
<html>
<head>
    <title>VisitorPass_Approval1</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script src="js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script src="js/jquery.callout.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/our.js"></script>
    <link rel="stylesheet" href="css/diggy.css?ver=4.5.3.3" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/jquery.callout.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="style/privilage.css" />
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/wave.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />


    <script type="text/javascript" language="javascript">
        $(document).ready(function () {


            //		        		        $(document).ajaxStop($.unblockUI);
            //		        		        getstrip("PENDING");
            //		        		        $("#ddlstrip_type").change(function () {
            //		        		            var my_abc = $(this).val();
            //		        		            getstrip(my_abc);
            //		        		        });

            //		        		        //function 
            //		        		        function getstrip(strip_type) {
            //		        		            $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
            //		        		            $.ajax({
            //		        		                type: "GET",
            //		        		                url: "services/complain/getusercomlainlist.aspx",
            //		        		                data: "strip_type=" + strip_type,
            //		        		                success: function (msg) {
            //		        		                    //alert(msg);
            //		        		                    var values = msg.split("~");

            //		        		                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
            //		        		                        $("#request_detail_div").html(values[3]);
            //		        		                        $(".view_cmpln_dtl_fancy").fancybox({
            //		                                        modal:true
            //		                                        });
            //		        		                        $("#theory_text").text(values[2]);
            //		        		                    } else if (values[0] == "FALSE" && values[1] == "ERR") {

            //		        		                        $.gritter.add({
            //		        		                            title: "Notification",
            //		        		                            image: "images/cross.png",
            //		        		                            text: values[2]
            //		        		                        });
            //		        		                        $("#request_detail_div").empty();
            //		        		                        $("#theory_text").text(values[2]);
            //		        		                    }

            //		        		                }
            //		        		            });
            //		        		        }
            //		        		        //end function


        });
    </script>
      <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
         <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtdatefrom").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            $("#txtdateto").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            var DivWindowHeight = parseInt($(window).height()) - 104;
            $('#report_container').css('max-height', DivWindowHeight + 'px');
        });
    </script>
</head>

<body>
    <form id="Form1" method="post" runat="server" style="margin-right: 10px;">
        <div class="div-fullwidth iframeMainDiv" style="width: 99%;">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading" style="width: auto;">Complaint Registration</h1>

                <a href="Complaint_Registration.aspx?my_hint=NEW`NEW" id="register_new_complaint" class="g-button g-button-red" type="button" style="float: right; margin-top: 5px; margin-right: 5px; text-decoration: none; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;">Register New Complaint</a>
            </div>
            <div class="div-pagebottom"></div>
            <%--
            <h1 class="gatepass-content" style="margin-top: 2px;">Select  Type</h1>
            <asp:DropDownList ID="ddlstrip_type" runat="server" Width="130px">
                 
                 <asp:ListItem Selected="True">PENDING</asp:ListItem>
                 <asp:ListItem>SOLVED</asp:ListItem>                 
            </asp:DropDownList>
            --%>
            <p id="P1" class="gatepass-content" style="font-size: 12px; font-family: Arial; margin-top: 10px; display: none"></p>
            <!-- Strip Container Starts ------------------------------------------------------------------------------------------->
          
           



           
             <!-- Strip Container Ends --------------------------------------------------------------------------------------------->
         <asp:Label ID="lbl1" runat="server" Text="Select Status Type"></asp:Label>&nbsp;&nbsp;&nbsp;
          <asp:DropDownList ID="ddl_status" AutoPostBack="true" runat="server" Width="160px" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged">
              <asp:ListItem Selected="True" Value="ALL">ALL</asp:ListItem>
              <asp:ListItem Value="NEW">NEW</asp:ListItem>
                <asp:ListItem Value="ASSIGN">ASSIGN</asp:ListItem>
                 <asp:ListItem Value="WORK IN PROGRESS">WORK IN PROGRESS</asp:ListItem> 
                 <asp:ListItem Value="RESOLVED">RESOLVED</asp:ListItem>     
          </asp:DropDownList>&nbsp;&nbsp;
              <asp:Label ID="eto" runat="server" Text="From Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdatefrom" runat="server" Width="90px" Height="26px"></asp:TextBox>&nbsp;&nbsp;
             <asp:Label ID="Label1" runat="server" Text="To Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdateto" runat="server" Width="90px" Height="26px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
             <asp:Button ID="cmdgenerates" runat="server" Text="generate" OnClick="cmdgenerates_Click" 
                     />

                 <div class="div-fullwidth" runat="server" style="margin-top: 10px; overflow:scroll; margin-left:10px; height:300px;">
               
               <asp:GridView ID="GridView_manager" runat="server"
                    AutoGenerateColumns="False"
                    OnRowDataBound="GridView_manager_RowDataBound" BackColor="LightGoldenrodYellow"
                    BorderColor="Tan" BorderWidth="1px" CellPadding="2"
                    GridLines="None" Font-Names="Calibri" Font-Size="Medium" OnRowCancelingEdit="GridView_manager_RowCancelingEdit" OnRowEditing="GridView_manager_RowEditing" OnRowUpdating="GridView_manager_RowUpdating" Width="1200px" ForeColor="Black">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:TemplateField HeaderText="S No." Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId_m" runat="server" Text='<%#Eval("ID")%>' CssClass="" ToolTip="S No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applicant">
                            <ItemTemplate>
                                <div style="width:140px;"><asp:Label ID="Label2" runat="server" Text='<%#Eval("APP_NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation ">
                            <ItemTemplate>
                                <div><asp:Label ID="Label3" runat="server" Text='<%#Eval("APP_DESIG")%>' CssClass="" ToolTip="Designation"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone Extension No.">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("APP_PHONE_EX_NO")%>' CssClass="" ToolTip="Phone Extension No."></asp:Label>
                            </ItemTemplate>
                            <%--    SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[] ,[APP_MOBILE_NO] ,[],[],[COMPLAIN_TYPE_ID] ,[] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[] ,[] --%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <div><asp:Label ID="Label5" runat="server" Text='<%#Eval("APP_LOCATION")%>' CssClass="" ToolTip="Location"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Problem">
                            <ItemTemplate>
                                <div><asp:Label ID="Label6" runat="server" Text='<%#Eval("PROBLEM_DISCRIPTION")%>' CssClass="" ToolTip="Problem"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room No.">
                            <ItemTemplate>
                                <div><asp:Label ID="Label7" runat="server" Text='<%#Eval("ROOM_NO")%>' CssClass="" ToolTip="Room No."></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Type">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("COMPLAIN_DEPARTMENT")%>' CssClass="" ToolTip="Complain Type"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Date">
                            <ItemTemplate>
                                <div style="width:100px;"><asp:Label ID="Label9" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd-MM-yyyy}")%>' CssClass="" ToolTip="Complain Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ACTION">
                            <ItemTemplate>
                                <asp:Label ID="lblComplaintStatus" runat="server" Visible="false" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status"></asp:Label>
                                 <asp:DropDownList ID="ddl_status1" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddl_status1_SelectedIndexChanged"></asp:DropDownList>
                            </ItemTemplate>
                           <%-- <EditItemTemplate>--%>
                                 <%--<asp:Label ID="lblComplaintStatus" runat="server" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status" Visible="false"></asp:Label>
                                <asp:DropDownList ID="ddl_status1" runat="server"></asp:DropDownList>--%>
                           <%-- </EditItemTemplate>--%>
                        </asp:TemplateField>
                       <%--  <asp:CommandField ShowEditButton="true" HeaderText="ACTION" />--%>

                    </Columns>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"
                        BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <RowStyle Wrap="True" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>

            </div>





           



        </div>
    </form>
</body>
</html>

