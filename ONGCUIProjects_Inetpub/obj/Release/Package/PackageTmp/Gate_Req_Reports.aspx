<%@ Page Language="c#" CodeBehind="Gate_Req_Reports.aspx.cs" AutoEventWireup="True"
    Inherits="ONGCUIProjects.Gate_Req_Reports" ValidateRequest="false" %>

<!DOCTYPE html>
<html>
<head id="Head1">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtdatefrom").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            $("#txtdateto").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            var DivWindowHeight = parseInt($(window).height()) - 104;
            $('#report_container').css('max-height', DivWindowHeight + 'px');
        });
    </script>
    <style type="text/css">
        th
        {
            background-color: Silver;
        }
    </style>
</head>
<body bgcolor="#BCC7D8">
    <form id="form1" runat="server" class="MainInterface-iframe">
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid #009f3c;">
            <h1 class="heading" style="width: auto;" id="txtHeading">
                Gate Pass Management Report</h1>
            <asp:Label ID="Label2" runat="server" class="heading-content-right" Style="color: Red;"></asp:Label>
        </div>
        <div class="div-pagebottom">
        </div>
        <div class="div-fullwidth marginbottom">
            
                <asp:Label ID="Label3" runat="server" Text="Report Type" class=" content"></asp:Label>
                <asp:DropDownList ID="ddltypeofreport" runat="server" CssClass="div-fullwidth" 
                    Style="width: auto; margin-right:10px;" AutoPostBack="True"
                    OnSelectedIndexChanged="ddltypeofreport_SelectedIndexChanged" >
                    <asp:ListItem Value="">Select Type</asp:ListItem>
                    <asp:ListItem Value="DATEWISE">Date wise</asp:ListItem>
                    <asp:ListItem Value="REQTYPE">Requisition Type Wise</asp:ListItem>
                    <asp:ListItem Value="AREAWISE">Entry Area Wise</asp:ListItem>
                    <asp:ListItem Value="OFFICERWISE">Applicant Wise</asp:ListItem>
                </asp:DropDownList>
                <asp:Panel ID="pnlArea" runat="server" CssClass="div-fullwidth" style="width:auto;" Visible="False">
                    <asp:Label ID="Label4" runat="server" Text="Area" class=" content"></asp:Label>
                    <asp:DropDownList ID="ddlarea" runat="server" Style="width: auto;">
                        <asp:ListItem Value="">Select Entry Area</asp:ListItem>
                        <asp:ListItem Value="Admin and White Area">Admin and White Area</asp:ListItem>
                        <asp:ListItem Value="White Area">White Area</asp:ListItem>
                        <asp:ListItem Value="Stores Complex">Stores Complex</asp:ListItem>
                        <asp:ListItem Value="Trombay Terminal">Trombay Terminal</asp:ListItem>
                        <asp:ListItem Value="Admin Area">Admin Area</asp:ListItem>
                        <asp:ListItem Value="Operational Area">Operational Area</asp:ListItem>
                        <asp:ListItem Value="All Area">All Area</asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnlReqType" runat="server" CssClass="div-fullwidth" style="width:auto;" Visible="False">
                    <asp:Label ID="Label5" runat="server" Text="Requisition Type" class=" content"></asp:Label>
                    <asp:DropDownList ID="ddlrequtiontype" runat="server" Style="width: auto;">
                        <asp:ListItem Value="">Select Requisition Type</asp:ListItem>
                        <asp:ListItem Value="Entry On Week Days">Entry On Week Days</asp:ListItem>
                        <asp:ListItem Value="Entry On Holidays">Entry On Holidays</asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnlDateWise" runat="server" CssClass="div-fullwidth" style="width:auto;" Visible="False">
                    <div style="position: relative; float: left; margin-left: 1px; width: auto; top: 0px;
                        left: 0px;">
                        <asp:Label ID="eto" runat="server" Text="From Date" CssClass="content"></asp:Label>
                        &nbsp;
                        <asp:TextBox ID="txtdatefrom" runat="server" Width="100px" placeholder="From Date"
                            Text=""></asp:TextBox>
                    </div>
                    <div style="position: relative; float: left; margin-left: 1px; width: auto; top: 0px;
                        left: 0px;">
                        <asp:Label ID="Label1" runat="server" Text="To Date" CssClass="content"></asp:Label>
                        &nbsp;
                        <asp:TextBox ID="txtdateto" runat="server" Width="100px" placeholder="To Date" Text=""></asp:TextBox>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEmployee" runat="server" CssClass="div-fullwidth" style="width:auto;" Visible="False">
                    <asp:DropDownList ID="ddlEmployee" runat="server">
                    </asp:DropDownList>
                </asp:Panel>
            
            
            <asp:Button ID="cmdExporttoExcel" runat="server" Text="Export To Excel" Style="position: relative;
                float: right; margin-left: 10px" OnClick="cmdExporttoExcel_Click" Visible="False"
                CssClass="g-button g-button-submit" />
                <asp:Button ID="cmdgenerate" runat="server" Text="generate" OnClick="cmdgenerate_Click1"
                CssClass="g-button g-button-submit" Visible="False" />
        </div>
        <div runat="server" id="report_container" class="div-fullwidth" style="overflow: auto;">
            <asp:GridView ID="GridView1" runat="server" CssClass="IncTable">
            </asp:GridView>
        </div>
    </div>
    </form>
</body>
</html>
