﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransportBookingAddEdit.aspx.cs"
    Inherits="ONGCUIProjects.TransportBookingAddEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- CSS Links Starts -->
    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen">
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen">
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css">
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <!-- JS Links Starts -->
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).ajaxStop($.unblockUI);

            var sActionType = $("#txtHideActionType").val();
            var sBookingID = $("#txtHideBookingID").val();

            $(function () {
                var startDateTextBox = $('#txtbookingstartdatetime');
                var endDateTextBox = $('#txtbookingEnddatetime');

                startDateTextBox.datetimepicker({
                    onClose: function (dateText, inst) {
                        if (endDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datetimepicker('getDate');
                            var testEndDate = endDateTextBox.datetimepicker('getDate');
                            if (testStartDate > testEndDate)
                                endDateTextBox.datetimepicker('setDate', testStartDate);
                        }
                        else {
                            endDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime) {
                        endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate'));
                    },
                    dateFormat: "dd-mm-yy",
                    minDate: 0
                });
                endDateTextBox.datetimepicker({
                    onClose: function (dateText, inst) {
                        if (startDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datetimepicker('getDate');
                            var testEndDate = endDateTextBox.datetimepicker('getDate');
                            if (testStartDate > testEndDate)
                                startDateTextBox.datetimepicker('setDate', testEndDate);
                        }
                        else {
                            startDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime) {
                        startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
                    },
                    dateFormat: "dd-mm-yy"
                });
            });


            $("#reporting_time").datetimepicker({ dateFormat: 'dd-mm-yy' });

            $('#txtbookingstartdatetime').focusout(function () {
                $("#reporting_time").val($(this).val());
            });

            /*--------------Start booking type autocomplete-----------------------*/
            var BookingTypeSearch_options, BookingTypeSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: BookingTypeSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'transport_type', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                BookingTypeSearch_a = $("#ddltypeofbooking").autocomplete(options);
            });

            var BookingTypeSearch_onAutocompleteSelect = function (BookingTypeSearch_value, BookingTypeSearch_data) {
                if (BookingTypeSearch_data == -1) {
                    $("#ddltypeofbooking").val("");
                } else {
                    $("#ddltypeofbooking").val(BookingTypeSearch_data);
                }
            }
            /*---------------End of booking type autocomplete box-------------------*/

            /*--------------Start of incharge_officer_name autocomplete-------------------*/
            var InchargeSearch_options, InchargeSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: InchargeSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'TransportIncharge', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                InchargeSearch_a = $("#incharge_officer_name").autocomplete(options);
            });

            var InchargeSearch_onAutocompleteSelect = function (InchargeSearch_value, InchargeSearch_data) {
                if (InchargeSearch_data == -1) {
                    $("#incharge_officer_name").val("");
                } else {
                    var InchargeSearchData_Arr = InchargeSearch_data.split("~");
                    $("#incharge_officer_cpf_no").val(InchargeSearchData_Arr[0]);
                    $("#incharge_officer_Desig").val(InchargeSearchData_Arr[1]);
                }
            }
            /*--------------End of incharge_officer_name autocomplete-------------------*/

            /*--------------Start of Approver_officer_name autocomplete-------------------*/
            var ApproverSearch_options, ApproverSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: ApproverSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'TransportApprover', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                ApproverSearch_a = $("#transport_approver_name").autocomplete(options);
            });

            var ApproverSearch_onAutocompleteSelect = function (ApproverSearch_value, ApproverSearch_data) {
                if (ApproverSearch_data == -1) {
                    $("#transport_approver_name").val("");
                } else {
                    var ApproverSearchData_Arr = ApproverSearch_data.split("~");
                    $("#transport_approver_cpf_no").val(ApproverSearchData_Arr[0]);
                    $("#transport_approver_desig").val(ApproverSearchData_Arr[1]);
                }
            }
            /*--------------End of Approver_officer_name autocomplete-------------------*/

            /*--------------Start of Approver_officer_name autocomplete-------------------*/
            var AlloterSearch_options, AlloterSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: AlloterSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'TransportAlloter', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                AlloterSearch_a = $("#transport_allotment_name").autocomplete(options);
            });

            var AlloterSearch_onAutocompleteSelect = function (AlloterSearch_value, AlloterSearch_data) {
                if (AlloterSearch_data == -1) {
                    $("#transport_allotment_name").val("");
                } else {
                    var AlloterSearchData_Arr = AlloterSearch_data.split("~");
                    $("#transport_allotment_cpf_no").val(AlloterSearchData_Arr[0]);
                    $("#transport_allotment_desig").val(AlloterSearchData_Arr[1]);
                }
            }
            /*--------------End of Approver_officer_name autocomplete-------------------*/

            /*--------------Save Request Function Starts----------------------------*/
            $("#sendrequest").click(function () {

                //Applicant Detail
                var app_name = $("#txtappname").val().trim();
                var app_cpf_no = $("#txtappcpf_no").val().trim();
                var app_design = $("#txtappdesign").val().trim();
                var app_location = $("#txtapplocation").val().trim();
                var app_phone_ex_no = $("#txtappphoneex_no").val().trim();

                //Booking Detail
                var type_of_booking = $("#ddltypeofbooking").val().trim();
                var booking_start_Time = $("#txtbookingstartdatetime").val().trim();
                var booking_end_Time = $("#txtbookingEnddatetime").val().trim();
                var remarks = $("#txtremarks").val().trim();
                var purpose = $("#txtpurpose").val().trim();
                var no_of_person_travel = $("#no_of_person_travel").val().trim();

                //incharge Detail
                var incharge_officer_name = $("#incharge_officer_name").val().trim();
                var incharge_officer_cpfno = $("#incharge_officer_cpf_no").val().trim();
                var incharge_officer_desig = $("#incharge_officer_Desig").val().trim();

                //approver Detail
                var approver_officer_name = $("#transport_approver_name").val().trim();
                var approver_officer_cpfno = $("#transport_approver_cpf_no").val().trim();
                var approver_officer_desig = $("#transport_approver_desig").val().trim();

                //allotement Detail
                var allotement_officer_name = $("#transport_allotment_name").val().trim();
                var allotement_officer_cpfno = $("#transport_allotment_cpf_no").val().trim();
                var allotement_officer_desig = $("#transport_allotment_desig").val().trim();

                // reporting detail
                var reporting_time = $("#reporting_time").val().trim();
                var report_to_name = $("#reporting_name").val().trim();
                var report_to_address = $("#reporting_address").val().trim();
                var report_contact1 = $("#reporting_contact1").val().trim();
                var report_contact2 = $("#reporting_contact2").val().trim();


                if (app_name == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Your Name"
                    });
                    return false;
                } else if (app_cpf_no == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Your CPF No."
                    });
                    return false;
                } else if (booking_start_Time == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Booking Start Date And Time"
                    });
                    return false;
                } else if (booking_end_Time == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Booking End Date And Time"
                    });
                    return false;
                } else if (purpose == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Purpose"
                    });
                    return false;
                } else if (type_of_booking == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill  Type of Booking"
                    });
                    return false;
                } else if (incharge_officer_name == "" || incharge_officer_cpfno == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Incharge Name"
                    });
                    return false;
                } else if (approver_officer_name == "" || approver_officer_cpfno == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Approver Name"
                    });
                    return false;
                } else if (allotement_officer_name == "" || allotement_officer_cpfno == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Valid Alloter Name"
                    });
                    return false;
                } else if (reporting_time == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Reporting Time"
                    });
                    return false;
                } else if (report_to_name == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Select Reporting Name"
                    });
                    return false;
                } else if (report_to_address == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please  Reporting Address"
                    });
                    return false;
                } else if (report_contact1 == "") {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Report To Contact 1"
                    });
                    return false;
                } else if (isNaN(report_contact1) || report_contact1.length != 10) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Valid Report To Contact 1"
                    });
                    return false;
                } else if (report_contact2 != "") {
                    if (isNaN(report_contact2) || report_contact2.length != 10) {
                        $.gritter.add({
                            title: "Notification",
                            text: "Please Fill Valid Report To Contact 2"
                        });
                        return false;
                    }
                } else if (isNaN(no_of_person_travel)) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Fill Valid No. of person Travelling"
                    });
                    return false;
                }
                var my_today_date = new Date();

                if (Date.parse(booking_start_Time) >= Date.parse(booking_end_Time)) {
                    alert("Booking End Date Must Be Greater Than to Booking Start Date");
                    return false;
                }

                $.blockUI({ message: '' });
                $.ajax({
                    type: "POST",
                    url: "services/transport/addandupdatetransportbooking.aspx",
                    data: "app_name=" + app_name + "&app_cpf_no=" + app_cpf_no + "&app_design=" + encodeURIComponent(app_design) + "&app_loc=" + app_location + "&app_ph_ex_no=" + app_phone_ex_no + "&type_booking=" + type_of_booking + "&booking_start_datetime=" + booking_start_Time + "&booking_end_datetime=" + booking_end_Time + "&remarks=" + remarks + "&purpose=" + purpose + "&approver_name=" + approver_officer_name + "&approver_cpf_no=" + approver_officer_cpfno + "&approver_desig=" + encodeURIComponent(approver_officer_desig) + "&module_type=" + sActionType + "&incharge_name=" + incharge_officer_name + "&incharge_cpf_no=" + incharge_officer_cpfno + "&incharge_desig=" + encodeURIComponent(incharge_officer_desig) + "&allotemet_name=" + allotement_officer_name + "&allotemet_cpf_no=" + allotement_officer_cpfno + "&allotemet_desig=" + encodeURIComponent(allotement_officer_desig) + "&reporting_time=" + reporting_time + "&report_to_name=" + report_to_name + "&report_to_address=" + report_to_address + "&report_contact1=" + report_contact1 + "&report_contact2=" + report_contact2 + "&no_of_person_travel=" + no_of_person_travel + "&my_index_no=" + sBookingID,
                    success: function (msg) {
                        // alert(msg);
                        var values = msg.split("~");
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                            window.location = self.location;
                        } else if (values[0] == "FALSE" && values[1] == "ERR") {
                            alert(values[2]);
                        }
                    }
                });
            });
            /*--------------Save Request Function Ends----------------------------*/

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="txtHideActionType" runat="server" />
    <asp:HiddenField ID="txtHideBookingID" runat="server" />
    <div class="div-fullwidth iframeMainDiv" style="width: 98%;">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                <img src="Images/transport.png" style="position: relative; float: left; margin: 8px 5px 0 0;
                    opacity: 0.78;" />Transport Booking System</h1>
            <a href="TransportBookingHistory.aspx" id="add_new_transport_Request" style="float: right;
                margin-right: 10px; margin-top: 5px;" class="g-button g-button-red">View Bookings</a>
        </div>
        <div class="div-pagebottom">
        </div>
        <div id="div_full_form" class="div-fullwidth" style="width: 100%; margin-top: 0;">
            <fieldset style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Applicant Details</legend>
                <div class="div-fullwidth marginbottom">
                    <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                        <h1 class="content">
                            Applicant Name</h1>
                        <input runat="server" id="txtappname" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                        <h1 class="content">
                            Designation</h1>
                        <input runat="server" id="txtappdesign" type="text" value="" class="contactdir-inputbox"
                            style="width: 180px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 26%; margin-top: 0;">
                        <h1 class="content">
                            CPF No.</h1>
                        <input runat="server" id="txtappcpf_no" type="text" value="" class="contactdir-inputbox"
                            style="width: 150px;" disabled="disabled" />
                    </div>
                </div>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 40%; margin-top: 0;">
                        <h1 class="content" style="width: 121px;">
                            Department</h1>
                        <input runat="server" id="txtapplocation" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content">
                            Phone Ex. no.</h1>
                        <input runat="server" id="txtappphoneex_no" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
            <fieldset style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Booking Details</legend>
                <div class="div-fullwidth marginbottom">
                    <div class="div-fullwidth" style="width: 30%; margin-top: 0;">
                        <h1 class="content">
                            Booking Type</h1>
                        <input runat="server" id="ddltypeofbooking" type="text" value="" class="contactdir-inputbox autocompleteclass"
                            style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat;
                            padding-left: 35px; width: 150px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                        <h1 class="content" style="width: 110px;">
                            Booking From</h1>
                        <input runat="server" id="txtbookingstartdatetime" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                        <h1 class="content" style="width: 110px;">
                            Booking To</h1>
                        <input runat="server" id="txtbookingEnddatetime" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" />
                    </div>
                </div>
                <div class="div-fullwidth marginbottom">
                    <div class="div-fullwidth" style="width: 30%; margin-top: 0;">
                        <h1 class="content">
                            No of Person</h1>
                        <div style="position: relative; float: left;">
                            <input runat="server" id="no_of_person_travel" type="text" value="" class="contactdir-inputbox"
                                style="width: 150px; margin-left: 5px;" />
                        </div>
                    </div>
                    <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                        <h1 class="content" style="width: 110px;">
                            Purpose</h1>
                        <input runat="server" id="txtpurpose" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 35%; margin-top: 0;">
                        <h1 class="content" style="width: 110px;">
                            Place To Visit</h1>
                        <input runat="server" id="txtremarks" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" />
                    </div>
                </div>
            </fieldset>
            <fieldset style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Reporting Details</legend>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 27%; margin-top: 0;">
                        <h1 class="content marginbottom">
                            Report To Name</h1>
                        <input runat="server" id="reporting_name" type="text" value="" class="contactdir-inputbox"
                            style="width: 90%;" />
                    </div>
                    <div class="div-fullwidth" style="width: 72%; margin-top: 0;">
                        <h1 class="content marginbottom" style="width: 100%;">
                            Report To Address</h1>
                        <input runat="server" id="reporting_address" type="text" value="" class="contactdir-inputbox"
                            style="width: 628px;" />
                    </div>
                </div>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 27%; margin-top: 0;">
                        <h1 class="content marginbottom" style="width: 100%;">
                            Reporting Time</h1>
                        <input runat="server" id="reporting_time" type="text" value="" class="contactdir-inputbox"
                            style="width: 90%" />
                    </div>
                    <div class="div-fullwidth" style="width: 210px; margin-top: 0;">
                        <h1 class="content marginbottom">
                            Report To Contact 1</h1>
                        <input runat="server" id="reporting_contact1" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 210px; margin-top: 0;">
                        <h1 class="content marginbottom">
                            Report To Contact 2</h1>
                        <input runat="server" id="reporting_contact2" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" />
                    </div>
                </div>
            </fieldset>
            <fieldset id="incharge_fieldset_bar" style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Incharge Detail</legend>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content" style="width: 124px;">
                            Incharge Name</h1>
                        <input runat="server" id="incharge_officer_name" type="text" value="" class="autocompleteclass"
                            style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat;
                            padding-left: 35px; width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 24%; margin-top: 0;">
                        <h1 class="content">
                            CPF No.</h1>
                        <input runat="server" id="incharge_officer_cpf_no" type="text" value="" class="contactdir-inputbox"
                            style="width: 130px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content">
                            Designation</h1>
                        <input runat="server" id="incharge_officer_Desig" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
            <fieldset id="approver_fieldset_bar" style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Approver Detail</legend>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content" style="width: 124px;">
                            Approver Name</h1>
                        <input runat="server" id="transport_approver_name" type="text" value="" class="autocompleteclass"
                            style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat;
                            padding-left: 35px; width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 24%; margin-top: 0;">
                        <h1 class="content">
                            CPF No.</h1>
                        <input runat="server" id="transport_approver_cpf_no" disabled="disabled" type="text"
                            value="" class="contactdir-inputbox" style="width: 130px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content">
                            Designation</h1>
                        <input runat="server" id="transport_approver_desig" disabled="disabled" type="text"
                            value="" class="contactdir-inputbox" style="width: 200px;" />
                    </div>
                </div>
            </fieldset>
            <fieldset id="allotment_fieldset_bar" style="width: 97.5%;">
                <legend class="content" style="float: none; color: green">Alloter Detail</legend>
                <div class="div-fullwidth">
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content" style="width: 124px;">
                            Alloter Name</h1>
                        <input runat="server" id="transport_allotment_name" type="text" value="" class="autocompleteclass"
                            style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat;
                            padding-left: 35px; width: 200px;" />
                    </div>
                    <div class="div-fullwidth" style="width: 24%; margin-top: 0;">
                        <h1 class="content">
                            CPF No.</h1>
                        <input runat="server" id="transport_allotment_cpf_no" type="text" value="" class="contactdir-inputbox"
                            style="width: 130px;" disabled="disabled" />
                    </div>
                    <div class="div-fullwidth" style="width: 38%; margin-top: 0;">
                        <h1 class="content">
                            Designation</h1>
                        <input runat="server" id="transport_allotment_desig" type="text" value="" class="contactdir-inputbox"
                            style="width: 200px;" disabled="disabled" />
                    </div>
                </div>
            </fieldset>
            <div class="div-fullwidth marginbottom">
                <input id="sendrequest" style="position: relative; float: right; text-transform: none;"
                    type="button" class="buttonBigGreen" value="Save Booking Details" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
