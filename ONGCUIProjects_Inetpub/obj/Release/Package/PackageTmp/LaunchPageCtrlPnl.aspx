﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LaunchPageCtrlPnl.aspx.cs"
    Inherits="ONGCUIProjects.LaunchPageCtrlPnl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="chrome=1">
    <link rel="stylesheet" href="../css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="../css/css3.css" media="screen" type="text/css" />
    <script type="text/javascript" language="javascript" src="../js/jquery-1.7.2.min_a39dcc03.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.timepicker.js"></script>
    <script type="text/javascript" src="../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <link rel="stylesheet" href="../js/themes/smoothness/jquery-ui-1.8.22.custom.css"type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../css/jquery.gritter.css" />
    <script type="text/javascript" src="../js/jquery.gritter.js"></script>
    <script src="../js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {
            var MenuGrpID = $("#lblMenuGrpID").text();
            var SubMenuID = $("#lblSubMenuID").text();
            $(".cmdEditTOD").fancybox({ modal: true });
            var WindowHeight = $(window).height();
            $('#divGroupSubMenu').css('max-height', parseInt(WindowHeight - 106) + 'px');
            //alert(WindowHeight);
            $('#WhatsNewUl').css('max-height', parseInt(WindowHeight - 64) + 'px');
            $('#PlantHeadUL').css('max-height', parseInt(WindowHeight - 64) + 'px');

            $(".cmdDeleteTOD").click(function () {
                var sDelID = $(this).attr("tod_id");
                $.ajax({
                    type: "POST",
                    url: "services/launchpageadmin/AddUpdTOD.aspx",
                    data: "type=DEL&id=" + sDelID,
                    success: function (msg) {
                        var msg_arr = msg.split("~");
                        if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                            alert(msg_arr[2]);
                            window.location = self.location;
                        } else {
                            alert(msg_arr[2]);
                        }
                    }
                });
            });

            $("#ddlMenuGrp").change(function () {
                MenuGrpID = $(this).val();
                window.location = 'LaunchPageCtrlPnl.aspx?setting=MENU&mnugrp=' + MenuGrpID;
            });

            $(".cmdEditSubMenu").click(function () {
                SubMenuID = $(this).parent('li').attr('submenuid');
                window.location = 'LaunchPageCtrlPnl.aspx?setting=MENU&mnugrp=' + MenuGrpID + '&submenu=' + SubMenuID;
            });

            $("input[name=OptLinkType]").change(function () {
                var LinkType = $(this).val();
                if (LinkType == "OptLink") {
                    $('#DivDownloadBox').hide();
                    $('#DivLink').show();
                } else if (LinkType == "OptDwld") {
                    $('#DivDownloadBox').show();
                    $('#DivLink').hide();
                } else if (LinkType == "OptUpldFile") {
                    $('#DivDownloadBox').show();
                    $('#DivLink').hide();
                }
            });
            $("#FileUploadControl").change(function () {
                alert(this.files[0].size);
                //                if (this.files[0].size > 209715200) {
                //                    alert("File Size Can Not be Greater Than 200 MB");
                //                    $("#FileUploadControl").val("");
                //                } else if (this.files[0].size == 0) {
                //                    alert("File Size Can Not be Zero");
                //                    $("#FileUploadControl").val("");
                //                }
            });

            $(".cmdDeleteWhatsNew").click(function () {
                var DelID = $(this).parent('li').attr('whatsnewid');
                if (confirm("Are you sure to delete it?")) {
                    $.ajax({
                        type: "POST",
                        url: "services/launchpageadmin/DeleteMenuOrWhatsNew.aspx",
                        data: "setting=WHATSNEW&delid=" + DelID,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                window.location = self.location;
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });

            $(".cmdDeletePlantHeadQuots").click(function () {
                var DelID = $(this).parent('li').attr('phid');
                if (confirm("Are you sure to delete it?")) {
                    $.ajax({
                        type: "POST",
                        url: "services/launchpageadmin/DeleteMenuOrWhatsNew.aspx",
                        data: "setting=PLANTHEAD&delid=" + DelID,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                window.location = self.location;
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });

            $(".cmdDeleteSubMenu").click(function () {
                var DelID = $(this).parent('li').attr('submenuid');
                if (confirm("Are you sure to delete this menu?")) {
                    $.ajax({
                        type: "POST",
                        url: "services/launchpageadmin/DeleteMenuOrWhatsNew.aspx",
                        data: "setting=MENU&delid=" + DelID,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                window.location = self.location;
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });
            $(".cmdShowHideWhatsNew").click(function () {
                if (confirm('Are you sure to send this whats new to archive?')) {
                    var WhatsNewID = $(this).parent('li').attr('whatsnewid');
                    var ActionType = "HIDE";
                    $.ajax({
                        type: "POST",
                        url: "services/launchpageadmin/ShowHideWhatsNew.aspx",
                        data: "action=" + ActionType + "&id=" + WhatsNewID,
                        success: function (msg) {
                            var msg_arr = msg.split("~");
                            if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                                window.location = self.location;
                            } else {
                                alert(msg_arr[2]);
                            }
                        }
                    });
                }
            });
            $("#txtMnuGrpPriority").keyup(function () {
                if (isNaN($(this).val())) {
                    $(this).val("");
                    alert("Please Enter a numeric digit.");
                }
            });

            $("#txtOrder").keyup(function () {
                if (isNaN($(this).val())) {
                    $(this).val("");
                    alert("Please Enter a numeric digit.");
                }
            });

            $("#cmdCreateMnuGrp").click(function () {
                var sActionType = $(this).val();
                var sUpdateID = $(this).attr("UpdateID");
                var sMnuGrpTitle = $("#txtMnuGrpTitle").val().trim();
                var sMnuGrpPriority = $("#txtMnuGrpPriority").val().trim();
                var sMenuPosition = $('input[name="optSelectMenuGrpPosition"]:checked').val();

                if (sMnuGrpTitle == "") {
                    alert("Please enter menu title");
                    return false;
                }
                if (sMnuGrpPriority == "") {
                    alert("Please enter menu priority");
                    return false;
                }

                if (sMenuPosition == undefined) {
                    alert("Please select menu display position");
                    return false;
                }
                //alert("ActionType=" + sActionType + "&MnuGrpTitle=" + sMnuGrpTitle + "&MnuGrpPriority=" + sMnuGrpPriority + "&MenuPosition=" + sMenuPosition);
                $.ajax({
                    type: "POST",
                    url: "services/launchpageadmin/AddMenuGroup.aspx",
                    data: "ActionType=" + sActionType + '~' + sUpdateID + "&MnuGrpTitle=" + sMnuGrpTitle + "&MnuGrpPriority=" + sMnuGrpPriority + "&MenuPosition=" + sMenuPosition,
                    success: function (msg) {
                        var msg_arr = msg.split("~");
                        if (msg_arr[0] == "TRUE" && msg_arr[1] == "SUCCESS") {
                            alert(msg_arr[2]);
                            window.location = self.location;
                        } else {
                            alert(msg_arr[2]);
                        }
                    }
                });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMenuGrpID" runat="server" Text="-1" Style="display: none"></asp:Label>
    <asp:Label ID="lblSubMenuID" runat="server" Text="-1" Style="display: none"></asp:Label>
    <div class="div-fullwidth iframeMainDiv">
        <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
            <h1 class="heading" style="width: auto;">
                <asp:Label ID="lblSettingTypeHeading" runat="server">Launch Page Settings</asp:Label></h1>
            <input type="button" class="g-button g-button-submit" style="text-transform: uppercase;
                float: right; margin-right: 10px; margin-top: 5px; text-decoration: none;" onclick="window.location='LaunchPageCtrlPnl.aspx?setting=WHATSNEW';"
                value="Whats New" />
            <input type="button" class="g-button g-button-submit" style="text-transform: uppercase;
                float: right; margin-right: 10px; margin-top: 5px; text-decoration: none;" onclick="window.location='LaunchPageCtrlPnl.aspx?setting=TOD';"
                value="Thought of Day" />
            <input type="button" class="g-button g-button-submit" style="text-transform: uppercase;
                float: right; margin-right: 10px; margin-top: 5px; text-decoration: none;" onclick="window.location='LaunchPageCtrlPnl.aspx?setting=MENU';"
                value="Menus" />
                <input type="button" class="g-button g-button-share" style="text-transform: uppercase;
                float: right; margin-right: 10px; margin-top: 5px; text-decoration: none;" onclick="window.location='LaunchPageCtrlPnl.aspx?setting=PLANTHEAD';"
                value="Plant Head Page" />
                
        </div>
        <div class="div-pagebottom">
        </div>
        <div runat="server" class="div-fullwidth" id="divContainer">
            <div runat="server" class="div-halfwidth" id="divLeftContainer">
                
                    
                        <asp:DropDownList ID="ddlMenuGrp" runat="server" Width="100%" Visible="False">
                        </asp:DropDownList>
                    
                <div runat="server" class="div-fullwidth" id="divGroupSubMenu" style='margin-top:12px;overflow:auto;'>
                </div>
            </div>
            <div runat="server" class="div-halfwidth" id="divRightContainer">
                <div runat="server" id="divCreateMenuGrp" class="createlink-container" style="margin-bottom: 10px;
                    float: right; width: 400px; padding: 10px; box-shadow: 1px 1px 2px #ccc; border: 1px solid rgb(175, 172, 172);
                    background-color: #f9f9f9;">
                    <h1 runat="server" id="CreateMenuHeading" class="heading" style="border-bottom: 2px solid rgb(160, 155, 155); width: 98%;">
                        Create Menu Group</h1>
                    <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 50px;">
                            Title</h1>
                        <asp:TextBox class="createlink-inputbox" ID="txtMnuGrpTitle" Style="width: 340px;"
                            runat="server"></asp:TextBox>
                    </div>
                    <div class="createlink-divfullwidth">
                        <div style='position: relative; float: left; width: 120px;'>
                            <h1 class="createlink-text" style="width: 50px;">
                                Priority</h1>
                            <asp:TextBox class="createlink-inputbox" ID="txtMnuGrpPriority" Style="width: 50px;"
                                runat="server"></asp:TextBox>
                        </div>
                        <div style="position: relative; float: right; left: 0px; width: 270px; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
                            background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc;
                            margin-bottom: 10px; padding:1px;">
                            <h1 class="createlink-text" style="margin-left: 10px;">
                                Display Position:</h1>
                            <asp:RadioButtonList ID="optSelectMenuGrpPosition" runat="server" 
                                CellSpacing="2" CssClass="createlink-text" RepeatDirection="Horizontal" 
                                style='margin:0px;' Width="125px">
                                <asp:ListItem Value="LEFT">Left</asp:ListItem>
                                <asp:ListItem Value="RIGHT">Right</asp:ListItem>
                            </asp:RadioButtonList>
                            
                        </div>
                    </div>
                    
                    <asp:Button ID="cmdDeleteMnuGrp" runat="server" Text="DELETE" 
                        CssClass="g-button g-button-red" style='margin-left:10px; float:right;' 
                        onclick="cmdDeleteMnuGrp_Click" />
                    <asp:Button ID="cmdCreateMnuGrp" runat="server" Text="CREATE" 
                        CssClass="g-button g-button-submit"  OnClientClick="return false;" />
                        <asp:HiddenField
                            ID="txtMenuGrpIDForUpdateDelete" runat="server" />

                </div>
                <div runat="server" id="divCreateSubMenu" class="createlink-container" style="margin-bottom: 10px;
                    float: right; width: 400px; padding: 10px; box-shadow: 1px 1px 2px #ccc; border: 1px solid rgb(175, 172, 172);
                    background-color: #f9f9f9;">
                    <h1 class="heading" style="border-bottom: 2px solid rgb(160, 155, 155); width: 98%;">
                        <asp:Label ID="CreateNewHeading" runat="server" Text=""></asp:Label></h1>
                    <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 88px;">
                            Title</h1>
                        <asp:TextBox class="createlink-inputbox" ID="txtTitle" Style="width: 300px;" runat="server"></asp:TextBox>
                    </div>
                    <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 88px;">
                            Tooltip</h1>
                        <asp:TextBox class="createlink-inputbox" ID="txtToolTip" Style="width: 300px;" runat="server"></asp:TextBox>
                    </div>
                    <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 88px;">
                            Position</h1>
                        <asp:TextBox class="createlink-inputbox" ID="txtOrder" Style="width: 100px;" runat="server"></asp:TextBox>
                    </div>
                    <div style="position: relative; float: left; top: 5px; left: 0px; padding: 2px 0 7px 0px;
                        width: 99.7%; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
                        background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc;
                        margin-bottom: 20px;">
                        <h1 class="createlink-text" style="margin-left: 10px;">
                            Link Type :
                        </h1>
                        <asp:RadioButton runat="server" ID="OptLink" Text="Address" Style="position: relative;
                            float: left; margin-right: 10px; top: 0px; left: 0px;" GroupName="OptLinkType"
                            CssClass="content"></asp:RadioButton>
                        <asp:RadioButton runat="server" ID="OptDwld" Text="Website" Style="position: relative;
                            float: left; margin-right: 10px;" GroupName="OptLinkType" CssClass="content">
                        </asp:RadioButton>
                        <asp:RadioButton runat="server" ID="OptUpldFile" Text="Upload File" Style="position: relative;
                            float: left; margin-right: 10px;" GroupName="OptLinkType" CssClass="content">
                        </asp:RadioButton>
                    </div>
                    <div class="createlink-divfullwidth" id="DivLink" style="display: none;">
                        <h1 class="createlink-text" style="width: 88px;">
                            Link URL</h1>
                        <asp:TextBox class="createlink-inputbox" ID="txtURL" Style="width: 300px;" runat="server"></asp:TextBox>
                    </div>
                    <div id="DivDownloadBox" class="createlink-divfullwidth" style="margin-bottom: 10px;
                        display: none">
                        <h1 class="createlink-text" style="width: 88px;">
                            Select File</h1>
                         <asp:FileUpload runat="server"  Multiple="Multiple"  ID="FileUploadControl"></asp:FileUpload>
                    </div>
                    <asp:Button ID="CmdSaveMenuItem" runat="server" class="g-button g-button-submit"
                        Text="SAVE" OnClick="CmdSaveMenuItem_Click" />
                </div>

               
                 <div runat="server" id="edit_sbmn" class="createlink-container" style="margin-bottom: 10px;
                    float: right; width: 400px; padding: 10px; box-shadow: 1px 1px 2px #ccc;border: 1px solid rgb(175, 172, 172);
                    background-color: #f9f9f9;">
                    <h1 class="heading" style="border-bottom: 2px solid rgb(160, 155, 155); width: 98%;">
                        Edit Submenu</h1>
                      <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 88px;">
                            Select Sub_Menu</h1>
                         <asp:DropDownList ID="ddl_sm" runat="server" Style="width: 200px;" AutoPostBack="true" OnSelectedIndexChanged="ddl_sm_SelectedIndexChanged"></asp:DropDownList>
                         
                    </div>
                    
                    <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 88px;">
                            Title</h1>
                        <asp:TextBox class="createlink-inputbox" ID="TextBox1" Style="width: 300px;" runat="server"></asp:TextBox>
                    </div>
                    <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 88px;">
                            Tooltip</h1>
                        <asp:TextBox class="createlink-inputbox" ID="TextBox2" Style="width: 300px;" runat="server"></asp:TextBox>
                    </div>
                    <div class="createlink-divfullwidth">
                        <h1 class="createlink-text" style="width: 88px;">
                            Position</h1>
                        <asp:TextBox class="createlink-inputbox" ID="TextBox3" Style="width: 100px;" runat="server"></asp:TextBox>
                    </div>
                   <%-- <div style="position: relative; float: left; top: 5px; left: 0px; padding: 2px 0 7px 0px;
                        width: 99.7%; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE);
                        background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc;
                        margin-bottom: 20px;">
                        <h1 class="createlink-text" style="margin-left: 10px;">
                            Link Type :
                        </h1>
                        <asp:RadioButton runat="server" ID="RadioButton1" Text="Address" Style="position: relative;
                            float: left; margin-right: 10px; top: 0px; left: 0px;" GroupName="OptLinkType"
                            CssClass="content"></asp:RadioButton>
                        <asp:RadioButton runat="server" ID="RadioButton2" Text="Website" Style="position: relative;
                            float: left; margin-right: 10px;" GroupName="OptLinkType" CssClass="content">
                        </asp:RadioButton>
                        <asp:RadioButton runat="server" ID="RadioButton3" Text="Upload File" Style="position: relative;
                            float: left; margin-right: 10px;" GroupName="OptLinkType" CssClass="content">
                        </asp:RadioButton>
                    </div>
                    <div class="createlink-divfullwidth" id="DivLink1" style="display: none;">
                        <h1 class="createlink-text" style="width: 88px;">
                            Link URL</h1>
                        <asp:TextBox class="createlink-inputbox" ID="TextBox4" Style="width: 300px;" runat="server"></asp:TextBox>
                    </div>
                    <div id="DivDownloadBox1" class="createlink-divfullwidth" style="margin-bottom: 10px;
                        display: none">
                        <h1 class="createlink-text" style="width: 88px;">
                            Select File</h1>
                         <asp:FileUpload runat="server"  Multiple="Multiple"  ID="FileUpload1"></asp:FileUpload>
                    </div>--%>
                    <asp:Button ID="btn_update" runat="server" class="g-button g-button-submit"
                        Text="Update" OnClick="btn_update_Click"/>
                </div>
             
                <%--<asp:Label ID="lblMenuType" runat="server" style="display:none"  ></asp:Label>--%>
            </div>
            <div style="left: 27%; top: 40%; position: fixed;" runat="server" id="DivDefault">
                <p style="font-size: 10pt;" class="content">
                    <span style="font-size: 30px; color: Green;">Welcome to Launch Page Control Panel</span><br />
                    Please select an option from the top right corner</p>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
