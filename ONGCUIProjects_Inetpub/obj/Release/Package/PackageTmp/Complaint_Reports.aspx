<%@ Page language="c#" Codebehind="Complaint_Reports.aspx.cs" AutoEventWireup="True" Inherits="ONGCUIProjects.Complaint_Reports" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>Complaint Management Report</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
         <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <!-- CSS Links Ends -->
    <!-- JS Links Starts -->
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" href="css/jquery.gritter.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <!-- JS Links Starts -->
    <!-- Script Starts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtdatefrom").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            $("#txtdateto").datepicker({ maxDate: "0D", dateFormat: "dd-mm-yy" });
            var DivWindowHeight = parseInt($(window).height()) - 104;
            $('#report_container').css('max-height', DivWindowHeight + 'px');
        });
    </script>
            <style>
            th
            {
                background-color:Silver;
              }
            </style>
    </head>
<body bgcolor="#BCC7D8">
    <form id="form1" runat="server" class="MainInterface-iframe">
        <div class="div-fullwidth iframeMainDiv">
            <div class="div-fullwidth" style=" background-color: White; border-top: 5px solid #009f3c;">
                <h1 class="heading" style="width: auto;" id="txtHeading">Complaint Management Report</h1>
                <asp:Label ID="Label2" runat="server" class="heading-content-right" style="color: Red;"></asp:Label>
            </div>
            <div class="div-pagebottom"></div>

            <div class="div-fullwidth marginbottom">
                <div style="position:relative; float :left; margin-left:1px; width:40%; top: 0px; left: 0px;">
                    <asp:Label ID="Label3" runat="server" Text="Select Type" class=" content"></asp:Label>
                    <asp:DropDownList ID="ddltypeofreport" runat="server" style="width: auto;" 
                        AutoPostBack="True" 
                        onselectedindexchanged="ddltypeofreport_SelectedIndexChanged">
                        <asp:ListItem>---Select Type---</asp:ListItem>
                        <asp:ListItem Value="DATEWISE">Date wise </asp:ListItem>
                        <asp:ListItem Value="CTYPE">Complaint Type</asp:ListItem>                       
                        </asp:DropDownList>

                   <asp:DropDownList ID="ddlcomplainttype" runat="server" style="width: auto;" 
                        AutoPostBack="True" Visible="False">    
                         <asp:ListItem Value="CIVIL">CIVIL</asp:ListItem>
                        <asp:ListItem Value="ELECTRICAL(AC)">ELECTRICAL(AC)</asp:ListItem>
                        <asp:ListItem Value="ELECTRICAL(MAINTENANCE)">ELECTRICAL(MAINTENANCE)</asp:ListItem>
                        <asp:ListItem Value="HOUSEKEEPING">HOUSEKEEPING</asp:ListItem>
                        <asp:ListItem Value="TELEPHONE">TELEPHONE</asp:ListItem>
                       <asp:ListItem Value="WalkieTalkie">Walkie-Talkie</asp:ListItem>
                        <asp:ListItem Value="PAPaging">PA Paging System</asp:ListItem>
                            
                       <%-- <asp:ListItem Value="OTHER">OTHER</asp:ListItem>--%>
                         </asp:DropDownList>
                  
                </div>
                 <div style="position:relative; float :left; margin-left:1px; width:20%; top: 0px; left: 0px;">
                   <asp:Label ID="eto" runat="server" Text="From Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdatefrom" runat="server" Width="180px" Height="26px" autoComplete="off"></asp:TextBox>

                  
                </div>
                 <div style="position:relative; float :left; margin-left:5px; width:20%; top: 0px; left: 0px;">
                   <asp:Label ID="Label1" runat="server" Text="To Date"></asp:Label>
                     &nbsp;
                     <asp:TextBox ID="txtdateto" runat="server" Width="180px" Height="26px" autoComplete="off"></asp:TextBox>

                  
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Button ID="cmdgenerate" runat="server" Text="generate" 
                    onclick="cmdgenerate_Click1" />
            </div>
            <div class="div-fullwidth" style="height:400px; overflow:scroll;">            
          
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" BackColor="LightGoldenrodYellow" 
                BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None">
                <AlternatingRowStyle BorderColor="#336666" BackColor="PaleGoldenrod" />
                <FooterStyle BackColor="Tan" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" Width="1200px" />
                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                   <Columns> 
                       <asp:TemplateField HeaderText="SR.NO.">
                            <ItemTemplate>
                                <div style=" text-align:center;"><asp:Label ID="Label2" runat="server" Text='<%# Container.DataItemIndex+1%>' CssClass="" ToolTip="Applicant"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>                     
                        <asp:TemplateField HeaderText="Applicant">
                            <ItemTemplate>
                                <div style="width:150px; text-align:center;"><asp:Label ID="Label2" runat="server" Text='<%#Eval("APP_NAME")%>' CssClass="" ToolTip="Applicant"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation ">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("APP_DESIG")%>' CssClass="" ToolTip="Designation"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone Extension No.">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("APP_PHONE_EX_NO")%>' CssClass="" ToolTip="Phone Extension No."></asp:Label>
                            </ItemTemplate>
                            <%--    SELECT [ID] ,[APP_CPF_NO],[APP_NAME] ,[APP_DESIG] ,[] ,[APP_MOBILE_NO] ,[],[],[COMPLAIN_TYPE_ID] ,[] ,[VIEWED_BY] ,[VIEWED_RESPONSE] ,[VIEW_TIMESTAMP] ,[ENTRY_BY],[] ,[] --%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("APP_LOCATION")%>' CssClass="" ToolTip="Location"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Problem">
                            <ItemTemplate>
                            <div style="width:110px;"><asp:Label ID="Label6" runat="server" Text='<%#Eval("PROBLEM_DISCRIPTION")%>' CssClass="" ToolTip="Problem"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room No.">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("ROOM_NO")%>' CssClass="" ToolTip="Room No."></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Type">
                            <ItemTemplate>
                             <asp:Label ID="Label8" runat="server" Text='<%#Eval("COMPLAIN_DEPARTMENT")%>' CssClass="" ToolTip="Complain Type"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint Date">
                            <ItemTemplate>
                                 <div style="width:110px; text-align:center;"><asp:Label ID="Label9" runat="server" Text='<%#Eval("TIMESTAMP","{0:dd-MM-yyyy}")%>' CssClass="" ToolTip="Complain Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Resolved Date">
                            <ItemTemplate>
                                 <div style="width:110px; text-align:center;"><asp:Label ID="Label9" runat="server" Text='<%#Eval("VIEW_TIMESTAMP")%>' CssClass="" ToolTip="Complain Date"></asp:Label></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblComplaintStatus" runat="server" Text='<%#Eval("VIEWED_RESPONSE")%>' CssClass="" ToolTip="Status"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                       

                    </Columns>
            </asp:GridView>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </div>
            <asp:Button ID="cmdExporttoExcel" runat="server" Text="Export To Excel" 
                style="position:relative; float:right; margin-right:10px" 
                onclick="cmdExporttoExcel_Click" Visible="False" />
        </div>
    </form>
</body>
</html>

                    
                   
