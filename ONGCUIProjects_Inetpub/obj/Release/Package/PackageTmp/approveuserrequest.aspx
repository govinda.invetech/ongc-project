﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="approveuserrequest.aspx.cs" Inherits="ONGCUIProjects.approveuserrequest" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>User Request Approval Form</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" src="js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css" media="screen">
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script src="js/jquery.callout.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/our.js"></script>
    <link rel="stylesheet" href="css/diggy.css?ver=4.5.3.3" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/jquery.callout.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/wave.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/styleGlobal.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/css3.css" media="screen" type="text/css" />

    <script language="javascript">
        $(document).ready(function () {
            $(document).ajaxStop($.unblockUI);
            $("#txt_dob").datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });
            $.gritter.options = {
                position: 'bottom-right',
                class_name: '', // could be set to 'gritter-light' to use white notifications
                fade_in_speed: 'medium', // how fast notifications fade in
                fade_out_speed: 1000, // how fast the notices fade out
                time: 3000 // hang on the screen for...
            }
            $("#lbldate").hide();
            $("#my_module_hint").hide();
            var myhint_arr = $("#my_module_hint").text().split('`');
            var myhint = myhint_arr[0];
            var myhint5 = myhint_arr[1];
            $('input[name="my_hint"][value="' + myhint5 + '"]').prop('checked', true);

            $('input[type="radio"][name="my_hint"]').change(function () {
                window.location = "approveuserrequest.aspx?myhint=APPROVE`" + $(this).val();
            });

            if (myhint == "UPDATE") {
                $('#div_for_approve').remove();
                $("#Label2").text('');
                $("#cmdGo").hide();
                $("#reset_pwd").show();
                $("#Label3").hide();
                $("#div_Edit").show();
                $("#ddlpendinguserlist").hide();
                $("#txtHeading").text("Update User Details");
                $("#delete_user").val("Remove User");
            } else if (myhint == "APPROVE") {
                $("#div_Edit").hide();
                $("#Label2").show();
                $("#cmdGo").show();
                $("#reset_pwd").hide();
                $("#Label3").show();
                $("#txtHeading").text("User Request Approval Form");
                $("#ddlpendinguserlist").show();
                var cmdDeleteText = (myhint5 == 'PENDING') ? "Reject this request" : "Delete this request"
                $("#delete_user").val(cmdDeleteText);
            } else {
                alert('Invalid Type');
                return false;
            }
            $("#fancyurl").fancybox({
                modal: true
            });

            $("#Table2").hide();
            $("#Emp_Err_Msg").hide();
            $("#Emailid_Err_Msg").hide();
            $("#Cpf_No_Err_Msg").hide();
            $("#lblfinalmsg").hide();
            $("#Mbl_Err_Msg").hide();
            $("#txtEmpName").focus(function () {
                $(this).css('border-color', '');
                $("#Emp_Err_Msg").hide();
            });
            $("#txtEmpCPFNo").focus(function () {
                $(this).css('border-color', '');
                $("#Cpf_No_Err_Msg").hide();
            });
            $("#txtMblNo").focus(function () {
                $(this).css('border-color', '');
                $("#Mbl_Err_Msg").hide();
            });
            $("#txtEmail1").focus(function () {
                $(this).css('border-color', '');
                $("#Emailid_Err_Msg").hide();
            });
            // for load dropdown list
            $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
            $.ajax({
                type: "GET",
                url: "services/getdesigsupervisiorandworklocationlist.aspx",
                data: "",
                success: function (msg) {
                    // alert(msg);
                    var values = msg.split("~");
                    if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                        $("#ddlDesignation").html(values[3]);
                        $("#ddlWorkLocation").html(values[4]);
                        $("#ddlSupervisiorname").html(values[5]);
                        $("#ddlDepartment2").html(values[7]);
                    }
                }
            });
            // for APPROVE
            if (myhint == "APPROVE") {
                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                $.ajax({
                    type: "GET",
                    url: "services/getpendinguserlist.aspx",
                    data: "type=" + myhint_arr[1],
                    success: function (msg) {
                        //alert(msg);
                        var values = msg.split("~");
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $("#Label2").text(values[3]);
                            $("#ddlpendinguserlist").html(values[4]);
                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            $("#Label2").text(values[2]);
                            $("#cmdGo").hide();
                            $("#Label3").hide();
                            $("#ddlpendinguserlist").hide();
                            $.gritter.add({
                                title: "Notification",
                                image: "images/exclamation.png",
                                text: values[2]
                            });
                        }
                    }
                });
            }

            if (myhint == "UPDATE") {
                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                $.ajax({
                    type: "GET",
                    url: "services/getdepartmentlistoremployeelist.aspx",
                    data: "type=ALL_CPF",
                    success: function (msg) {
                        //  alert(msg);
                        var values = msg.split("~");
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $("#Label2").text(values[3]);
                            $("#ddlDepartment").html(values[4]);
                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            $("#Label2").text(values[2]);
                            $("#cmdGo").hide();
                            $("#Label3").hide();
                            $("#ddlDepartment").hide();
                            $.gritter.add({
                                title: "Notification",
                                image: "images/exclamation.png",
                                text: values[2]
                            });
                        }
                    }
                });
            }
            //autocomplete js
            var searchClick = 0;
            $("#ashish").focus(function () {
                $("#ashish").val("");
                searchClick = 1;
            });

            $("#ashish").val("Employee name..");
            $(document).click(function () {
                if (searchClick == 0) {
                    $("#ashish").val("Employee name..");
                } else {
                    searchClick = 0;
                }
            });

            var mainmenuSearch_options, mainmenuSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: mainmenuSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'emp_list', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                mainmenuSearch_a = $("#ashish").autocomplete(options);
            });


            var mainmenuSearch_onAutocompleteSelect = function (mainmenuSearch_value, mainmenuSearch_data1) {
                if (mainmenuSearch_data1 != -1) {
                    /*-----start-------------for name seach change on 12 feb 2015--*/
                    var val = $("#ashish").val().split('/');
                    mainmenuSearch_data1 = val[1];
                    /*-----end-------------for name seach change on 12 feb 2015--*/

                    $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                    $.ajax({
                        type: "GET",
                        url: "services/editandupdateuserdetail.aspx",
                        data: "cpf_no=" + mainmenuSearch_data1 + "&type=" + myhint,
                        success: function (msg) {
                            // alert(msg);
                            var values = msg.split("~");
                            if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                $("#txtEmpName").val(values[4]);
                                $("#txtEmpCPFNo").val(values[3]);
                                $("#txtAddr1").val(values[5]);
                                $("#txtAddr2").val(values[6]);
                                $("#txtCity").val(values[7]);
                                $("#txtState").val(values[8]);
                                $("#txtPinCode").val(values[9]);
                                $("#txtHomeNo").val(values[10]);
                                $("#txtMblNo").val(values[11]);
                                $("#CmbGender").val(values[12]);
                                $("#txt_dob").val(values[13]);
                                $("#txtEmail1").val(values[14]);
                                $("#txtEmail2").val(values[15]);
                                $("#ddlDesignation").val(values[17]);
                                $("#ddlSupervisiorname").val(values[18]);
                                $("#ddlDepartment2").val(values[19]);
                                $("#ddlWorkLocation").val(values[20]);
                                $("#txtemployeeexno").val(values[21]);
                                $("#cmbBldGrp").val(values[22]);
                                $("#cmdsaveandassign").val("Update and Assign Privilege");
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/tick.png",
                                    text: values[2]
                                });
                                $("#Table2").show();
                            } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {

                                $("#Table2").hide();
                                $("#lbldate").hide();
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/exclamation.png",
                                    text: values[2]
                                });
                            }
                        }
                    });
                }
            }
            // select department 

            // cmdgo click 
            $("#cmdGo").click(function () {
                var cpf_no = $("#ddlpendinguserlist").val();
                var request_date = $("#ddlpendinguserlist").children('option:selected').attr("reuestdate");
                if (cpf_no != "") {
                    $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                    $.ajax({
                        type: "GET",
                        url: "services/editandupdateuserdetail.aspx",
                        data: "cpf_no=" + cpf_no + "&type=" + myhint,
                        success: function (msg) {
                            // alert(msg);
                            var values = msg.split("~");
                            if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                $("#lbldate").text("Request Posted On : " + request_date);
                                $("#lbldate").show();
                                $("#txtEmpName").val(values[4]);
                                $("#txtEmpCPFNo").val(values[3]);
                                $("#txtAddr1").val(values[5]);
                                $("#txtAddr2").val(values[6]);
                                $("#txtCity").val(values[7]);
                                $("#txtState").val(values[8]);
                                $("#txtPinCode").val(values[9]);
                                $("#txtHomeNo").val(values[10]);
                                $("#txtMblNo").val(values[11]);
                                $("#CmbGender").val(values[12]);
                                $("#txt_dob").val(values[13]);
                                $("#txtEmail1").val(values[14]);
                                $("#txtEmail2").val(values[15]);
                                $("#ddlDesignation").val(values[17]);
                                $("#ddlSupervisiorname").val(values[18]);
                                $("#ddlDepartment2").val(values[19]);
                                $("#ddlWorkLocation").val(values[20]);
                                $("#txtemployeeexno").val(values[21]);
                                $("#cmbBldGrp").val(values[22]);
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/tick.png",
                                    text: values[2]
                                });
                                $("#Table2").show();
                            } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                                $("#Table2").hide();
                                $("#lbldate").hide();
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/exclamation.png",
                                    text: values[2]
                                });
                            }
                        }
                    });
                }
            });


            //cmdUpdateGo click
            $("#cmdUpdateGo").click(function () {
                var cpf_no = $("#ddlDepartment").val();
                if (cpf_no != "null") {
                    $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                    $.ajax({
                        type: "GET",
                        url: "services/editandupdateuserdetail.aspx",
                        data: "cpf_no=" + cpf_no + "&type=" + myhint,
                        success: function (msg) {


                            var values = msg.split("~");
                            if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                $("#txtEmpName").val(values[4]);
                                $("#txtEmpCPFNo").val(values[3]);
                                $("#txtAddr1").val(values[5]);
                                $("#txtAddr2").val(values[6]);
                                $("#txtCity").val(values[7]);
                                $("#txtState").val(values[8]);
                                $("#txtPinCode").val(values[9]);
                                $("#txtHomeNo").val(values[10]);
                                $("#txtMblNo").val(values[11]);
                                $("#CmbGender").val(values[12]);
                                $("#txt_dob").val(values[13]);
                                $("#txtEmail1").val(values[14]);
                                $("#txtEmail2").val(values[15]);
                                $("#ddlDesignation").val(values[17]);
                                $("#ddlSupervisiorname").val(values[18]);
                                $("#ddlDepartment2").val(values[19]);
                                $("#ddlWorkLocation").val(values[20]);
                                $("#txtemployeeexno").val(values[21]);
                                $("#cmbBldGrp").val(values[22]);
                                $("#cmdsaveandassign").val("Update and Assign Privilege");
                                $.gritter.add({
                                    title: "Notification",
                                    text: values[2]
                                });
                                $("#Table2").show();
                            } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                                $("#Table2").hide();
                                $("#lbldate").hide();
                                $.gritter.add({
                                    title: "Notification",
                                    image: "images/exclamation.png",
                                    text: values[2]
                                });
                            }
                        }
                    });
                }
            });
            // save asign privledge
            $("#cmdsaveandassign").click(function () {
                var employee_name = $("#txtEmpName").val();
                var mbl_no = $("#txtMblNo").val();
                var cpf_no = $("#txtEmpCPFNo").val();
                var email = $("#txtEmail1").val();
                var add1 = $("#txtAddr1").val();
                var add2 = $("#txtAddr2").val();
                var city = $("#txtCity").val();
                var state = $("#txtState").val();
                var pincode = $("#txtPinCode").val();
                var homephoeno = $("#txtHomeNo").val();
                var mobile_no = $("#txtMblNo").val();
                var department = $("#ddlDepartment2").val();
                var email2 = $("#txtEmail2").val();
                var email3 = $("#txtEmail3").val();
                var dob = $("#txt_dob").val();
               
                var worklocation = $("#ddlWorkLocation").val();
                var designation = $("#ddlDesignation").val();
                var supervisior = $("#ddlSupervisiorname").val();
                var employee_ex_no = $("#txtemployeeexno").val();
                var designnation = $("#cmbDesg").val();
                var blood_group = $("#cmbBldGrp").val();
                var gender = $("#CmbGender").val();
                if (employee_name == "") {
                    $("#txtEmpName").css('border-color', 'red');
                    $("#Emp_Err_Msg").show();
                    return false;
                } else {
                    $("#Emp_Err_Msg").hide();
                }
                if (cpf_no == "") {
                    $("#txtEmpCPFNo").css('border-color', 'red');
                    $("#Cpf_No_Err_Msg").show();
                    return false;
                } else {
                    $("#Cpf_No_Err_Msg").hide();
                }
                if (mbl_no == "") {
                    $("#txtMblNo").css('border-color', 'red');
                    $("#Mbl_Err_Msg").show();
                    return false;
                } else {
                    $("#Mbl_Err_Msg").hide();
                }
                // For Email
                if (email == "") {
                    $("#txtEmail1").css('border-color', 'red');
                    $("#Emailid_Err_Msg").show();
                    return false;
                } else {
                    $("#Emailid_Err_Msg").hide();
                    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                    if (emailPattern.test(email) == false) {
                        $("#txtEmail1").css('border-color', 'red');
                        $.gritter.add({
                            title: "Notification",
                            image: "images/exclamation.png",
                            text: "Please Enter A Valid Email"
                        });
                        return false;
                    } else {
                    }
                }

                //for check that mobile no is valid or not
                if (isNaN(mbl_no)) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Mobile No."
                    });
                    return false;

                } else if (mbl_no.length != 10) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Mobile No."
                    });
                    return false;
                }
                if (isNaN(homephoeno)) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Home PHone No."
                    });
                    return false;
                }
                if (isNaN(pincode)) {
                    $.gritter.add({
                        title: "Notification",
                        text: "Please Enter a Valid Pin Code"
                    });
                    return false;
                }
                // for blood group
                blood_group = jQuery.trim(blood_group);
                if (blood_group == "SELECT") {
                    blood_group = "";
                }
                blood_group = encodeURIComponent(blood_group);
                // calling service
                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                $.ajax({
                    type: "GET",
                    url: "services/Approveuserandupdatedetail.aspx",
                    data: "e_name=" + employee_name + "&cpf_no=" + cpf_no + "&add1=" + add1 + "&add2=" + add2 + "&city=" + city + "&state=" + state + "&pin_no=" + pincode + "&home_ph_no=" + homephoeno + "&mbl_no=" + mbl_no + "&gender=" + gender + "&dob=" + dob + "&email1=" + email + "&email2=" + email2 + "&email3=" + email3 + "&blood_group=" + blood_group + "&department=" + encodeURIComponent(department) + "&worklocation=" + worklocation + "&type=" + myhint + "&designation=" + encodeURIComponent(designation) + "&supervisior=" + supervisior + "&employee_ex_no=" + employee_ex_no,
                    success: function (msg) {
                        // alert(msg);
                        var values = msg.split("~");
                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                            $("#fancyurl").attr('href', "SetPrivilege.aspx?cpf_no=" + cpf_no);
                            if (confirm("Default privileges have been assigned to the user. Click Ok to add more privilege.")) {
                                $("#fancyurl").click();
                            } else {
                                window.location = self.location;
                            }
                            $('#save_privilage').live("click", function () {
                                var child_ids = '';
                                $.each($(".module_list"), function (index, value) {
                                    if ($(this).children('input').attr("checked")) {
                                        child_ids = child_ids + $(this).children('input').attr('id') + "`";
                                    }
                                });
                                $(this).attr("disabled", true);
                                $(this).val('Saving...');
                                $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please wait...</h1>' });
                                $.ajax({
                                    type: "GET",
                                    url: "services/save_pri_to_emp.aspx",
                                    data: "cpf_no=" + cpf_no + "&child_ids=" + child_ids,
                                    success: function (msg) {
                                        var values = msg.split("~");
                                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                            $.gritter.add({
                                                title: "Notification",
                                                image: "images/tick.png",
                                                text: values[2]
                                            });
                                            $('#save_privilage').attr("disabled", false);
                                            $('#save_privilage').val('Save');
                                            if (myhint != "UPDATE") {
                                                alert(values[2]);
                                                window.location = self.location;
                                            }
                                        }
                                    }
                                });
                            });
                        } else if (values[0] == "FALSE" && values[1] == "SUCCESS") {
                            $.gritter.add({
                                title: "Notification",
                                image: "images/exclamation.png",
                                text: values[2]
                            });
                        }
                    }
                });
            });

            $("#cmd_cancel").live('click', function () {
                //if (myhint != "UPDATE") {
                window.location = self.location;
                //}
                $.fancybox.close();
            });
            //reset_pwd
            $("#reset_pwd").click(function () {
                var cpf_no = $("#txtEmpCPFNo").val();
                if (cpf_no == "") {
                    alert("Please Enter CPF No.");
                } else {
                    if (confirm("Are you sure want to reset password of selected user ?")) {
                        $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please Wait...</h1>' });
                        $.ajax({
                            type: "GET",
                            url: "services/deleteuserandresetpwd.aspx",
                            data: "cpf_no=" + cpf_no + "&type=RESET_PWD",
                            success: function (msg) {
                                var values = msg.split("~");
                                if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/tick.png",
                                        text: values[2]
                                    });
                                } else {
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/cross.png",
                                        text: values[2]
                                    });
                                }
                            }
                        });
                    }
                }
            });
            //delete_user
            $("#delete_user").click(function () {
                var sActionHint = (myhint5 == 'PENDING') ? "REJECTED" : "DELETE";
                var txtEmpName = $("#txtEmpName").val();
                if (confirm("Are you sure want to reject selected user :" + txtEmpName + "?")) {
                    var cpf_no = $("#txtEmpCPFNo").val();
                    if (cpf_no == "") {
                        alert("Please Enter CPF No.");
                    } else {
                        $.blockUI({ message: '<h1><img src="images/busy.gif" /> Please Wait...</h1>' });
                        $.ajax({
                            type: "GET",
                            url: "services/deleteuserandresetpwd.aspx",
                            data: "cpf_no=" + cpf_no + "&type=" + sActionHint,
                            success: function (msg) {
                                var values = msg.split("~");
                                if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/tick.png",
                                        text: values[2]
                                    });
                                    alert(values[2]);
                                    window.location = self.location;
                                } else {
                                    $.gritter.add({
                                        title: "Notification",
                                        image: "images/cross.png",
                                        text: values[2]
                                    });
                                }
                            }
                        });
                    }
                }
            });
            var CitySearch_options, CitySearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: CitySearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'CitySearch', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                CitySearch_a = $("#txtCity").autocomplete(options);
            });
            var CitySearch_onAutocompleteSelect = function (CitySearch_value, CitySearch_data) {
                if (CitySearch_value != "Enter something else..") {
                    var data_arr = CitySearch_data.split("~");
                    $("#txtCity").val(data_arr[0]);
                    $("#txtState").val(data_arr[1]);
                }
            }

            var StateSearch_options, StateSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: StateSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'StateSearch', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                CitySearch_a = $("#txtState").autocomplete(options);
            });
            var StateSearch_onAutocompleteSelect = function (StateSearch_value, StateSearch_data) {
                if (StateSearch_value != "Enter something else..") {
                    $("#txtState").val(StateSearch_data);
                }
            }
        });
    </script>

</head>
<body bgcolor="#BCC7D8">
    <form id="form1" runat="server" class="MainInterface-iframe">
        <div class="div-fullwidth iframeMainDiv">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid #009f3c;">
                <h1 class="heading" style="width: auto;" id="txtHeading">User Request Approval Form</h1>
                <asp:Label ID="Label2" runat="server" class="heading-content-right" Style="color: Red;"></asp:Label>
            </div>
            <div class="div-pagebottom"></div>
            <div id="div_for_approve" style="position: relative; float: left; left: 0px; padding: 2px 10px 2px 10px; width: 98%; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom: 10px;">
                <div style="position: relative; float: left;">
                    <input type="radio" name="my_hint" id="optPending" value="PENDING" style="float: left; margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="optPending" class="content" style="cursor: pointer; margin-right: 20px;">PENDING</label>
                    <input type="radio" name="my_hint" id="optRejected" value="REJECTED" style="float: left; margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="optRejected" class="content" style="cursor: pointer;">REJECTED</label>
                </div>
            </div>
            <asp:Label ID="my_module_hint" runat="server"></asp:Label>

            <div class="div-fullwidth marginbottom">
                <div style="position: relative; float: left; margin-left: 1px; width: 530px;">
                    <asp:Label ID="Label3" runat="server" Text="Select Employee" class=" content"></asp:Label>
                    <asp:DropDownList ID="ddlpendinguserlist" runat="server"></asp:DropDownList>
                    <asp:Button ID="cmdGo" runat="server" Text="Go" OnClientClick="return false;" class="g-button g-button-share" />

                    <div id="div_Edit">
                        <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 150px; float: left; margin-right: 10px;">
                            <asp:ListItem Selected="True">Select CPF No.</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="cmdUpdateGo" runat="server" Text="GO" OnClientClick="return false;" class="g-button g-button-share" Style="float: left;" />
                        <asp:Label ID="Label4" runat="server" Text="OR" Style="margin-left: 20px; margin-right: 20px;" class="content"></asp:Label>
                        <div>
                            <asp:TextBox ID="ashish" runat="server" class="autocompleteclass" Style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat; padding-left: 35px; width: 240px;">Filter Employee</asp:TextBox>
                        </div>
                    </div>
                </div>
                <div style="position: relative; float: right; margin-left: 1px; width: 40%">
                    <asp:Label ID="lbldate" class="content" runat="server" Text="Label" Style="color: Red; float: right; text-align: right;"></asp:Label>
                </div>
            </div>

            <div class="div-fullwidth" id="Table2" style="border: 1px solid #C4C4C4; border-radius: 10px 10px 0 0; margin-bottom: 20px;">
                <div class="div-fullwidth base" style="border-radius: 10px 10px 0 0;">
                    <h1 class="registration-content" style="width: 100%; margin-bottom: 20px; font-size: 16px;">Fields Marked with <span style="color: Red;">( * )</span> are Mandatory Fields</h1>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content" style="width: 165px;">Employee Name <span style="color: Red;">( * )</span> :</h1>
                        <asp:TextBox ID="txtEmpName" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>

                    </div>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">CPF No <span style="color: Red;">( * )</span> :</h1>
                        <asp:TextBox ID="txtEmpCPFNo" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>

                    </div>
                </div>
                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Designation :</h1>
                        <asp:DropDownList ID="ddlDesignation" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                        </asp:DropDownList>
                    </div>

                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Gender :</h1>
                        <asp:DropDownList ID="CmbGender" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">DOB <span style="color: Red;">( * )</span>:</h1>
                        <asp:TextBox ID="txt_dob" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>

                    </div>

                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Department :</h1>
                        <asp:DropDownList ID="ddlDepartment2" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                        </asp:DropDownList>
                        <%--<asp:textbox id="txtdepartment" runat="server" CssClass="flattxt" style="width: 61%;"></asp:textbox>--%>
                    </div>
                </div>
                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content" style="width: 165px;">Mobile No <span style="color: Red;">( * )</span> :</h1>
                        <asp:TextBox ID="txtMblNo" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>

                    </div>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Employee Ext. No. :</h1>
                        <asp:TextBox ID="txtemployeeexno" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>
                    </div>
                </div>

                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content" style="width: 165px;">ONGC Mail <span style="color: Red;">( * )</span> :</h1>
                        <asp:TextBox ID="txtEmail1" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>

                    </div>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Email ID 2 :</h1>
                        <asp:TextBox ID="txtEmail2" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>
                    </div>
                </div>
                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Blood Group :</h1>
                        <asp:DropDownList ID="cmbBldGrp" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                            <asp:ListItem Value="SELECT" Selected="True">Select Blood Group</asp:ListItem>
                            <asp:ListItem Value="A +ve">A +ve</asp:ListItem>
                            <asp:ListItem Value="A -ve">A -ve</asp:ListItem>
                            <asp:ListItem Value="AB +ve">AB +ve</asp:ListItem>
                            <asp:ListItem Value="AB -ve">AB -ve</asp:ListItem>
                            <asp:ListItem Value="B +ve">B +ve</asp:ListItem>
                            <asp:ListItem Value="B -ve">B -ve</asp:ListItem>
                            <asp:ListItem Value="O +ve">O +ve</asp:ListItem>
                            <asp:ListItem Value="O -ve">O -ve</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Work Location :</h1>
                        <asp:DropDownList ID="ddlWorkLocation" runat="server" Width="208px" CssClass="flattxt" Style="width: 61%;">
                            <asp:ListItem Value="M">M</asp:ListItem>
                            <asp:ListItem Value="F">F</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Address Line 1 :</h1>
                        <asp:TextBox ID="txtAddr1" runat="server" CssClass="flattxt" TextMode="Multiline" Style="width: 60%;"></asp:TextBox>
                    </div>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Address Line 2 :</h1>
                        <asp:TextBox ID="txtAddr2" runat="server" CssClass="flattxt" TextMode="Multiline" Style="width: 60%;"></asp:TextBox>
                    </div>
                </div>
                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">City :</h1>
                        <asp:TextBox ID="txtCity" runat="server" CssClass="flattxt" class="autocompleteclass" Style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat; padding-left: 35px; width: 61%;"></asp:TextBox>
                    </div>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">State :</h1>
                        <asp:TextBox ID="txtState" runat="server" CssClass="flattxt" class="autocompleteclass" Style="background-image: url('../images/search-icon.jpg'); background-repeat: no-repeat; padding-left: 35px; width: 61%;"></asp:TextBox>
                    </div>
                </div>
                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Pin Code :</h1>
                        <asp:TextBox ID="txtPinCode" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>
                    </div>
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Office Phone No :</h1>
                        <asp:TextBox ID="txtHomeNo" runat="server" CssClass="flattxt" Style="width: 61%;"></asp:TextBox>
                    </div>
                </div>
                <div class="div-fullwidth base">
                    <div class="div-halfwidth">
                        <h1 class="userapproval-content">Supervisior Name :</h1>
                        <asp:DropDownList ID="ddlSupervisiorname" runat="server" CssClass="flattxt" Style="width: 61%;">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="div-fullwidth base" style="border-bottom: 1px solid #ccc;">
                </div>
                <div class="div-fullwidth base" style="margin-top: 10px; padding-bottom: 10px; padding-top: 0; border-top: 1px solid #ccc;">
                    <asp:Button ID="cmdsaveandassign" runat="server"
                        Text="Save And Assign Privilege" OnClientClick="return false;" class="buttonBigGreen" />
                    <a id="fancyurl" style="display: none" href="SetPrivilege.aspx"></a>
                    <input id="reset_pwd" type="button" value="Reset Password" class="buttonBigGreen" />
                    <input id="delete_user" type="button" value="Reject User Request" class="buttonBigRed" style="float: right;" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
