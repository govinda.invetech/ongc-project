﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects
{

    public class MyApplication1
    {
        #region Coding Tools Functions

        public String strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];

        public DateTime ConvertDateStringInDateTime(string StringDate)
        {
            string sFormat = System.Configuration.ConfigurationSettings.AppSettings["DateFormat"];
            string[] sArrFormat = sFormat.Split('~');
            DateTime myDate = DateTime.ParseExact(StringDate, sArrFormat, System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
            return myDate;
        }


        public string ConvertDateintoSQLDateString(DateTime dt)
        {
            string ss = dt.Month + "/" + dt.Day + "/" + dt.Year;
            return ss;
        }

        public string GetFormatedDateFromDateTime(DateTime dt)
        {
            string formated = dt.ToString("d MMM");
            return formated;
        }
        public string convertsqltoddmmyyyy(string dt)
        {
            string[] arr;

            if (dt.Contains("-"))
            {
                arr = dt.Split('-');
            }
            else
            {
                arr = dt.Split('/');
            }
            return arr[2] + "-" + arr[1] + "-" + arr[0];

        }

        public string convertdateforsql(string dt)
        {
            string[] arr;

            if (dt.Contains("-"))
            {
                arr = dt.Split('-');
            }
            else
            {
                arr = dt.Split('/');
            }
            return arr[2] + "-" + arr[0] + "-" + arr[1];

        }
        public string convertdateforsql1(string dt)
        {
            string[] arr;

            if (dt.Contains("-"))
            {
                arr = dt.Split('-');
            }
            else
            {
                arr = dt.Split('/');
            }
            return arr[2] + "-" + arr[1] + "-" + arr[0];

        }
        public string convertfordt(string dt)
        {
            string[] arr;

            if (dt.Contains("-"))
            {
                arr = dt.Split('-');
            }
            else
            {
                arr = dt.Split('/');
            }
            return arr[1] + "-" + arr[0] + "-" + arr[2];
        }
        public string convertdatetimeformattosql(string dt)
        {
            string datess = "";
            string[] arr;
            if (dt.Contains(" "))
            {
                arr = dt.Split(' ');
                datess = arr[0];
            }

            if (datess.Contains("-"))
            {
                arr = datess.Split('-');
            }
            else
            {
                arr = datess.Split('/');
            }
            return arr[2] + "-" + arr[1] + "-" + arr[0];
        }
        public string convertdtforsql2008(string dt)
        {
            string datess = "";
            string[] arr;
            if (dt.Contains(" "))
            {
                arr = dt.Split(' ');
                datess = arr[0];
            }

            if (datess.Contains("-"))
            {
                arr = datess.Split('-');
            }
            else
            {
                arr = datess.Split('/');
            }
            return arr[1] + "-" + arr[0] + "-" + arr[2];
        }

        public string ConvertDateStringintoSQLDateString(string dt)
        {
            string[] arr = null;
            String ss = "";
            if (dt.Contains("-"))   // Date format : DD-MM-YYYY i.e 24-11-2012
            {
                arr = dt.Split('-');
                ss = arr[2] + "/" + arr[1] + "/" + arr[0];
                //  ss = arr[0] + "/" + arr[1] + "/" + arr[2];   // for local testing!
            }
            else if (dt.Contains("/"))  // Date format : MM/DD/YYYY i.e 11/24/2012
            {
                arr = dt.Split('/');
                ss = arr[0] + "/" + arr[1] + "/" + arr[2];
            }
            return ss;
        }

        public string sDataStringTrim(short i, string sStr)
        {
            int K = 0;
            // ERROR: Not supported in C#: OnErrorStatement

            int X = 0;
            int Y = 0;
            string sTemp = "";
            sStr = "~" + sStr + "~";
            X = sStr.IndexOf("~");
            for (K = 1; K <= i; K++)
            {
                Y = sStr.IndexOf("~", X + 1);
                sTemp = sStr.Substring(X + 1, Y - X - 1);
                X = Y;
            }

            return sTemp;
        }

        public Boolean ExecuteSQLQuery(String sQuery)
        {
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand(sQuery);
            cmd.Connection = conn;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            return true;
        }


        public Boolean updateQuery(string sQuery)
        {
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand(sQuery);
            cmd.Connection = conn;
            conn.Open();
            if (cmd.ExecuteNonQuery() == 0)
            {
                conn.Close();
                return false;
            }
            else
            {
                conn.Close();
                return true;
            }
        }


        public static String strConn1 = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
        public static Boolean ExecuteSQLQuery_static(String sQuery1)
        {

            SqlConnection conn = new SqlConnection(strConn1);
            SqlCommand cmd = new SqlCommand(sQuery1);
            cmd.Connection = conn;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            return true;
        }

        public DataSet ExecuteSELECTQuery(String sQuery)
        {
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            da = new SqlDataAdapter(sQuery, conn);
            da.Fill(ds);
            return ds;
        }


        #endregion



        #region Database driven functions


        public string getAnswerFromCPFNo(string QueID, string CPFNo)
        {
            string output = "";
            string sStr = "SELECT [option_id] FROM [CY_EMP_ANS_RELATION] WHERE [cpf_no] = '" + CPFNo + "' AND [que_id] = '" + QueID + "'";
            DataSet ds = ExecuteSELECTQuery(sStr);
            if (ds.Tables[0].Rows.Count == 0)
            {
                output = "FALSE";
            }
            else
            {
                output = ds.Tables[0].Rows[0][0].ToString();
            }
            return output;
        }

        public string getTwoRecentClosedQuestion(string ConnString)
        {
            string output = "";
            SqlConnection conn = new SqlConnection(ConnString);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT TOP 2 [id],[start_date] FROM [CY_QUE_DTLS] WHERE [is_approved] = 'YES' AND [CY_QUE_DTLS].[end_date] < '" + ConvertDateintoSQLDateString(System.DateTime.Today) + "' ORDER BY [start_date] DESC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                output = "FALSE";
            }
            else
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string QueID = ds.Tables[0].Rows[i][0].ToString();
                    DateTime StartDate = System.Convert.ToDateTime(ds.Tables[0].Rows[i][1].ToString());
                    output = output + QueID + "`" + StartDate + "~";
                }
                output = output.Substring(0, output.Length - 1);
            }
            return output;
        }

        public string getTwoRecentOpenQuestion(string ConnString)
        {
            string output = "";
            SqlConnection conn = new SqlConnection(ConnString);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT TOP 2 [id],[start_date] FROM [CY_QUE_DTLS] WHERE [is_approved] = 'YES' AND [CY_QUE_DTLS].[end_date] > '" + ConvertDateintoSQLDateString(System.DateTime.Today) + "' ORDER BY [start_date] DESC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                output = "FALSE";
            }
            else
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string QueID = ds.Tables[0].Rows[i][0].ToString();
                    DateTime StartDate = System.Convert.ToDateTime(ds.Tables[0].Rows[i][1].ToString());
                    if (StartDate < System.DateTime.Today)
                    {
                        output = output + QueID + "`" + StartDate + "~";
                    }
                }
                if (output == "")
                {
                    output = "FALSE";
                }
                else
                {
                    output = output.Substring(0, output.Length - 1);
                }
            }
            return output;
        }

        public string getMenusFromMenuType(string MenuType, string ConnectionString)
        {
            string output = "";
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [ID],[MENU_TITLE],[TOOLTIP],[HREF],[IS_IMPORTANT] FROM [CY_LP_MENU_ITEM] WHERE [MENU_TYPE] = '" + MenuType + "' ORDER BY [ORDER_BY] ASC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                output = "FALSE";
            }
            else
            {
                string MenuListImp = "";
                string MenuListNormal = "";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string MenuID = ds.Tables[0].Rows[i][0].ToString();
                    string MenuTitle = ds.Tables[0].Rows[i][1].ToString();
                    string MenuTooltip = ds.Tables[0].Rows[i][2].ToString();
                    string MenuHref = ds.Tables[0].Rows[i][3].ToString();
                    string MenuIsImp = ds.Tables[0].Rows[i][4].ToString();


                    string sNewDivOpen = "<div style=\"position: relative; float: left; width: 100%;\" class=\"div_menu_item\">";
                    string sNewDivClose = "</div>";

                    switch (MenuType)
                    {
                        case "MAIN_MENU":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + sNewDivOpen + "<li class=\"menu-item\">";
                                MenuListImp = MenuListImp + "<a href=\"" + MenuHref + "\" target=\"_blank\" style=\"background-color: rgb(47, 209, 223); color: white; font-size: 16px;\">" + MenuTitle + "</a>";
                                MenuListImp = MenuListImp + "</li>" + sNewDivClose;
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + sNewDivOpen + "<li class=\"menu-item\"><a href=\"" + MenuHref + "\" target=\"_blank\">" + MenuTitle + "</a></li>" + sNewDivClose;
                            }
                            break;
                        case "NEWS_BULLET":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + "<li><img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\" /><a target=\"_blank\" href=\"" + MenuHref + "\">" + MenuTitle + "</a></li>";
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + "<li><img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\" /><a target=\"_blank\" href=\"" + MenuHref + "\">" + MenuTitle + "</a></li>";
                            }
                            break;
                        case "OTHER_LINK":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + sNewDivOpen + "<li><a target=\"_blank\" href=\"" + MenuHref + "\">" + MenuTitle + "</a></li>" + sNewDivClose;
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + sNewDivOpen + "<li><a target=\"_blank\" href=\"" + MenuHref + "\">" + MenuTitle + "</a></li>" + sNewDivClose;
                            }
                            break;
                        case "BOXES":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"tile1\">" + MenuTitle + "</a>";
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"tile1\">" + MenuTitle + "</a>";
                            }
                            break;
                    }
                }
                output = MenuListImp + MenuListNormal;
            }
            return output;
        }

        public string getMenusFromMenuTypeForAdminPanel(string MenuType, string ConnectionString)
        {
            string output = "";
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            string sStr = "SELECT [id],[menu_title],[tooltip],[href],[is_important],[order_by] FROM [cy_launch_page_menu] WHERE [menu_type] = '" + MenuType + "' ORDER BY [order_by] ASC";
            da = new SqlDataAdapter(sStr, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                output = "FALSE";
            }
            else
            {
                string MenuListImp = "";
                string MenuListNormal = "";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string MenuID = ds.Tables[0].Rows[i][0].ToString();
                    string MenuTitle = (i + 1) + ". " + ds.Tables[0].Rows[i][1].ToString();
                    string MenuTooltip = ds.Tables[0].Rows[i][2].ToString();
                    string MenuHref = ds.Tables[0].Rows[i][3].ToString();
                    string MenuIsImp = ds.Tables[0].Rows[i][4].ToString();
                    string MenuOrder = ds.Tables[0].Rows[i][5].ToString();

                    switch (MenuType)
                    {
                        case "MAIN_MENU":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + "<li class=\"menu-item\">";
                                MenuListImp = MenuListImp + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"menuitem\" style=\"background-color: rgb(47, 209, 223); color: white; font-size: 16px;\">" + MenuTitle + "</a>";
                                MenuListImp = MenuListImp + "<a href=\"javascript:void(0)\" MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item cdm_delete_menu\" title=\"Remove\"></a>";
                                MenuListImp = MenuListImp + "</li>";
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + "<li class=\"menu-item\">";
                                MenuListNormal = MenuListNormal + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"menuitem\">" + MenuTitle + "</a>";
                                MenuListNormal = MenuListNormal + "<a href=\"javascript:void(0)\" MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item cdm_delete_menu\" title=\"Remove\"></a>";
                                MenuListNormal = MenuListNormal + "</li>";
                            }
                            break;
                        case "NEWS_BULLET":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + "<li>";
                                MenuListImp = MenuListImp + "<img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\"/>";
                                MenuListImp = MenuListImp + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"newsitem\">" + MenuTitle + "</a>";
                                MenuListImp = MenuListImp + "<a href=\"javascript:void(0)\" MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item_small_whatsnews cdm_delete_menu\" title=\"Remove\"></a>";
                                MenuListImp = MenuListImp + "</li>";
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + "<li>";
                                MenuListNormal = MenuListNormal + "<img src=\"images/bullet.png\" style=\"width: 14px; height: 14px;\"/>";
                                MenuListNormal = MenuListNormal + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"newsitem\">" + MenuTitle + "</a>";
                                MenuListNormal = MenuListNormal + "<a href=\"javascript:void(0)\" MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item_small_whatsnews cdm_delete_menu\" title=\"Remove\"></a>";
                                MenuListNormal = MenuListNormal + "</li>";
                            }
                            break;
                        case "OTHER_LINK":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + "<li>";
                                MenuListImp = MenuListImp + "<img src=\"images/bullet.png\" />";
                                MenuListImp = MenuListImp + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"otherlistitems\">" + MenuTitle + "</a>";
                                MenuListImp = MenuListImp + "<a href=\"javascript:void(0)\" MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item_small cdm_delete_menu\" title=\"Remove\"></a>";
                                MenuListImp = MenuListImp + "</li>";
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + "<li>";
                                MenuListNormal = MenuListNormal + "<img src=\"images/bullet.png\" />";
                                MenuListNormal = MenuListNormal + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"otherlistitems\">" + MenuTitle + "</a>";
                                MenuListNormal = MenuListNormal + "<a href=\"javascript:void(0)\" MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item_small cdm_delete_menu\" title=\"Remove\"></a>";
                                MenuListNormal = MenuListNormal + "</li>";
                            }
                            break;
                        case "BOXES":
                            if (MenuIsImp == "YES")
                            {
                                MenuListImp = MenuListImp + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"tile1\">" + MenuTitle;
                                MenuListImp = MenuListImp + "<span MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item-tile cdm_delete_menu\" title=\"Remove\" id=\"cmddeletetile\"></span>";
                                MenuListImp = MenuListImp + "</a>";
                            }
                            else
                            {
                                MenuListNormal = MenuListNormal + "<a target=\"_blank\" href=\"" + MenuHref + "\" class=\"tile1\">" + MenuTitle;
                                MenuListNormal = MenuListNormal + "<span MenuID=\"" + MenuID + "\" MenuOrder=\"" + MenuOrder + "\" class=\"button-delete-item-tile cdm_delete_menu\" title=\"Remove\" id=\"cmddeletetile\"></span>";
                                MenuListNormal = MenuListNormal + "</a>";
                            }
                            break;
                    }
                }
                output = MenuListImp + MenuListNormal;
            }
            return output;
        }

        public Boolean InsertFeedback(string sCPFNo, string sRemark, string sRate)
        {
            string str;
            str = "INSERT INTO [cy_feedback] ([cpf_no],[remark],[rate]) VALUES";
            str = str + "('" + sCPFNo + "','" + sRemark + "','" + sRate + "')";
            return ExecuteSQLQuery(str);
        }

        public Boolean insertBulkData(DataTable dt, string sTableName)
        {
            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            Boolean IsInsert = false;

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConn, SqlBulkCopyOptions.KeepIdentity))
            {
                bulkCopy.DestinationTableName = sTableName;
                try
                {
                    foreach (var colName in dt.Columns)
                        bulkCopy.ColumnMappings.Add(colName.ToString(), colName.ToString());

                    bulkCopy.WriteToServer(dt);
                    IsInsert = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }

            return IsInsert;
        }

        public Boolean DeleteMenuItemofLAunchPage(int iMenuId, int iOrder)
        {
            Boolean bb = false;

            string str = "DELETE FROM [cy_launch_page_menu] WHERE [id]='" + iMenuId + "'";
            bb = ExecuteSQLQuery(str);
            str = "UPDATE [cy_launch_page_menu] SET [order_by]=[order_by]-1";
            str = str + "WHERE [order_by]>'" + iOrder + "' AND [menu_type] ='MAIN_MENU';";
            bb = ExecuteSQLQuery(str);
            return bb;
        }

        public Boolean InsertThoughtoftheDay(string sCPFNo, string sThoughtText, String sDate)
        {
            string str;
            str = "DELETE FROM [CY_LP_TOD] WHERE [publish_date]='" + sDate + "'";
            ExecuteSQLQuery(str);


            return ExecuteSQLQuery(str);
        }

        public String GetThoughtoftheDay(DateTime sdate)
        {
            string str;
            str = "SELECT [thought_text] FROM [CY_LP_TOD] WHERE [publish_date]='" + ConvertDateintoSQLDateString(sdate) + "'";
            DataSet ds;
            ds = ExecuteSELECTQuery(str);
            if (ds.Tables[0].Rows.Count == 0)
            {
                DataSet ds1;
                str = "SELECT TOP 1 [thought_text] FROM [CY_LP_TOD] WHERE [publish_date]<'" + ConvertDateintoSQLDateString(sdate) + "' ORDER BY [publish_date] DESC";
                ds1 = ExecuteSELECTQuery(str);
                if (ds1.Tables[0].Rows.Count == 0)
                {
                    return "Default Thought of the Day Text";
                }
                else
                {
                    return ds1.Tables[0].Rows[0][0].ToString();
                }
            }

            else
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
        }

        public String GetCountOfContactInCategory(string CategoryID)
        {
            string str;
            str = "SELECT COUNT([id])FROM [CY_CONTACT_CATEGORY_RELATION] WHERE [category_id] = '" + CategoryID + "'";
            DataSet ds;
            ds = ExecuteSELECTQuery(str);
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public String GetEmployeeNameFromCPFNo(string sCPFNo)
        {
            string str;
            str = "SELECT [O_EMP_NM] FROM [O_EMP_MSTR] WHERE [O_CPF_NMBR] = '" + sCPFNo + "'";
            DataSet ds;
            ds = ExecuteSELECTQuery(str);
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public Boolean InsertPageHit(string sCPFNo, string sPageName)
        {
            string str;
            str = "INSERT INTO [CY_PAGE_HIT] ([page_name] ,[cpf_no]) VALUES ('" + sPageName + "' ,'" + sCPFNo + "')";
            return ExecuteSQLQuery(str);
        }

        public DateTime EmpLastLogin(string sCPFNo)
        {
            //23/11/2012, 11:27 AM
            DateTime dtLastLogin;
            string sQuery = "SELECT TOP 2 [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [USER_ID] = '" + sCPFNo + "' AND [TYPE] = 'SUCCESS' ORDER BY [TIMESTAMP] DESC";
            DataSet dsLastLogin = ExecuteSELECTQuery(sQuery);
            if (dsLastLogin.Tables[0].Rows.Count == 1)
            {
                dtLastLogin = System.Convert.ToDateTime(dsLastLogin.Tables[0].Rows[0][0]);
            }
            else
            {
                dtLastLogin = System.Convert.ToDateTime(dsLastLogin.Tables[0].Rows[1][0]);
            }

            return dtLastLogin;
        }

        public Boolean InsUpdEmpAccountStatus(string sCPFNo, string sStatus)
        {
            string sQueryString = "";
            switch (sStatus)
            {
                case "DEACTIVE":
                    sQueryString = "UPDATE [E_USER_MSTR] SET [CY_STATUS] = '" + sStatus + "' WHERE [E_USER_CODE] = '" + sCPFNo + "'";
                    break;
                case "REQUEST":
                    sQueryString = "UPDATE [E_USER_MSTR] SET [CY_STATUS] = '" + sStatus + "', [CY_REQUEST_TIMESTAMP] = CURRENT_TIMESTAMP WHERE [E_USER_CODE] = '" + sCPFNo + "'";
                    break;
                case "REJECT":
                    sQueryString = "UPDATE [E_USER_MSTR] SET [CY_STATUS] = '" + sStatus + "', [CY_REPONSE_TIMESTAMP] = CURRENT_TIMESTAMP WHERE [E_USER_CODE] = '" + sCPFNo + "'";
                    break;
                case "ADMINDEACTIVE":
                    sQueryString = "UPDATE [E_USER_MSTR] SET [CY_STATUS] = '" + sStatus + "' WHERE [E_USER_CODE] = '" + sCPFNo + "'";
                    break;
                case "ADMINACTIVE":
                    sQueryString = "UPDATE [E_USER_MSTR] SET [CY_STATUS] = '" + sStatus + "' WHERE [E_USER_CODE] = '" + sCPFNo + "'";
                    break;
                case "ACTIVE":
                    sQueryString = "UPDATE [E_USER_MSTR] SET [CY_STATUS] = '" + sStatus + "' WHERE [E_USER_CODE] = '" + sCPFNo + "'";
                    break;
            }

            try
            {
                if (ExecuteSQLQuery(sQueryString) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string sAccountStatus(string sCPFNo)
        {
            string sQuery = "SELECT [E_USER_CODE],[CY_STATUS],[CY_REQUEST_TIMESTAMP],[CY_REPONSE_TIMESTAMP] FROM [E_USER_MSTR] WHERE [E_USER_CODE] = '" + sCPFNo + "'";
            DataSet dsStatus = ExecuteSELECTQuery(sQuery);
            string output = "";
            if (dsStatus.Tables[0].Rows.Count == 0)
            {
                InsUpdEmpAccountStatus(sCPFNo, "DEACTIVE");
                output = "DEACTIVE";
            }
            else
            {
                output = dsStatus.Tables[0].Rows[0][1].ToString();
            }
            return output;
        }

        public string getUserLoginCount(string CPFNo)
        {
            string sQuery = "SELECT COUNT([ID]) FROM [CY_LOGIN_LOGS] WHERE [TYPE] = 'SUCCESS' AND [USER_ID] = '" + CPFNo + "'";
            DataSet dsStatus = ExecuteSELECTQuery(sQuery);
            string output = dsStatus.Tables[0].Rows[0][0].ToString();
            return output;
        }

        public string getUserLastLogin(string CPFNo)
        {
            string sLastLogin = "";
            string sQuery = "SELECT TOP 1 [TIMESTAMP] FROM [CY_LOGIN_LOGS] WHERE [USER_ID] = '" + CPFNo + "' AND [TYPE] = 'SUCCESS' ORDER BY [TIMESTAMP] DESC";
            DataSet dsLastLogin = ExecuteSELECTQuery(sQuery);
            if (dsLastLogin.Tables[0].Rows.Count != 0)
            {
                sLastLogin = System.Convert.ToDateTime(dsLastLogin.Tables[0].Rows[0][0]).ToString("dd-MM-yyyy HH:mm");
            }

            return sLastLogin;

        }

        #region Notification Functions and HTML Output

        public string ExecuteSELECTQueryAndGetFirstParameter(String sQuery)
        {
            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            da = new SqlDataAdapter(sQuery, conn);
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }

        }

        public string getPagePathFromModuleID(int iModuleID)
        {
            string str = "SELECT [child_code] FROM [CY_CHILD_MENU] WHERE [id]='" + iModuleID + "'";
            DataSet da_fist = ExecuteSELECTQuery(str);
            if (da_fist.Tables[0].Rows.Count == 0)
            {
                return "ERROR";
            }
            else
            {
                return da_fist.Tables[0].Rows[0][0].ToString();
            }

        }

        // function User Registration
        public string getTotalPendingUserRequest(String sCPFNo)
        {
            //  string str = "SELECT [cpf_number] FROM [CY_MENU_EMP_RELATION] where [cpf_number]='" + sCPFNo + "' and [child_id]='54'";
            string str = "SELECT [cpf_number] FROM [CY_MENU_EMP_RELATION] where [cpf_number]='" + sCPFNo + "'and child_id='54'";
            DataSet da_fist = ExecuteSELECTQuery(str);
            if (da_fist.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                string str1 = "SELECT COUNT([O_CPF_NMBR]) FROM [CY_O_EMP_MSTR] WHERE [O_CPF_NMBR]!='' AND [cy_user_status] = 'PENDING'";
                string total_count = ExecuteSELECTQueryAndGetFirstParameter(str1);
                if (total_count == "FALSE")
                {
                    return "FALSE";
                }
                else
                {
                    if (total_count == "0")
                    {
                        return "FALSE";
                    }
                    else
                    {
                        return total_count + " User Registration Request Pending To Approve~54";
                    }
                }
            }
        }

        public string getTotalPendingFeedback(String sCPFNo)
        {
            string str = "SELECT COUNT([ID]) FROM cy_feedback WHERE [cpf_no]!='' AND [status] = 0 ";
            string total_count = ExecuteSELECTQueryAndGetFirstParameter(str);
            if (total_count == "FALSE")
            {
                return "FALSE";
            }
            else
            {
                if (total_count == "0")
                {
                    return "FALSE";
                }
                else
                {
                    return total_count + " Get Feeback Request Pending To Approve~60";
                }
            }
        }

        // function User Activation
        public string getTotalActivationRequest(String sCPFNo)
        {
            string str = "SELECT [cpf_number] FROM [CY_MENU_EMP_RELATION] where [cpf_number]='" + sCPFNo + "' and [child_id]='80'";
            DataSet da_fist = ExecuteSELECTQuery(str);
            if (da_fist.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {

                string str1 = "SELECT COUNT([E_USER_MSTR].[E_USER_CODE]) FROM [O_EMP_MSTR],[E_USER_MSTR] WHERE [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE] AND [E_USER_MSTR].[CY_STATUS] = 'REQUEST'";

                string total_count = ExecuteSELECTQueryAndGetFirstParameter(str1);
                if (total_count == "FALSE")
                {
                    return "FALSE";
                }
                else
                {
                    if (total_count == "0")
                    {
                        return "FALSE";
                    }
                    else
                    {
                        if (Convert.ToInt32(total_count) == 1)
                        {
                            string str2 = "SELECT [O_EMP_MSTR].[O_EMP_NM],[O_EMP_MSTR].[O_CPF_NMBR] FROM [O_EMP_MSTR],[E_USER_MSTR] WHERE [O_EMP_MSTR].[O_CPF_NMBR] = [E_USER_MSTR].[E_USER_CODE] AND [E_USER_MSTR].[CY_STATUS] = 'REQUEST'";
                            DataSet ds = ExecuteSELECTQuery(str2);
                            return total_count + "  User Activation Request from " + ds.Tables[0].Rows[0][0].ToString() + " (" + ds.Tables[0].Rows[0][1].ToString() + ")~80";

                        }
                        else
                            return total_count + " User Activation Request~80";

                        // return total_count + " User Activation Request~80";
                    }
                }
            }
        }

        // function Gate Pass
        public string getTotalPendingRequestofGetPass(string cpf_no)
        {
            string str = "Select COUNT(DISTINCT([CY_GROUP])) from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_ACP1_CPF_NMBR = '" + cpf_no + "' and O_PASS_TYPE_FLG = 'V' and O_REQ_COMPLTN_FLG = 'A' and CY_GROUP <>''";
            string total_count = ExecuteSELECTQueryAndGetFirstParameter(str);
            if (total_count == "FALSE")
            {
                return "FALSE";
            }
            else
            {
                if (total_count == "0")
                {
                    return "FALSE";
                }
                else
                {
                    return total_count + " Gate Pass Request Pending To Approve~18";
                }
            }

        }

        // function Transport Request
        public string getTotalPendingTransportRequest(String sCPFNo)
        {
            string str = "SELECT [cpf_number] FROM [CY_MENU_EMP_RELATION] where [cpf_number]='" + sCPFNo + "'  ";
            //  string str = "SELECT [cpf_number] FROM [CY_MENU_EMP_RELATION] where [cpf_number]='" + sCPFNo + "' and [child_id]='15'";
            DataSet da_fist = ExecuteSELECTQuery(str);
            if (da_fist.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                int AllTransportCount = 0;
                string str1 = "SELECT COUNT([id]) FROM [CY_TRANSPORT_BOOKING_DETAIL] WHERE [INCHARGE_CPF_NO] = '" + sCPFNo + "' AND [INCHARGE_STATUS] = 'PENDING' AND CAST([BOOKING_START_DATETIME] AS DATE) >= CAST(GETDATE() AS DATE)";

                string total_count = ExecuteSELECTQueryAndGetFirstParameter(str1);
                if (total_count == "FALSE")
                {
                    total_count = "FALSE";
                }
                else
                {
                    if (total_count == "0")
                    {
                        total_count = "FALSE";
                    }
                    else
                    {
                        AllTransportCount = Convert.ToInt32(total_count);
                    }
                }

                str1 = "SELECT COUNT([id]) FROM [CY_TRANSPORT_BOOKING_DETAIL] WHERE [APPROVER_CPF_NO] = '" + sCPFNo + "' AND [APPROVER_STATUS] = 'PENDING' AND [INCHARGE_STATUS] = 'APPROVE' AND CAST([BOOKING_START_DATETIME] AS DATE) >= CAST(GETDATE() AS DATE)";

                total_count = ExecuteSELECTQueryAndGetFirstParameter(str1);
                if (total_count == "FALSE")
                {
                    total_count = "FALSE";
                }
                else
                {
                    if (total_count == "0")
                    {
                        total_count = "FALSE";
                    }
                    else
                    {
                        AllTransportCount = AllTransportCount + Convert.ToInt32(total_count);
                    }
                }

                str = "SELECT [cpf_number] FROM [CY_MENU_EMP_RELATION] where [cpf_number]='" + sCPFNo + "' and [child_id]='75'";
                da_fist = ExecuteSELECTQuery(str);
                if (da_fist.Tables[0].Rows.Count == 0)
                {
                    total_count = "FALSE";
                }
                else
                {
                    str1 = "SELECT COUNT([id]) FROM [CY_TRANSPORT_BOOKING_DETAIL] WHERE [ALLOCATER_CPF_NO] = '" + sCPFNo + "' AND [ALLOCATTER_STATUS] = 'PENDING' AND [INCHARGE_STATUS] = 'APPROVE' AND CAST([BOOKING_START_DATETIME] AS DATE) >= CAST(GETDATE() AS DATE)";

                    total_count = ExecuteSELECTQueryAndGetFirstParameter(str1);
                    if (total_count == "FALSE")
                    {
                        total_count = "FALSE";
                    }
                    else
                    {
                        if (total_count == "0")
                        {
                            total_count = "FALSE";
                        }
                        else
                        {
                            AllTransportCount = AllTransportCount + Convert.ToInt32(total_count);
                        }
                    }
                }
                if (AllTransportCount == 0)
                {
                    return "FALSE";
                }
                else
                {
                    return AllTransportCount + " Transport Booking To Approve~15";
                }
            }
        }

        // function Meeting
        public string getTotalPendingRequestofMeeting(string cpf_no)
        {
            var todaydate = DateTime.Now.ToShortDateString();
            todaydate = DateTime.Now.ToString("MM/dd/yyyy");
            string str = "SELECT O_TC_MTNG_MBR_DTL. O_MTNG_NO ,O_TC_MTNG_MBR_DTL.MEMBER_TASK, O_TC_MTNG_DTL.O_DLNG_MEMBR_NM  ,O_TC_MTNG_DTL.O_VENUE ,O_TC_MTNG_DTL.O_MTNG_DT ,O_TC_MTNG_DTL.O_MTNG_TM ,O_TC_MTNG_DTL.O_MTNG_DTLS,O_TC_MTNG_DTL.CY_MTNG_TITLE FROM O_TC_MTNG_MBR_DTL ,O_TC_MTNG_DTL  where O_TC_MTNG_MBR_DTL.O_MTNG_MBR_CPF_NO='" + cpf_no + "' and  O_TC_MTNG_DTL.O_INDX_NMBR=O_TC_MTNG_MBR_DTL.O_MTNG_NO and O_TC_MTNG_DTL.O_MTNG_DT>='" + todaydate + "' ORDER BY O_TC_MTNG_DTL.O_MTNG_DT ,O_TC_MTNG_DTL.O_MTNG_TM";
            DataSet dsnnew = ExecuteSELECTQuery(str);
            if (dsnnew.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else if (dsnnew.Tables[0].Rows.Count == 1)
            {
                return "1 Meeting at " + dsnnew.Tables[0].Rows[0][5].ToString() + " on " + dsnnew.Tables[0].Rows[0][4].ToString() + " In " + dsnnew.Tables[0].Rows[0][3].ToString() + "~26";
            }
            else if (dsnnew.Tables[0].Rows.Count == 2)
            {
                return "2 Upcoming  Meeting on " + dsnnew.Tables[0].Rows[0][4].ToString() + " And  " + dsnnew.Tables[0].Rows[1][4].ToString() + "~26";
            }
            else
            {
                int abc_count = dsnnew.Tables[0].Rows.Count;
                string my_thory = abc_count + " Upcomming Meeting on ";
                for (int i = 0; i < dsnnew.Tables[0].Rows.Count - 1; i++)
                {
                    string meetingdate = dsnnew.Tables[0].Rows[i][4].ToString();
                    string meetingtime = dsnnew.Tables[0].Rows[i][5].ToString();
                    my_thory = my_thory + meetingdate + " , ";
                    abc_count = i;
                }

                my_thory = my_thory.Substring(0, my_thory.Length - 1);
                my_thory = my_thory + " And " + dsnnew.Tables[0].Rows[abc_count + 1][4].ToString();
                return my_thory + "~26";
            }

        }

        // function VOX Populi
        public string getTotalPendingVOXPopuliQuestion(string cpf_no)
        {
            string output = "";
            DateTime todaydate = DateTime.Today;

            //string sQuery = "SELECT [CY_QUE_DTLS].[id], [CY_QUE_DTLS].[start_date] FROM [CY_QUE_DTLS] WHERE [CY_QUE_DTLS].[start_date] <= '" + ConvertDateintoSQLDateString(todaydate) + "' AND [CY_QUE_DTLS].[end_date] >= '" + ConvertDateintoSQLDateString(todaydate) + "'  AND [CY_QUE_DTLS].[is_approved] = 'YES'";
            string sQuery = "SELECT [CY_QUE_DTLS].[id], [CY_QUE_DTLS].[start_date] FROM [CY_QUE_DTLS] WHERE [CY_QUE_DTLS].[start_date] <= GETDATE() AND [CY_QUE_DTLS].[end_date] >= GETDATE()  AND [CY_QUE_DTLS].[is_approved] = 'YES'";
            sQuery = sQuery + "AND NOT EXISTS (SELECT * FROM [CY_EMP_ANS_RELATION] WHERE [CY_EMP_ANS_RELATION].[que_id] = [CY_QUE_DTLS].[id] AND [cpf_no]='" + cpf_no + "')";

            DataSet ds = ExecuteSELECTQuery(sQuery);
            int iCount = ds.Tables[0].Rows.Count;
            if (iCount == 0)
            {
                output = "FALSE";
            }
            else if (iCount == 1)
            {
                DateTime sDate = System.Convert.ToDateTime(ds.Tables[0].Rows[0][1]);
                output = iCount + " Question of " + GetFormatedDateFromDateTime(sDate) + " is pending to answer~57";
            }
            else if (iCount > 1)
            {
                output = iCount + " Questions Pending to answer~57";
            }
            return output;
        }
        public string getnotification(string sNotification)
        {
            string c = sNotification;
            string output = "<li class=\"notification-li\">";
            output = output + "<a href=\"complaintReg.aspx\" target=\"_blank\" class=\"notification-a\">";  //ONGCMenu            

            output = output + "<p class=\"notification-p\">" + c + "</p>";

            output = output + "</a></li>";
            return output;

        }

        public string sGetNotificationLi(string sNotificationStr)
        {
            string a;
            string b;
            a = sDataStringTrim(1, sNotificationStr);
            b = sDataStringTrim(2, sNotificationStr);
            string sOutput = "<li class=\"notification-li\">";
            if (b == "57")
            {
                sOutput = sOutput + "<a href=\"launchpage.aspx\" class=\"notification-a\">";
            }
            else
            {
                sOutput = sOutput + "<a href=\"ONGCMenu.aspx?p_id=" + b + "\" target=\"_blank\" class=\"notification-a\">";
            }
            //sOutput = sOutput + "<img src=\"images/bullet.png\" class=\"notification-arrow-img\">";
            sOutput = sOutput + "<p class=\"notification-p\">" + a + "</p>";
            sOutput = sOutput + "</a></li>";
            return sOutput;
        }
        public string managernotification(string sNotificationStr)
        {
            string a;
            string b;
            a = sDataStringTrim(1, sNotificationStr);
            b = sDataStringTrim(2, sNotificationStr);
            string sOutput = "<li class=\"notification-li\">";
            if (b == "57")
            {
                sOutput = sOutput + "<a href=\"launchpage.aspx\" class=\"notification-a\">";
            }
            else
            {
                sOutput = sOutput + "<a href=\"complaintReg1.aspx\" target=\"_blank\" class=\"notification-a\">";//ONGCMenu.aspx?p_id=" + b + "
            }
            //sOutput = sOutput + "<img src=\"images/bullet.png\" class=\"notification-arrow-img\">";
            sOutput = sOutput + "<p class=\"notification-p\">" + a + "</p>";
            sOutput = sOutput + "</a></li>";
            return sOutput;
        }

        public string getNotificationHtml(string sCPFNo, string Departments)
        {

            string sOutput = "";
            int iNotificationCount = 0;
            //string totalAssigncomplint = getonlyAssignstatusofuser(sCPFNo);
            //if (totalAssigncomplint != "FALSE")
            //{
            //    iNotificationCount = iNotificationCount + 1;
            //    sOutput = sOutput + getnotification(totalAssigncomplint);
            //}
            string totalsolvecomplint = getonlysolvedstatusofuser(sCPFNo);
            if (totalsolvecomplint != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + getnotification(totalsolvecomplint);
            }
            //string totalworkinprogresscomplint = getonlyWorkin_progressstatusofuser(sCPFNo);
            //if (totalsolvecomplint != "FALSE")
            //{
            //    iNotificationCount = iNotificationCount + 1;
            //    sOutput = sOutput + getnotification(totalworkinprogresscomplint);
            //}
            //string totalsolveandpendingcomplint = getTotalSolvedUnsolvedStatusofUser(sCPFNo);
            //if (totalsolveandpendingcomplint != "FALSE")
            //{
            //    iNotificationCount = iNotificationCount + 1;
            //    sOutput = sOutput + getnotification(totalsolveandpendingcomplint);
            //}
            string totalpendingcomplaints = getTotalPendingcomplain(sCPFNo, Departments);
            if (totalpendingcomplaints != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + managernotification(totalpendingcomplaints);
            }
            string sTotalPendingUserRegistion = getTotalPendingUserRequest(sCPFNo);
            if (sTotalPendingUserRegistion != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + sGetNotificationLi(sTotalPendingUserRegistion);
            }

            string sTotalTransportBooking = getTotalPendingTransportRequest(sCPFNo);
            if (sTotalTransportBooking != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + sGetNotificationLi(sTotalTransportBooking);
            }

            string sTotalPendingGatePass = getTotalPendingRequestofGetPass(sCPFNo);
            if (sTotalPendingGatePass != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + sGetNotificationLi(sTotalPendingGatePass);
            }

            /*--Feeback back for notification--*/
            string sTotalPendingPendingFeedback = getTotalPendingFeedback(sCPFNo);
            if (sTotalPendingPendingFeedback != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + sGetNotificationLi(sTotalPendingPendingFeedback);
            }

            string sTotalPendingMeeting = getTotalPendingRequestofMeeting(sCPFNo);
            if (sTotalPendingMeeting != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + sGetNotificationLi(sTotalPendingMeeting);
            }

            string sTotalPendingActivation = getTotalActivationRequest(sCPFNo);
            if (sTotalPendingActivation != "FALSE")
            {
                iNotificationCount = iNotificationCount + 1;
                sOutput = sOutput + sGetNotificationLi(sTotalPendingActivation);
            }

            string sTotalPendingVOXPopuli = getTotalPendingVOXPopuliQuestion(sCPFNo);
            if (sTotalPendingVOXPopuli != "FALSE")
            {
                if (sCPFNo != "admin")
                {
                    iNotificationCount = iNotificationCount + 1;
                    sOutput = sOutput + sGetNotificationLi(sTotalPendingVOXPopuli);
                }
            }

            if (iNotificationCount == 0)
            {
                sOutput = "No Notification for you";
            }

            return iNotificationCount + "~" + sOutput;
        }

        #endregion

        #region validatecpf_no
        public string validatecpf_no(string cpf_no)
        {

            string sStr = "SELECT [O_CPF_NMBR] FROM [O_EMP_MSTR] where [O_CPF_NMBR]='" + cpf_no + "'";
            DataSet ds = ExecuteSELECTQuery(sStr);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";

            }
            else
            {
                return "TRUE";
            }
        }
        #endregion

        public string getTotalSolvedUnsolvedStatusofUser(string cpf_no)
        {
            string str = "SELECT COUNT([ID])  FROM [CY_COMPLAIN_REGISTER_DETAIL] where  [APP_CPF_NO]='" + cpf_no + "' and  [notification_status]='NO'";
            string total_count = ExecuteSELECTQueryAndGetFirstParameter(str);
            if (total_count == "FALSE")
            {
                return "FALSE";
            }
            else
            {
                if (total_count == "0")
                {
                    return "FALSE";
                }
                else
                {
                    return " Response of " + total_count + " Complaints intiated by You";
                }
            }

        }
        public string getonlysolvedstatusofuser(string cpf_no)
        {
            string query = "";
            query = "select COUNT([ID]) from CY_COMPLAIN_REGISTER_DETAIL where [APP_CPF_NO]='" + cpf_no + "' and [VIEWED_RESPONSE]='RESOLVED' and notification_status='YES1'";
            string total = ExecuteSELECTQueryAndGetFirstParameter(query);
            if (total == "FALSE")
            {
                return "FALSE";
            }
            else
            {
                if (total == "0")
                {
                    return "FALSE";
                }
                else
                {
                    return " Your " + total + " Complaint has been resolved";
                }
            }

        }

        public string getonlyAssignstatusofuser(string cpf_no)
        {
            string query = "";
            query = "select COUNT([ID]) from CY_COMPLAIN_REGISTER_DETAIL where [APP_CPF_NO]='" + cpf_no + "' and [VIEWED_RESPONSE]='Assign'";
            string total = ExecuteSELECTQueryAndGetFirstParameter(query);
            if (total == "FALSE")
            {
                return "FALSE";
            }
            else
            {
                if (total == "0")
                {
                    return "FALSE";
                }
                else
                {
                    return " Your " + total + " Complaint hasbeen Assigned";
                }
            }

        }
        public string getonlyWorkin_progressstatusofuser(string cpf_no)
        {
            string query = "";
            query = "select COUNT([ID]) from CY_COMPLAIN_REGISTER_DETAIL where [APP_CPF_NO]='" + cpf_no + "' and [VIEWED_RESPONSE]='Work in progress'";
            string total = ExecuteSELECTQueryAndGetFirstParameter(query);
            if (total == "FALSE")
            {
                return "FALSE";
            }
            else
            {
                if (total == "0")
                {
                    return "FALSE";
                }
                else
                {
                    return " Your " + total + " Complaint has been Work in progress";
                }
            }

        }

        public string getTotalPendingcomplain(string cpf_no, string Department)
        {
            string page_id = "";
            if (Department == "TELEPHONE")
            {
                page_id = "62";
            }
            else if (Department == "ELECTRICAL(AC)")
            {
                page_id = "63";
            }
            else if (Department == "CIVIL")
            {
                page_id = "64";
            }
            else if (Department == "HOUSEKEEPING")
            {
                page_id = "65";
            }
            else if (Department == "ELECTRICAL(MAINTENANCE)")
            {
                page_id = "77";
            }
            else if (Department == "OTHER")
            {
                page_id = "67";
            }
            else if (Department == "PAPaging")
            {
                page_id = "98";
            }
            else if (Department == "WalkieTalkie")
            {
                page_id = "99";
            }
            else
            {
                return "FALSE";
            }
            if (checkcpfhaveassignedmodule(cpf_no, page_id) == "FALSE")
            {
                return "FALSE";
            }
            string str = "SELECT COUNT([ID])  FROM [CY_COMPLAIN_REGISTER_DETAIL] where [COMPLAIN_DEPARTMENT]='" + Department + "' and [VIEWED_RESPONSE]='NEW' and notification_status='NO'";
            string total_count = ExecuteSELECTQueryAndGetFirstParameter(str);
            if (total_count == "FALSE")
            {
                return "FALSE";
            }
            else
            {
                if (total_count == "0")
                {
                    return "FALSE";
                }
                else
                {
                    return total_count + " Complaints registered in " + Department + " section~" + page_id + "";
                }
            }

        }

        public string checkcpfhaveassignedmodule(string cpf_no, string page_id)
        {
            string str = "SELECT [cpf_number] FROM [CY_MENU_EMP_RELATION] where [cpf_number]='" + cpf_no + "' and [child_id]='" + page_id + "'";
            DataSet ds = ExecuteSELECTQuery(str);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                return "TRUE";
            }

        }

        public string deleteUserPermanantly(string cpf_no)
        {
            string str1 = "Delete From O_EMP_MSTR where O_CPF_NMBR = '" + cpf_no + "'";
            string str2 = "Delete From E_USER_MSTR where E_USER_CODE = '" + cpf_no + "'";
            try
            {
                if (ExecuteSQLQuery(str1) == true && ExecuteSQLQuery(str2) == true)
                {
                    return "TRUE";
                }
                else
                {
                    return "FALSE";
                }
            }
            catch (Exception a)
            {
                return "FALSE";
            }

        }

        public string RejectUserRequest(string cpf_no)
        {
            string str1 = "UPDATE [CY_O_EMP_MSTR] SET [cy_user_status] = 'REJECTED' WHERE [O_CPF_NMBR] = '" + cpf_no + "'";
            try
            {
                if (ExecuteSQLQuery(str1) == true)
                {
                    return "TRUE";
                }
                else
                {
                    return "FALSE";
                }
            }
            catch (Exception a)
            {
                return "FALSE";
            }

        }

        public string DeleteUserRequest(string cpf_no)
        {
            string str1 = "DELETE FROM [CY_O_EMP_MSTR] WHERE [O_CPF_NMBR] = '" + cpf_no + "'";
            try
            {
                if (ExecuteSQLQuery(str1) == true)
                {
                    return "TRUE";
                }
                else
                {
                    return "FALSE";
                }
            }
            catch (Exception a)
            {
                return "FALSE";
            }

        }

        public string resetUserPassword(string cpf_no)
        {
            string str1 = "UPDATE [E_USER_MSTR] SET [E_USER_PSWRD] = '" + cpf_no + "' WHERE  E_USER_CODE ='" + cpf_no + "'";


            try
            {
                if (ExecuteSQLQuery(str1) == true)
                {
                    return "TRUE";

                }
                else
                {
                    return "FALSE";
                }
            }
            catch (Exception a)
            {

                return "FALSE";
            }

        }

        public string GetTotalContactCount()
        {
            string str1 = "SELECT COUNT([id]) FROM [CY_VIEW_CONTACTS]";
            DataSet ds = ExecuteSELECTQuery(str1);
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public DataSet RegisteredUsers()
        {
            // string sQuery = "SELECT ROW_NUMBER() OVER (ORDER BY [O_EMP_NM] ASC) AS 'SLNO',UPPER([O_EMP_NM]), [O_CPF_NMBR], UPPER([O_EMP_DESGNTN]) FROM [O_EMP_MSTR] ORDER BY [O_EMP_NM] ASC";
            string varname1 = "";
            varname1 = varname1 + "SELECT   ROW_NUMBER() OVER (ORDER BY [O_EMP_NM] ASC) AS 'SLNO', " + "\n";
            varname1 = varname1 + "[O_EMP_NM]  , " + "\n";
            varname1 = varname1 + "[O_CPF_NMBR],   " + "\n";
            varname1 = varname1 + "  [O_EMP_DESGNTN] ,LOGIN_COUNT " + "\n";
            varname1 = varname1 + "FROM CY_EMP_STATUS " + "\n";
            varname1 = varname1 + "WHERE CY_STATUS='ACTIVE' " + "\n";
            varname1 = varname1 + "ORDER BY [O_EMP_NM] ASC";
            //DataSet ds = ExecuteSELECTQuery(sQuery);
            DataSet ds = ExecuteSELECTQuery(varname1);

            return ds;
        }
        #endregion

        #region Incident Management
        public string getDesignationFromCPFNo(string CPFNo)
        {
            string str1 = "SELECT [O_EMP_DESGNTN] FROM [O_EMP_MSTR] WHERE [O_CPF_NMBR] = '" + CPFNo + "'";
            DataSet ds = ExecuteSELECTQuery(str1);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0][0].ToString();

            }
            else
            {
                return "";
            }

        }
        public string GetAreaManagerFromAreaAndLocation(string sArea, string sLocation)
        {
            string str1 = "SELECT [LOCATION_MANAGER] FROM [CY_INC_0_LOCATION_MSTR] WHERE [AREA_NAME] = '" + sArea + "' AND [LOCATION_NAME] = '" + sLocation + "'";
            DataSet ds = ExecuteSELECTQuery(str1);
            if (ds.Tables[0].Rows[0][0].ToString() == "")
            {
                return "";
            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString() + "~" + GetEmployeeNameFromCPFNo(ds.Tables[0].Rows[0][0].ToString());
            }
        }

        public Boolean UpdateIncidentStatus(string IncidentID, int Status)
        {
            string sUpdateQuery = "UPDATE [CY_INC_1_DETAILS] SET [CY_STATUS_LEVEL] = '" + Status + "' WHERE [O_INDX_NMBR] ='" + IncidentID + "'";
            return ExecuteSQLQuery(sUpdateQuery);
        }

        public string getStatusStringFromLevel(string Level)
        {

            string str1 = "SELECT[STATUS_MESSAGE] FROM [CY_INC_STATUS_MESSAGE] WHERE [STATUS]='" + Level + "'";
            DataSet ds = ExecuteSELECTQuery(str1);
            if (ds.Tables[0].Rows[0][0].ToString() == "")
            {
                return "";
            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }

            //string sOutput = "";
            //switch (Level)
            //{
            //    case "1":
            //        sOutput = "Reported";
            //        break;
            //    case "2":
            //        sOutput = "Reviewed and Forwarded to Incidence Manager";
            //        break;
            //    case "3":
            //        sOutput = "Task Assigned to FPR";
            //        break;
            //    case "4":
            //        sOutput = "Task Completed and back to Incidence Manager";
            //        break;
            //    case "5":
            //        sOutput = "Incidence Manager fwd to Accp Level 1";
            //        break;
            //    case "6":
            //        sOutput = "Incidence Manager fwd for Revised Action";
            //        break;
            //    case "7":
            //        sOutput = "Level 1 Accepted and Closed";
            //        break;
            //    case "8":
            //        sOutput = "Level 1 Accepted and Fwd to level 2";
            //        break;
            //    case "9":
            //        sOutput = "Level 1 fwd for Revised Action";
            //        break;
            //    case "10":
            //        sOutput = "Level 2 Accepted and Closed";
            //        break;
            //    case "11":
            //        sOutput = "Level 2 fwd for Revised Action";
            //        break;
            //}
            //return sOutput;
        }

        public Boolean UpdateActionStatus(string IncidentID, string NewStatus, string PreStatus)
        {
            string sUpdateQuery = "UPDATE [CY_INC_3_ACTION_DTL] SET [CY_ACTN_STATUS] = '" + NewStatus + "' WHERE [O_INCIDENT_NMBR] ='" + IncidentID + "' AND [CY_ACTN_STATUS] = '" + PreStatus + "'";
            return ExecuteSQLQuery(sUpdateQuery);
        }

        public string getStatusOfIncident(string IncidentID)
        {
            string sStr = "SELECT [CY_STATUS_LEVEL] FROM [CY_INC_1_DETAILS] WHERE [O_INDX_NMBR] = '" + IncidentID + "'";
            DataSet ds = ExecuteSELECTQuery(sStr);
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public Boolean CheckAndInsertRelation(string IncidentID, string CPFNo, string Level, string EntryBy)
        {
            string sStr = "SELECT [INCDNT_NO] ,[EMP_CPF_NO] ,[ENTRY_BY] FROM [CY_INC_EMP_RELATION] WHERE [INCDNT_NO] ='" + IncidentID + "' AND [EMP_CPF_NO] ='" + CPFNo + "' AND [LEVEL]='" + Level + "'";
            DataSet dsCheck = ExecuteSELECTQuery(sStr);
            if (dsCheck.Tables[0].Rows.Count == 0)
            {
                string sInsertRelation = "INSERT INTO [CY_INC_EMP_RELATION] ([INCDNT_NO], [EMP_CPF_NO], [LEVEL], [ENTRY_BY], [TIMESTAMP]) VALUES ";
                sInsertRelation = sInsertRelation + "('" + IncidentID + "','" + CPFNo + "','" + Level + "','" + EntryBy + "',getdate())";
                if (ExecuteSQLQuery(sInsertRelation) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Energy Conservation Management
        public string EC_GetAreaManagerFromAreaAndLocation(string sArea, string sLocation)
        {
            string str1 = "SELECT [LOCATION_MANAGER] FROM [CY_EC_0_LOCATION_MSTR] WHERE [AREA_NAME] = '" + sArea + "' AND [LOCATION_NAME] = '" + sLocation + "'";
            DataSet ds = ExecuteSELECTQuery(str1);
            if (ds.Tables[0].Rows.Count < 1)
            {
                return "";
            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString() + "~" + GetEmployeeNameFromCPFNo(ds.Tables[0].Rows[0][0].ToString());
            }
        }

        public Boolean EC_UpdateIncidentStatus(string IncidentID, int Status)
        {
            string sUpdateQuery = "UPDATE [CY_EC_1_DETAILS] SET [CY_STATUS_LEVEL] = '" + Status + "' WHERE [O_INDX_NMBR] ='" + IncidentID + "'";
            return ExecuteSQLQuery(sUpdateQuery);
        }

        public string EC_getStatusStringFromLevel(string Level)
        {

            string str1 = "SELECT[STATUS_MESSAGE] FROM [CY_EC_STATUS_MESSAGE] WHERE [STATUS]='" + Level + "'";
            DataSet ds = ExecuteSELECTQuery(str1);
            if (ds.Tables[0].Rows[0][0].ToString() == "")
            {
                return "";
            }
            else
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }

            //string sOutput = "";
            //switch (Level)
            //{
            //    case "1":
            //        sOutput = "Reported";
            //        break;
            //    case "2":
            //        sOutput = "Reviewed and Forwarded to Incidence Manager";
            //        break;
            //    case "3":
            //        sOutput = "Task Assigned to FPR";
            //        break;
            //    case "4":
            //        sOutput = "Task Completed and back to Incidence Manager";
            //        break;
            //    case "5":
            //        sOutput = "Incidence Manager fwd to Accp Level 1";
            //        break;
            //    case "6":
            //        sOutput = "Incidence Manager fwd for Revised Action";
            //        break;
            //    case "7":
            //        sOutput = "Level 1 Accepted and Closed";
            //        break;
            //    case "8":
            //        sOutput = "Level 1 Accepted and Fwd to level 2";
            //        break;
            //    case "9":
            //        sOutput = "Level 1 fwd for Revised Action";
            //        break;
            //    case "10":
            //        sOutput = "Level 2 Accepted and Closed";
            //        break;
            //    case "11":
            //        sOutput = "Level 2 fwd for Revised Action";
            //        break;
            //}
            //return sOutput;
        }

        public Boolean EC_UpdateActionStatus(string IncidentID, string NewStatus, string PreStatus)
        {
            string sUpdateQuery = "UPDATE [CY_EC_3_ACTION_DTL] SET [CY_ACTN_STATUS] = '" + NewStatus + "' WHERE [O_EC_NMBR] ='" + IncidentID + "' AND [CY_ACTN_STATUS] = '" + PreStatus + "'";
            return ExecuteSQLQuery(sUpdateQuery);
        }

        public string EC_getStatusOfIncident(string IncidentID)
        {
            string sStr = "SELECT [CY_STATUS_LEVEL] FROM [CY_EC_1_DETAILS] WHERE [O_INDX_NMBR] = '" + IncidentID + "'";
            DataSet ds = ExecuteSELECTQuery(sStr);
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public Boolean EC_CheckAndInsertRelation(string IncidentID, string CPFNo, string Level, string EntryBy)
        {
            string sStr = "SELECT [EC_NO] ,[EMP_CPF_NO] ,[ENTRY_BY] FROM [CY_EC_EMP_RELATION] WHERE [EC_NO] ='" + IncidentID + "' AND [EMP_CPF_NO] ='" + CPFNo + "' AND [LEVEL]='" + Level + "'";
            DataSet dsCheck = ExecuteSELECTQuery(sStr);
            if (dsCheck.Tables[0].Rows.Count == 0)
            {
                string sInsertRelation = "INSERT INTO [CY_EC_EMP_RELATION] ([EC_NO], [EMP_CPF_NO], [LEVEL], [ENTRY_BY], [TIMESTAMP]) VALUES ";
                sInsertRelation = sInsertRelation + "('" + IncidentID + "','" + CPFNo + "','" + Level + "','" + EntryBy + "',getdate())";
                if (ExecuteSQLQuery(sInsertRelation) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Quiz
        public string getOptionLetter(string OptionID)
        {
            int index = Convert.ToInt32(OptionID);
            index = index - 1;
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (index > letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[index % letters.Length];

            return value;
        }

        public decimal getReponseCountFromQueID(string QueID)
        {
            string sStr = "SELECT COUNT([id]) FROM [CY_EMP_ANS_RELATION] WHERE [que_id] = '" + QueID + "'";
            DataSet ds = ExecuteSELECTQuery(sStr);
            return Convert.ToDecimal(ds.Tables[0].Rows[0][0]);
        }

        public decimal getOptionReponseCountFromQueIDAndOptID(string QueID, string OptID)
        {
            string sStr = "SELECT COUNT([id]) FROM [CY_EMP_ANS_RELATION] WHERE [que_id] = '" + QueID + "' AND [option_id] = '" + OptID + "'";
            DataSet ds = ExecuteSELECTQuery(sStr);
            return Convert.ToDecimal(ds.Tables[0].Rows[0][0]);
        }
        public string checkEmpGiveAns(string QueID, string cpf_no)
        {
            string sStr = "SELECT [id] FROM [CY_EMP_ANS_RELATION] WHERE [que_id] = '" + QueID + "' AND [cpf_no] = '" + cpf_no + "'";
            DataSet ds = ExecuteSELECTQuery(sStr);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return "FALSE";
            }
            else
            {
                return "TRUE";
            }

        }
        #endregion

        #region Stroed Procedure
        public string deleteEmployeeSP(string stroedProcedureName, string cpfNo)
        {
            try
            {
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand(stroedProcedureName, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@cpf_no", SqlDbType.Int).Value = cpfNo;
                conn.Open();
                int x = cmd.ExecuteNonQuery();
                conn.Close();
                return "User has been removed successfully !Effected Row -" + x + "";
            }
            catch (SqlException ex)
            {
                return "SQL Error :" + ex.Message.ToString();
            }

        }

        #endregion


    }

}