<%@ Page Language="c#" CodeBehind="Gate_pass_Requisition.aspx.cs" AutoEventWireup="True"
    Inherits="WebApplication1.Gate_pass_Requisition" %>

<!DOCTYPE html>
<html>
<head>

    <link href="css/styleGlobal.css" type="text/css" rel="stylesheet" />
    <link href="css/css3.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" language="javascript" src="js/jquery-1.7.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" language="javascript" src="js/slider.js"></script>
    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />
    <script src="js/jquery.autocomplete-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <link rel="stylesheet" href="js/themes/smoothness/jquery-ui-1.8.22.custom.css" type="text/css"
        media="screen" />


    <link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
    <script type="text/javascript" src="js/jquery.multiselect.js"></script>
    <script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
    <script type="text/javascript" src="js/jquery.gritter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.gritter.css" />
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>


    <link rel="stylesheet" href="css/AutoCompleteStyle.css" type="text/css" media="screen" />





    <script type="text/javascript">
        $(document).ready(function () {

            $("#cmdSendRequest").hide();

            /*--for Modifying tools--*/
            $('#div_visitor_table_data').on('click', '.cmdedit', function (event) {
                var index = $(this).attr('index');
                var groupid = $(this).attr('groupid');

                var tool = $(this).closest('tr').find('.txtTools').val();
                var purpose = $(this).closest('tr').find('.purposeofvisit').val();
                var gender = $(this).closest('tr').find('.gendercombo').val();

                var company = $(this).closest('tr').find('.txtVisitorCompany').val();
                var name = $(this).closest('tr').find('.txtVisitorName').val();
                var age = $(this).closest('tr').find('.txtVisitorAge').val();




                var data = '{INDEX: "' + index + '",GROUPID: "' + groupid + '",TOOL: "' + tool + '",PURPOSE: "' + purpose + '",GENDER: "' + gender + '",COMPANY: "' + company + '",NAME: "' + name + '",AGE: "' + age + '" }';
                //alert(data);

                $.ajax({
                    type: "POST",
                    url: "Gate_pass_Requisition.aspx/updategatepassdata",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var result = jQuery.parseJSON(response.d);
                        if (result.STATUS == "TRUE") {
                            alert(result.MESSAGE);
                        } else {
                            alert(result.MESSAGE);
                        }
                    }
                });
            });



            $("#txtWhomToMeet").focus();
            var sRequisitionHint = $("#RequisitioHint").val()
            if (sRequisitionHint != "NEW") {
                LoadVisitorStrip(0);
                $('#div_visitor_table_data').show();
                $('#VisitorForm').after($("#EditVisitorList").val());
                //$("#cmdSendRequest").show();
                var ExistingNoOfVisitor = $('#hiddenNoofVisitor').val();
                if (ExistingNoOfVisitor > 5) {
                    $('#txtNoVisitor').val(ExistingNoOfVisitor);
                } else {
                    $('input[name="my_hint"][value="' + ExistingNoOfVisitor + '"]').prop('checked', true);
                }
            } else if (sRequisitionHint == "NEW") {
                LoadVisitorStrip(1);
                $('input[name="my_hint"][value="1"]').prop('checked', true);
            }

            $(document).ajaxStop($.unblockUI);
            $("#visitor_start_date").datepicker({
                dateFormat: "dd-mm-yy",
                minDate: 0
            });
            $("#visitor_end_date").datepicker({
                dateFormat: "dd-mm-yy",
                minDate: 0
            });

            $('input[type="text"]').focusin(function () {
                $(this).css('background-color', 'rgb(250, 255, 189)');
            });

            $('input[type="text"]').focusout(function () {
                $(this).css('background-color', '');
            });

            $("input[type='text'], textarea").live('keyup', function () {
                $('#FillNotification').hide();
            });

            $('button[type="button"], select').live('click', function () {
                $('#FillNotification').hide();
                $(this).css('border-color', '');
            });

            var no_of_location = parseInt($('#cmbMeetingLocation option').size());
            $("#cmbMeetingLocation").multiselect({
                noneSelectedText: 'Select Meeting Location',
                selectedList: no_of_location,
                header: ""
            });
            $('.ui-multiselect').css('width', '100%');
            $('.ui-multiselect-menu').css('width', 'auto');

            // Start autocomplete 
            $("#txtWhomToMeet").focus(function () {
                if ($(this).val().trim() == "") {
                    $("#txtWhomToMeet").val("");
                    $("#txtMtng_Off_Desg").val("");
                    $("#cmbMtng_off_Lctn").val("");
                }
            });

            /********Start of Whom To Meet TextBox*************/
            var mainmenuSearch_options, mainmenuSearch_a;
            jQuery(function () {
                var options = {
                    serviceUrl: 'services/auto.aspx',
                    onSelect: mainmenuSearch_onAutocompleteSelect,
                    deferRequestBy: 0, //miliseconds
                    params: { type: 'emp_for_guesthouse', limit: '10' },
                    noCache: true //set to true, to disable caching
                };
                mainmenuSearch_a = $("#txtWhomToMeet").autocomplete(options);
            });
            var mainmenuSearch_onAutocompleteSelect = function (mainmenuSearch_value, mainmenuSearch_data1) {
                if (mainmenuSearch_data1 != -1) {
                    //35778~CE (E&T)~SATELLITE EARTH STATION~4202~9969228543
                    var abc = mainmenuSearch_data1.split("~");
                    $("#txtWhomToMeetCPF").val(abc[0]);
                    $("#txtWTMExtNo").val(abc[3]);
                    $("#txtMtng_Off_Desg").val(abc[1]);

                }
            }

            $(".txtVisitorCompany").live('keyup', function (e) {
                $(this).val(($(this).val()).toLowerCase());
            });

            $(".txtVisitorName").live('keyup', function (e) {
                $(this).val(($(this).val()).toLowerCase());
            });
            /********End OfWhom To Meet TextBox*************/


            $("#txtWhomToMeet").bind('keyup', function (e) {
                $("#txtWhomToMeet").val(($("#txtWhomToMeet").val()).toLowerCase());
                // $("#txtWhomToMeet").val(($("#txtWhomToMeet").val()));
            });

            // Send Button Request 

            $("#cmdSendRequest").click(function () {
                var visitor_full_list = "";
                $(".VisitorEntryForm").each(function () {
                    var NewVisitorName = $(this).children('td').children('input[name=visitorname]').val().trim();
                    var NewVisitorCompany = $(this).children('td').children('input[name=visitorcompany]').val().trim();
                    var NewVisitorAge = $(this).children('td').children('input[name=visitorage]').val().trim();
                    var NewVisitorSex = $(this).children('td').children('select[name=visitorsex]').val().trim();
                    var NewVisitorPurpose = $(this).children('td').children('input[name=visitorpurpose]').val().trim();
                    var NewVisitorTool = $(this).children('td').children('input[name=visitortool]').val().trim();

                    //var NewVisitorPurpose = NewVisitorPurpose.toString().replace("'", "''");
                    if (NewVisitorName != "") {
                        visitor_full_list += "name`" + NewVisitorName + "`age`" + NewVisitorAge + "`cmny_name`" + NewVisitorCompany + "`gender`" + NewVisitorSex + "`purpose`" + NewVisitorPurpose + "`tools`" + NewVisitorTool + "~";
                    }
                });


                //Reqesitioner Details
                var RequesitionerName = $("#txtEmpName").val();

                var RequesitionerCPF = $("#txtCPFNo").val();
                var RequesitionerDesig = $("#txtDesg").val();
                var RequesitionerLocation = $("#cmbLocation").val();
                var RequesitionerExtn = $("#txtExtNo").val();


                //Meeting Details
                var WhomToMeet = $("#txtWhomToMeet").val();
                var WhomToMeetDesig = $("#txtMtng_Off_Desg").val();
                var WhomToMeetCPF = $("#txtWhomToMeetCPF").val();
                var MeetingLocations = "";
                $('#cmbMeetingLocation :selected').each(function (i, selected) {
                    MeetingLocations += $(selected).text() + " and ";
                });

                //Pass Details
                var visitorentrylocation = $("#cmbEntryArea").val();
                alert(visitorentrylocation);
                var visitor_start_date_time = $("#visitor_start_date").val();
                var visitor_end_date_time = $("#visitor_end_date").val();
                var reasonofvisit = $("#cmbReason").val();
                var RequisitionType = $("#CmbRequisitionType").val();
                //   var purposeofvisit = $("#txtRemarks").val();
                var PassApproveAuthority_cpf_no = $("#ddlPassApproveAuthority").val();
                var PassApproveAuthority_name = $("#ddlPassApproveAuthority option:selected").text();

                if (RequesitionerName == "") {
                    alert("Please fill Requisioner Name");
                    return false;
                }

                if (RequesitionerCPF == "") {
                    alert("Please fill Requisioner CPF");
                    return false;
                }

                if (RequesitionerDesig == "") {
                    alert("Please fill Requisioner Designation");
                    return false;
                }

                if (RequesitionerLocation == "") {
                    alert("Please fill Requisioner Location");
                    return false;
                }

                if (RequesitionerExtn == "") {
                    alert("Please fill Requisioner Extension");
                    return false;
                }

                if (WhomToMeet == "") {
                    $("#txtWhomToMeet").focus();
                    $('#FillNotification').html("Please fill Meeting officer name");
                    $('#FillNotification').show();
                    //alert("Please fill Meeting officer name");
                    return false;
                }

                if (WhomToMeetDesig == "") {
                    alert("Please fill Meeting officer Designation");
                    return false;
                }

                if (WhomToMeetCPF == "") {
                    alert("Please fill Meeting officer CPF");
                    return false;
                }

                if (MeetingLocations == "") {
                    $('button[type="button"]').focus();
                    $('button[type="button"]').css('border-color', 'red');
                    $('#FillNotification').html("Please select Meeting Location");
                    $('#FillNotification').show();
                    //alert("Please select Meeting Location");
                    return false;
                }


                if (visitor_start_date_time == "") {
                    $('#visitor_start_date').focus();
                    $('#FillNotification').html("Please select start date");
                    $('#FillNotification').show();
                    //alert("Please select start date");
                    return false;
                }

                if (visitor_end_date_time == "") {
                    $('#visitor_end_date').focus();
                    $('#FillNotification').html("Please please select end date");
                    $('#FillNotification').show();
                    //alert("Please please select end date");
                    return false;
                }
                alert(visitorentrylocation);
                if (visitorentrylocation == "") {
                    $('#cmbEntryArea').focus();
                    $('#cmbEntryArea').css('border-color', 'red');
                    $('#FillNotification').html("Please Select visitor entry location");
                    $('#FillNotification').show();
                    //alert("Please Select visitor entry location");
                    return false;
                }



                if (reasonofvisit == "") {
                    $('#cmbReason').focus();
                    $('#cmbReason').css('border-color', 'red');
                    $('#FillNotification').html("Please select reason of visit");
                    $('#FillNotification').show();
                    //alert("Please select reason of visit");
                    return false;
                }

                if (RequisitionType == "") {
                    $('#CmbRequisitionType').focus();
                    $('#CmbRequisitionType').css('border-color', 'red');
                    $('#FillNotification').html("Please select requisition type");
                    $('#FillNotification').show();
                    //alert("Please select requisition type");
                    return false;
                }



                if (PassApproveAuthority_cpf_no == "") {
                    $('#ddlPassApproveAuthority').focus();
                    $('#ddlPassApproveAuthority').css('border-color', 'red');
                    $('#FillNotification').html("Please select pass approver authority");
                    $('#FillNotification').show();
                    //alert("Please select pass approver authority");
                    return false;
                }

                if (PassApproveAuthority_name == "") {
                    $('#ddlPassApproveAuthority').focus();
                    $('#ddlPassApproveAuthority').css('border-color', 'red');
                    $('#FillNotification').html("Please select pass approver authority");
                    $('#FillNotification').show();
                    //alert("Please select pass approver authority");
                    return false;
                }
                //                if (purposeofvisit == "") {
                //                    $('#txtRemarks').focus();
                //                    $('#FillNotification').html("Please fill purpose of visit");
                //                    $('#FillNotification').show();
                //                    //alert("Please fill purpose of visit");
                //                    return false;
                //                }
                if (visitor_full_list == "") {
                    if (sRequisitionHint == "NEW") {
                        alert("Please Add Visitor First");
                        return false;
                    }
                }
                //alert(sRequisitionHint);

                $.ajax({
                    type: "POST",
                    url: "services/sendpassapproverequest.aspx",
                    data: "RequesitionerName=" + RequesitionerName + "&RequesitionerCPF=" + RequesitionerCPF + "&RequesitionerDesig=" + encodeURIComponent(RequesitionerDesig) + "&RequesitionerLocation=" + encodeURIComponent(RequesitionerLocation) + "&RequesitionerExtn=" + RequesitionerExtn + "&WhomToMeet=" + WhomToMeet + "&WhomToMeetDesig=" + encodeURIComponent(WhomToMeetDesig) + "&WhomToMeetCPF=" + WhomToMeetCPF + "&MeetingLocations=" + encodeURIComponent(MeetingLocations) + "&visitorentrylocation=" + visitorentrylocation + "&visitor_start_date_time=" + visitor_start_date_time + "&visitor_end_date_time=" + visitor_end_date_time + "&reasonofvisit=" + reasonofvisit + "&RequisitionType=" + RequisitionType + "&PassApproveAuthority_cpf_no=" + PassApproveAuthority_cpf_no + "&PassApproveAuthority_name=" + PassApproveAuthority_name + "&VisitorFullList=" + encodeURIComponent(visitor_full_list) + "&RequesitionHint=" + sRequisitionHint,
                    success: function (msg) {
                        //alert(msg);
                        var values = msg.split("~");

                        if (values[0] == "TRUE" && values[1] == "SUCCESS") {
                            alert(values[2]);
                            window.location = self.location;
                            if (my_module_hint != "EDIT") {
                                emptyfield();
                            }

                        } else if (values[0] == "FALSE" && values[1] == "ERR") {
                            alert(values[2]);
                        }
                    }
                });
            });

            var NoOfVisitor = 0;
            $('input[type="radio"][name="my_hint"]').change(function () {
                NoOfVisitor = $(this).val();
                $('#txtNoVisitor').val(NoOfVisitor);
                LoadVisitorStrip(NoOfVisitor);
            });


            $('#cmdNoVisitor').click(function () {
                NoOfVisitor = $('#txtNoVisitor').val().trim();
                if (NoOfVisitor == "") {
                    alert('Please enter no of visitor.');
                } else {
                    LoadVisitorStrip(NoOfVisitor);
                    if (NoOfVisitor <= 5) {
                        $('input[name="my_hint"][value="' + NoOfVisitor + '"]').prop('checked', true);
                    } else {
                        $('input[name="my_hint"]').prop('checked', false);
                    }
                }
            });
            function LoadVisitorStrip(NoOfVisitor) {
                var current_no_of_strip = $('.VisitorEntryForm').size();
                var sVisitorStrips = "";
                $("#cmdSendRequest").show();

                if (NoOfVisitor > current_no_of_strip) {
                    NoOfVisitor = (NoOfVisitor - current_no_of_strip);
                    for (var i = 0; i < parseInt(NoOfVisitor) ; i++) {
                        var sVisitorToAdd = $('#HiddenField1').val();
                        sVisitorToAdd = sVisitorToAdd.replace('[replaceme]', current_no_of_strip + 1 + i);
                        sVisitorStrips += sVisitorToAdd;
                    }
                    $('#div_visitor_table_data').show();
                    $('#VisitorForm').after(sVisitorStrips);
                } else if (current_no_of_strip > NoOfVisitor) {
                    NoOfVisitor = (current_no_of_strip - NoOfVisitor);

                    if (NoOfVisitor == current_no_of_strip) /*--to hide Buuton when o row is added--*/
                        $("#cmdSendRequest").hide();

                    for (var i = 0; i < parseInt(NoOfVisitor) ; i++) {
                        $('#VisitorForm').next('tr').remove();
                    }
                }
                else {
                    $("#cmdSendRequest").hide();
                }


                /*************Start of company list autocomplte***********/
                $("input.txtVisitorCompany:not(.ui-autocomplete-input)").live("focus", function (event) {

                    var CompanySearch_options, CompanySearch_a;
                    var options = {
                        serviceUrl: 'services/auto.aspx',
                        onSelect: CompanySearch_onAutocompleteSelect,
                        deferRequestBy: 0, //miliseconds
                        params: { type: 'cmpny_list', limit: '10' },
                        noCache: true //set to true, to disable caching
                    };
                    CompanySearch_a = $(this).autocomplete(options);
                });

                var CompanySearch_onAutocompleteSelect = function (CompanySearch_value, CompanySearch_data) {
                    $('div.autocomplete').hide();
                }
                /**********End of company list autocomplte***********/

                /*************Start of company visitor autocomplte***********/

                $("input.txtVisitorCompany").live("keyup", function (event) {
                    $('#FillNotification').hide();
                });
                $("input.txtVisitorName").die();
                $("input.txtVisitorName").live("keyup", function (event) {
                    if ($(this).parent('td').siblings('td').children('input[name="visitorcompany"]').val() == "") {
                        alert("Please enter company name first");
                        $(this).val("");
                        $(this).parent('td').siblings('td').children('input[name="visitorcompany"]').focus();
                        return false;
                    }
                });
                var Current_tr;
                $("input.txtVisitorName:not(.ui-autocomplete-input)").live("focus", function (event) {
                    Current_tr = $(this).parent('td').parent('tr').attr('replaceme');
                    //alert(Current_tr);
                    var my_cmpny_list = $(this).parent('td').siblings('td').children('input[name="visitorcompany"]').val();
                    if (my_cmpny_list == "") {
                        $('#FillNotification').html("Please enter company name first");
                        $('#FillNotification').show();
                        $(this).val("");
                    } else {
                        var CompanyVisitorSearch_options, CompanyVisitorSearch_a;
                        var options = {
                            serviceUrl: 'services/auto.aspx',
                            onSelect: CompanyVisitorSearch_onAutocompleteSelect,
                            deferRequestBy: 0, //miliseconds
                            params: { type: 'cmpny_name_list', limit: '10', extra_data: my_cmpny_list },
                            noCache: true //set to true, to disable caching
                        };
                        CompanyVisitorSearch_a = $(this).autocomplete(options);
                    }
                });
                var CompanyVisitorSearch_onAutocompleteSelect = function (CompanyVisitorSearch_value, CompanyVisitorSearch_data) {
                    if (CompanyVisitorSearch_data != -1) {
                        //Ranjeet Kumar~22~M
                        //alert(Current_tr.length);

                        var CompanyVisitorSearch_data_arr = CompanyVisitorSearch_data.split('~');
                        if (typeof Current_tr !== "undefined" && Current_tr.length < 4) {
                            //alert('check1');
                            $('tr.VisitorEntryForm[replaceme="' + Current_tr + '"]').children('td').children('input[name="visitorname"]').val(CompanyVisitorSearch_data_arr[0]);
                            $('tr.VisitorEntryForm[replaceme="' + Current_tr + '"]').children('td').children('input[name="visitorage"]').val(CompanyVisitorSearch_data_arr[1]);
                            $('tr.VisitorEntryForm[replaceme="' + Current_tr + '"]').children('td').children('select[name="visitorsex"]').val(CompanyVisitorSearch_data_arr[2]);
                        }
                        else {

                            //alert('check2');
                            $('tr.modifyEntryForm[replaceme="' + Current_tr + '"]').children('td').children('input[name="visitorname"]').val(CompanyVisitorSearch_data_arr[0]);
                            $('tr.modifyEntryForm[replaceme="' + Current_tr + '"]').children('td').children('input[name="visitorage"]').val(CompanyVisitorSearch_data_arr[1]);
                            $('tr.modifyEntryForm[replaceme="' + Current_tr + '"]').children('td').children('select[name="visitorsex"]').val(CompanyVisitorSearch_data_arr[2]);
                        }
                    }
                    else {
                        alert('not found')
                    }
                    $('div.autocomplete').hide();
                }
                /**********End of Company Aisitor Autocomplete***********/
            }


            function emptyfield() {
                $("#cmbEntryArea").val("---- Select Entry Area ----");
                $("#txtWhomToMeet").val("Search Employee..");
                $("#txtMtng_Off_Desg").val("");
                $("#cmbMtng_off_Lctn").val("");
                $("#CmbRequisitionType").val("Select The Type of Requisition");
                $("#txtRemarks").val("");
                $("#ddlPassApproveAuthority").val("");
                $("#txtWhomToMeet").val();
                $("#txtWhomToMeet").val();
                $("#txtWhomToMeet").val();
                $("#total_theory").text("");
                $(".my_complete_div_strip").remove();
            }
            //id="edit_old_pass_request"
        });
    </script>
    <style type="text/css">
        .flattxt {
            margin-top: 0px;
        }

        .strip_my_p {
            position: relative;
            float: left;
            font-size: 15px;
            color: #7C7575;
            margin-left: 12px;
            font-family: arial;
        }

        .ui-multiselect-menu {
            font-size: 12px;
        }

        .ui-multiselect {
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <asp:HiddenField ID="EditVisitorList" runat="server" />
        <asp:HiddenField ID="RequisitioHint" runat="server" />
        <asp:HiddenField ID="GroupID" runat="server" />
        <div class="div-fullwidth iframeMainDiv" style="width: 98%;">
            <div class="div-fullwidth" style="background-color: White; border-top: 5px solid green;">
                <h1 class="heading-GatePass">Visitor Pass Requisition</h1>
                <%--   <a runat="server" id='cmdViewApprovals' href="VisitorPass_Approval1.aspx" class="g-button g-button-share" type="button" style="position: relative;
                float: right; text-decoration: none; margin-top: 6px; margin-right: 10px;">View Pending Approvals</a>--%>
                <asp:HyperLink ID="cmdViewApprovals" runat="server"
                    CssClass="g-button g-button-share" NavigateUrl="~/VisitorPass_Approval1.aspx" Style="position: relative; float: right; text-decoration: none; margin-top: 6px; margin-right: 10px;">View Pending Approvals</asp:HyperLink>
                <a href="Gate_Pass_History.aspx" class="g-button g-button-red" type="button" style="position: relative; float: right; text-decoration: none; margin-top: 6px; margin-right: 10px;">VIEW
                HISTORY</a>

            </div>
            <div class="div-pagebottom">
            </div>
            <div id="div_full_form" class="div-fullwidth" style="width: 100%; margin-top: 0;">
                <div class="div-fullwidth marginbottom">
                    <div class="div-halfwidth">
                        <asp:Label ID="Label1" runat="server" Text="Select Pass Type" class="gatepass-content"
                            Style="width: 125px;"></asp:Label>
                        <asp:DropDownList ID="DropDownList1" runat="server" Style="position: relative; float: left; width: 200px;">
                            <asp:ListItem>Select Pass Type</asp:ListItem>
                            <asp:ListItem Selected="True">Visitors Gate Pass</asp:ListItem>
                            <asp:ListItem>Temporary Duty Pass</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <fieldset style="width: 97%;">
                    <legend class="content" style="float: none; color: green">Requisitioner's Details</legend>
                    <div class="div-fullwidth marginbottom">
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Name</h1>
                            <asp:TextBox ID="txtEmpName" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Designation</h1>
                            <asp:TextBox ID="txtDesg" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">CPF No.</h1>
                            <asp:TextBox ID="txtCPFNo" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                    <div class="div-fullwidth">
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Location</h1>
                            <asp:TextBox ID="cmbLocation" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Ext. No.</h1>
                            <asp:TextBox ID="txtExtNo" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                </fieldset>
                <fieldset style="width: 97%;">
                    <legend class="content" style="float: none; color: green">Meeting Details</legend>
                    <div class="div-fullwidth marginbottom">
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Whom to Meet</h1>
                            <asp:TextBox ID="txtWhomToMeet" runat="server" Style="width: 185px;"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Designation</h1>
                            <asp:TextBox ID="txtMtng_Off_Desg" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">CPF No.</h1>
                            <asp:TextBox ID="txtWhomToMeetCPF" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                    <div class="div-fullwidth">
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0; display: none;">
                            <h1 class="gatepass-content2" style="width: 110px;">Ext. No.</h1>
                            <asp:TextBox ID="TextBox1" runat="server" Style="width: 185px;" Enabled="False"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth">
                            <h1 class="gatepass-content2" style="width: 110px; color: #800000;">Location</h1>
                            <div class="div-fullwidth" id="divMeetingLocation" runat="server" style="width: 86%;">
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset style="width: 97%;">
                    <legend class="content" style="float: none; color: green">Pass Details</legend>
                    <div class="div-fullwidth marginbottom">
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Entry Date</h1>
                            <asp:TextBox ID="visitor_start_date" runat="server" CssClass="flattxt" Style="position: relative; float: left; width: 185px;"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Valid Untill</h1>
                            <asp:TextBox ID="visitor_end_date" runat="server" CssClass="flattxt" Style="position: relative; float: left; margin-right: 5px; width: 185px;"></asp:TextBox>
                        </div>
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px; color: #800000;">Entry Area</h1>
                            <asp:DropDownList ID="cmbEntryArea" runat="server" CssClass="flattxt" Style="width: 185px;">
                                <asp:ListItem Value="">Select Entry Area</asp:ListItem>
                                <asp:ListItem Value="Admin and White Area">Admin and White Area</asp:ListItem>
                                <asp:ListItem Value="White Area">White Area</asp:ListItem>
                                <asp:ListItem Value="Stores Complex">Stores Complex</asp:ListItem>
                                <asp:ListItem Value="Trombay Terminal">Trombay Terminal</asp:ListItem>
                                <asp:ListItem Value="Admin Area">Admin Area</asp:ListItem>
                                <asp:ListItem Value="Operational Area">Operational Area</asp:ListItem>
                                <asp:ListItem Value="All Area">All Area</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="div-fullwidth marginbottom">
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Reason of Visit</h1>
                            <%-- <asp:DropDownList ID="cmbReason" runat="server" Style="width: 185px;">
                                <asp:ListItem Value="">--- Select Reason Of Visit ---</asp:ListItem>
                                <asp:ListItem Value="Official">Official</asp:ListItem>
                                <asp:ListItem Value="Personal">Personal</asp:ListItem>
                            </asp:DropDownList>--%>

                           <%-- <asp:TextBox ID="cmbReason" runat="server"></asp:TextBox>--%>
                             <asp:DropDownList ID="cmbReason" runat="server" Style="width: 163px;">
                            <asp:ListItem Value="">--- Select Reason Of Visit ---</asp:ListItem>
                            <asp:ListItem Value="Official">Official</asp:ListItem>
                            <asp:ListItem Value="Personal">Personal</asp:ListItem>
                            </asp:DropDownList>




                        </div>
                        <div class="div-fullwidth" style="width: 34%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px;">Requisition Type</h1>
                            <asp:DropDownList ID="CmbRequisitionType" runat="server" Style="width: 185px;">
                                <asp:ListItem Value="">Select Requisition Type</asp:ListItem>
                                <asp:ListItem Value="Entry On Week Days">Entry On Week Days</asp:ListItem>
                                <asp:ListItem Value="Entry On Holidays">Entry On Holidays</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="div-fullwidth" style="width: 33%; margin-top: 0;">
                            <h1 class="gatepass-content2" style="width: 110px; color: #800000;">Approver Name</h1>
                            <div id="div_pass_approving_auth" runat="server">
                            </div>
                        </div>
                    </div>
                    <div class="div-fullwidth" style="display: none;">
                        <h1 class="gatepass-content2" style="width: 110px;">Purpose of Visit</h1>
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="flattxt" TextMode="MultiLine"
                            Style="padding-top: 4px; width: 185px; max-width: 185px; height: 27px; max-height: 50px; overflow-y: auto; overflow-x: hidden;"></asp:TextBox>
                    </div>
                </fieldset>
                <fieldset style="width: 97%;">
                    <legend class="content" style="float: none; color: green">Visitor's Details</legend>
                    <div style="position: relative; float: left; left: 0px; padding: 2px 10px 2px 10px; width: 98%; background-image: -webkit-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); background-image: -moz-linear-gradient(#EDEDED, #EDEDED 38%, #DEDEDE); border: 1px solid #ccc; margin-bottom: 10px;">
                        <h1 class="content">Select or Enter No of visitor :</h1>
                        <input type="radio" name="my_hint" id="v1" value="1" style="float: left; margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="v1" class="content"
                            style="cursor: pointer; margin-right: 20px;">1</label>
                        <input type="radio" name="my_hint" id="v2" value="2" style="float: left; margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="v2" class="content"
                            style="cursor: pointer; margin-right: 20px;">2</label>
                        <input type="radio" name="my_hint" id="v3" value="3" style="float: left; margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="v3" class="content"
                            style="cursor: pointer; margin-right: 20px;">3</label>
                        <input type="radio" name="my_hint" id="v4" value="4" style="float: left; margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="v4" class="content"
                            style="cursor: pointer; margin-right: 20px;">4</label>
                        <input type="radio" name="my_hint" id="v5" value="5" style="float: left; margin: 6px; border: 3px solid #3E92A7; height: 19px; width: 19px;" /><label for="v5" class="content"
                            style="cursor: pointer; margin-right: 20px;">5</label>
                        <h1 class="content" style="font-style: italic; margin-right: 20px;">or</h1>
                        <input type="text" id="txtNoVisitor" class="flattxt" style="padding-top: 4px; float: left; margin-right: 10px; width: 50px;" />
                        <input type="button" value="GO" class="g-button g-button-submit" id="cmdNoVisitor"
                            style="float: left;" />
                    </div>
                    <div id="div_visitor_table_data" style="display: none;">
                        <table id="Table1" class="tftable" border="1">
                            <tr id="VisitorForm">
                                <th>Company Name
                                </th>
                                <th>Visitor's Name
                                </th>
                                <th style="width: 40px;">Age
                                </th>
                                <th style="width: 80px;">Gender
                                </th>
                                <th style="width: 178px;">Purpose of Visit
                                </th>
                                <th style="width: 200px;">With Tools ( Eg. Laptop , Bag etc.)
                                </th>
                                <th style="width: 80px;">Edit Details
                                </th>
                            </tr>
                        </table>
                    </div>
                </fieldset>
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="hiddenNoofVisitor" runat="server" />
                <div class="div-fullwidth" id="tblVisitorDetails">
                    <h1 id="FillNotification" style="float: left; color: rgb(255, 0, 0); font-size: 15px; font-family: MyriadPro-Regular; padding: 2px 5px; position: relative; margin-top: 10px; display: none;"></h1>
                    <input type="button" id="cmdSendRequest" value="Submit Request" class="g-button g-button-share"
                        style="float: right; margin-top: 10px; margin-bottom: 20px;" />
                </div>

                <div runat="server" class="div-fullwidth" id="table_data" style="margin-bottom: 10px;">
                </div>
            </div>
        </div>
    </form>
</body>
</html>
