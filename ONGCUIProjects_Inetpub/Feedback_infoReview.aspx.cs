using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
namespace ONGCUIProjects
{
    public partial class Feedback_infoReview : System.Web.UI.Page
    {
        #region Variables


        int i;
        int j;
        int k;
        string strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"]);
        #endregion

        ONGCUIProjects.MyApplication1 my = new ONGCUIProjects.MyApplication1();



        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
                Binduser();
        }
        private void Binduser()
        {
            try
            {
                string query = "";
                query = "SELECT distinct([cpf_no]) FROM [cy_Feedback_info] order by cpf_no ";
                DataSet ds = (DataSet)my.ExecuteSELECTQuery(query);
                if (ds != null)
                {

                    DataRow dr = ds.Tables[0].NewRow();
                    dr["cpf_no"] = "--SELECT--";
                    //dr["cpf_no"] = "0";
                    ds.Tables[0].Rows.InsertAt(dr, 0);
                    ddl_user.DataSource = ds;
                    ddl_user.DataTextField = "cpf_no";
                    ddl_user.DataValueField = "cpf_no";
                    ddl_user.DataBind();
                }
            }
            catch (Exception exe)
            {
                WebMsgApp.WebMsgBox.Show("Error" + exe);
            }

        }

        private string GetMenuItemsOfEmployee(ref List<string> mainmenu, ref List<string> childitems, string sCPF)
        {
            mainmenu.Clear();
            childitems.Clear();

            // Data set for Bind TreeView
            string sStr;
            sStr = "SELECT [CY_MAIN_MENU].[id],";
            sStr = sStr + "[CY_MAIN_MENU].[caption],";
            sStr = sStr + "[CY_MAIN_MENU].[tool_tip_content],";
            sStr = sStr + "[CY_MAIN_MENU].[priority],";
            sStr = sStr + "[CY_CHILD_MENU].[id],";
            sStr = sStr + "[CY_CHILD_MENU].[caption],";
            sStr = sStr + "[CY_CHILD_MENU].[tool_tip_content],";
            sStr = sStr + "[CY_CHILD_MENU].[priority],";//7
            sStr = sStr + "[CY_CHILD_MENU].[main_menu_id],";
            sStr = sStr + "[CY_CHILD_MENU].[child_code],";
            sStr = sStr + "[CY_MENU_EMP_RELATION].[child_id]";
            sStr = sStr + " FROM ((";
            sStr = sStr + "[CY_MAIN_MENU] LEFT JOIN [CY_CHILD_MENU] ON [CY_MAIN_MENU].[id] = [CY_CHILD_MENU].[main_menu_id])";
            sStr = sStr + " LEFT JOIN [CY_MENU_EMP_RELATION] ON [CY_CHILD_MENU].[id] = [CY_MENU_EMP_RELATION].[child_id]) WHERE [CY_MENU_EMP_RELATION].[cpf_number]='" + sCPF + "' AND [CY_CHILD_MENU].[menu_type] = 'MENU' ORDER BY [CY_MAIN_MENU].[priority] ASC, [CY_CHILD_MENU].[priority] ASC";
            SqlConnection con = new SqlConnection(strConn);
            SqlDataAdapter sda1 = new SqlDataAdapter(sStr, con);
            DataSet ds = new DataSet();
            sda1.Fill(ds);
            String sMainMenuCaption_old = "NA";
            int iCount = 0;
            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                String sMainMenuCaption_new = ds.Tables[0].Rows[i][1].ToString();
                if (sMainMenuCaption_new == sMainMenuCaption_old)
                {
                    //Sample data of Child Menu collection/List  i.e. "11~Incidentchild~Incident_Data_Entry.aspx~1"
                    string sChildItem;
                    sChildItem = ds.Tables[0].Rows[i][4].ToString() + "~" + ds.Tables[0].Rows[i][5].ToString() + "~" + ds.Tables[0].Rows[i][9].ToString() + "~" + iCount;  //MainMenu capion and its ID
                    childitems.Add(sChildItem);
                }
                else
                {
                    iCount++;
                    string sMainItem;
                    sMainItem = sMainMenuCaption_new + "~" + ds.Tables[0].Rows[i][0].ToString();  //MainMenu capion and its ID
                    mainmenu.Add(sMainItem);

                    //Sample data of Child Menu collection/List  i.e. "11~Incidentchild~Incident_Data_Entry.aspx~1"
                    string sChildItem;
                    sChildItem = ds.Tables[0].Rows[i][4].ToString() + "~" + ds.Tables[0].Rows[i][5].ToString() + "~" + ds.Tables[0].Rows[i][9].ToString() + "~" + iCount;  //MainMenu capion and its ID
                    childitems.Add(sChildItem);

                    sMainMenuCaption_old = sMainMenuCaption_new;
                }
            }

            con.Close();
            return "YES";
        }



        protected void cmdHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("launchpage.aspx");
        }

        protected void cmdLogout_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Session["Login_Name"] = "";
            Session["EmployeeName"] = "";
        }

        protected void ddl_user_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "";
            query = "SELECT  ID FROM CY_FEEDBACK_INFO WHERE [CPF_NO]='" + ddl_user.SelectedValue.ToString() + "'";
            DataSet ds1 = (DataSet)my.ExecuteSELECTQuery(query);
            if (ds1 != null)
            {
                ds1.Tables[0].Columns.Add("TEXT");
                for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                    ds1.Tables[0].Rows[i]["TEXT"] = "Feedback " + i;
                DataRow dr = ds1.Tables[0].NewRow();
                dr["TEXT"] = "--SELECT--";
                dr["ID"] = "0";
                ds1.Tables[0].Rows.InsertAt(dr, 0);

                ddl_id.DataSource = ds1;
                ddl_id.DataTextField = "TEXT";
                ddl_id.DataValueField = "ID";
                ddl_id.DataBind();
            }
        }
        private void loadgrid()
        {
            try
            {
                string sStr1 = " ";
                sStr1 += " SELECT ISNULL(COMMENT.NAME,'') AS Service_NAME,(RATE) as Rating,Remark,CPF_NO,TIMESTAMP,Emp_name  FROM [cy_feedback]";
                sStr1 += " left join COMMENT                        ";
                sStr1 += " on COMMENT.ID=[CY_FEEDBACK].C_ID  ";

                sStr1 += " where F_ID='" + ddl_id.SelectedValue.ToString() + "'   ";
                sStr1 += " ORDER BY [TIMESTAMP] DESC           ";
                DataSet ds = (DataSet)my.ExecuteSELECTQuery(sStr1);
                if (ds != null)
                {
                    lbls_nme.Text = ds.Tables[0].Rows[0]["Emp_name"].ToString();
                    lbl_cpf.Text = ds.Tables[0].Rows[0]["CPF_NO"].ToString();
                    lbl_dates.Text = ds.Tables[0].Rows[0]["TIMESTAMP"].ToString();

                    DataTable dt = ds.Tables[0];

                    grid_service.DataSource = convertcolumntorows(dt);
                    grid_service.DataBind();
                    grid_service.Rows[0].ForeColor = System.Drawing.Color.Blue;

                    // grid_service.Rows[1].HorizontalAlign = HorizontalAlign.Right;
                    grid_service.HeaderRow.Visible = false;
                    grid_service.Rows[3].Visible = false;
                    grid_service.Rows[4].Visible = false;
                    grid_service.Rows[5].Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable convertcolumntorows(DataTable dt)
        {
            DataTable dtnew = new DataTable();
            //Convert all the rows to columns
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtnew.Columns.Add(Convert.ToString(i));
            }
            DataRow dr;
            // Convert All the Columns to Rows
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dr = dtnew.NewRow();
                dr[0] = dt.Columns[j].ToString();
                for (int k = 1; k <= dt.Rows.Count; k++)
                    dr[k] = dt.Rows[k - 1][j];
                dtnew.Rows.Add(dr);
            }
            return dtnew;
        }

        protected void chkbox_Header_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = (CheckBox)sender;
                GridViewRow gr = (GridViewRow)chk.Parent.Parent;
                int currentRowIndex = int.Parse(gr.RowIndex.ToString());

                CheckBox ChkBoxHeader = (CheckBox)grid_service.HeaderRow.FindControl("chkbox_Header");
                foreach (GridViewRow row in grid_service.Rows)
                {
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkbox_confgrd");
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                    }
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Please try again,Error massage " + ex.Message + "');", true);

            }
        }

        protected void btn_all_Click(object sender, EventArgs e)
        {
            try
            {

                con.Open();
                string str = "";
                str = "UPDATE  [cy_feedback] SET [STATUS]=1  WHERE F_ID='" + ddl_id.SelectedValue.ToString() + "'";
                SqlCommand cmd = new SqlCommand(str, con);
                cmd.ExecuteNonQuery();
                con.Close();
                loadgrid();


            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddl_id_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_view_Click(object sender, EventArgs e)
        {
            loadgrid();
        }

        protected void grid_service_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].CssClass = "gridcss";
            }
        }

    }
}