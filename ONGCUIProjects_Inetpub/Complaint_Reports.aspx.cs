using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace ONGCUIProjects
{
    /// <summary>
    public partial class Complaint_Reports : System.Web.UI.Page
    {
        //SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]);
        ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
        protected void Page_Load(object sender, System.EventArgs e)
        {

        }


        protected void ddltypeofreport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddltypeofreport.SelectedItem.Value == "CTYPE")
            {

                ddlcomplainttype.Visible = true;
            }
            else
            {
                ddlcomplainttype.Visible = false;
            }
        }



        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void cmdgenerate_Click1(object sender, EventArgs e)
        { 

         
            #region DataTAble
            DataTable dt = new DataTable();
            dt.Columns.Add("SERIAL_NO.", typeof(string));
            dt.Columns.Add("APP_NAME", typeof(string));
            dt.Columns.Add("APP_DESIG", typeof(string));
            dt.Columns.Add("APP_LOCATION", typeof(string));
            dt.Columns.Add("ROOM_NO", typeof(string));
            dt.Columns.Add("APP_PHONE_EX_NO", typeof(string));
            dt.Columns.Add("COMPLAIN_DEPARTMENT", typeof(string));
            dt.Columns.Add("TIMESTAMP", typeof(string));
            dt.Columns.Add("PROBLEM_DISCRIPTION", typeof(string));
            dt.Columns.Add("VIEW_TIMESTAMP", typeof(string));
            dt.Columns.Add("VIEWED_RESPONSE", typeof(string));
            dt.Columns.Add("O_EMP_NM/cpf_number", typeof(string));
            dt.Columns.Add("O_EMP_DESGNTN", typeof(string));
            dt.Columns.Add("CY_EMP_EX_NO", typeof(string));
            dt.Columns.Add("O_EMP_WRK_LCTN", typeof(string)); 
            #endregion
            string Department = "";
            MyApplication1 myapp = new MyApplication1();
            Label2.Text = "";
            cmdExporttoExcel.Visible = false;
            string type = ddltypeofreport.SelectedItem.Value.ToString();
            string date1 = myapp.convertdateforsql1(txtdatefrom.Text);
            string date2 = "";


            if (type=="DATEWISE")
            {
                 date2 = "";
            }
            else
            {
                 date2 = myapp.convertdateforsql1(txtdateto.Text);
            }
            //DateTime dt1;
            //dt1 = Convert.ToDateTime(myapp.convertfordt(txtdateto.Text));
            //string dates = dt1.AddDays(1).ToString();
            //date2 = myapp.convertdatetimeformattosql(dates);
            string str = "";
            if(type=="DATEWISE")
            {
                if (date1 == "")
                {
                    Response.Write("<Script language='JavaScript'>" + "alert('Please Select Valid From Date Or To Date')" + "</Script>");
                    return;
                }
            }
            else {
                if (date1 == "" || date2 == "")
                {
                    Response.Write("<Script language='JavaScript'>" + "alert('Please Select Valid From Date Or To Date')" + "</Script>");
                    return;
                }
            }
           
            if (type == "DATEWISE")
            {
                //str = "Select ROW_NUMBER() OVER(ORDER BY [O_INDX_NMBR]) AS [SERIAL], O_APLCNT_NM as [Applicant Name], O_APLCNT_CPF_NMBR as [Applicant CPF No.], O_APLCNT_DESG as [Designation], O_APLCNT_LCTN as [Location], O_APLCNT_EXT_NMBR as [Extn. No.], O_REQSTN_TYPE as [Requisition Type], O_ENTRY_DT as [Entry Date], O_EXIT_DT as [Exit Date], O_ENTRY_AREA as [Entry Area], O_MTNG_OFF_NM as [Meeting Officer Name], O_MTNG_OFF_DESG as [Officer Designation], O_MTNG_OFF_LCTN as [Meeting Location], O_VISTR_NM as [Visitor Name], O_VISTR_ORGNZTN as [Visitor Organization], O_VISTR_AGE as [Visitor Age], O_VISTR_GNDR as [Visitor Gender], O_PRPS_OF_VISIT as [Purpose of Visit], O_RMRKS as [Comments], O_ACP1_NM as [Pass Approver], O_ACP1_CPF_NMBR as [Approver CPF No.], O_SYS_DT_TM as [System Date] from [O_VSTR_GATE_PASS_RQSTN_DTL] where O_REQ_COMPLTN_FLG = 'P' and O_PASS_TYPE_FLG = 'V' and  O_ENTRY_DT between '" + date1 + "' and '" + date2 + "' order by O_INDX_NMBR";
                //str = "select (APP_NAME+'/'+APP_CPF_NO)as [APP_NAME],APP_DESIG,APP_LOCATION,ROOM_NO,APP_PHONE_EX_NO,COMPLAIN_DEPARTMENT,TIMESTAMP,PROBLEM_DISCRIPTION,VIEW_TIMESTAMP,VIEWED_RESPONSE from CY_COMPLAIN_REGISTER_DETAIL where TIMESTAMP between '" + date1 + "' and '" + date2 + "'";
                str = "select (APP_NAME+'/'+APP_CPF_NO)as [APP_NAME],APP_DESIG,APP_LOCATION,ROOM_NO,APP_PHONE_EX_NO,COMPLAIN_DEPARTMENT,CONVERT(varchar,TIMESTAMP,105)+' '+ CONVERT(varchar,TIMESTAMP,108) AS TIMESTAMP ,PROBLEM_DISCRIPTION,VIEW_TIMESTAMP,VIEWED_RESPONSE from CY_COMPLAIN_REGISTER_DETAIL where TIMESTAMP >='" + date1 + "' ORDER BY ID DESC";
                DataSet ds = My.ExecuteSELECTQuery(str);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i + 1;
                    dr[1] = ds.Tables[0].Rows[i]["APP_NAME"].ToString();
                    dr[2] = ds.Tables[0].Rows[i]["APP_DESIG"].ToString();
                    dr[3] = ds.Tables[0].Rows[i]["APP_LOCATION"].ToString();
                    dr[4] = ds.Tables[0].Rows[i]["ROOM_NO"].ToString();
                    dr[5] = ds.Tables[0].Rows[i]["APP_PHONE_EX_NO"].ToString();
                    dr[6] = ds.Tables[0].Rows[i]["COMPLAIN_DEPARTMENT"].ToString();
                    dr[7] = ds.Tables[0].Rows[i]["TIMESTAMP"].ToString();
                    dr[8] = ds.Tables[0].Rows[i]["PROBLEM_DISCRIPTION"].ToString();
                    dr[9] = ds.Tables[0].Rows[i]["VIEW_TIMESTAMP"].ToString();
                    dr[10] = ds.Tables[0].Rows[i]["VIEWED_RESPONSE"].ToString();
                    Department = ds.Tables[0].Rows[i]["COMPLAIN_DEPARTMENT"].ToString();
                    string page_id = "";
                    if (Department == "TELEPHONE")
                    {
                        page_id = "62";          // page_id commented for temporary
                    }
                    else if (Department == "ELECTRICAL(AC)")
                    {
                        page_id = "63";
                    }
                    else if (Department == "ELECTRICAL(MAINTENANCE)")
                    {
                        page_id = "77";
                    }
                    else if (Department == "CIVIL")
                    {
                        page_id = "64";
                    }
                    else if (Department == "HOUSEKEEPING")
                    {
                        page_id = "65";
                    }
                    else if (Department == "MECHANICAL")
                    {
                        page_id = "66";
                    }
                    else if (Department == "OTHER")
                    {
                        page_id = "67";
                    }

                    else if (Department == "WalkieTalkie")
                    {
                        page_id = "99";
                    }
                    else if (Department == "PAPaging")
                    {
                        page_id = "98";
                    }

                    string str1 = "SELECT ([O_EMP_MSTR].[O_EMP_NM]+'/'+[CY_MENU_EMP_RELATION].[cpf_number])as [O_EMP_NM/cpf_number],[O_EMP_MSTR].[O_EMP_DESGNTN],[O_EMP_MSTR].[CY_EMP_EX_NO],[O_EMP_MSTR].[O_EMP_WRK_LCTN] FROM [CY_MENU_EMP_RELATION],[O_EMP_MSTR] where  [CY_MENU_EMP_RELATION].[child_id]='" + page_id + "' and [O_EMP_MSTR].O_CPF_NMBR=[CY_MENU_EMP_RELATION].cpf_number ";
                    DataSet ds2 = My.ExecuteSELECTQuery(str1);
                    for (int j = 0; j < ds2.Tables[0].Rows.Count; j++)
                    {
                        dr[11] = ds2.Tables[0].Rows[j]["O_EMP_NM/cpf_number"].ToString();
                        dr[12] = ds2.Tables[0].Rows[j]["O_EMP_DESGNTN"].ToString();
                        dr[13] = ds2.Tables[0].Rows[j]["CY_EMP_EX_NO"].ToString();
                        dr[14] = ds2.Tables[0].Rows[j]["O_EMP_WRK_LCTN"].ToString();
                    }
                    dt.Rows.Add(dr);
                }

            }


            else
            {
                string emp_depart = ddlcomplainttype.SelectedValue.ToString();
                str = "select (APP_NAME+'/'+APP_CPF_NO)as [APP_NAME],APP_DESIG,APP_LOCATION,ROOM_NO,APP_PHONE_EX_NO,COMPLAIN_DEPARTMENT,CONVERT(varchar,TIMESTAMP,105)+' '+ CONVERT(varchar,TIMESTAMP,108) AS TIMESTAMP,PROBLEM_DISCRIPTION,VIEW_TIMESTAMP,VIEWED_RESPONSE from CY_COMPLAIN_REGISTER_DETAIL where COMPLAIN_DEPARTMENT='" + emp_depart + "' and TIMESTAMP between '" + date1 + "' and '" + date2 + "' ORDER BY ID DESC";
                DataSet ds = My.ExecuteSELECTQuery(str);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //string timestamp = ds.Tables[0].Rows[i]["TIMESTAMP"].ToString();
                    //DateTime changedtFormat = DateTime.ParseExact(timestamp, "dd/MM/yyyy", null);
                    DataRow dr = dt.NewRow();
                    dr[0] = i + 1;
                    dr[1] = ds.Tables[0].Rows[i]["APP_NAME"].ToString();
                    dr[2] = ds.Tables[0].Rows[i]["APP_DESIG"].ToString();
                    dr[3] = ds.Tables[0].Rows[i]["APP_LOCATION"].ToString();
                    dr[4] = ds.Tables[0].Rows[i]["ROOM_NO"].ToString();
                    dr[5] = ds.Tables[0].Rows[i]["APP_PHONE_EX_NO"].ToString();
                    dr[6] = ds.Tables[0].Rows[i]["COMPLAIN_DEPARTMENT"].ToString();
                    dr[7] = ds.Tables[0].Rows[i]["TIMESTAMP"].ToString();
                    dr[8] = ds.Tables[0].Rows[i]["PROBLEM_DISCRIPTION"].ToString();
                    dr[9] = ds.Tables[0].Rows[i]["VIEW_TIMESTAMP"].ToString();
                    dr[10] = ds.Tables[0].Rows[i]["VIEWED_RESPONSE"].ToString();
                    Department = ds.Tables[0].Rows[i]["COMPLAIN_DEPARTMENT"].ToString();
                    string page_id = "";
                    if (Department == "TELEPHONE")
                    {
                        page_id = "62";          // page_id commented for temporary
                    }
                    else if (Department == "ELECTRICAL(AC)")
                    {
                        page_id = "63";
                    }
                    else if (Department == "ELECTRICAL(MAINTENANCE)")
                    {
                        page_id = "77";
                    }
                    else if (Department == "CIVIL")
                    {
                        page_id = "64";
                    }
                    else if (Department == "HOUSEKEEPING")
                    {
                        page_id = "65";
                    }
                    else if (Department == "MECHANICAL")
                    {
                        page_id = "66";
                    }
                    else if (Department == "OTHER")
                    {
                        page_id = "67";
                    }
                    else if (Department == "WalkieTalkie")
                    {
                        page_id = "99";
                    }
                    else if (Department == "PAPaging")
                    {
                        page_id = "98";
                    }
                    string str1 = "SELECT ([O_EMP_MSTR].[O_EMP_NM]+'/'+[CY_MENU_EMP_RELATION].[cpf_number])as [O_EMP_NM/cpf_number],[O_EMP_MSTR].[O_EMP_DESGNTN],[O_EMP_MSTR].[CY_EMP_EX_NO],[O_EMP_MSTR].[O_EMP_WRK_LCTN] FROM [CY_MENU_EMP_RELATION],[O_EMP_MSTR] where  [CY_MENU_EMP_RELATION].[child_id]='" + page_id + "' and [O_EMP_MSTR].O_CPF_NMBR=[CY_MENU_EMP_RELATION].cpf_number ";
                    DataSet ds2 = My.ExecuteSELECTQuery(str1);
                    for (int j = 0; j < ds2.Tables[0].Rows.Count; j++)
                    {
                        dr[11] = ds2.Tables[0].Rows[j]["O_EMP_NM/cpf_number"].ToString();
                        dr[12] = ds2.Tables[0].Rows[j]["O_EMP_DESGNTN"].ToString();
                        dr[13] = ds2.Tables[0].Rows[j]["CY_EMP_EX_NO"].ToString();
                        dr[14] = ds2.Tables[0].Rows[j]["O_EMP_WRK_LCTN"].ToString();
                    }
                    dt.Rows.Add(dr);
                }

            }
            //DataSet ds1 = My.ExecuteSELECTQuery(str);

            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count > 0)
            {
                cmdExporttoExcel.Visible = true;
            }
            else
            {
                Label2.Text = "No Data Found b/w Selected Date";

            }
        }

        protected void cmdExporttoExcel_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=GatePassRequistionReport.xls");
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stwr = new StringWriter();
            HtmlTextWriter htew = new HtmlTextWriter(stwr);
            GridView1.RenderControl(htew);
            Response.Write(stwr.ToString());
            Response.End();
        }


    }
}
