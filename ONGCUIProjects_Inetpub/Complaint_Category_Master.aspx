<%@ Page language="c#" Codebehind="Complaint_Category_Master.aspx.cs" AutoEventWireup="True" Inherits="WebApplication1.Complaint_Category_Master" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Complaint_Category_Master</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
			function ConfirmDelete(msg)
			{
				return confirm("Are your sure to Insert ? \nThis can not be undone ...!");
			}
		</script>
	</HEAD>
	<body>
		<FORM id="Form1" method="post" runat="server">
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U></U></STRONG></FONT></FONT>&nbsp;</P>
			<P align="center">&nbsp;</P>
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U></U></STRONG></FONT></FONT>&nbsp;</P>
			<P align="center"><FONT face="Verdana" size="4"><FONT face="Verdana" color="#660000" size="3"><STRONG><U>Complaint 
								Category Master</U></STRONG></FONT></FONT><STRONG><FONT face="Verdana" color="red" size="1">&nbsp;&nbsp;&nbsp;
					</FONT></STRONG>
			</P>
			<P align="center">
				<TABLE id="Table2" style="WIDTH: 654px; HEIGHT: 56px" cellSpacing="8" cellPadding="4" width="654"
					align="center" bgColor="whitesmoke" border="1">
					<TR>
						<TD style="WIDTH: 335px" borderColor="gray" align="center" bgColor="whitesmoke">
							<P align="center"><FONT face="Verdana" color="#660000" size="1"><STRONG>Record New 
										Complaint Category</STRONG></FONT></P>
						</TD>
						<TD style="WIDTH: 281px" borderColor="gray" align="center" bgColor="whitesmoke"><FONT face="Verdana" size="1">
								<P align="center"><asp:textbox id="txtComplaintCatgry" runat="server" Width="280px" Font-Names="Verdana" Font-Size="XX-Small"
										MaxLength="200" CssClass="flattxt" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:textbox></P>
							</FONT>
						</TD>
					</TR>
				</TABLE>
			</P>
			<P align="center"><asp:button id="BtnSaveCmplntCtgry" runat="server" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Text="Create New Complaint Category" Font-Bold="True" BackColor="Silver"
					ForeColor="ControlText" onclick="BtnSaveCmplntCtgry_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="BtnRefresh" runat="server" Width="158px" Font-Names="Verdana" Font-Size="XX-Small"
					BorderStyle="Solid" BorderColor="DarkGray" Text="Refresh" CausesValidation="False" Font-Bold="True"
					BackColor="Silver" ForeColor="ControlText" Height="18px" onclick="BtnRefresh_Click"></asp:button></P>
		</FORM>
	</body>
</HTML>
