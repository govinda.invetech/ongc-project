using System.Data;


namespace WebApplication1
{
    public partial class Complaint_Registration : System.Web.UI.Page
    {


        #region PageLoad
        protected void Page_Load(object sender, System.EventArgs e)
        {
            string my_hint = "";
            if (Request.QueryString["my_hint"] != null)
            {
                my_hint = Request.QueryString["my_hint"].ToString();
            }
            lblmodule_hint.Text = my_hint;
            if(!IsPostBack)
            {
                Load_detail();
            }
            string cpf_no = "";
            cpf_no = Session["Login_Name"].ToString();
             string query = "";
                query = "select [child_id] from [CY_MENU_EMP_RELATION] where [cpf_number]='" + cpf_no + "' and child_id in('62','63','64','65','67','77')";
                ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
                DataSet ds = My.ExecuteSELECTQuery(query);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    mgr_cmlnt.Visible = true;
                }
            else
                {
                    mgr_cmlnt.Visible = false;
                }
        }
        #endregion
        public void Load_detail()
        {
            string query = "";
            query = "select [O_EMP_NM],[O_CPF_NMBR],[O_EMP_DESGNTN],[O_EMP_WRK_LCTN],[O_EMP_MBL_NMBR],[CY_EMP_EX_NO] from [CY_EMP_STATUS] where [O_CPF_NMBR]='" + Session["Login_Name"].ToString() + "' and [CY_STATUS]='ACTIVE'";
            ONGCUIProjects.MyApplication1 My = new ONGCUIProjects.MyApplication1();
            DataSet ds = My.ExecuteSELECTQuery(query);
            if(ds!=null && ds.Tables[0].Rows.Count>0)
            {
                txtappname.Text = ds.Tables[0].Rows[0]["O_EMP_NM"].ToString().Trim();
                string s = ds.Tables[0].Rows[0]["O_EMP_DESGNTN"].ToString();
                s = s.Replace(" ", "");
                txtappdesign.Text = s.ToString();
                Session["desigvalue"] = txtappdesign.Text.ToString();
                txtappcpf_no.Text = ds.Tables[0].Rows[0]["O_CPF_NMBR"].ToString().Trim();
                txtapplocation.Text = ds.Tables[0].Rows[0]["O_EMP_WRK_LCTN"].ToString().Trim();
                txtappphoneex_no.Text = ds.Tables[0].Rows[0]["CY_EMP_EX_NO"].ToString().Trim();
                txtmobile_no.Text = ds.Tables[0].Rows[0]["O_EMP_MBL_NMBR"].ToString().Trim();
                //txtcomment.Text = ds.Tables[0].Rows[0]["remarks"].ToString().Trim();
            }
            else
            {
                Response.Write("Employee Status Not Active");
            }
        }   
            

    }
}
