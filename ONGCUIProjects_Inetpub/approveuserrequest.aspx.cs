﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ONGCUIProjects
{
    public partial class approveuserrequest : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["Login_Name"] != null)
            {
                string myhint = Request.QueryString["myhint"];
                my_module_hint.Text = myhint;
            }
            else
            {
                Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
                return;
            }
            
        }
    }
}