using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace ONGCUIProjects
{
	public partial class PrivilagesMaster : System.Web.UI.Page
	{
		/*#region Variables
		TreeViewNode pNode;
		TreeViewNode pcNode;
		TreeViewNode strNode;
		int i;
		int j;
		string str;
		int k;
		ArrayList strNodeAry = new ArrayList();
		ArrayList strProjectCode = new ArrayList();
		string strcon=ConfigurationSettings.AppSettings["ConnectionString"];
		#endregion
	
		#region PageLoad
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				if(Session["Login_Name"] != null)
				{ 
					fillUserRole();
				}
				else
				{
					Response.Write("<script type=\"text/javascript\">top.location = 'Index.aspx?error=session&message=Session expired, please login again.';</script>");
				}
			}
			if(Session["DDLSelectedValue"] != null && Session["DDlSelectedText"] != null)
			{
				BindCheckdTreeView();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Ddl fillUserRole()       
		private void fillUserRole()
		{
			DataSet dsUserRole; 
			ONGCBusinessProjects.CerateUser objBusCerateUser = new ONGCBusinessProjects.CerateUser();
			dsUserRole = (DataSet)objBusCerateUser.FillUserAndRole(); 
			ddlRole.DataSource = dsUserRole;
			ddlRole.DataMember= dsUserRole.Tables[1].ToString();
			ddlRole.DataValueField = dsUserRole.Tables[1].Columns["P_ROLE_ID"].ToString();
			ddlRole.DataTextField =  dsUserRole.Tables[1].Columns["P_ROLE_DSCRPTN"].ToString().ToUpper();
			ddlRole.DataBind();	
			ddlRole.Items.Insert(0,"Select Role"); 
		}
		#endregion	

		#region SubParentNode
		private void fillTree(string parentID)
		{
			DataSet ds2=fillDataSet();
			for(j = 0 ; j <ds2.Tables[0].Rows.Count; j++)
			{
				if(ds2.Tables[0].Rows[j][0].ToString()==parentID)
				{
					strNode=new TreeViewNode();
					strNode.ID=ds2.Tables[0].Rows[j][2].ToString();
					strNode.Text=ds2.Tables[0].Rows[j][3].ToString();
					strNode.ShowCheckBox=true;
					DataSet dspc=new DataSet();
					dspc=fillDataSetPrivilages();
					foreach(DataRow drow in dspc.Tables[0].Rows)
					{
						if(strNode.ID == drow["P_CMPNT_CODE"].ToString())
						{
							strNode.Checked=true;
						}
					}
					pNode.Nodes.Add(strNode);
					fillTree1(strNode.ID);
				}	
			}	
		}
		#endregion

		#region SubParentNode
		private void fillTree1(string parentID)
		{	//it will bind the child node in parent node
			DataSet ds3=fillDataSet();
			for(k = 0 ; k <ds3.Tables[0].Rows.Count; k++)
			{
				if(ds3.Tables[0].Rows[k][0].ToString()==parentID)
				{
					pcNode=new TreeViewNode();
					pcNode.ID=ds3.Tables[0].Rows[k][2].ToString();
					pcNode.Text=ds3.Tables[0].Rows[k][3].ToString();
					pcNode.ShowCheckBox=true;
					DataSet dspcc=new DataSet();
					dspcc=fillDataSetPrivilages();
					foreach(DataRow drow in dspcc.Tables[0].Rows)
					{
						if(pcNode.ID == drow["P_CMPNT_CODE"].ToString())
						{
							pcNode.Checked=true;
						}
					}
					strNode.Nodes.Add(pcNode);
				}	
			}	
		}
		#endregion

		
		#region DataSet For Node
		private DataSet fillDataSet()
		{
			SqlConnection con=new SqlConnection(strcon);
			SqlDataAdapter sda1=new SqlDataAdapter("SELECT E_CMPNT_CODE_PRNT,E.E_CMPNT_DSCRPTN AS Parent, E_CMPNT_CODE_CHLD ,F.E_CMPNT_DSCRPTN AS Child, 'Y' AS Role, D.E_SQNCE_NMBR, F.E_CMPNT_URL,F.E_CMPNT_TYPE From E_CMPNT_MSTR E, E_CMPNT_MSTR F ,E_CMPNT_DTLS D Where E_CMPNT_CODE_PRNT=E.E_CMPNT_CODE AND E_CMPNT_CODE_CHLD = F.E_CMPNT_CODE AND D.E_CMPNT_CODE_CHLD IN (SELECT P_CMPNT_CODE FROM E_ROLE_DTLS WHERE P_ROLE_ID in (select E_ROLE_ID From E_USER_MSTR where E_USER_CODE = 'admin'))  ORDER BY E_SQNCE_NMBR",con );
			DataSet ds=new DataSet();
			sda1.Fill(ds);
			con.Close();
			return ds;
		}
		#endregion 

		#region DataSet For Check Privilages
		private DataSet fillDataSetPrivilages()
		{
			SqlConnection con=new SqlConnection(strcon);			
			SqlDataAdapter sda2=new SqlDataAdapter("SELECT P_ROLE_ID,P_CMPNT_CODE FROM E_ROLE_DTLS WHERE P_ROLE_ID='"+ddlRole.SelectedValue.ToString()+"'" ,con );
			DataSet dsPrivilages=new DataSet();
			sda2.Fill(dsPrivilages);
			con.Close();
			return dsPrivilages;
		}
		#endregion 

		#region  ShowCheckedNodes
		public void ShowCheckedNodes()
		{
			foreach (TreeViewNode myNode in TreeView2.CheckedNodes)
			{
				if(myNode.Checked)
				{
					strNodeAry.Add(myNode.ID);
				}	
			}
			
			for(int i=0;i<strNodeAry.Count;i++)
			{
				if(i == strNodeAry.Count-1)
				{
					str= str+"'"+strNodeAry[i]+"'";
				}
				else
				{
					str= str+"'"+strNodeAry[i]+"',";
				}
			}
			GetProjectCode(str);
			
		}
		#endregion

		#region GetProjectCode
		public void GetProjectCode(string str)
		{
			string code=str;
			if( str != null )
			{
				SqlConnection con=new SqlConnection(strcon);
				SqlCommand sqlcmd =new SqlCommand("SELECT DISTINCT E_CMPNT_MDLE FROM E_CMPNT_MSTR WHERE E_CMPNT_CODE IN("+code+")" ,con );
				con.Open();
				SqlDataReader sdr=sqlcmd.ExecuteReader();
				while(sdr.Read())
				{
					strProjectCode.Add(sdr["E_CMPNT_MDLE"]);
				}
			}

		}
		#endregion
		
		#region Image Button Click
		protected void imgBtnAddPrivillege_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			//calling Method to retrive the value of Checked Check Box
			ShowCheckedNodes();
			DeleteRoleModuleDetails();
			InsertRoleDetails();
			InsertRoleModuleDetails();
			TreeRefresh();
		}
		#endregion

		#region Insert into RoleDetails
		public void InsertRoleDetails()
		{
			ONGCCustumEntity.RoleCreation objCusRole = new ONGCCustumEntity.RoleCreation();
			ONGCBusinessProjects.RoleCreation objbusRole = new ONGCBusinessProjects.RoleCreation();
			objCusRole.p_ROLE_ID =ddlRole.SelectedValue.ToString();
			foreach(string strCMPNT_CODE in strNodeAry)
			{
				objCusRole.p_ROLE_ID = ddlRole.SelectedValue.ToString();
				objCusRole.p_CMPNT_CODE = strCMPNT_CODE;
				objbusRole.InsertRoleDetails(objCusRole);
			}
		}
		#endregion

		#region Delete from RoleDetails DeleteRoleModuleDetails
		public void DeleteRoleModuleDetails()
		{
			ONGCCustumEntity.RoleCreation objCusRole = new ONGCCustumEntity.RoleCreation();
			ONGCBusinessProjects.RoleCreation objbusRole = new ONGCBusinessProjects.RoleCreation();
			objCusRole.p_ROLE_ID =ddlRole.SelectedValue.ToString();
			objbusRole.DeleteRoleModuleDetails(objCusRole);
		}
		#endregion

		#region Insert into RoleModuleDetails
		public void InsertRoleModuleDetails()
		{
			ONGCCustumEntity.RoleCreation objCusRole = new ONGCCustumEntity.RoleCreation();
			ONGCBusinessProjects.RoleCreation objbusRole = new ONGCBusinessProjects.RoleCreation();
			objCusRole.p_ROLE_ID =ddlRole.SelectedValue.ToString();
			foreach(string strModuleCode in strProjectCode)
			{
				objCusRole.p_ROLE_ID = ddlRole.SelectedValue.ToString();
				objCusRole.p_ROLE_MDLE = strModuleCode;
				objbusRole.InsertRoleModuleDetails(objCusRole);
			}
		}
		#endregion

		#region DDl Selected index Change
		protected void ddlRole_SelectedIndexChanged(object sender, System.EventArgs e)
		{	
			Session["DDLSelectedValue"]=ddlRole.SelectedValue.ToString();
			Session["DDlSelectedText"]=ddlRole.SelectedItem.Text;
			TreeRefresh();
		}
		#endregion

		#region BindChecked TreeView
		public void BindCheckdTreeView()
		{	
			ddlRole.SelectedValue      =  Session["DDLSelectedValue"].ToString();
			ddlRole.SelectedItem.Text  =  Session["DDlSelectedText"].ToString();
			DataSet ds1=fillDataSet();
			for(i = 0 ; i <ds1.Tables[0].Rows.Count; i++)
			{
				string str=ds1.Tables[0].Rows[i]["parent"].ToString();
				//to Check parent node
				if(ds1.Tables[0].Rows[i]["E_SQNCE_NMBR"].ToString()== "1.00000000")
				{
					pNode=new TreeViewNode();
					pNode.ID=ds1.Tables[0].Rows[i][0].ToString();
					pNode.Text=ds1.Tables[0].Rows[i][1].ToString();
					pNode.ShowCheckBox=true;
					DataSet dsp=new DataSet();
					dsp=fillDataSetPrivilages();
					foreach(DataRow drow in dsp.Tables[0].Rows)
					{
						if(pNode.ID == drow["P_CMPNT_CODE"].ToString())
						{
							pNode.Checked=true;
						}
					}
					TreeView2.Nodes.Add(pNode);
					// for bind second child node of child node
					fillTree(pNode.ID);
				}	
			}
			Session["DDLSelectedValue"]= null;
			Session["DDlSelectedText"]=null;
		}
		#endregion

		#region ImageButton Refresh Click
		protected void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			TreeRefresh();
		}
		#endregion

		#region for refresh 
		public void TreeRefresh()
		{// for refreshing the treeView
			Response.Redirect("PrivilagesMaster.aspx");
		}
		#endregion*/
	}
}
